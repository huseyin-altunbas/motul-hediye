﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    public class SepetToplamlar
    {
        private decimal listeTutar;

        public decimal ListeTutar
        {
            get { return listeTutar; }
            set { listeTutar = value; }
        }
        private decimal indirimTutar;

        public decimal IndirimTutar
        {
            get { return indirimTutar; }
            set { indirimTutar = value; }
        }
        private decimal kdvTutar;

        public decimal KdvTutar
        {
            get { return kdvTutar; }
            set { kdvTutar = value; }
        }
        private decimal genelTutar;

        public decimal GenelTutar
        {
            get { return genelTutar; }
            set { genelTutar = value; }
        }
    }
}
