﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Goldb2cEntity.Custom
{
    public class Enums
    {
        public class Generic
        {
            public enum Status
            {
                [Description("Silinmiş")]
                Deleted = -1,
                [Description("Yeni")]
                New = 0,
                [Description("Aktif")]
                Active = 1,
                [Description("Pasif")]
                Passive = 2,
                [Description("Onaylandı")]
                Approved = 3,
                [Description("Reddedildi")]
                Denied = 4
            }

        }
        public class Transaction
        {

            public enum TransactionType
            {
                [Description("Aylık puan yüklemesi.")]
                MonthlyPlusPoints = 1,
                [Description("Siparişe istinaden düşülen puan.")]
                OrderMinusPoint = 2,
                [Description("Siparişe iptaline istinaden yüklenen puan.")]
                OrderPlusPoint = 3,
                [Description("Kampanya için yüklenen puan.")]
                CampaignPlusPoint = 4,
                [Description("Ceza için düşülen puan.")]
                CriminalMinusPoint = 5,
                [Description("Manuel yüklenen puan.")]
                ManuelPlusPoint = 6,
                [Description("Avans puan için yüklenen puan.")]
                AdvancePlusPoint = 7,
                [Description("Avans puan için düşülen puan.")]
                AdvanceMinusPoint = 8,
                [Description("Yıl sonunda silinen puan.")]
                PointDeleteYear = 9,
            }
        }


        public static string GetDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static Dictionary<string, int> GetDescriptions(Type enumType)
        {
            Array enumTypeValues = Enum.GetValues(enumType);

            Dictionary<string, int> descriptions = new Dictionary<string, int>(enumTypeValues.Length);
            for (int i = 0; i <= enumTypeValues.Length - 1; i++)
            {
                descriptions.Add(GetDescription((Enum)enumTypeValues.GetValue(i)), Convert.ToInt32(enumTypeValues.GetValue(i)));
            }
            return descriptions;
        }
    }
}
