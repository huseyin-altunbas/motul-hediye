﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    public class BankayaGonderilecekData
    {
        //out TaksitTip, out PostURL, out PostTip, out PostData, out Ayrac, out ClientId, out StoreKey, out TerminalId, out PosNetId, out UserName, out Password, out ProvizyonMail, out ProvizyonMailIcerik, out ProvizyonMinTutar, out ProvizyonKKHane, out BankaKod, out BankaAd
        public string TaksitTip { get; set; }
        public string PostUrl { get; set; }
        public string PostTip { get; set; }
        public string PostData { get; set; }
        public string Ayrac { get; set; }
        public string ClientId { get; set; }
        public string StoreKey { get; set; }
        public string TerminalId { get; set; }
        public string PosNetId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ProvizyonMail { get; set; }
        public string ProvizyonMailIcerik { get; set; }
        public decimal ProvizyonMinTutar { get; set; }
        public int ProvizyonKKHane { get; set; }
    }
}
