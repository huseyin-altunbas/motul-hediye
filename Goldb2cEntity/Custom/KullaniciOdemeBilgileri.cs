﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{

    [Serializable]
    public class KullaniciOdemeBilgileri
    {
        public int SiparisMasterId { get; set; }
        public int TahsilatSecenekId { get; set; }
        public int TahsilatSecenekDetayId { get; set; }
        public string GuvenlikSeviye { get; set; }
        public bool PuanSorgula { get; set; }
        public decimal CekilecekTutar { get; set; }
        public string OdeyenAdSoyad { get; set; }
        public string KartNo { get; set; }
        public string Cvc { get; set; }
        public string SonKulAy { get; set; }
        public string SonKulYil { get; set; }
        public string KartTipi { get; set; }
    }



}
