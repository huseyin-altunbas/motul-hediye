﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Goldb2cEntity.Custom
{
    public class TransactionView : Transaction
    {
        public int totalRows { get; set; }
        public string statusText { get { return Enums.GetDescription(this.status); } }
        public int statusNumber { get { return (int)this.status; } }
        public string transactionTypeText { get { return Enums.GetDescription(this.transactionType); } }
        public int KullaniciId { get; set; }
        public string KullaniciAd { get; set; }
        public string CRMCustomerGUID { get; set; }
    }
}
