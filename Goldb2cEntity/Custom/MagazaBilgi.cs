﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    public class MagazaBilgi
    {
        public string MagazaAd { get; set; }
        public string MagazaAdres1 { get; set; }
        public string MagazaAdres2 { get; set; }
        public string MagazaAdres3 { get; set; }
        public string MagazaYetkili { get; set; }
        public string MagazaTelefon { get; set; }
        public string MagazaFax { get; set; }
        public string MagazaEmail { get; set; }
        public string MagazaIlce { get; set; }
        public string MagazaSehir { get; set; }
        public string MagazaResimKod { get; set; }
        public string MagazaAcilisKapanisSaat { get; set; }
    }
}
