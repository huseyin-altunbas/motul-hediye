﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    [Serializable]
    public class BkmExpressParams
    {
        public string PostForm { get; set; }
        public decimal OdenecekTutar { get; set; }
    }
}
