﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    [Serializable]
    public class OciParams
    {
        public string HookUrl { get; set; }
        public string ViewId { get; set; }
        public string ViewPasswd { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string OciVersion { get; set; }
        public string Vendor { get; set; }
    }
}
