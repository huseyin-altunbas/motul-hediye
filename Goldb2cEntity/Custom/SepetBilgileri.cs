﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    public class SepetBilgileri
    {
        public SepetMaster SepetMaster { get; set; }
        public List<SepetDetay> SepetDetayList { get; set; }
    }

    public class SepetDetay
    {
        public int SepetDetayId { get; set; }
        public int SepetMasterId { get; set; }
        public string UrunId { get; set; }
        public string UrunAd { get; set; }
        public int UrunAdet { get; set; }
        public string UrunLink { get; set; }
        public decimal ListeTutar { get; set; }
        public decimal ListeNetTutar { get; set; }
        public string ResimAd { get; set; }
    }

    public class SepetMaster
    {
        public int SepetMasterId { get; set; }
        public decimal ListeTutar { get; set; }
        public decimal IndirimTutar { get; set; }
        public decimal KdvTutar { get; set; }
        public decimal GenelTutar { get; set; }
        public int UrunSayisi { get; set; }
    }
}
