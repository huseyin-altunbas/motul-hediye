﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Goldb2cEntity
{
    [Serializable]
    public class SiparisOdemeLog
    {
        public int OdemeLogId { get; set; }                 /* kayitid */
        public string PostUrl { get; set; }                 /* banka post url */
        public int ErpCariId { get; set; }                  /* cari_id */
        public string IpAdres { get; set; }                 /* ip_adres */
        public string KartNo { get; set; }                  /* kart_no */
        public string KartAdSoyad { get; set; }             /* kart_sahibi */
        public string KartCvc { get; set; }                 /* kart cvc */
        public string KartTip { get; set; }                 /* kart_tipi */
        public string SonKulAy { get; set; }                /* kart_skay */
        public string SonKulYil { get; set; }               /* kart_skyil */
        public decimal Tutar { get; set; }                  /* bruttahsilat_tutar */
        public DateTime IslemBaslangicTarih { get; set; }   /* trans_baszaman */
        public DateTime IslemBitisTarih { get; set; }       /* trans_bitzaman */
        public string OrderId { get; set; }                 /* order_id */
        public string PortalKod { get; set; }               /* portal_kod */
        public string CekimTur { get; set; }                /* cekim_tur */
        public string GidenForm { get; set; }               /* giden_form */
        public string GelenForm { get; set; }               /* gelen_form */
        public string HataKod { get; set; }                 /* hata_kod */
        public string HataAciklama { get; set; }            /* hata_aciklama */
        public string MdStatus { get; set; }                /* md_status */
        public string SessionId { get; set; }               /* session_id */
        public int SiparisMasterId { get; set; }            /* siparis_id */
        public string TaksitAdet { get; set; }              /* taksit_adet*/
        public int TahsilatSecenekId { get; set; }          /* odeme_masterid */
        public int TahsilatSecenekDetayId { get; set; }     /* odeme_detayid */
        public string OnayDurum { get; set; }               /* onay_durum */
        public string Email { get; set; }                   /* email */
    }



}
