﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    public class TedarikciFaturaDetayEnt
    {
        public string fatura_masterno { get; set; }
        public string stok_kod { get; set; }
        public string stok_ad { get; set; }
        public string islem_miktar { get; set; }
        public string islem_tutar { get; set; }
    }

    public class entTedarikciFaturaDetay
    {
        public string fatura_masterno { get; set; }
        public string belge_tarih { get; set; }
        public string belge_no { get; set; }
        public string stok_kod { get; set; }
        public string stok_ad { get; set; }
        public string islem_miktar { get; set; }
        public string islem_tutar { get; set; }
    }

    public class entTedarikciDepo
    {
        public string depo_kod { get; set; }
        public string depo_ad { get; set; }
        public string stok_kod { get; set; }
        public string stok_ad { get; set; }
        public string stok_miktar { get; set; }
        public string satis_miktar { get; set; }
        public string bekleme { get; set; }
    }

    public class entTedarikciDepoFatura
    {
        public List<entTedarikciDepo> depo { get; set; }
        public List<entTedarikciFaturaDetay> fatura { get; set; }
    }

    [Serializable]
    public class entTedarikciKullanici
    {
        public string tedarikci_kod { get; set; }
        public string tedarikci_mesaj { get; set; }
        public decimal tedarikci_bakiye { get; set; }
        public string tedarikci_bakiyetip { get; set; }
    }
}
