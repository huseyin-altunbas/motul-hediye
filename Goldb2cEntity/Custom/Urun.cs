﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    public class SonIncelenenUrun
    {
        public string UrunId { get; set; }
        public string UrunAd { get; set; }
        public string UrunResim { get; set; }
        public string UrunLink { get; set; }
        private string netTutar;
        public string NetTutar
        {
            get { return String.Format("{0:0.00}", Convert.ToDecimal(netTutar)); }
            set { netTutar = value; }
        }

        private string brutTutar;
        public string BrutTutar
        {
            get { return String.Format("{0:0.00}", Convert.ToDecimal(brutTutar)); }
            set { brutTutar = value; }
        }

    }

    public class HitPanelUrun
    {
        public string UrunId { get; set; }
        public string UrunAd { get; set; }
        public string UrunLink { get; set; }
        public string UrunResimAd { get; set; }
        private string urunBrutTutar;
        public string UrunBrutTutar
        {
            get { return String.Format("{0:###,###,###}", Convert.ToDecimal(urunBrutTutar)); }
            set { urunBrutTutar = value; }
        }
        private string urunNetTutar;
        public string UrunNetTutar
        {
            get { return String.Format("{0:###,###,###}", Convert.ToDecimal(urunNetTutar)); }
            set { urunNetTutar = value; }
        }
    }
}





