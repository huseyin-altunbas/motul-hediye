﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    public class Kategori
    {
        public string UrunGrupId { get; set; }
        public string UrunGrupAd { get; set; }

        private string urunGrupUrl;

        public string UrunGrupUrl
        {
            get { return urunGrupUrl; }
            set { urunGrupUrl = value; }
        }

        public string UrunSayisi { get; set; }
    }

}
