﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    public class SiparisDetayEx
    {
        public int Miktar { get; set; }
        public double TLTutar { get; set; }
        public double PuanTutar { get; set; }
        public string StokKod { get; set; }
        public string StokAd { get; set; }
        public double PuanCarpan { get; set; }
        public string EkAlan1 { get; set; }
        public string EkAlan2 { get; set; }
    }
}
