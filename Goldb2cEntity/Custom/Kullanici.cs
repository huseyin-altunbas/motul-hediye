﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{

    [Serializable]
    public struct Kullanici
    {
        public int ErpCariId;
        public int KullaniciId;
        public string KullaniciAd;
        public string KullaniciSoyad;
        public string Email;
        public string Tel;
        public string CepTel;
        public string UlkeKod;
        public string SehirKod;
        public string IlceKod;
        public DateTime DogumTarihi;
        public DateTime UyelikTarihi;
        public DateTime GuncellemeTarihi;
        public DateTime SonGirisTarihi;
        public string SonGirisIP;
        public bool Online;
        public int UyelikTipi;
        public int GirisSayisi;
        public string IndirimKod;
        public string Cinsiyet;
        public string EmailAktivasyonKod;
        public string RefEmail;
        public bool? EmailAktif;
        public bool? SmsAKtif;
        public string PortalKod;
        public string UyelikKanali;
        public string FacebookId;
        public int totalGoldPoint;
        public int totalPointAdvance;
        public string CRMCustomerGUID;
        public string Key0;
        public string Key1;
        public int totalRows;
        public string CepTelNo;
        public bool Aktif;
        public string AktifText;
        public Guid ServiceKey;
        public bool IlkSifreDegistimi;
                                           //public static sKullanici KullaniciBilgileriVer()
                                           //{
                                           //    sKullanici user = new sKullanici();
                                           //    if (HttpContext.Current.Session["sUser"] != null)
                                           //    {
                                           //        user = (sKullanici)HttpContext.Current.Session["sUser"];
                                           //    }
                                           //    return user;
                                           //}
    }

    [Serializable]
    public class FacebookUser
    {

        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string birthday { get; set; }
        public string gender { get; set; }
    }

}
