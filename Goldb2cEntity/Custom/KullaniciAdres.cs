﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    public class KullaniciAdres
    {
        public string AdresBaslik { get; set; }
        public string AdresTanim { get; set; }
        public string YazismaUnvan { get; set; }
        public string PostaKod { get; set; }
        public string SehirKod { get; set; }
        public string IlceKod { get; set; }
        public string SabitTelKod { get; set; }
        public string SabitTel { get; set; }
        public string CepTelKod { get; set; }
        public string CepTel { get; set; }
        public string VergiDaire { get; set; }
        public string VergiNo { get; set; }
        public string TcKimlikNo { get; set; }

    }
}
