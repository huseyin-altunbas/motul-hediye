﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity.Custom
{
    public class KullaniciSiparisLog
    {

        public int LogId { get; set; }
        public int? KullaniciId { get; set; }
        public int? KullaniciUyelikTip { get; set; }
        public int? SiparisId { get; set; }
        public string Aciklama { get; set; }
        public string HataMesaj { get; set; }
        public DateTime? LogTarih { get; set; }
        public string KullaniciIpAdres { get; set; }
        public string EkAlan1 { get; set; }
        public string EkAlan2 { get; set; }
        public string Tip { get; set; }
        public static KullaniciSiparisLog LogObjeOlustur(int _KullaniciId, int _SiparisId, String _Aciklama, string _HataMesaj, string _KullaniciIpAdres)
        {
            KullaniciSiparisLog _KullaniciSiparisLog = new KullaniciSiparisLog();
            _KullaniciSiparisLog.KullaniciId = _KullaniciId;
            _KullaniciSiparisLog.SiparisId = _SiparisId;
            _KullaniciSiparisLog.Aciklama = _Aciklama;
            _KullaniciSiparisLog.HataMesaj = _HataMesaj;
            _KullaniciSiparisLog.LogTarih = DateTime.Now;
            _KullaniciSiparisLog.KullaniciIpAdres = _KullaniciIpAdres;
            return _KullaniciSiparisLog;
        }

        public static KullaniciSiparisLog LogObjeOlustur(int _KullaniciId, int _SiparisId, String _Aciklama, string _HataMesaj, string _EkAlan1, string _EkAlan2, string _KullaniciIpAdres)
        {
            KullaniciSiparisLog _KullaniciSiparisLog = new KullaniciSiparisLog();
            _KullaniciSiparisLog.KullaniciId = _KullaniciId;
            _KullaniciSiparisLog.SiparisId = _SiparisId;
            _KullaniciSiparisLog.Aciklama = _Aciklama;
            _KullaniciSiparisLog.HataMesaj = _HataMesaj;
            _KullaniciSiparisLog.EkAlan1 = _EkAlan1;
            _KullaniciSiparisLog.EkAlan1 = _EkAlan2;
            _KullaniciSiparisLog.LogTarih = DateTime.Now;
            _KullaniciSiparisLog.KullaniciIpAdres = _KullaniciIpAdres;
            return _KullaniciSiparisLog;
        }
    }


}
