﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    public class Transaction
    {
        public int id { get; set; }
        public int userId { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public decimal amount { get; set; }
        public Custom.Enums.Transaction.TransactionType transactionType { get; set; }
        public Goldb2cEntity.Custom.Enums.Generic.Status status { get; set; }
        public DateTime period { get; set; }
        public DateTime created { get; set; }
        public DateTime updated { get; set; }
    }

}
