﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    public class UzmanSihirbazi
    {

    }

    public class entUzmanSihirbazResim
    {
        public string ResimAd { get; set; }
        public string ResimWebAd { get; set; }
    }

    public class entUzmanGorusuSablon
    {
        public string SablonId { get; set; }
        public string SablonAd { get; set; }
        public string SablonHtml { get; set; }
        public string SablonInputHtml { get; set; }
        public string SablonParametre { get; set; }
    }
}
