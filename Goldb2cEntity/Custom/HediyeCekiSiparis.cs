﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity.Custom
{
    public class HediyeCekiSiparis
    {
        public int Id { get; set; }
        public int? ErpCariId { get; set; }
        public int? SiparisId { get; set; }
        public int? StokKod { get; set; }
        public int? Adet { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Email { get; set; }
        public string Gsm { get; set; }
        public bool HediyeCekiGonderildimi { get; set; }
        public DateTime? Olusturma { get; set; }
    }
}
