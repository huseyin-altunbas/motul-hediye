﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{

    public class TahsilatDetay
    {
        public int MasterId { get; set; }
        public int DetayId { get; set; }
        public string TahsilatAd { get; set; }
        public string Aciklama { get; set; }
        public string TaksitAdet { get; set; }
        public int gTaksitAdet { get; set; }
        public decimal ToplamTutar { get; set; }
        public decimal TaksitTutar { get; set; }
    }

    public class TahsilatSecenek
    {
        public int MasterId { get; set; }
        public string BankaAd { get; set; }
        public string BankaResimYol { get; set; }
        public string AnlasmaliKurumlar { get; set; }
    }

    public class AlinanTahsilatlar
    {
        public string TahsilatAd { get; set; }
        public decimal TahsilatTutar { get; set; }
    }
}
