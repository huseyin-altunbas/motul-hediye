﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Goldb2cEntity.Custom
{
    public class PuanView
    {
        public int kalan { get; set; }
        public int yuklenen { get; set; }
        public int silinecek { get; set; }

        public string kalanFormat
        {
            get
            {
                
                    return kalan.ToString("###,###,###");
               
            }
        }


        public string yuklenenFormat
        {
            get
            {
                if (yuklenen > 0)
                    return yuklenen.ToString("###,###,###");
                else
                    return "0";
            }
        }

        public string silinecekFormat
        {
            get
            {
                if (silinecek > 0)
                    return silinecek.ToString("###,###,###");
                else
                    return "0";
            }
        }



        public string harcananFormat
        {
            get
            {
                int harcanan = yuklenen - kalan;
                if (harcanan > 0)
                    return harcanan.ToString("###,###,###");
                else
                    return "0";
            }
        }


    }
}
