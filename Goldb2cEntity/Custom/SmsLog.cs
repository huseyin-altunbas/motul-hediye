﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity.Custom
{
    public class SmsLog
    {
        public int Id { get; set; }
        public int? SiparisId { get; set; }
        public int? SmsGonderim { get; set; }
        public string Gsm { get; set; }
        public DateTime? Olusturma { get; set; }
    }
}