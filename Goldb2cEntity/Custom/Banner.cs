﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cEntity
{
    public class entAnaSayfaBanner
    {
        public string BannerResim { get; set; }
        public string BannerLink { get; set; }
        public string AlternateTextTitle { get; set; }
    }
}
