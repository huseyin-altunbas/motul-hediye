﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Goldb2cDAL;
using System.Data;
using System.Data.SqlClient;

public class MenuItemIslemleriDAT
{
    DbConnector dbConn = new DbConnector();

    public MenuItemIslemleriDAT()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetirMenuItems()
    {
        DataTable dtMenuItems = new DataTable();

        dbConn.OpenConnection();
        dtMenuItems = dbConn.ExecuteDataTable("GetirMenuItems");
        dbConn.CloseConnecion();

        return dtMenuItems;
    }

    public DataTable GetirMenuItems(string itemId)
    {
        DataTable dtMenuItems = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@ItemId", itemId);

        dbConn.OpenConnection();
        dtMenuItems = dbConn.ExecuteDataTable("GetirMenuItemsByItemId",dbParamColl);
        dbConn.CloseConnecion();

        return dtMenuItems;
    }

    public int GuncelleItemNameItemUrl(string itemId,string itemName,string itemUrl)
    {
        int sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();

        dbParamColl.Add("@ItemId", itemId);
        dbParamColl.Add("@ItemName", itemName);
        dbParamColl.Add("@ItemUrl", itemUrl);

        dbConn.OpenConnection();
        sonuc = dbConn.ExecuteNonQuery("GuncelleItemNameItemUrlByItemId", dbParamColl);
        dbConn.CloseConnecion();

        return sonuc;
    }

    public int GuncelleItemHtml(string itemId, string itemHtml)
    {
        int sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();

        dbParamColl.Add("@ItemId", itemId);
        dbParamColl.Add("@ItemHtml", itemHtml);

        dbConn.OpenConnection();
        sonuc = dbConn.ExecuteNonQuery("GuncelleItemHtmlbyItemId", dbParamColl);
        dbConn.CloseConnecion();

        return sonuc;
    }
}

