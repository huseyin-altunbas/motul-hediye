﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cDAL;
using System.Data;

namespace Goldb2cDAT
{
    public class BannerDAT
    {
        DbConnector dbConn = new DbConnector();
        public DataTable GetirBanner(int BannerId)
        {
            DataTable dtBanner = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@BannerId", BannerId);
            dbConn.OpenConnection();
            dtBanner = dbConn.ExecuteDataTable("GetirBanner", dbParamColl);
            dbConn.CloseConnecion();

            return dtBanner;
        }

        public int GuncelleBanner(int BannerId, string BannerHtml)
        {
            int Sonuc = 0;
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@BannerId", BannerId);
            dbParamColl.Add("@BannerHtml", BannerHtml);
            dbConn.OpenConnection();
            Sonuc = dbConn.ExecuteNonQuery("GuncelleBanner", dbParamColl);
            dbConn.CloseConnecion();

            return Sonuc;
        }

        public int GuncelleMenuItemBanner(string ItemId, string ItemBannerField)
        {
            int Sonuc = 0;
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@ItemId", ItemId);
            dbParamColl.Add("@ItemBannerField", ItemBannerField);
            dbConn.OpenConnection();
            Sonuc = dbConn.ExecuteNonQuery("GuncelleMenuItemBanner", dbParamColl);
            dbConn.CloseConnecion();

            return Sonuc;
        }
    }
}
