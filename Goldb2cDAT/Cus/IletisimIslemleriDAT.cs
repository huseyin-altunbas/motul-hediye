﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Goldb2cDAL;
using System.Data;
using System.Data.SqlClient;


public class IletisimIslemleriDAT
{
    DbConnector dbConn = new DbConnector();

    public DataTable IletisimFormuGetir()
    {
        DataTable dt = new DataTable();

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("IletisimFormuGetir");
        dbConn.CloseConnecion();

        return dt;
    }

    public int IletisimFormuKaydet(string ad, string soyad, string email, string cepTelKod, string cepTelNo, string kartNo, string soru, string ipAdres)
    {
        int Sonuc = 0;
        try
        {
            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@ad", ad);
            dbParamColl.Add("@soyad", soyad);
            dbParamColl.Add("@email", email);
            dbParamColl.Add("@cepTelKod", cepTelKod);
            dbParamColl.Add("@cepTelNo", cepTelNo);
            dbParamColl.Add("@kartNo", kartNo);
            dbParamColl.Add("@soru", soru);
            dbParamColl.Add("@ipAdres", ipAdres);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteScalar("IletisimFormuKaydet", dbParamColl));
            dbConn.CloseConnecion();

        }
        catch (Exception)
        {
            return 0;
        }
        return Sonuc;
    }


}

