﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cDAL;
using System.Data;
using System.Data.SqlClient;

namespace Goldb2cDAT
{
    public class BkmExpressDAT
    {
        DbConnector dbConn = new DbConnector();

        public int InsertBkmVeriLog(string OrderId, int SiparisId, string IpAdres, int KullaniciId, string BkmKampanyaKod)
        {
            int BkmVeriLogId = 0;


            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@OrderId", OrderId);
            dbParamColl.Add("@SiparisId", SiparisId);
            dbParamColl.Add("@IpAdres", IpAdres);
            dbParamColl.Add("@KullaniciId", KullaniciId);
            dbParamColl.Add("@BkmKampanyaKod", BkmKampanyaKod);

            dbConn.OpenConnection();
            BkmVeriLogId = Convert.ToInt32(dbConn.ExecuteScalar("InsertBkmVeriLog2", dbParamColl));
            dbConn.CloseConnecion();

            return BkmVeriLogId;
        }

        public int UpdateBkmVeriLogByBkmVeriLogId(int BkmVeriLogId, string BkmTransactionId)
        {

            int Sonuc = 0;

            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@BkmVeriLogId", BkmVeriLogId);
            dbParamColl.Add("@BkmTransactionId", BkmTransactionId);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteNonQuery("UpdateBkmVeriLogByBkmVeriLogId", dbParamColl));
            dbConn.CloseConnecion();

            return Sonuc;
        }

        public int InsertBkmVeriLogDetay(int BkmVeriLogId, DataRow drTahsilatDetay)
        {
            int Sonuc = 0;

            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@BkmVeriLogId", BkmVeriLogId);
            dbParamColl.Add("@BkmBankId", drTahsilatDetay["bkm_id"].ToString());
            dbParamColl.Add("@BkmTaksitSayisi", drTahsilatDetay["gtaksit_adet"].ToString());
            dbParamColl.Add("@TahsilatSecenekMasterId", drTahsilatDetay["master_id"].ToString());
            dbParamColl.Add("@TahsilatSecenekDetayId", drTahsilatDetay["detay_id"].ToString());
            dbParamColl.Add("@ToplamTutar", Convert.ToDecimal(drTahsilatDetay["toplam_tutar"].ToString()));

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteNonQuery("InsertBkmVeriLogDetay", dbParamColl));
            dbConn.CloseConnecion();

            return Sonuc;

        }

        public void UpdateBkmVeriLogSonuc(string CevapGeldi, string Bkm_t, string Bkm_posMessage, string Bkm_XID, string Bkm_MD, string Bkm_ts)
        {
            int Sonuc = 0;

            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@BkmTransactionId", Bkm_t);
            dbParamColl.Add("@CevapGeldi", CevapGeldi);
            dbParamColl.Add("@Bkm_posMessage", Bkm_posMessage);
            dbParamColl.Add("@Bkm_XID", Bkm_XID);
            dbParamColl.Add("@Bkm_MD", Bkm_MD);
            dbParamColl.Add("@Bkm_ts", Bkm_ts);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteNonQuery("UpdateBkmVeriLogSonuc", dbParamColl));
            dbConn.CloseConnecion();

        }


        public DataTable SelectSecilenBkmVeriLogByBkmTransactionId(string t)
        {
            DataTable dtBkmVeriLog = new DataTable();

            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@BkmTransactionId", t);


            dbConn.OpenConnection();
            dtBkmVeriLog = dbConn.ExecuteDataTable("SelectSecilenBkmVeriLogByBkmTransactionId", dbParamColl);
            dbConn.CloseConnecion();

            return dtBkmVeriLog;
        }

        public int SelectBkmKampanyaGecerlilik(int KullaniciId, string BkmKampanyaKod, int MaxKatilimSayisi, DateTime KampanyaBaslangic, DateTime KampanyaBitis)
        {
            int Sonuc = 0;

            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@KullaniciId", KullaniciId);
            dbParamColl.Add("@BkmKampanyaKod", BkmKampanyaKod);
            dbParamColl.Add("@MaxKatilimSayisi", MaxKatilimSayisi);
            dbParamColl.Add("@KampanyaBaslangic", KampanyaBaslangic);
            dbParamColl.Add("@KampanyaBitis", KampanyaBitis);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteScalar("SelectBkmKampanyaGecerlilik", dbParamColl));
            dbConn.CloseConnecion();

            return Sonuc;
        }
    }
}
