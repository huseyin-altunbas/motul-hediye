﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cDAL;

namespace Goldb2cDAT
{
    public class PostaciDAT
    {
        DbConnector dbConn = new DbConnector();
        public int InsertPostaci(int ProjeId, string MailTo, string MailCc, string MailBcc, bool IsHtml, string MailAttach, string MailSubject, string MailBody, string Data1)
        {
            int Sonuc = 0;

            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@ProjeId", ProjeId);
            dbParamColl.Add("@MailTo", MailTo);
            dbParamColl.Add("@MailCc", MailCc);
            dbParamColl.Add("@MailBcc", MailBcc);
            dbParamColl.Add("@IsHtml", IsHtml);
            dbParamColl.Add("@MailAttach", MailAttach);
            dbParamColl.Add("@MailSubject", MailSubject);
            dbParamColl.Add("@MailBody", MailBody);
            dbParamColl.Add("@Data1", Data1);


            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteScalar("InsertPostaci", dbParamColl));
            dbConn.CloseConnecion();

            return Sonuc;
        }

    }
}
