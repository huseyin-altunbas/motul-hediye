﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Goldb2cDAL;
using System.Data;
using System.Data.SqlClient;
using Kendo.DynamicLinq;
using Goldb2cEntity;
using Dapper;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for UyelikIslemleriDAT
/// </summary>
public class UyelikIslemleriDAT
{
    DbConnector dbConn = new DbConnector();

    public UyelikIslemleriDAT()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetirKullaniciByServiceKey(string ServiceKey)
    {
        DataTable dtKullanici = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@ServiceKey", ServiceKey);


        dbConn.OpenConnection();
        dtKullanici = dbConn.ExecuteDataTable("GetirKullaniciByServiceKey", dbParamColl);
        dbConn.CloseConnecion();

        return dtKullanici;
    }

    public DataTable MusteriKullaniciGirisWithCustomerID(string CustomerID, string Sifre)
    {
        DataTable kullanici = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();

        dbParamColl.Add("@CustomerID", CustomerID);
        dbParamColl.Add("@Sifre", Sifre);

        dbConn.OpenConnection();
        kullanici = dbConn.ExecuteDataTable("sp_KullaniciLoginWithCustomerID", dbParamColl);
        dbConn.CloseConnecion();

        return kullanici;

    }

    public List<Kullanici> Get(View filters)
    {

        string sql = @"  select  COUNT(*) OVER () as totalRows,* from (
      


 select *,cast(yuklenen-kalan as int) as harcanan from (
 
                      
 select *,
 
 CASE Aktif
  WHEN 1 THEN 'Aktif' 
  WHEN 0 THEN 'Pasif'  
  ELSE 'Pasif' 
END as AktifText,

(select coalesce(sum(_Transaction.amount) ,0)  from [Transaction] _Transaction where userId=Kullanici.KullaniciId) as kalan,
(select coalesce(sum(_Transaction.amount) ,0)  from [Transaction] _Transaction where _Transaction.amount>0 and userId=Kullanici.KullaniciId) as yuklenen

from Kullanici where UyelikTipi=0

) as gg


                            ) as t
                              {0} {1}
                              OFFSET {2} ROWS FETCH NEXT {3} ROWS ONLY";
        QueryView queryView = FilterHelper.SqlBuilder(filters, sql);
        queryView.sql = queryView.sql.Replace("ORDER BY id", "ORDER BY KullaniciId");
        var args = new DynamicParameters(new { });
        queryView.filterParameters.ForEach(p => args.Add(p.ParameterName, p.Value));
        using (IDbConnection db = new SqlConnection(dbConn.ConnectionString))
        {
            return (db.Query<Kullanici>(queryView.sql, args)).ToList();
        }
    }


    public DataTable GetDataTable(View filters)
    {

        string sql = @"  select  COUNT(*) OVER () as totalRows,* from (


 select *,cast(yuklenen-kalan as int) as harcanan from (
 
                      
 select *,
 
 CASE Aktif
  WHEN 1 THEN 'Aktif' 
  WHEN 0 THEN 'Pasif'  
  ELSE 'Pasif' 
END as AktifText,

(select coalesce(sum(_Transaction.amount) ,0)  from [Transaction] _Transaction where userId=Kullanici.KullaniciId) as kalan,
(select coalesce(sum(_Transaction.amount) ,0)  from [Transaction] _Transaction where _Transaction.amount>0 and userId=Kullanici.KullaniciId) as yuklenen

from Kullanici where UyelikTipi=0

) as gg

                            ) as t
                              {0} {1}
                              OFFSET {2} ROWS FETCH NEXT {3} ROWS ONLY";
        QueryView queryView = FilterHelper.SqlBuilder(filters, sql);
        queryView.sql = queryView.sql.Replace("ORDER BY id", "ORDER BY KullaniciId");
        //var args = new DynamicParameters(new { });
        //queryView.filterParameters.ForEach(p => args.Add(p.ParameterName, p.Value));
        //using (IDbConnection db = new SqlConnection(dbConn.ConnectionString))
        //{
        //    return (db.Query<Kullanici>(queryView.sql, args)).ToList();
        //}

        DataSet ds = SqlHelper.ExecuteDataset(dbConn.ConnectionString, CommandType.Text, queryView.sql, queryView.filterParameters.ToArray());
        return ds.Tables[0];
    }



    public int MusteriKullaniciGiris(string email, string sifre)
    {
        int KullaniciId = 0;

        DbParamCollection dbParamColl = new DbParamCollection();

        dbParamColl.Add("@email", email);
        dbParamColl.Add("@sifre", sifre);

        dbConn.OpenConnection();
        KullaniciId = Convert.ToInt32(dbConn.ExecuteScalar("GetirKullaniciGiris", dbParamColl));
        dbConn.CloseConnecion();

        return KullaniciId;

    }

    public DataTable MusteriKullaniciSistemKayit(int MusteriKullaniciId, string sonGirisIP, string SessionId)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@kullaniciid", MusteriKullaniciId);
        dbParamColl.Add("@songirisip", sonGirisIP);
        dbParamColl.Add("@sessionid", SessionId);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("GuncelleMusteriKullaniciByKullaniciId", dbParamColl);
        dbConn.CloseConnecion();

        return dt;
    }

    public void MusteriKullaniciCikis(int kullaniciID)
    {
        DbParamCollection dbParamColl = new DbParamCollection();

        dbParamColl.Add("@kullaniciid", kullaniciID);
        dbConn.OpenConnection();
        dbConn.ExecuteNonQuery("GuncelleKullaniciCikis", dbParamColl);
        dbConn.CloseConnecion();
    }

    public int MusteriKullaniciKayit(string musteriAd, string musteriSoyad, string email, string sifre, DateTime dogumTarihi, string cinsiyet, string ulkeKod, string sehirKod, string ilceKod, string telKod, string telNo, string telDahili, string cepTelKod, string cepTelNo, bool cepHaber, bool emailHaber, string UyelikKanali, string neredenDuydu, string ilgiAlanlari, string sonGirisIP, string refemail, string EmailAktivasyonKod, string CRMCustomerGUID, bool Aktif)
    {
        int val = 0;
        try
        {

            SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
            SqlCommand sqlcomm = new SqlCommand("KaydetKullanici");
            sqlcomm.CommandType = CommandType.StoredProcedure;
            sqlcomm.Connection = sqlcon;
            sqlcomm.Parameters.AddWithValue("@musteriad", musteriAd);
            sqlcomm.Parameters.AddWithValue("@musterisoyad", musteriSoyad);
            sqlcomm.Parameters.AddWithValue("@email", email);
            sqlcomm.Parameters.AddWithValue("@sifre", sifre);
            sqlcomm.Parameters.AddWithValue("@dogumtarihi", dogumTarihi);
            sqlcomm.Parameters.AddWithValue("@cinsiyet", cinsiyet);
            sqlcomm.Parameters.AddWithValue("@ulkekod", ulkeKod);
            sqlcomm.Parameters.AddWithValue("@sehirkod", sehirKod);
            sqlcomm.Parameters.AddWithValue("@ilcekod", ilceKod);
            sqlcomm.Parameters.AddWithValue("@telkod", telKod);
            sqlcomm.Parameters.AddWithValue("@telno", telNo);
            sqlcomm.Parameters.AddWithValue("@teldahili", telDahili);
            sqlcomm.Parameters.AddWithValue("@ceptelkod", cepTelKod);
            sqlcomm.Parameters.AddWithValue("@ceptelno", cepTelNo);
            sqlcomm.Parameters.AddWithValue("@cephaber", cepHaber);
            sqlcomm.Parameters.AddWithValue("@emailhaber", emailHaber);
            sqlcomm.Parameters.AddWithValue("@neredenduydu", neredenDuydu);
            sqlcomm.Parameters.AddWithValue("@ilgialanlari", ilgiAlanlari);
            sqlcomm.Parameters.AddWithValue("@songirisip", sonGirisIP);
            sqlcomm.Parameters.AddWithValue("@refemail", refemail);
            sqlcomm.Parameters.AddWithValue("@emailaktivasyonkod", EmailAktivasyonKod);
            sqlcomm.Parameters.AddWithValue("@portalkod", "GoldB2C");
            sqlcomm.Parameters.AddWithValue("@UyelikKanali", UyelikKanali);
            sqlcomm.Parameters.AddWithValue("@CRMCustomerGUID", CRMCustomerGUID);
            sqlcomm.Parameters.AddWithValue("@Aktif", Aktif);

            sqlcon.Open();
            val = Convert.ToInt32(sqlcomm.ExecuteScalar());
            sqlcomm.Dispose();
            sqlcon.Dispose();

        }
        catch (Exception ex)
        {

            return 0;
        }
        return val;
    }

    public int MusteriKullaniciUpdateSmsEmailActivationStatus(int KullaniciId, bool SmsStatus, bool EmailStatus)
    {
        int val = 0;
        try
        {

            SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
            SqlCommand sqlcomm = new SqlCommand("GuncelleKullaniciBySmsEmailAktivasyon");
            sqlcomm.CommandType = CommandType.StoredProcedure;
            sqlcomm.Connection = sqlcon;
            sqlcomm.Parameters.AddWithValue("@KullaniciId", KullaniciId);
            sqlcomm.Parameters.AddWithValue("@SmsStatus", SmsStatus);
            sqlcomm.Parameters.AddWithValue("@EmailStatus", EmailStatus);
            sqlcon.Open();
            val = Convert.ToInt32(sqlcomm.ExecuteScalar());
            sqlcomm.Dispose();
            sqlcon.Dispose();

        }
        catch (Exception ex)
        {

            return 0;
        }
        return val;
    }


    public int GuncelleKullaniciErpCariId(int? KullaniciID, int ErpCariId)
    {
        int Sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();

        dbParamColl.Add("@KullaniciID", KullaniciID);
        dbParamColl.Add("@ErpCariId", ErpCariId);

        dbConn.OpenConnection();
        Sonuc = Convert.ToInt32(dbConn.ExecuteScalar("GuncelleKullaniciErpCariId", dbParamColl));
        dbConn.CloseConnecion();

        return Sonuc;
    }

    public int GuncelleKullaniciSifre(int KullaniciID, string Sifre, string YeniSifre)
    {
        int Sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();

        dbParamColl.Add("@KullaniciID", KullaniciID);
        dbParamColl.Add("@Sifre", Sifre);
        dbParamColl.Add("@YeniSifre", YeniSifre);

        dbConn.OpenConnection();
        Sonuc = dbConn.ExecuteNonQuery("GuncelleKullaniciSifre", dbParamColl);
        dbConn.CloseConnecion();

        return Sonuc;
    }

    public int GuncelleKullanici(int KullaniciId, DateTime DogumTarihi, string Cinsiyet, string UlkeKod, string SehirKod, string IlceKod, string TelKod, string TelNo,
    string CepTelKod, string CepTelNo, bool CepHaber, bool EmailHaber, string RefEmail)
    {
        int Sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@KullaniciId", KullaniciId);
        dbParamColl.Add("@DogumTarihi", DogumTarihi);
        dbParamColl.Add("@Cinsiyet", Cinsiyet);
        dbParamColl.Add("@UlkeKod", UlkeKod);
        dbParamColl.Add("@SehirKod", SehirKod);
        dbParamColl.Add("@IlceKod", IlceKod);
        dbParamColl.Add("@TelKod", TelKod);
        dbParamColl.Add("@TelNo", TelNo);
        dbParamColl.Add("@CepTelKod", CepTelKod);
        dbParamColl.Add("@CepTelNo", CepTelNo);
        dbParamColl.Add("@CepHaber", CepHaber);
        dbParamColl.Add("@EmailHaber", EmailHaber);
        dbParamColl.Add("@RefEmail", RefEmail);

        dbConn.OpenConnection();
        Sonuc = dbConn.ExecuteNonQuery("GuncelleKullanici", dbParamColl);
        dbConn.CloseConnecion();

        return Sonuc;
    }

    public DataTable GetirKullanici(int KullaniciId)
    {
        DataTable dtKullanici = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@KullaniciId", KullaniciId);


        dbConn.OpenConnection();
        dtKullanici = dbConn.ExecuteDataTable("GetirKullanici", dbParamColl);
        dbConn.CloseConnecion();

        return dtKullanici;
    }

    public DataTable GetirKullaniciBySiparisId(Int32 SiparisId)
    {
        DataTable dtKullanici = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@SiparisId", SiparisId);


        dbConn.OpenConnection();
        dtKullanici = dbConn.ExecuteDataTable("GetirKullaniciBySiparisId", dbParamColl);
        dbConn.CloseConnecion();

        return dtKullanici;
    }



    public DataTable GetirKullanici(string UyeId)
    {
        DataTable dtKullanici = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@UyeId", UyeId);


        dbConn.OpenConnection();
        dtKullanici = dbConn.ExecuteDataTable("GetirKullaniciForUyeId", dbParamColl);
        dbConn.CloseConnecion();

        return dtKullanici;
    }

    public DataTable GetirKullaniciByEmail(string Email)
    {
        DataTable dtKullanici = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@Email", Email);


        dbConn.OpenConnection();
        dtKullanici = dbConn.ExecuteDataTable("GetirKullaniciByEmail", dbParamColl);
        dbConn.CloseConnecion();

        return dtKullanici;
    }

    public DataTable GetirKullaniciByFacebookId(string FacebookId)
    {
        DataTable dtKullanici = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@FacebookId", FacebookId);


        dbConn.OpenConnection();
        dtKullanici = dbConn.ExecuteDataTable("GetirKullaniciByFacebookId", dbParamColl);
        dbConn.CloseConnecion();

        return dtKullanici;
    }


    public bool GuncelleKullaniciFacebookId(int KullaniciId, string FacebookId)
    {
        int Sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@KullaniciId", KullaniciId);
        dbParamColl.Add("@FacebookId", FacebookId);

        dbConn.OpenConnection();
        Sonuc = dbConn.ExecuteNonQuery("GuncelleKullaniciFacebookId", dbParamColl);
        dbConn.CloseConnecion();

        if (Sonuc > 0)
            return true;
        else
            return false;
    }


    public int GuncelleKullaniciSifreAktif(int kullaniciId, string sifre, bool aktif)
    {
        int Sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();

        dbParamColl.Add("@kullaniciId", kullaniciId);
        dbParamColl.Add("@sifre", sifre);
        dbParamColl.Add("@aktif", aktif);

        dbConn.OpenConnection();
        Sonuc = Convert.ToInt32(dbConn.ExecuteNonQuery("GuncelleKullaniciAktifSifre", dbParamColl));
        dbConn.CloseConnecion();

        return Sonuc;
    }

    public DataTable GetirKullaniciAll()
    {
        DataTable dtKullaniciAll = new DataTable();

        dbConn.OpenConnection();
        dtKullaniciAll = dbConn.ExecuteDataTable("GetirKullaniciHepsi");
        dbConn.CloseConnecion();

        return dtKullaniciAll;
    }

    public DataTable GetirMinMaxCariId()
    {
        DataTable dtMinMaxCariId = new DataTable();

        dbConn.OpenConnection();
        dtMinMaxCariId = dbConn.ExecuteDataTable("GetirMinMaxCariId");
        dbConn.CloseConnecion();

        return dtMinMaxCariId;
    }

    public DataTable GetirCariId()
    {
        DataTable dtCariId = new DataTable();

        dbConn.OpenConnection();
        dtCariId = dbConn.ExecuteDataTable("GetirErpCariId");
        dbConn.CloseConnecion();

        return dtCariId;
    }

    public int MusteriKullaniciKayitAdmin(string musteriAd, string musteriSoyad, string email, string sifre, DateTime dogumTarihi, string cinsiyet, string ulkeKod, string sehirKod, string ilceKod, string telKod, string telNo, string telDahili, string cepTelKod, string cepTelNo, bool cepHaber, bool emailHaber, string UyelikKanali, string neredenDuydu, string ilgiAlanlari, string sonGirisIP, string refemail, string EmailAktivasyonKod, int uyelikTipi)
    {
        int val = 0;
        try
        {

            SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
            SqlCommand sqlcomm = new SqlCommand("KaydetKullaniciAdmin");
            sqlcomm.CommandType = CommandType.StoredProcedure;
            sqlcomm.Connection = sqlcon;
            sqlcomm.Parameters.AddWithValue("@musteriad", musteriAd);
            sqlcomm.Parameters.AddWithValue("@musterisoyad", musteriSoyad);
            sqlcomm.Parameters.AddWithValue("@email", email);
            sqlcomm.Parameters.AddWithValue("@sifre", sifre);
            sqlcomm.Parameters.AddWithValue("@dogumtarihi", dogumTarihi);
            sqlcomm.Parameters.AddWithValue("@cinsiyet", cinsiyet);
            sqlcomm.Parameters.AddWithValue("@ulkekod", ulkeKod);
            sqlcomm.Parameters.AddWithValue("@sehirkod", sehirKod);
            sqlcomm.Parameters.AddWithValue("@ilcekod", ilceKod);
            sqlcomm.Parameters.AddWithValue("@telkod", telKod);
            sqlcomm.Parameters.AddWithValue("@telno", telNo);
            sqlcomm.Parameters.AddWithValue("@teldahili", telDahili);
            sqlcomm.Parameters.AddWithValue("@ceptelkod", cepTelKod);
            sqlcomm.Parameters.AddWithValue("@ceptelno", cepTelNo);
            sqlcomm.Parameters.AddWithValue("@cephaber", cepHaber);
            sqlcomm.Parameters.AddWithValue("@emailhaber", emailHaber);
            sqlcomm.Parameters.AddWithValue("@neredenduydu", neredenDuydu);
            sqlcomm.Parameters.AddWithValue("@ilgialanlari", ilgiAlanlari);
            sqlcomm.Parameters.AddWithValue("@songirisip", sonGirisIP);
            sqlcomm.Parameters.AddWithValue("@refemail", refemail);
            sqlcomm.Parameters.AddWithValue("@emailaktivasyonkod", EmailAktivasyonKod);
            sqlcomm.Parameters.AddWithValue("@portalkod", "GoldB2C");
            sqlcomm.Parameters.AddWithValue("@UyelikKanali", UyelikKanali);
            sqlcomm.Parameters.AddWithValue("@uyelikTipi", uyelikTipi);
            sqlcon.Open();
            val = Convert.ToInt32(sqlcomm.ExecuteScalar());
            sqlcomm.Dispose();
            sqlcon.Dispose();

        }
        catch (Exception ex)
        {

            return 0;
        }
        return val;
    }

    public DataTable UyeListesiRaporu(string ad, string soyad, DateTime uyelikTarih1, DateTime uyelikTarih2, DateTime sonGirisTarih1, DateTime sonGirisTarih2)
    {
        DataTable dtUyeListesi = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@KullaniciAd", ad);
        dbParamColl.Add("@KullaniciSoyad", soyad);
        dbParamColl.Add("@UyelikTarihi1", uyelikTarih1);
        dbParamColl.Add("@UyelikTarihi2", uyelikTarih2);
        dbParamColl.Add("@SonGirisTarihi1", sonGirisTarih1);
        dbParamColl.Add("@SonGirisTarihi2", sonGirisTarih2);


        dbConn.OpenConnection();
        dtUyeListesi = dbConn.ExecuteDataTable("UyeListesiRaporu", dbParamColl);
        dbConn.CloseConnecion();

        return dtUyeListesi;
    }
}