﻿using Dapper;
using Goldb2cDAL;
using Goldb2cEntity;
using Goldb2cEntity.Custom;
using Kendo.DynamicLinq;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Goldb2cDAT.Cus
{

    public class TransactionDAT
    {
        DbConnector dbConn = new DbConnector();

        public List<TransactionView> Get(View filters)
        {

            string sql = @"  select  COUNT(*) OVER () as totalRows,* from (
                             
select _Transaction.*, Kullanici.KullaniciId, Kullanici.KullaniciAd, Kullanici.CRMCustomerGUID from [Transaction] _Transaction
inner join Kullanici on Kullanici.KullaniciId = _Transaction.userId
where Kullanici.UyelikTipi=0

                            ) as t
                              {0} {1}
                              OFFSET {2} ROWS FETCH NEXT {3} ROWS ONLY";
            QueryView queryView = FilterHelper.SqlBuilder(filters, sql);
            var args = new DynamicParameters(new { });
            queryView.filterParameters.ForEach(p => args.Add(p.ParameterName, p.Value));
            using (IDbConnection db = new SqlConnection(dbConn.ConnectionString))
            {
                return (db.Query<TransactionView>(queryView.sql, args)).ToList();
            }
        }


        public PuanView KullaniciPuanGetir(int KullaniciId)
        {
            string sql = @"
	

  declare @kalan int=0;
	declare @yuklenen int=0;
  declare @silinecek int=0;
	select @kalan=coalesce(sum(_Transaction.amount) ,0)  from [Transaction] _Transaction where userId={0}
	select @yuklenen=coalesce(sum(_Transaction.amount) ,0)  from [Transaction] _Transaction where _Transaction.amount>0 and userId={0}
    --select @silinecek=sum(amount) from [Transaction] where userId={0} and year(period)=2017

    declare @ikiTakvimYiliOncesindeKazanilanPuanlar int= ( select coalesce( SUM(amount),0) from [Transaction] where userId={0} and year(period)=2019 and amount>0)
    declare @ikiTakvimYiliIcindeHarcananPuanlar int= (select coalesce( SUM(amount),0) from [Transaction] where userId={0} and year(period) in (2019,2020) and amount<0)

 --set @silinecek = @ikiTakvimYiliOncesindeKazanilanPuanlar - abs(@ikiTakvimYiliIcindeHarcananPuanlar)
 --   if @silinecek <0
 --   begin
 --    set @silinecek=0;
 --   end
 set @silinecek =  dbo.SilinecekPuanGetir({0});




select @kalan as kalan,@yuklenen as yuklenen, @silinecek as silinecek

";

            sql = string.Format(sql, KullaniciId);
            if (KullaniciId == 17487 || KullaniciId == 15384 || KullaniciId == 21030)
            {
                sql = sql.Replace("SilinecekPuanGetir", "SilinecekPuanGetir2020");
            }
            using (IDbConnection db = new SqlConnection(dbConn.ConnectionString))
            {
                return (db.Query<PuanView>(sql)).FirstOrDefault();
            }
        }


        public int Insert(int userId, string title, string description, decimal amount, Goldb2cEntity.Custom.Enums.Transaction.TransactionType transactionType, Goldb2cEntity.Custom.Enums.Generic.Status status, DateTime period)
        {
            Transaction transaction = new Transaction
            {
                userId = userId,
                title = title,
                description = description,
                amount = amount,
                transactionType = transactionType,
                period = period,
                status = status,
                created = DateTime.Now,
                updated = DateTime.Now
            };
            object o = DapperManager.Insert(dbConn.ConnectionString, transaction);
            return Convert.ToInt32(o.ToString());
        }




        public DataTable KulanciPuanDurumlari()
        {
            string sql = @"

 select AcenteKodu,  cast(yuklenen as int) as yuklenen, cast(kalan as int) as kalan, cast(yuklenen-kalan as int) as harcanan from (
select Kullanici.CRMCustomerGUID as AcenteKodu,
(select coalesce(sum(_Transaction.amount) ,0)  from [Transaction] _Transaction where userId=Kullanici.KullaniciId) as kalan,
(select coalesce(sum(_Transaction.amount) ,0)  from [Transaction] _Transaction where _Transaction.amount>0 and userId=Kullanici.KullaniciId) as yuklenen
 from Kullanici where UyelikTipi=0
 ) as t

";

            DataTable dt = SqlHelper.ExecuteDataset(dbConn.ConnectionString, CommandType.Text, sql).Tables[0];

            return dt;
        }


    }
}
