﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Goldb2cDAL;
using System.Data;
using System.Data.SqlClient;


public class SoruCevapIslemleriDAT
{
    DbConnector dbConn = new DbConnector();

    public DataTable SoruGetir()
    {
        DataTable dt = new DataTable();

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("WebAdminSoruGetir");
        dbConn.CloseConnecion();

        return dt;
    }

    public int SoruKaydet(string soru, int soruAcanKullaniciId, int siparisId, int kullaniciId, int soruBaslikId, string dosyaYolu)
    {
        int Sonuc = 0;
        try
        {
            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@soru", soru);
            dbParamColl.Add("@soruAcanKullaniciId", soruAcanKullaniciId);
            dbParamColl.Add("@siparisId", siparisId);
            dbParamColl.Add("@kullaniciId", kullaniciId);
            dbParamColl.Add("@soruBaslikId", soruBaslikId);
            dbParamColl.Add("@dosyaYolu", dosyaYolu);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteScalar("WebAdminSoruKaydet", dbParamColl));
            dbConn.CloseConnecion();

        }
        catch (Exception)
        {
            return 0;
        }
        return Sonuc;
    }

    public DataTable CevapGetir(int soruId)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@soruId", soruId);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("WebAdminCevapGetir", dbParamColl);
        dbConn.CloseConnecion();

        return dt;
    }

    public int CevapKaydet(int soruId, string cevap, int cevapVerenKullaniciId, string dosyaYolu)
    {
        int Sonuc = 0;
        try
        {
            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@soruId", soruId);
            dbParamColl.Add("@cevap", cevap);
            dbParamColl.Add("@cevapVerenKullaniciId", cevapVerenKullaniciId);
            dbParamColl.Add("@dosyaYolu", dosyaYolu);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteScalar("WebAdminCevapKaydet", dbParamColl));
            dbConn.CloseConnecion();

        }
        catch (Exception)
        {
            return 0;
        }
        return Sonuc;
    }

    public int SoruAktifPasif(int soruId, bool aktifpasif, int kullaniciId)
    {
        int Sonuc = 0;
        try
        {
            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@soruId", soruId);
            dbParamColl.Add("@aktifpasif", aktifpasif);
            dbParamColl.Add("@kullaniciId", kullaniciId);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteNonQuery("WebAdminSoruAktifPasif", dbParamColl));
            dbConn.CloseConnecion();

        }
        catch (Exception)
        {
            return 0;
        }
        return Sonuc;
    }

    public DataTable SoruAra(string soruAraText, int soruId, int siparisId, int kullaniciId, int soruBaslikId, int aktifPasif)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@soruAraText", soruAraText);
        dbParamColl.Add("@soruId", soruId);
        dbParamColl.Add("@siparisId", siparisId);
        dbParamColl.Add("@kullaniciId", kullaniciId);
        dbParamColl.Add("@soruBaslikId", soruBaslikId);
        dbParamColl.Add("@aktifPasif", aktifPasif);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("WebAdminSoruAra", dbParamColl);
        dbConn.CloseConnecion();

        return dt;
    }

    public DataTable SoruBaslikGetir()
    {
        DataTable dt = new DataTable();

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("WebAdminSoruBaslikGetir");
        dbConn.CloseConnecion();

        return dt;
    }

    public DataTable SoruAktifPasifGetir()
    {
        DataTable dt = new DataTable();

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("WebAdminSoruAktifPasifGetir");
        dbConn.CloseConnecion();

        return dt;
    }

}

