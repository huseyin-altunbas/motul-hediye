﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Goldb2cDAT
{
    public class MesajDAL
    {
        public MesajDAL()
        {
        }

        public DataTable SelectMesajForWebByMusteriKullanicId(int kullaniciId)
        {
            DataTable dtReturn = new DataTable();
            try
            {

                SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
                SqlCommand sqlcomm = new SqlCommand("SelectMesajForWebByMusteriKullanicId");
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.Connection = sqlcon;
                sqlcomm.Parameters.AddWithValue("@pMusteriKullanicId", kullaniciId);
                SqlDataAdapter sqldadap = new SqlDataAdapter(sqlcomm);
                sqldadap.Fill(dtReturn);
                sqldadap.Dispose();
                sqlcon.Dispose();
                return dtReturn;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable SelectMesajForWebByParentMesajId(int mesajId)
        {
            DataTable dtReturn = new DataTable();
            try
            {

                SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
                SqlCommand sqlcomm = new SqlCommand("SelectMesajForWebByParentMesajId");
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.Connection = sqlcon;
                sqlcomm.Parameters.AddWithValue("@pParentMesajId", mesajId);
                SqlDataAdapter sqldadap = new SqlDataAdapter(sqlcomm);
                sqldadap.Fill(dtReturn);
                sqldadap.Dispose();
                sqlcon.Dispose();
                return dtReturn;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable SelectMemnuniyetDurumForWeb()
        {
            DataTable dtReturn = new DataTable();
            try
            {

                SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
                SqlCommand sqlcomm = new SqlCommand("SelectMemnuniyetDurumForWeb");
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.Connection = sqlcon;
                SqlDataAdapter sqldadap = new SqlDataAdapter(sqlcomm);
                sqldadap.Fill(dtReturn);
                sqldadap.Dispose();
                sqlcon.Dispose();
                return dtReturn;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable SelectMesajByMesajId(int mesajId)
        {
            DataTable dtReturn = new DataTable();
            try
            {

                SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
                SqlCommand sqlcomm = new SqlCommand("SelectMesajByMesajId");
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.Connection = sqlcon;
                sqlcomm.Parameters.AddWithValue("@pMesajId", mesajId);
                SqlDataAdapter sqldadap = new SqlDataAdapter(sqlcomm);
                sqldadap.Fill(dtReturn);
                sqldadap.Dispose();
                sqlcon.Dispose();
                return dtReturn;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateMesajByMesajId(DataRow parDataRow)
        {
            try
            {
                SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
                SqlCommand sqlcomm = new SqlCommand("UpdateMesajByMesajId");
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.Connection = sqlcon;
                if (parDataRow != null)
                {
                    AddMesajParameters(sqlcomm, parDataRow);
                    sqlcomm.Parameters.AddWithValue("@pMesajId", Convert.ToInt32(parDataRow["MesajId"]));
                    sqlcon.Open();
                    object res = sqlcomm.ExecuteScalar();
                    sqlcomm.Dispose();
                    sqlcon.Dispose();
                    return true;
                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public int InsertMesaj(DataRow parDataRow)
        {
            int val = 0;
            try
            {
                SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
                SqlCommand sqlcomm = new SqlCommand("InsertMesaj");
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.Connection = sqlcon;
                if (parDataRow != null)
                {
                    AddMesajParameters(sqlcomm, parDataRow);
                    sqlcomm.Parameters.Add("@pMesajId", SqlDbType.Int);
                    sqlcomm.Parameters["@pMesajId"].Direction = ParameterDirection.Output;
                    sqlcon.Open();
                    sqlcomm.ExecuteScalar();
                    val = Convert.ToInt32(sqlcomm.Parameters["@pMesajId"].Value);
                    sqlcomm.Dispose();
                    sqlcon.Dispose();
                }
            }
            catch (Exception ex)
            {
            }
            return val;
        }

        public int InsertMesajCevapForWeb(DataRow parDataRow)
        {
            int val = 0;
            try
            {
                SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
                SqlCommand sqlcomm = new SqlCommand("InsertMesajCevapForWeb");
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.Connection = sqlcon;
                if (parDataRow != null)
                {
                    AddMesajParameters(sqlcomm, parDataRow);
                    sqlcomm.Parameters.Add("@pMesajId", SqlDbType.Int);
                    sqlcomm.Parameters["@pMesajId"].Direction = ParameterDirection.Output;
                    sqlcon.Open();
                    sqlcomm.ExecuteScalar();
                    val = Convert.ToInt32(sqlcomm.Parameters["@pMesajId"].Value);
                    sqlcomm.Dispose();
                    sqlcon.Dispose();
                }
            }
            catch (Exception ex)
            {
            }
            return val;
        }

        public bool UpdateMesajForWebByParentMesajId(int parentMesajId)
        {
            try
            {
                SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
                SqlCommand sqlcomm = new SqlCommand("UpdateMesajForWebByParentMesajId");
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.Connection = sqlcon;
                sqlcomm.Parameters.AddWithValue("@pParentMesajId", parentMesajId);
                sqlcon.Open();
                object res = sqlcomm.ExecuteScalar();
                sqlcomm.Dispose();
                sqlcon.Dispose();
                return true;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        private void AddMesajParameters(SqlCommand sqlcomm, DataRow parDataRow)
        {
            object _MesajId = 0;
            object _ParentMesajId = DBNull.Value;
            object _FirmaKod = DBNull.Value;
            object _ErpCariId = DBNull.Value;
            object _PortalKod = DBNull.Value;
            object _KaynakUrl = DBNull.Value;
            object _Email = DBNull.Value;
            object _VeriNo = DBNull.Value;
            object _VeriKonum = DBNull.Value;
            object _TedarikciKod = DBNull.Value;
            object _TedarikciAd = DBNull.Value;
            object _SmsId = DBNull.Value;
            object _Konu = DBNull.Value;
            object _Icerik = DBNull.Value;
            object _KullaniciId = DBNull.Value;
            object _MusteriKullaniciId = DBNull.Value;
            object _DepartmanId = DBNull.Value;
            object _AltDepartmanId = DBNull.Value;
            object _GuncKullaniciId = DBNull.Value;
            object _AcilmaTarih = DBNull.Value;
            object _GuncTarih = DBNull.Value;
            object _OkunduTarih = DBNull.Value;
            object _YonlendirmeTarih = DBNull.Value;
            object _OtelemeTarih = DBNull.Value;
            object _KonuSahibi = DBNull.Value;
            object _CevapYazan = DBNull.Value;
            object _YayinlansinMi = DBNull.Value;
            object _MailAtildiMi = DBNull.Value;
            object _SmsAtildiMi = DBNull.Value;
            object _TelBilgiVerildiMi = DBNull.Value;
            object _YanitlayacakKullaniciId = DBNull.Value;
            object _YonlendirenKullaniciId = DBNull.Value;
            object _YonlendirilenKullaniciId = DBNull.Value;
            object _YonlendirenDepartmanId = DBNull.Value;
            object _YonlendirilenDepartmanId = DBNull.Value;
            object _YonlendirenAltDepartmanId = DBNull.Value;
            object _YonlendirilenAltDepartmanId = DBNull.Value;
            object _MesajDurumId = DBNull.Value;
            object _MemnuniyetDurumId = DBNull.Value;
            object _MesajOnemId = DBNull.Value;
            object _TelefonNo = DBNull.Value;
            object _CepTelefonNo = DBNull.Value;

            if (parDataRow["MesajId"] != DBNull.Value)
            {
                _MesajId = Convert.ToInt32(parDataRow["MesajId"]);
            }
            if (parDataRow["ParentMesajId"] != DBNull.Value)
            {
                _ParentMesajId = Convert.ToInt32(parDataRow["ParentMesajId"]);
            }
            if (parDataRow["FirmaKod"] != DBNull.Value)
            {
                _FirmaKod = parDataRow["FirmaKod"].ToString();
            }
            if (parDataRow["ErpCariId"] != DBNull.Value)
            {
                _ErpCariId = Convert.ToInt32(parDataRow["ErpCariId"]);
            }
            if (parDataRow["PortalKod"] != DBNull.Value)
            {
                _PortalKod = parDataRow["PortalKod"].ToString();
            }
            if (parDataRow["KaynakUrl"] != DBNull.Value)
            {
                _KaynakUrl = parDataRow["KaynakUrl"].ToString();
            }
            if (parDataRow["Email"] != DBNull.Value)
            {
                _Email = parDataRow["Email"].ToString();
            }
            if (parDataRow["VeriNo"] != DBNull.Value)
            {
                _VeriNo = parDataRow["VeriNo"].ToString();
            }
            if (parDataRow["VeriKonum"] != DBNull.Value)
            {
                _VeriKonum = parDataRow["VeriKonum"].ToString();
            }
            if (parDataRow["TedarikciKod"] != DBNull.Value)
            {
                _TedarikciKod = parDataRow["TedarikciKod"].ToString();
            }
            if (parDataRow["TedarikciAd"] != DBNull.Value)
            {
                _TedarikciAd = parDataRow["TedarikciAd"].ToString();
            }
            if (parDataRow["SmsId"] != DBNull.Value)
            {
                _SmsId = parDataRow["SmsId"].ToString();
            }
            if (parDataRow["Konu"] != DBNull.Value)
            {
                _Konu = parDataRow["Konu"].ToString();
            }
            if (parDataRow["Icerik"] != DBNull.Value)
            {
                _Icerik = parDataRow["Icerik"].ToString();
            }
            if (parDataRow["KullaniciId"] != DBNull.Value)
            {
                _KullaniciId = Convert.ToInt32(parDataRow["KullaniciId"]);
            }
            if (parDataRow["MusteriKullaniciId"] != DBNull.Value)
            {
                _MusteriKullaniciId = Convert.ToInt32(parDataRow["MusteriKullaniciId"]);
            }
            if (parDataRow["DepartmanId"] != DBNull.Value)
            {
                _DepartmanId = Convert.ToInt32(parDataRow["DepartmanId"]);
            }
            if (parDataRow["AltDepartmanId"] != DBNull.Value)
            {
                _AltDepartmanId = Convert.ToInt32(parDataRow["AltDepartmanId"]);
            }
            if (parDataRow["GuncKullaniciId"] != DBNull.Value)
            {
                _GuncKullaniciId = Convert.ToInt32(parDataRow["GuncKullaniciId"]);
            }
            if (parDataRow["AcilmaTarih"] != DBNull.Value)
            {
                _AcilmaTarih = Convert.ToDateTime(parDataRow["AcilmaTarih"]);
            }
            if (parDataRow["GuncTarih"] != DBNull.Value)
            {
                _GuncTarih = Convert.ToDateTime(parDataRow["GuncTarih"]);
            }
            if (parDataRow["OkunduTarih"] != DBNull.Value)
            {
                _OkunduTarih = Convert.ToDateTime(parDataRow["OkunduTarih"]);
            }
            if (parDataRow["YonlendirmeTarih"] != DBNull.Value)
            {
                _YonlendirmeTarih = Convert.ToDateTime(parDataRow["YonlendirmeTarih"]);
            }
            if (parDataRow["OtelemeTarih"] != DBNull.Value)
            {
                _OtelemeTarih = Convert.ToDateTime(parDataRow["OtelemeTarih"]);
            }
            if (parDataRow["KonuSahibi"] != DBNull.Value)
            {
                _KonuSahibi = parDataRow["KonuSahibi"].ToString();
            }
            if (parDataRow["CevapYazan"] != DBNull.Value)
            {
                _CevapYazan = parDataRow["CevapYazan"].ToString();
            }
            if (parDataRow["YayinlansinMi"] != DBNull.Value)
            {
                _YayinlansinMi = Convert.ToBoolean(parDataRow["YayinlansinMi"]);
            }
            if (parDataRow["MailAtildiMi"] != DBNull.Value)
            {
                _MailAtildiMi = Convert.ToBoolean(parDataRow["MailAtildiMi"]);
            }
            if (parDataRow["SmsAtildiMi"] != DBNull.Value)
            {
                _SmsAtildiMi = Convert.ToBoolean(parDataRow["SmsAtildiMi"]);
            }
            if (parDataRow["TelBilgiVerildiMi"] != DBNull.Value)
            {
                _TelBilgiVerildiMi = Convert.ToBoolean(parDataRow["TelBilgiVerildiMi"]);
            }
            if (parDataRow["YanitlayacakKullaniciId"] != DBNull.Value)
            {
                _YanitlayacakKullaniciId = Convert.ToInt32(parDataRow["YanitlayacakKullaniciId"]);
            }
            if (parDataRow["YonlendirenKullaniciId"] != DBNull.Value)
            {
                _YonlendirenKullaniciId = Convert.ToInt32(parDataRow["YonlendirenKullaniciId"]);
            }
            if (parDataRow["YonlendirilenKullaniciId"] != DBNull.Value)
            {
                _YonlendirilenKullaniciId = Convert.ToInt32(parDataRow["YonlendirilenKullaniciId"]);
            }
            if (parDataRow["YonlendirenDepartmanId"] != DBNull.Value)
            {
                _YonlendirenDepartmanId = Convert.ToInt32(parDataRow["YonlendirenDepartmanId"]);
            }
            if (parDataRow["YonlendirilenDepartmanId"] != DBNull.Value)
            {
                _YonlendirilenDepartmanId = Convert.ToInt32(parDataRow["YonlendirilenDepartmanId"]);
            }
            if (parDataRow["YonlendirenAltDepartmanId"] != DBNull.Value)
            {
                _YonlendirenAltDepartmanId = Convert.ToInt32(parDataRow["YonlendirenAltDepartmanId"]);
            }
            if (parDataRow["YonlendirilenAltDepartmanId"] != DBNull.Value)
            {
                _YonlendirilenAltDepartmanId = Convert.ToInt32(parDataRow["YonlendirilenAltDepartmanId"]);
            }
            if (parDataRow["MesajDurumId"] != DBNull.Value)
            {
                _MesajDurumId = Convert.ToInt32(parDataRow["MesajDurumId"]);
            }
            if (parDataRow["MemnuniyetDurumId"] != DBNull.Value)
            {
                _MemnuniyetDurumId = Convert.ToInt32(parDataRow["MemnuniyetDurumId"]);
            }
            if (parDataRow["MesajOnemId"] != DBNull.Value)
            {
                _MesajOnemId = Convert.ToInt32(parDataRow["MesajOnemId"]);
            }
            if (parDataRow["TelefonNo"] != DBNull.Value)
            {
                _TelefonNo = parDataRow["TelefonNo"].ToString();
            }
            if (parDataRow["CepTelefonNo"] != DBNull.Value)
            {
                _CepTelefonNo = parDataRow["CepTelefonNo"].ToString();
            }

            //sqlcomm.Parameters.AddWithValue("_MesajId",_MesajId);
            sqlcomm.Parameters.Add("@pParentMesajId", _ParentMesajId);
            sqlcomm.Parameters.Add("@pFirmaKod", _FirmaKod);
            sqlcomm.Parameters.Add("@pErpCariId", _ErpCariId);
            sqlcomm.Parameters.Add("@pPortalKod", _PortalKod);
            sqlcomm.Parameters.Add("@pKaynakUrl", _KaynakUrl);
            sqlcomm.Parameters.Add("@pEmail", _Email);
            sqlcomm.Parameters.Add("@pVeriNo", _VeriNo);
            sqlcomm.Parameters.Add("@pVeriKonum", _VeriKonum);
            sqlcomm.Parameters.Add("@pTedarikciKod", _TedarikciKod);
            sqlcomm.Parameters.Add("@pTedarikciAd", _TedarikciAd);
            sqlcomm.Parameters.Add("@pSmsId", _SmsId);
            sqlcomm.Parameters.Add("@pKonu", _Konu);
            sqlcomm.Parameters.Add("@pIcerik", _Icerik);
            sqlcomm.Parameters.Add("@pKullaniciId", _KullaniciId);
            sqlcomm.Parameters.Add("@pMusteriKullaniciId", _MusteriKullaniciId);
            sqlcomm.Parameters.Add("@pDepartmanId", _DepartmanId);
            sqlcomm.Parameters.Add("@pAltDepartmanId", _AltDepartmanId);
            sqlcomm.Parameters.Add("@pGuncKullaniciId", _GuncKullaniciId);
            sqlcomm.Parameters.Add("@pAcilmaTarih", _AcilmaTarih);
            sqlcomm.Parameters.Add("@pGuncTarih", _GuncTarih);
            sqlcomm.Parameters.Add("@pOkunduTarih", _OkunduTarih);
            sqlcomm.Parameters.Add("@pYonlendirmeTarih", _YonlendirmeTarih);
            sqlcomm.Parameters.Add("@pOtelemeTarih", _OtelemeTarih);
            sqlcomm.Parameters.Add("@pKonuSahibi", _KonuSahibi);
            sqlcomm.Parameters.Add("@pCevapYazan", _CevapYazan);
            sqlcomm.Parameters.Add("@pYayinlansinMi", _YayinlansinMi);
            sqlcomm.Parameters.Add("@pMailAtildiMi", _MailAtildiMi);
            sqlcomm.Parameters.Add("@pSmsAtildiMi", _SmsAtildiMi);
            sqlcomm.Parameters.Add("@pTelBilgiVerildiMi", _TelBilgiVerildiMi);
            sqlcomm.Parameters.Add("@pYanitlayacakKullaniciId", _YanitlayacakKullaniciId);
            sqlcomm.Parameters.Add("@pYonlendirenKullaniciId", _YonlendirenKullaniciId);
            sqlcomm.Parameters.Add("@pYonlendirilenKullaniciId", _YonlendirilenKullaniciId);
            sqlcomm.Parameters.Add("@pYonlendirenDepartmanId", _YonlendirenDepartmanId);
            sqlcomm.Parameters.Add("@pYonlendirilenDepartmanId", _YonlendirilenDepartmanId);
            sqlcomm.Parameters.Add("@pYonlendirenAltDepartmanId", _YonlendirenAltDepartmanId);
            sqlcomm.Parameters.Add("@pYonlendirilenAltDepartmanId", _YonlendirilenAltDepartmanId);
            sqlcomm.Parameters.Add("@pMesajDurumId", _MesajDurumId);
            sqlcomm.Parameters.Add("@pMemnuniyetDurumId", _MemnuniyetDurumId);
            sqlcomm.Parameters.Add("@pMesajOnemId", _MesajOnemId);
            sqlcomm.Parameters.Add("@pTelefonNo", _TelefonNo);
            sqlcomm.Parameters.Add("@pCepTelefonNo", _CepTelefonNo);
        }
    }


    public class DepartmanDAL
    {
        public DepartmanDAL()
        {
        }

        public DataTable SelectDepartman()
        {
            DataTable dtReturn = new DataTable();
            try
            {

                SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
                SqlCommand sqlcomm = new SqlCommand("SelectDepartman");
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.Connection = sqlcon;
                SqlDataAdapter sqldadap = new SqlDataAdapter(sqlcomm);
                sqldadap.Fill(dtReturn);
                sqldadap.Dispose();
                sqlcon.Dispose();
                return dtReturn;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable SelectAltDepartmanByDepartmanId(int departmanId)
        {
            DataTable dtReturn = new DataTable();
            try
            {

                SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
                SqlCommand sqlcomm = new SqlCommand("SelectAltDepartmanByDepartmanId");
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.Connection = sqlcon;
                sqlcomm.Parameters.AddWithValue("@pDepartmanId", departmanId);
                SqlDataAdapter sqldadap = new SqlDataAdapter(sqlcomm);
                sqldadap.Fill(dtReturn);
                sqldadap.Dispose();
                sqlcon.Dispose();
                return dtReturn;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    public class MesajKullaniciDAL
    {
        public MesajKullaniciDAL()
        {

        }

        public DataTable SelectMesajKullaniciByAltDepartmanId(int AltDepartmanId)
        {
            DataTable dtReturn = new DataTable();
            try
            {

                SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
                SqlCommand sqlcomm = new SqlCommand("SelectMesajKullaniciByAltDepartmanId");
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.Connection = sqlcon;
                sqlcomm.Parameters.AddWithValue("@AltDepartmanId", AltDepartmanId);
                SqlDataAdapter sqldadap = new SqlDataAdapter(sqlcomm);
                sqldadap.Fill(dtReturn);
                sqldadap.Dispose();
                sqlcon.Dispose();
                return dtReturn;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
