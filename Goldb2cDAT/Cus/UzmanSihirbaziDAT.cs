﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cDAL;

namespace Goldb2cDAT
{
    public class UzmanSihirbaziDAT
    {
        DbConnector dbConn = new DbConnector();

        public DataTable GetirUzmanGorusuSablon()
        {
            DataTable dtSablon = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbConn.OpenConnection();
            dtSablon = dbConn.ExecuteDataTable("GetirUzmanGorusuSablon", dbParamColl);
            dbConn.CloseConnecion();

            return dtSablon;
        }

        public DataTable GetirUzmanGorusuSablonById(int SablonId)
        {
            DataTable dtSablon = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@SablonId", SablonId);

            dbConn.OpenConnection();
            dtSablon = dbConn.ExecuteDataTable("GetirUzmanGorusuSablonById", dbParamColl);
            dbConn.CloseConnecion();

            return dtSablon;
        }
    }
}
