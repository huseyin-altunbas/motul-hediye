﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Goldb2cDAL;
using System.Data;
using System.Data.SqlClient;

public class AnketIslemleriDAT
{
    DbConnector dbConn = new DbConnector();

    public AnketIslemleriDAT()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public DataTable AnketGetir()
    {
        DataTable dtAnket = new DataTable();

        try
        {
            dbConn.OpenConnection();
            dtAnket = dbConn.ExecuteDataTable("AnketGetir");
            dbConn.CloseConnecion();
        }
        catch (Exception)
        {
            dtAnket = new DataTable();
        }

        return dtAnket;
    }

    public DataTable AnketSecenekleriGetir()
    {
        DataTable dtAnketSecenekleri = new DataTable();

        try
        {
            dbConn.OpenConnection();
            dtAnketSecenekleri = dbConn.ExecuteDataTable("AnketSecenekleriGetir");
            dbConn.CloseConnecion();
        }
        catch (Exception)
        {
            dtAnketSecenekleri = new DataTable();
        }

        return dtAnketSecenekleri;
    }

    public int AnketOylariKaydet(int anketId, int secenekId, int kullaniciId, string ipNo)
    {
        int val = 0;
        try
        {

            SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
            SqlCommand sqlcomm = new SqlCommand("AnketOylariKaydet");
            sqlcomm.CommandType = CommandType.StoredProcedure;
            sqlcomm.Connection = sqlcon;
            sqlcomm.Parameters.AddWithValue("@anketId", anketId);
            sqlcomm.Parameters.AddWithValue("@secenekId", secenekId);
            sqlcomm.Parameters.AddWithValue("@kullaniciId", kullaniciId);
            sqlcomm.Parameters.AddWithValue("@ipNo", ipNo);
            sqlcon.Open();
            val = Convert.ToInt32(sqlcomm.ExecuteScalar());
            sqlcomm.Dispose();
            sqlcon.Dispose();

        }
        catch (Exception ex)
        {
            return 0;
        }
        return val;
    }

    public DataTable AnketOylariGetir()
    {
        DataTable dtAnketOylari = new DataTable();

        try
        {
            dbConn.OpenConnection();
            dtAnketOylari = dbConn.ExecuteDataTable("AnketOylariGetir");
            dbConn.CloseConnecion();
        }
        catch (Exception)
        {
            dtAnketOylari = new DataTable();
        }

        return dtAnketOylari;
    }

    public int AnketTextKaydet(int anketId, int secenekId, int kullaniciId, string text2)
    {
        int val = 0;
        try
        {

            SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
            SqlCommand sqlcomm = new SqlCommand("AnketTextKaydet");
            sqlcomm.CommandType = CommandType.StoredProcedure;
            sqlcomm.Connection = sqlcon;
            sqlcomm.Parameters.AddWithValue("@anketId", anketId);
            sqlcomm.Parameters.AddWithValue("@secenekId", secenekId);
            sqlcomm.Parameters.AddWithValue("@kullaniciId", kullaniciId);
            sqlcomm.Parameters.AddWithValue("@text2", text2);
            sqlcon.Open();
            val = Convert.ToInt32(sqlcomm.ExecuteScalar());
            sqlcomm.Dispose();
            sqlcon.Dispose();

        }
        catch (Exception ex)
        {
            return 0;
        }
        return val;
    }

    public DataTable AnketTextGetir()
    {
        DataTable dtAnketText = new DataTable();

        try
        {
            dbConn.OpenConnection();
            dtAnketText = dbConn.ExecuteDataTable("AnketTextGetir");
            dbConn.CloseConnecion();
        }
        catch (Exception)
        {
            dtAnketText = new DataTable();
        }

        return dtAnketText;
    }

    public DataTable AnketDetayGetir(int kullaniciId, int anketId, int aciklama, string oyVermeTarih1, string oyVermeTarih2)
    {

        DataTable dtAnketDetay = new DataTable();
        try
        {
            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@kullaniciId", kullaniciId);
            dbParamColl.Add("@anketId", anketId);
            dbParamColl.Add("@aciklama", aciklama);
            dbParamColl.Add("@oyVermeTarih1", oyVermeTarih1);
            dbParamColl.Add("@oyVermeTarih2", oyVermeTarih2);

            dbConn.OpenConnection();
            dtAnketDetay = dbConn.ExecuteDataTable("AnketDetayGetir", dbParamColl);
            dbConn.CloseConnecion();

            return dtAnketDetay;
        }
        catch (Exception)
        {
            dtAnketDetay = new DataTable();
        }

        return dtAnketDetay;
    }
}

