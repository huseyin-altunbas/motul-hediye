﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Goldb2cDAL;
using System.Data;
using System.Data.SqlClient;


public class AkaryakitIslemleriDAT
{
    DbConnector dbConn = new DbConnector();

    public AkaryakitIslemleriDAT()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable CariAkaryakitKartListele(int erpCariId, int kartId)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@erpCariId", erpCariId);
        dbParamColl.Add("@kartId", kartId);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("AkaryakitKartGetir", dbParamColl);
        dbConn.CloseConnecion();

        return dt;
    }

    public bool CariAkaryakitKartSil(int erpCariId, int kartId)
    {
        int Sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@erpCariId", erpCariId);
        dbParamColl.Add("@kartId", kartId);

        dbConn.OpenConnection();
        Sonuc = dbConn.ExecuteNonQuery("AkaryakitKartSil", dbParamColl);
        dbConn.CloseConnecion();

        if (Sonuc > 0)
            return true;
        else
            return false;
    }

    public int CariAkaryakitKartKaydet(int erpCariId, int kartId, string plaka, decimal tutar)
    {
        int Sonuc = 0;
        try
        {
            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@erpCariId", erpCariId);
            dbParamColl.Add("@kartId", kartId);
            dbParamColl.Add("@tutar", tutar);
            dbParamColl.Add("@plaka", plaka);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteScalar("AkaryakitKartKaydet", dbParamColl));
            dbConn.CloseConnecion();
        }
        catch (Exception)
        {
            return 0;
        }

        return Sonuc;
    }

    public int CariAkaryakitKartSiparisKaydet(int erpCariId, string kartIds, int siparisNo)
    {
        int Sonuc = 0;
        try
        {
            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@erpCariId", erpCariId);
            dbParamColl.Add("@kartIds", kartIds);
            dbParamColl.Add("@siparisNo", siparisNo);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteScalar("AkaryakitKartSiparisKaydet", dbParamColl));
            dbConn.CloseConnecion();
        }
        catch (Exception ex)
        {
            return 0;
        }

        return Sonuc;
    }

    public DataTable AkaryakitKartUrunListele()
    {
        DataTable dt = new DataTable();

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("AkaryakitKartUrunGetir");
        dbConn.CloseConnecion();

        return dt;
    }

    public DataTable AkaryakitKartSiparisListele(int erpCariId, int siparisNo, string kartNo, int kartYukleme, int gonderimDurumu, string ilkTarih, string sonTarih)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@erpCariId", erpCariId);
        dbParamColl.Add("@siparisNo", siparisNo);
        dbParamColl.Add("@kartNo", kartNo);
        dbParamColl.Add("@kartYukleme", kartYukleme);
        dbParamColl.Add("@gonderimDurumu", gonderimDurumu);
        dbParamColl.Add("@ilkTarih", ilkTarih);
        dbParamColl.Add("@sonTarih", sonTarih);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("AkaryakitKartSiparisGetir", dbParamColl);
        dbConn.CloseConnecion();

        return dt;
    }

    public bool AkaryakitKartSiparisGuncelle(int id, int erpCariId, int siparisNo, int kartId, string kartNo, string belgeNo)
    {
        int Sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@id", id);
        dbParamColl.Add("@erpCariId", erpCariId);
        dbParamColl.Add("@siparisNo", siparisNo);
        dbParamColl.Add("@kartId", kartId);
        dbParamColl.Add("@kartNo", kartNo);
        dbParamColl.Add("@belgeNo", belgeNo);

        dbConn.OpenConnection();
        Sonuc = dbConn.ExecuteNonQuery("AkaryakitKartSiparisGuncelle", dbParamColl);
        dbConn.CloseConnecion();

        if (Sonuc > 0)
            return true;
        else
            return false;
    }

    public bool AkaryakitKartKartKontrol(string plaka, string kartNo)
    {
        DataTable Sonuc;

        DbParamCollection dbParamColl = new DbParamCollection();

        dbParamColl.Add("@plaka", plaka);
        dbParamColl.Add("@kartNo", kartNo);

        dbConn.OpenConnection();
        Sonuc = dbConn.ExecuteDataTable("AkaryakitKartKontrol", dbParamColl);
        dbConn.CloseConnecion();

        if (Sonuc.Rows.Count > 0)
            return true;
        else
            return false;
    }

    public bool AkaryakitKartSiparisYuklemeDurumu(int akaryakitKartSiparisId, bool kartYuklemeDurumu)
    {
        int Sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@akaryakitKartSiparisId", akaryakitKartSiparisId);
        dbParamColl.Add("@kartYuklemeDurumu", kartYuklemeDurumu);

        dbConn.OpenConnection();
        Sonuc = dbConn.ExecuteNonQuery("AkaryakitKartSiparisYuklemeDurumu", dbParamColl);
        dbConn.CloseConnecion();

        if (Sonuc > 0)
            return true;
        else
            return false;
    }

    public DataTable AkaryakitKartSiparisListele(int id)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@id", id);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("AkaryakitKartSiparisGetirbyId", dbParamColl);
        dbConn.CloseConnecion();

        return dt;
    }

    public DataTable AkaryakitKartSiparisGetirbySiparisNo(int SiparisNo)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@SiparisNo", SiparisNo);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("AkaryakitKartSiparisGetirbySiparisNo", dbParamColl);
        dbConn.CloseConnecion();

        return dt;
    }


}

