﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Goldb2cDAL;
using System.Data;
using System.Data.SqlClient;

public class KategoriIslemleriDAT
{
    DbConnector dbConn = new DbConnector();

    public KategoriIslemleriDAT()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetirKategoriItems()
    {
        DataTable dtMenuItems = new DataTable();

        dbConn.OpenConnection();
        dtMenuItems = dbConn.ExecuteDataTable("GetirKategoriItems");
        dbConn.CloseConnecion();

        return dtMenuItems;
    }

    public DataTable GetirKategoriItems(string itemId)
    {
        DataTable dtMenuItems = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@ItemId", itemId);

        dbConn.OpenConnection();
        dtMenuItems = dbConn.ExecuteDataTable("GetirKategoriItemsByItemId", dbParamColl);
        dbConn.CloseConnecion();

        return dtMenuItems;
    }

    public int GuncelleItemNameItemUrl(string itemId, string itemName, string itemUrl, int itemOrder, string itemImage)
    {
        int sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();

        dbParamColl.Add("@ItemId", itemId);
        dbParamColl.Add("@ItemName", itemName);
        dbParamColl.Add("@ItemUrl", itemUrl);
        dbParamColl.Add("@ItemOrder", itemOrder);
        dbParamColl.Add("@ItemImage", itemImage);

        dbConn.OpenConnection();
        sonuc = dbConn.ExecuteNonQuery("GuncelleKategoriItemNameItemUrl", dbParamColl);
        dbConn.CloseConnecion();

        return sonuc;
    }

    public int GuncelleItemHtml(string itemId, string itemHtml)
    {
        int sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();

        dbParamColl.Add("@ItemId", itemId);
        dbParamColl.Add("@ItemHtml", itemHtml);

        dbConn.OpenConnection();
        sonuc = dbConn.ExecuteNonQuery("GuncelleKategoriItemHtml", dbParamColl);
        dbConn.CloseConnecion();

        return sonuc;
    }

    public DataTable GetirKategoriMenuItem(int menuItemId)
    {
        DataTable dtMenuItems = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@menuItemId", menuItemId);

        dbConn.OpenConnection();
        dtMenuItems = dbConn.ExecuteDataTable("GetirKategoriItemsByMenuItemId", dbParamColl);
        dbConn.CloseConnecion();

        return dtMenuItems;
    }
}

