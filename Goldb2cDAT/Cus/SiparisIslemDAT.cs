﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Goldb2cDAL;
using System.Data;
using System.Data.SqlClient;

public class SiparisIslemDAT
{
    DbConnector dbConn = new DbConnector();

    public SiparisIslemDAT()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int SiparisIslemKaydet(int siparisId, int urunId, DateTime siparisIslemTarihi)
    {
        int val = 0;
        try
        {

            SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
            SqlCommand sqlcomm = new SqlCommand("SiparisIslemKaydet");
            sqlcomm.CommandType = CommandType.StoredProcedure;
            sqlcomm.Connection = sqlcon;
            sqlcomm.Parameters.AddWithValue("@siparisId", siparisId);
            sqlcomm.Parameters.AddWithValue("@urunId", urunId);
            sqlcomm.Parameters.AddWithValue("@siparisIslemTarihi", siparisIslemTarihi);
            sqlcon.Open();
            val = Convert.ToInt32(sqlcomm.ExecuteScalar());
            sqlcomm.Dispose();
            sqlcon.Dispose();

        }
        catch (Exception ex)
        {
            return 0;
        }
        return val;
    }

    public DataTable SiparisIslemGetir()
    {
        DataTable dtSiparisIslem = new DataTable();

        try
        {
            dbConn.OpenConnection();
            dtSiparisIslem = dbConn.ExecuteDataTable("SiparisIslemGetir");
            dbConn.CloseConnecion();
        }
        catch (Exception)
        {
            dtSiparisIslem = new DataTable();
        }

        return dtSiparisIslem;
    }
}

