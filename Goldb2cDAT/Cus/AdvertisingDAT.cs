﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cDAL;
using System.Data;

namespace Goldb2cDAT
{
    public class AdvertisingDAT
    {
        DbConnector dbConn = new DbConnector();

        public DataTable GetirAdvertisingDetay(int AdvertisingMasterId, bool Aktif)
        {
            DataTable dtWebGorselDetay = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@AdvertisingMasterId", AdvertisingMasterId);
            dbParamColl.Add("@Aktif", Aktif);
            dbConn.OpenConnection();
            dtWebGorselDetay = dbConn.ExecuteDataTable("GetirAdvertisingDetayByMasterIdAktif", dbParamColl);
            dbConn.CloseConnecion();

            return dtWebGorselDetay;
        }

        public void KaydetAdvertisingLog(int AdvertisingMasterId, string IpAdres)
        {
            DataTable dtWebGorselDetay = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@AdvertisingMasterId", AdvertisingMasterId);
            dbParamColl.Add("@IpAdres", IpAdres);
            dbConn.OpenConnection();
            dtWebGorselDetay = dbConn.ExecuteDataTable("KaydetAdvertisingLog", dbParamColl);
            dbConn.CloseConnecion();
        }

        public void KaydetAdvertisingDetayLog(int AdvertisingDetayId, string IpAdres)
        {
            DataTable dtWebGorselDetay = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@AdvertisingDetayId", AdvertisingDetayId);
            dbParamColl.Add("@IpAdres", IpAdres);
            dbConn.OpenConnection();
            dtWebGorselDetay = dbConn.ExecuteDataTable("KaydetAdvertisingDetayLog", dbParamColl);
            dbConn.CloseConnecion();
        }
    }
}
