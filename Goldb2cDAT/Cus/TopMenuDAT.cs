﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Caching;
using System.Data.SqlClient;
using Goldb2cDAL;
using Goldb2cInfrastructure;

/// <summary>
/// Summary description for TopMenuDAT
/// </summary>
public class TopMenuDAT
{
    public TopMenuDAT()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    static Cache cache;
    DbConnector dbConn = new DbConnector();

    public DataTable SelectTopMenu()
    {

        //String CacheKey = CustomConfiguration.getCacheKey;
        //String CacheValue = CustomConfiguration.getCacheValue;
        //int CacheTime = CustomConfiguration.getCacheTimeMinutes;
        //cache = HttpContext.Current.Cache;


        //DataTable dtTopMenu = null;
        //dtTopMenu = (DataTable)cache["TopMenu"];

        try
        {
            //if (dtTopMenu == null || HttpContext.Current.Request.QueryString[CacheKey] == CacheValue)

            ////  lock (obj)
            //{
            DataTable _dtTopMenu = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbConn.OpenConnection();
            _dtTopMenu = dbConn.ExecuteDataTable("GetirMenuItem", dbParamColl);
            dbConn.CloseConnecion();

            //cache.Insert("TopMenu", _dtTopMenu, null, DateTime.MaxValue, TimeSpan.FromMinutes(720));
            //dtTopMenu = (DataTable)cache["TopMenu"];

            //}

            return _dtTopMenu;
        }
        catch
        {
            throw;
        }
    }
}