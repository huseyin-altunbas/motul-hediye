﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cDAL;

namespace Goldb2cDAT
{
    public class AdminGenelDAT
    {
        DbConnector dbConn = new DbConnector();

        public DataTable GunlukUyeRapor(DateTime baslangic, DateTime bitis)
        {
            DataTable dtUyeRapor = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@BaslangicTarih", baslangic);
            dbParamColl.Add("@BitisTarih", bitis);
            dbConn.OpenConnection();
            dtUyeRapor = dbConn.ExecuteDataTable("GetirGunlukUyeRapor", dbParamColl);
            dbConn.CloseConnecion();

            return dtUyeRapor;
        }

        public DataTable GetirAdvertisingMaster()
        {
            DataTable dtWebGorselDetay = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbConn.OpenConnection();
            dtWebGorselDetay = dbConn.ExecuteDataTable("GetirAdvertisingMaster", dbParamColl);
            dbConn.CloseConnecion();

            return dtWebGorselDetay;
        }

        public DataTable GetirAdvertisingDetay(int AdvertisingMasterId)
        {
            DataTable dtWebGorselMaster = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@AdvertisingMasterId", AdvertisingMasterId);
            dbConn.OpenConnection();
            dtWebGorselMaster = dbConn.ExecuteDataTable("GetirAdvertisingDetay", dbParamColl);
            dbConn.CloseConnecion();

            return dtWebGorselMaster;
        }

        public DataTable GetirAdvertisingDetayByDetayId(int AdvertisingDetayId)
        {
            DataTable dtWebGorselMaster = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@AdvertisingDetayId", AdvertisingDetayId);
            dbConn.OpenConnection();
            dtWebGorselMaster = dbConn.ExecuteDataTable("GetirAdvertisingDetayByDetayId", dbParamColl);
            dbConn.CloseConnecion();

            return dtWebGorselMaster;
        }

        public bool KaydetAdvertisingDetay(int AdvertisingMasterId, string GorselUrl, string HedefUrl, string UrunAd, string UrunResim, string UrunLink, decimal UrunFiyat, decimal UrunIndFiyat)
        {
            int Sonuc = 0;
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@AdvertisingMasterId", AdvertisingMasterId);
            dbParamColl.Add("@GorselUrl", GorselUrl);
            dbParamColl.Add("@HedefUrl", HedefUrl);
            dbParamColl.Add("@UrunAd", UrunAd);
            dbParamColl.Add("@UrunResim", UrunResim);
            dbParamColl.Add("@UrunLink", UrunLink);
            dbParamColl.Add("@UrunFiyat", UrunFiyat);
            dbParamColl.Add("@UrunIndFiyat", UrunIndFiyat);

            dbConn.OpenConnection();
            Sonuc = dbConn.ExecuteNonQuery("KaydetAdvertisingDetay", dbParamColl);
            dbConn.CloseConnecion();

            if (Sonuc > 0)
                return true;
            else
                return false;

        }

        public bool GuncelleAdvertisingDetay(int AdvertisingDetayId, string GorselUrl, string HedefUrl, string UrunAd, string UrunResim, string UrunLink, decimal UrunFiyat, decimal UrunIndFiyat, bool Aktif)
        {
            int Sonuc = 0;
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@AdvertisingDetayId", AdvertisingDetayId);
            dbParamColl.Add("@GorselUrl", GorselUrl);
            dbParamColl.Add("@HedefUrl", HedefUrl);
            dbParamColl.Add("@UrunAd", UrunAd);
            dbParamColl.Add("@UrunResim", UrunResim);
            dbParamColl.Add("@UrunLink", UrunLink);
            dbParamColl.Add("@UrunFiyat", UrunFiyat);
            dbParamColl.Add("@UrunIndFiyat", UrunIndFiyat);
            dbParamColl.Add("@Aktif", Aktif);

            dbConn.OpenConnection();
            Sonuc = dbConn.ExecuteNonQuery("GuncelleAdvertisingDetay", dbParamColl);
            dbConn.CloseConnecion();

            if (Sonuc > 0)
                return true;
            else
                return false;

        }

        public DataTable GetirAdvertisingLogRapor(int AdvertisingMasterId)
        {
            DataTable dtAdvertisingLogRapor = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@AdvertisingMasterId", AdvertisingMasterId);
            dbConn.OpenConnection();
            dtAdvertisingLogRapor = dbConn.ExecuteDataTable("GetirAdvertisingLogRapor", dbParamColl);
            dbConn.CloseConnecion();

            return dtAdvertisingLogRapor;
        }

        public DataTable GetirAdvertisingDetayLogRapor(int AdvertisingMasterId)
        {
            DataTable dtAdvertisingDetayLog = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@AdvertisingMasterId", AdvertisingMasterId);
            dbConn.OpenConnection();
            dtAdvertisingDetayLog = dbConn.ExecuteDataTable("GetirAdvertisingDetayLogRapor", dbParamColl);
            dbConn.CloseConnecion();

            return dtAdvertisingDetayLog;
        }

        public void KaydetUzmanSihirbaziLog(int KullaniciId, int UrunId, bool IslemSonuc, string HataMesaj)
        {
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@KullaniciId", KullaniciId);
            dbParamColl.Add("@UrunId", UrunId);
            dbParamColl.Add("@IslemSonuc", IslemSonuc);
            dbParamColl.Add("@HataMesaj", HataMesaj);

            dbConn.OpenConnection();
            dbConn.ExecuteNonQuery("KaydetUzmanSihirbaziLog", dbParamColl);
            dbConn.CloseConnecion();

        }

        public DataTable GetirUzmanSihirbaziRaporByTarih(DateTime BaslangicTarih, DateTime BitisTarih)
        {
            DataTable dtUzmanSihirbazi = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@BaslangicTarih", BaslangicTarih);
            dbParamColl.Add("@BitisTarih", BitisTarih);
            dbConn.OpenConnection();
            dtUzmanSihirbazi = dbConn.ExecuteDataTable("GetirUzmanSihirbaziRaporByTarih", dbParamColl);
            dbConn.CloseConnecion();

            return dtUzmanSihirbazi;

        }

        public DataTable GetirBankaKampanya()
        {
            DataTable dtBankaKampanya = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbConn.OpenConnection();
            dtBankaKampanya = dbConn.ExecuteDataTable("GetirBankaKampanya", dbParamColl);
            dbConn.CloseConnecion();

            return dtBankaKampanya;
        }


        public void GuncelleBankaKampanya(string BankaKod, string KampanyaBaslik, string KampanyaIcerik, int KampanyaTaksitSayi, string KampanyaSolGorsel)
        {
            DataTable dtBankaKampanya = new DataTable();
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@BankaKod", BankaKod);
            dbParamColl.Add("@KampanyaBaslik", KampanyaBaslik);
            dbParamColl.Add("@KampanyaIcerik", KampanyaIcerik);
            dbParamColl.Add("@KampanyaTaksitSayi", KampanyaTaksitSayi);
            dbParamColl.Add("@KampanyaSolGorsel", KampanyaSolGorsel);
            dbConn.OpenConnection();
            dbConn.ExecuteNonQuery("GuncelleBankaKampanya", dbParamColl);
            dbConn.CloseConnecion();
        }
    }
}
