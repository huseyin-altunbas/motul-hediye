﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Goldb2cDAL;
using System.Data;
using System.Data.SqlClient;

public class HediyeIslemDAT
{
    DbConnector dbConn = new DbConnector();

    public HediyeIslemDAT()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int HediyeIslemKaydet(int siparisId, int erpCariId, DateTime hediyeIslemTarihi)
    {
        int val = 0;
        try
        {

            SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString());
            SqlCommand sqlcomm = new SqlCommand("HediyeIslemKaydet");
            sqlcomm.CommandType = CommandType.StoredProcedure;
            sqlcomm.Connection = sqlcon;
            sqlcomm.Parameters.AddWithValue("@siparisId", siparisId);
            sqlcomm.Parameters.AddWithValue("@erpCariId", erpCariId);
            sqlcomm.Parameters.AddWithValue("@hediyeIslemTarihi", hediyeIslemTarihi);
            sqlcon.Open();
            val = Convert.ToInt32(sqlcomm.ExecuteScalar());
            sqlcomm.Dispose();
            sqlcon.Dispose();

        }
        catch (Exception ex)
        {
            return 0;
        }
        return val;
    }

    public DataTable HediyeIslemGetir()
    {
        DataTable dtHediyeIslem = new DataTable();

        try
        {
            dbConn.OpenConnection();
            dtHediyeIslem = dbConn.ExecuteDataTable("HediyeIslemGetir");
            dbConn.CloseConnecion();
        }
        catch (Exception)
        {
            dtHediyeIslem = new DataTable();
        }

        return dtHediyeIslem;
    }
}

