﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Goldb2cDAL;
using System.Data;
using System.Data.SqlClient;


public class LogIslemleriDAT
{
    DbConnector dbConn = new DbConnector();

    public int KullaniciSiparisLogKayit(int kullaniciId, int kullaniciUyelikTip, int siparisId, string aciklama, string hataMesaj, string kullaniciIpAdres)
    {
        int Sonuc = 0;
        try
        {
            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@kullaniciId", kullaniciId);
            dbParamColl.Add("@kullaniciUyelikTip", kullaniciUyelikTip);
            dbParamColl.Add("@siparisId", siparisId);
            dbParamColl.Add("@aciklama", aciklama);
            dbParamColl.Add("@hataMesaj", hataMesaj);
            dbParamColl.Add("@kullaniciIpAdres", kullaniciIpAdres);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteScalar("KullaniciSiparisLogKayit", dbParamColl));
            dbConn.CloseConnecion();

        }
        catch (Exception)
        {
            return 0;
        }
        return Sonuc;
    }

    public DataTable KullaniciSiparisLogBilgileriGetir(int kullaniciId, int kullaniciUyelikTip, int siparisId, string logTarih1, string logTarih2)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@kullaniciId", kullaniciId);
        dbParamColl.Add("@kullaniciUyelikTip", kullaniciUyelikTip);
        dbParamColl.Add("@siparisId", siparisId);
        dbParamColl.Add("@logTarih1", logTarih1);
        dbParamColl.Add("@logTarih2", logTarih2);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("KullaniciSiparisLogBilgileriGetir", dbParamColl);
        dbConn.CloseConnecion();

        return dt;

    }

    public int KullaniciOlusturLogKayit(int kullaniciId, int kullaniciUyelikTip, string aciklama, string hataMesaj, string kullaniciIpAdres, int olusturulanKullaniciId)
    {
        int Sonuc = 0;
        try
        {
            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@kullaniciId", kullaniciId);
            dbParamColl.Add("@kullaniciUyelikTip", kullaniciUyelikTip);
            dbParamColl.Add("@aciklama", aciklama);
            dbParamColl.Add("@hataMesaj", hataMesaj);
            dbParamColl.Add("@kullaniciIpAdres", kullaniciIpAdres);
            dbParamColl.Add("@olusturulanKullaniciId", olusturulanKullaniciId);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteScalar("KullaniciOlusturLogKayit", dbParamColl));
            dbConn.CloseConnecion();

        }
        catch (Exception)
        {
            return 0;
        }
        return Sonuc;
    }

    public DataTable KullaniciOlusturLogBilgileriGetir(int kullaniciId, int kullaniciUyelikTip, string logTarih1, string logTarih2, int olusturulanKullaniciId)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@kullaniciId", kullaniciId);
        dbParamColl.Add("@kullaniciUyelikTip", kullaniciUyelikTip);
        dbParamColl.Add("@logTarih1", logTarih1);
        dbParamColl.Add("@logTarih2", logTarih2);
        dbParamColl.Add("@olusturulanKullaniciId", olusturulanKullaniciId);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("KullaniciOlusturLogBilgileriGetir", dbParamColl);
        dbConn.CloseConnecion();

        return dt;

    }

    public int KullaniciPuanLogKayit(string CRMCustomerGUID, int? totalGoldPoint, int? totalPointAdvance, int siparisId, string aciklama, bool islemOncesiSonrasi)
    {
        int Sonuc = 0;
        try
        {
            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@CRMCustomerGUID", CRMCustomerGUID);
            dbParamColl.Add("@totalGoldPoint", totalGoldPoint);
            dbParamColl.Add("@totalPointAdvance", totalPointAdvance);
            dbParamColl.Add("@siparisId", siparisId);
            dbParamColl.Add("@aciklama", aciklama);
            dbParamColl.Add("@islemOncesiSonrasi", islemOncesiSonrasi);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteScalar("KullaniciPuanLogKayit", dbParamColl));
            dbConn.CloseConnecion();

        }
        catch (Exception)
        {
            return 0;
        }
        return Sonuc;
    }

    public DataTable KullaniciPuanLogBilgileriGetir(string CRMCustomerGUID, int siparisId)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@CRMCustomerGUID", CRMCustomerGUID);
        dbParamColl.Add("@siparisId", siparisId);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("KullaniciPuanLogBilgileriGetir", dbParamColl);
        dbConn.CloseConnecion();

        return dt;

    }

    public int KullaniciAvansPuanLogKayit(int kullaniciId, int kullanilacakAvansPuan, int siparisId, string aciklama, DateTime siparisTarih)
    {
        int Sonuc = 0;
        try
        {
            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@kullaniciId", kullaniciId);
            dbParamColl.Add("@kullanilacakAvansPuan", kullanilacakAvansPuan);
            dbParamColl.Add("@siparisId", siparisId);
            dbParamColl.Add("@aciklama", aciklama);
            dbParamColl.Add("@siparisTarih", siparisTarih);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteScalar("KullaniciAvansPuanLogKayit", dbParamColl));
            dbConn.CloseConnecion();

        }
        catch (Exception)
        {
            return 0;
        }
        return Sonuc;
    }

    public int KullaniciAvansPuanLogSiparisOnay(int siparisId)
    {
        int Sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@siparisId", siparisId);

        dbConn.OpenConnection();
        Sonuc = dbConn.ExecuteNonQuery("KullaniciAvansPuanLogSiparisOnay", dbParamColl);
        dbConn.CloseConnecion();

        return Sonuc;
    }

    public int KullaniciAvansPuanLogSiparisIptal(int siparisId)
    {
        int Sonuc = 0;

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@siparisId", siparisId);

        dbConn.OpenConnection();
        Sonuc = dbConn.ExecuteNonQuery("KullaniciAvansPuanLogSiparisIptal", dbParamColl);
        dbConn.CloseConnecion();

        return Sonuc;
    }

    public DataTable KullaniciAvansPuanLogBilgileriGetir(int kullaniciId, int siparisId, string siparisTarih1, string siparisTarih2, int siparisDurum)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@kullaniciId", kullaniciId);
        dbParamColl.Add("@siparisId", siparisId);
        dbParamColl.Add("@siparisTarih1", siparisTarih1);
        dbParamColl.Add("@siparisTarih2", siparisTarih2);
        dbParamColl.Add("@siparisDurum", siparisDurum);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("KullaniciAvansPuanLogBilgileriGetir", dbParamColl);
        dbConn.CloseConnecion();

        return dt;

    }
}

