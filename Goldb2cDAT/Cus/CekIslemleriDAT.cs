﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Goldb2cDAL;
using System.Data;
using System.Data.SqlClient;


public class CekIslemleriDAT
{
    DbConnector dbConn = new DbConnector();

    public DataTable IndirimCekleriKullaniciGetir(int kullaniciId, int firmaKod)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@kullaniciId", kullaniciId);
        dbParamColl.Add("@firmaKod", firmaKod);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("IndirimCekleriKullaniciGetir", dbParamColl);
        dbConn.CloseConnecion();

        return dt;
    }

    public int IndirimCekleriKullaniciKaydet(int kullaniciId, string gonderilenEmailAdresi, int firmaKod)
    {
        int Sonuc = 0;
        try
        {
            DbParamCollection dbParamColl = new DbParamCollection();
            dbParamColl.Add("@kullaniciId", kullaniciId);
            dbParamColl.Add("@gonderilenEmailAdresi", gonderilenEmailAdresi);
            dbParamColl.Add("@firmaKod", firmaKod);

            dbConn.OpenConnection();
            Sonuc = Convert.ToInt32(dbConn.ExecuteScalar("IndirimCekleriKullaniciKaydet", dbParamColl));
            dbConn.CloseConnecion();

        }
        catch (Exception)
        {
            return 0;
        }
        return Sonuc;
    }

    public DataTable IndirimCekleriAlanKullanicilar(int kullaniciId, string gonderilmeTarih1, string gonderilmeTarih2, int firmaKod)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@kullaniciId", kullaniciId);
        dbParamColl.Add("@gonderilmeTarih1", gonderilmeTarih1);
        dbParamColl.Add("@gonderilmeTarih2", gonderilmeTarih2);
        dbParamColl.Add("@firmaKod", firmaKod);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("IndirimCekleriAlanKullanicilar", dbParamColl);
        dbConn.CloseConnecion();

        return dt;
    }

    public DataTable IndirimCekleriFirmaGetir(int firmaKod)
    {
        DataTable dt = new DataTable();

        DbParamCollection dbParamColl = new DbParamCollection();
        dbParamColl.Add("@firmaKod", firmaKod);

        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("IndirimCekleriFirmaGetir", dbParamColl);
        dbConn.CloseConnecion();

        return dt;
    }

    public DataTable IndirimCekleriFirmalarGetir()
    {
        DataTable dt = new DataTable();
        dbConn.OpenConnection();
        dt = dbConn.ExecuteDataTable("IndirimCekleriFirmalarGetir");
        dbConn.CloseConnecion();

        return dt;
    }

}

