﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cDAL;

namespace Goldb2cDAT
{
    public class GeziciIslemleriDAT
    {
        DbConnector dbConn = new DbConnector();

        public GeziciIslemleriDAT()
        {

        }

        public void KaydetGeziciLog(string pc, string domain, string osversion)
        {
            DbParamCollection dbParamColl = new DbParamCollection();

            dbParamColl.Add("@pc", pc);
            dbParamColl.Add("@domain", domain);
            dbParamColl.Add("@osversion", osversion);

            dbConn.OpenConnection();
            dbConn.ExecuteNonQuery("KaydetGeziciLog", dbParamColl);
            dbConn.CloseConnecion();
        }
    }
}
