

namespace Goldb2cInfrastructure
{
    /// <summary>
    /// Summary description for BusinessRuleException.
    /// </summary>
    public class BusinessRuleException : System.Exception
    {
        public BusinessRuleException() { }
        public BusinessRuleException(string szMessage)
            : base(szMessage)
        {
            this.message = szMessage;
        }
        public BusinessRuleException(string szMessageDis, string szMessageIc)
            : base(szMessageIc)
        {
            this.message = szMessageIc;
        }
        private string message;
        public string Message
        {
            get { return this.message; }
            set { this.message = value; }
        }
    }
}
