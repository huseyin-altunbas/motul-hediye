using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Net;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Globalization;

namespace Goldb2cInfrastructure
{
    public static class FormatControl
    {
        private static readonly string CharactersLowerCase = "abcdefghijklmnopqrstuvwxyz";
        public static readonly string ValidEMailCharacters = "0123456789abcdefghijklmnopqrstuvwxyz_";
        public static bool IsInteger(string value)
        {
            if (value == null)
                return false;
            string tmp = value.Trim();
            if (tmp.Length == 0)
                return false;
            for (int i = 0; i < tmp.Length; i++)
                if (!char.IsDigit(tmp, i))
                    return false;
            return true;
        }

        public static void SendMail(string subject, string body, string from, string to)
        {
            System.Net.Mail.MailMessage posta = new System.Net.Mail.MailMessage();
            SmtpClient postaSunucu = new SmtpClient();

            posta.From = new MailAddress(from);

            if (to.IndexOf(";") > 0) // Birden �ok adres
            {
                string[] gonderilecekler = to.Split(";".ToCharArray());
                for (int i = 0; i < gonderilecekler.Length; i++)
                {
                    if (gonderilecekler[i] != "")
                        posta.Bcc.Add(new MailAddress(gonderilecekler[i].ToLower().Replace("�", "i").Replace("�", "g").Replace("�", "u").Replace("�", "s").Replace("�", "o").Replace("�", "c")));
                }

            }
            else if (to.IndexOf(",") > 0) // Birden �ok adres
            {
                string[] gonderilecekler = to.Split(",".ToCharArray());
                for (int i = 0; i < gonderilecekler.Length; i++)
                {
                    if (gonderilecekler[i] != "")
                        posta.Bcc.Add(new MailAddress(gonderilecekler[i].ToLower().Replace("�", "i").Replace("�", "g").Replace("�", "u").Replace("�", "s").Replace("�", "o").Replace("�", "c")));
                }

            }
            else if (to.IndexOf(" ") > 0) // Birden �ok adres
            {
                string[] gonderilecekler = to.Split(" ".ToCharArray());
                for (int i = 0; i < gonderilecekler.Length; i++)
                {
                    if (gonderilecekler[i] != "")
                        posta.Bcc.Add(new MailAddress(gonderilecekler[i].ToLower().Replace("�", "i").Replace("�", "g").Replace("�", "u").Replace("�", "s").Replace("�", "o").Replace("�", "c")));
                }
            }
            else
                posta.To.Add(new MailAddress(to));

            posta.Subject = subject;
            posta.Body = body;
            posta.IsBodyHtml = true;
            posta.BodyEncoding = System.Text.Encoding.UTF8;

            //postaSunucu.Host = "";
            postaSunucu.Host = "127.0.0.1";
            postaSunucu.Send(posta);
        }

        public static string GetDateFormated(DateTime parDate)
        {
            return FormatControl.AddCarToString(false, 2, '0', parDate.Day.ToString()) + "." + FormatControl.AddCarToString(false, 2, '0', parDate.Month.ToString()) + "." + parDate.Year.ToString();
        }
        public static string GetDatePathFormated(DateTime parDate)
        {
            return parDate.Year.ToString() + "/" + FormatControl.AddCarToString(false, 2, '0', parDate.Month.ToString()) + "/" + FormatControl.AddCarToString(false, 2, '0', parDate.Day.ToString()) + "/";
        }

        public static bool IsDate(string value)
        {
            try
            {
                Convert.ToDateTime(value);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public static bool IsNumeric(string text)
        {
            bool isNumeric;
            try
            {
                Convert.ToInt64(text);
                isNumeric = true;
            }
            catch (Exception)
            {
                isNumeric = false;
            }
            return isNumeric;
        }
        public static void AddSeciniz(ref DataTable parDataTable)
        {
            DataRow dr = parDataTable.NewRow();
            
            dr[1] = "Se�iniz";
            parDataTable.Rows.InsertAt(dr, 0);
        }

        private static void GetRequest(string url)
        {
            WebRequest wr = WebRequest.Create(url);
            wr.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse response = (HttpWebResponse)wr.GetResponse();
            response.Close();
        }
        public static bool IsMoney(string value)
        {
            if (value == null)
                return false;
            string tmp = value.Trim();
            if (tmp.Length == 0)
                return false;
            for (int i = 0; i < tmp.Length; i++)
                if (!char.IsDigit(tmp, i) && tmp.ToCharArray()[i] != ',')
                    return false;
            return true;
        }
        public static string AddCarToString(bool right,int length,char chartoadd,string data)
        {
            if(chartoadd.ToString() != "")
            while (data.Length < length)
            {
                if (right)
                    data = data + chartoadd;
                else
                    data = chartoadd+data;
            }
        return data;
        }
        public static bool IsDate(string value,char parSeperator )
        {
            if (value == null)
                return false;
            string[] parts = value.Trim().Split(new char[] { parSeperator });
            if (parts.Length != 3)
                return false;
            for (int i = 0; i < parts.Length; i++)
                if (!IsInteger(parts[i]))
                    return false;
            int day = int.Parse(parts[0].Trim());
            int month = int.Parse(parts[1].Trim());
            int year = int.Parse(parts[2].Trim());
            if (year < 1900 || year > 3000 || month < 1 || month > 12 || day < 1 || day > DateTime.DaysInMonth(year, month))
                return false;
            return true;
        }
        public static Nullable<DateTime> ConvertToDate(string value)
        {
            if (IsDate(value,'/'))
            {
                string[] parts = value.Split(new char[] { '/' });
                return new DateTime(int.Parse(parts[2].Trim()), int.Parse(parts[1].Trim()), int.Parse(parts[0].Trim()));
            }
            else
                return null;
        }
        public static string ConvertToString(Nullable<DateTime> date)
        {
            if (date == null)
                return "";
            return date.Value.Day.ToString() + "/" + date.Value.Month.ToString() + "/" + date.Value.Year;
        }
        public static bool CheckEMail(string eMail)
        {
            if (eMail == null)
                return false;
            string email = eMail.Trim();
            string[] strEmail = email.Split(new char[] { '@' });
            if (strEmail.Length != 2 || strEmail[0].Length < 2 || strEmail[1].Length < 5)
                return false;

            string[] left = strEmail[0].Split(new char[] { '.', '-' });
            for (int i = 0; i < left.Length; i++)
                if (left[i].Length < 1 || !CheckEMailPart(left[i]))
                    return false;
            string[] right = strEmail[1].Split(new char[] { '.' });
            if (right.Length < 2 || right[right.Length - 1].Length < 2)
                return false;
            for (int i = 0; i < right.Length - 1; i++)
            {
                if (right[i].Length < 2)
                    return false;
                string[] tmp = right[i].Split(new char[] { '-' });
                for (int j = 0; j < tmp.Length; j++)
                    if (tmp[j].Length < 1 || !CheckEMailPart(tmp[j]))
                        return false;
            }
            return CheckOnlyLowerCaseCharacters(right[right.Length - 1]);
        }
        private static bool CheckEMailPart(string str)
        {
            if (str == null || str == "")
                return false;
            for (int i = 0; i < str.Length; i++)
                if (ValidEMailCharacters.IndexOf(str[i]) < 0)
                    return false;
            return true;
        }
        private static bool CheckOnlyLowerCaseCharacters(string str)
        {
            if (str == null || str == "")
                return false;
            for (int i = 0; i < str.Length; i++)
                if (CharactersLowerCase.IndexOf(str[i]) < 0)
                    return false;
            return true;
        }
        
        
        
        public static DataRow[] SelectDistinct(DataTable SourceTable, params string[] FieldNames)
        {
            object[] lastValues;
            DataTable newTable;
            DataRow[] orderedRows;

            if (FieldNames == null || FieldNames.Length == 0)
                throw new ArgumentNullException("FieldNames");

            lastValues = new object[FieldNames.Length];
            newTable = new DataTable();

            foreach (string fieldName in FieldNames)
                newTable.Columns.Add(fieldName, SourceTable.Columns[fieldName].DataType);

            orderedRows = SourceTable.Select("", string.Join(", ", FieldNames));

            foreach (DataRow row in orderedRows)
            {
                if (!fieldValuesAreEqual(lastValues, row, FieldNames))
                {
                    newTable.Rows.Add(createRowClone(row, newTable.NewRow(), FieldNames));

                    setLastValues(lastValues, row, FieldNames);
                }
            }

            return newTable.Select("", FieldNames[0]);
        }
        private static DataRow createRowClone(DataRow sourceRow, DataRow newRow, string[] fieldNames)
        {
            foreach (string field in fieldNames)
                newRow[field] = sourceRow[field];

            return newRow;
        }

        private static void setLastValues(object[] lastValues, DataRow sourceRow, string[] fieldNames)
        {
            for (int i = 0; i < fieldNames.Length; i++)
                lastValues[i] = sourceRow[fieldNames[i]];
        }


        private static bool fieldValuesAreEqual(object[] lastValues, DataRow currentRow, string[] fieldNames)
        {
            bool areEqual = true;

            for (int i = 0; i < fieldNames.Length; i++)
            {
                if (lastValues[i] == null || !lastValues[i].Equals(currentRow[fieldNames[i]]))
                {
                    areEqual = false;
                    break;
                }
            }

            return areEqual;
        }
        public static string PostGetHtml(string reqUrl)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(reqUrl);
            try
            {

                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                Encoding encoding = Encoding.UTF8;
                StreamReader reader = new StreamReader(httpWebResponse.GetResponseStream(), encoding);
                string responseXml = reader.ReadToEnd();

                httpWebResponse.Close();
                reader.Close();
                return responseXml;
            }
            catch (Exception e)
            {
                return null;
            }

        }
        public static string AddParameterToUrl(string parUrl, string parParameterName, string parParameterValue)
        {
            if (parUrl.IndexOf(parParameterName) == -1)
            {
                if (parUrl.IndexOf("?") == -1)
                    parUrl += "?" + parParameterName + "=" + parParameterValue;
                else
                    parUrl += "&" + parParameterName + "=" + parParameterValue;
            }
            else
            {
                if (parUrl.IndexOf("&", parUrl.IndexOf(parParameterName)) != -1)
                    parUrl = parUrl.Replace(parUrl.Substring(parUrl.IndexOf(parParameterName), parUrl.IndexOf("&", parUrl.IndexOf(parParameterName)) - parUrl.IndexOf(parParameterName)), parParameterName + "=" + parParameterValue);
                else
                    parUrl = parUrl.Replace(parUrl.Substring(parUrl.IndexOf(parParameterName)), parParameterName + "=" + parParameterValue);
            }
            return parUrl;
        }
    }
    public static class cons
    {
        public static DateTimeFormatInfo GetDatTimeFormatInfo
        {
            get
            {
                DateTimeFormatInfo myDTFI = new CultureInfo("tr-TR", false).DateTimeFormat;

                //DateTimeFormatInfo dfi = new DateTimeFormatInfo();
                //dfi.ShortDatePattern = "DD.MM.YYYY";
                return myDTFI;
            }
        }
    }
}
