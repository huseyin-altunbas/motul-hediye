using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Collections.Specialized;
using System.Globalization;

namespace Goldb2cInfrastructure
{
    public static class RequestCus
    {
        #region Daisy
        public static int? GetRequestNullableInteger(string key)
        {
            if (HttpContext.Current.Request[key] == null)
            {
                return null;
            }

            if (!FormatControl.IsInteger(HttpContext.Current.Request[key].Replace(".", "")))
            {
                return null;
            }

            long intMax = Convert.ToInt64(HttpContext.Current.Request[key].Replace(".", ""));
            if (intMax > int.MaxValue)
                intMax = int.MaxValue;
            return Convert.ToInt32(intMax);
        }
        public static bool? GetRequestNullableBool(string key)
        {
            if (HttpContext.Current.Request[key] == null)
            {
                return null;
            }

            if (Convert.ToBoolean( HttpContext.Current.Request[key].Replace(".", "")) != true)
            {
                return false;
            }

           return true;
        }

             #endregion


    }
}
