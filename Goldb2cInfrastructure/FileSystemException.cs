using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Goldb2cInfrastructure
{
    public class FileSystemException : DbConnectorException
    {
        public FileSystemException(IOException ioe)
        {
            // TODO: FS specific...
            throw new DbConnectorException("FileSystemExceptionID: " + ioe.Message,ioe);
        }
    }
}
