using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;

namespace Goldb2cInfrastructure
{
    public static class ExceptionLogger
    {
        public static void SaveNewException(Exception e, Enums.ExceptionType exceptionType, bool redirectToErrorPage)
        {
            string _ConnectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
            
            DbProviderFactory insDbProviderFactory = DbProviderFactories.GetFactory("System.Data.SqlClient");
            DbConnection _Connection = insDbProviderFactory.CreateConnection();
            System.Web.HttpContext cnt = System.Web.HttpContext.Current;
            
            int? ExceptionID = 0;
            try
            {
            DbCommand insDbCommand = insDbProviderFactory.CreateCommand();
            insDbCommand.CommandText = "GenInsertExceptions";
            insDbCommand.CommandType = CommandType.StoredProcedure;
            insDbCommand.Connection = _Connection;
            #region Link
            DbParameter insDbparameterLink = insDbProviderFactory.CreateParameter();
            insDbparameterLink.ParameterName = "Link";
            insDbparameterLink.Value = cnt.Request.Url.ToString();
            insDbCommand.Parameters.Add(insDbparameterLink);
            #endregion
            #region Message
            DbParameter insDbparameterMessage = insDbProviderFactory.CreateParameter();
            insDbparameterMessage.ParameterName = "Message";
            insDbparameterMessage.Value = e.Message;
            insDbCommand.Parameters.Add(insDbparameterMessage);
             #endregion
            #region InnerException
            DbParameter insDbparameterInnerException = insDbProviderFactory.CreateParameter();
            insDbparameterInnerException.ParameterName = "InnerException";
            insDbparameterInnerException.Value = e.InnerException.Message;
            insDbCommand.Parameters.Add(insDbparameterInnerException);
            #endregion
            #region ExceptionType
            DbParameter insDbparameterExceptionType = insDbProviderFactory.CreateParameter();
            insDbparameterExceptionType.ParameterName = "ExceptionTypeID";
            insDbparameterExceptionType.Value = (int)exceptionType;
            insDbCommand.Parameters.Add(insDbparameterExceptionType);
            #endregion
            #region StackTrace
            DbParameter insDbparameterStackTrace = insDbProviderFactory.CreateParameter();
            insDbparameterStackTrace.ParameterName = "StackTrace";
            insDbparameterStackTrace.Value = e.StackTrace;
            insDbCommand.Parameters.Add(insDbparameterStackTrace);
             #endregion
            #region Source
            DbParameter insDbparameterSource = insDbProviderFactory.CreateParameter();
            insDbparameterSource.ParameterName = "Source";
            insDbparameterSource.Value = e.Source;
            insDbCommand.Parameters.Add(insDbparameterSource);
            #endregion
            #region ClientMemberID
            DbParameter insDbparameterClientMemberID = insDbProviderFactory.CreateParameter();
            insDbparameterClientMemberID.ParameterName = "ClientMemberID";
            if (cnt.Session["MemberID"] != null)
                insDbparameterClientMemberID.Value = Convert.ToInt32(cnt.Session["MemberID"]);
            else
                insDbparameterClientMemberID.Value = DBNull.Value;
            insDbCommand.Parameters.Add(insDbparameterClientMemberID);
             #endregion
            #region UserAgent
            DbParameter insDbparameterUserAgent = insDbProviderFactory.CreateParameter();
            insDbparameterUserAgent.ParameterName = "UserAgent";
            insDbparameterUserAgent.Value = cnt.Request.UserAgent;
            insDbCommand.Parameters.Add(insDbparameterUserAgent);
            #endregion
            #region WebServer
            DbParameter insDbparameterWebServer = insDbProviderFactory.CreateParameter();
            insDbparameterWebServer.ParameterName = "WebServer";
            insDbparameterWebServer.Value = cnt.Server.MachineName;
            insDbCommand.Parameters.Add(insDbparameterWebServer);
            #endregion
            #region ExceptionID
            DbParameter insDbparameterExceptionID = insDbProviderFactory.CreateParameter();
            insDbparameterExceptionID.ParameterName = "ExceptionID";
            insDbparameterExceptionID.Direction = ParameterDirection.Output;
            insDbparameterExceptionID.Value = ExceptionID;
            insDbCommand.Parameters.Add(insDbparameterExceptionID);
            #endregion

           
            _Connection.ConnectionString = _ConnectionString;
            if (_Connection.State == ConnectionState.Closed)
               _Connection.Open();

            insDbCommand.ExecuteNonQuery();

                if(redirectToErrorPage)
                    cnt.Response.Redirect(CustomConfiguration.ErrorPage);
            }
            catch(System.Exception eHolder)
            {
                
            }
            finally
            {
                if (_Connection.State == ConnectionState.Open)
                    _Connection.Close();
            }
        }
    }
}