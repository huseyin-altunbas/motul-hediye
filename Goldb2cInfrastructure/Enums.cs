using System;
using System.Collections.Generic;
using System.Text;

namespace Goldb2cInfrastructure
{
    public static class Enums
    {
        public enum MailType
        {
            ArkadasinaOner = 1,
            Aktivasyon = 2,
            SifreHatirlatma = 3,
            Iletisim = 4,
            Yorum=5,
            YorumStatus=6,
            YarismaOner=7,
            ArkadasOnerResim = 8,
            ResimStatus=9
        }
        public enum Permissions
        { 
            NoPermission=0,
            ReadOnly=128,
            FullPermission = 255,
        }
        public enum ExceptionType
        { 
            FromDbConnector = 1,
            FromGlobalasax = 2
        }
        public enum UyelikTipi
        {
            Kullanici = 0,
            WebAdmin = 10,
            Admin = 1
        }
    }
}
