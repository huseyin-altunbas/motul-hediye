using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Goldb2cInfrastructure
{
    public static class CustomConfiguration
    {
        #region B2c
        public static string ErrorPage
        {
            get
            {
                return ConfigurationManager.AppSettings["ErrorPage"];
            }
        }


        public static string ConnectionString
        {
            get
            {
                return ConfigurationManager.AppSettings["ConnectionString"];
            }
        }

        public static string BaseUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["BaseUrl"];
            }
        }

        public static string getCacheKey
        {
            get
            {
                return "fc";
            }
        }

        public static string getCacheValue
        {
            get
            {
                return "b2c";
            }
        }
        public static int getCacheTimeMinutes
        {
            get
            {
                return 30;
            }
        }

        public static string getCapchaEncryptClearText
        {
            get
            {
                return "tufantufannl";
            }
        }
        public static string getCapchaEncryptPassword
        {
            get
            {
                return "tufanb2c";
            }
        }

        public static string getEncryptKey
        {
            get
            {
                return "";
            }
        }

        public static string getUyumBaglanti
        {
            get
            {
                return ConfigurationManager.AppSettings["UyumBaglanti"];
            }
        }

        public static string UcDGeriDonusUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["3dGeriDonusUrl"];
            }
        }
        public static bool DisableCache
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["DisableCache"]);
            }
        }

        public static string NoReply
        {
            get
            {
                return ConfigurationManager.AppSettings["NoReply"].ToString();
            }
        }

        public static string Facebook_client_id
        {
            get
            {
                return ConfigurationManager.AppSettings["Facebook_client_id"].ToString();
            }
        }

        public static string Facebook_redirect_url
        {
            get
            {
                return ConfigurationManager.AppSettings["Facebook_redirect_url"].ToString();
            }
        }

        public static string Facebook_client_secret
        {
            get
            {
                return ConfigurationManager.AppSettings["Facebook_client_secret"].ToString();
            }
        }

        public static string BkmMerchantId
        {
            get
            {
                return ConfigurationManager.AppSettings["BkmMerchantId"].ToString();
            }
        }

        public static string BkmSuccessUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["BkmSuccessUrl"].ToString();
            }
        }

        public static string BkmCancelUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["BkmCancelUrl"].ToString();
            }
        }

        public static string ImageDomain
        {
            get
            {
                return ConfigurationManager.AppSettings["ImageDomain"].ToString();
            }
        }


        public static string CekAdminServiceUser
        {
            get
            {
                return ConfigurationManager.AppSettings["CekAdminServiceUser"].ToString();
            }
        }

        public static string CekAdminServicePass
        {
            get
            {
                return ConfigurationManager.AppSettings["CekAdminServicePass"].ToString();
            }
        }
        public static int CekAdminProjectId
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["CekAdminProjectId"]);
            }
        }

        public static string GoldB2CSqlConn
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ToString();
            }
        }

        public static string SmsServiceUser
        {
            get
            {
                return ConfigurationManager.AppSettings["SmsServiceUser"].ToString();
            }
        }


        public static string SmsServicePass
        {
            get
            {
                return ConfigurationManager.AppSettings["SmsServicePass"].ToString();
            }
        }

        public static string SmsServiceOrjinator
        {
            get
            {
                return ConfigurationManager.AppSettings["SmsServiceOrjinator"].ToString();
            }
        }

        #endregion
    }
}
