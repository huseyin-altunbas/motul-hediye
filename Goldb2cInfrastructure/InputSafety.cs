using System;
using System.Collections.Generic;
using System.Text;

namespace Goldb2cInfrastructure
{
    public class InputSafety
    {
        public InputSafety()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static string SecureString(string szInsecure)
        {
            return SecureStringForXSS(SecureStringForSQLInjection(szInsecure));

        }

        public static string SecureStringForSQLInjection(string szInsecure)
        {

            return szInsecure.Replace(";", "").Replace("--", "");
        }

        public static string SecureStringForXSS(string szInsecure)
        {
            return szInsecure.Replace("<script>", "").Replace("</script>", "");
        }

    }
}
