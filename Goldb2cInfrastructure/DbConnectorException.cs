using System.Data;
using System.Data.SqlClient;

namespace Goldb2cInfrastructure
{
    /// <summary>
    /// Summary description for BusinessException.
    /// </summary>
    public class DbConnectorException : System.Exception
    {
        public DbConnectorException() { }
        public DbConnectorException(string szMessage,System.Exception ex)
            : base(szMessage)
        {
            this.Exception = ex;
            this.message = szMessage;
            WriteExceptionToDb();
        }
        //public DbConnectorException(string szMessageDis, string szMessageIc)
        //    : base(szMessageIc)
        //{
        //    this.message = szMessageIc;
        //}
        private string message;
        public string Message
        {
            get { return this.message; }
            set { this.message = value; }
        }

        private System.Exception exception;
        public System.Exception Exception
        {
            get { return this.exception; }
            set { this.exception = value; }
        }

        private void WriteExceptionToDb()
        {
            if (System.Configuration.ConfigurationSettings.AppSettings["LogExceptionsToDatabase"] == null ||
                System.Configuration.ConfigurationSettings.AppSettings["LogExceptionsToDatabase"] != "1")
            {
                return;
            }
            ExceptionLogger.SaveNewException(Exception,Enums.ExceptionType.FromDbConnector,false);
            
            //			System.Web.HttpContext cnt = System.Web.HttpContext.Current;
            //			cnt.Request.Path.ToString();
            //			cnt.Request.UserAgent;
            //			cnt.Request.QueryString.ToString();
            //			cnt.Timestamp;
            //			System.Environment.MachineName;
        }
    }
}
