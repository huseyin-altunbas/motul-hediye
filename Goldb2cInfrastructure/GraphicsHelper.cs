using System;
using System.Drawing;

namespace Goldb2cInfrastructure
{
	public enum RelativePosition
	{
		//TopLeft,
		//TopCenter,
		//TopRight,
		BottomLeft, 
		BottomCenter,
		BottomRight,
		//MiddleLeft,
		//MiddleCenter,
		//MiddleRight,
		Unspecified
	}

	/// <summary>
	/// Summary description for GraphicsHelper.
	/// </summary>
	public class GraphicsHelper
	{

		public static void WatermarkImage(Image imgToBeMarked, string szText, RelativePosition relPosition)
		{
			if(imgToBeMarked == null || szText == null || szText == "")
			{
				return;
			}
			
			int nWidth = imgToBeMarked.Width;
			int nHeight = imgToBeMarked.Height;
			
			Graphics g = Graphics.FromImage(imgToBeMarked);
			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;               

			//Watermark text'inin s��d��� en b�y�k boyutu ar�yoruz
			int[] sizes = new int[]{16,14,12,10,8,6,4};
			Font font = null;
			SizeF sizeOfFont = new SizeF();
			for (int i=0 ;i<7; i++)
			{
				font = new Font("arial", sizes[i], FontStyle.Bold);
				sizeOfFont = g.MeasureString(szText, font);
				if((ushort) sizeOfFont.Width < (ushort)nWidth) break; 
			}

			// A�a��dan %5 uzakl�k al�n�r
			int yPixelsFromBottom = (int)(nHeight *.05);
			// y koordinat� bulunur
			float yPosFromBottom = ((nHeight - yPixelsFromBottom) - (sizeOfFont.Height / 2));
			// x koordinat� bulunur
			float xCenterOfImg = (nWidth/2);

			StringFormat sf = new StringFormat();
			switch(relPosition)
			{
				case RelativePosition.BottomLeft:
					sf.Alignment = StringAlignment.Near;
					break;
				case RelativePosition.BottomCenter:
					sf.Alignment = StringAlignment.Center;
					break;
				case RelativePosition.BottomRight:
					sf.Alignment = StringAlignment.Far;
					break;
				default :
					sf.Alignment = StringAlignment.Center;
					break;
			}
			sf.Alignment = StringAlignment.Center;

			// Transparan siyah f�r�a (Alpha:153)
			SolidBrush brushBlackTransparant = new SolidBrush(Color.FromArgb(100, 0, 0, 0));
			// Transparan beyaz f�r�a (Alpha:153)
			SolidBrush brushWhiteTransparant = new SolidBrush(Color.FromArgb(100, 255, 255, 255));

			// Watermark
			g.DrawString(szText, font,                                   
				brushBlackTransparant,       
				new PointF(xCenterOfImg + 1, yPosFromBottom + 1), 
				sf);
			// ve g�lgesi �izilir
			g.DrawString(szText, font,                                   
				brushWhiteTransparant,                           
				new PointF(xCenterOfImg, yPosFromBottom),  
				sf);                               

			g.Dispose();
			brushBlackTransparant.Dispose();
			brushWhiteTransparant.Dispose();
		}

        public static Image LimitImageSize(Image imgSource, int? nWidthLimit, int? nHeightLimit)
        {
            //return new Bitmap(1000,1000);

            // Limit yoksa veya resim limitlerin alt�nda veya e�itse kopyas� gider
            if ((!nWidthLimit.HasValue || nWidthLimit.Value >= imgSource.Width) && (!nHeightLimit.HasValue || nHeightLimit.Value >= imgSource.Height))
            {
                return new Bitmap(imgSource);
            }

            int nScaledWidthDueToHeight = imgSource.Width;
            int nScaledHeightDueToWidth = imgSource.Height;
            int nChoosenWidth = imgSource.Width;
            int nChoosenHeight = imgSource.Height;

            // Geni�lik&Y�kseklik limitine g�re, �l��lendirilmi� y�kseklik hesab�
            if (nWidthLimit.HasValue && nHeightLimit.HasValue)
            {
                nScaledHeightDueToWidth = GetHeightWithWidthScaled(imgSource.Size, nWidthLimit.Value);
                if(nScaledHeightDueToWidth <= nHeightLimit.Value)
                {
                    nChoosenWidth = nWidthLimit.Value;
                    nChoosenHeight = nScaledHeightDueToWidth;
                }
                else
                {
                    nScaledWidthDueToHeight = GetWidthWithHeightScaled(imgSource.Size, nHeightLimit.Value);
                    if(nScaledWidthDueToHeight <= nWidthLimit.Value)
                    {
                        nChoosenWidth = nScaledWidthDueToHeight;
                        nChoosenHeight = nHeightLimit.Value;
                    }
                }
            }

            // Geni�lik limitine g�re, �l��lendirilmi� geni�lik hesab�
            if (nWidthLimit.HasValue && !nHeightLimit.HasValue)
            {
                nChoosenWidth = nWidthLimit.Value;
                nChoosenHeight = GetHeightWithWidthScaled(imgSource.Size, nWidthLimit.Value);
            }

            // Y�kseklik limitine g�re, �l��lendirilmi� geni�lik hesab�
            if (!nWidthLimit.HasValue && nHeightLimit.HasValue)
            {
                nChoosenWidth = GetWidthWithHeightScaled(imgSource.Size, nHeightLimit.Value);
                nChoosenHeight = nHeightLimit.Value;
            }


            //float aspectRatioOriginal = (float) imgSource.Width / imgSource.Height;
            //float aspectRatioLimits = (float)imgSource.Width / imgSource.Height;


            Bitmap bmp = new Bitmap(nChoosenWidth, nChoosenHeight);
            Graphics g = Graphics.FromImage(bmp);

            g.DrawImage(imgSource, 0, 0, nChoosenWidth, nChoosenHeight);
            
            g.Dispose();

            return bmp;
        }
        //public static Image StretchImageSize(Image imgSource, int? nWidthLimit, int? nHeightLimit)
        public static Image StretchImageSize(Image imgSource, int _NewWidth, int _NewHeight, int _OriginalWidth, int _OriginalHeight)
        {
            
            Bitmap btmNew = new Bitmap(_NewWidth, _NewHeight);
            Graphics.FromImage(btmNew).FillRectangle(Brushes.White, new Rectangle(0, 0, _NewWidth, _NewHeight));
            Graphics.FromImage(btmNew).DrawImage(imgSource, (_NewWidth - _OriginalWidth) / 2, (_NewHeight - _OriginalHeight) / 2, _OriginalWidth, _OriginalHeight);

            //btmNew.Dispose();
            return btmNew;
            //int nChoosenWidth = imgSource.Width;
            //if (nWidthLimit.HasValue) nChoosenWidth = nWidthLimit.Value;

            //int nChoosenHeight = imgSource.Height;
            //if (nHeightLimit.HasValue) nChoosenHeight = nHeightLimit.Value;

            //Bitmap bmp = new Bitmap(nChoosenWidth, nChoosenHeight);
            //Graphics g = Graphics.FromImage(bmp);
            //g.DrawImage(imgSource, 0, 0, nChoosenWidth, nChoosenHeight);

            //g.Dispose();

            //return bmp;
        }
        public static int GetHeightWithWidthScaled(Size sizeOriginal, int nWidthLimit)
        {
            if(sizeOriginal.Width <= nWidthLimit) 
                return sizeOriginal.Height;

            return Convert.ToInt32((float) (nWidthLimit*sizeOriginal.Height)/sizeOriginal.Width);
        }
        public static int GetWidthWithHeightScaled(Size sizeOriginal, int nHeightLimit)
        {
            if (sizeOriginal.Height <= nHeightLimit)
                return sizeOriginal.Width;

            return Convert.ToInt32((float)(nHeightLimit * sizeOriginal.Width) / sizeOriginal.Height);
        }
    }
}
