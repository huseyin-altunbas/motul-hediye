using System;
using System.Text.RegularExpressions;

namespace Goldb2cInfrastructure
{
	public class Validation
	{
		public static bool ValidateEmail(string parEmail)
		{
			Regex regExp = new Regex(@"^([a-zA-Z0-9_\-])+(\.([a-zA-Z0-9_\-])+)*@((\[(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5]))\]))|((([a-zA-Z0-9])+(([\-])+([a-zA-Z0-9])+)*\.)+([a-zA-Z])+(([\-])+([a-zA-Z0-9])+)*))$");
			return regExp.IsMatch(parEmail);
		}
		public static bool ValidateNumber(string parNumber)
		{
            if (parNumber == "")
                return false;
			for (int i = 0; i < parNumber.Length; i++)
			{
                if (!(char.IsNumber(parNumber[i])) && parNumber[i] != '-') 
                    return false;
			}
			return true;
		}
		public static bool ValidateCurrency(string parCurrency)
		{
			try
			{
				decimal.Parse(parCurrency);
			}
			catch (Exception)
			{
				return false;
			}
			return true;
		}
		public static bool ValidateDateTime(string parDateTime)
		{
			Regex regExp = new Regex(@"^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-./])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$");

			return regExp.IsMatch(parDateTime);
		}
		public static bool ValidateAmongDates(string parDateToValidate, DateTime parSmallDate, DateTime parBigDate)
		{
			if (DateTime.Parse(parDateToValidate) <= parBigDate && DateTime.Parse(parDateToValidate) >= parSmallDate)
				return true;
			return false;
		}
		public static bool ValidateAmongNumbers(string parNumberToValidate, long parSmallNumber, long parBigNumber)
		{
			if (!(long.Parse(parNumberToValidate) <= parBigNumber && long.Parse(parNumberToValidate) >= parSmallNumber))
				return false;
			return true;
		}
		public static bool ValidateAmongNumbers(string parNumberToValidate, decimal parSmallNumber, decimal parBigNumber)
		{
			if (!(decimal.Parse(parNumberToValidate) <= parBigNumber && decimal.Parse(parNumberToValidate) >= parSmallNumber))
				return false;
			return true;
		}
		public static bool ValidateLength(string parString, int parMinLength, int parMaxLength)
		{
			if (parString.Length >= parMinLength && parString.Length <= parMaxLength)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		public static bool ValidateNotNull(string parValue)
		{
			if (parValue == null)
				return false;
			if (parValue == "")
				return false;
			else
				return true;
		}
	}
}
