﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using Goldb2cEntity;
using System.Text;
using Goldb2cInfrastructure;
using System.IO;
using System.Net;
using System.Web.Services;
using Goldb2cDAT.Cus;
using Goldb2cBLL.Custom;
//using com.alcikart.www;

public partial class SiparisOnay : System.Web.UI.Page
{
    private static object lockObject = new Object();
    public string GenelToplam { get; set; }
    SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
    SendSmsBLL sendSmsBll = new SendSmsBLL();
    //SayiOkuma sayiOkuma = new SayiOkuma();

    SiparisIslemleriBLL bllSiparisIslemleri = new SiparisIslemleriBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();

    Kullanici kullanici;
    BannerBLL bllBanner = new BannerBLL();
    TransactionBLL transactionBLL = new TransactionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {


        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (Session["Kullanici"] != null)
            kullanici = (Kullanici)Session["Kullanici"];

        int SiparisMasterId = 0;
        if (Session["SiparisMasterId"] != null)
        {
            string _SiparisMasterId = Session["SiparisMasterId"].ToString();
            string[] _SiparisMasterIdArr = _SiparisMasterId.Split(',');
            foreach (string item in _SiparisMasterIdArr)
            {
                SiparisMasterId = 0;
                int.TryParse(item, out SiparisMasterId);
                if (SiparisMasterId > 0)
                {
                    break;
                }
            }
        }
        else
        {
            Response.Redirect("/", true);
        }


        Helper.PuanSorgula();

        //string outOnayDurum1;
        //string outOnayDurum2;
        //bllSiparisGoruntuleme.SiparisOnayDurum(SiparisMasterId, "Bekliyor", "Onaylandı", out  outOnayDurum1, out outOnayDurum2);
        //if (outOnayDurum1 != "Onaylandı")
        //{
        //    //outOnayDurum1 = "1 " + outOnayDurum1 + Convert.ToString(SiparisMasterId);
        //    tamamdiv.Visible = false; onaydiv.Visible = true;
        //    //ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script type=\"text/javascript\">alert('" + outOnayDurum1 + "');</script>");
        //}
        //else
        //{
        //    //outOnayDurum1 = "2 " + outOnayDurum1 + Convert.ToString(SiparisMasterId);
        tamamdiv.Visible = false; onaydiv.Visible = true;
        //ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script type=\"text/javascript\">alert('" + outOnayDurum1 + "');</script>");

        //}
        if (Request.QueryString["status"] != null)
        {
            tamamdiv.Visible = true; onaydiv.Visible = false;
            btnGonder.Visible = false;
        }
        DataSet dsSiparisVerileri = bllSiparisGoruntuleme.SiparisGoruntule(SiparisMasterId, kullanici.ErpCariId);
        //if (dsSiparisVerileri != null && dsSiparisVerileri.Tables["SiparisOdemeTT"].Rows.Count > 0)
        if (dsSiparisVerileri != null)
        {
            if (dsSiparisVerileri != null && dsSiparisVerileri.Tables["SiparisMasterTT"].Rows.Count > 0)
            {
                DataTable dtSiparisAdresTT = dsSiparisVerileri.Tables["SiparisAdresTT"];
                DataTable dtSiparisOdemeTT = dsSiparisVerileri.Tables["SiparisOdemeTT"];
                DataTable dtSiparisMasterTT = dsSiparisVerileri.Tables["SiparisMasterTT"];
                DataTable dtSiparisDetayTT = dsSiparisVerileri.Tables["SiparisDetayTT"];

                if (dtSiparisAdresTT != null && dtSiparisAdresTT.Rows.Count > 0)
                {
                    ////Fatura Adres
                    //ltrFaturaYazismaUnvan.Text = dtSiparisAdresTT.Rows[0]["fyazisma_unvan"].ToString();
                    //ltrFaturaAdres.Text = dtSiparisAdresTT.Rows[0]["fadres_tanim"].ToString();
                    //ltrFaturaIlce.Text = dtSiparisAdresTT.Rows[0]["filce_kod"].ToString();
                    //ltrFaturaSehir.Text = dtSiparisAdresTT.Rows[0]["fsehir_kod"].ToString();
                    //ltrFaturaCepTel.Text = dtSiparisAdresTT.Rows[0]["fmobil_telefon"].ToString();
                    //ltrFaturaSabitTel.Text = dtSiparisAdresTT.Rows[0]["fsabit_telefon"].ToString();

                    //Siparis Adres
                    ltrSiparisYazismaUnvan.Text = dtSiparisAdresTT.Rows[0]["syazisma_unvan"].ToString();
                    ltrSiparisAdres.Text = dtSiparisAdresTT.Rows[0]["sadres_tanim"].ToString();
                    ltrSiparisIlce.Text = dtSiparisAdresTT.Rows[0]["silce_kod"].ToString();
                    ltrSiparisSehir.Text = dtSiparisAdresTT.Rows[0]["ssehir_kod"].ToString();
                    ltrSiparisCepTel.Text = dtSiparisAdresTT.Rows[0]["smobil_telefon"].ToString();
                    ltrSiparisSabitTel.Text = dtSiparisAdresTT.Rows[0]["ssabit_telefon"].ToString();
                }

                //Sipariş Bilgileri
                ltrSiparisTarih.Text = Convert.ToDateTime(dtSiparisMasterTT.Rows[0]["siparis_tarih"]).ToString("dd/MM/yyyy");
                ltrSiparisSaat.Text = dtSiparisMasterTT.Rows[0]["siparis_saat"].ToString().Substring(0, 2) + ":" + dtSiparisMasterTT.Rows[0]["siparis_saat"].ToString().Substring(2, 2);
                ltrSiparisMasterId.Text = dtSiparisMasterTT.Rows[0]["siparis_no"].ToString();
                ltrKargo.Text = dtSiparisMasterTT.Rows[0]["kargo_kod"].ToString();

                //Sipariş Detayları
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("<!-- title -->");
                sb.AppendLine("<thead><tr>");
                sb.AppendLine("<th>Sıra<br />No</th>");
                sb.AppendLine("<th>Açıklama<br />&nbsp;</th>");
                sb.AppendLine("<th>Adet<br />&nbsp;</th>");
                sb.AppendLine("<th>Birim Puan<br />&nbsp;</th>");
                sb.AppendLine("<th>Toplam Tutarı<br />&nbsp;</th>");
                sb.AppendLine("</tr></thead>");
                sb.AppendLine("<!-- title -->");


                string _SiparisMasterId = Session["SiparisMasterId"].ToString();
                string[] _SiparisMasterIdArr = _SiparisMasterId.Split(',');
                int genelToplam = 0;
                foreach (string item in _SiparisMasterIdArr)
                {
                    SiparisMasterId = 0;
                    int.TryParse(item, out SiparisMasterId);
                    if (SiparisMasterId > 0)
                    {
                        DataSet dsSiparisVerileriV2 = bllSiparisGoruntuleme.SiparisGoruntule(SiparisMasterId, kullanici.ErpCariId);

                        DataTable dtSiparisAdresTTV2 = dsSiparisVerileriV2.Tables["SiparisAdresTT"];
                        DataTable dtSiparisOdemeTTV2 = dsSiparisVerileriV2.Tables["SiparisOdemeTT"];
                        DataTable dtSiparisMasterTTV2 = dsSiparisVerileriV2.Tables["SiparisMasterTT"];
                        DataTable dtSiparisDetayTTV2 = dsSiparisVerileriV2.Tables["SiparisDetayTT"];

                        for (int i = 0; i < dtSiparisDetayTT.Rows.Count; i++)
                        {
                            if (i % 2 == 0)
                            {
                                //dtSiparisDetayTT.Rows[i]["sira_no"].ToString()
                                sb.AppendLine("<!-- chooes -->");
                                sb.AppendLine("<tbody><tr>");
                                sb.AppendLine("<td>" + (i + 1).ToString() + "</td>");
                                sb.AppendLine("<td>" + dtSiparisDetayTTV2.Rows[i]["urun_ad"].ToString() + "</td>");
                                sb.AppendLine("<td>" + dtSiparisDetayTT.Rows[i]["siparis_adet"].ToString() + "</td>");
                                sb.AppendLine("<td>" + (Convert.ToInt32(dtSiparisDetayTTV2.Rows[i]["birim_fiyat"])).ToString("###,###,###") + " PUAN</td>");
                                sb.AppendLine("<td>" + (Convert.ToInt32(dtSiparisDetayTTV2.Rows[i]["toplam_tutar"])).ToString("###,###,###") + " PUAN</td>");
                                sb.AppendLine("</tr></tbody>");
                                sb.AppendLine("<!-- chooes -->");

                                genelToplam += Convert.ToInt32(dtSiparisDetayTTV2.Rows[i]["toplam_tutar"]);

                            }
                            else
                            {
                                //dtSiparisDetayTT.Rows[i]["sira_no"].ToString()
                                sb.AppendLine("<!-- chooes -->");
                                sb.AppendLine("<tbody><tr>");
                                sb.AppendLine("<td>" + (i + 1).ToString() + "</td>");
                                sb.AppendLine("<td>" + dtSiparisDetayTTV2.Rows[i]["urun_ad"].ToString() + "</td>");
                                sb.AppendLine("<td>" + dtSiparisDetayTTV2.Rows[i]["siparis_adet"].ToString() + "</td>");
                                sb.AppendLine("<td>" + (Convert.ToInt32(dtSiparisDetayTTV2.Rows[i]["birim_fiyat"])).ToString("###,###,###") + " PUAN</td>");
                                sb.AppendLine("<td>" + (Convert.ToInt32(dtSiparisDetayTTV2.Rows[i]["toplam_tutar"])).ToString("###,###,###") + " PUAN</td>");
                                sb.AppendLine("</tr></tbody>");
                                sb.AppendLine("<!-- chooes -->");

                                genelToplam += Convert.ToInt32(dtSiparisDetayTTV2.Rows[i]["toplam_tutar"]);
                            }
                        }
                    }
                }

                ltrSiparisDetaylari.Text = sb.ToString();



                ltrGenelToplam.Text = Convert.ToInt32(genelToplam).ToString("###,###,###");

                string ToplamTutar = dtSiparisMasterTT.Rows[0]["siparis_tutar"].ToString().Replace(".", ",");
                string Lira;
                string Kurus;

                if (ToplamTutar.IndexOf(",") > -1)
                {
                    Lira = ToplamTutar.Split(',')[0];
                    Kurus = ToplamTutar.Split(',')[1];
                    //ltrYaziyla.Text = sayiOkuma.oku(Lira) + " Türk Lirası " + sayiOkuma.oku(Kurus) + " Kuruş";
                }
                else
                {
                    //ltrYaziyla.Text = sayiOkuma.oku(ToplamTutar) + " Türk Lirası";
                }

                GenelToplam = dtSiparisMasterTT.Rows[0]["siparis_tutar"].ToString().Replace(",", ".");




            }
            else
                Response.Redirect("~/Default.aspx", false);
        }

        //else
        //    Response.Redirect("~/Odeme.aspx", false);

    }
    [System.Web.Services.WebMethod(enableSession: true)]
    public static string SiparisMailGonder()
    {
        Kullanici kullanici = new Kullanici();
        kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];
        if (HttpContext.Current.Session["SiparisMasterId"] != null)
        {
            string _SiparisMasterId = HttpContext.Current.Session["SiparisMasterId"].ToString();
            string[] _SiparisMasterIdArr = _SiparisMasterId.Split(',');
            foreach (string item in _SiparisMasterIdArr)
            {
                int SiparisMasterId = 0;
                int.TryParse(item, out SiparisMasterId);
                if (SiparisMasterId > 0)
                {

                    SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
                    DataSet dsSiparisVerileri = bllSiparisGoruntuleme.SiparisGoruntule(SiparisMasterId, kullanici.ErpCariId);
                    SiparisOnayMailYolla(dsSiparisVerileri, SiparisMasterId);


                   


                }
            }
        }

        return "1";
    }
    protected void btnOnayla_Click(object sender, EventArgs e)
    {
        chkUyari.Text = "";
        if (!chkUygulamaKosullar.Checked)
        {
            chkUyari.Text = "<font color='red'>* Lütfen Uygulama ve Koşullarını kabul ediniz.</font>";
            chkUyari.Focus();

            Swal.Message(this, "info", "Uygulama Koşulları", "Lütfen Uygulama ve Koşullarını kabul ediniz.");
            return;
        }



        DateTime siparisTarihi = DateTime.Parse(Session["SiparisTarihi"].ToString()).AddMinutes(5);
        if (DateTime.Now > siparisTarihi)
        {
            Swal.Message(this, "info", "", "Sipariş onay zamanı geçtiği için lütfen tekrar sipariş geçiniz.");
            return;
        }

        Kullanici _Kullanici = (Kullanici)Session["Kullanici"];

        lock (lockObject)
        {
            int SiparisMasterId = 0;
            if (Session["SiparisMasterId"] != null)
            {
                string _SiparisMasterId = Session["SiparisMasterId"].ToString();
                string[] _SiparisMasterIdArr = _SiparisMasterId.Split(',');
                foreach (string item in _SiparisMasterIdArr)
                {
                    SiparisMasterId = 0;
                    int.TryParse(item, out SiparisMasterId);
                    if (SiparisMasterId > 0)
                    {
                        int TotalPoint = 0;
                        if (Session["SiparisMasterId"] != null)
                        {

                            SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
                            DataSet dsSiparisVerileri = bllSiparisGoruntuleme.SiparisGoruntule(SiparisMasterId, kullanici.ErpCariId);
                            DataTable dtSiparisDetayTT = dsSiparisVerileri.Tables["SiparisDetayTT"];

                            if (SiparisMasterId > 0)
                            {
                                LogIslemleriBLL bllLogIslemleri = new LogIslemleriBLL();
                                bllLogIslemleri.KullaniciSiparisLogKayit(kullanici.KullaniciId, kullanici.UyelikTipi, SiparisMasterId, string.Format("{0} nolu sipariş için kullanıcı onay işlemini başlattı.", SiparisMasterId), "");
                                int webServiceErrorCount = 0;
                                if (dsSiparisVerileri != null)
                                {
                                    if (dsSiparisVerileri != null && dsSiparisVerileri.Tables["SiparisMasterTT"].Rows.Count > 0)
                                    {
                                        DataTable dtSiparisAdresTT = dsSiparisVerileri.Tables["SiparisAdresTT"];
                                        DataTable dtSiparisOdemeTT = dsSiparisVerileri.Tables["SiparisOdemeTT"];
                                        DataTable dtSiparisMasterTT = dsSiparisVerileri.Tables["SiparisMasterTT"];

                                        if (dtSiparisAdresTT != null && dtSiparisAdresTT.Rows.Count > 0)
                                        {
                                            TotalPoint = int.Parse(dtSiparisMasterTT.Rows[0]["siparis_tutar"].ToString().Replace(",", "."));
                                            string outOnayDurum1 = string.Empty;
                                            string outOnayDurum2 = string.Empty;
                                            bllSiparisGoruntuleme.SiparisOnayDurum(SiparisMasterId, "Bekliyor", "Bekliyor", out outOnayDurum1, out outOnayDurum2);

                                            if (dtSiparisDetayTT.Rows.Count > 0 && outOnayDurum1 == "Bekliyor")
                                            {
                                                bllLogIslemleri.KullaniciPuanLogKayit(_Kullanici.KullaniciId.ToString(), _Kullanici.totalGoldPoint, 0, SiparisMasterId, "Sipariş Onay", false);
                                                Kullanici Kullanici = (Kullanici)Session["Kullanici"];
                                                string _onayDurum1 = string.Empty;
                                                string _onayDurum2 = string.Empty;
                                                bllSiparisGoruntuleme.SiparisOnayDurum(SiparisMasterId, "Bekliyor", "Bekliyor", out _onayDurum1, out _onayDurum2);


                                                DataRow dr = dtSiparisDetayTT.Rows[0];
                                                Int32 UrunId = Convert.ToInt32(dr["urun_id"]);
                                                string UrunAd = dr["urun_ad"].ToString();

                                                int OrderPoint = int.Parse("-" + TotalPoint);
                                                //bool sonuc = _BigesOperation.HareketEkle(int.Parse(Kullanici.CRMCustomerGUID), OrderPoint, "Yeni Sipariş", SiparisMasterId.ToString() + " nolu sipariş oluşturuldu.");
                                                //bool sonuc = _BigesOperation.HareketEkle(int.Parse(Kullanici.CRMCustomerGUID), OrderPoint, -1, SiparisMasterId, UrunId, UrunAd);
                                                int sonuc = transactionBLL.Insert(Kullanici.KullaniciId, "", SiparisMasterId + " nolu siparişe istinaden düşülen puan.", OrderPoint, Goldb2cEntity.Custom.Enums.Transaction.TransactionType.OrderMinusPoint, Goldb2cEntity.Custom.Enums.Generic.Status.Active, DateTime.Now);
                                                bllLogIslemleri.KullaniciSiparisLogKayit(kullanici.KullaniciId, kullanici.UyelikTipi, SiparisMasterId, string.Format("{0} nolu sipariş için webservisten puan düşürüldü.", SiparisMasterId), "");

                                                if (sonuc > 0)
                                                {
                                                    bllSiparisGoruntuleme.SiparisOnay(SiparisMasterId, "Onaylandı", "Onaylandı", out SiparisMasterId);
                                                    bllLogIslemleri.KullaniciSiparisLogKayit(kullanici.KullaniciId, kullanici.UyelikTipi, SiparisMasterId, string.Format("{0} nolu sipariş onaylandı.", SiparisMasterId), "");
                                                }
                                                if (_onayDurum1 == "Bekliyor")
                                                {
                                                    // Response.Redirect("~/SiparisOnay.aspx", false);
                                                }

                                            }
                                        }
                                    }
                                }


                            }
                        }
                    }
                }
            }


        }
        Session["totalGoldPoint"] = Helper.PuanSorgula();
        tamamdiv.Visible = true;
        onaydiv.Visible = false;
        btnGonder.Visible = false;
        Response.Redirect("~/SiparisOnay.aspx?status=1", false);
    }

    private void SendSMSContact(string mobilePhone, string name, string surname, string messagetext)
    {
    }

    private static void SiparisOnayMailYolla(DataSet dsSiparisVerileri, int SiparisMasterId)
    {
        Kullanici kullanici = new Kullanici();
        kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];
        string strHTML = File.ReadAllText(HttpContext.Current.Server.MapPath("~/MailSablon/SiparisOnay.htm"));
        StringBuilder sbMailUrunListe = new StringBuilder();

        DataTable dtSiparisDetayTT = dsSiparisVerileri.Tables["SiparisDetayTT"];
        DataTable dtSiparisOdemeTT = dsSiparisVerileri.Tables["SiparisOdemeTT"];
        DataTable dtSiparisAdresTT = dsSiparisVerileri.Tables["SiparisAdresTT"];
        DataTable dtSiparisMasterTT = dsSiparisVerileri.Tables["SiparisMasterTT"];

        for (int i = 0; i < dtSiparisDetayTT.Rows.Count; i++)
        {
            if (i % 2 == 0)
            {
                sbMailUrunListe.AppendLine("<tr>");
                sbMailUrunListe.AppendLine("<td align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #E8E8E8; text-align: center;\">");
                sbMailUrunListe.AppendLine((i + 1).ToString());
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"303\" align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px;line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #E8E8E8;\">");
                sbMailUrunListe.AppendLine(dtSiparisDetayTT.Rows[i]["urun_ad"].ToString());
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"39\" align=\"center\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #E8E8E8; text-align: center;\">");
                sbMailUrunListe.AppendLine(dtSiparisDetayTT.Rows[i]["siparis_adet"].ToString());
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"147\" align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #E8E8E8; text-align: right;\">");
                sbMailUrunListe.AppendLine(dtSiparisDetayTT.Rows[i]["birim_fiyat"].ToString() + " PUAN");
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"148\" align=\"right\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #E8E8E8; text-align: right;\">");
                sbMailUrunListe.AppendLine(dtSiparisDetayTT.Rows[i]["toplam_tutar"].ToString() + " PUAN");
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("</tr>");
            }
            else
            {
                sbMailUrunListe.AppendLine("<tr>");
                sbMailUrunListe.AppendLine("<td align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #F2F2F2; text-align: center;\">");
                sbMailUrunListe.AppendLine((i + 1).ToString());
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"303\" align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px;line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #F2F2F2;\">");
                sbMailUrunListe.AppendLine(dtSiparisDetayTT.Rows[i]["urun_ad"].ToString());
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"39\" align=\"center\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #F2F2F2; text-align: center;\">");
                sbMailUrunListe.AppendLine(dtSiparisDetayTT.Rows[i]["siparis_adet"].ToString());
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"147\" align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #F2F2F2; text-align: right;\">");
                sbMailUrunListe.AppendLine(dtSiparisDetayTT.Rows[i]["birim_fiyat"].ToString() + " PUAN");
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"148\" align=\"right\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #F2F2F2; text-align: right;\">");
                sbMailUrunListe.AppendLine(dtSiparisDetayTT.Rows[i]["toplam_tutar"].ToString() + " PUAN");
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("</tr>");
            }
        }
        int genelToplam = 0;
        for (int i = 0; i < dtSiparisDetayTT.Rows.Count; i++)
        {
            genelToplam += Convert.ToInt32(dtSiparisDetayTT.Rows[i]["toplam_tutar"]);
        }
        strHTML = strHTML.Replace("#SiparisYazismaUnvan#", dtSiparisAdresTT.Rows[0]["syazisma_unvan"].ToString());
        strHTML = strHTML.Replace("#SiparisAdres#", dtSiparisAdresTT.Rows[0]["sadres_tanim"].ToString());
        strHTML = strHTML.Replace("#SiparisIlce#", dtSiparisAdresTT.Rows[0]["silce_kod"].ToString());
        strHTML = strHTML.Replace("#SiparisSehir#", dtSiparisAdresTT.Rows[0]["ssehir_kod"].ToString());
        strHTML = strHTML.Replace("#SiparisCepTel#", dtSiparisAdresTT.Rows[0]["smobil_telefon"].ToString());
        strHTML = strHTML.Replace("#SiparisSabitTel#", dtSiparisAdresTT.Rows[0]["ssabit_telefon"].ToString());

        strHTML = strHTML.Replace("#SiparisTarih#", Convert.ToDateTime(dtSiparisMasterTT.Rows[0]["siparis_tarih"]).ToString("dd/MM/yyyy"));
        strHTML = strHTML.Replace("#SiparisSaat#", dtSiparisMasterTT.Rows[0]["siparis_saat"].ToString().Substring(0, 2) + ":" + dtSiparisMasterTT.Rows[0]["siparis_saat"].ToString().Substring(2, 2));
        strHTML = strHTML.Replace("#SiparisMasterId#", dtSiparisMasterTT.Rows[0]["siparis_no"].ToString());
        strHTML = strHTML.Replace("#Kargo#", dtSiparisMasterTT.Rows[0]["kargo_kod"].ToString());

        strHTML = strHTML.Replace("#UrunListe#", sbMailUrunListe.ToString());
        strHTML = strHTML.Replace("#GenelToplam#", Convert.ToInt32(genelToplam).ToString("###,###,###"));

        //string MesafeliSatisSozlesmesi = SozlesmeIcerikGetir(@"http://" + Request.Url.Authority + "/Sozlesmeler/MesafeliSatis.aspx?OdemeTur=" + OdemeTur + "&SiparisMasterId=" + SiparisMasterId.ToString());
        //string OnBilgilendirmeFormu = SozlesmeIcerikGetir(@"http://" + Request.Url.Authority + "/Sozlesmeler/OnBilgilendirme.aspx?SiparisMasterId=" + SiparisMasterId.ToString());

        //strHTML = strHTML.Replace("#MesafeliSatisSozlesmesi#", MesafeliSatisSozlesmesi);
        //strHTML = strHTML.Replace("#OnBilgilendirmeFormu#", OnBilgilendirmeFormu);

        string MailBanner = "";

        DataTable dtBanner = new DataTable();
        BannerBLL bllBanner = new BannerBLL();
        dtBanner = bllBanner.GetirBanner(3);
        if (dtBanner != null && dtBanner.Rows.Count > 0)
        {
            MailBanner = dtBanner.Rows[0]["BannerHtml"].ToString();
        }

        string[] _MailBanner = MailBanner.Split(new string[] { "##" }, StringSplitOptions.None);
        string BannerImage = _MailBanner[0];
        string BannerLink = _MailBanner[0];

        StringBuilder sb = new StringBuilder();
        sb.AppendLine("<tr>");
        sb.AppendLine("<td colspan=\"4\" width=\"720\" height=\"120\">");
        sb.AppendLine("<div style=\"height: 120px\">");
        if (!string.IsNullOrEmpty(BannerLink))
            sb.AppendLine("<a href=\"" + BannerLink + "\">");
        sb.AppendLine("<img alt=\"kampanya\" src=\"http://www.gold.com.tr/UrunResim/MarketingResim/banner/bilgimail/" + BannerImage + "\" style=\"border:none\"  />");
        if (!string.IsNullOrEmpty(BannerLink))
            sb.AppendLine("</a>");
        sb.AppendLine("</div>");
        sb.AppendLine("</td>");
        sb.AppendLine("</tr>");

        //if (string.IsNullOrEmpty(MailBanner))
        //    strHTML = strHTML.Replace("#mailbanner#", "");
        //else
        //    strHTML = strHTML.Replace("#mailbanner#", sb.ToString());

        SendEmail mail = new SendEmail();
        mail.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
        mail.MailTo = new string[] { kullanici.Email };
        //mail.MailBcc = new string[] { "burcu.acikel@tutkal.com.tr", "ozlem.canturk@tutkal.com.tr", "guven.erden@tutkal.com.tr" };
        mail.MailHtml = true;
        mail.MailSubject = "motulteam.com Siparişiniz Tamamlanmıştır";

        mail.MailBody = strHTML;
        bool mailSent = mail.SendMail(false);



        DataRow[] rows = dtSiparisDetayTT.Select("urun_id in (20043118,20043119,20043120,20043121,20043122,20043123,20043124)");
        if (rows.Count() > 0)
        {
            SendEmail mailSatialma = new SendEmail();
            mailSatialma.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
            mailSatialma.MailTo = new string[] { "fulya.akcora@tutkal.com.tr", "huseyin.altunbas@tutkal.com.tr", "burcu.acikel@tutkal.com.tr", "ozlem.canturk@tutkal.com.tr", "guven.erden@tutkal.com.tr" };

            mailSatialma.MailHtml = true;
            mailSatialma.MailSubject = "motulteam.com Altın Siparişi Var";

            mailSatialma.MailBody = strHTML;
            bool mailSendSatianlama = mailSatialma.SendMail(false);
        }


        //Ödemesi tamamlanmış siparişleri Admin kullanıcılarına da gönder.
        //SendEmail mail2 = new SendEmail();
        //mail2.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
        ////mail2.MailTo = new string[] { "huseyin.altunbas@tutkal.com.tr" };
        //mail2.MailTo = new string[] { "tutkaloperasyon@tutkal.com.tr" };
        //mail2.MailBcc = new string[] { "baris.ersan@tutkal.com.tr", "fulya.akcora@tutkal.com.tr" };
        ////01.12.2017
        ////mail2.MailBcc = new string[] { "fulya.akcora@tutkal.com.tr", "bilge.aslan@tutkal.com.tr", "baris.ersan@tutkal.com.tr", "huseyin.altunbas@tutkal.com.tr" };
        ////01.12.2017
        //mail2.MailHtml = true;
        //mail2.MailSubject = "motulteam.com Siparişiniz Tamamlanmıştır";

        //mail2.MailBody = strHTML;
        //bool sonuc = mail2.SendMail(false);

    }


    protected void CustomValidatorSozlesme_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = (chkUygulamaKosullar.Checked);
    }

}

//public class SayiOkuma
//{
//    private string[] yuzler;
//    private string[] onlar;
//    private string[] birler;
//    private string[] hane;
//    private string[] rakam;
//    // arrayları tanımlıyoruz
//    public SayiOkuma()
//    {
//        yuzler = new string[10];
//        onlar = new string[10];
//        birler = new string[10];
//        hane = new string[5];
//        rakam = new string[5];

//        // içlerini dolduruyoruz
//        yuzler.SetValue("dokuzyüz", 9);
//        yuzler.SetValue("sekizyüz", 8);
//        yuzler.SetValue("yediyüz", 7);
//        yuzler.SetValue("altıyüz", 6);
//        yuzler.SetValue("beşyüz", 5);
//        yuzler.SetValue("dörtyüz", 4);
//        yuzler.SetValue("üçyüz", 3);
//        yuzler.SetValue("ikiyüz", 2);
//        yuzler.SetValue("yüz", 1);
//        yuzler.SetValue("", 0);

//        onlar.SetValue("doksan", 9);
//        onlar.SetValue("seksen", 8);
//        onlar.SetValue("yetmiş", 7);
//        onlar.SetValue("altmış", 6);
//        onlar.SetValue("elli", 5);
//        onlar.SetValue("kırk", 4);
//        onlar.SetValue("otuz", 3);
//        onlar.SetValue("yirmi", 2);
//        onlar.SetValue("on", 1);
//        onlar.SetValue("", 0);
//        birler.SetValue("dokuz", 9);
//        birler.SetValue("sekiz", 8);
//        birler.SetValue("yedi", 7);
//        birler.SetValue("altı", 6);
//        birler.SetValue("beş", 5);
//        birler.SetValue("dört", 4);
//        birler.SetValue("üç", 3);
//        birler.SetValue("iki", 2);
//        birler.SetValue("bir", 1);
//        birler.SetValue("", 0);


//    /*  ilk olarak bu arrayın elemanlarını boş olarak ayarlıyoruz eğer küme elemanları
//        000 değilse trilyon,milyar,milyon bin değerleri ile dolduruyoruz
//        */
//    }
//    public string oku(string sayi)
//    {
//        hane.SetValue("", 0);
//        hane.SetValue("", 1);
//        hane.SetValue("", 2);
//        hane.SetValue("", 3);
//        hane.SetValue("", 4);

//        int uzunluk = sayi.Length;
//        if (uzunluk > 15)
//            return "Hata girilen değerin uzunluğu en fazla 15 olmalı";
//        // uzunluk 15 karakterden fazla olmamalı. si
//        try
//        {
//            long k = Convert.ToInt64(sayi);
//        }
//        catch (Exception ex)
//        {
//            return ex.Message.ToString();
//        }
//        sayi = "000000000000000" + sayi;
//        sayi = sayi.Substring(uzunluk, 15);
//        rakam.SetValue(sayi.Substring(0, 3), 0);
//        rakam.SetValue(sayi.Substring(3, 3), 1);
//        rakam.SetValue(sayi.Substring(6, 3), 2);
//        rakam.SetValue(sayi.Substring(9, 3), 3);
//        rakam.SetValue(sayi.Substring(12, 3), 4);
//        if (rakam[0].ToString() != "000")
//            hane.SetValue("trilyon", 0);
//        if (rakam[1].ToString() != "000")
//            hane.SetValue("milyar", 1);
//        if (rakam[2].ToString() != "000")
//            hane.SetValue("milyon", 2);
//        if (rakam[3].ToString() != "000")
//            hane.SetValue("bin", 3);
//        string sonuc = "";
//        for (int i = 0; i < 5; i++)
//        {
//            sonuc = sonuc + yuzler[Convert.ToInt16(rakam[i][0].ToString())] + birsorunu(onlar[Convert.ToInt16(rakam[i][1].ToString())] + birler[Convert.ToInt16(rakam[i][2].ToString())] + hane[i]);
//        }
//        return sonuc;
//    }
//    private string birsorunu(string sorun)
//    {
//        string cozum = "";
//        if (sorun == "birbin")
//            cozum = "bin";
//        else
//            cozum = sorun;
//        return cozum;
//    }

//}

