﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="Menu.aspx.cs" Inherits="Menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .product-container {
            min-height: 200px !important;
        }
        .product-list li .left-block {
            height: 150px !important;
        }
        .product-list li .left-block img {
            max-height: 140px !important;
        }
        .product-list li .right-block {
            height: 30px !important;
        }
    </style>
    <div class="columns-container">
        <div class="container" id="columns">
            <div class="breadcrumb clearfix">
                <asp:Literal ID="ltrAyakizi" runat="server"></asp:Literal>
            </div>
            <div class="row">
                <div class="center_column col-xs-12" id="center_column">
                    <div id="view-product-list" class="view-product-list">
                        <h2 class="page-heading">
                            <span class="page-heading-title">
                                <asp:Label ID="lblMenuAd" runat="server" Text=""></asp:Label></span>
                        </h2>
                        <ul class="row product-list grid">
                            <asp:Repeater ID="rptMenu" runat="server">
                                <ItemTemplate>
                                    <li class="col-xs-12 col-sm-3 col-md-2">
                                        <div class="product-container">
                                            <div class="left-block">
                                                <%#DataBinder.Eval(Container.DataItem, "CategoryImage")%>
                                            </div>
                                            <div class="right-block">
                                                <h5 class="product-name">
                                                    <%#DataBinder.Eval(Container.DataItem, "CategoryName")%>
                                                </h5>
                                            </div>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
