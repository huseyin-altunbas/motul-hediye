(function($){  
  

	$.fn.showcase = function(options){
		
		var settings = $.extend({}, $.fn.showcase.defaults, options);
		
        var _this = $(this);

		var blocks = {
            wrapper: '<div class="sc-wrapper" />',
            controls: '<div class="sc-controls next">Next</div><div class="sc-controls prev">Previous</div>'
        };
		var li_width, li_height, ul_width, ul_height, elements;
		
        var isMoving = false; 

		var make = function()
        {
            var timer, timer_on_blur;

			elements = $('li',_this);

            _this.wrap(blocks.wrapper).addClass('sc').find('li').addClass('sc-block-holder');

            li_width = $('li:first-child',_this).innerWidth();
            li_height = $('li:first-child',_this).innerHeight();


            ul_width = li_width*settings.columns;
            ul_height = li_height*settings.rows; 

            _this
                .css({'height': ul_height+'px','width': ul_width+'px'})
                .parent('div')
                .css({'height': ul_height+'px','width': ul_width+'px'});
            
            if(settings.arrowsNav == true) {
                _this.after(blocks.controls);

                _controls = $(_this).parent('div').find('.sc-controls');
                _next = $(_this).parent('div').find('.sc-controls.next');
                _prev = $(_this).parent('div').find('.sc-controls.prev');
            }

            $.fn.showcase.setpositions(_this, settings, li_width, li_height);
            
            if(settings.autoStart == true){
                timer = setInterval(function(){
                    switch(settings.direction){
                        case 'horizontal':
                            $.fn.showcase.move(settings, _this, 'right', isMoving, li_width, li_height);
                            break;
                        case 'vertical':
                            $.fn.showcase.move(settings, _this, 'down', isMoving, li_width, li_height);
                            break;
                    }
                },settings.pauseTime);
            }

            if(settings.autoStart == true && settings.pauseOnHover == true ){
                _this.hover(function(){ 
                    clearInterval(timer); 
                },function(){ 
                    timer = setInterval(function(){
                        switch(settings.direction){
                            case 'horizontal':
                                $.fn.showcase.move(settings, _this, 'right', isMoving, li_width, li_height);
                                break;
                            case 'vertical':
                                $.fn.showcase.move(settings, _this, 'down', isMoving, li_width, li_height);
                                break;
                        }
                    },settings.pauseTime);
                });

                if(settings.arrowsNav == true){
                    _controls.hover(function(){ clearInterval(timer); },function(){ 
                        timer = setInterval(function(){
                            switch(settings.direction){
                                case 'horizontal':
                                    $.fn.showcase.move(settings, _this, 'right', isMoving, li_width, li_height);
                                    break;
                                case 'vertical':
                                    $.fn.showcase.move(settings, _this, 'down', isMoving, li_width, li_height);
                                    break;
                            }
                        },settings.pauseTime); 
                    });
                }
            }

            if(settings.arrowsNav == true){
                _next.bind('click',function(){
                    switch(settings.direction){
                        case 'horizontal':
                            $.fn.showcase.move(settings, _this, 'left', isMoving, li_width, li_height);
                            break;
                        case 'vertical':
                            $.fn.showcase.move(settings, _this, 'up', isMoving, li_width, li_height);
                            break;
                    }
                });

                _prev.bind('click',function(){
                    switch(settings.direction){
                        case 'horizontal':
                            $.fn.showcase.move(settings, _this, 'right', isMoving, li_width, li_height);
                            break;
                        case 'vertical':
                            $.fn.showcase.move(settings, _this, 'down', isMoving, li_width, li_height);
                            break;
                    }   
                });
            }
        };

        return this.each(make);
    };

    $.fn.showcase.popout = function(_this){
        var x = $('li',_this).eq(0).clone();
        $('li',_this).eq(0).remove();
        _this.append( x );
    };

    $.fn.showcase.popin = function(x, direction, elements, _this, li_width, li_height){

        var last_element = elements.length;

        for(x; x>0; x--){

            var elt = $('li',_this).eq(last_element-1).clone();
            
            switch(direction){
                case 'vertical':
                    var left = li_width*(x-1);
                    var top = li_height;
                    elt.css({'top': '-'+top+'px','left':left+'px'});
                    break;

                case 'horizontal':
                    var left = li_width;
                    var top = li_height*(x-1);
                    elt.css({'top': top+'px','left':'-'+left+'px'});
                    break;
            }

            $('li',_this).eq(last_element-1).remove();
            _this.prepend( elt );
        }
    }

    $.fn.showcase.setpositions = function(_this, settings, li_width, li_height){
        elements = $('li',_this);

        var w = settings.columns;
        var h = settings.rows;
      
        var c = r = o = 1; // colums, row, block offset
       
        switch(settings.direction){
            case 'horizontal':
                for(var i = 0; i < elements.length; i++){
                    var left = li_width*(c-1);
                    var top = li_height*(r-1);

                    $(elements[i]).css({'top': top+'px','left': left+'px'});                
                    
                    if( r == h ) {
                        r=0; c++;
                    }
                    r++; 
                }
                break;
            
            case 'vertical':
                for(var i = 0; i < elements.length; i++){
                    var left = li_width*(c-1);
                    var top = li_height*(r-1);

                    $(elements[i]).css({'top': top+'px','left': left+'px'});                
                    
                    if( c == w ) {
                        r++; c=0;
                    }
                    c++; 
                }
                break;
        }
        
    };

    $.fn.showcase.move = function(settings, _this, dir, isMoving, li_width, li_height){
        var elements = $('li',_this);

        if(isMoving == true) return false;
        else {
            isMoving = true;
            
            switch(dir){
                case 'up':
                    $.fn.showcase.setpositions(_this, settings, li_width, li_height);
                    var popout = $.fn.showcase.popout;
                    setTimeout(function(){
                        for(var i = 0; i<elements.length; i++){
                            (function(i,elements){ 
                                setTimeout(function(){ 
                                    $(elements[i]).animate({'top': '-='+li_height+'px'}, settings.animSpeed, settings.easingEfect, function(){
                                        if( i < settings.columns ) popout(_this);
                                    });
                                }, settings.delay*i)
                            })(i,elements);
                        }
                    },10);
                   
                    break;

                case 'down':
                    $.fn.showcase.popin(settings.columns, settings.direction, elements, _this, li_width, li_height);
                    elements = $('li',_this);
                    setTimeout(function(){
                        var j = 0;
                        var t = (settings.rows+1)*settings.columns;
                        var i = elements.length;


                        for(i; i>=0; i--){
                            if( i < t ){
                                (function(i,elements,j,t){ 
                                    setTimeout(function(){ 
                                        $(elements[i]).animate({'top': '+='+li_height+'px'}, settings.animSpeed, settings.easingEfect);
                                    }, settings.delay*j)
                                })(i,elements,j,t);
                                j++;
                            }
                        }
                    },10);

                    break;

                case 'left':
                    $.fn.showcase.setpositions(_this,settings, li_width, li_height);
                    var popout = $.fn.showcase.popout;
                    setTimeout(function(){
                        for(var i = 0; i<elements.length; i++){
                            (function(i,elements){ 
                                setTimeout(function(){ 
                                    $(elements[i]).animate({'left': '-='+li_width+'px'}, settings.animSpeed, settings.easingEfect, function(){
                                        if( i < settings.rows ) popout(_this);
                                    });
                                }, settings.delay*i)
                            })(i,elements);
                        }
                    },10);
                   
                    break;

                case 'right':
                    $.fn.showcase.popin(settings.rows, settings.direction, elements, _this, li_width, li_height);
                    elements = $('li',_this);
                    setTimeout(function(){
                        var j = 0;
                        var t = (settings.columns+1)*settings.rows;

                        for(var i = elements.length; i>=0; i--){
                            if( i > t ) j = 0;

                            (function(i,elements,j,t){ 
                                setTimeout(function(){ 
                                    $(elements[i]).animate({'left': '+='+li_width+'px'}, settings.animSpeed, settings.easingEfect);
                                }, settings.delay*j)
                            })(i,elements,j,t);
                            j++;
                        }
                    },10);

                    break;
            }

            setTimeout(function(){
                isMoving = false;
            },settings.delay*elements.length);
        }
        
    };

    $.fn.showcase.defaults = {
        animSpeed:550,
        autoStart:true,
        pauseTime:3000,
        arrowsNav:true,
        arrowsNavHide:true,
        columns: 5,
        rows: 3,
        delay: 35,
        direction: 'vertical', //vertical, horizontal
        pauseOnHover: true,
        easingEfect: 'swing'
    };
    
})(jQuery);  