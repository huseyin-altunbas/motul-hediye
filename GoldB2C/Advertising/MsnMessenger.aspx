﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MsnMessenger.aspx.cs" Inherits="Advertising_MsnMessenger"
    EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
    <title>Alçıkart - Türkiye'nin En Büyük Online Alışveriş Sitesi</title>
    <link rel="stylesheet" media="all" href="css/style.css?v=1.0.1.3.6" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/Advertising/js/showcase/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="/Advertising/js/showcase/jquery.showcase-1.0.js"></script>
    <script type="text/javascript" src="/Advertising/js/highlight/highlight.pack.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#showcase').showcase({
                autoStart: <%=AutoStartAndNav %>,
                arrowsNav: <%=AutoStartAndNav %>,
                direction: 'horizontal',
                delay: 45,
                rows: 2,
                columns: 3,
                easingEfect: 'easeInOutQuart'
            });
        });

        hljs.tabReplace = '    ';
        hljs.initHighlightingOnLoad();
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="advertising">
        <div class="header">
            <a href="https://motulteam.com/" title="anadoludanaspara" class="logo">
                <img src="images/logo.png" alt="Alçıkart" style="border: none;" />
            </a>
            <asp:Image ID="imgBaslik" runat="server" AlternateText="Fırsatın Farkında mısın?" />
        </div>
        <!-- REKLAM -->
        <div class="l-full canvas">
            <div class="shadow">
                <div class="l-full-inner b-holder" id="showcase-example-one-wrapper">
                    <ul id="showcase">
                        <asp:Literal ID="ltrAdvertising" runat="server"></asp:Literal>
                    </ul>
                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">
        </div>
        <!-- REKLAM -->
    </div>
    </form>
    <script type="text/javascript">
        function AdvertisingDetayLog(AdvertisingDetayId) {
            $.ajax({
                type: 'POST',
                url: '/Advertising/MsnMessenger.aspx/AdvertisingDetayLog',
                data: '{ "AdvertisingDetayId" : "' + AdvertisingDetayId + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {

                },
                error: function (err) {
                }
            });

        }
    </script>
</body>
</html>
