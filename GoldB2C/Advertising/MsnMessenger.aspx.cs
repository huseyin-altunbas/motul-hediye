﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using System.Text;
using System.Web.Services;

public partial class Advertising_MsnMessenger : System.Web.UI.Page
{
    public string AutoStartAndNav = "false";
    protected void Page_Load(object sender, EventArgs e)
    {
        AdvertisingBLL bllAdvertising = new AdvertisingBLL();

        if (!IsPostBack)
        {
            bllAdvertising.KaydetAdvertisingLog(1, UyelikIslemleriBLL.FindIP(HttpContext.Current.Request.Headers));

            DataTable dtAdvertising = new DataTable();
            dtAdvertising = bllAdvertising.GetirAdvertisingDetay(1, true);
            if (dtAdvertising != null && dtAdvertising.Rows.Count > 0)
            {
                if (dtAdvertising.Rows.Count > 6)
                {
                    AutoStartAndNav = "true";
                    imgBaslik.ImageUrl = "~/Advertising/images/firsatin-farkinda-misin-1.png";
                    imgBaslik.CssClass = "advertisingTitle";
                }
                else
                {
                    imgBaslik.ImageUrl = "~/Advertising/images/firsatin-farkinda-misin-2.png";
                    imgBaslik.CssClass = "advertisingTitle2";
                }

                StringBuilder sbAdvertising = new StringBuilder();
                int index = 0;
                foreach (DataRow dr in dtAdvertising.Rows)
                {

                    if (index % 2 != 0)
                        sbAdvertising.AppendLine("<li>");
                    else
                        sbAdvertising.AppendLine("<li style=\"*padding-bottom:7px\">");

                    if (string.IsNullOrEmpty(dr["GorselUrl"].ToString().Trim()))
                    {
                        sbAdvertising.AppendLine("<div class=\"sc-block\">");
                        sbAdvertising.AppendLine("<a onclick=\"AdvertisingDetayLog(" + dr["AdvertisingDetayId"].ToString() + ");\" href=\"" + dr["UrunLink"].ToString() + "\" target=\"_blank\" class=\"imgDiv\"><img src=\"" + dr["UrunResim"].ToString() + "\" /></a>");
                        sbAdvertising.AppendLine("<a onclick=\"AdvertisingDetayLog(" + dr["AdvertisingDetayId"].ToString() + ");\" href=\"" + dr["UrunLink"].ToString() + "\" target=\"_blank\" class=\"productName\">" + dr["UrunAd"].ToString() + "</a>");
                        sbAdvertising.AppendLine("<a onclick=\"AdvertisingDetayLog(" + dr["AdvertisingDetayId"].ToString() + ");\" href=\"" + dr["UrunLink"].ToString() + "\" target=\"_blank\" class=\"productPrice\"><span class=\"price\">" + Math.Round(Convert.ToDecimal(dr["UrunFiyat"])) + " TL</span> " + Math.Round(Convert.ToDecimal(dr["UrunIndFiyat"])) + "<span class=\"turkishLira\">TL</span></a>");
                        sbAdvertising.AppendLine("</div>");

                        /*sbAdvertising.AppendLine("<div class=\"sc-block\"><a href=\"" + dr["UrunLink"].ToString() + "\" target=\"_blank\">");
                        sbAdvertising.AppendLine("<div class=\"imgDiv\"><img style=\"border:none;\" src=\"" + dr["Urunresim"].ToString() + "\" alt=\"" + dr["UrunAd"].ToString() + "\" /></div>");
                        sbAdvertising.AppendLine("<p class=\"productName\">" + dr["UrunAd"].ToString() + "</p>");
                        sbAdvertising.AppendLine("<p class=\"productPrice\"><span class=\"price\">" + Math.Round(Convert.ToDecimal(dr["UrunFiyat"])) + " TL</span> " + dr["UrunIndFiyat"].ToString() + "<span class=\"turkishLira\">TL</span></p>");
                        sbAdvertising.AppendLine("</a>");
                        sbAdvertising.AppendLine("</div>");
                        sbAdvertising.AppendLine("</li>");*/
                    }
                    else
                    {
                        sbAdvertising.AppendLine("<div class=\"sc-block\"><a onclick=\"AdvertisingDetayLog(" + dr["AdvertisingDetayId"].ToString() + ");\" href=\"" + dr["HedefUrl"].ToString() + "\" target=\"_blank\">");
                        sbAdvertising.AppendLine("<img src=\"http://www.motulteam.com/urunresim/marketingresim/Advertising/" + dr["GorselUrl"].ToString() + "\" />");
                        sbAdvertising.AppendLine("</a>");
                        sbAdvertising.AppendLine("</div>");
                    }

                    sbAdvertising.AppendLine("</li>");


                    index++;


                }

                ltrAdvertising.Text = sbAdvertising.ToString();

            }


        }
    }

    [WebMethod]
    public static void AdvertisingDetayLog(string AdvertisingDetayId)
    {
        AdvertisingBLL bllAdvertising = new AdvertisingBLL();
        string IpAdres = UyelikIslemleriBLL.FindIP(HttpContext.Current.Request.Headers);
        int _AdvertisingDetayId = 0;
        if (int.TryParse(AdvertisingDetayId, out _AdvertisingDetayId))
            bllAdvertising.KaydetAdvertisingDetayLog(_AdvertisingDetayId, IpAdres);

    }
}