﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="Kargo.aspx.cs" Inherits="Kargo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/goldBasket.css" rel="stylesheet" type="text/css" />
    <!-- page wapper-->
    <div class="columns-container">
        <div class="container" id="columns">
            <!-- breadcrumb -->
            <div class="breadcrumb clearfix">
                <a class="home" href="/" title="Ana Sayfa">Ana Sayfa</a> <span class="navigation-pipe">
                    &nbsp;</span> <span class="navigation_page">Sepet</span> <span class="navigation-pipe">
                        &nbsp;</span> <span class="navigation_page">Adres</span> <span class="navigation-pipe">
                            &nbsp;</span> <span class="navigation_page">Kargo</span>
            </div>
            <!-- ./breadcrumb -->
            <!-- page heading-->
            <%--<h2 class="page-heading no-line">
                <span class="page-heading-title2">KARGO</span>
            </h2>--%>
            <!-- ../page heading-->
            <div id="addressInfo" class="page-content page-order">
                <ul class="step">
                    <li><span>01. Sepet</span></li>
                    <li><span>02. Adres</span></li>
                    <li class="current-step"><span>03. Kargo</span></li>
                    <li><span>04. Ödeme</span></li>
                    <li><span>05. Onay</span></li>
                </ul>
                <div class="heading-counter warning">
                    KARGO
                </div>
                <div>
                    <asp:Repeater ID="rptKargo" runat="server">
                        <ItemTemplate>
                            <!-- chooes -->
                            <div class="row" style="margin: 0px !important;">
                                <div class="col-xs-2">
                                    <input <%# Container.ItemIndex.ToString() == "0" ? "checked" : "" %> name="rbKargoSecim"
                                        value='<%# DataBinder.Eval(Container.DataItem, "kargo_id") %>' type="radio" />
                                    <%#DataBinder.Eval(Container.DataItem,"kargo_ad") %>
                                </div>
                                <div class="col-xs-3">
                                    <img src="<%#DataBinder.Eval(Container.DataItem,"kargo_imajurl") %>" width="200px"
                                        height="76px" alt="<%#DataBinder.Eval(Container.DataItem,"kargo_ad") %>" />
                                </div>
                                <div class="col-xs-3">
                                    <%#DataBinder.Eval(Container.DataItem,"kargo_fiyat").ToString() != "0" ? DataBinder.Eval(Container.DataItem,"kargo_fiyat") +  " TL" : "Kargo Bedava" %>
                                </div>
                                <div class="col-xs-4">
                                    <%#DataBinder.Eval(Container.DataItem,"kargo_aciklama") %>
                                </div>
                            </div>
                            <!-- chooes -->
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="cargoroot">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="adressinfo">
                                <div class="adressname">
                                    Fatura<br />
                                    Adresi</div>
                                <div style="margin-top: 50px;">
                                    <%=FaturaYazismaUnvan %></div>
                                <div>
                                    <%=FaturaAdres %>
                                    <br />
                                    <%=FaturaTelefon %>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="adressinfo">
                                <div class="adressname">
                                    Teslimat
                                    <br />
                                    Adresi</div>
                                <div style="margin-top: 50px;">
                                    <%=TeslimatYazismaUnvan %></div>
                                <div>
                                    <%=TeslimatAdres %>
                                    <br />
                                    <%=TeslimatTelefon %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shipment_navigation shipmentBar">
                    <asp:LinkButton ID="btnGeri" runat="server" CssClass="prev-btn" OnClick="btnGeri_Click">Geri</asp:LinkButton>
                    <asp:LinkButton ID="btnIleri" runat="server" CssClass="next-btn" OnClick="btnIleri_Click">İleri</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
