﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="KrediKarti.aspx.cs" Inherits="KrediKarti" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/goldBasket.css" rel="stylesheet" type="text/css" />
    <!-- basket container -->
    <div class="containerOut982">
        <div class="containerIn960">
            <!-- basket root -->
            <div class="basketroot">
                <!-- basket navigation -->
                <div class="basketnav">
                    <ul>
                        <li class="menu1"><a class="amenu1 cursordef" href="#">Sepetim</a> </li>
                        <li class="menu2"><a class="amenu2 cursordef" href="#">Teslimat
                            <br />
                            Fatura</a> </li>
                        <li class="menu3"><a class="amenu3 cursordef" href="#">Kargo</a> </li>
                        <li class="menu4 menu4selected real"><a href="#" class="amenu4 selected cursordef">Ödeme
                            <br />
                            Seçenekleri</a></li>
                        <li class="menu5"><a class="amenu5 cursordef" href="#">Ödeme
                            <br />
                            Onay</a> </li>
                    </ul>
                </div>
                <!-- basket navigation -->
                <!-- credit card content -->
                <div class="creditcardcontainer">
                    <h6>
                        Ödeme Seçenekleri</h6>
                    <!-- credit card root -->
                    <div class="creditcardroot">
                        <h5>
                            Kredi Kartı ile Ödeme</h5>
                        <!-- payment price -->
                        <div class="paymentprice">
                            <div class="price1">
                                Sipariş Tutarı: <span class="colord8">2.600,22 PUAN</span></div>
                            <div class="price2">
                                Ödenen Tutar: <span class="colord8">0 PUAN</span></div>
                            <div class="price3">
                                Kalan Tutar: <span class="colord8">2.600,22 PUAN</span></div>
                        </div>
                        <!-- payment price -->
                        <!-- money transfer -->
                        <div class="moneytransfer">
                            <div class="transferlink">
                                <img src="imagesgld/bullet_red6x6.png" width="6" height="6" />
                                <a href="#">Ön bilgilendirme formunu okumak için tıklayın!</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <img src="imagesgld/bullet_red6x6.png" width="6" height="6" />
                                <a href="#">Mesafeli satış sözleşmesini okumak için tıklayın!</a>
                            </div>
                            <div class="transferapproval">
                                <input name="" type="checkbox" value="" />
                                Ön bilgilendirme formu ve mesafeli satış sözleşmesini okudum, kabul ediyorum.
                            </div>
                        </div>
                        <!-- money transfer -->
                        <!-- bank chooes -->
                        <div class="bankchooes">
                            <div class="fleft ml300 mt5 mr5">
                                Banka Adı:</div>
                            <div class="fleft">
                                <asp:DropDownList ID="dpBankalar" CssClass="w185" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="dpBankalar_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="financingoptions">
                            <asp:GridView ID="grdOdemeSecenekleri" runat="server">
                            </asp:GridView>
                        </div>
                        <!-- bank chooes -->
                        <!-- credit card info -->
                        <div class="creditcardinfo">
                            <!-- parameter -->
                            <div class="formparameter">
                                <div class="formlabel">
                                    Kart Üzerindeki Ad-Soyad :
                                </div>
                                <div class="formdata">
                                    <input name="" type="text" class="w210" />
                                </div>
                            </div>
                            <!-- parameter -->
                            <!-- parameter -->
                            <div class="formparameter">
                                <div class="formlabel">
                                    Kart Numarası :
                                </div>
                                <div class="formdata">
                                    <input name="" type="text" class="w210" />
                                </div>
                            </div>
                            <!-- parameter -->
                            <!-- parameter -->
                            <div class="formparameter">
                                <div class="formlabel">
                                    Son Kullanma Tarihi :
                                </div>
                                <div class="formdata">
                                    <select name="" class="w35">
                                        <option selected="selected" value="">G&#252;n</option>
                                        <option value="1">01</option>
                                        <option value="2">02</option>
                                        <option value="3">03</option>
                                        <option value="4">04</option>
                                        <option value="5">05</option>
                                        <option value="6">06</option>
                                        <option value="7">07</option>
                                        <option value="8">08</option>
                                        <option value="9">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select>
                                    <select name="" class="w35">
                                        <option selected="selected" value="">Ay</option>
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                    <select name="" class="w35">
                                        <option selected="selected" value="">Yıl</option>
                                        <option value="2007">2007</option>
                                        <option value="2006">2006</option>
                                        <option value="2005">2005</option>
                                        <option value="2004">2004</option>
                                        <option value="2003">2003</option>
                                        <option value="2002">2002</option>
                                        <option value="2001">2001</option>
                                        <option value="2000">2000</option>
                                        <option value="1999">1999</option>
                                        <option value="1998">1998</option>
                                        <option value="1997">1997</option>
                                        <option value="1996">1996</option>
                                        <option value="1995">1995</option>
                                        <option value="1994">1994</option>
                                        <option value="1993">1993</option>
                                        <option value="1992">1992</option>
                                        <option value="1991">1991</option>
                                        <option value="1990">1990</option>
                                        <option value="1989">1989</option>
                                        <option value="1988">1988</option>
                                        <option value="1987">1987</option>
                                        <option value="1986">1986</option>
                                        <option value="1985">1985</option>
                                        <option value="1984">1984</option>
                                        <option value="1983">1983</option>
                                        <option value="1982">1982</option>
                                        <option value="1981">1981</option>
                                        <option value="1980">1980</option>
                                        <option value="1979">1979</option>
                                        <option value="1978">1978</option>
                                        <option value="1977">1977</option>
                                        <option value="1976">1976</option>
                                        <option value="1975">1975</option>
                                        <option value="1974">1974</option>
                                        <option value="1973">1973</option>
                                        <option value="1972">1972</option>
                                        <option value="1971">1971</option>
                                        <option value="1970">1970</option>
                                        <option value="1969">1969</option>
                                        <option value="1968">1968</option>
                                        <option value="1967">1967</option>
                                        <option value="1966">1966</option>
                                        <option value="1965">1965</option>
                                        <option value="1964">1964</option>
                                        <option value="1963">1963</option>
                                        <option value="1962">1962</option>
                                        <option value="1961">1961</option>
                                        <option value="1960">1960</option>
                                        <option value="1959">1959</option>
                                        <option value="1958">1958</option>
                                        <option value="1957">1957</option>
                                        <option value="1956">1956</option>
                                        <option value="1955">1955</option>
                                        <option value="1954">1954</option>
                                        <option value="1953">1953</option>
                                        <option value="1952">1952</option>
                                        <option value="1951">1951</option>
                                        <option value="1950">1950</option>
                                        <option value="1949">1949</option>
                                        <option value="1948">1948</option>
                                        <option value="1947">1947</option>
                                        <option value="1946">1946</option>
                                        <option value="1945">1945</option>
                                        <option value="1944">1944</option>
                                        <option value="1943">1943</option>
                                        <option value="1942">1942</option>
                                        <option value="1941">1941</option>
                                        <option value="1940">1940</option>
                                        <option value="1939">1939</option>
                                        <option value="1938">1938</option>
                                        <option value="1937">1937</option>
                                        <option value="1936">1936</option>
                                        <option value="1935">1935</option>
                                        <option value="1934">1934</option>
                                        <option value="1933">1933</option>
                                        <option value="1932">1932</option>
                                        <option value="1931">1931</option>
                                        <option value="1930">1930</option>
                                        <option value="1929">1929</option>
                                        <option value="1928">1928</option>
                                        <option value="1927">1927</option>
                                        <option value="1926">1926</option>
                                        <option value="1925">1925</option>
                                        <option value="1924">1924</option>
                                        <option value="1923">1923</option>
                                        <option value="1922">1922</option>
                                        <option value="1921">1921</option>
                                        <option value="1920">1920</option>
                                        <option value="1919">1919</option>
                                        <option value="1918">1918</option>
                                        <option value="1917">1917</option>
                                        <option value="1916">1916</option>
                                        <option value="1915">1915</option>
                                        <option value="1914">1914</option>
                                        <option value="1913">1913</option>
                                        <option value="1912">1912</option>
                                        <option value="1911">1911</option>
                                        <option value="1910">1910</option>
                                    </select>
                                </div>
                            </div>
                            <!-- parameter -->
                            <!-- parameter -->
                            <div class="formparameter">
                                <div class="formlabel">
                                    Güvenlik Kodu :
                                </div>
                                <div class="formdata">
                                    <input name="" type="text" class="w210" />
                                    &nbsp;<a href="#"><span class="colord8">Detaylı Bilgi</span></a>
                                </div>
                            </div>
                            <!-- parameter -->
                            <!-- parameter -->
                            <div class="formparameter">
                                <div class="formlabel">
                                    Kart Tipi :
                                </div>
                                <div class="formdata">
                                    <div class="formdata">
                                        <select name="name" class="w185">
                                            <option value="Adana">Adana</option>
                                            <option value="Adıyaman">Adıyaman</option>
                                            <option value="Afyon">Afyon</option>
                                            <option value="Ağrı">Ağrı</option>
                                            <option value="Aksaray">Aksaray</option>
                                            <option value="Amasya">Amasya</option>
                                            <option value="Ankara">Ankara</option>
                                            <option value="Antalya">Antalya</option>
                                            <option value="Ardahan">Ardahan</option>
                                            <option value="Artvin">Artvin</option>
                                            <option value="Aydın">Aydın</option>
                                            <option value="Balıkesir">Balıkesir</option>
                                            <option value="Bartın">Bartın</option>
                                            <option value="Batman">Batman</option>
                                            <option value="Bayburt">Bayburt</option>
                                            <option value="Bilecik">Bilecik</option>
                                            <option value="Bingöl">Bing&#246;l</option>
                                            <option value="Bitlis">Bitlis</option>
                                            <option value="Bolu">Bolu</option>
                                            <option value="Burdur">Burdur</option>
                                            <option value="Bursa">Bursa</option>
                                            <option value="Çanakkale">&#199;anakkale</option>
                                            <option value="Çankırı">&#199;ankırı</option>
                                            <option value="Çorum">&#199;orum</option>
                                            <option value="Denizli">Denizli</option>
                                            <option value="Diyarbakır">Diyarbakır</option>
                                            <option value="Düzce">D&#252;zce</option>
                                            <option value="Edirne">Edirne</option>
                                            <option value="Elazığ">Elazığ</option>
                                            <option value="Erzincan">Erzincan</option>
                                            <option value="Erzurum">Erzurum</option>
                                            <option value="Eskişehir">Eskişehir</option>
                                            <option value="Gaziantep">Gaziantep</option>
                                            <option value="Giresun">Giresun</option>
                                            <option value="Gümüşhane">G&#252;m&#252;şhane</option>
                                            <option value="Hakkari">Hakkari</option>
                                            <option value="Hatay">Hatay</option>
                                            <option value="Iğdır">Iğdır</option>
                                            <option value="Isparta">Isparta</option>
                                            <option value="İçel">İ&#231;el</option>
                                            <option selected="selected" value="İstanbul">İstanbul</option>
                                            <option value="İzmir">İzmir</option>
                                            <option value="Kahramanmaraş">Kahramanmaraş</option>
                                            <option value="Karabük">Karab&#252;k</option>
                                            <option value="Karaman">Karaman</option>
                                            <option value="Kars">Kars</option>
                                            <option value="Kastamonu">Kastamonu</option>
                                            <option value="Kayseri">Kayseri</option>
                                            <option value="Kırıkkale">Kırıkkale</option>
                                            <option value="Kırklareli">Kırklareli</option>
                                            <option value="Kırşehir">Kırşehir</option>
                                            <option value="Kilis">Kilis</option>
                                            <option value="Kocaeli">Kocaeli</option>
                                            <option value="Konya">Konya</option>
                                            <option value="Kütahya">K&#252;tahya</option>
                                            <option value="Malatya">Malatya</option>
                                            <option value="Manisa">Manisa</option>
                                            <option value="Mardin">Mardin</option>
                                            <option value="Muğla">Muğla</option>
                                            <option value="Muş">Muş</option>
                                            <option value="Nevşehir">Nevşehir</option>
                                            <option value="Niğde">Niğde</option>
                                            <option value="Ordu">Ordu</option>
                                            <option value="Osmaniye">Osmaniye</option>
                                            <option value="Rize">Rize</option>
                                            <option value="Sakarya">Sakarya</option>
                                            <option value="Samsun">Samsun</option>
                                            <option value="Siirt">Siirt</option>
                                            <option value="Sinop">Sinop</option>
                                            <option value="Sivas">Sivas</option>
                                            <option value="Şanlıurfa">Şanlıurfa</option>
                                            <option value="Şırnak">Şırnak</option>
                                            <option value="Tekirdağ">Tekirdağ</option>
                                            <option value="Tokat">Tokat</option>
                                            <option value="Trabzon">Trabzon</option>
                                            <option value="Tunceli">Tunceli</option>
                                            <option value="Uşak">Uşak</option>
                                            <option value="Van">Van</option>
                                            <option value="Yalova">Yalova</option>
                                            <option value="Yozgat">Yozgat</option>
                                            <option value="Zonguldak">Zonguldak</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- parameter -->
                            <!-- parameter -->
                            <div class="formparameter mt10">
                                <div class="formlabel colord8">
                                    Ödenecek Tutar :
                                </div>
                                <div class="formdata">
                                    <input name="" type="text" class="w210" />
                                </div>
                            </div>
                            <!-- parameter -->
                            <div class="buttons">
                                <a href="#">
                                    <img src="imagesgld/btn_odemeyionaylayin.jpg" alt="Ödemeyi Onaylayın" /></a>
                                &nbsp; <a href="#">
                                    <img src="imagesgld/btn_3dsecureodeme.jpg" alt="3D Secure ile Ödemeyi Tamamlayın" /></a>
                            </div>
                        </div>
                        <!-- credit card info -->
                        <!-- form buttons -->
                        <div class="formbuttons">
                            <a href="#">
                                <img src="imagesgld/btn_odemesecenekleri.jpg" alt="Ödeme Seçenekleri Sayfasına Geri Dönün" /></a>
                        </div>
                        <!-- form buttons -->
                    </div>
                    <!-- credit card root -->
                </div>
                <!-- credit card content -->
            </div>
            <!-- basket root -->
        </div>
    </div>
    <!-- basket container -->
</asp:Content>
