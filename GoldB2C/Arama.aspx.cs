﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Goldb2cBLL;
using Goldb2cInfrastructure;
using Goldb2cEntity;

public partial class Arama : System.Web.UI.Page
{
    AramaMotoruBLL bllAramaMotoru = new AramaMotoruBLL();
    SepetIslemleriBLL bllSepetIslemleri = new SepetIslemleriBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    DataTable dtAramaSonuc;
    int UrunId;
    Kullanici kullanici;

    bool CokluBundle = false;
    public string VipProtectKod = "";
    public string VipProtectAciklama = "";
    public string SayfaUrl = "";

    public string OzelUrunId = "0";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            UserControl headerControl = (UserControl)Page.Master.FindControl("Header1");
            string ArananKelime = string.Empty;
            string UrunKod1 = string.Empty;
            if (!string.IsNullOrEmpty(Request.QueryString["category"]))
            {
                UrunKod1 = InputSafety.SecureString(HttpUtility.UrlDecode(Request.QueryString["category"]));

                DropDownList drpCategories = (DropDownList)headerControl.FindControl("drpCategories");
                drpCategories.SelectedValue = UrunKod1;
            }

            if (!string.IsNullOrEmpty(Request.QueryString["text"]))
                ArananKelime = InputSafety.SecureString(HttpUtility.UrlDecode(Request.QueryString["text"]));
            {
                TextBox txtSearch = (TextBox)headerControl.FindControl("txtSearch");
                txtSearch.Text = ArananKelime;

                if (ArananKelime.Length >= 2)
                {
                    int ToplamSayfa = 0;
                    int ToplamUrun = 0;
                    int ToplamMarka = 0;
                    int SayfaNo = 1;
                    string AramaOneriStr = "";

                    dtAramaSonuc = bllAramaMotoru.getKategoriAramaMotoru(UrunKod1, ArananKelime);
                    if (dtAramaSonuc != null && dtAramaSonuc.Rows.Count > 0)
                    {
                        int rowsay = 0;
                        foreach (DataRow row in dtAramaSonuc.Rows)
                        {

                            if (dtAramaSonuc.Rows[rowsay]["resim_ad"].ToString().IndexOf("http") == -1)
                            {
                                dtAramaSonuc.Rows[rowsay]["resim_ad"] = "http://cdn.gold.com.tr/UrunResim/BuyukResim/" + dtAramaSonuc.Rows[rowsay]["resim_ad"];
                            }
                            rowsay = rowsay + 1;

                        }

                        rptUrunler.DataSource = dtAramaSonuc;
                        rptUrunler.DataBind();

                        //ltrArananKelime.Text = ArananKelime;
                        //ltrUrunSayisi.Text = dtAramaSonuc.Rows.Count.ToString();
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Aradığınız ürün bulunamadı.')</script>");
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Arama yapabilmek için en az 2 harf giriniz.')</script>");
                }
            }
        }
    }



    private void SayfalamaOlustur(int ToplamSayfa, int SayfaNo)
    {

        //Dictionary<string, string[]> QueryStringDict = QueryStrBuilder.GetirQueryString();
        //string inpExtraVeri = "";
        //if (QueryStringDict.ContainsKey("kbilesen_id"))
        //{
        //    string[] QueryStringKbilesenId = QueryStringDict["kbilesen_id"];
        //    foreach (string kbilesenId in QueryStringKbilesenId)
        //    {
        //        if (string.IsNullOrEmpty(inpExtraVeri))
        //            inpExtraVeri = "&kbilesen_id=" + kbilesenId;
        //        else
        //            inpExtraVeri = inpExtraVeri + kbilesenId;
        //    }
        //}
        //int BaslangicSayfaNo = 0;
        //int BitisSayfaNo = 0;

        //if (SayfaNo % 10 == 0)
        //{
        //    if (SayfaNo - 4 > 0)
        //        BaslangicSayfaNo = SayfaNo - 4;
        //    else
        //        BaslangicSayfaNo = 1;

        //    if (SayfaNo + 5 <= ToplamSayfa)
        //        BitisSayfaNo = SayfaNo + 5;
        //    else
        //        BitisSayfaNo = ToplamSayfa;
        //}
        //else
        //{
        //    BaslangicSayfaNo = ((SayfaNo / 10) * 10) - 4;
        //    if (BaslangicSayfaNo < 1)
        //        BaslangicSayfaNo = 1;
        //    if (BaslangicSayfaNo + 9 > ToplamSayfa)
        //        BitisSayfaNo = ToplamSayfa;
        //    else
        //        BitisSayfaNo = BaslangicSayfaNo + 9;
        //}

        //StringBuilder sb = new StringBuilder();

        //sb.Append("<a onclick=\"SayfaYonlendir('1');\" class=\"skip\" title=\"Geri\"><img src=\"imagesgld/arrow_paginationStart.png\" alt=\"Geri\" /></a> <span>| </span>");

        //for (int i = BaslangicSayfaNo; i <= BitisSayfaNo; i++)
        //{
        //    if (i == SayfaNo)
        //        sb.Append("<strong>" + SayfaNo.ToString() + "</strong> <span>| </span>");
        //    else
        //        sb.Append("<a onclick=\"SayfaYonlendir('" + i.ToString() + "');\" class=\"curhand\" >" + i.ToString() + "</a> <span>| </span>");
        //}

        //sb.Append("<span>| </span><a onclick=\"SayfaYonlendir('" + ToplamSayfa.ToString() + "');\" class=\"skip\" title=\"İleri\"><img src=\"imagesgld/arrow_paginationEnd.png\" alt=\"İleri\" /></a>");
        //ltrSayfalama.Text = sb.ToString();

    }

    protected void rptUrunler_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //Ürün Fiyatları
        DataRowView drv = e.Item.DataItem as DataRowView;
        Label lblBrutTutar = (Label)e.Item.FindControl("lblBrutTutar");
        Label lblNetTutar = (Label)e.Item.FindControl("lblNetTutar");
        Label lblBilgi = (Label)e.Item.FindControl("lblBilgi");
        Literal ltrSepeteEkle = (Literal)e.Item.FindControl("ltrSepeteEkle");

        if (Convert.ToBoolean(drv.Row["temin_durum"]))
        {
            string urun_id = drv.Row["urun_id"].ToString();

            string sepeteEkleDiv = "<div class=\"add-to-cart\">" +
                                 "<a title=\"Sepete Ekle\" href=\"#\" onclick=\"return SepetEkle(" + urun_id + ");\">Sepete Ekle</a>" +
                                 "</div>";
            ltrSepeteEkle.Text = sepeteEkleDiv;

            decimal brut_tutar = Math.Round(Convert.ToDecimal(drv.Row["brut_tutar"]), 2);
            decimal net_tutar = Math.Round(Convert.ToDecimal(drv.Row["net_tutar"]), 2);

            decimal onikiTaksit = Math.Round((Convert.ToDecimal(drv.Row["net_tutar"]) / 12), 2);

            //lblNetTutar.Text = onikiTaksit.ToString() + " PUAN";
            //lblBilgi.Text = "x 12 Taksit";

            //lblBilgi.Text = "KDV Dahil";
            if (brut_tutar > net_tutar)
            {
                lblBrutTutar.Text = brut_tutar.ToString("###,###,###") + " <span>PUAN</span>";
                lblNetTutar.Text = net_tutar.ToString("###,###,###") + " <span> PUAN</span>";
            }
            else
            {
                lblBrutTutar.Visible = false;
                lblNetTutar.Text = brut_tutar.ToString("###,###,###") + " <span> PUAN</span>";
            }
        }
        else
        {
            ltrSepeteEkle.Text = string.Empty;

            if (!string.IsNullOrEmpty(drv.Row["temin_aciklama"].ToString()))
                lblBilgi.Text = "&nbsp;&raquo;&nbsp;" + drv.Row["temin_aciklama"].ToString();
            else
                lblBilgi.Text = "&nbsp;&raquo;&nbsp;" + "Ürün geçici olarak temin edilemiyor.";
        }

        //İkonlar
        //if (Convert.ToBoolean(drv.Row["indirimli"]))
        //    ((Image)e.Item.FindControl("imgIndirimli")).ImageUrl = "~/imagesgld/icon_indirimliurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgIndirimli")).ImageUrl = "~/imagesgld/icon_indirimliurunOff.png";

        //if (Convert.ToBoolean(drv.Row["aynigun_sevk"]))
        //    ((Image)e.Item.FindControl("imgAyniGunSevk")).ImageUrl = "~/imagesgld/icon_hizlikargoOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgAyniGunSevk")).ImageUrl = "~/imagesgld/icon_hizlikargoOff.png";

        //if (Convert.ToBoolean(drv.Row["hediyeli_urun"]))
        //    ((Image)e.Item.FindControl("imgHediyeliUrun")).ImageUrl = "~/imagesgld/icon_hediyeliurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgHediyeliUrun")).ImageUrl = "~/imagesgld/icon_hediyeliurunOff.png";

        //if (Convert.ToBoolean(drv.Row["kuryeile_sevk"]))
        //    ((Image)e.Item.FindControl("imgKuryeileSevk")).ImageUrl = "~/imagesgld/icon_coksatanurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgKuryeileSevk")).ImageUrl = "~/imagesgld/icon_coksatanurunOff.png";

        //if (Convert.ToBoolean(drv.Row["kargo_bedava"]))
        //    ((Image)e.Item.FindControl("imgKargoBedava")).ImageUrl = "~/imagesgld/icon_kargobedavaOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgKargoBedava")).ImageUrl = "~/imagesgld/icon_kargobedavaOff.png";

        //if (Convert.ToBoolean(drv.Row["yeni_urun"]))
        //    ((Image)e.Item.FindControl("imgYeniUrun")).ImageUrl = "~/imagesgld/icon_yeniurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgYeniUrun")).ImageUrl = "~/imagesgld/icon_yeniurunOff.png";

    }
}