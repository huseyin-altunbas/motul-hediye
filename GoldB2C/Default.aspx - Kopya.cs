﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NLog;
using Goldb2cBLL;
using System.Data;
using System.Text;
using System.Web.Services;
using Goldb2cEntity;

public partial class _Default : System.Web.UI.Page
{
    OzelKategoriBLL bllOzelKategori = new OzelKategoriBLL();
    DataTable dtOzelKategoriMaster;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CategoriesBind();
        }

        if (Request.UrlReferrer != null && Request.UrlReferrer.Host == "alcikart.com")
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != string.Empty)
            {
                Session["UrlRefHost"] = "alcikart.com";
                Response.Redirect("~/MusteriHizmetleri/uyegirisi.aspx?sessionid=" + Request.QueryString["id"].ToString(), true);
            }
        }
    }

    public void CategoriesBind()
    {
        Session["ElevatorId"] = 1;
        Session["NavMenuId"] = 1;

        dtOzelKategoriMaster = bllOzelKategori.OzelKategoriMaster(0);
        List<int> categoryList = dtOzelKategoriMaster.AsEnumerable()
                           .Select(r => r.Field<int>("master_id"))
                           .ToList();

        rptCategories.DataSource = categoryList;
        rptCategories.DataBind();
    }

    [WebMethod]
    public static List<HitPanelUrun> GetHitUrun(string HitUrunTip)
    {
        List<HitPanelUrun> hitUrunList = new List<HitPanelUrun>();

        DataTable dt = new DataTable();
        HitUrunIslemleriBLL bllHitUrunIslemleri = new HitUrunIslemleriBLL();
        dt = bllHitUrunIslemleri.HitUrunKartListesi(HitUrunTip, 0, 0);
        var teminEdilenUrunler = dt.AsEnumerable().Where(u => u.Field<bool>("temin_durum") == true && u.Field<decimal>("net_tutar") > 0);
        dt = teminEdilenUrunler.Count() > 0 ? teminEdilenUrunler.CopyToDataTable() : dt.Clone();

        HitPanelUrun hitUrun;
        foreach (DataRow dr in dt.Rows)
        {
            if (dr["resim_ad"].ToString().IndexOf("http") == -1)
            {
                dr["resim_ad"] = "http://www.gold.com.tr/UrunResim/MegaResim/" + dr["resim_ad"].ToString();
            }

            hitUrun = new HitPanelUrun();
            hitUrun.UrunId = dr["urun_id"].ToString();
            hitUrun.UrunAd = dr["urun_ad"].ToString();
            hitUrun.UrunLink = dr["urun_link"].ToString();
            hitUrun.UrunResimAd = dr["resim_ad"].ToString();
            hitUrun.UrunBrutTutar = dr["brut_tutar"].ToString();
            hitUrun.UrunNetTutar = dr["net_tutar"].ToString();

            hitUrunList.Add(hitUrun);
        }

        return hitUrunList;
    }
}