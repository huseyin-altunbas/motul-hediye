﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="LandingPage.aspx.cs" Inherits="LandingPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/landingpage.css" rel="stylesheet" type="text/css" />
    <asp:Literal ID="ltrCssClass" runat="server"></asp:Literal>
    <div class="rootercnav">
        <div class="rcframe">
            <div style="width: 100%;" class="rootername">
                <a href="https://motulteam.com/">Anasayfa</a> /
                <asp:Label ID="lblLandingBaslik" runat="server" Text="Label"></asp:Label>
            </div>
        </div>
    </div>
    <div class="containerOut982">
        <asp:Literal ID="ltrLandingIcerik" runat="server"></asp:Literal>
    </div>
    <%--    <div class="landingPage">
        <ul class="breadcrumbs">
            <li></li>
            <li>/</li>
            <li>
                <asp:Label ID="lblLandingBaslik" runat="server" Text="Label"></asp:Label></li>
        </ul>
        <div class="landingClear">
        </div>
        <div class="landingContainer">
            <!-- Landing Page Content Start -->
            <asp:Literal ID="ltrLandingIcerik" runat="server"></asp:Literal>
            <!-- Landing Page Content End -->
        </div>
    </div>--%>
</asp:Content>
