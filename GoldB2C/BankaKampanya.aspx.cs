﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Goldb2cBLL;

public partial class BankaKampanya : System.Web.UI.Page
{
    AdminGenelBLL bllAdminGenel = new AdminGenelBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            BankaKampanyaGetir();

    }

    private void BankaKampanyaGetir()
    {
        DataTable dtBankaKampanya = new DataTable();
        dtBankaKampanya = bllAdminGenel.GetirBankaKampanya();

        ContentPlaceHolder cp = (ContentPlaceHolder)Page.Master.FindControl("ContentPlaceHolder1");

        if (dtBankaKampanya != null && dtBankaKampanya.Rows.Count > 0)
        {
            foreach (DataRow dr in dtBankaKampanya.Rows)
            {
                string BankaKod = dr["BankaKod"].ToString();

                // ((Literal)cp.FindControl("ltrKampanyaBaslik" + BankaKod)).Text = dr["KampanyaBaslik"].ToString();
                ((Literal)cp.FindControl("ltrKampanyaIcerik" + BankaKod)).Text = dr["KampanyaIcerik"].ToString();
                ((Image)cp.FindControl("ImgTaksit" + BankaKod)).ImageUrl = "/ImagesGld/BankCampaigns/Kampanya" + BankaKod + dr["KampanyaTaksitSayi"].ToString() + ".png";
            }
        }
    }
}