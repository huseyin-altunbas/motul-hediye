﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="UserControls_Header" %>
<div class="top-header">
    <div id="divOpen" runat="server">
        <div class="container">
            <div class="nav-top-links">
                <a href="/MusteriHizmetleri/Iletisim.aspx">Müşteri Hizmetleri</a>
            </div>
            <%--<div class="support-link">
            </div>--%>
            <div id="user-info-top" class="user-info pull-right">
                <div class="dropdown">
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                        href="#"><span>Hesabım</span></a>
                    <ul class="dropdown-menu mega_dropdown" role="menu">
                        <%-- <li><a href="http://www.biges.com/login?BIGESCLUB=1">Üye Girişi</a></li>--%>
                        <%--<li><a href="/MusteriHizmetleri/UyeKayit.aspx">Üye Kayıt</a></li>
                        <li><a href="/MusteriHizmetleri/SifremiUnuttum.aspx">Şifremi Unuttum</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <style>
        .fbIMG table {
            margin: 0px;
            padding: 0px;
            line-height: 1.5;
        }

            .fbIMG table tr {
                margin: 0px;
                padding: 0px;
            }

            .fbIMG table tr td{
                margin: 0px;
                padding: 0px;
            }
    </style>

    <div id="divClose" runat="server" visible="false">
        <div class="container">
            <div class="nav-top-links">
                <%--  <a href="http://www.biges.com/bilgilerim">
                        Üyelik Bilgilerim</a><a href="/MusteriHizmetleri/Iletisim.aspx"> Müşteri Hizmetleri</a>--%>
            </div>
            <div class="support-link">
                <span class="fbIMG">
                    <asp:Image ID="imgFacebook" runat="server" />





                    <table>
                        <tr>
                            <td>
                </span>Hoş geldiniz Sayın <strong>
                    <asp:Label ID="lblKullaniciAdSoyad" runat="server" />
                </strong></td>
         
                        <td><span style="float: right;">
                            <asp:LinkButton ID="lnbCikis" runat="server" OnClick="lnbCikis_Click">Güvenli Çıkış</asp:LinkButton>
                        </span></td>
                </tr>

                    <tr>

                        <td>
                            <asp:Label ID="lblToplamPuan" runat="server" />
                        </td>
                        <td><strong style="color: red; padding-left: 10px; display:none;">
                            <asp:Label ID="lblSilinecekPuan" runat="server" /></strong> 

                        </td>

                    </tr>
                </table>

            </div>
        </div>
    </div>
</div>
<!--/.top-header -->
<!-- MAIN HEADER -->
<div class="container main-header">
    <div class="row">
        <div class="col-xs-12 col-sm-3 logo">
            <a href="/">
                <img alt="Kute shop - themelock.com" src="/assets/images/logo.png" /></a>
        </div>
        <div class="col-xs-7 col-sm-7 header-search-box">
            <div class="form-inline">
                <div class="form-group form-category">
                    <asp:DropDownList ID="drpCategories" runat="server" CssClass="select-category">
                    </asp:DropDownList>
                </div>
                <div class="form-group input-search">
                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Ara..."></asp:TextBox>
                </div>
                <asp:Button ID="btnSearch" runat="server" Text="" CssClass="pull-right btn-search"
                    OnClick="btnSearch_Click" />
            </div>
        </div>
        <div id="cart-block" class="col-xs-5 col-sm-2 shopping-cart-box">
        </div>
    </div>
</div>
<!-- END MANIN HEADER -->
