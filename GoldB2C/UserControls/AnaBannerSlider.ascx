﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AnaBannerSlider.ascx.cs"
    Inherits="UserControls_AnaBannerSlider" %>
<link rel="stylesheet" type="text/css" href="/css/rotator.css?v=1.0.4.1" />
<div class="slider-wrap" id="demo4" style="width: 632px; height: 350px;">
    <div class="slider4-wrap">
        <asp:Literal ID="ltrRotatorBanner" runat="server"></asp:Literal>
        <div class="slider4-arrow-prev slider4-arrow">
        </div>
        <div class="slider4-arrow-next slider4-arrow">
        </div>
        <div class="sliderjs-play-pause">
        </div>
    </div>
</div>
<script type="text/javascript" src="/js/slider/slider.js?v=3.1.5.1"></script>
<script type="text/javascript" src="/js/slider/script.js?v=3.1.5.2"></script>
<script type="text/javascript">

    $("#demo4").mouseenter(function () {
        $('.sliderjs-play-pause').click();
    }).mouseleave(function () {
        $('.sliderjs-play-pause').click();
    });

    $('.slider4-ticker').mouseenter(
        function () {
            $(this).click();
        });
</script>
