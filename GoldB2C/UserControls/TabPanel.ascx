﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TabPanel.ascx.cs" Inherits="UserControls_TabPanel" %>
<div class="popular-tabs">
    <ul class="nav-tab">
        <li class="active"><a data-toggle="tab" href="#tab-1">BEST SELLERS</a></li>
        <li><a data-toggle="tab" href="#tab-2">ON SALE</a></li>
        <li><a data-toggle="tab" href="#tab-3">New products</a></li>
    </ul>
    <div class="tab-container">
        <asp:Repeater ID="rptTabPanel" runat="server">
            <HeaderTemplate>
                <div id="tab-1" class="tab-panel active">
                    <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav="true"
                        data-margin="30" data-autoplaytimeout="1000" data-autoplayhoverpause="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":3}}'>
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <div class="left-block">
                        <a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>">
                            <img class="img-responsive" alt="product" src='<%# DataBinder.Eval(Container.DataItem,"resim_ad") %>' />
                        </a>
                        <div class="quick-view">
                            <a title="Add to my wishlist" class="heart" href="#"></a><a title="Add to compare"
                                class="compare" href="#"></a><a title="Quick view" class="search" href="#"></a>
                        </div>
                        <div class="add-to-cart">
                            <a title="Add to Cart" href="#">Add to Cart</a>
                        </div>
                        <div class="group-price">
                            <span class="product-new">NEW</span> <span class="product-sale">Sale</span>
                        </div>
                    </div>
                    <div class="right-block">
                        <h5 class="product-name">
                            <a href="#">Sexy Lady</a></h5>
                        <div class="content_price">
                            <%# Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "brut_tutar")) > Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")) ? 
                        "<span class=\"price product-price\">"+ Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")), 2).ToString("###,###,###")+" PUAN</span>"
                                                    + "<span class=\"price old-price\">" + Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "brut_tutar")), 2).ToString("###,###,###") + " PUAN</span>" :
                        "<span class=\"price product-price\">"+ Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")), 2).ToString("###,###,###")+" PUAN</span>"
                            %>
                        </div>
                        <div class="product-star">
                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                class="fa fa-star"></i><i class="fa fa-star-half-o"></i>
                        </div>
                    </div>
                </li>
            </ItemTemplate>
            <FooterTemplate>
                </ul> </div>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>
