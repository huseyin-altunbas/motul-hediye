﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OdemeFirsatlari.ascx.cs" Inherits="UserControls_OdemeFirsatlari" %>
<!--  BankTicker  -->
<script type="text/javascript" src="js/jquery.vticker-min.js"></script>
<script type="text/javascript">
    $(function () {
        $('#contentholder .bankticker .bankinstalment .bankcontents').vTicker({
            speed: 500,
            pause: 5000,
            animation: 'fade',
            mousePause: true,
            showItems: 1,
            direction: 'up'
        });
    });
</script>
<!--  BankTicker  -->
<div class="bankinstalment">
    <div class="bankcontents">
        <ul class="creditcards">
            <asp:Repeater ID="rptOdemeFirsatlari" runat="server">
                <ItemTemplate>
                    <li>
                        <div class="cards">
                            <div class="logo">
                                <img src="<%# DataBinder.Eval(Container.DataItem, "ilkbolum_imaj") %>" alt="<%# DataBinder.Eval(Container.DataItem, "firsat_aciklamasi") %>" title="<%# DataBinder.Eval(Container.DataItem, "firsat_aciklamasi") %>" />
                            </div>
                            <div class="instalment"><%# DataBinder.Eval(Container.DataItem, "sonbolum_imaj") %></div>                            
                        </div>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</div>