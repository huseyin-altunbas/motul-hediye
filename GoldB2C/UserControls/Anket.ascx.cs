﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using Goldb2cInfrastructure;

public partial class UserControls_Anket : System.Web.UI.UserControl
{
    AnketIslemleriBLL bllAnketIslemleri = new AnketIslemleriBLL();
    DataTable dtAnket;
    DataTable dtAnketSecenekleri;
    DataTable dtAnketOylari;
    Kullanici kullanici;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            Bind();
    }

    public void Bind()
    {
        if (Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)Session["Kullanici"];

            DataTable dt = bllAnketIslemleri.AnketGetir();
            dtAnket = dt.AsEnumerable().Where(x => x.Field<bool>("Aktif")).Count() > 0 ? dt.AsEnumerable().Where(x => x.Field<bool>("Aktif")).CopyToDataTable() : dt.Clone();

            if (dtAnket.Rows.Count > 0)
            {
                int[] arrayAnketIds = arrayAnketIds = new int[dtAnket.Rows.Count];

                for (int i = 0; i < dtAnket.Rows.Count; i++)
                {
                    arrayAnketIds[i] = Convert.ToInt32(dtAnket.Rows[i]["AnketId"]);
                }

                dtAnketSecenekleri = bllAnketIslemleri.AnketSecenekleriGetir();

                dtAnketOylari = bllAnketIslemleri.AnketOylariGetir();
                var kullanicininOyKullandigiAnketler = (from m in dtAnketOylari.AsEnumerable()
                                                        where m.Field<int>("KullaniciId") == kullanici.KullaniciId && arrayAnketIds.Contains(m.Field<int>("AnketId"))
                                                        select new { AnketId = m.Field<int>("AnketId") }).ToList();


                string strAnketIds = "";
                if (kullanicininOyKullandigiAnketler.Count > 0)
                {
                    foreach (int item in arrayAnketIds)
                    {
                        bool b = false;
                        foreach (var lst in kullanicininOyKullandigiAnketler)
                        {
                            if (item == lst.AnketId)
                            {
                                b = true;
                                break;
                            }
                        }
                        if (!b)
                            strAnketIds += item.ToString() + ",";
                    }
                }
                else
                {
                    foreach (int item in arrayAnketIds)
                    {
                        strAnketIds += item.ToString() + ",";
                    }
                }

                strAnketIds = strAnketIds.TrimEnd(',');

                if (strAnketIds != string.Empty)
                {
                    ViewState["AnketIds"] = strAnketIds;

                    DataRow[] drAnket = dtAnket.Select("AnketId in (" + strAnketIds + ")");

                    rptAnket.DataSource = drAnket.Length > 0 ? drAnket.CopyToDataTable() : dtAnket.Clone();
                    rptAnket.DataBind();

                    lblBaslik.Visible = true;
                    btnKaydet.Visible = true;
                }
                else
                {
                    rptAnket.DataSource = null;
                    rptAnket.DataBind();

                    lblBaslik.Visible = false;
                    btnKaydet.Visible = false;

                }
            }
            else
            {
                lblBaslik.Visible = false;
                btnKaydet.Visible = false;
            }
        }
    }

    protected void rptAnket_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = e.Item.DataItem as DataRowView;
            Repeater rptAnketSecenekleri = (Repeater)e.Item.FindControl("rptAnketSecenekleri");

            DataRow[] drAnketSecenekleri = dtAnketSecenekleri.Select("AnketId = '" + drv.Row["AnketId"].ToString() + "'");

            rptAnketSecenekleri.DataSource = drAnketSecenekleri.Length > 0 ? drAnketSecenekleri.CopyToDataTable() : dtAnketSecenekleri.Clone();
            rptAnketSecenekleri.DataBind();
        }
    }

    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)Session["Kullanici"];
            bool empty = false;
            int sonuc = 0;
            int sonuc2 = 0;
            string[] strIds = ViewState["AnketIds"].ToString().Split(',');
            StringBuilder sbMessage = new StringBuilder();
            string strIsText = "";

            foreach (string item in strIds)
            {
                if (Request.Form[item] == null)
                {
                    empty = true;
                    break;
                }
                else
                {
                    strIsText += Request.Form[item].ToString() + ",";
                }
            }



            if (!empty)
            {
                strIsText = strIsText.TrimEnd(',');
                bool emptyText = false;
                DataTable dtAnketSecenekleri = bllAnketIslemleri.AnketSecenekleriGetir();
                DataRow[] drAnketSecenekleri = dtAnketSecenekleri.Select("SecenekId in ( " + strIsText + ") AND IsText=True");
                DataTable dtAnketIsText = drAnketSecenekleri.Length > 0 ? drAnketSecenekleri.CopyToDataTable() : dtAnketSecenekleri.Clone();

                foreach (DataRow dr in dtAnketIsText.Rows)
                {
                    if (Request.Form["txt_" + dr["AnketId"].ToString() + "_" + dr["SecenekId"]] == null || Request.Form["txt_" + dr["AnketId"].ToString() + "_" + dr["SecenekId"]].ToString() == string.Empty || Request.Form["txt_" + dr["AnketId"].ToString() + "_" + dr["SecenekId"]].ToString().Length > 100)
                    {
                        emptyText = true;
                        break;
                    }
                }

                if (!emptyText)
                {
                    int i = 0;
                    foreach (string item in strIds)
                    {
                        sonuc = bllAnketIslemleri.AnketOylariKaydet(Convert.ToInt32(item), Convert.ToInt32(Request.Form[item.ToString()]), kullanici.KullaniciId);
                        if (sonuc > 0)
                        {
                            i++;
                            sbMessage.Append(string.Format("{0} nolu anket cevabınız kaydedilmiştir.<br/>", i.ToString()));
                        }

                    }

                    //Diğer text seçeneğinde kullanıcının kendi istediğini kaydeder.
                    foreach (DataRow dr in dtAnketIsText.Rows)
                    {
                        string txt2 = InputSafety.SecureString(Request.Form["txt_" + dr["AnketId"].ToString() + "_" + dr["SecenekId"]].ToString());
                        sonuc2 = bllAnketIslemleri.AnketTextKaydet(Convert.ToInt32(dr["AnketId"].ToString()), Convert.ToInt32(dr["SecenekId"].ToString()), kullanici.KullaniciId, txt2);
                    }

                    //Kayıt yapılamayanlar tekrar gösterilecek.
                    Bind();

                    if (i == strIds.Length)
                    {
                        Page.ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Tüm anket cevaplarınız kaydedilmiştir.<br/>Anketimize katıldığınız için teşekkür ederiz."));
                    }
                    else if (i == 0)
                    {
                        Page.ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Anket cevaplarınız kaydedilemedi."));
                    }
                    else if (i > 0 && i != strIds.Length)
                    {
                        Page.ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur(sbMessage.ToString() + "<br/>Kaydedilemeyen diğer anket sorularına tekrar oy verebilirsiniz."));
                    }

                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Text alanlarının doldurulması gerekmektedir.En fazla 100 karakter girilebilir."));
                }
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Tüm anket sorularını cevaplandırmanız gerekmektedir."));
            }
        }
    }

    private string MesajScriptOlustur(string Mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + Mesaj + "</td></tr></table></div>');</script>";
    }
}