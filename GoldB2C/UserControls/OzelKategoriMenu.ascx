﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OzelKategoriMenu.ascx.cs"
    Inherits="UserControls_OzelKategoriMenu" %>
<div class="filterContainer">
    <asp:Repeater ID="rptOzelKategoriMenu" runat="server">
        <HeaderTemplate>
            <ul class="mothersMenu">
        </HeaderTemplate>
        <ItemTemplate>
            <li><a href="/OzelKategori.aspx?KategoriId=<%#DataBinder.Eval(Container.DataItem, "master_id") %>&KategoriDetayId=<%#DataBinder.Eval(Container.DataItem, "detay_id") %>"
                class="mothersNoBorderTop">
                <%#DataBinder.Eval(Container.DataItem, "ayakizi_ad") %></a> </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul></FooterTemplate>
    </asp:Repeater>
</div>
