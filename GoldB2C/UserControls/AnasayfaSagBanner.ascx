﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AnasayfaSagBanner.ascx.cs"
    Inherits="UserControls_AnasayfaSagBanner" %>
<div class="mbright">
    <ul class="smallbanner">
        <asp:Repeater ID="rptBanner" runat="server">
            <ItemTemplate>
                <li style="margin-bottom: 7px;"><a onclick="TiklamaKaydet($(this).attr('href'));"
                    href="<%# DataBinder.Eval(Container.DataItem, "banner_url") %>">
                    <%# DataBinder.Eval(Container.DataItem, "banner_html") %></a></li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>
