﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Category.ascx.cs" Inherits="UserControls_Category" %>
<div class="category-featured">
    <nav class="navbar nav-menu show-brand <%#GetNavMenuColor() %>">
    <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <asp:Literal ID="ltrCategory" runat="server"></asp:Literal>
    <span class="toggle-menu"></span>
    <asp:Repeater ID="rptSubCategory" runat="server">
        <HeaderTemplate>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">           
            <ul class="nav navbar-nav">
        </HeaderTemplate>
        <ItemTemplate>
            <li><a href="<%# DataBinder.Eval(Container.DataItem,"url_rewrite") %>"><%# DataBinder.Eval(Container.DataItem,"urun_ad") %></a></li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
            </div><!-- /.navbar-collapse -->
        </FooterTemplate>
    </asp:Repeater>
    </div><!-- /.container-fluid -->
    <asp:Literal ID="ltrElevator" runat="server"></asp:Literal>
    </nav>
    <%--<div class="category-banner">
        <asp:Literal ID="ltrCategoryBanner" runat="server"></asp:Literal>
    </div>--%>
    <div class="product-featured clearfix">
        <div class="banner-featured">
            <div class="featured-text">
              </div>
            <div class="banner-img">
                <asp:Literal ID="ltrFeaturedImage" runat="server"></asp:Literal>
            </div>
        </div>
        <asp:Repeater ID="rptCategory" runat="server">
            <HeaderTemplate>
                <div class="product-featured-content">
                    <div class="product-featured-list">
                        <div class="tab-container">
                            <!-- tab product -->
                            <div class="tab-panel active" id="tab-4">
                                <ul class="product-list owl-carousel" data-dots="false" data-loop="false" data-nav="true"
                                    data-margin="0" data-autoplaytimeout="1000" data-autoplayhoverpause="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":4}}'>
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <div class="left-block">
                        <a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>">
                            <img class="img-responsive lazy" alt="product" data-original="<%# DataBinder.Eval(Container.DataItem, "resim_ad") %>" /></a>
                        <%--<div class="quick-view">
                        <a title="Add to my wishlist" class="heart" href="#"></a><a title="Add to compare"
                            class="compare" href="#"></a><a title="Quick view" class="search" href="#"></a>
                    </div>--%>
                        <div class="add-to-cart">
                            <a title="Sepete Ekle" href="#" onclick="return SepetEkle(<%# DataBinder.Eval(Container.DataItem, "urun_id") %>);">
                                Sepete Ekle</a>
                        </div>
                    </div>
                    <div class="right-block">
                        <h5 class="product-name">
                            <a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>">
                                <%# DataBinder.Eval(Container.DataItem,"urun_ad") %></a></h5>
                        <div class="content_price">
                            <%# Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "brut_tutar")) > Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")) ? 
                        "<span class=\"price product-price\">"+ Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")), 2).ToString("###,###,###")+" PUAN</span>"
                                                    + "<span class=\"price old-price\">" + Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "brut_tutar")), 2).ToString("###,###,###") + " PUAN</span>" :
                        "<span class=\"price product-price\">"+ Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")), 2).ToString("###,###,###")+" PUAN</span>"
                            %>
                        </div>
                        <%--<div class="product-star">
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                            class="fa fa-star"></i><i class="fa fa-star-half-o"></i>
                    </div>--%>
                    </div>
                </li>
            </ItemTemplate>
            <FooterTemplate>
                </ul> </div> </div> </div> </div>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>
