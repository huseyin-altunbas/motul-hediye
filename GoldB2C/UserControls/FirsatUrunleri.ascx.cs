﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;

public partial class UserControls_FirsatUrunleri : System.Web.UI.UserControl
{
    FirsatUrunleriBLL bllFirsatUrunleri = new FirsatUrunleriBLL();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            //burada cari grup id verilebilecek. ürünler kişiye göre gösterilebilsin diye
            rptGununFirsati.DataSource = TabloSirasiniKaristir(bllFirsatUrunleri.GetFirsatUrun(0, 0));
            rptGununFirsati.DataBind();
        }
    }


    private DataTable TabloSirasiniKaristir(DataTable dtFirsatUrunleri)
    {
        if (dtFirsatUrunleri != null && dtFirsatUrunleri.Rows.Count > 0)
        {
            Random rnd = new Random();

            dtFirsatUrunleri.Columns.Add(new DataColumn("RowNumber", typeof(int)));
            foreach (DataRow dr in dtFirsatUrunleri.Rows)
            {
                dr["RowNumber"] = rnd.Next();
            }

            DataView dv = dtFirsatUrunleri.DefaultView;
            dv.Sort = "RowNumber";
            dtFirsatUrunleri = dv.ToTable();
        }
        return dtFirsatUrunleri;
    }
}