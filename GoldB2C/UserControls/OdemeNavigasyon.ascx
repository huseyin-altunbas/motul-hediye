﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OdemeNavigasyon.ascx.cs"
    Inherits="UserControls_OdemeNavigasyon" %>
<div class="basketnav">
    <ul>
        <li class="menu1 menu1selected real"><a class="amenu1 cursordef <%=SepetSelected%>"
            href="<%=SepetLink%>">Sepetim</a> </li>
        <li class="menu2"><a class="amenu2 cursordef <%=AdresbilgileriSelected%>" href="<%=AdresbilgileriLink%>">
            Teslimat
            <br />
            Adresi</a> </li>
        <%--<li class="menu3 menu1selected real"><a class="amenu3 cursordef <%=KargoSelected%>" href="<%=KargoLink%>">
            Kargo</a> </li>--%>
        <%--<li class="menu4"><a class="amenu4 cursordef <%=OdemeSelected%>" href="<%=OdemeLink%>">
            Ödeme
            <br />
            Seçenekleri</a> </li>--%>
        <li class="menu5"><a class="amenu5 cursordef <%=OdemeonaySelected%>" href="<%=OdemeonayLink%>">
            Sipariş
            <br />
            Onay</a> </li>
    </ul>
</div>
