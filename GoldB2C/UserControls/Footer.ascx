﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Footer.ascx.cs" Inherits="UserControls_Footer" %>
<style type="text/css">
    #introduce-box {
        margin-top: 2px !important;
    }
</style>
<!-- Footer -->
<footer id="footer">
     <div class="container">
            <!-- introduce-box -->
            <div id="introduce-box" class="row">
                <div class="col-md-3">
                    <div>
                        <a href="/"><img src="/assets/data/introduce-logo.png" alt="" /></a>
                    </div>
                </div>
                <div class="col-md-6">                    
                </div>
                <div class="col-md-3">
                <div id="address-box">
                        <div id="address-list">
                            
                           
                        </div>
                    </div> 
                </div>
            </div>
            <div id="footer-menu-box">
                <p class="text-center">Copyright &#169; 2021  Motul Hediye Tüm Hakları Saklıdır. Powered by<a href="https://www.tutkal.com.tr/" target="_blank">  Tutkal Bilişim</a> </p>
            </div>
        </div> 
</footer>
