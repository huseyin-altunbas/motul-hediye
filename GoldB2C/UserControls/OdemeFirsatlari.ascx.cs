﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;

public partial class UserControls_OdemeFirsatlari : System.Web.UI.UserControl
{
    OdemeFirsatBLL bllOdemeFirsat = new OdemeFirsatBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int UyelikDurum = 0;
            if (Session["Kullanici"] != null)
                UyelikDurum = 1;

            rptOdemeFirsatlari.DataSource = bllOdemeFirsat.GetirOdemeFirsatlari(UyelikDurum);
            rptOdemeFirsatlari.DataBind();
        }

    }
}