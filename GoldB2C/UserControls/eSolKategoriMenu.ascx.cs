﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Goldb2cBLL;
using Goldb2cEntity;

public partial class UserControls_eSolKategoriMenu : System.Web.UI.UserControl
{
    UrunGrupIslemleriBLL bllUrunGrupIslemleri = new UrunGrupIslemleriBLL();

    private int _UrunGrupId;
    public int UrunGrupId
    {
        get { return _UrunGrupId; }
        set { _UrunGrupId = value; }
    }

    private CagiranSayfa cagiranSayfa;
    public CagiranSayfa CagiranSayfa
    {
        get { return cagiranSayfa; }
        set { cagiranSayfa = value; }
    }

    public string KategorilerBaslik;


    Kategori ustKategori1;
    Kategori ustKategori2;
    Kategori ustKategori3;
    Kategori ustKategori4;
    Kategori ustKategori5;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (UrunGrupId > 0)
        {
            DataTable dtAyakIzi = new DataTable();
            dtAyakIzi = bllUrunGrupIslemleri.GetUrunGrupIzi(UrunGrupId);


            if (dtAyakIzi.Rows.Count > 0)
            {
                ustKategori1 = new Kategori();
                ustKategori1.UrunGrupId = dtAyakIzi.Rows[0]["urunust_id1"].ToString();
                ustKategori1.UrunGrupAd = dtAyakIzi.Rows[0]["urunust_ad1"].ToString();
                ustKategori1.UrunSayisi = dtAyakIzi.Rows[0]["urun_sayisi1"].ToString();

                ustKategori2 = new Kategori();
                ustKategori2.UrunGrupId = dtAyakIzi.Rows[0]["urunust_id2"].ToString();
                ustKategori2.UrunGrupAd = dtAyakIzi.Rows[0]["urunust_ad2"].ToString();
                ustKategori2.UrunSayisi = dtAyakIzi.Rows[0]["urun_sayisi2"].ToString();

                ustKategori3 = new Kategori();
                ustKategori3.UrunGrupId = dtAyakIzi.Rows[0]["urunust_id3"].ToString();
                ustKategori3.UrunGrupAd = dtAyakIzi.Rows[0]["urunust_ad3"].ToString();
                ustKategori3.UrunSayisi = dtAyakIzi.Rows[0]["urun_sayisi3"].ToString();

                ustKategori4 = new Kategori();
                ustKategori4.UrunGrupId = dtAyakIzi.Rows[0]["urunust_id4"].ToString();
                ustKategori4.UrunGrupAd = dtAyakIzi.Rows[0]["urunust_ad4"].ToString();
                ustKategori4.UrunSayisi = dtAyakIzi.Rows[0]["urun_sayisi4"].ToString();

                ustKategori5 = new Kategori();
                ustKategori5.UrunGrupId = dtAyakIzi.Rows[0]["urunust_id5"].ToString();
                ustKategori5.UrunGrupAd = dtAyakIzi.Rows[0]["urunust_ad5"].ToString();
                ustKategori5.UrunSayisi = dtAyakIzi.Rows[0]["urun_sayisi5"].ToString();

                if (ustKategori1.UrunGrupId == UrunGrupId.ToString())
                {
                    rptKategori2.DataSource = GetirUrunGrupListesi(UrunGrupId);
                    rptKategori2.DataBind();
                }
                else
                    SetRepeaterDatasource(rptKategori1, ustKategori1);

                if (ustKategori2.UrunGrupId == UrunGrupId.ToString())
                {
                    rptKategori3.DataSource = GetirUrunGrupListesi(UrunGrupId);
                    rptKategori3.DataBind();
                }
                else
                    SetRepeaterDatasource(rptKategori2, ustKategori2);

                if (ustKategori3.UrunGrupId == UrunGrupId.ToString())
                {
                    rptKategori4.DataSource = GetirUrunGrupListesi(UrunGrupId);
                    rptKategori4.DataBind();
                }
                else
                    SetRepeaterDatasource(rptKategori3, ustKategori3);

            }
        }
        else
            Response.Redirect("~/default.aspx", false);
    }

    public void SetRepeaterDatasource(Repeater repeater, Kategori kategori)
    {
        List<Kategori> _kategori = new List<Kategori>();
        _kategori.Add(kategori);

        repeater.DataSource = _kategori;
        repeater.DataBind();
    }

    public List<Kategori> GetirUrunGrupListesi(int UrunGrupId)
    {
        List<Kategori> kategoriler = new List<Kategori>();
        //DataTable dtUrunGrupListesi = new DataTable();
        //dtUrunGrupListesi = bllUrunGrupIslemleri.GetUrunGrupListesi(UrunGrupId);

        //if (dtUrunGrupListesi.Rows.Count > 0)
        //{
        //    foreach (DataRow dr in dtUrunGrupListesi.Rows)
        //    {
        //        Kategori kategori = new Kategori();
        //        kategori.UrunGrupAd = dr["urun_ad"].ToString();
        //        kategori.UrunGrupId = dr["urun_id"].ToString();

        //        kategoriler.Add(kategori);
        //    }
        //}

        return kategoriler;
    }
}

public enum CagiranSayfa
{
    Vitrin,
    UrunListeleme
}