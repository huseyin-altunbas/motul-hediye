﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CokSatanlar.ascx.cs" Inherits="UserControls_CokSatanlar" %>
<link href="/css/hovermenu.css" rel="stylesheet" type="text/css" />
<asp:Repeater ID="rptCokSatanlar" runat="server">
    <HeaderTemplate>
        <div class="container">
            <p style="font-size: 14px; font-weight: bold;">
                En Çok Tercih Edilen Ürünler</p>
            <hr />
            <div class="main">
    </HeaderTemplate>
    <ItemTemplate>
        <div class="view view-first">
            <img style="width: 179px; height: 179px; padding: 4px;" src='<%# DataBinder.Eval(Container.DataItem,"resim_ad") %>'
                alt="<%# DataBinder.Eval(Container.DataItem,"urun_ad") %>" />
            <a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>" class="mask">
                <h2>
                    <%# DataBinder.Eval(Container.DataItem,"urun_ad") %></h2>
                <p>
                    <img src="/images/search.png" style="padding-left: 45px;" width="50" height="50"></p>
                <a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>" title="<%# DataBinder.Eval(Container.DataItem,"urun_ad") %>"
                    class="info">
                    <%# Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "brut_tutar")) > Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")) ?
                                                                                                                                                                                                    "<span>" + Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "brut_tutar")), 2).ToString("###,###,###") + " PUAN" + "</span>" + " " + Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")), 2).ToString("###,###,###") + " PUAN" :
                                                                                                                                                                                                Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")), 2).ToString("###,###,###")+" PUAN"%>
                </a></a>
        </div>
    </ItemTemplate>
    <FooterTemplate>
        </div> </div>
    </FooterTemplate>
</asp:Repeater>
