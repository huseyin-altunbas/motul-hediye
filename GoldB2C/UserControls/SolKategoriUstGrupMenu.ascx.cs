﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;

public partial class UserControls_SolKategoriUstGrupMenu : System.Web.UI.UserControl
{

    private int _UrunGrupId;
    public int UrunGrupId
    {
        get { return _UrunGrupId; }
        set { _UrunGrupId = value; }
    }

    UrunGrupIslemleriBLL bllUrunGrupIslemleri = new UrunGrupIslemleriBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (UrunGrupId > 0)
        {
            DataTable dt = bllUrunGrupIslemleri.GetUstUrunGrupListesi(UrunGrupId);
            dt = LinkDegistir(dt);
            rptUstUrunGrup.DataSource = dt;
            rptUstUrunGrup.DataBind();
        }
    }

    DataTable LinkDegistir(DataTable dt)
    {
        DataColumn dcLink = new DataColumn("link");
        dt.Columns.Add(dcLink);

        foreach (DataRow dr in dt.Rows)
        {
            dr["link"] = !string.IsNullOrEmpty(dr["url_rewrite"].ToString()) ? (dr["url_rewrite"].ToString().Split('_').Length == 2 ? "/" + dr["url_rewrite"].ToString().Split('_')[1].ToString().ToUpper() + dr["urun_id"].ToString() + dr["url_rewrite"].ToString().Split('_')[0].ToString() : dr["url_rewrite"].ToString()) : "";
        }

        return dt;
    }
}