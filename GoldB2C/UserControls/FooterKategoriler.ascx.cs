﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Goldb2cBLL;
using System.Text;

public partial class UserControls_FooterKategoriler : System.Web.UI.UserControl
{
    UrunGrupIslemleriBLL bllUrunGrupIslemleri = new UrunGrupIslemleriBLL();

    DataTable dtFooterVeri;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            FooterVeriDoldur();
        }
    }

    private void FooterVeriDoldur()
    {
        dtFooterVeri = bllUrunGrupIslemleri.GetFooterVeri(0);

        StringBuilder sbKategoriler = new StringBuilder();

        sbKategoriler.AppendLine("<div class=\"fl\">");
        sbKategoriler.AppendLine("<ul>");
        sbKategoriler.AppendLine("<li class=\"categoryTitle\">Kategoriler</li>");

        for (int i = 0; i < dtFooterVeri.Rows.Count; i++)
        {
            if (i == 5 || i == 11 || i == 17 || i == 23)
                sbKategoriler.AppendLine("</ul></div><div class=\"fl\"><ul>");

            sbKategoriler.AppendLine("<li><a title=\"" + dtFooterVeri.Rows[i]["urun_ad"].ToString() + "\" href=\"" + dtFooterVeri.Rows[i]["url_rewrite"].ToString() + "\">" + dtFooterVeri.Rows[i]["urun_ad"].ToString() + "</a></li>");

            if (i == 28)
                sbKategoriler.AppendLine("</ul></div>");
        }

        //ltrKategoriler.Text = sbKategoriler.ToString();

    }

}