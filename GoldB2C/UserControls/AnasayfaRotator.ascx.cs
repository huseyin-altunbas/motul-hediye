﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Goldb2cBLL;
using System.Text;

public partial class UserControls_AnasayfaRotator : System.Web.UI.UserControl
{
    BannerBLL bllBanner = new BannerBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dtBanner = new DataTable();
            dtBanner = bllBanner.GetirBanner(1);
            if (dtBanner.Rows.Count > 0)
                ltrRotatorBanner.Text = dtBanner.Rows[0]["BannerHtml"].ToString();

        }
    }
}