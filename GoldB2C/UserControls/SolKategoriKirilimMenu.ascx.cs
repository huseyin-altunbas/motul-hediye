﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class UserControls_SolKategoriKirilimMenu : System.Web.UI.UserControl
{
    private int _UrunGrupId;
    public int UrunGrupId
    {
        get { return _UrunGrupId; }
        set { _UrunGrupId = value; }
    }

    public string DigerKategoriVar { get; set; }

    UrunGrupIslemleriBLL bllUrunGrupIslemleri = new UrunGrupIslemleriBLL();

    DataTable dtUrunGrupKart;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (UrunGrupId > 0)
        {
            dtUrunGrupKart = new DataTable();
            dtUrunGrupKart = bllUrunGrupIslemleri.GetUrunGrupListesi(UrunGrupId);

            DataTable dtOrtaSeviye = new DataTable();
            var OrtaSeviye = from urungrup in dtUrunGrupKart.AsEnumerable()
                             where urungrup.Field<string>("urun_seviye") == "Orta Seviye"
                             select urungrup;

            if (OrtaSeviye.Any())
                dtOrtaSeviye = OrtaSeviye.CopyToDataTable();

            dtOrtaSeviye = LinkDegistir(dtOrtaSeviye);

            rptKategoriler.DataSource = dtOrtaSeviye;
            rptKategoriler.DataBind();

            DataTable dtAyakIzi = new DataTable();
            dtAyakIzi = bllUrunGrupIslemleri.GetUrunGrupIzi(UrunGrupId);
        }
    }

    protected void rptKategoriler_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        int UrunGrupId = Convert.ToInt32(((DataRowView)(e.Item.DataItem)).Row["urun_id"]);

        Repeater rptKategorilerIcerik = new Repeater();
        rptKategorilerIcerik = ((Repeater)e.Item.FindControl("rptKategorilerIcerik"));

        Repeater rptKategorilerIcerikDiger = new Repeater();
        rptKategorilerIcerikDiger = ((Repeater)e.Item.FindControl("rptKategorilerIcerikDiger"));

        DataTable dtIcerik = new DataTable();
        DataTable dtDiger = new DataTable();

        DataTable dtSonSeviye = new DataTable();
        var SonSeviye = from urungrup in dtUrunGrupKart.AsEnumerable()
                        where urungrup.Field<int>("usturun_id") == UrunGrupId
                        select urungrup;

        if (SonSeviye.Any())
            dtSonSeviye = SonSeviye.CopyToDataTable();

        if (dtSonSeviye.Rows.Count > 7)
        {

            var Icerik = (from urungrup in dtSonSeviye.AsEnumerable()
                          where urungrup.Field<int>("usturun_id") == UrunGrupId
                          select urungrup).Take(7);

            if (Icerik.Any())
                dtIcerik = Icerik.CopyToDataTable();

            var Diger = (from urungrup in dtSonSeviye.AsEnumerable()
                         where urungrup.Field<int>("usturun_id") == UrunGrupId
                         select urungrup).Skip(7);

            if (Diger.Any())
                dtDiger = Diger.CopyToDataTable();

            dtIcerik = LinkDegistir(dtIcerik);
            rptKategorilerIcerik.DataSource = dtIcerik;
            rptKategorilerIcerik.DataBind();

            dtDiger = LinkDegistir(dtDiger);
            rptKategorilerIcerikDiger.DataSource = dtDiger;
            rptKategorilerIcerikDiger.DataBind();

        }
        else
        {
            ((HtmlControl)e.Item.FindControl("DivDigerKategoriler")).Visible = false;

            dtSonSeviye = LinkDegistir(dtSonSeviye);

            rptKategorilerIcerik.DataSource = dtSonSeviye;
            rptKategorilerIcerik.DataBind();
        }

    }

    protected void rptKategorilerIcerik_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }

    DataTable LinkDegistir(DataTable dt)
    {
        DataColumn dcLink = new DataColumn("link");
        dt.Columns.Add(dcLink);

        foreach (DataRow dr in dt.Rows)
        {
            dr["link"] = !string.IsNullOrEmpty(dr["url_rewrite"].ToString()) ? (dr["url_rewrite"].ToString().Split('_').Length == 2 ? "/" + dr["url_rewrite"].ToString().Split('_')[1].ToString().ToUpper() + dr["urun_id"].ToString() + dr["url_rewrite"].ToString().Split('_')[0].ToString() : dr["url_rewrite"].ToString()) : "";
        }

        return dt;
    }
}