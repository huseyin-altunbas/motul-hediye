﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Goldb2cEntity;
using Goldb2cBLL;
using System.Data;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using Goldb2cBLL.Custom;
using Goldb2cEntity.Custom;

public partial class UserControls_Header : System.Web.UI.UserControl
{
    UrunGrupIslemleriBLL bllUrunGrupIslemleri = new UrunGrupIslemleriBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    Kullanici kullanici;


    #region Page Init
    protected void Page_Init(object sender, EventArgs e)
    {
        txtSearch.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnSearch.ClientID + "').click();return false;}} else {return true;} ");
    }
    #endregion
    TransactionBLL transactionBLL = new TransactionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Helper.PuanSorgula();
            DataTable dtKategori = bllUrunGrupIslemleri.GetUrunGrupTip("1-Grup");

            var vKategori = dtKategori.AsEnumerable()
                .Where(a => a.Field<string>("urun_ad") != string.Empty)
                .OrderBy(b => b.Field<string>("urun_ad"))
                .Select(c => new
                {
                    UrunAd = c.Field<string>("urun_ad").Length > 20 ? c.Field<string>("urun_ad").ToUpper().Substring(0, 20) + "..." : c.Field<string>("urun_ad").ToUpper(),
                    UrunId = c.Field<int>("urun_id")
                }).ToList();

            var vKategori1 = vKategori.RemoveAll(a => a.UrunAd.Contains("ALÇI"));

            var distinctItems = vKategori.GroupBy(x => x.UrunAd).Select(y => y.First());
            drpCategories.DataTextField = "UrunAd";
            drpCategories.DataValueField = "UrunId";
            drpCategories.DataSource = distinctItems;
            drpCategories.DataBind();
            drpCategories.Items.Insert(0, (new ListItem("Tüm Kategoriler", string.Empty)));
        }

        if (Session["Kullanici"] != null)
        {
            KullaniciBarAcKapa(true);

            //    //UyeGirisiCell.Visible = false;
            //    //HesabimCell.Visible = true;
            Kullanici kullanici = (Kullanici)Session["Kullanici"];

            PuanView puanView = transactionBLL.KullaniciPuanGetir(kullanici.KullaniciId);
            Session["totalGoldPoint"] = puanView.kalan;



            if (kullanici.Email == "murat.balci@bigesclub.com" || kullanici.Email == "mustafa.ayar@bigesclub.com")
            {
                lblKullaniciAdSoyad.Text = Server.MachineName + " " + kullanici.KullaniciAd + " " + kullanici.KullaniciSoyad;
                if (puanView.kalan != 0)
                { lblToplamPuan.Text = "Kullanılabilir Puanınız : " + (Convert.ToInt32(Session["totalGoldPoint"])).ToString("###,###,###"); }
                else
                { lblToplamPuan.Text = "Kullanılabilir Puanınız : " + Session["totalGoldPoint"].ToString(); }

                //lblAvansPuan.Text = "Kullanabileceğiniz Avans Puanınız : " + (Convert.ToInt32(Session["totalPointAdvance"])).ToString("###,###,###");
                if (Convert.ToInt32(Session["totalGoldPoint"]) <= 0) lblToplamPuan.Text = "Kullanılabilir Puanınız : " + "0";
                //if (Convert.ToInt32(Session["totalPointAdvance"]) <= 0) lblAvansPuan.Text = "Kullanabileceğiniz Avans Puanınız : " + "0";


            }

            else
            {
                lblKullaniciAdSoyad.Text = kullanici.KullaniciAd + " " + kullanici.KullaniciSoyad;
                if (puanView.kalan != 0)
                {
                    lblToplamPuan.Text = "Kullanılabilir Puanınız : " + (Convert.ToInt32(Session["totalGoldPoint"])).ToString("###,###,###");
                }
                else
                {
                    lblToplamPuan.Text = "Kullanılabilir Puanınız : " + Session["totalGoldPoint"].ToString();
                }

                //lblAvansPuan.Text = "Kullanabileceğiniz Avans Puanınız : " + (Convert.ToInt32(Session["totalPointAdvance"])).ToString("###,###,###");
                //if (Convert.ToInt32(Session["totalGoldPoint"]) <= 0) lblToplamPuan.Text = "Kullanılabilir Puanınız : " + "0";
                //if (Convert.ToInt32(Session["totalPointAdvance"]) <= 0) lblAvansPuan.Text = "Kullanabileceğiniz Avans Puanınız : " + "0";
            }

            if (!string.IsNullOrEmpty(kullanici.FacebookId))
            {
                imgFacebook.Visible = true;
                imgFacebook.ImageUrl = "https://graph.facebook.com/" + kullanici.FacebookId + "/picture";
            }
            else
                imgFacebook.Visible = false;


            string silinecekPuan = "";
            if (kullanici.KullaniciId == 17487 || kullanici.KullaniciId == 15384 || kullanici.KullaniciId == 21030)
            {
                silinecekPuan = "31.01.2021 de Silinecek Puanınız  : ";
            }
            else
            {
                silinecekPuan = "31.12.2021 de Silinecek Puanınız  : ";
            }


            if (puanView.kalan != 0)
            {
                if (puanView.silinecek > 0)
                {
                    lblSilinecekPuan.Text = silinecekPuan + puanView.silinecek.ToString("###,###,###");
                }
                else
                {
                    lblSilinecekPuan.Text = silinecekPuan + " 0";
                }

            }
            else
            {
                lblSilinecekPuan.Text = silinecekPuan + puanView.silinecek;
            }
        }
        // eğer session yoksa  cookie control et
        else if (Session["Kullanici"] == null && Request.Cookies["User"] != null && Request.Cookies["User"]["Email"] != null && Request.Cookies["User"]["Sifre"] != null)
        {
            Crypt crypt = new Crypt();
            string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
            string Email = crypt.DecryptData(encryptKey, Request.Cookies["User"]["Email"].ToString());
            string Sifre = crypt.DecryptData(encryptKey, Request.Cookies["User"]["Sifre"].ToString());

            int kullaniciID = bllUyelikIslemleri.MusteriKullaniciGiris(Email, Sifre);
            if (kullaniciID > 0)
            {

                //        if (sepet.GetGeciciSepetMasterID(kullaniciID) != null && sepet.GetGeciciSepetMasterID(kullaniciID) != "")
                //            Session["MasterID"] = sepet.GetGeciciSepetMasterID(kullaniciID);

                Kullanici kullanici = bllUyelikIslemleri.KullaniciSistemKayit(kullaniciID);

                //        bool IndirimKodCiksin = false;
                //        string IndirimKod = "";
                //        hesabim.GetIndirimKod(((sUser)Session["sUser"]).ERPKod, out IndirimKodCiksin, out IndirimKod);
                //        sUser.IndirimKod = IndirimKod;
                //        //  Session.Add("sUser", sUser);


                KullaniciBarAcKapa(true);
                lblKullaniciAdSoyad.Text = ((Kullanici)Session["Kullanici"]).KullaniciAd + " " + ((Kullanici)Session["Kullanici"]).KullaniciSoyad + " " + HttpContext.Current.Session["totalPointAdvance"].ToString() + "";

                imgFacebook.Visible = false;
            }
            else
            {
                KullaniciBarAcKapa(false);
            }

        }
        // session yoksa
        else
        {
            KullaniciBarAcKapa(false);
            HttpCookie cookie = new HttpCookie("HNetLogin");
            cookie.Value = "false";
            DateTime dtNow = DateTime.Now;
            cookie.Expires = dtNow;
            Response.Cookies.Add(cookie);
        }
    }

    private void KullaniciBarAcKapa(bool GirisYapti)
    {
        divOpen.Visible = !GirisYapti;
        divClose.Visible = GirisYapti;
    }

    protected void lnbCikis_Click(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {
            bllUyelikIslemleri.MusteriKullaniciCikis(((Kullanici)Session["Kullanici"]).KullaniciId);
            Session.Abandon();
        }

        Response.Redirect("/", false);
    }

    protected void btnFacebookBaglan_Click(object sender, EventArgs e)
    {
        ///*string FacebookAppID = CustomConfiguration.Facebook_client_id;
        //string FacebookCallbackURL = CustomConfiguration.Facebook_redirect_url;
        //string serviceURL = string.Format("https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}&scope=email,user_birthday",
        //FacebookAppID, FacebookCallbackURL);
        //if (!string.IsNullOrEmpty(Request.UrlReferrer.OriginalString))
        //    Session["FbConnectSonUrl"] = Request.UrlReferrer.OriginalString;
        //else
        //    Session["FbConnectSonUrl"] = "~/MusteriHizmetleri/Hosgeldiniz.aspx";
        //Response.Redirect(serviceURL);*/
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string searchUrl = string.Empty;
        if (drpCategories.SelectedValue == string.Empty)
            searchUrl = string.Format("/Arama.aspx?text={0}", HttpUtility.UrlEncode(txtSearch.Text.Trim()));
        else
            searchUrl = string.Format("/Arama.aspx?category={0}&text={1}", drpCategories.SelectedValue, HttpUtility.UrlEncode(txtSearch.Text.Trim()));
        Response.Redirect(searchUrl, false);
    }
}