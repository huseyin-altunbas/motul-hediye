﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class UserControls_HitUrunlerMenu : System.Web.UI.UserControl
{
    string HitUrunTip;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    DataTable dtAnaKategorilerView;
    DataTable dtHitUrunKategoriler;
    public void MenuOlustur(DataTable _dtHitUrunKategoriler, string SeciliUstGrupId)
    {
        if (_dtHitUrunKategoriler != null && _dtHitUrunKategoriler.Rows.Count > 0)
        {
            dtHitUrunKategoriler = _dtHitUrunKategoriler;

            if (!string.IsNullOrEmpty(dtHitUrunKategoriler.Rows[0]["hiturun_tip"].ToString()))
                HitUrunTip = dtHitUrunKategoriler.Rows[0]["hiturun_tip"].ToString();

            DataTable dtAnaKategorilerView = new DataTable();
            DataView dvAnaKategorilerView = new DataView(dtHitUrunKategoriler);
            dtAnaKategorilerView = dvAnaKategorilerView.ToTable(true, new string[] { "ustgrup_id", "ustgrup_ad" });
            dtAnaKategorilerView = dtAnaKategorilerView.Select("ustgrup_ad <> ''").CopyToDataTable();

            string SeciliUstGrupRowIndex = "";
            for (int i = 0; i < dtAnaKategorilerView.Rows.Count; i++)
            {
                if (dtAnaKategorilerView.Rows[i]["ustgrup_id"].ToString() == SeciliUstGrupId)
                    SeciliUstGrupRowIndex = i.ToString();
            }

            if (!string.IsNullOrEmpty(SeciliUstGrupRowIndex))
                ltrAccordionScript.Text = "<script type=\"text/javascript\">$(document).ready(function () {$(\"#accordion\").accordion({ autoHeight: false, active: " + SeciliUstGrupRowIndex + "  });});</script>";
            else
                ltrAccordionScript.Text = "<script type=\"text/javascript\">$(document).ready(function () {$(\"#accordion\").accordion({ autoHeight: false});});</script>";

            rptHitUrunlerMenu.DataSource = dtAnaKategorilerView;
            rptHitUrunlerMenu.DataBind();


        }
    }
    protected void rptHitUrunlerMenu_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptHitUrunlerDetay = (Repeater)e.Item.FindControl("rptHitUrunlerMenuDetay");
            Literal ltrUrunSayisi = (Literal)e.Item.FindControl("ltrUrunSayisi");
            string UstGrupId = ((DataRowView)e.Item.DataItem).Row["ustgrup_id"].ToString();
            DataRow[] drKategoriler = dtHitUrunKategoriler.Select("ustgrup_id = '" + UstGrupId + "'");
            int UrunSayisi = 0;
            foreach (DataRow dr in drKategoriler)
            {
                UrunSayisi = UrunSayisi + Convert.ToInt32(dr["urun_sayisi"]);

                string url_rewrite = dr["url_rewrite"].ToString();

                if (url_rewrite.IndexOf('/') > -1 && url_rewrite.LastIndexOf('/') > 1)
                {
                    url_rewrite = url_rewrite.Substring(url_rewrite.LastIndexOf('/') + 1, url_rewrite.Length - url_rewrite.LastIndexOf('/') - 1);
                }

                dr["url_rewrite"] = "/HIT" + dr["hiturun_tip"].ToString() + "." + dr["ustgrup_id"].ToString() + "." + dr["altgrup_id"].ToString() + "/" + url_rewrite;

            }

            ltrUrunSayisi.Text = UrunSayisi.ToString();

            rptHitUrunlerDetay.DataSource = drKategoriler;
            rptHitUrunlerDetay.DataBind();
        }

    }
}