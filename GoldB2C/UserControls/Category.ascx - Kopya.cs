﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using System.Text.RegularExpressions;

public partial class UserControls_Category : System.Web.UI.UserControl
{
    public int CategoryMasterId { get; set; }

    UrunListelemeBLL bllUrunListeleme = new UrunListelemeBLL();
    UrunGrupIslemleriBLL bllUrunGrupIslemleri = new UrunGrupIslemleriBLL();
    OzelKategoriBLL bllOzelKategori = new OzelKategoriBLL();
    DataTable dtUrunGrupKod;
    DataTable dtOzelKategoriMaster;
    DataTable dtOzelKategoriDetay;
    protected void Page_Load(object sender, EventArgs e)
    {
        dtOzelKategoriMaster = bllOzelKategori.OzelKategoriMaster(CategoryMasterId);
        int masterId, detayId;
        string kategoriUrl = string.Empty;
        string kategoriAd = string.Empty;
        string kategoriImage = string.Empty;
        string kategoriBanner = string.Empty;
        string urunGrupId = string.Empty;
        int _urunGrupId = 0;
        foreach (DataRow item in dtOzelKategoriMaster.Rows)
        {
            if (item["master_id"] != null && item["detay_id"] != null)
            {
                Int32.TryParse(item["master_id"].ToString(), out masterId);
                Int32.TryParse(item["detay_id"].ToString(), out detayId);

                if (item["kategori_url"] != null)
                {
                    kategoriUrl = item["kategori_url"].ToString();
                    if (kategoriUrl != string.Empty)
                    {
                        string[] urunGrupIds = kategoriUrl.Split('/');
                        if (urunGrupIds.Length > 1)
                        {
                            urunGrupId = Regex.Match(urunGrupIds[1], @"\d+").Value;
                            Int32.TryParse(urunGrupId, out _urunGrupId);
                        }
                    }
                }

                //category name
                if (item["kategori_ad"] != null)
                    kategoriAd = item["kategori_ad"].ToString();
                if (kategoriAd.Length > 10)
                    kategoriAd = kategoriAd.Substring(0, 10) + "...";

                //category image
                if (item["kategori_imaj"] != null)
                    kategoriImage = item["kategori_imaj"].ToString();
                ltrFeaturedImage.Text = string.Format("<a href=\"#\"><img alt=\"{0}\" class=\"img-responsive lazy\" data-original=\"{1}\"/></a>", kategoriAd, kategoriImage);

                //category banner
                //if (item["kategori_banner"] != null)
                //    kategoriBanner = item["kategori_banner"].ToString();
                //ltrCategoryBanner.Text = kategoriBanner;

                //category top left
                string category = string.Format("<div class=\"navbar-brand\"><a href=\"{0}\"><img alt=\"\" data-original=\"\" />{1}</a></div>", kategoriUrl, kategoriAd);
                ltrCategory.Text = category;

                //category top right
                if (_urunGrupId > 0)
                {
                    DataTable dtUrunGrupBilgisi = bllUrunGrupIslemleri.GetUrunGrupBilgisi(_urunGrupId);
                    if (dtUrunGrupBilgisi != null && dtUrunGrupBilgisi.Rows.Count > 0)
                    {
                        if (dtUrunGrupBilgisi.Rows[0]["urun_seviye"] != null && dtUrunGrupBilgisi.Rows[0]["urun_seviye"] != string.Empty)
                        {
                            string urunSeviye = dtUrunGrupBilgisi.Rows[0]["urun_seviye"].ToString();

                            //Sub Category
                            if (urunSeviye == "1-Grup")
                            {
                                dtUrunGrupKod = bllUrunGrupIslemleri.GetUrunGrupKod(_urunGrupId.ToString(), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                                var vUrunGrupKod = dtUrunGrupKod.AsEnumerable().Where(k => k.Field<string>("urun_seviye") == "2-Grup");
                                dtUrunGrupKod = vUrunGrupKod.Count() > 0 ? vUrunGrupKod.Take(5).CopyToDataTable() : dtUrunGrupKod.Clone();
                            }
                            else if (urunSeviye == "2-Grup")
                            {
                                dtUrunGrupKod = bllUrunGrupIslemleri.GetUrunGrupKod(string.Empty, _urunGrupId.ToString(), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                                var vUrunGrupKod = dtUrunGrupKod.AsEnumerable().Where(k => k.Field<string>("urun_seviye") == "3-Grup");
                                dtUrunGrupKod = vUrunGrupKod.Count() > 0 ? vUrunGrupKod.Take(5).CopyToDataTable() : dtUrunGrupKod.Clone();
                            }
                            else if (urunSeviye == "3-Grup")
                            {
                                dtUrunGrupKod = bllUrunGrupIslemleri.GetUrunGrupKod(string.Empty, string.Empty, _urunGrupId.ToString(), string.Empty, string.Empty, string.Empty, string.Empty);
                                var vUrunGrupKod = dtUrunGrupKod.AsEnumerable().Where(k => k.Field<string>("urun_seviye") == "4-Grup");
                                dtUrunGrupKod = vUrunGrupKod.Count() > 0 ? vUrunGrupKod.Take(5).CopyToDataTable() : dtUrunGrupKod.Clone();
                            }
                            else if (urunSeviye == "4-Grup")
                            {
                                dtUrunGrupKod = bllUrunGrupIslemleri.GetUrunGrupKod(string.Empty, string.Empty, string.Empty, _urunGrupId.ToString(), string.Empty, string.Empty, string.Empty);
                                var vUrunGrupKod = dtUrunGrupKod.AsEnumerable().Where(k => k.Field<string>("urun_seviye") == "5-Grup");
                                dtUrunGrupKod = vUrunGrupKod.Count() > 0 ? vUrunGrupKod.Take(5).CopyToDataTable() : dtUrunGrupKod.Clone();
                            }
                            else if (urunSeviye == "5-Grup")
                            {
                                dtUrunGrupKod = bllUrunGrupIslemleri.GetUrunGrupKod(string.Empty, string.Empty, string.Empty, string.Empty, _urunGrupId.ToString(), string.Empty, string.Empty);
                                var vUrunGrupKod = dtUrunGrupKod.AsEnumerable().Where(k => k.Field<string>("urun_seviye") == "6-Grup");
                                dtUrunGrupKod = vUrunGrupKod.Count() > 0 ? vUrunGrupKod.Take(5).CopyToDataTable() : dtUrunGrupKod.Clone();
                            }
                            else if (urunSeviye == "6-Grup")
                            {
                                dtUrunGrupKod = bllUrunGrupIslemleri.GetUrunGrupKod(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, _urunGrupId.ToString(), string.Empty);
                                var vUrunGrupKod = dtUrunGrupKod.AsEnumerable().Where(k => k.Field<string>("urun_seviye") == "7-Grup");
                                dtUrunGrupKod = vUrunGrupKod.Count() > 0 ? vUrunGrupKod.Take(5).CopyToDataTable() : dtUrunGrupKod.Clone();
                            }


                            if (dtUrunGrupKod != null && dtUrunGrupKod.Rows.Count > 0)
                            {
                                rptSubCategory.DataSource = dtUrunGrupKod;
                                rptSubCategory.DataBind();
                            }

                        }
                    }
                }
                //category elevator
                int elevatorId = 0;
                if (Session["ElevatorId"] != null && Int32.TryParse(Session["ElevatorId"].ToString(), out elevatorId))
                {
                    string elevator = string.Empty;
                    elevator = string.Format("<div id=\"elevator-{0}\" class=\"floor-elevator\">"
                            + "<a href=\"#elevator-{1}\" class=\"btn-elevator up fa fa-angle-up\"></a>"
                            + "<a href=\"#elevator-{2}\" class=\"btn-elevator down fa fa-angle-down\"></a>"
                            + "</div>", elevatorId.ToString(), (elevatorId - 1).ToString(), (elevatorId + 1).ToString());
                    ltrElevator.Text = elevator;

                    Session["ElevatorId"] = elevatorId + 1;
                }

                if (masterId > 0 && detayId > 0)
                {
                    //category detail
                    dtOzelKategoriDetay = bllOzelKategori.OzelKategoriDetay(masterId, detayId);
                    if (dtOzelKategoriDetay != null && dtOzelKategoriDetay.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtOzelKategoriDetay.Rows)
                        {
                            if (dr["resim_ad"].ToString().IndexOf("http") == -1)
                            {
                                dr["resim_ad"] = "http://cdn.gold.com.tr/UrunResim/MegaResim/" + dr["resim_ad"].ToString();
                            }
                        }

                        rptCategory.DataSource = dtOzelKategoriDetay;
                        rptCategory.DataBind();
                    }
                }
            }
        }
    }

    public string GetNavMenuColor()
    {
        Dictionary<int, string> dicNavMenuClass = new Dictionary<int, string>()
        {
            {1, "nav-menu-red"},
            {2, "nav-menu-green"},
            {3, "nav-menu-orange"},
            {4, "nav-menu-blue"},
            {5, "nav-menu-gray"}
        };

        string navMenuClass = "nav-menu-red";
        int navMenuId = 0;
        if (Session["NavMenuId"] != null && Int32.TryParse(Session["NavMenuId"].ToString(), out navMenuId))
        {
            navMenuClass = dicNavMenuClass[navMenuId % dicNavMenuClass.Count];
            Session["NavMenuId"] = navMenuId + 1;
        }
        return navMenuClass;
    }
}