﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AnasayfaRotator.ascx.cs"
    Inherits="UserControls_AnasayfaRotator" %>
<script src="/Tools/li-slider/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<link href="/Tools/li-slider/css/jquery.lightbox-0.5.css" rel="stylesheet" type="text/css" />
<link href="/Tools/li-slider/css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<link href="/Tools/li-slider/skins/Pure/skin.css" rel="stylesheet" type="text/css" />
<script src="/Tools/li-slider/js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="/Tools/li-slider/js/jquery.lightbox-0.5.min.js" type="text/javascript"></script>
<script src="/Tools/li-slider/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="/Tools/li-slider/js/li-slider-animations-1.1.min.js" type="text/javascript"></script>
<script src="/Tools/li-slider/js/li-slider-1.1.js" type="text/javascript"></script>
<script type="text/javascript">
    $(window).load(
        function () {
            $("#li-banner").sp_Li_Slider({
                animation: 'Random',
                transitions: 'None',
                auto_play: true,
                repeat: true,
                modalMode: 'prettyPhoto',
                goToSlideOnStart: 1,
                timer: 'line_bottom',
                pauseOnMouseOver: true,
                tooltip: 'image',
                tooltipSize: 40,
                shuffle: false,
                delay: 3500,
                trans_period: 800,
                vert_sections: 10,
                sqr_sections_Y: 4,
                active_links: true,
                buttons_show: true,
                play_pause_show: false,
                next_prev_show: false,
                auto_hide: false,
                buttons_show_time: 300,
                buttons_show_delay: 300,
                buttons_hide_time: 300,
                buttons_hide_delay: 300

            });
        }
    );
    
</script>
<div id="li-banner" class="li-banner" style="width: 636px; height: 338px;">
    <ul>
        <asp:Literal ID="ltrRotatorBanner" runat="server"></asp:Literal>
    </ul>
</div>
