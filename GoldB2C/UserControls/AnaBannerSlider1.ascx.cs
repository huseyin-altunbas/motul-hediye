﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using System.Text;
using Goldb2cEntity;
using Goldb2cInfrastructure;

public partial class UserControls_AnaBannerSlider : System.Web.UI.UserControl
{
    BannerBLL bllBanner = new BannerBLL();
    DataTable dtBanner;
    protected void Page_Load(object sender, EventArgs e)
    {
        dtBanner = new DataTable();

        if (!IsPostBack)
        {
            BannerHtmlHazirla();
        }

    }

    private void BannerHtmlHazirla()
    {
        string ImageDomain = CustomConfiguration.ImageDomain;
        string AnaSayfaBanner = "";

        dtBanner = bllBanner.GetirBanner(1);
        if (dtBanner.Rows.Count > 0)
            AnaSayfaBanner = dtBanner.Rows[0]["BannerHtml"].ToString();

        List<entAnaSayfaBanner> lstAnaSayfaBanner = new List<entAnaSayfaBanner>();
        if (!string.IsNullOrEmpty(AnaSayfaBanner))
        {
            entAnaSayfaBanner AnaSayfaBannerEnt;
            string[] Bannerlar = AnaSayfaBanner.Split(new string[] { "||" }, StringSplitOptions.None);
            foreach (string banner in Bannerlar)
            {
                AnaSayfaBannerEnt = new entAnaSayfaBanner();
                string[] _banner = banner.Split(new string[] { "##" }, StringSplitOptions.None);
                AnaSayfaBannerEnt.BannerResim = _banner[0];
                AnaSayfaBannerEnt.BannerLink = _banner[1];
                AnaSayfaBannerEnt.AlternateTextTitle = _banner[2];

                lstAnaSayfaBanner.Add(AnaSayfaBannerEnt);
            }

            StringBuilder sbBanner = new StringBuilder();

            //Bannerlar

            int Say = 1;
            foreach (entAnaSayfaBanner banner in lstAnaSayfaBanner)
            {
                sbBanner.AppendLine("<div class=\"slider4-slide\">");
                if (string.IsNullOrEmpty(banner.BannerLink.Trim()))
                    sbBanner.AppendFormat("<img class=\"bigCampaign\" src=\"//" + ImageDomain + "/UrunResim/MarketingResim/banner/anasayfa/{0}\" alt=\"{1}\" />", banner.BannerResim.Trim(), banner.AlternateTextTitle.Trim());
                else
                    sbBanner.AppendFormat("<a href=\"{0}\" title=\"{1}\" > <img class=\"bigCampaign\" src=\"//" + ImageDomain + "/UrunResim/MarketingResim/banner/anasayfa/{2}\" alt=\"{3}\" /></a>", banner.BannerLink.Trim(), banner.AlternateTextTitle.Trim(), banner.BannerResim.Trim(), banner.AlternateTextTitle.Trim());
                sbBanner.AppendLine("</div>");
                Say++;

            }

            //Banner Thumbs
            sbBanner.AppendLine("<div class=\"slider4-ticker-wrap\">");
            foreach (entAnaSayfaBanner bannerthumb in lstAnaSayfaBanner)
            {
                if (!string.IsNullOrEmpty(bannerthumb.BannerLink))
                    sbBanner.AppendLine("<div class=\"slider4-ticker\"><img alt=\"" + bannerthumb.AlternateTextTitle + "\" onclick=\"javascript:location.href='" + bannerthumb.BannerLink + "'\" src=\"//" + ImageDomain + "/UrunResim/MarketingResim/banner/anasayfa/thumbs/" + bannerthumb.BannerResim.Trim() + "\"></div>");
                else
                    sbBanner.AppendLine("<div class=\"slider4-ticker\"><img src=\"//" + ImageDomain + "/UrunResim/MarketingResim/banner/anasayfa/thumbs/" + bannerthumb.BannerResim.Trim() + "\"></div>");

            }
            sbBanner.AppendLine("</div>");

            ltrRotatorBanner.Text = sbBanner.ToString();

        }
    }


}