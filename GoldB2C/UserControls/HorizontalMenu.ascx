﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HorizontalMenu.ascx.cs"
    Inherits="UserControls_HorizontalMenu" %>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="#">MENU</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/Hesabim/UyelikBilgilerim.aspx">Hesabım</a></li>                
                <li><a href="/Hesabim/Siparislerim.aspx">Siparişlerim</a></li>                
                <li class="dropdown">
                  <%--  <a href="http://www.biges.com/bilgilerim" class="dropdown-toggle" data-toggle="dropdown" style="display:none;">Üyelik Bilgilerim</a>--%>
                    <ul class="dropdown-menu container-fluid">
                        <li class="block-container">
                            <ul class="block">
                               <%-- <li class="link_container"><a href="http://www.biges.com/bilgilerim">Üyelik Bilgilerimi Güncelle</a></li>--%>
                                <%--<li class="link_container"><a href="/Hesabim/SifremiDegistir.aspx">Şifremi Değiştir</a></li>--%>
                                <li class="link_container"><a href="/Hesabim/AdresBilgilerim.aspx">Adres Bilgilerimi Güncelle</a></li>
                            </ul>
                        </li>
                    </ul> 
                </li>
                <li class="dropdown">
                    <a href="/MusteriHizmetleri/Iletisim.aspx" class="dropdown-toggle" data-toggle="dropdown">Müşteri Hizmetleri</a>
                    <ul class="dropdown-menu container-fluid">
                        <li class="block-container">
                            <ul class="block">
                                <li class="link_container"><a href="/MusteriHizmetleri/Iletisim.aspx">İletişim</a></li>
                                <li class="link_container" style="display:none;"><a href="/MusteriHizmetleri/Hakkimizda.aspx">Hakkımızda</a></li>
                                <li class="link_container"><a href="/MusteriHizmetleri/KargoTeslimatSartlari.aspx">Kargo Teslimat Şartları</a></li>
                                <%--<li class="link_container"><a href="/MusteriHizmetleri/PuanKazanmaKosullari.aspx">Puan Kazanma Koşulları</a></li>--%>
                                 <li class="link_container"><a href="/MusteriHizmetleri/sss.aspx">Sık Sorulan Sorular</a></li>
                                <li class="link_container"><a href="/MusteriHizmetleri/GizlilikPrensiplerimiz.aspx">Gizlilik</a></li>
                            </ul>
                        </li>
                    </ul> 
                </li>
                <li><a href="/Sepet.aspx">Sepetim</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
