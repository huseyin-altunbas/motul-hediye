﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LatestDeal.ascx.cs" Inherits="UserControls_LatestDeal" %>
<div class="latest-deals">
    <h2 class="latest-deal-title">
        Kampanyalı Ürünler</h2>
    <div class="latest-deal-content">
        <asp:Repeater ID="rptLatestDeal" runat="server">
            <HeaderTemplate>
                <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav="true"
                    data-autoplaytimeout="1000" data-autoplayhoverpause="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":1}}'>
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <div class="count-down-time" data-countdown="">
                    </div>
                    <div class="left-block">
                        <a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>">
                            <img class="img-responsive" alt="product" src='<%# DataBinder.Eval(Container.DataItem,"resim_ad") %>' /></a>
                        <div class="quick-view">
                            <a title="Add to my wishlist" class="heart" href="#"></a><a title="Add to compare"
                                class="compare" href="#"></a><a title="Quick view" class="search" href="#"></a>
                        </div>
                        <div class="add-to-cart">
                            <a title="Sepete Ekle" href="#" onclick="return SepetEkle('<%# DataBinder.Eval(Container.DataItem,"urun_id") %>');">
                                Sepete Ekle</a>
                        </div>
                    </div>
                    <div class="right-block">
                        <h5 class="product-name">
                            <a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>">
                                <%# DataBinder.Eval(Container.DataItem,"urun_ad") %></a></h5>
                        <div class="content_price">
                            <%# Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "brut_tutar")) > Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")) ? 
                        "<span class=\"price product-price\">"+ Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")), 2).ToString("###,###,###")+" PUAN</span>"
                                                    + "<span class=\"price old-price\">" + Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "brut_tutar")), 2).ToString("###,###,###") + " PUAN</span>" :
                        "<span class=\"price product-price\">"+ Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")), 2).ToString("###,###,###")+" PUAN</span>"
                            %>
                        </div>
                    </div>
                </li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>
