﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VerticalMenu.ascx.cs"
    Inherits="UserControls_VerticalMenu" %>
<div class="box-vertical-megamenus">
    <h4 class="title">
        <span class="title-menu">KATEGORİLER</span> <span class="btn-open-mobile pull-right home-page">
            <i class="fa fa-bars"></i></span>
    </h4>
    <div class="vertical-menu-content is-home">
        <ul class="vertical-menu-list">
            <asp:Literal ID="ltrVerticalMenu" runat="server"></asp:Literal>
        </ul>
        <div class="all-category">
            <span class="open-cate">Hepsi</span></div>
    </div>
</div>
