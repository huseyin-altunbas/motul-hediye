﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SolKategoriUstGrupMenu.ascx.cs"
    Inherits="UserControls_SolKategoriUstGrupMenu" %>
<div class="filterContainer">
    <asp:Repeater ID="rptUstUrunGrup" runat="server">
        <ItemTemplate>
            <div class="filterTitle">
                <a href="<%#DataBinder.Eval(Container.DataItem, "link") %>" title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                    <%#DataBinder.Eval(Container.DataItem, "urun_ad") %>&nbsp;(<%#DataBinder.Eval(Container.DataItem, "urun_sayisi") %>)</a></div>
        </ItemTemplate>
    </asp:Repeater>
</div>
