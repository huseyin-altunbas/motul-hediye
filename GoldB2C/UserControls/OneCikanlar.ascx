﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OneCikanlar.ascx.cs" Inherits="UserControls_OneCikanlar" %>
<div class="ofcontainer">
     <div class="alloffers"><%--
         <a href="#">
            <img src="imagesgld/arrow_alloffers.png" width="12" height="12" alt="Daha fazlasını listeleyiniz"
                style="margin-bottom: -2px;" />
            Daha fazlasını listeleyiniz</a>--%></div>
    <!-- offerscarousel -->
    <div id="my-carousel-3" class="carousel module">
        <ul class="mask">
            <asp:Repeater ID="rptOneCikanlar" runat="server">
                <ItemTemplate>
                    <li><a onclick="TiklamaKaydet($(this).attr('href'));" href='<%# DataBinder.Eval(Container.DataItem, "yonlenme_linki") %>'
                        title="<%# DataBinder.Eval(Container.DataItem, "imaj_aciklama") %>">
                        <div class="offerpic">
                            <img width="100px" height="100px" src="<%# DataBinder.Eval(Container.DataItem, "imaj_adres") %>"
                                alt="<%# DataBinder.Eval(Container.DataItem, "imaj_aciklama") %>" /></div>
                        <div class="offername">
                            <%# DataBinder.Eval(Container.DataItem, "ozet_aciklama") %>
                        </div>
                        <div class="offerproperties">
                            <%# DataBinder.Eval(Container.DataItem, "detay_aciklama") %></div>
                    </a></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
    <!-- offerscarousel -->
</div>
