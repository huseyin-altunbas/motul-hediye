﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;

public partial class UserControls_OneCikanlar : System.Web.UI.UserControl
{
    OneCikanlarBLL bllOneCikanlar = new OneCikanlarBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dtOneCikanlar = new DataTable();
            dtOneCikanlar = bllOneCikanlar.OneCikanlar();

            string[] Izinliler = new string[] { "/Arama.aspx?text=fiyatlar+kontrolden+cikti", "/Arama.aspx?text=hftnyldz", "/Arama.aspx?text=ozelurunkampanyasi", "/Arama.aspx?text=philips-arzumda-kampanya-basladi", "/Arama.aspx?text=zfrrn", "/Arama.aspx?text=fakirde-kampanya", "/Arama.aspx?text=wrapsol-kampanyasi", "/Arama.aspx?text=braunda-kampanya", "/Arama.aspx?text=tefal-sen-bir-harikasin", "/Arama.aspx?text=coksatanlar-kacmaz", "/Arama.aspx?text=siemens-cildirdi", "/Arama.aspx?text=alts-kmps", "/Arama.aspx?text=fiyatlar+kontrolden+cikti", "/Arama.aspx?text=lg-kampanyasi-bitmiyor" };
            List<int> SilinecekRow = new List<int>();
            for (int i = 0; i < dtOneCikanlar.Rows.Count; i++)
            {
                bool Silinecek = true;

                if (dtOneCikanlar.Rows[i]["yonlenme_linki"].ToString().IndexOf("Arama") > -1)
                {
                    foreach (string izinli in Izinliler)
                    {
                        if (dtOneCikanlar.Rows[i]["yonlenme_linki"].ToString() == izinli)
                        {
                            Silinecek = false;
                            break;
                        }
                    }
                }
                else
                {
                    Silinecek = false;
                }


                if (Silinecek)
                    SilinecekRow.Add(i);

            }

            foreach (int rowindex in SilinecekRow.Reverse<int>())
            {
                dtOneCikanlar.Rows.RemoveAt(rowindex);
            }

            rptOneCikanlar.DataSource = dtOneCikanlar;
            rptOneCikanlar.DataBind();
        }

    }
}

