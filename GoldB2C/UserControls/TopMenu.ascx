﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopMenu.ascx.cs" Inherits="UserControls_TopMenu" %>
<%--<script src="/js/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>--%>
<!--  AutoComplete  -->
<%--<link rel="stylesheet" href="/css/jquery.ui.autocomplete.css?v=2" />--%>
<%--<script type="text/javascript" src="/js/jquery.ui.autocomplete.js"></script>--%>
<%--<style type="text/css">
    .ui-autocomplete
    {
        max-height: 650px;
        overflow-y: hidden; /* prevent horizontal scrollbar */
        overflow-x: hidden; /* add padding to account for vertical scrollbar */
        padding: 0px;
    }
    /* IE 6 doesn't support max-height
 * we use height instead, but this forces the menu to always be this tall
 */
    * html .ui-autocomplete
    {
        height: 90px;
    }
</style>--%>
<!--  AutoComplete  -->
<%--<script type="text/javascript">
    $(function () {
        $('#' + "<%=txtSearchInput.ClientID %>").autocomplete({
            source: "/Common/Components/Search.ashx",
            minLength: 1,
            maxRows: 50,
            select: function (event, ui) {

                location.href = '/' + ui.item.id;

                // alert(ui.item.adres + " / " + ui.item.veriAd);
            }

        });
    });
</script>--%>
<%--<script type="text/javascript">
    $(function () {
        var obj = $('#' + "<%=txtSearchInput.ClientID %>").autocomplete({
            source: "/Common/Components/Search.ashx",

            open: function () { $('#search-container').width(300) },
            minLength: 1,
            maxRows: 20,
            select: function (event, ui) {
                location.href = ui.item.urunveri_link;
            }
        }).data("autocomplete");


        obj && (obj._renderItem = function (ul, item) {
            $(ul[0]).css("position", "fixed");
            if (item.urunveri_resim != null && item.urunveri_resim.length > 0) {
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a href=" + item.urunveri_link + "><div style='height:35px'><div style='float:left'><img width='35px' height='35px' src='https://www.gold.com.tr/UrunResim/KucukResim/" + item.urunveri_resim + "' /></div><div style='float:left;text-align:left;margin-right:25px;padding-top:10px;padding-left:5px;height:21px;font-size:12px;'>" + item.urunveri_ad + "</div></div></a>")
                .appendTo(ul);
            }
            else {
                return $("<li style='background-color:#dedede'></li>")
                .data("item.autocomplete", item)
                .append("<a href=" + item.urunveri_link + "><div style='height:20px'><div style='float:left;text-align:left;padding-top:2px;padding-left:40px;height:21px;'>" + item.urunveri_ad + "</div></div></a>")
                .appendTo(ul);
            }
        });
    });
</script>--%>
<link href="/css/jquery.ui.autocomplete.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .ui-autocomplete
    {
        max-height: 650px;
        overflow-y: hidden;
        overflow-x: hidden;
        padding: 0px;
        z-index: 9999 !important;
        width: 571px !important;
    }
    * html .ui-autocomplete
    {
        height: 90px;
    }
    .ui-menu .ui-menu-item a
    {
        border-left: 1px solid #ccc !important;
        border-right: 1px solid #ccc !important;
        border-bottom: 1px solid #ccc !important;
    }
</style>
<%--<script src="/js/jquery-ui-autocomplete.min.js" type="text/javascript"></script>--%>
<script type="text/javascript">
    $(document).ready(function () {
        $('#<%= txtSearchInput.ClientID %>').autocomplete({
            minLength: 3,
            maxRows: 8,
            source: "/Common/SearchProduct.ashx",
            select: function (event, ui) {
                location.href = ui.item.link;
            }
        })
            .data("autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a><div style=\"height:58px;\"><div style=\"position:absolute;\"><img width=\"50\" height=\"50\" src=\"" + item.image + "\"></img></div><div style=\"position:absolute;margin-left:60px;\"><span>" + item.value + "</span></div></div></a>")
                .appendTo(ul);
            };
    });              
</script>
<!-- header top -->
<div class="htop" onmouseover="MenuItemDoldur();">
    <div class="hcontainer" id="DivUyeGirisiKapali" runat="server">
        <div class="htleft">
            <ul class="usermenu">
                <li><a href="/Hesabim/Siparislerim.aspx">Hesabım</a></li>
                <%--<li><a href="#">Siparişlerim</a></li>
                <li><a href="#">Müşteri Temsilcisi</a>
                    <ul class="userdropdown">
                        <li><a href="#">Bekleyen Siparişlerim</a></li>
                        <li><a href="#">Satın Aldıklarım</a></li>
                        <li><a href="#">Sipariş Listem</a></li>
                    </ul>
                </li>--%>
                <li><a href="/MusteriHizmetleri/Iletisim.aspx" title="İletişim" rel="nofollow">Müşteri
                    Hizmetleri</a></li>
                <li><a href="/Sepet.aspx" title="MüşteriHizmetleri">Sepetim</a></li>
            </ul>
        </div>
        <div class="htright">
            <a href="https://motulteam.com/BayiContents/BayiOncekiSatislar.aspx" style="display: inline-block;">
                <span class="fbIMG">
                    <%--<a href="https://motulteam.com/Profile/MainProfile.aspx" style="display: inline-block;"><span class="fbIMG">--%>
                    <asp:Image ID="imgFacebook" runat="server" />
                </span>Hoş geldiniz Sayın <strong>
                    <asp:Label ID="lblKullaniciAdSoyad" runat="server" /></strong> &nbsp;&nbsp;
                <img src="/imagesGld/btn_mail.png" width="16" height="16" alt="E-posta" style="margin-bottom: -4px;" /></a>&nbsp;&nbsp<asp:LinkButton
                    ID="lnbCikis" runat="server" OnClick="lnbCikis_Click">Çıkış</asp:LinkButton>
        </div>
    </div>
    <div class="hcontainer" id="DivUyeGirisiAcik" runat="server">
        <div class="htleft">
            <ul class="usermenu">
                <li><a href="/Hesabim/DestekTalebi.aspx" rel="nofollow"></a></li>
            </ul>
        </div>
        <div class="htright">
            <ul class="usermenuright">
                <%-- <li><a href="https://www.facebook.com/UstaKulubu" title="Facebook" rel="nofollow"
                    target="_blank">
                    <img src="/imagesGld/footer/facebook.png" width="11" height="11" alt="Facebook" />
                    Facebook</a></li>--%>
                <%--<li class="uyeGirisi"><a href="/MusteriHizmetleri/UyeGirisi.aspx" title="Üye Girişi"--%>
                <%-- <li class="uyeGirisi"><a href="/Hesabim/Uye.aspx" title="Üye Girişi"
                    rel="nofollow">Üye Girişi</a>  --%>
              <%--  <li class="uyeGirisi" style="padding-right: 27px;"><a href="http://www.biges.com/login?BIGESCLUB=1"
                    title="Üye Girişi" rel="nofollow">Üye Girişi</a>--%>
                    <%--<asp:Button ID="btnFacebookBaglan" runat="server" Text=""  OnClick="btnFacebookBaglan_Click"
                        CssClass="htFacebookLogin" />--%>
                    <%--<a href="/MusteriHizmetleri/UyeGirisi.aspx"> <img src="../ImagesGld/facebookIco.png" alt="fb" /> Facebook ile Bağlan</a>--%>
                </li>
                <li><a href="/MusteriHizmetleri/KampanyaKurallari.aspx" title="İletişim" rel="nofollow">
                    Kampanya Kuralları</a></li>
                <%--<li><a href="/MusteriHizmetleri/Iletisim.aspx" title="İletişim" rel="nofollow">İletişim</a></li>--%>
                <%--<asp:Button ID="Button1" runat="server" Text="Button" onclick="Button1_Click" />--%>
            </ul>
        </div>
    </div>
</div>
<!-- header top -->
<!-- header middle -->
<div class="hmiddle">
    <div class="hcontainer">
        <div class="hmleft">
            <a title="Lafarge Alçı Kart" href="/">
                <img src="/imagesgld/ustlogo.png" width="228" height="78" alt="Lafarge Alçı Kart" /></a></div>
        <div class="hmcenter">
            <!-- banenr alanı -->
            <asp:HyperLink ID="lnkHeader" runat="server">
                <asp:Image ID="imgHeader" runat="server" /></asp:HyperLink>
            <div style="clear: both;">
            </div>
            <div style="font-size: 16px; float: left; margin-left: 21px; margin-top: 15px;">
                <div style="width: 225px; float: left; font-weight: bold; color: #00965E;">
                    <asp:Label ID="lblToplamPuan" runat="server" />
                </div>
                <%--<div style="width:75px; float:left; font-weight:bold">   </div> --%>
            </div>
            <div class="search">
                <div class="searchbutton">
                    <asp:ImageButton ID="btnAra" runat="server" Style="border-width: 0px;" ImageUrl="/imagesGld/btn_search.png"
                        CausesValidation="false" OnClick="btnAra_Click" />
                    <asp:HiddenField ID="hdfArama" runat="server" />
                </div>
                <div class="searchinput">
                    <asp:TextBox ID="txtSearchInput" runat="server" CssClass="txbSearch"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="hmright">
            <div style="margin-top: 8px;">
                <img width="150px" height="78px" src="/imagesgld/mus-hiz-min2.gif" /></div>
        </div>
    </div>
</div>
<!-- header middle -->
<!-- header bottom -->
<%--<div class="hbottom">
    <div class="hcontainer">
        <div id="navigation">
            <ul id="nav-one" class="dropmenu">
                <li onmouseover="MenuItemDoldur()"><a href='<%= item_1_Url %>' title='<%= item_1_name %>'
                    class="drop">
                    <%= item_1_name %></a>
                    <!-- Begin PC Item -->
                    <div id="divitem1" class="pc">
                    </div>
                    <!-- End PC Item -->
                </li>
                <li onmouseover="MenuItemDoldur()"><a href='<%= item_2_Url %>' title='<%= item_2_name %>'
                    class="drop">
                    <%= item_2_name %></a>
                    <!-- Begin Telephone Item -->
                    <div id="divitem2" class="telephone">
                    </div>
                    <!-- End Telephone Item -->
                </li>
                <li onmouseover="MenuItemDoldur()"><a href='<%= item_3_Url %>' title='<%= item_3_name %>'
                    class="drop">
                    <%= item_3_name %></a>
                    <!-- Begin Electronic Item -->
                    <div id="divitem3" class="electronic">
                    </div>
                    <!-- End Electronic Item -->
                </li>
                <li onmouseover="MenuItemDoldur()"><a href='<%= item_4_Url %>' title='<%= item_4_name %>'
                    class="drop">
                    <%= item_4_name %></a>
                    <!-- Begin FotoCamera Item -->
                    <div id="divitem4" class="fotocamera">
                    </div>
                    <!-- End FotoCamera Item -->
                </li>
                <li onmouseover="MenuItemDoldur()"><a href='<%= item_5_Url %>' title='<%= item_5_name %>'
                    class="drop">
                    <%= item_5_name %></a>
                    <!-- Begin HomeAppliances Item -->
                    <div id="divitem5" class="beyazEsya">
                    </div>
                    <!-- End HomeAppliances Item -->
                </li>
                <li onmouseover="MenuItemDoldur()"><a href='<%= item_6_Url %>' title='<%= item_6_name %>'
                    class="drop">
                    <%= item_6_name %></a>
                    <!-- Begin OtherProducts Item -->
                    <div id="divitem6" class="homeappliances">
                    </div>
                    <!-- End OtherProducts Item -->
                </li>
                <li onmouseover="MenuItemDoldur()"><a href='<%= item_7_Url %>' title='<%= item_7_name %>'
                    class="drop">
                    <%= item_7_name %></a>
                    <!-- Begin BenimMenum Item -->
                    <div id="divitem7" class="otherproducts">
                    </div>
                    <!-- End BenimMenum Item -->
                </li>
                <li onmouseover="MenuItemDoldur()"><a href='<%= item_8_Url %>' title='<%= item_8_name %>'
                    class="drop">
                    <%= item_8_name %></a>
                    <!-- Begin BenimMenum Item -->
                    <div id="divitem8" class="mymenu" style="margin-left: -76px;">
                    </div>
                    <div id="divitem8" class="specialproducts">
                    </div>
                    <!-- End BenimMenum Item -->
                    <div class="clear">
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>--%>
<%--<script type="text/javascript">
    function MenuItemDoldur() {

        if ($('#divitem1').html().trim() == '' || $('#divitem2').html().trim() == '' || $('#divitem3').html().trim() == '' || $('#divitem4').html().trim() == '' || $('#divitem5').html().trim() == '' || $('#divitem6').html().trim() == '') {
            {
                $.ajax({
                    type: 'POST',
                    url: '/usercontrols/topmenuservice.asmx/GetMenuItemHtml',
                    data: '{ }',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        $.each(result.d, function (index, data) {
                            if (index == 0)
                                $('#divitem1').html(data);
                            if (index == 1)
                                $('#divitem2').html(data);
                            if (index == 2)
                                $('#divitem3').html(data);
                            if (index == 3)
                                $('#divitem4').html(data);
                            if (index == 4)
                                $('#divitem5').html(data);
                            if (index == 5)
                                $('#divitem6').html(data);
                            if (index == 6)
                                $('#divitem7').html(data);
                            if (index == 7)
                                $('#divitem8').html(data);



                        });

                        $('.ddproducts li a').click(function () {
                            $(this).parents().filter('div[id^="divitem"]').hide();
                        });

                    },
                    error: function (err) {
                        ;
                    }
                });
            }
        }


    }

    
</script>--%>
<!-- header bottom -->
<%--<script type="text/javascript">
    $(document).ready(function () { $('#<%= txtSearchInput.ClientID %>').autocomplete("/aramaoneri.ashx"); });

    //      document.getElementById("ctl00$ctl12$txtSearchedWord").onkeydown = entercont;
</script>
<script type="text/javascript">
    function KarakterSayiKontrol() {

        if ($('#<%= txtSearchInput.ClientID %>').val().length < 3 && window.location.pathname != '/Sepetim.aspx') {
            alert("Arama kriteri 3 karakterden az olmamalıdır!");
            return false;
        }
        else return true;
    }
    //    $('#<%= txtSearchInput.ClientID %>').onkeydown = entercont;
</script>--%>