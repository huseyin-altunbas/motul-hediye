﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditorunSecimi.ascx.cs"
    Inherits="UserControls_EditorunSecimi" %>
<div class="clear">
</div>
<asp:Repeater ID="rptEditorunSecimi" runat="server">
    <HeaderTemplate>
        <div class="example-wrapper">
            <p class="title">
                Editörün Seçimi <span>(<a href="/EditorunSecimi.aspx" title="Tümünü Görün">Tümünü Görün</a>)</span></p>
            <div id="services-example-1" class="theme1">
                <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li><a href="<%#DataBinder.Eval(Container.DataItem,"urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem,"urun_ad") %>">
            <img class="thumb" src="<%#DataBinder.Eval(Container.DataItem,"resim_ad") %>"
                alt="<%#DataBinder.Eval(Container.DataItem,"urun_ad") %>" data-bw="/Tools/ShowBiz/services-plugin/assets/button/transparent.gif" /></a>
            <p class="productName">
                <a href="<%#DataBinder.Eval(Container.DataItem,"urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem,"urun_ad") %>">
                    <%# DataBinder.Eval(Container.DataItem, "urun_ad").ToString().Length > 70 ? DataBinder.Eval(Container.DataItem, "urun_ad").ToString().Substring(0, 65) + "..." : DataBinder.Eval(Container.DataItem, "urun_ad").ToString()%>
                </a>
            </p> 
            <p class="productPrice">
                <%# Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "brut_tutar")) > Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")) ?
                                                                                                                                                                                                Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")), 2).ToString("###,###,###") :
                                                                                                                                                                                                Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")), 2).ToString("###,###,###")%> PUAN</p>
            <center>
                <a class="buttonlight morebutton" href="<%#DataBinder.Eval(Container.DataItem,"urun_link") %>">
                    Ürünü İncele</a></center>
            <%--     <div class="page-more">
                <div class="imgDiv">
                    <img class="big-image" src="http://www.gold.com.tr/UrunResim/BuyukResim/<%#DataBinder.Eval(Container.DataItem,"resim_ad") %>">
                </div>
                <div class="details_double">
                    <h2 class="detailTitle">
                        Just An Image & Text</h2>
                    Ürün açıklaması <a class="buttonlight" href="<%#DataBinder.Eval(Container.DataItem,"urun_link") %>">
                        Ürünü İncele</a>
                </div>
                <div class="closer">
                </div>
            </div>--%>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
        <div class="toolbar">
            <div class="left">
            </div>
            <div class="right">
            </div>
        </div>
        </div> </div>
    </FooterTemplate>
</asp:Repeater>
<script type="text/javascript">

    //    var tpj = jQuery;
    //    tpj.noConflict();

    $(document).ready(function () {

        if ($.fn.cssOriginal != undefined)
            $.fn.css = $.fn.cssOriginal;

        $('#services-example-1').services(
						{
						    width: 966,
						    height: 230,
						    slideAmount: 5,
						    slideSpacing: 30,
						    touchenabled: "on",
						    mouseWheel: "on",
						    hoverAlpha: "off", 		// Turns Alpha Fade on/off by Hovering
						    slideshow: 3000, 			// 0 = No SlideShow, 1000 = 1 sec Slideshow (rotating automatically)
						    hovereffect: "on", 		// All Hovereffect on/off
						    callBack: function () { } 	//Callback any Function after loaded

						});

    });
</script>
