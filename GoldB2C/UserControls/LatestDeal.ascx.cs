﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Goldb2cBLL;

public partial class UserControls_LatestDeal : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        HitUrunIslemleriBLL bllHitUrunIslemleri = new HitUrunIslemleriBLL();
        dt = bllHitUrunIslemleri.HitUrunKartListesi("13", 0, 0);
        var teminEdilenUrunler = dt.AsEnumerable().Where(u => u.Field<bool>("temin_durum") == true && u.Field<decimal>("net_tutar") > 0);
        dt = teminEdilenUrunler.Count() > 0 ? teminEdilenUrunler.CopyToDataTable() : dt.Clone();

        foreach (DataRow dr in dt.Rows)
        {
            if (dr["resim_ad"].ToString().IndexOf("http") == -1)
            {
                dr["resim_ad"] = "http://www.gold.com.tr/UrunResim/MegaResim/" + dr["resim_ad"].ToString();
            }
        }

        if (dt.Rows.Count > 15)
            rptLatestDeal.DataSource = dt.AsEnumerable().Take(15).CopyToDataTable();
        else
            rptLatestDeal.DataSource = dt;

        rptLatestDeal.DataBind();

    }
}