﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Specialized;
using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using System.Web.Services;
using System.Text;

public partial class UserControls_SolMenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MenuOlustur();
    }

    public void MenuOlustur()
    {
        string subKategori = "";
        StringBuilder sbSubKategori = new StringBuilder();

        DataTable dtKategoriItem = new DataTable();
        KategoriIslemleriBLL bllKategoriIslemleri = new KategoriIslemleriBLL();
        dtKategoriItem = bllKategoriIslemleri.GetirKategoriItems().AsEnumerable()
            .Where(x => x.Field<string>("ItemHtml") != string.Empty)
            .OrderBy(x => x.Field<int>("ItemOrder")).CopyToDataTable();

        string ItemHtml = "";
        string ItemName = "";
        string ItemUrl = "";
        string AllSubCategoryHtml = "";
        foreach (DataRow dr in dtKategoriItem.Rows)
        {
            ItemHtml = dr["ItemHtml"].ToString();
            ItemName = dr["ItemName"].ToString();
            ItemUrl = dr["ItemUrl"].ToString();
            AllSubCategoryHtml = dr["AllSubCategoryHtml"].ToString();

            subKategori = " <li><a href=\"" + ItemUrl + "\" title=\"" + ItemName + "\""
                         + "class=\"has-sub\">"
                         + ItemName + "</a>"
                         + "<ul>"
                         + "<li class=\"has-sub\">"
                         + "<div class=\"otherproducts\">"
                         + ItemHtml
                         + "</div>"
                         + "</li>"
                         + "</ul>"
                         + "</li>";

            sbSubKategori.Append(subKategori);

        }

        string kategori = "<div id=\"navigation\">"
            + "<div class=\"solmenu\">"
            + "<div id=\"cssmenu\">"
            + "<ul id=\"nav-one\" class=\"dropmenu\">"
            + "<li style=\"background: #a8a8a8!important;font-weight:bold;"
            + "-webkit-box-shadow: inset 0px 13px 19px -6px rgba(255,255,255,0.54);"
            + "-moz-box-shadow: inset 0px 13px 19px -6px rgba(255,255,255,0.54);"
            + "box-shadow: inset 0px 13px 19px -6px rgba(255,255,255,0.54);\""
            + "class=\"has-sub\"><a href=\"#\" style=\"color: #FFF;\"><span >Tüm Kategoriler</span></a>"
            + "<ul>"
            + sbSubKategori.ToString()
            + "</ul>"
            + "</li>"
            + "</ul>"
            + "</div>"
            + "</div>"
            + " </div>";

        ltrKategori.Text = kategori;

    }
}