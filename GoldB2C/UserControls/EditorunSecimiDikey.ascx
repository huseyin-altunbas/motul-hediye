﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditorunSecimiDikey.ascx.cs"
    Inherits="UserControls_EditorunSecimi" %>
<link href="/Tools/cycle2/cycle2.css" rel="stylesheet" type="text/css" />
<script src="/Tools/cycle2/jquery.cycle2.js" type="text/javascript"></script>
<script src="/Tools/cycle2/jquery.cycle2.carousel.js" type="text/javascript"></script>
<script type="text/javascript">
    $.fn.cycle.defaults.autoSelector = '.slideshow';
</script>
<style type="text/css">
    .toolbar
    {
        position: relative !important;
        left: 14px !important;
        margin-bottom: 20px !important;
    }
</style>
<asp:Repeater ID="rptEditorunSecimi" runat="server">
    <HeaderTemplate>
        <span style="font-weight: bold; margin-left: 10px;">Editörün Seçimi</span><a style="color: #a8a8a8;"
            href="/EditorunSecimi.aspx" title="Editörün Seçimi(Tümünü Görün)"> (Tümü)</a>
        <div class="center" style="height: 30px;">
            <div class="toolbar" style="visibility: visible;">
                <div id="prev3" class="left">
                </div>
                <div id="next3" class="right">
                </div>
            </div>
        </div>
        <div class="cycle-pager" id="pager3">
        </div>
        <div class="slideshow vertical" data-cycle-fx="carousel" data-cycle-timeout="2000"
            data-cycle-next="#next3" data-cycle-prev="#prev3" data-cycle-pager="#pager3"
            data-cycle-carousel-visible="5" data-cycle-carousel-vertical="true">
    </HeaderTemplate>
    <ItemTemplate>
        <div class="imgContent" style="width: 270px; height: 165px; border-bottom: 1px solid #ccc;
            padding: 8px;">
            <a href="<%#DataBinder.Eval(Container.DataItem,"urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem,"urun_ad") %>">
                <img class="thumb" src="<%#DataBinder.Eval(Container.DataItem,"resim_ad") %>" alt="<%#DataBinder.Eval(Container.DataItem,"urun_ad") %>" /></a>
            <p class="productName" style="width: 100px;">
                <a href="<%#DataBinder.Eval(Container.DataItem,"urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem,"urun_ad") %>">
                    <%# DataBinder.Eval(Container.DataItem, "urun_ad").ToString().Length > 35 ? DataBinder.Eval(Container.DataItem, "urun_ad").ToString().Substring(0, 30) + "..." : DataBinder.Eval(Container.DataItem, "urun_ad").ToString()%>
                </a>
            </p>
            <p class="productPrice" style="color: #a8a8a8;">
                <%# Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "brut_tutar")) > Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")) ?
                                                                                                                                                                                                Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")), 2).ToString("###,###,###") :
                                                                                                                                                                                                Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")), 2).ToString("###,###,###")%>
                PUAN</p>
        </div>
    </ItemTemplate>
    <FooterTemplate>
        </div>
    </FooterTemplate>
</asp:Repeater>
