﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Anket.ascx.cs" Inherits="UserControls_Anket" %>
<link href="/css/goldHeader.css?v=1.2.6" rel="stylesheet" type="text/css" />
<link href="/css/goldForms.css?v=2" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/js/flashobject.js"></script>
<script src="/js/jquery.blockUI.js" type="text/javascript"></script>
<script src="/js/Gold/goldGenel.js?v=1.1" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/css/navigation.css?v=4.3.3" />
<script type="text/javascript" src="/js/navigation.js"></script>
<link rel="stylesheet" type="text/css" href="/css/userdropdown.css?v=1.2" />
<script src="/js/jquery.blockUI.js" type="text/javascript"></script>
<link href="../css/goldAccount.css?v=3" rel="stylesheet" type="text/css" />
<style type="text/css">
    .girisButton
    {
        padding: 5px;
        width: 150px;
        background-color: #00965E;
        border-radius: 5px;
        border: 0px none;
        color: White;
    }
</style>
<script type="text/javascript">
    function unblockDivKapat() {
        $(document).ready(function () {
            $.unblockUI();
        });
    }

    function UyariDivGoster(mesaj) {
        $(document).ready(function () {
            $.blockUI({
                fadeIn: 600,
                fadeOut: 600,
                css: {
                    cursor: 'default',
                    left: ($(window).width() - 400) / 2 + 'px',
                    width: '400px'
                },
                message: mesaj + '</br>'
            });
        });
    }

    $(document).ready(function () {
        $("input[type='radio']").click(function () {
            var anketId = $(this).attr("name");
            var secenekId = $(this).val();
            var isText = $(this).attr("istext");
            if (isText == "True") {
                $(".tr_" + anketId + "").remove();
                $(this).closest(".odchooes").after("<tr class='odchooes tr_" + anketId + "'><td class='chooes6'><textarea rows='2' cols='50' name='txt_" + anketId + "_" + secenekId + "' id='txtAciklama' /><br/><span style = 'color:Red;'>En fazla 100 karakter girebilirsiniz.</span></td></tr>");
            }
            else {
                $(".tr_" + anketId + "").remove();
            }
        });

        $('#txtAciklama').live('keyup blur', function () {
            var maxlength = "100";
            var val = $(this).val();

            if (val.length > maxlength) {
                $(this).val(val.slice(0, maxlength));
            }
        });
    });


</script>
<div style="background-color: White; padding: 10px 10px; border-radius: 5px;">
    <div style="clear: both;">
    </div>
    <div style="font-weight: bold; font-size: 20px;">
        <asp:Label ID="lblBaslik" runat="server" Text="Label">motulteam.com Anket Soruları</asp:Label>
    </div>
    <div>
        <table class="accountRoot" style="padding: 1px;">
            <tr class="accordionContainer">
                <td>
                    <asp:Repeater ID="rptAnket" runat="server" OnItemDataBound="rptAnket_ItemDataBound">
                        <HeaderTemplate>
                            <table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="orderchooes" style="width: 715px;">
                                <td class="chooes6" style="font-weight: bold; padding: 3px 3px;">
                                    <%#DataBinder.Eval(Container.DataItem, "AnketSorusu")%>
                                </td>
                                <td class="accordionDetail" style="width: 700px;">
                                    <asp:Repeater ID="rptAnketSecenekleri" runat="server">
                                        <HeaderTemplate>
                                            <table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class="odchooes">
                                                <td class="chooes6" style="padding: 3px 3px;">
                                                    <input name='<%#Eval("AnketId")%>' type="radio" value='<%#Eval("SecenekId")%>' istext='<%#Eval("IsText")%>' /><span><%#Eval("SecenekMetni")%></span>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </div>
    <div style="clear: both;">
    </div>
    <div>
        <asp:Button ID="btnKaydet" runat="server" Text="Kaydet" class="girisButton" OnClick="btnKaydet_Click" />
    </div>
</div>
