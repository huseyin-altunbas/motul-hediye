﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;

public partial class UserControls_CokSatanlar : System.Web.UI.UserControl
{
    EnCokSatanlarBLL bllEnCokSatanlar = new EnCokSatanlarBLL();
    DataTable dtEnCokSatanlar;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dtEnCokSatanlar = bllEnCokSatanlar.EnCokSatanlarListesi();
            var teminEdilenUrunler = dtEnCokSatanlar.AsEnumerable().Where(u => u.Field<bool>("temin_durum") == true && u.Field<decimal>("net_tutar") > 0);
            dtEnCokSatanlar = teminEdilenUrunler.Count() > 0 ? teminEdilenUrunler.CopyToDataTable() : dtEnCokSatanlar.Clone();
            if (dtEnCokSatanlar.Rows.Count > 0)
            {
                foreach (DataRow drEnCokSatanlar in dtEnCokSatanlar.Rows)
                {
                    if (drEnCokSatanlar["resim_ad"].ToString().IndexOf("http") == -1)
                    {
                        drEnCokSatanlar["resim_ad"] = "http://www.gold.com.tr/UrunResim/MegaResim/" + drEnCokSatanlar["resim_ad"].ToString();
                    }
                }

                rptCokSatanlar.DataSource = dtEnCokSatanlar.AsEnumerable().OrderByDescending(k => k.Field<decimal>("dmiktar")).Take(9).CopyToDataTable();
                rptCokSatanlar.DataBind();
            }
        }
    }
}
