﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;

public partial class UserControls_TabPanel : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        HitUrunIslemleriBLL bllHitUrunIslemleri = new HitUrunIslemleriBLL();
        dt = bllHitUrunIslemleri.HitUrunKartListesi("14", 0, 0);
        var teminEdilenUrunler = dt.AsEnumerable().Where(u => u.Field<bool>("temin_durum") == true && u.Field<decimal>("net_tutar") > 0);
        dt = teminEdilenUrunler.Count() > 0 ? teminEdilenUrunler.CopyToDataTable() : dt.Clone();

        int rowsay = 0;
        foreach (DataRow row in dt.Rows)
        {
            if (dt.Rows[rowsay]["resim_ad"].ToString().IndexOf("http") == -1)
            {
                dt.Rows[rowsay]["resim_ad"] = "http://www.gold.com.tr/UrunResim/MegaResim/" + dt.Rows[rowsay]["resim_ad"];
            }
            rowsay = rowsay + 1;
        }

        if (dt.Rows.Count > 15)
            rptTabPanel.DataSource = dt.AsEnumerable().Take(15).CopyToDataTable();
        else
            rptTabPanel.DataSource = dt;

        rptTabPanel.DataBind();
    }
}