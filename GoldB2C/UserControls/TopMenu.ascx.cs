﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Specialized;
using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using System.Web.Services;



public partial class UserControls_TopMenu : System.Web.UI.UserControl
{

    //public delegate ErpData.StrongTypesNS.LeftMenuDSDataSet.LeftMenuTTDataTable Temsilci();
    //public ErpData.StrongTypesNS.LeftMenuDSDataSet.LeftMenuTTDataTable tumKat;
    //public KategorilerBLL kategoriler = new KategorilerBLL();
    //public UyelikBLL uyelik = new UyelikBLL();
    //public SepetBLL sepet = new SepetBLL();
    //public HesabimBLL hesabim = new HesabimBLL();

    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();

    #region MenuItem
    public string item_1 = "";
    public string item_2 = "";
    public string item_3 = "";
    public string item_4 = "";
    public string item_5 = "";
    public string item_6 = "";
    public string item_7 = "";
    public string item_8 = "";

    public string item_1_name = "";
    public string item_2_name = "";
    public string item_3_name = "";
    public string item_4_name = "";
    public string item_5_name = "";
    public string item_6_name = "";
    public string item_7_name = "";
    public string item_8_name = "";

    public string item_1_AllSubCategoryHtml = "";
    public string item_2_AllSubCategoryHtml = "";
    public string item_3_AllSubCategoryHtml = "";
    public string item_4_AllSubCategoryHtml = "";
    public string item_5_AllSubCategoryHtml = "";
    public string item_6_AllSubCategoryHtml = "";
    public string item_7_AllSubCategoryHtml = "";
    public string item_8_AllSubCategoryHtml = "";

    public string item_1_Url = "";
    public string item_2_Url = "";
    public string item_3_Url = "";
    public string item_4_Url = "";
    public string item_5_Url = "";
    public string item_6_Url = "";
    public string item_7_Url = "";
    public string item_8_Url = "";

    #endregion

    #region Page Init
    protected void Page_Init(object sender, EventArgs e)
    {
        txtSearchInput.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnAra.ClientID + "').click();return false;}} else {return true;} ");

        string PageUrl = Request.Url.PathAndQuery.ToString().ToLower();
        string HttpHost = Request.Url.GetLeftPart(UriPartial.Authority).ToLower();

        //-- HTTPS ise ve aşağıdakilerden biriyse DEĞİLSE "HTTP YAP"
        /*
         if (SSL_Guvenli()
               && (PageUrl.IndexOf("sepet.aspx") < 0)
               && (PageUrl.IndexOf("adresbilgileri.aspx") < 0)
               && (PageUrl.IndexOf("kargo.aspx") < 0)
               && (PageUrl.IndexOf("odeme.aspx") < 0)
               && (PageUrl.IndexOf("odemeonay.aspx") < 0)
               && (PageUrl.IndexOf("uyegirisi.aspx") < 0)
               && (PageUrl.IndexOf("uyekayit.aspx") < 0)
               && (PageUrl.IndexOf("3dKarsila.aspx") < 0)
            )
        {
            PageUrl = HttpHost.Replace("https", "http").ToString() + Request.Url.PathAndQuery.ToString();
            Response.Redirect(PageUrl);
        }

        //-- HTTPS ve LOCALHOST değilse ve aşağıdakilerden BİRİYSE "HTTPS YAP"
        else if ((SSL_Guvenli() == false) &&
               (HttpHost.IndexOf("localhost") <= 0) &&
               (
                 (PageUrl.IndexOf("sepet.aspx") > -1)
              || (PageUrl.IndexOf("adresbilgileri.aspx") > -1)
              || (PageUrl.IndexOf("kargo.aspx") > -1)
              || (PageUrl.IndexOf("odeme.aspx") > -1)
              || (PageUrl.IndexOf("odemeonay.aspx") > -1)
              || (PageUrl.IndexOf("uyegirisi.aspx") > -1)
              || (PageUrl.IndexOf("uyekayit.aspx") > -1)
              || (PageUrl.IndexOf("3dKarsila.aspx") > -1)
            )
          )
        {
            PageUrl = HttpHost.Replace("http", "https").ToString() + Request.Url.PathAndQuery.ToString();
            Response.Redirect(PageUrl);
        }  */

    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        Helper.PuanSorgula();
        //Header a link vermek için 
        imgHeader.Visible = false;
        lnkHeader.Visible = false;
        //if (DateTime.Now.Day == 24)
        {
            imgHeader.Visible = false;
            lnkHeader.Visible = true;
            //imgHeader.ImageUrl = "~/imagesgld/Header/kargoBedava.png";
            lnkHeader.NavigateUrl = "/OK9706066/Kargo-Bedava-Kapmanyasi";
        }


        if (Request.QueryString["ref"] != null)
            Response.Redirect("Default.aspx", true);

        //UYELİK İŞLEMLERİ
        //Eğer Session Varsa uyeliği aç
        if (Session["Kullanici"] != null)
        {
            KullaniciBarAcKapa(true);

            //    //UyeGirisiCell.Visible = false;
            //    //HesabimCell.Visible = true;
            Kullanici kullanici = (Kullanici)Session["Kullanici"];

            if (Session["CRMCustomerGUID"] != null && Session["CRMCustomerGUID"].ToString() != "")
            {
                int id = Int32.Parse(Session["CRMCustomerGUID"].ToString());
                //GoldService uyeler = new GoldService();
                //TGOLDUserInfo xuser = uyeler.UserLogin(id);

               // Session["totalGoldPoint"] = (int)xuser.Puan;
                //Session["CRMCustomerGUID"] = xuser.ID;
            }
            else
            {
                Session["totalGoldPoint"] = 0;
                Session["CRMCustomerGUID"] = null;
            }

            if (kullanici.Email == "murat.balci@bigesclub.com" || kullanici.Email == "mustafa.ayar@bigesclub.com")
            {
                lblKullaniciAdSoyad.Text = Server.MachineName + " " + kullanici.KullaniciAd + " " + kullanici.KullaniciSoyad;
                lblToplamPuan.Text = "Kullanılabilir Puanınız : " + (Convert.ToInt32(Session["totalGoldPoint"])).ToString("###,###,###");
                //lblAvansPuan.Text = "Kullanabileceğiniz Avans Puanınız : " + (Convert.ToInt32(Session["totalPointAdvance"])).ToString("###,###,###");
                if (Convert.ToInt32(Session["totalGoldPoint"]) <= 0) lblToplamPuan.Text = "Kullanılabilir Puanınız : " + "0";
                //if (Convert.ToInt32(Session["totalPointAdvance"]) <= 0) lblAvansPuan.Text = "Kullanabileceğiniz Avans Puanınız : " + "0";


            }

            else
            {
                lblKullaniciAdSoyad.Text = kullanici.KullaniciAd + " " + kullanici.KullaniciSoyad;
                lblToplamPuan.Text = "Kullanılabilir Puanınız : " + (Convert.ToInt32(Session["totalGoldPoint"])).ToString("###,###,###");
                //lblAvansPuan.Text = "Kullanabileceğiniz Avans Puanınız : " + (Convert.ToInt32(Session["totalPointAdvance"])).ToString("###,###,###");
                if (Convert.ToInt32(Session["totalGoldPoint"]) <= 0) lblToplamPuan.Text = "Kullanılabilir Puanınız : " + "0";
                //if (Convert.ToInt32(Session["totalPointAdvance"]) <= 0) lblAvansPuan.Text = "Kullanabileceğiniz Avans Puanınız : " + "0";
            }

            if (!string.IsNullOrEmpty(kullanici.FacebookId))
            {
                imgFacebook.Visible = true;
                imgFacebook.ImageUrl = "https://graph.facebook.com/" + kullanici.FacebookId + "/picture";
            }
            else
                imgFacebook.Visible = false;
        }

        // eğer session yoksa  cookie control et
        else if (Session["Kullanici"] == null && Request.Cookies["User"] != null && Request.Cookies["User"]["Email"] != null && Request.Cookies["User"]["Sifre"] != null)
        {
            Crypt crypt = new Crypt();
            string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
            string Email = crypt.DecryptData(encryptKey, Request.Cookies["User"]["Email"].ToString());
            string Sifre = crypt.DecryptData(encryptKey, Request.Cookies["User"]["Sifre"].ToString());

            int kullaniciID = bllUyelikIslemleri.MusteriKullaniciGiris(Email, Sifre);
            if (kullaniciID > 0)
            {

                //        if (sepet.GetGeciciSepetMasterID(kullaniciID) != null && sepet.GetGeciciSepetMasterID(kullaniciID) != "")
                //            Session["MasterID"] = sepet.GetGeciciSepetMasterID(kullaniciID);

                Kullanici kullanici = bllUyelikIslemleri.KullaniciSistemKayit(kullaniciID);

                //        bool IndirimKodCiksin = false;
                //        string IndirimKod = "";
                //        hesabim.GetIndirimKod(((sUser)Session["sUser"]).ERPKod, out IndirimKodCiksin, out IndirimKod);
                //        sUser.IndirimKod = IndirimKod;
                //        //  Session.Add("sUser", sUser);


                KullaniciBarAcKapa(true);
                lblKullaniciAdSoyad.Text = ((Kullanici)Session["Kullanici"]).KullaniciAd + " " + ((Kullanici)Session["Kullanici"]).KullaniciSoyad + " " + HttpContext.Current.Session["totalPointAdvance"].ToString() + "";

                imgFacebook.Visible = false;
            }
            else
            {
                KullaniciBarAcKapa(false);
            }

        }
        // session yoksa
        else
        {
            KullaniciBarAcKapa(false);
            HttpCookie cookie = new HttpCookie("HNetLogin");
            cookie.Value = "false";
            DateTime dtNow = DateTime.Now;
            cookie.Expires = dtNow;
            Response.Cookies.Add(cookie);
        }
        //UYELİK İŞLEMLERİ

        ////Mesajlaşma

        //if (!this.IsPostBack)
        //{
        //    if (Session["sUser"] != null)
        //    {
        //        MusteriMesajBLL musterimesaj = new MusteriMesajBLL();
        //        int yeniMesaj = 0;
        //        yeniMesaj = musterimesaj.MusteriYeniCevapAdet(((sUser)Session["sUser"]).ERPKod);

        //        if (yeniMesaj > 0)
        //        {
        //            //lnkYeniMesaj.Visible = true;
        //            //imgokundu.Visible = true;
        //            //imgokundu.Alt = yeniMesaj.ToString() + " yeni mesajınız var.";
        //        }
        //        else
        //        {
        //            //lnkYeniMesaj.Visible = false;
        //            //imgokundu.Visible = false;
        //        }
        //    }
        //}
        ////Mesajlaşma

        TopMenuOlustur();

        //Temsilci t1 = new Temsilci(kategoriler.GetKategoriler);
        //CachingBLL cachebll = new CachingBLL();
        //tumKat = (ErpData.StrongTypesNS.LeftMenuDSDataSet.LeftMenuTTDataTable)cachebll.GetCache(t1, false, "TopBar", Cache, null);

        //if (
        //    Request.Url.ToString().IndexOf("UyeGirisi.aspx") == -1 &&
        //    Request.Url.ToString().IndexOf("UyeGirisiOnay.aspx") == -1 &&
        //    Request.Url.ToString().ToLower().IndexOf("uyelik.aspx") == -1 &&
        //    Request.Url.ToString().ToLower().IndexOf("hizliuyelikdetay.aspx") == -1 &&
        //    Request.Url.ToString().ToLower().IndexOf("uyelikonay.aspx") == -1
        //    )
        //{
        //    if (Request.Url.ToString().IndexOf("Sepetim.aspx") == -1)
        //        Session["lastshopurl"] = Request.Url.ToString();

        //    Session["lasturl"] = Request.AppRelativeCurrentExecutionFilePath;
        //}

        //if (Request.Url.Host.ToString() != "localhost" &&
        //    Request.Url.Host.ToString() != "www.hzllocal.com" &&
        //    Request.Url.Host.ToString() != "www.hzllocalv2.com")

        //    //lblGoogle.Text = "<script type=\"text/javascript\"> var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\"); document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\")); </script> <script type=\"text/javascript\"> var pageTracker = _gat._getTracker(\"UA-11339360-1\"); pageTracker._initData(); pageTracker._trackPageview(); </script>";
        //    if (tumKat.Rows.Count > 0)
        //    {
        //        // sadece anakategorileri al
        //        ErpData.StrongTypesNS.LeftMenuDSDataSet.LeftMenuTTDataTable anaKat;
        //        anaKat = kategoriler.GetAnaKategori(tumKat);
        //        // aramayı oluştur
        //        //my_dropdown.DataSource = anaKat;
        //        //my_dropdown.DataTextField = "urun_ad";
        //        //my_dropdown.DataValueField = "urun_kod";
        //        //my_dropdown.DataBind();
        //    }

        //if (!this.IsPostBack)
        //{

        //    int userCinsiyet = 0;
        //    if (Session["sUser"] != null)
        //    {
        //        switch (((sUser)Session["sUser"]).Cinsiyet)
        //        {
        //            case "E": userCinsiyet = 1; break;
        //            case "K": userCinsiyet = 2; break;
        //        }
        //    }
        //    LandingPageBLL landingPage = new LandingPageBLL();
        //    DataTable dtLandingPage = new DataTable();
        //    dtLandingPage = landingPage.SelectBannerbyPositionIdCinsiyetId(1, userCinsiyet, "");
        //    //if (dtLandingPage.Rows.Count > 0)
        //    //{
        //    //    if (dtLandingPage.Rows[0]["banner_url"].ToString() == "")
        //    //        topBanner.InnerHtml = dtLandingPage.Rows[0]["banner_html"].ToString();
        //    //    else
        //    //        topBanner.InnerHtml = "<a href=\"" + dtLandingPage.Rows[0]["banner_url"].ToString() + "\">" + dtLandingPage.Rows[0]["banner_html"].ToString() + "</a>";
        //    //    topBanner.Visible = true;
        //    //}
        //    //else
        //    //{
        //    //    topBanner.InnerHtml = "";
        //    //    topBanner.Visible = false;
        //    //}

        //    // site map path
        //    string thisPage = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath);

        //    //if (!string.IsNullOrEmpty(CatIDFromUrunInceleme.Text))
        //    //{
        //    //    lblSiteMap.Text = kategoriler.GetKategoriSiteMapPath(tumKat, int.Parse(CatIDFromUrunInceleme.Text));
        //    //    CatIDFromUrunInceleme.Text = string.Empty;
        //    //}
        //    //else if (thisPage != "Default.aspx" && thisPage != "default.aspx" && Request.QueryString["CatID"] != null)
        //    //{
        //    //    lblSiteMap.Text = kategoriler.GetKategoriSiteMapPath(tumKat, int.Parse(Request.QueryString["CatID"]));
        //    //}
        //    //else
        //    //{
        //    //    divSiteMap.Visible = false;
        //    //}
        //}
        //// veri alma işlemi başarısız, hata sayfasına yönlendir
        //else { }

        if (Session["FacebookEsksikBilgi"] != null && Convert.ToBoolean(Session["FacebookEsksikBilgi"]))
        {
            Page.ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>FacebookDivGoster('<iframe width=\"530px\" height=\"490px\" src=\"/FacebookUyeKayit.aspx?ResirectUrl=" + Request.Url.PathAndQuery + "\"></iframe>')</script>");
        }

        if (!string.IsNullOrEmpty(Request.QueryString["kampanya"]) && Request.QueryString["kampanya"] == "atesolcer")
        {
            if (Session["Kullanici"] != null)
                Page.ClientScript.RegisterStartupScript(typeof(string), "lightbox", "<script type=\"text/javascript\">jQuery(document).ready(function ($) { $.lightbox('/imagesgld/atesolcer2.jpg');});</script>");
            else
                Page.ClientScript.RegisterStartupScript(typeof(string), "lightbox", "<script type=\"text/javascript\">jQuery(document).ready(function ($) { $.lightbox('/imagesgld/atesolcer1.jpg');});</script>");
        }

    }

    private void KullaniciBarAcKapa(bool GirisYapti)
    {
        DivUyeGirisiAcik.Visible = !GirisYapti;
        DivUyeGirisiKapali.Visible = GirisYapti;
    }

    public static string GetHeaderValue(NameValueCollection headers, string key)
    {
        for (int i = 0; i < headers.Count; i++)
        {
            if (string.Equals(headers.GetKey(i), key,
                System.StringComparison.OrdinalIgnoreCase))
            {
                return headers.Get(i);
            }
        }

        return string.Empty;
    }

    public static bool SSL_Guvenli()
    {
        bool ssl_durum = false;

        if (GetHeaderValue(HttpContext.Current.Request.Headers, "Front-End-Https") == String.Empty)
        {
            if (HttpContext.Current.Request.ServerVariables["HTTPS"].ToString() == "off")
            {
                ssl_durum = false;
            }
            else ssl_durum = true;
        }
        else ssl_durum = true;

        return ssl_durum;

    }
    public void TopMenuOlustur()
    {

        DataTable TopMenuItem = new DataTable();
        TopMenuBLL topMenuBLL = new TopMenuBLL();
        TopMenuItem = topMenuBLL.SelectTopMenu();

        item_1 = TopMenuItem.Rows[0]["ItemHtml"].ToString();
        item_2 = TopMenuItem.Rows[1]["ItemHtml"].ToString();
        item_3 = TopMenuItem.Rows[2]["ItemHtml"].ToString();
        item_4 = TopMenuItem.Rows[3]["ItemHtml"].ToString();
        item_5 = TopMenuItem.Rows[4]["ItemHtml"].ToString();
        item_6 = TopMenuItem.Rows[5]["ItemHtml"].ToString();
        item_7 = TopMenuItem.Rows[6]["ItemHtml"].ToString();
        item_8 = TopMenuItem.Rows[7]["ItemHtml"].ToString();

        item_1_name = TopMenuItem.Rows[0]["ItemName"].ToString();
        item_2_name = TopMenuItem.Rows[1]["ItemName"].ToString();
        item_3_name = TopMenuItem.Rows[2]["ItemName"].ToString();
        item_4_name = TopMenuItem.Rows[3]["ItemName"].ToString();
        item_5_name = TopMenuItem.Rows[4]["ItemName"].ToString();
        item_6_name = TopMenuItem.Rows[5]["ItemName"].ToString();
        item_7_name = TopMenuItem.Rows[6]["ItemName"].ToString();
        item_8_name = TopMenuItem.Rows[7]["ItemName"].ToString();

        item_1_Url = TopMenuItem.Rows[0]["ItemUrl"].ToString();
        item_2_Url = TopMenuItem.Rows[1]["ItemUrl"].ToString();
        item_3_Url = TopMenuItem.Rows[2]["ItemUrl"].ToString();
        item_4_Url = TopMenuItem.Rows[3]["ItemUrl"].ToString();
        item_5_Url = TopMenuItem.Rows[4]["ItemUrl"].ToString();
        item_6_Url = TopMenuItem.Rows[5]["ItemUrl"].ToString();
        item_7_Url = TopMenuItem.Rows[6]["ItemUrl"].ToString();
        item_8_Url = TopMenuItem.Rows[7]["ItemUrl"].ToString();

        item_1_AllSubCategoryHtml = TopMenuItem.Rows[0]["AllSubCategoryHtml"].ToString();
        item_2_AllSubCategoryHtml = TopMenuItem.Rows[1]["AllSubCategoryHtml"].ToString();
        item_3_AllSubCategoryHtml = TopMenuItem.Rows[2]["AllSubCategoryHtml"].ToString();
        item_4_AllSubCategoryHtml = TopMenuItem.Rows[3]["AllSubCategoryHtml"].ToString();
        item_5_AllSubCategoryHtml = TopMenuItem.Rows[4]["AllSubCategoryHtml"].ToString();
        item_6_AllSubCategoryHtml = TopMenuItem.Rows[5]["AllSubCategoryHtml"].ToString();
        item_7_AllSubCategoryHtml = TopMenuItem.Rows[6]["AllSubCategoryHtml"].ToString();
        item_8_AllSubCategoryHtml = TopMenuItem.Rows[7]["AllSubCategoryHtml"].ToString();
    }
    protected void btnAra_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("/Arama.aspx?text=" + HttpUtility.UrlEncode(txtSearchInput.Text.Trim()), false);
    }
    protected void lnbCikis_Click(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {
            bllUyelikIslemleri.MusteriKullaniciCikis(((Kullanici)Session["Kullanici"]).KullaniciId);
            Session.Abandon();
        }

        Response.Redirect("/default.aspx", false);
    }

    protected void btnFacebookBaglan_Click(object sender, EventArgs e)
    {
        ///*string FacebookAppID = CustomConfiguration.Facebook_client_id;
        //string FacebookCallbackURL = CustomConfiguration.Facebook_redirect_url;
        //string serviceURL = string.Format("https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}&scope=email,user_birthday",
        //FacebookAppID, FacebookCallbackURL);
        //if (!string.IsNullOrEmpty(Request.UrlReferrer.OriginalString))
        //    Session["FbConnectSonUrl"] = Request.UrlReferrer.OriginalString;
        //else
        //    Session["FbConnectSonUrl"] = "~/MusteriHizmetleri/Hosgeldiniz.aspx";
        //Response.Redirect(serviceURL);*/
    }
}