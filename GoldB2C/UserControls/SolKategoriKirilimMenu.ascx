﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SolKategoriKirilimMenu.ascx.cs"
    Inherits="UserControls_SolKategoriKirilimMenu" %>
<div class="filterContainer">
    <%--Ürün grubu 1--%>
    <asp:Repeater ID="rptKategoriler" runat="server" OnItemDataBound="rptKategoriler_ItemDataBound">
        <ItemTemplate>
            <div class="filterTitle">
                <a href="<%#DataBinder.Eval(Container.DataItem, "link") %>" title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                    <%#DataBinder.Eval(Container.DataItem, "urun_ad") %></a></div>
            <div class="filterHierrarchy">
                <!-- first category -->
                <asp:Repeater ID="rptKategorilerIcerik" runat="server" OnItemDataBound="rptKategorilerIcerik_ItemDataBound">
                    <HeaderTemplate>
                        <div class="firstCategory">
                            <ul class="firstHierrarchyMenu">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li><span class="bullet"><a href="<%#DataBinder.Eval(Container.DataItem, "link") %>"
                            title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                            <%#DataBinder.Eval(Container.DataItem, "urun_ad") %>
                            <span class="prnumber">(<%#DataBinder.Eval(Container.DataItem, "urun_sayisi") %>)</span></a>
                        </span></li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul> </div>
                    </FooterTemplate>
                </asp:Repeater>
                <!-- first category -->
                <!-- other category-->
                <div class="otherCategory" id="DivDigerKategoriler" runat="server">
                    <div id="imgDigerKategoriler<%#Container.ItemIndex %>" style="cursor: pointer;" onclick="ShowMore('imgDigerKategoriler<%#Container.ItemIndex %>','imgDigerContainer<%#Container.ItemIndex %>','slidingDiv<%#Container.ItemIndex %>');">
                        <div id="imgDigerContainer<%#Container.ItemIndex %>">
                            <img id="imgDiger" src="/imagesgld/icon_allcategory.png" alt="Diğer Kategotiler" />
                            Diğer Kategoriler
                        </div>
                    </div>
                    <div id="slidingDiv<%#Container.ItemIndex %>" class="slidingDiv" style="display: none;">
                        <ul class="firstHierrarchyMenu" id="showme">
                            <asp:Repeater ID="rptKategorilerIcerikDiger" runat="server">
                                <ItemTemplate>
                                    <li><span class="bullet"><a href="<%#DataBinder.Eval(Container.DataItem, "link") %>"
                                        title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                                        <%#DataBinder.Eval(Container.DataItem, "urun_ad") %>
                                        <span class="prnumber">(<%#DataBinder.Eval(Container.DataItem, "urun_sayisi") %>)</span></a>
                                    </span></li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
                <!-- other category-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
<script type="text/javascript">
    function ShowMore(sender, senderContainer, cId) {
        if ($("#" + cId).css("display") == "none") {
            $("#" + senderContainer).delay(500).fadeOut(400);
            $("#" + sender).delay(800).slideUp(250);
            $("#" + cId).slideDown(500);
        }
        else {
            $("#" + cId).slideUp('fast');
            $("#" + sender).html('Diger');
        }
    }
</script>
