﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_OdemeNavigasyon : System.Web.UI.UserControl
{
    public string SepetLink { get; set; }
    public string AdresbilgileriLink { get; set; }
    //public string KargoLink { get; set; }
    //public string OdemeLink { get; set; }
    public string OdemeonayLink { get; set; }

    public string SepetSelected { get; set; }
    public string AdresbilgileriSelected { get; set; }
    //public string KargoSelected { get; set; }
    //public string OdemeSelected { get; set; }
    public string OdemeonaySelected { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            string sayfa = Request.Url.AbsolutePath.ToLower();
            if (sayfa == "/sepet.aspx")
            {
                SepetLink = "/sepet.aspx";
                AdresbilgileriLink = "/sepet.aspx";
                //KargoLink = "/sepet.aspx";
                //OdemeLink = "/sepet.aspx";
                OdemeonayLink = "/sepet.aspx";

                SepetSelected = "selected";
                AdresbilgileriSelected = "";
                //KargoSelected = "";
                //OdemeSelected = "";
                OdemeonaySelected = "";
            }
            else if (sayfa == "/adresbilgileri.aspx")
            {
                SepetLink = "/sepet.aspx";
                AdresbilgileriLink = "/adresbilgileri.aspx";
                //KargoLink = "/kargo.aspx";
                //OdemeLink = "/odeme.aspx";
                OdemeonayLink = "/siparisonay.aspx";

                SepetSelected = "";
                AdresbilgileriSelected = "selected";
                //KargoSelected = "";
                //OdemeSelected = "";
                OdemeonaySelected = "";
            }
            else if (sayfa == "/kargo.aspx")
            {
                SepetLink = "/sepet.aspx";
                AdresbilgileriLink = "/adresbilgileri.aspx";
                //KargoLink = "/kargo.aspx";
                //OdemeLink = "/odeme.aspx";
                OdemeonayLink = "/siparisonay.aspx";

                SepetSelected = "";
                AdresbilgileriSelected = "";
                //KargoSelected = "selected";
                //OdemeSelected = "";
                OdemeonaySelected = "";
            }
            else if (sayfa == "/odeme.aspx")
            {
                SepetLink = "/sepet.aspx";
                AdresbilgileriLink = "/adresbilgileri.aspx";
                //KargoLink = "/kargo.aspx";
                //OdemeLink = "/odeme.aspx";
                OdemeonayLink = "/siparisonay.aspx";

                SepetSelected = "";
                AdresbilgileriSelected = "";
                //KargoSelected = "";
                //OdemeSelected = "selected";
                OdemeonaySelected = "";
            }
            else if (sayfa == "/siparisonay.aspx")
            {
                SepetLink = "/sepet.aspx";
                AdresbilgileriLink = "/adresbilgileri.aspx";
                //KargoLink = "/kargo.aspx";
                //OdemeLink = "/odeme.aspx";
                OdemeonayLink = "/siparisonay.aspx";

                SepetSelected = "";
                AdresbilgileriSelected = "";
                //KargoSelected = "";
                //OdemeSelected = "";
                OdemeonaySelected = "selected";
            }

        }
    }
}