﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class UserControls_OzelKategoriMenuV2 : System.Web.UI.UserControl
{
    string KategoriBaslik;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void MenuOlustur(DataTable dtOzelKategoriMenu)
    {
        if (dtOzelKategoriMenu != null && dtOzelKategoriMenu.Rows.Count > 0)
        {
            dtOzelKategoriMenu.DefaultView.Sort = "siralama_no";
            DataView dv = dtOzelKategoriMenu.DefaultView;

            dtOzelKategoriMenu = dv.ToTable();

            if (!string.IsNullOrEmpty(dtOzelKategoriMenu.Rows[0]["kategori_ad"].ToString()))
                KategoriBaslik = dtOzelKategoriMenu.Rows[0]["kategori_ad"].ToString();

            rptOzelKategoriMenu.DataSource = dtOzelKategoriMenu;
            rptOzelKategoriMenu.DataBind();


        }
    }
    protected void rptOzelKategoriMenu_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
            ((Literal)e.Item.FindControl("ltrBaslik")).Text = KategoriBaslik;

    }
}