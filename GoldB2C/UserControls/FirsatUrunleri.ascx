﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FirsatUrunleri.ascx.cs"
    Inherits="UserControls_FirsatUrunleri" %>
<div class="gununfirsati">
   <div class="gununfirsatiBaslik">
        Günün En Çok Satanları
   </div>     

   <a href="/GununFirsatlari.aspx" title="BÜTÜN ÜRÜNLERİ GÖR" class="allDeals"><span>BÜTÜN ÜRÜNLERİ GÖR</span></a>
   
    <ul id="first-carousel" class="first-and-second-carousel jcarousel-skin-tango">
        <asp:Repeater ID="rptGununFirsati" runat="server">
            <ItemTemplate>
                <!-- pr -->
                <li>
                    <a onclick="TiklamaKaydet($(this).attr('href'));" href="<%# string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem,"firsat_link").ToString()) ? DataBinder.Eval(Container.DataItem,"urun_link")  : DataBinder.Eval(Container.DataItem,"firsat_link") %>" class="crproduct">
                        <div class="prname">
                            <span><%# DataBinder.Eval(Container.DataItem,"spot_kelime") %></span>
                        </div>

                        <div class="prpic">
                                <img src="/urunresim/BuyukResim/<%# DataBinder.Eval(Container.DataItem,"resim_ad") %>" width="227" height="227" alt="Fırsat Ürünü" />
                        </div>

                        <div class="prname2">
                                <%# DataBinder.Eval(Container.DataItem,"firsat_aciklama") %>                             
                        </div>

                        <div class="prdetail">
                            <%# DataBinder.Eval(Container.DataItem,"firsat_ekbilgi") %>
                        </div>

                        <div class="prprice2">
                                <span class="discount"><%# DataBinder.Eval(Container.DataItem,"liste_fiyat") %><span class="tl">PUAN</span></span>
                        </div>

                        <div class="prprice">
                            <%# DataBinder.Eval(Container.DataItem,"indirimli_fiyat") %><span class="tl">PUAN</span>
                        </div>

                    </a>
                </li>
                <!-- pr -->
            </ItemTemplate>
        </asp:Repeater>
    </ul>    
    <div class="clear"></div>
</div>
