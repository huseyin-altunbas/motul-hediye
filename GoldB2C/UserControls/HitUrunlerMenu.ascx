﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HitUrunlerMenu.ascx.cs"
    Inherits="UserControls_HitUrunlerMenu" %>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<%--<script type="text/javascript">
    $(document).ready(function () {
        $("#accordion").accordion({ autoHeight: false });
    });
</script>--%>
<asp:Literal ID="ltrAccordionScript" runat="server"></asp:Literal>
<div id="lmframe">
    <div id="accordion">
        <asp:Repeater ID="rptHitUrunlerMenu" runat="server" OnItemDataBound="rptHitUrunlerMenu_OnItemDataBound">
            <HeaderTemplate>
            </HeaderTemplate>
            <ItemTemplate>
                <h3>
                    <a href="#">
                        <%# DataBinder.Eval(Container.DataItem, "ustgrup_ad") %>
                        (<asp:Literal ID="ltrUrunSayisi" runat="server"></asp:Literal>) </a>
                </h3>
                <div>
                    <asp:Repeater ID="rptHitUrunlerMenuDetay" runat="server">
                        <ItemTemplate>
                            <a href="<%# DataBinder.Eval(Container.DataItem, "url_rewrite") %>">
                                <%# DataBinder.Eval(Container.DataItem, "altgrup_ad") %>
                                (<%# DataBinder.Eval(Container.DataItem, "urun_sayisi") %>) </a>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </ItemTemplate>
            <FooterTemplate>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>
