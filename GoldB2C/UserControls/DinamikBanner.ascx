﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DinamikBanner.ascx.cs"
    Inherits="UserControls_DinamikBanner" %>
<!--  BannerSlider -->
<link rel="stylesheet" type="text/css" href="/css/vitrinbanner.css?v=1.1" />
<script language="javascript" type="text/javascript" src="/js/jquery.easing.js"></script>
<script language="javascript" type="text/javascript" src="/js/bannerscript.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // buttons for next and previous item						 
        var buttons = { previous: $('#jslidernews1 .button-previous'),
            next: $('#jslidernews1 .button-next')
        };
        $obj = $('#jslidernews1').lofJSidernews({ interval: 5000,
            easing: 'easeInOutQuad',
            duration: 1200,
            auto: true,
            maxItemDisplay: 10,
            startItem: 0,
            navPosition: 'horizontal', // horizontal
            navigatorHeight: null,
            navigatorWidth: null,
            mainWidth: 780,
            buttons: buttons
        });
    });
</script>
<!--  BannerSlider -->
<asp:Repeater ID="rptDinamikBanner" runat="server" OnItemDataBound="rptDinamikBanner_ItemDataBound">
    <HeaderTemplate>
        <div class="campaignbanner">
            <div class="cbtop">
                <div id="jslidernews1" class="lof-slidecontent" style="width: 780px; height: 270px;">
                    <div class="preload">
                        <div>
                        </div>
                    </div>
                    <div class="button-previous">
                    </div>
                    <div class="button-next">
                    </div>
                    <!-- MAIN CONTENT -->
                    <div class="main-slider-content" style="width: 780px; height: 270px;">
                        <ul class="sliders-wrap-inner">
    </HeaderTemplate>
    <ItemTemplate>
        <li>
            <div class="slide">
                <div class="left">
                    <div class="pic">
                        <a onclick="TiklamaKaydet('<%#DataBinder.Eval(Container.DataItem,"urun_link") %>');"
                            href="<%#DataBinder.Eval(Container.DataItem,"urun_link") %>">
                            <img src="/UrunResim/BuyukResim/<%#DataBinder.Eval(Container.DataItem,"resim_ad") %>"
                                alt="Gold" /></a>
                    </div>
                    <div class="buttons">
                        &nbsp;
                    </div>
                </div>
                <div class="right">
                    <div class="productprice">
                        <div class="price">
                            <span class="VtrinText">Fiyat:</span>
                            <div class="discount">
                                <span>
                                    <%#DataBinder.Eval(Container.DataItem,"dliste_fiyat") %></span>
                                <%#DataBinder.Eval(Container.DataItem,"doviz_kod") %>&nbsp;+KDV
                            </div>
                            <span class="VtrinText1">KDV Dahil:</span>
                            <div class="cost">
                                <span>
                                    <asp:Literal ID="ltrBrutTutar" runat="server"></asp:Literal>
                                </span>
                                <asp:Literal ID="ltrTlText" runat="server"></asp:Literal></div>
                            <asp:Label ID="lblIndirimliText" runat="server" Text="İndirimli:" CssClass="VtrinText2"></asp:Label>
                            <div class="tlira">
                                <span>
                                    <asp:Literal ID="ltrNetTutar" runat="server"></asp:Literal>
                                </span>TL </br>(KDV Dahil)</div>
                            <div class="gain" <%# Convert.ToInt32(DataBinder.Eval(Container.DataItem,"indirim_yuzde")) >= 100 ? "style=\"display:none\"" : ""  %>>
                                <div class="gright">
                                    %<%# 100 - Convert.ToInt32(DataBinder.Eval(Container.DataItem,"indirim_yuzde")) %>&nbsp;/&nbsp;<%#DataBinder.Eval(Container.DataItem,"indirim_tutar") %>&nbsp;TL</div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="VtrinTaksit">
                                <strong></strong>
                            </div>
                        </div>
                    </div>
                    <div class="productdetail">
                        <div class="title curhand" onclick="TiklamaKaydet('<%#DataBinder.Eval(Container.DataItem,"urun_link") %>'); location.href='<%#DataBinder.Eval(Container.DataItem,"urun_link") %>'">
                            <%#DataBinder.Eval(Container.DataItem,"spot_kelime") %>
                            <br />
                            <span>
                                <%#DataBinder.Eval(Container.DataItem,"urun_ad") %></span>
                        </div>
                        <div class="contents curhand" onclick="TiklamaKaydet('<%#DataBinder.Eval(Container.DataItem,"urun_link") %>'); location.href='<%#DataBinder.Eval(Container.DataItem,"urun_link") %>'">
                            <%#DataBinder.Eval(Container.DataItem,"genel_bilgi") %>
                        </div>
                        <div class="iconss">
                            <%#DataBinder.Eval(Container.DataItem, "banner_bilgiimaj1")%>
                            <%#DataBinder.Eval(Container.DataItem, "banner_bilgiimaj2")%>
                            <%#DataBinder.Eval(Container.DataItem, "banner_bilgiimaj3")%>
                            <%#DataBinder.Eval(Container.DataItem, "banner_bilgiimaj4")%>
                            <%#DataBinder.Eval(Container.DataItem, "banner_bilgiimaj5")%>
                            <%--     <%# !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "banner_bilgiimaj1").ToString()) ? "<img src=\"/UrunResim/KucukResim/" + DataBinder.Eval(Container.DataItem,"banner_bilgiimaj1") + ".jpg\" />" : ""  %>
                            <%# !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "banner_bilgiimaj2").ToString()) ? "<img src=\"/UrunResim/KucukResim/" + DataBinder.Eval(Container.DataItem, "banner_bilgiimaj2") + ".jpg\" />" : ""%>
                            <%# !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "banner_bilgiimaj3").ToString()) ? "<img src=\"/UrunResim/KucukResim/" + DataBinder.Eval(Container.DataItem, "banner_bilgiimaj3") + ".jpg\" />" : ""%>
                            <%# !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "banner_bilgiimaj4").ToString()) ? "<img src=\"/UrunResim/KucukResim/" + DataBinder.Eval(Container.DataItem, "banner_bilgiimaj4") + ".jpg\" />" : ""%>
                            <%# !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "banner_bilgiimaj5").ToString()) ? "<img src=\"/UrunResim/KucukResim/" + DataBinder.Eval(Container.DataItem, "banner_bilgiimaj5") + ".jpg\" />" : ""%>
                            <%# !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "banner_bilgiimaj6").ToString()) ? "<img src=\"/UrunResim/KucukResim/" + DataBinder.Eval(Container.DataItem, "banner_bilgiimaj6") + ".jpg\" />" : ""%>
                            <%# !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "banner_bilgiimaj7").ToString()) ? "<img src=\"/UrunResim/KucukResim/" + DataBinder.Eval(Container.DataItem, "banner_bilgiimaj7") + ".jpg\" />" : ""%>
                            <%# !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "banner_bilgiimaj8").ToString()) ? "<img src=\"/UrunResim/KucukResim/" + DataBinder.Eval(Container.DataItem, "banner_bilgiimaj8") + ".jpg\" />" : ""%>
                            <%# !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "banner_bilgiimaj9").ToString()) ? "<img src=\"/UrunResim/KucukResim/" + DataBinder.Eval(Container.DataItem, "banner_bilgiimaj9") + ".jpg\" />" : ""%>
                            <%# !string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "banner_bilgiimaj10").ToString()) ? "<img src=\"/UrunResim/KucukResim/" + DataBinder.Eval(Container.DataItem, "banner_bilgiimaj10") + ".jpg\" />" : ""%>--%>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul> </div>
        <!-- END MAIN CONTENT -->
        <!-- NAVIGATOR -->
        <div class="navigator-content">
            <div class="button-control">
                <span></span>
            </div>
            <div class="navigator-wrapper">
                <ul class="navigator-wrap-inner">
                    <asp:Literal ID="ltrSayfalama" runat="server"></asp:Literal>
                </ul>
            </div>
        </div>
        <!----------------- END OF NAVIGATOR --------------------->
        </div> </div> </div>
    </FooterTemplate>
</asp:Repeater>
