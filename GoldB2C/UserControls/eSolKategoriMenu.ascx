﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="eSolKategoriMenu.ascx.cs"
    Inherits="UserControls_eSolKategoriMenu" %>
<div class="filterContainer">
    <div class="filterTitle">
        <%=KategorilerBaslik %></div>
    <!-- first hierrarchy -->
    <div class="filterHierrarchy">
        <!-- first category -->
        <div class="firstCategory" id="DivKategori1" runat="server">
            <ul class="hierrarchyMenu">
                <asp:Repeater ID="rptKategori1" runat="server">
                    <ItemTemplate>
                        <li><span><a href="<%#Eval("UrunGrupUrl") %>" class="current">
                            <%#Eval("UrunGrupAd") %></a></span></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <!-- first category -->
        <!-- second category -->
        <div class="secondCategory" id="DivKategori2" runat="server">
            <ul class="hierrarchyMenu">
                <asp:Repeater ID="rptKategori2" runat="server">
                    <ItemTemplate>
                        <li><span><a href="<%#Eval("UrunGrupUrl") %>" class="current">
                            <%#Eval("UrunGrupAd")%></a></span></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <!-- second category -->
        <!-- third category -->
        <div class="thirdCategory" id="DivKategori3" runat="server">
            <ul class="hierrarchyMenu">
                <asp:Repeater ID="rptKategori3" runat="server">
                    <ItemTemplate>
                        <li><span><a href="<%#Eval("UrunGrupUrl") %>" class="current">
                            <%#Eval("UrunGrupAd")%></a></span></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <!-- third category -->
        <!-- fourth category -->
        <div class="fourthCategory" id="DivKategori4" runat="server">
            <ul class="hierrarchyMenu">
                <asp:Repeater ID="rptKategori4" runat="server">
                    <ItemTemplate>
                        <li><span><a href="<%#Eval("UrunGrupUrl") %>" class="current">
                            <%#Eval("UrunGrupAd")%></a></span></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <!-- fourth category -->
        <!-- fifth category -->
        <div class="fifthCategory" id="DivKategori5" runat="server">
            <ul class="hierrarchyMenu">
                <asp:Repeater ID="rptKategori5" runat="server">
                    <ItemTemplate>
                        <li><span><a href="<%#Eval("UrunGrupUrl") %>" class="current">
                            <%#Eval("UrunGrupAd")%></a></span></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <!-- fifth category -->
        <!-- other category-->
        <div class="otherCategory fourthOther" runat="server">
            <div id="imgDigerKategoriler" style="cursor: pointer;" onclick="ShowMore('imgDigerKategoriler','imgDigerContainer','slidingDiv');">
                <div id="imgDigerContainer">
                    <img id="imgDiger" src="/imagesgld/icon_allcategory.png" alt="Diğer Kategotiler" />
                    Diğer Kategoriler
                </div>
            </div>
            <div id="slidingDiv" class="slidingDiv" style="display: none;">
                <ul class="hierrarchyMenu" id="showme">
                    <li><span><a href="#">Boks</a></span></li>
                    <li><span><a href="#">Futbol</a></span></li>
                    <li><span><a href="#">Tenis</a></span></li>
                    <li><span><a href="#">Kayak-Snowboard</a></span></li>
                    <li><span><a href="#">Atletizm Ürünleri</a></span></li>
                    <li><span><a href="#">Baseball</a></span></li>
                    <li><span><a href="#">Bilardo</a></span></li>
                    <li><span><a href="#">Futbol</a></span></li>
                    <li><span><a href="#">Jimnastik</a></span></li>
                    <li><span><a href="#">Voleybol</a></span></li>
                    <li><span><a href="#">Hentbol</a></span></li>
                </ul>
            </div>
        </div>
        <!-- other category-->
    </div>
    <!-- first hierrarchy -->
</div>
<script type="text/javascript">
    function ShowMore(sender, senderContainer, cId) {
        if ($("#" + cId).css("display") == "none") {
            $("#" + senderContainer).delay(500).fadeOut(400);
            $("#" + sender).delay(800).slideUp(250);
            $("#" + cId).slideDown(500);
        }
        else {
            $("#" + cId).slideUp('fast');
            $("#" + sender).html('Diger');
        }
    }
</script>
