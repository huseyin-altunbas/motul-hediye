﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OzelKategoriMenuV2.ascx.cs"
    Inherits="UserControls_OzelKategoriMenuV2" %>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#accordion").accordion({ autoHeight: false });
    });
</script>
<div id="lmframe">
    <div id="accordion">
        <asp:Repeater ID="rptOzelKategoriMenu" runat="server" OnItemDataBound="rptOzelKategoriMenu_OnItemDataBound">
            <HeaderTemplate>
                <h3>
                    <a href="#">
                        <asp:Literal ID="ltrBaslik" runat="server"></asp:Literal>
                    </a>
                </h3>
                <div>
            </HeaderTemplate>
            <ItemTemplate>
                <a href="/OK<%# DataBinder.Eval(Container.DataItem, "master_id") %>/<%# DataBinder.Eval(Container.DataItem, "kategori_url")%>_<%# DataBinder.Eval(Container.DataItem, "detay_id") %>">
                    <%# DataBinder.Eval(Container.DataItem, "ayakizi_ad") %>
                </a>
            </ItemTemplate>
            <FooterTemplate>
                </div>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>
