﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;

public partial class UserControls_DinamikBanner : System.Web.UI.UserControl
{

    private int _UrunGrupId;
    public int UrunGrupId
    {
        get { return _UrunGrupId; }
        set { _UrunGrupId = value; }
    }

    VitrinIslemleriBLL bllVitrinIslemleri = new VitrinIslemleriBLL();
    string sayfalama;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            sayfalama = "";
            DataTable dtDinamikBanner = new DataTable();
            try
            {
                dtDinamikBanner = bllVitrinIslemleri.GetVitrinBanner(_UrunGrupId);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message + " UrunGrupId : " + _UrunGrupId.ToString());
            }
           

            if (dtDinamikBanner != null && dtDinamikBanner.Rows.Count > 0)
            {
                for (int i = 1; i <= dtDinamikBanner.Rows.Count; i++)
                {
                    sayfalama = sayfalama + "<li><span>" + i.ToString() + "</span></li>";
                }

                rptDinamikBanner.DataSource = dtDinamikBanner;
                rptDinamikBanner.DataBind();

            }
        }
    }

    protected void rptDinamikBanner_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            ((Literal)e.Item.FindControl("ltrSayfalama")).Text = sayfalama;
        }

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            decimal brutTutar = Convert.ToDecimal(((DataRowView)e.Item.DataItem).Row["brut_tutar"]);
            decimal netTutar = Convert.ToDecimal(((DataRowView)e.Item.DataItem).Row["net_tutar"]);

            if (netTutar < brutTutar)
            {
                ((Literal)e.Item.FindControl("ltrBrutTutar")).Text = ((DataRowView)e.Item.DataItem).Row["brut_tutar"].ToString();
                ((Literal)e.Item.FindControl("ltrNetTutar")).Text = ((DataRowView)e.Item.DataItem).Row["net_tutar"].ToString();
                ((Literal)e.Item.FindControl("ltrTlText")).Text = "PUAN";
            }
            else
            {
                ((Literal)e.Item.FindControl("ltrBrutTutar")).Text = "";
                ((Literal)e.Item.FindControl("ltrNetTutar")).Text = ((DataRowView)e.Item.DataItem).Row["net_tutar"].ToString();
                ((Label)e.Item.FindControl("lblIndirimliText")).Visible = false;
            }


        }
    }
}