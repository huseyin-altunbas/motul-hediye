﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;

public partial class UserControls_FooterBanner : System.Web.UI.UserControl
{
    BannerBLL bllBanner = new BannerBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dtBanner = new DataTable();
            dtBanner = bllBanner.GetirBanner(2);
            if (dtBanner.Rows.Count > 0)
                ltrFooterBanner.Text = dtBanner.Rows[0]["BannerHtml"].ToString();
        }
    }
}