﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Specialized;
using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using System.Web.Services;
using System.Text;
using System.Xml.Linq;

public partial class UserControls_VerticalMenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MenuOlustur();
        }
    }

    public void MenuOlustur()
    {
        string subKategori = "";
        StringBuilder sbSubKategori = new StringBuilder();

        DataTable dtKategoriItem = new DataTable();
        KategoriIslemleriBLL bllKategoriIslemleri = new KategoriIslemleriBLL();
        dtKategoriItem = bllKategoriIslemleri.GetirKategoriItems().AsEnumerable()
            .OrderBy(x => x.Field<int>("ItemOrder")).CopyToDataTable();

        string ItemId = string.Empty;
        string ItemHtml = string.Empty;
        string ItemName = string.Empty;
        string ItemUrl = string.Empty;
        string AllSubCategoryHtml = string.Empty;
        string ItemIconImage = string.Empty;
        string ItemHorizontalBannerUrl = string.Empty;
        string ItemHorizontalBannerImage = string.Empty;
        string ItemVerticalBannerUrl = string.Empty;
        string ItemVerticalBannerImage = string.Empty;

        int categoryCount = 0;
        foreach (DataRow dr in dtKategoriItem.Rows)
        {
            categoryCount++;
            ItemId = string.Empty;
            ItemHtml = string.Empty;
            ItemName = string.Empty;
            ItemUrl = string.Empty;
            AllSubCategoryHtml = string.Empty;
            ItemIconImage = string.Empty;
            ItemHorizontalBannerUrl = string.Empty;
            ItemHorizontalBannerImage = string.Empty;
            ItemVerticalBannerUrl = string.Empty;
            ItemVerticalBannerImage = string.Empty;

            ItemId = dr["ItemId"].ToString();
            ItemHtml = dr["ItemHtml"].ToString();
            ItemName = dr["ItemName"].ToString();
            ItemUrl = dr["ItemUrl"].ToString();
            AllSubCategoryHtml = dr["AllSubCategoryHtml"].ToString();


            if (dr["ItemIconImage"] != null && dr["ItemIconImage"].ToString() != string.Empty)
                ItemIconImage = "<img class=\"icon-menu\" style=\"max-width:30px;\" src=\"/assets/data/" + dr["ItemIconImage"].ToString() + "\">";
            else
                ItemIconImage = string.Empty;


            if (dr["ItemHorizontalBannerImage"] != null && dr["ItemHorizontalBannerImage"].ToString() != string.Empty)
            {

                if (dr["ItemHorizontalBannerUrl"] != null && dr["ItemHorizontalBannerUrl"].ToString() != string.Empty)
                    ItemHorizontalBannerUrl = dr["ItemHorizontalBannerUrl"].ToString();
                else
                    ItemHorizontalBannerUrl = "#";

                //ItemHorizontalBannerImage = "<div class=\"mega-custom-html col-sm-12\" style=\"padding-bottom: 10px;\">"
                //        + "<a href=\"" + ItemHorizontalBannerUrl + "\">"
                //        + "<img src=\"/assets/images/verticalmenubanner/" + dr["ItemHorizontalBannerImage"].ToString() + "\" alt=\"Banner\"></a>"
                //        + "</div>";
            }
            else
                ItemHorizontalBannerImage = string.Empty;


            if (dr["ItemVerticalBannerImage"] != null && dr["ItemVerticalBannerImage"].ToString() != string.Empty)
            {
                if (dr["ItemVerticalBannerUrl"] != null && dr["ItemVerticalBannerUrl"].ToString() != string.Empty)
                    ItemVerticalBannerUrl = dr["ItemVerticalBannerUrl"].ToString();
                else
                    ItemVerticalBannerUrl = "#";

                //ItemVerticalBannerImage = "<li>"
                //                        + "<a href=\"" + ItemVerticalBannerUrl + "\">"
                //                        + "<img src=\"/assets/images/verticalmenubanner/" + dr["ItemVerticalBannerImage"].ToString() + "\" alt=\"Banner\"></a>"
                //                        + "</li>";
            }
            else
                ItemVerticalBannerImage = string.Empty;


            if (ItemHtml == "")
            {
                if (categoryCount < 12)
                    subKategori = "<li><a class=\"\" href=\"" + ItemUrl + "\">"
                                + ItemIconImage + ItemName + "</a>";
                else
                    subKategori = "<li class=\"cat-link-orther\"><a class=\"\" href=\"" + ItemUrl + "\">"
                                + ItemIconImage + ItemName + "</a>";
            }
            else
            {
                if (categoryCount < 12)
                    subKategori = "<li><a class=\"parent\" href=\"" + ItemUrl + "\">"
                                + ItemIconImage + ItemName + "</a>";
                else
                    subKategori = "<li class=\"cat-link-orther\"><a class=\"parent\" href=\"" + ItemUrl + "\">"
                            + ItemIconImage + ItemName + "</a>";

                string divs = "";

                divs = bllKategoriIslemleri.GetirKategoriItems(ItemId).Rows[0]["ItemHtml"].ToString();

                string newDivs = "<divs>" + divs + "</divs>";
                if (newDivs.Contains("&amp;"))
                    newDivs = newDivs.Replace("&amp;", "&");
                if (newDivs.Contains("&amp"))
                    newDivs = newDivs.Replace("&amp", "&");//alt satırda &amp; -> &amp;amp; olmaması için.
                newDivs = newDivs.Replace("&", "&amp;");


                XDocument xDoc = new XDocument();
                var xmlDivs = XElement.Parse(newDivs);
                xDoc.Add(xmlDivs);

                int i = 0;
                var liCategory = xDoc.Descendants("div").Descendants("a").Where(element => element.Attribute("class").Value == "categoryName");

                subKategori += "<div class=\"vertical-dropdown-menu\"><div class=\"vertical-groups col-sm-12\">"
                    + "<h4 class=\"mega-group-header\"><span>" + ItemName + "</span></h4>";

                if (ItemVerticalBannerImage == string.Empty)
                {
                    int rowCount = 6;
                    int columnCount = 4;//12'yi tam bölen bir sayı olmalı.
                    bool isOpen = true;
                    foreach (XElement element in liCategory)
                    {
                        i++;
                        if (i % rowCount == 1)
                        {
                            subKategori += "<div class=\"mega-group col-sm-" + (12 / columnCount).ToString() + "\">"
                                        + "<ul class=\"group-link-default\">";
                            isOpen = true;
                        }
                        subKategori += "<li>" + element.ToString() + "</li>";
                        if (i % rowCount == 0)
                        {
                            subKategori += "</ul>"
                                   + "</div>";
                            isOpen = false;
                        }
                    }
                    if (isOpen)
                    {
                        subKategori += "</ul>"
                                   + "</div>";
                    }
                    if (liCategory.Count() <= rowCount * (columnCount - 1) && ItemVerticalBannerImage != string.Empty)
                    {
                        subKategori += "<div class=\"mega-group col-sm-" + (12 / columnCount).ToString() + "\">"
                                    + "<ul class=\"group-link-default\">"
                                    + ItemVerticalBannerImage
                                    + "</ul>"
                                   + "</div>";
                    }
                    subKategori += "</div>";

                    if (ItemHorizontalBannerImage != string.Empty)
                        subKategori += ItemHorizontalBannerImage;
                }
                else
                {
                    int rowCount = 6;
                    int columnCount = 4;//12'yi tam bölen bir sayı olmalı.
                    bool isOpen = true;
                    foreach (XElement element in liCategory)
                    {
                        i++;
                        if (i % rowCount == 1)
                        {
                            subKategori += "<div class=\"mega-group col-sm-" + (12 / columnCount).ToString() + "\">"
                                        + "<ul class=\"group-link-default\">";
                            isOpen = true;
                        }
                        subKategori += "<li>" + element.ToString() + "</li>";
                        if (i % rowCount == 0)
                        {
                            subKategori += "</ul>"
                                   + "</div>";
                            isOpen = false;
                        }
                    }
                    if (isOpen)
                    {
                        subKategori += "</ul>"
                                   + "</div>";
                    }
                    if (liCategory.Count() <= rowCount * (columnCount - 1) && ItemVerticalBannerImage != string.Empty)
                    {
                        subKategori += "<div class=\"mega-group col-sm-" + (12 / columnCount).ToString() + "\">"
                                    + "<ul class=\"group-link-default\">"
                                    + ItemVerticalBannerImage
                                    + "</ul>"
                                   + "</div>";
                    }
                    subKategori += "</div>";

                    if (ItemHorizontalBannerImage != string.Empty)
                        subKategori += ItemHorizontalBannerImage;
                }
                subKategori += "</div>";
            }
            subKategori += "</li>";

            sbSubKategori.Append(subKategori);

        }

        ltrVerticalMenu.Text = sbSubKategori.ToString();

    }
}