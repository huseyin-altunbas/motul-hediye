﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using Goldb2cInfrastructure;

public partial class GununFirsatlari : System.Web.UI.Page
{
    FirsatUrunleriBLL bllFirsatUrunleri = new FirsatUrunleriBLL();
    public static string ImageDomain = CustomConfiguration.ImageDomain;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            rptGununFirsatlari.DataSource = bllFirsatUrunleri.GetFirsatUrun(0, 0);
            rptGununFirsatlari.DataBind();
        }
    }
}