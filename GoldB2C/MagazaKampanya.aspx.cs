﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using Goldb2cInfrastructure;

public partial class MagazaKampanya : System.Web.UI.Page
{
    public static string ImageDomain = CustomConfiguration.ImageDomain;
    MagazaBilgileriBLL bllMagazaBilgileri = new MagazaBilgileriBLL();
    DataTable dtDuyuruMagazaListesi;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dtDuyuruMagazaListesi = new DataTable();
            dtDuyuruMagazaListesi = (DataTable)bllMagazaBilgileri.MagazaDuyurulari();

            DataTable dtMagazaListesi = new DataTable();
            DataView dtView = new DataView(dtDuyuruMagazaListesi);
            dtMagazaListesi = dtView.ToTable(true, "magaza_ad");

            if (dtDuyuruMagazaListesi != null && dtDuyuruMagazaListesi.Rows.Count > 0)
            {
                foreach (DataRow dr in dtMagazaListesi.Rows)
                {
                    if (dr["magaza_ad"].ToString().Length > 29)
                    {
                        dr["magaza_ad"] = dr["magaza_ad"].ToString().Replace("Mağaza", "Mğz");
                        dr["magaza_ad"] = dr["magaza_ad"].ToString().Substring(0, 26).Trim() + "...";
                    }
                }

                rptMagazalar.DataSource = dtMagazaListesi;
                rptMagazalar.DataBind();
            }
            else
            {
                imgKampanyaIcerik.ImageUrl = "~/imagesgld/MagazaKampanya.jpg";
            }

        }
    }


    protected void rptMagazalar_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptKampanya = (Repeater)e.Item.FindControl("rptKampanyalar");
            rptKampanya.DataSource = dtDuyuruMagazaListesi.Select("magaza_ad = '" + ((DataRowView)e.Item.DataItem).Row["magaza_ad"].ToString() + "'");
            rptKampanya.DataBind();
        }
    }

    protected void rptKampanyalar_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            if (string.IsNullOrEmpty(imgKampanyaIcerik.ImageUrl))
            {
                imgKampanyaIcerik.ImageUrl = "http://" + ImageDomain + ((ErpData.StrongTypesNS.MagazaDuyurulariTTRow)(e.Item.DataItem)).imaj_Adres;
            }
        }
    }
}


