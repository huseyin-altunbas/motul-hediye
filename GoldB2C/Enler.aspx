﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true" CodeFile="Enler.aspx.cs" Inherits="Enler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <!-- rootercontainer -->
      <div class="rootercontainer">
          
          <!-- rootercnav -->
          <div class="rootercnav">
              <div class="rcframe">
                 <div class="rootername">
                    <a href="#">En Yeniler</a>
                 </div>
                   <div class="rooterbuttons">
                     <div class="status">
                         <%--<div class="offerbtn"><a href="#">Karşılaştır</a></div>--%>
                         <div class="listbutton"><a href="#">Sırala</a>
                         	  <ul class="listdropdown">
                                    <li><a href="#">Eklenme Tarihine Göre</a></li>
                                    <li><a href="#">A dan Z ye</a></li>
                                    <li><a href="#">Z den A ya</a></li>
                                    <li><a href="#">Hızlı Gönderi</a></li>
                                    <li><a href="#">Kurye İle Gönderi</a></li>
                                    <li><a href="#">Temin Edilebilenler</a></li>
                                    <li><a href="#">Düşük Fiyat-Yüksek Fiyat</a></li>
                                    <li><a href="#">Yüksek Fiyat-Düşük Fiyat</a></li>
                                </ul>
                         </div>
                     </div>
                   <%--  <a href="#" class="someClass" title="En Çok Satan"><img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" /></a>
                     <a href="#" class="someClass" title="Yeni Ürün"><img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" /></a>
                     <a href="#" class="someClass" title="Kargo Bedava"><img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" /></a>
                     <a href="#" class="someClass" title="İndirimli Ürün"><img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" /></a>
                     <a href="#" class="someClass" title="Hediyeli Ürün"><img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" /></a>
                     <a href="#" class="someClass" title="Özel Ürün"><img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" /></a>
                     <a href="#" class="someClass" title="Hızlı Kargo"><img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" /></a>--%>
                 </div>
              </div>
          </div>
          <!-- rootercnav -->
          
          <!-- leftmaster -->
          <div class="leftmaster">
            <div id="lmframe">
                
                <!-- filter menu -->  
                <div class="filterMenu">
                	
                    <!-- Alt Kategoriler -->
                    <div class="filterContainer"> 
                        <div class="filterTitle">Alt Kategoriler</div>
                        	<div class="filterCategory">
                				<ul class="first">                   
                                    <li><span><a href="#">Spor ve Fitness</a></span>
                                        <ul class="second">
                                            <li><span><a href="#">Spor Branşları</a></span>
                                                <ul class="third">
                                                    <li><span><a href="#">Masa Tenisi <span class="prnumber">(218)</span></a></span>
                                                    	<ul class="fourth">
                                                            <li><span><a href="#" class="current">Masa Tenisi (Ping..) <span class="prnumber">(29)</span></a></span>
                                                            	<ul class="fifth">
                                                                    <li><span><a href="#">3 Yıldız Maç Topu <span class="prnumber">(17)</span></a></span></li>
                                                                    <li><span><a href="#">Yıldız Maç Topu <span class="prnumber">(22)</span></a></span></li>
                                                                    <li><span><a href="#">Notebook Aksesuar <span class="prnumber">(23)</span></a></span></li>
                                                                    <li><span><a href="#">Tablet PC <span class="prnumber">(5)</span></a></span></li>
                                                                    <li><span><a href="#">Taşınabilir Bilgisayar <span class="prnumber">(6)</span></a></span></li>                
                                                                </ul>
                                                            </li>                
                                                        </ul>
                                                            <div>
                                                            	<script>
                                                            	    $(document).ready(function () {
                                                            	        $(".slidingDiv").hide();
                                                            	        $(".show_hide").show();

                                                            	        $('.show_hide').click(function () {
                                                            	            $(".slidingDiv").slideToggle();
                                                            	        });
                                                            	    });
																</script>
                                                            
                                                                <a href="#" class="show_hide"><img src="imagesgld/icon_allcategory.png" alt="Diğer Kategotiler" /> Diğer Kategoriler</a>
                                                                <div class="slidingDiv">
                                                                    <ul id="showme">
                                                                        <li><span><a href="#">Boks</a></span></li>
                                                                        <li><span><a href="#">Futbol</a></span></li>
                                                                        <li><span><a href="#">Tenis</a></span></li>
                                                                        <li><span><a href="#">Kayak-Snowboard</a></span></li>
                                                                        <li><span><a href="#">Atletizm Ürünleri</a></span></li>
                                                                        <li><span><a href="#">Baseball</a></span></li>
                                                                        <li><span><a href="#">Bilardo</a></span></li>
                                                                        <li><span><a href="#">Futbol</a></span></li>
                                                                        <li><span><a href="#">Jimnastik</a></span></li>
                                                                        <li><span><a href="#">Voleybol</a></span></li>
                                                                        <li><span><a href="#">Hentbol</a></span></li>                
                                                                    </ul>
                                                                </div>
                                                        </div>
															
                                                    </li>             
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                            	</ul>
           				    </div>               
                    </div>
                    <!-- Alt Kategoriler -->
                    
                    <!-- Markalar -->
                    <div class="filterContainer">
                        <div class="filterTitle">Markalar</div>
                        <div class="filterList">
                            <ul class="List">
                                <li><input name="checkbox" type="checkbox" value="0" /> APPLE</li>
                                <li><input name="checkbox" type="checkbox" value="0" /> ACER</li>
                                <li><input name="checkbox" type="checkbox" value="0" /> CASPER</li>
                                <li><input name="checkbox" type="checkbox" value="0" /> ESCORT</li>
                                <li><input name="checkbox" type="checkbox" value="0" /> EXPER</li>
                                <li><input name="checkbox" type="checkbox" value="0" /> MSI</li>
                                <li><input name="checkbox" type="checkbox" value="0" /> TOSHIBA</li>
                            </ul>
                        </div> 
                    </div>
                    <!-- Markalar -->
                    
                    
                    <!-- FiyatınaGore -->
                    <div class="filterContainer">
                        <div class="filterTitle">Puanına Göre</div>                        
                        
                        <div class="layoutSlider">
                        	<div style="float:right; width:100%;"><div id="slider-range"> </div></div>
                            <div style="float:left; width:100%;"><input type="text" id="amount" /></div>
                        </div>
                    
                    </div>
                    <!-- FiyatınaGore -->
                    
                    
                    <!-- Degerlendirme -->
                    <div class="filterContainer">
                        <div class="filterTitle">Degerlendirme</div>
                        
                        <div class="filterList">
                            <ul class="List">
                                <li><input name="checkbox" type="checkbox" value="0" />  
                                    <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                    <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                    <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                    <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                    <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                </li>
                                <li><input name="checkbox" type="checkbox" value="0" />  
                                	<img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                    <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                    <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                    <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />

                                </li>
                                <li><input name="checkbox" type="checkbox" value="0" />  
                                    <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                    <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                    <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                </li>
                                <li><input name="checkbox" type="checkbox" value="0" />  
                                	<img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                    <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                </li>
                                <li><input name="checkbox" type="checkbox" value="0" />  
                                	<img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Değerlendirme" />
                                </li>
                            </ul>
                        </div> 
                    
                    </div>
                    <!-- Degerlendirme -->
                    
                </div>
                <!-- filter menu -->
                               
                
            </div>
          </div>
          <!-- leftmaster -->
          
          <!-- rightmaster -->
          <div class="rightmaster">
            <div id="rmframe">
                <ul class="productList">
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic1s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Smartphone</a> / <a href="#">Apple</a></div>
                            <div class="productName"><a href="#">SAMSUNG UE-40D5003 40 (120cm) LED TV</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic2s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Dizüstü Bilgisayar Laptop</a> / <a href="#">Acer</a></div>
                            <div class="productName"><a href="#">AERA AOD257-138QRR N455 1 G 250G 10.1 W7S KIRMIZI AERA AOD257-138QRR N455 1 G 250G 10.1 W7S KIRMIZI</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic3s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Dizüstü Bilgisayar Laptop</a> / <a href="#">Sony</a></div>
                            <div class="productName"><a href="#">SONY AOD257-138QRR N455 1 G 250G 10.1 W7S SİYAH</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic1s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Smartphone</a> / <a href="#">Apple</a></div>
                            <div class="productName"><a href="#">SAMSUNG UE-40D5003 40 (120cm) LED TV</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic2s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Dizüstü Bilgisayar Laptop</a> / <a href="#">Acer</a></div>
                            <div class="productName"><a href="#">AERA AOD257-138QRR N455 1 G 250G 10.1 W7S KIRMIZI AERA AOD257-138QRR N455 1 G 250G 10.1 W7S KIRMIZI</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic3s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Dizüstü Bilgisayar Laptop</a> / <a href="#">Sony</a></div>
                            <div class="productName"><a href="#">SONY AOD257-138QRR N455 1 G 250G 10.1 W7S SİYAH</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic1s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Smartphone</a> / <a href="#">Apple</a></div>
                            <div class="productName"><a href="#">SAMSUNG UE-40D5003 40 (120cm) LED TV</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic2s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Dizüstü Bilgisayar Laptop</a> / <a href="#">Acer</a></div>
                            <div class="productName"><a href="#">AERA AOD257-138QRR N455 1 G 250G 10.1 W7S KIRMIZI AERA AOD257-138QRR N455 1 G 250G 10.1 W7S KIRMIZI</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic3s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Dizüstü Bilgisayar Laptop</a> / <a href="#">Sony</a></div>
                            <div class="productName"><a href="#">SONY AOD257-138QRR N455 1 G 250G 10.1 W7S SİYAH</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic1s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Smartphone</a> / <a href="#">Apple</a></div>
                            <div class="productName"><a href="#">SAMSUNG UE-40D5003 40 (120cm) LED TV</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic2s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Dizüstü Bilgisayar Laptop</a> / <a href="#">Acer</a></div>
                            <div class="productName"><a href="#">AERA AOD257-138QRR N455 1 G 250G 10.1 W7S KIRMIZI AERA AOD257-138QRR N455 1 G 250G 10.1 W7S KIRMIZI</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic3s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Dizüstü Bilgisayar Laptop</a> / <a href="#">Sony</a></div>
                            <div class="productName"><a href="#">SONY AOD257-138QRR N455 1 G 250G 10.1 W7S SİYAH</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic1s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Smartphone</a> / <a href="#">Apple</a></div>
                            <div class="productName"><a href="#">SAMSUNG UE-40D5003 40 (120cm) LED TV</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic2s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Dizüstü Bilgisayar Laptop</a> / <a href="#">Acer</a></div>
                            <div class="productName"><a href="#">AERA AOD257-138QRR N455 1 G 250G 10.1 W7S KIRMIZI AERA AOD257-138QRR N455 1 G 250G 10.1 W7S KIRMIZI</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                    
                    <!-- product -->
                    <li>
                      <div class="productContainer">
                            <div class="productPic"><a href="#"><img src="imagesgld/products_pic3s.jpg" width="200" height="200" alt="iphone" /></a></div>
                            <div class="productCategory"><a href="#">Dizüstü Bilgisayar Laptop</a> / <a href="#">Sony</a></div>
                            <div class="productName"><a href="#">SONY AOD257-138QRR N455 1 G 250G 10.1 W7S SİYAH</a></div>
                            <div class="productPrice"><span class="discount">1.850PUAN</span> 1.499PUAN <span class="kdv"></span></div>
                            <div class="productCommentSearch">
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                (142 Yorum)
                            </div>
                            <div class="productButtons">
                                 <img src="imagesgld/icon_coksatanurunOn.png" width="29" height="29" alt="En Çok Satan" class="someClass" title="En Çok Satan" />
                                 <img src="imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" class="someClass" title="Yeni Ürün"/>
                                 <img src="imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" class="someClass" title="Kargo Bedava"/>
                                 <img src="imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" class="someClass" title="İndirimli Ürün"/>
                                 <img src="imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" class="someClass" title="Hediyeli Ürün"/>
                                 <img src="imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" class="someClass" title="Özel Ürün"/>
                                 <img src="imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" class="someClass" title="Hızlı Kargo"/>
                          </div>
                      </div>
                     </li>
                    <!-- product -->
                     
                </ul>
                
                
                <!-- pagination -->
                <div id="pagingcontainer">
                	<div class="pagination">
                    <a href="" class="skip" title="Geri"><img src="imagesgld/arrow_paginationLeft.png" alt="Geri"/></a>
                        <span> | </span>
               			 <a href="">1</a>
                        <span> | </span>
                        <strong>2</strong>
                        <span> | </span>
                        <a href="">3</a>
                        <span> | </span>
                        <a href="">4</a>
                        <span> | </span>
                    	<a href="">5</a>
                        <span> | </span>
                        <a href="">6</a>
                    	<span> | </span>
                    <a href="" class="skip" title="İleri"><img src="imagesgld/arrow_paginationRight.png" alt="İleri"/></a>
           	   </div>
                </div>
                <!-- pagination -->
               
               
            </div>
          </div>
          <!-- rightmaster -->
          
      </div>
      <!-- rootercontainer -->
</asp:Content>

