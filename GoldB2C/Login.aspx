﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Login</title>
    <link rel="stylesheet" href="css/login-screen/reset.css">
    <link rel="stylesheet" href="css/login-screen/animate.css">
    <link rel="stylesheet" href="css/login-screen/styles.css">

    <script src="js/jquery-1.10.1.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <link href="css/jquery.msgbox.css" rel="stylesheet" />
    <script src="js/jquery.msgbox.min.js"></script>
</head>
<body>
    <div id="container">
        <img style="padding: 10px 0px 0px 0px;" src="images/logoXXX.png" alt="Logo">
        <form id="form1" runat="server">
            <label for="name">  Cep Telefonunuz: <span style=" font-size: 12px; color: red; line-height: 1.4; "><br> *Telefon numaranızı başında 0 olmadan giriniz.</span></label>
            <asp:TextBox ID="name" runat="server" ValidationGroup="a101"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="ValidationCSS1" runat="server" Display="None" ControlToValidate="name" ErrorMessage="Lütfen E-Posta Adresinizi Girin!" ValidationGroup="a101"></asp:RequiredFieldValidator>
            <div style="clear: both;"></div>
            <label for="username" style="float: left;">Şifreniz:</label>
            <p>
                <a href="Sifremi-Unuttum.aspx" style="margin-top: 10px;">Şifremi Unuttum?</a>
                <asp:TextBox ID="password" TextMode="Password" runat="server" ValidationGroup="a101"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="ValidationCSS1" runat="server" Display="None" ControlToValidate="password" ErrorMessage="Lütfen Şifrenizi Girin!" ValidationGroup="a101"></asp:RequiredFieldValidator>
            </p>



            
            <br />
           <%-- <p style="padding-left: 18px; padding-right: 31px;">


                <br />

                <asp:Image ID="Image1" ImageUrl="" runat="server" />

                <asp:TextBox ID="guvenlikkodu" runat="server" placeholder="Güvenlik Kodu" ></asp:TextBox>

            </p>--%>



            <div id="lower">
                <asp:CheckBox ID="CheckBox1" runat="server" /><label>Beni Hatırla</label>
                <asp:Button ID="Button1" ValidationGroup="a101" runat="server" Text="Giriş Yap" OnClick="Button1_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="a101" ShowMessageBox="true" ShowSummary="false" runat="server" />
            </div>
        </form>
    </div>
</body>


<style>
    #Image1 {
        width: 126px;
    }

    #guvenlikkodu {
        width: 140px;
        float: right;
        margin: inherit;
        padding-right: 30px;
    }

    #container {
        height: 467px;
    }
</style>
