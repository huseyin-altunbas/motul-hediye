﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FacebookUyeKayit.aspx.cs"
    Inherits="FacebookUyeKayit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/css/facebookLogin.css?v=2.2" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/css/facebookForm.css">
    <!--[if IE 7]><link rel="stylesheet" href="/css/facebookForm.ie7.css"><![endif]-->
    <script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="/js/jquery.plum.form.js"></script>
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/Gold/goldGenel.js?v=2.1.4" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="facebookLoginStepBox">
        <div class="facebookLoginHeader">
        </div>
        <div class="welcomeWebSite">
            Hoşgediniz <strong>
                <asp:Label ID="lblAdSoyad" runat="server" Text=""></asp:Label>,</strong><br />
            <br />
            Üyeliğinizi tamamlayabilmek adına aşağıdaki alanları doldurmanız gerekmektedir.
        </div>
        <div class="alert">
            <asp:Literal ID="ltrHataMesaj" runat="server"></asp:Literal>
        </div>
        <fieldset>
            <asp:DropDownList ID="dpSehir" runat="server">
            </asp:DropDownList>
            <span>
                <asp:TextBox ID="txtCepTelKod" CssClass="telephoneCode" runat="server"></asp:TextBox>
                <asp:TextBox ID="txtCepTelNo" CssClass="telephoneNumber" runat="server"></asp:TextBox>
            </span>
            <label>
                <asp:CheckBox ID="chkEposta" runat="server" Checked="true" />
                Kampanyalardan e-posta ile haberdar olmak istiyorum</label><br />
            <label>
                <asp:CheckBox ID="chkSMS" runat="server" Checked="true" />
                Kampanyalardan SMS ile haberdar olmak istiyorum</label><br />
            <asp:Button ID="btnKaydet" runat="server" Text="" CssClass="BTN" OnClick="btnKaydet_Click" />
            <asp:Button ID="btnVazgec" runat="server" Text="" CssClass="cancelBTN" OnClick="btnVazgec_Click"
                OnClientClick="Validasyon(this.id);" />
        </fieldset>
    </div>
    <asp:HiddenField ID="hdnVazgec" runat="server" />
    </form>
    <script type="text/javascript">
        $("#<%=dpSehir.ClientID %>").plum('form');
        $("#<%=txtCepTelKod.ClientID %>").plum('form');
        $("#<%=txtCepTelNo.ClientID %>").plum('form');
        $("#<%=chkSMS.ClientID %>").plum('form');
        $("#<%=chkEposta.ClientID %>").plum('form');

        $("#<%=txtCepTelKod.ClientID %>").mask("599");
        $("#<%=txtCepTelNo.ClientID %>").mask("9999999");



        document.getElementById('<%=form1.ClientID %>').onsubmit = function () {
            if ($("#<%=hdnVazgec.ClientID %>").val() != 'true') {
                var hataMesaj = "";
                var CepTelKod = $("#<%=txtCepTelKod.ClientID %>").val();
                var CepTelNo = $("#<%=txtCepTelNo.ClientID %>").val();
                var Sehir = $("#<%=dpSehir.ClientID %>").val();



                if (CepTelKod == null || CepTelKod == "" || CepTelNo == null || CepTelNo == "")
                    hataMesaj = hataMesaj + "<p><span><img src=\"imagesGld/fbCoonect/error.png\" alt=\"Hata\" /></span>Lütfen Cep Telefonu Numaranızı Giriniz.</p>";


                if (Sehir == null || Sehir == "")
                    hataMesaj = hataMesaj + "<p><span><img src=\"imagesGld/fbCoonect/error.png\" alt=\"Hata\" /></span>Lütfen Yaşadığınız Şehri Seçiniz.</p>";

                if (hataMesaj != "") {
                    $(".alert").html(hataMesaj);
                    return false;
                }


            }
        }

        function Validasyon() {
            $("#<%=hdnVazgec.ClientID %>").val('true');
        }        
    </script>
</body>
</html>
