﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Goldb2cBLL;
using Goldb2cInfrastructure;
using System.Text;

public partial class Vitrin : System.Web.UI.Page
{
    UrunGrupIslemleriBLL bllUrunGrupIslemleri = new UrunGrupIslemleriBLL();
    VitrinIslemleriBLL bllVitrinIslemleri = new VitrinIslemleriBLL();
    MarketingScriptHelper marketingScriptHelper = new MarketingScriptHelper();
    OlapUrun bllOlapUrun = new OlapUrun();
    public static string ImageDomain = CustomConfiguration.ImageDomain;
    int UrunGrupId;
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["Kullanici"] == null)
        //    Response.Redirect(@"\MusteriHizmetleri\UyeGirisi.aspx");
        if (!IsPostBack)
        {
            DataTable dtVitrin = new DataTable();

            if (!string.IsNullOrEmpty(Request.QueryString["UrunGrupId"]) && int.TryParse(Request.QueryString["UrunGrupId"], out UrunGrupId))
            {
                dtVitrin = bllVitrinIslemleri.GetVitrinListele(UrunGrupId);
                if (dtVitrin != null && dtVitrin.Rows.Count > 0)
                {

                    int rowsay = 0;
                    foreach (DataRow row in dtVitrin.Rows)
                    {
                        if (dtVitrin.Rows[rowsay]["resim_ad"].ToString().IndexOf("http") == -1)
                        {
                            dtVitrin.Rows[rowsay]["resim_ad"] = "http://cdn.gold.com.tr/UrunResim/BuyukResim/" + dtVitrin.Rows[rowsay]["resim_ad"];
                        }
                        rowsay = rowsay + 1;
                    }
                    rptUrunler.DataSource = dtVitrin;
                    rptUrunler.DataBind();


                    //SolKategoriKirilimMenu1.UrunGrupId = UrunGrupId;
                    //SolKategoriUstGrupMenu1.UrunGrupId = UrunGrupId;
                    AyakIziOlustur(UrunGrupId);
                    CokSatanUrunler(UrunGrupId);

                    //DinamikBanner1.UrunGrupId = UrunGrupId;
                }
                else
                    Response.Redirect("~/Default.aspx", false);
            }
            else
                Response.Redirect("~/Default.aspx", false);
        }
    }

    private void CokSatanUrunler(int UrunGrupId)
    {
        DataTable dtCokSatanUrun = bllOlapUrun.CokSatanUrun(UrunGrupId, 0, 0);
        if (dtCokSatanUrun != null && dtCokSatanUrun.Rows.Count > 0)
        {
            rptCokSatanlarVitrin.DataSource = dtCokSatanUrun;
            rptCokSatanlarVitrin.DataBind();
        }
        else
            rptCokSatanlarVitrin.Visible = false;
    }

    protected void rptUrunler_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        DataRowView drv = e.Item.DataItem as DataRowView;
        Label lblNetTutar = (Label)e.Item.FindControl("lblnet_tutar");
        Label lblBrutTutar = (Label)e.Item.FindControl("lblbrut_tutar");
        Label lblBilgi = (Label)e.Item.FindControl("lblBilgi");

        if (Convert.ToBoolean(drv.Row["temin_durum"]))
        {
            //Ürün Fiyatları
            decimal brut_tutar = Math.Round(Convert.ToDecimal(drv.Row["brut_tutar"]), 2);
            decimal net_tutar = Math.Round(Convert.ToDecimal(drv.Row["net_tutar"]), 2);

            decimal onikiTaksit = Math.Round((Convert.ToDecimal(drv.Row["net_tutar"]) / 12), 2);

            //lblnet_tutar.Text = onikiTaksit.ToString() + " PUAN";
            //lblBilgi.Text = "x 12 Taksit";

            //lblBilgi.Text = "KDV Dahil";
            if (brut_tutar > net_tutar)
            {
                lblBrutTutar.Text = brut_tutar.ToString("###,###,###") + " <span>PUAN</span>";
                lblNetTutar.Text = net_tutar.ToString("###,###,###") + " <span>PUAN</span>";
            }
            else
            {
                lblBrutTutar.Visible = false;
                lblNetTutar.Text = brut_tutar.ToString("###,###,###") + " <span>PUAN</span>";
            }

        }
        else
        {
            if (!string.IsNullOrEmpty(drv.Row["temin_aciklama"].ToString()))
                lblBilgi.Text = drv.Row["temin_aciklama"].ToString();
            else
                lblBilgi.Text = "Ürün geçici olarak temin edilemiyor.";
        }




        //İkonlar
        //if (Convert.ToBoolean(drv.Row["indirimli"]))
        //    ((Image)e.Item.FindControl("imgIndirimli")).ImageUrl = "~/imagesgld/icon_indirimliurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgIndirimli")).ImageUrl = "~/imagesgld/icon_indirimliurunOff.png";

        //if (Convert.ToBoolean(drv.Row["aynigun_sevk"]))
        //    ((Image)e.Item.FindControl("imgAyniGunSevk")).ImageUrl = "~/imagesgld/icon_hizlikargoOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgAyniGunSevk")).ImageUrl = "~/imagesgld/icon_hizlikargoOff.png";

        //if (Convert.ToBoolean(drv.Row["hediyeli_urun"]))
        //    ((Image)e.Item.FindControl("imgHediyeliUrun")).ImageUrl = "~/imagesgld/icon_hediyeliurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgHediyeliUrun")).ImageUrl = "~/imagesgld/icon_hediyeliurunOff.png";

        //if (Convert.ToBoolean(drv.Row["kuryeile_sevk"]))
        //    ((Image)e.Item.FindControl("imgKuryeileSevk")).ImageUrl = "~/imagesgld/icon_coksatanurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgKuryeileSevk")).ImageUrl = "~/imagesgld/icon_coksatanurunOff.png";

        //if (Convert.ToBoolean(drv.Row["kargo_bedava"]))
        //    ((Image)e.Item.FindControl("imgKargoBedava")).ImageUrl = "~/imagesgld/icon_kargobedavaOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgKargoBedava")).ImageUrl = "~/imagesgld/icon_kargobedavaOff.png";

        //if (Convert.ToBoolean(drv.Row["yeni_urun"]))
        //    ((Image)e.Item.FindControl("imgYeniUrun")).ImageUrl = "~/imagesgld/icon_yeniurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgYeniUrun")).ImageUrl = "~/imagesgld/icon_yeniurunOff.png";

    }

    private void AyakIziOlustur(int UrunGrupId)
    {
        DataTable dtAyakIzi = new DataTable();
        dtAyakIzi = bllUrunGrupIslemleri.GetUrunGrupIzi(UrunGrupId);
        string UcKategori = "";
        DataRow dr;
        if (dtAyakIzi.Rows.Count > 0)
        {
            dr = dtAyakIzi.Rows[0];


            //Meta description title
            if (!string.IsNullOrEmpty(dr["urun_title"].ToString()))
                Page.Title = dr["urun_title"].ToString();
            if (!string.IsNullOrEmpty(dr["urun_keywords"].ToString()))
                Page.MetaKeywords = dr["urun_keywords"].ToString();
            if (!string.IsNullOrEmpty(dr["urun_description"].ToString()))
                Page.MetaDescription = dr["urun_description"].ToString();

            if (!string.IsNullOrEmpty(dr["urunust_ad1"].ToString()))
            {
                UcKategori = dr["urunust_ad1"].ToString();
                ltrAyakizi.Text = "<a title=\"" + dr["urunust_ad1"].ToString() + "\" href=\"" + dr["url_rewrite1"].ToString() + "\">" + dr["urunust_ad1"].ToString() + "</a>";
            }
            if (!string.IsNullOrEmpty(dr["urunust_ad2"].ToString()))
            {
                UcKategori = dr["urunust_ad2"].ToString();
                ltrAyakizi.Text = ltrAyakizi.Text + "/<a title=\"" + dr["urunust_ad2"].ToString() + "\" href=\"" + dr["url_rewrite2"].ToString() + "\">" + dr["urunust_ad2"].ToString() + "</a>";
            }
            if (!string.IsNullOrEmpty(dr["urunust_ad3"].ToString()))
            {
                UcKategori = dr["urunust_ad3"].ToString();
                ltrAyakizi.Text = ltrAyakizi.Text + "/<a title=\"" + dr["urunust_ad3"].ToString() + "\" href=\"" + dr["url_rewrite3"].ToString() + "\">" + dr["urunust_ad3"].ToString() + "</a>";
            }
            if (!string.IsNullOrEmpty(dr["urunust_ad4"].ToString()))
            {
                UcKategori = dr["urunust_ad4"].ToString();
                ltrAyakizi.Text = ltrAyakizi.Text + "/<a title=\"" + dr["urunust_ad4"].ToString() + "\" href=\"" + dr["url_rewrite4"].ToString() + "\">" + dr["urunust_ad4"].ToString() + "</a>";
            }
            if (!string.IsNullOrEmpty(dr["urunust_ad5"].ToString()))
            {
                UcKategori = dr["urunust_ad5"].ToString();
                ltrAyakizi.Text = ltrAyakizi.Text + "/<a title=\"" + dr["urunust_ad5"].ToString() + "\" href=\"" + dr["url_rewrite5"].ToString() + "\">" + dr["urunust_ad5"].ToString() + "</a>";
            }
            //Content Remarketing
            ContentRemarketingAlan(dr["content_alan"].ToString(), dr["remarketing_alan"].ToString());



        }

    }

    private void ContentRemarketingAlan(string ContentAlan, string RemarketingAlan)
    {
        string Ayrac = "#DevamiVar#";
        if (!string.IsNullOrEmpty(ContentAlan))
        {
            int AyracIndex = ContentAlan.IndexOf(Ayrac);
            if (AyracIndex > -1)
            {
                ltrSeoContent.Text = ContentAlan.Substring(0, AyracIndex).TrimEnd();
                hdnSeoContent.Value = ContentAlan.Substring(AyracIndex + Ayrac.Length, ContentAlan.Length - (AyracIndex + Ayrac.Length)).TrimStart();
            }
            else
            {
                ltrSeoContent.Text = ContentAlan;
                hdnSeoContent.Value = "";
                spnSeoDevam.Visible = false;
            }
        }
        else
            DivSeoContent.Visible = false;

        if (!string.IsNullOrEmpty(RemarketingAlan))
        {
            ltrRemarketingAlan.Text = RemarketingAlan;
        }
    }
}