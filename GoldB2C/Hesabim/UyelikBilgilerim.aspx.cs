﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using System.Data;

public partial class Hesabim_UyelikBilgilerim : System.Web.UI.Page
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();

    Kullanici kullanici;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)Session["Kullanici"];
        }
        else
            UyelikIslemleriBLL.MusteriGirisineYonlendir(Request.Url.PathAndQuery);


        if (!IsPostBack)
        {

            dpSehir.DataSource = bllUyelikIslemleri.SehirListesi("TÜRKİYE");
            dpSehir.DataValueField = "SehirAd";
            dpSehir.DataTextField = "SehirAd";
            dpSehir.DataBind();
            dpSehir.Items.Insert(0, new ListItem("Seçiniz", "0"));

            dpDogumGun.Items.Add(new ListItem("Gün", "0"));
            for (int i = 1; i <= 31; i++)
            {
                dpDogumGun.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            dpDogumAy.Items.Add(new ListItem("Ay", "0"));
            for (int i = 1; i <= 12; i++)
            {
                dpDogumAy.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            dpDogumYil.Items.Add(new ListItem("Yıl", "0"));
            for (int i = 1900; i <= DateTime.Now.Year; i++)
            {
                dpDogumYil.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            dpSehir.DataSource = bllUyelikIslemleri.SehirListesi("TÜRKİYE");
            dpSehir.DataValueField = "SehirAd";
            dpSehir.DataTextField = "SehirAd";
            dpSehir.DataBind();
            dpSehir.Items.Insert(0, new ListItem("Seçiniz", "0"));



            dpIlce.Items.Insert(0, new ListItem("Seçiniz", "0"));

            KullaniciBilgileriniDoldur();
        }


    }

    private void KullaniciBilgileriniDoldur()
    {
        DataTable dtKullanici = new DataTable();
        dtKullanici = bllUyelikIslemleri.GetirKullanici(kullanici.KullaniciId);

        if (dtKullanici.Rows.Count > 0)
        {

            dpIlce.DataSource = UyelikIslemleriBLL.IlceListesi(dtKullanici.Rows[0]["SehirKod"].ToString());
            dpIlce.DataValueField = "IlceAd";
            dpIlce.DataTextField = "IlceAd";
            dpIlce.DataBind();
            dpIlce.Items.Insert(0, new ListItem("Seçiniz", "0"));



            txtAd.Text = dtKullanici.Rows[0]["KullaniciAd"].ToString();
            //txtSoyad.Text = dtKullanici.Rows[0]["KullaniciSoyad"].ToString();
            txtEmail.Text = dtKullanici.Rows[0]["Email"].ToString();
            txtEmailTekrar.Text = dtKullanici.Rows[0]["Email"].ToString();

            DateTime DogumTarihi = Convert.ToDateTime(dtKullanici.Rows[0]["DogumTarihi"].ToString());

            if (DogumTarihi.Year > 1900)
            {
                if (dpDogumGun.SelectedIndex >= 0)
                    dpDogumGun.Items[dpDogumGun.SelectedIndex].Selected = false;
                dpDogumGun.Items.FindByValue(DogumTarihi.Day.ToString()).Selected = true;

                if (dpDogumAy.SelectedIndex >= 0)
                    dpDogumAy.Items[dpDogumAy.SelectedIndex].Selected = false;
                dpDogumAy.Items.FindByValue(DogumTarihi.Month.ToString()).Selected = true;

                if (dpDogumYil.SelectedIndex >= 0)
                    dpDogumYil.Items[dpDogumYil.SelectedIndex].Selected = false;
                dpDogumYil.Items.FindByValue(DogumTarihi.Year.ToString()).Selected = true;
            }

            if (dpSehir.SelectedIndex >= 0)
                dpSehir.Items[dpSehir.SelectedIndex].Selected = false;
            if (!string.IsNullOrEmpty(dtKullanici.Rows[0]["SehirKod"].ToString()) && dtKullanici.Rows[0]["SehirKod"].ToString() != "0")
                dpSehir.Items.FindByValue(dtKullanici.Rows[0]["SehirKod"].ToString()).Selected = true;

            if (dpIlce.SelectedIndex >= 0)
                dpIlce.Items[dpIlce.SelectedIndex].Selected = false;
            if (!string.IsNullOrEmpty(dtKullanici.Rows[0]["IlceKod"].ToString()) && dtKullanici.Rows[0]["IlceKod"].ToString() != "0")
                dpIlce.Items.FindByValue(dtKullanici.Rows[0]["IlceKod"].ToString()).Selected = true;

            txtCepTelKod.Text = dtKullanici.Rows[0]["CepTelKod"].ToString();
            txtCepTelNo.Text = dtKullanici.Rows[0]["CepTelNo"].ToString();

            txtTelKod.Text = dtKullanici.Rows[0]["TelKod"].ToString();
            txtTelNo.Text = dtKullanici.Rows[0]["TelNo"].ToString();

            //chkEposta.Checked = Convert.ToBoolean(dtKullanici.Rows[0]["EmailHaber"]);
            //chkSms.Checked = Convert.ToBoolean(dtKullanici.Rows[0]["CepHaber"]);

            if (dtKullanici.Rows[0]["Cinsiyet"].ToString() == "E")
                rbErkek.Checked = true;
            else if (dtKullanici.Rows[0]["Cinsiyet"].ToString() == "K")
                rbKadin.Checked = true;
            else
            {
                rbErkek.Checked = false;
                rbKadin.Checked = false;
            }


            if (string.IsNullOrEmpty(dtKullanici.Rows[0]["RefEmail"].ToString()))
            {
                //DivTavsiyeEden.Visible = true;
                ////lblTavsiyeEdenEmail.Visible = true;
                //txtRefEmail.Visible = true;
            }
            else
            {
                ////lblTavsiyeEdenEmail.Visible = false;
                //DivTavsiyeEden.Visible = false;
                //txtRefEmail.Visible = false;
            }


        }
    }

    [System.Web.Services.WebMethod]
    public static List<Ilce> getIlceListesi(string SehirKod)
    {
        DataTable dtIlce = new DataTable();
        dtIlce = UyelikIslemleriBLL.IlceListesi(SehirKod);
        List<Ilce> ilceler = new List<Ilce>();
        Ilce ilce;
        foreach (DataRow dr in dtIlce.Rows)
        {
            ilce = new Ilce();
            ilce.IlceAd = dr["IlceAd"].ToString();
            ilce.SehirKod = dr["SehirKod"].ToString();
            ilceler.Add(ilce);
        }

        return ilceler;
    }

    protected void btnGuncelle_Click(object sender, EventArgs e)
    {
        string Sonuc = "";
        Kullanici kullanici;
        DateTime dogumTarihi;
        string Cinsiyet;

        //  DateTime.TryParse(dpDogumGun.SelectedValue + "." + dpDogumAy.SelectedValue + "." + dpDogumYil.SelectedValue, out dogumTarihi);

        if (dpDogumYil.SelectedValue != "0" && dpDogumAy.SelectedValue != "0" && dpDogumGun.SelectedValue != "0")
            dogumTarihi = new DateTime(Convert.ToInt32(dpDogumYil.SelectedValue), Convert.ToInt32(dpDogumAy.SelectedValue), Convert.ToInt32(dpDogumGun.SelectedValue));
        else
            dogumTarihi = new DateTime(1900, 1, 1);

        if (Page.IsValid)
        {
            if (Session["Kullanici"] != null)
            {
                kullanici = ((Kullanici)Session["Kullanici"]);

                if (rbErkek.Checked)
                    Cinsiyet = "E";
                else if (rbKadin.Checked)
                    Cinsiyet = "K";
                else
                    Cinsiyet = "T";


                string RefEmail = "";
                //if (txtRefEmail.Visible == true)
                //    RefEmail = txtRefEmail.Text.Trim();

                Sonuc = bllUyelikIslemleri.GuncelleKullanici(kullanici.KullaniciId, dogumTarihi, Cinsiyet, "TR", dpSehir.SelectedValue, Request.Form[dpIlce.UniqueID], txtTelKod.Text.Trim(), txtTelNo.Text.Trim(), txtCepTelKod.Text.Trim(), txtCepTelNo.Text.Trim(), true, true, RefEmail);

                KullaniciBilgileriniDoldur();

                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur(Sonuc));
            }
        }
    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">Popup('" + mesaj + "');</script>";
    }
}