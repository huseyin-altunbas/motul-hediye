﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Hesabim/MpHesabim.master" AutoEventWireup="true" CodeFile="Puan-Hareketlerim.aspx.cs" Inherits="Hesabim_Puan_Hareketlerim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" Runat="Server">
     <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Üyelik Bilgilerim</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" Runat="Server">
     <span class="page-heading-title2">HESAP HAREKETLERİM  </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" Runat="Server">
     <script>
          $.ajaxSetup({
              beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", "<%=kullanici.ServiceKey%>");
                }
            });

    </script>
    <script>
        function RunKendoTrancaction() {
            var remoteDataSource = new kendo.data.DataSource({
                pageSize: 20,
                transport: {
                    read: {
                        url: "/api/UserTransaction/Get?Token=<%=kullanici.ServiceKey%>",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json",
                        complete: function (data) {

                        }

                    },
                    create: {
                        url: "/api/Transaction/Post",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json",
                    },
                    update: {
                        url: "#",
                        dataType: "json",
                        type: "PUT",
                    },
                    destroy: {
                        url: "#",
                        dataType: "json",
                        type: "DELETE"
                    },
                    parameterMap: function (options, operation) {

                        operation = operation;

                        if (operation === "read") {
                            //Export settings start
                            var filter = encodeURIComponent(JSON.stringify(options));
                            $(".k-grid-customExcelExport").attr("href", '/api/Transaction/Export/?filter=' + filter)
                            $('.k-grid-customExcelExport').attr('target', '_blank');
                            //Export settings stop


                        }

                        if (operation !== "read" && options.model) {
                            return JSON.stringify(options.models[0]);
                        }
                        else {

                            return JSON.stringify(options);
                        }


                    }
                },
                 //filter: [{ "field": "KullaniciId", "operator": "eq", "value": <%=Request.QueryString["KullaniciId"]%> }, ],
                schema: {
                    data: "Data",
                    total: "Total",
                    model: {
                        id: "id",
                        fields: {
                            id: { editable: false, hidden: true },
                            created: { type: "date", editable: false, nullable: true },
                            period: { type: "date", editable: true, nullable: true },

                        }
                    }
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                error: function (e) {
                    //console.log(e);
                    swal("", "Lütfen tüm alanları kontrol ediniz.", "info");
                }
            });

            $('#gridTrancaction').kendoGrid({
                dataSource: remoteDataSource,
                
               

                scrollable: true,
                sortable: true,
                edit: onEdit,
                filterMenuInit: filterMenu,
                filterable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [

                    { field: "id", title: "Id", width: "60px" },
                    //{ field: "firstname", title: "Ad", editable: false, filterable: true, width: "100px" },
                    //{ field: "lastname", title: "Soyad", editable: false, filterable: true, width: "100px" },
                    //{ field: "email", title: "Email", editable: false, filterable: true, width: "200px" },
                    { field: "description", title: "Açıklama", editable: false, filterable: true, width: "200px" },
                    //{ field: "stockName", title: "Ürün Adı", editable: false, filterable: true, width: "180px" },
                    { field: "amount", title: "Puan", editable: false, filterable: true, width: "90px" },
                    //{ field: "archiveNo", title: "Arşiv No", editable: false, filterable: true, width: "110px" },
                    //{ field: "campaignName", title: "Kampanya Adı", editable: false, filterable: true, width: "150px" },
                    //{ field: "transactionTypeText", title: "Tip", editable: false, filterable: { ui: transactionTypeFilter }, sortable: false },
                    //{ field: "period", title: "Hakedis Tarihi", format: "{0:dd/MM/yyyy}", editable: true, filterable: true, width: "150px" },
                    { field: "created", title: "Oluşturma Tarihi", format: "{0:dd/MM/yyyy}", editable: false, filterable: true, width: "150px" },
                    //{ field: "statusText", title: "Durum", editable: false, sortable: false, filterable: { ui: statusFilter }, width: "130px" },


                    //{
                    //    command: [
                    //        { text: "Onayla", click: TransactionConfirm }

                    //    ],
                    //    title: "İşlemler",
                    //    width: "180px",
                    //    //locked: true,
                    //    //lockable: false,
                    //},

                ]
            });


 


            function onEdit(e) {
                  e.container
                .find("[name=KullaniciId]")
                .val(<%=Request.QueryString["KullaniciId"]%>)
                    .trigger("change");

                 $("#period").kendoDatePicker({

                    // display month and year in the input
                    format: "dd/MM/yyyy",
                });
            }

            function statusFilter(element) {
                var form = element.closest("form");
                // changes the help text. (you might want to localise this)
                form.find(".k-filter-help-text:first").text("Durum seçiniz:");
                // removes the dropdown list containing the operators (contains etc)
                form.find("select").remove();
                form.find(".k-dropdown-wrap").remove();
                element.kendoDropDownList({
                    dataSource: ['Aktif', 'Pasif'],
                    optionLabel: "Durum Seçiniz",
                });
            }

            function transactionTypeFilter(element) {
                var form = element.closest("form");
                // changes the help text. (you might want to localise this)
                form.find(".k-filter-help-text:first").text("Durum seçiniz:");
                // removes the dropdown list containing the operators (contains etc)
                form.find("select").remove();
                form.find(".k-dropdown-wrap").remove();
                element.kendoDropDownList({
                    dataSource: ['Faturadan kazanılan puan.', 'Fatura iptaline istinaden silinen puan.', 'Eğitimden kazanılan puan.', 'Manuel yüklenen puan.', 'Manuel silinen puan.'],
                    optionLabel: "Tip Seçiniz",
                });

            }

            function filterMenu(e) {
                if (e.field == "created") {
                    var beginOperator = e.container.find("[data-role=dropdownlist]:eq(0)").data("kendoDropDownList");
                    beginOperator.value("gte");
                    beginOperator.trigger("change");

                    var endOperator = e.container.find("[data-role=dropdownlist]:eq(2)").data("kendoDropDownList");
                    endOperator.value("lte");
                    endOperator.trigger("change");

                    e.container.find(".k-dropdown").hide();
                }
            }

        }



        $(document).ready(function () {
            RunKendoTrancaction();

        });

    </script>


    <div class="page-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box-form">
                   <div id="gridTrancaction"></div>
                </div>
            </div>
          
        </div>
    </div>

      
        <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2018.2.620/js/cultures/kendo.culture.tr-TR.min.js"></script>
    <script src="/Admin/js/kendo.tr-TR.js"></script>
</asp:Content>



