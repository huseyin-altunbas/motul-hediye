﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Hesabim/MpHesabim.master" AutoEventWireup="true"
    CodeFile="SifremiDegistir.aspx.cs" Inherits="Hesabim_SifremiDegistir" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Üyelik Bilgilerim</span>
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Şifremi Değiştir</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
    <span class="page-heading-title2">ŞİFREMİ DEĞİŞTİR</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <script src="/assets/js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/assets/lib/fancyBox/jquery.fancybox.css" />
    <script type="text/javascript" src="/assets/lib/fancyBox/jquery.fancybox.js"></script>
    <!-- popup -->
    <script type="text/javascript">
        function Popup(message) {
            if ($('#popup').length > 0 && $('#popupContent').length > 0) {
                $("#popupContent").html(message).promise().done(function () {
                    $('#popup').fancybox().trigger('click');
                });
            }
        }
    </script>
    <a id="popup" class="fancybox" href="#popupContent" style="display: none"></a>
    <div id="popupContent" style="display: none">
    </div>
    <!-- popup -->
    <div class="page-content">
        <div class="row">
            <div class="col-sm-6">
                <div class="box-form">
                    <% if (!kullanici.IlkSifreDegistimi)
                        { %>
                    <p id="msg" runat="server" style="color: red;">Tarafınıza paylaşılan geçici şifrenizi değiştirmeniz zorunludur, şifrenizi değiştirerek Motul Hediye'yi kullanmaya devam edebilirsiniz. </p>
                    <%} %>
                    <div class="mt10">
                        <label for="">
                            Mevcut Şifreniz</label>
                        <asp:TextBox ID="txtMevcutSifre" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqMevcutSifre" runat="server" ErrorMessage="Mevcut şifrenizi girin."
                                ControlToValidate="txtMevcutSifre" ValidationGroup="grpSifremiDegistir" CssClass="w230"></asp:RequiredFieldValidator>
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="">
                            Yeni Şifreniz</label>
                        <asp:TextBox ID="txtYeniSifre" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqYeniSifre" runat="server" ErrorMessage="Yeni şifrenizi girin."
                                ControlToValidate="txtYeniSifre" ValidationGroup="grpSifremiDegistir" CssClass="w230"></asp:RequiredFieldValidator>
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="">
                            Yeni Şifreniz(Tekrar)</label>
                        <asp:TextBox ID="txtYeniSifreTekrar" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqYeniSifreTekrar" runat="server" ErrorMessage="Yeni şifrenizi tekrar girin."
                                ControlToValidate="txtYeniSifreTekrar" ValidationGroup="grpSifremiDegistir" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cmptxtYeniSifreTekrar" runat="server" ErrorMessage="Şifrenizi tekrar girin."
                                ControlToValidate="txtYeniSifreTekrar" ControlToCompare="txtYeniSifre" Display="Dynamic"
                                ValidationGroup="grpSifremiDegistir"></asp:CompareValidator>
                        </label>
                    </div>
                    <div class="mt10">
                        <asp:LinkButton ID="btnSifremiDegistir" CssClass="btn btnInput" ValidationGroup="grpSifremiDegistir"
                            runat="server" OnClick="btnSifremiDegistir_Click"><span class="fa fa-lock">
                        </span>&nbsp;Şifremi Değiştir</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box-form">
                    <div style="text-align: center;">
                        <h3 style="margin-bottom: 50px;">
                            <span class="colorF36">Motul Hediye </span>'ye üye olduysanız ve şifrenizi değiştirmek
                            istiyorsanız;<br />
                            <br />
                            kayıtlı olan şifrenizi ve yeni şifrenizi ilgili alanlara yazarak <span class="colorF36">"Şifremi Değiştir"</span> butonuna basın.</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div id="myModal" class="modal fade" role="dialog" data-toggle="modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <%--    <button type="button" class="close" data-dismiss="modal" onclick="Kapat()">&times;</button>--%>
                    <span>Motül İnternet Sitesi Aydınlatma Metni </span>
                </div>



                <div class="modal-body" style="/* height: 400px; */">


                    <p>
                        MOTUL TR İLERİ TEKNOLOJİ OTOMOTİV VE END&Uuml;STRİ TİCARET ANONİM<br />
                        ŞİRKETİ GİZLİLİK POLİTİKASI<br />
                        Son G&uuml;ncelleme Tarihi: 1.07.2020<br />
                        KİŞİSEL VERİLERİNİZİN KORUNMASINA İLİŞKİN BİLGİLENDİRME<br />
                        MOTUL TR İLERİ TEKNOLOJİ OTOMOTİV VE END&Uuml;STRİ TİCARET A.Ş. (&ldquo;MOTUL&rdquo;<br />
                        veya &ldquo;Şirket&rdquo;) tarafından toplanan ve işlenen kişisel veriler, Şirket&rsquo;in koruması altındadır.<br />
                        Kişisel veriler, MOTUL TR İLERİ TEKNOLOJİ OTOMOTİV VE END&Uuml;STRİ TİCARET<br />
                        A.Ş. tarafından işbu Gizlilik Politikası (&ldquo;Politika&rdquo;) kapsamında 6698 sayılı Kişisel Verilerin<br />
                        Korunması Kanunu (&ldquo;KVKK&rdquo;) ve ikincil d&uuml;zenlemeleri ve kişisel verilerin korunmasına ilişkin<br />
                        sair mevzuatı uygun olarak işlenmektedir.<br />
                        KVKK uyarınca kişisel veriler, kimliğinizi belirli ya da belirlenebilir kılan her t&uuml;rl&uuml; bilgi<br />
                        anlamına gelmektedir. Şirket tarafından işlenen kişisel verileriniz, bunların işlenme ama&ccedil;ları,<br />
                        aktarılabileceği alıcı grupları, toplanma y&ouml;ntemi, hukuki sebebi ve s&ouml;z konusu kişisel verilere<br />
                        ilişkin haklarınız aşağıda yer almaktadır.<br />
                        A- WEB SİTESİ KULLANICILARI (&Ccedil;EVRİMİ&Ccedil;İ ZİYARET&Ccedil;İ)<br />
                        A.I. İŞLENEN KİŞİSEL VERİLERİNİZ NELERDİR?<br />
                        İnternet sitemizi ziyaret etmeniz halinde aşağıdaki kişisel verileriniz işlenebilecektir;<br />
                        ● Kimlik Bilgileri: Ad, soyad,<br />
                        ● İletişim Bilgileri: E-posta adresi,<br />
                        ● İşlem G&uuml;venliği Bilgileriniz: IP adresi bilgileri, &ccedil;erez kayıtları, internet sitesi giriş &ccedil;ıkış<br />
                        bilgileri.<br />
                        A.II. KİŞİSEL VERİLERİNİZİN İŞLENME AMA&Ccedil;LARI<br />
                        Kişisel verileriniz, ilgili mevzuata uygun olarak aşağıdaki ama&ccedil;lar ile işlenebilecektir;<br />
                        &bull; Kullanıcılar tarafından y&ouml;neltilen abonelik işlemlerinin ger&ccedil;ekleştirilmesi,<br />
                        &bull; İletişim faaliyetlerinin y&uuml;r&uuml;t&uuml;lmesi ve iletişim formunun doldurulması,<br />
                        &bull; 5651 Sayılı Kanun&rsquo;dan doğan y&uuml;k&uuml;ml&uuml;l&uuml;klerin yerine getirilmesi,<br />
                        &bull; İlgili mevzuat gereği saklanması gereken bilgilerinizin muhafazası; bilgi kayıplarının<br />
                        &ouml;nlenebilmesi i&ccedil;in kopyalanması, yedeklenmesi; bilgilerinizin tutarlılığının<br />
                        kontrol&uuml;n&uuml;n sağlanması, şebekelerimizin ve bilgilerinizin g&uuml;venliği i&ccedil;in gerekli teknik<br />
                        ve idari tedbirlerin alınması,<br />
                        &bull; D&uuml;zenleyici ve denetleyici kurumlarla, yasal d&uuml;zenlemelerin gerektirdiği veya zorunlu<br />
                        kıldığı hukuki y&uuml;k&uuml;ml&uuml;l&uuml;klerin yerine getirilmesi, yasal takiplerin ve hukuki s&uuml;re&ccedil;lerin<br />
                        y&uuml;r&uuml;t&uuml;lmesi.<br />
                        ● Digital ortamda reklam ve tanıtım amacıyla pazarlama faaliyetlerinin yerine<br />
                        getirilmesi.<br />
                        A.III. KİŞİSEL VERİLERİNİZİN AKTARIMI<br />
                        Kişisel verileriniz, KVKK&rsquo;nın kişisel verilerin aktarılmasına ilişkin h&uuml;k&uuml;mleri kapsamında işbu<br />
                        Politika&rsquo;nın A.II. başlığı altında yer alan ama&ccedil;larla; ilgili mevzuat gereği talep edilmesi halinde<br />
                        yurt i&ccedil;indeki adli makamlar veya kolluk kuvvetleri ile ve reklam, tanıtım gibi pazarlama<br />
                        iletişiminde kullanılmak &uuml;zere yurt i&ccedil;indeki iş ortakları ile paylaşılabilecektir.<br />
                        A.IV. KİŞİSEL VERİLERİN İŞLENMESİNİN HUKUKİ SEBEBİ VE TOPLANMA<br />
                        Y&Ouml;NTEMİ<br />
                        Kişisel verileriniz internet sitemizi ziyaret etmeniz sırasında elektronik mecralardan yukarıda<br />
                        yer verilen ama&ccedil; dahilinde Kanun&rsquo;un 5 ve 8. madde h&uuml;k&uuml;mlerinde sayılan ve aşağıda yer verilen<br />
                        hukuka uygunluk sebeplerine dayanılarak otomatik veya otomatik olmayan yollarla<br />
                        işlenmektedir.<br />
                        ● Şirket&rsquo;in tabi olduğu mevzuatta &ouml;ng&ouml;r&uuml;lm&uuml;ş olması,<br />
                        ● Şirket&rsquo;in hukuki y&uuml;k&uuml;ml&uuml;l&uuml;klerini yerine getirebilmesi i&ccedil;in veri işlemenin zorunlu<br />
                        olması,<br />
                        ● Tarafınızca alenileştirilmiş olması,<br />
                        ● Bir hakkın tesisi, kullanılması veya korunması i&ccedil;in veri işlemenin zorunlu olması,<br />
                        ● Temel hak ve &ouml;zg&uuml;rl&uuml;klerinize zarar vermemek kaydıyla, Şirket&rsquo;in meşru menfaatleri<br />
                        i&ccedil;in veri işlemenin zorunlu olması.<br />
                        C- TEDARİK&Ccedil;İ<br />
                        C.I. İŞLENEN KİŞİSEL VERİLERİNİZ NELERDİR?<br />
                        Şirket ile ilişkiniz kapsamında aşağıdaki kişisel verileriniz işlenmektedir;<br />
                        &bull; Kimlik Bilgileriniz: Ad soyad, TCKN/VKN, imza,<br />
                        &bull; İletişim Bilgileriniz: Telefon numarası, adres bilgileri, e-posta adresi, kayıtlı elektronik<br />
                        posta adresi (KEP),<br />
                        &bull; Finans Bilgileriniz: Fatura/senet/&ccedil;ek bilgileri, bor&ccedil; bilgisi, banka/hesap kart bilgileri,<br />
                        &bull; Mesleki Deneyim Bilgileriniz: Meslek bilgisi,<br />
                        &bull; G&ouml;rsel ve İşitsel Kayıtlara İlişkin Bilgileriniz: Fotoğraf,<br />
                        &bull; Araca İlişkin Bilgileriniz: Ara&ccedil; plaka bilgileri,<br />
                        &bull; Fiziksel Bilgileriniz: Beden &ouml;l&ccedil;&uuml;s&uuml;<br />
                        C.II. KİŞİSEL VERİLERİNİZİN İŞLENME AMA&Ccedil;LARI<br />
                        Kişisel verileriniz, ilgili mevzuata uygun olarak aşağıdaki ama&ccedil;lar ile işlenebilecektir:<br />
                        &bull; S&ouml;zleşme s&uuml;re&ccedil;lerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Mal ve hizmet satın alım ve satış s&uuml;re&ccedil;lerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Muhasebeye ilişkin s&uuml;re&ccedil;lerin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Lojistik faaliyetinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Bilgi teknolojileri s&uuml;re&ccedil;lerinin y&uuml;r&uuml;t&uuml;lmesi.<br />
                        &bull; İletişim faaliyetlerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Mevzuattan doğan yasal y&uuml;k&uuml;ml&uuml;l&uuml;klerin yerine getirilmesi,<br />
                        &bull; Saklama ve arşiv faaliyetlerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Hukuk, dava ve idari işlerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Tedarik zinciri y&ouml;netimi s&uuml;re&ccedil;lerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; İş s&uuml;rekliliğinin sağlanması faaliyetlerinin y&uuml;r&uuml;t&uuml;lmesi.<br />
                        C.III. KİŞİSEL VERİLERİNİZİN AKTARIMI<br />
                        Kişisel verileriniz, KVKK&rsquo;nın kişisel verilerin yurti&ccedil;i ve yurtdışına aktarılmasına ilişkin<br />
                        h&uuml;k&uuml;mleri kapsamında işbu İşbu Politika&rsquo;nın C.II başlığı altında yer alan ama&ccedil;larla; yurt<br />
                        i&ccedil;indeki resmi kurum ve kuruluşlara, tedarik&ccedil;ilerimize, iş ortaklarımıza, bayilerimize ve<br />
                        bankalara; yurtdışındaki tedarik&ccedil;ilerimize aktarılabilmektedir.<br />
                        C. IV. KİŞİSEL VERİLERİN İŞLENMESİNİN HUKUKİ SEBEBİ VE TOPLANMA<br />
                        Y&Ouml;NTEMİ<br />
                        Şirket, kişisel verilerinizi tedarik hizmeti aldığımız ya da iş ortağı olunan firmadan,<br />
                        Şirketimizde g&ouml;rev almanız sırasında doğrudan sizlerden, &uuml;&ccedil;&uuml;nc&uuml; kişilerden ve yasal<br />
                        mercilerden olmak kaydıyla internet, telefon, e-posta aracılığıyla ve fiziki, yazılı, s&ouml;zl&uuml; ve<br />
                        elektronik mecralardan yukarıda belirtilen ama&ccedil;lar dahilinde KVKK&rsquo;nın 5, 8. ve 9. madde<br />
                        h&uuml;k&uuml;mlerinde sayılan ve aşağıda yer verilen hukuka uygunluk sebeplerine dayanılarak otomatik<br />
                        ve otomatik olmayan yollarla işlenmektedir.<br />
                        ● Şirketimizin tabi olduğu mevzuatta a&ccedil;ık&ccedil;a &ouml;ng&ouml;r&uuml;lm&uuml;ş olması,<br />
                        ● Bir s&ouml;zleşmenin kurulması veya ifasıyla doğrudan doğruya ilgili olması kaydıyla,<br />
                        s&ouml;zleşmenin taraflarına ait kişisel verilerin işlenmesinin gerekli olması,<br />
                        ● Hukuki y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml;n yerine getirebilmesi i&ccedil;in zorunlu olması,<br />
                        ● Bir hakkın tesisi, kullanılması veya korunması i&ccedil;in veri işlemenin zorunlu olması,<br />
                        ● İlgili kişinin temel hak ve &ouml;zg&uuml;rl&uuml;klerine zarar vermemek kaydıyla, veri sorumlusunun<br />
                        meşru menfaatleri i&ccedil;in veri işlenmesinin zorunlu olması.<br />
                        D- BAYİ VE İŞ ORTAĞI<br />
                        D.I. İŞLENEN KİŞİSEL VERİLERİNİZ NELERDİR?<br />
                        Şirket ile ilişkiniz kapsamında aşağıdaki kişisel verileriniz işlenmektedir;<br />
                        ● Kimlik Bilgileriniz: Ad soyad, anne baba adı, TCKN/VKN, pasaport numarası, uyruk,<br />
                        medeni hal, doğum tarihi, doğum yeri, imza,<br />
                        ● İletişim Bilgileriniz: Telefon numarası, adres bilgileri, e-posta adresi, sosyal medya<br />
                        hesapları, kayıtlı elektronik posta adresi (KEP),
                            <br />
                        ● Lokasyon Bilgisiniz: Konum bilgisi,<br />
                        ● Finans Bilgileriniz: Fatura, senet, &ccedil;ek ve bor&ccedil; bilgileri, banka hesabı ve kart bilgileri,<br />
                        finansal performans bilgileri, kredi ve risk bilgileri, malvarlığı bilgileri,<br />
                        ● G&ouml;rsel ve İşitsel Kayıtlara İlişkin Bilgileriniz: Fotoğraf, video, ses kayıtları,<br />
                        ● İş, Profesyonel Yaşam ve Eğitim Bilgileriniz: &Ouml;zge&ccedil;miş bilgileri, sicil numarası,<br />
                        &ccedil;alışma/performans bilgileri, profesyonel/kişisel yetkinlikler,<br />
                        ● Fiziksel &Ouml;zellik Bilgileriniz: Beden &ouml;l&ccedil;&uuml;s&uuml;,<br />
                        ● Araca İlişkin Bilgileriniz: Ara&ccedil; plaka bilgileri,<br />
                        ● Diğer Bilgileriniz: Şirket adı ve g&ouml;rev tanımı,<br />
                        ● &Ouml;zel Nitelikli Verileriniz: Kişisel sağlık bilgisi.<br />
                        D.II. KİŞİSEL VERİLERİNİZİN İŞLENME AMA&Ccedil;LARI<br />
                        Kişisel verileriniz, ilgili mevzuata uygun olarak aşağıdaki ama&ccedil;lar ile işlenebilecektir:<br />
                        &bull; Noter ve Resmi Kurum işlemlerinin yapılması,<br />
                        &bull; Seyahat organizasyonlarının y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; S&ouml;zleşme s&uuml;re&ccedil;lerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Satış s&uuml;recine ilişkin faaliyetlerin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Bayilerin &ouml;zel etkinlikler kapsamında ziyaret edilmesi,<br />
                        &bull; Eğitim ve toplantı faaliyetinin y&uuml;r&uuml;t&uuml;lmesi.<br />
                        &bull;<br />
                        &bull; Dijital ortam da dahil olmak &uuml;zere her t&uuml;rl&uuml; mecrada reklam ve tanıtım amacıyla<br />
                        pazarlama faaliyetlerinin yerine getirilmesi.<br />
                        &bull;<br />
                        D. III. İŞLENEN KİŞİSEL VERİLERİN AKTARIMI<br />
                        Kişisel verileriniz, KVKK&rsquo;nın kişisel verilerin yurti&ccedil;i ve yurtdışına aktarılmasına ilişkin<br />
                        h&uuml;k&uuml;mleri kapsamında işbu Politika&rsquo;nın D.II başlığı altında yer alan ama&ccedil;larla yurt i&ccedil;indeki;<br />
                        resmi kurum ve kuruluşlara, tedarik&ccedil;ilere, topluluk şirketlerine, yurtdışındaki; iştirak ve bağlı<br />
                        ortaklıklara ve tedarik&ccedil;ilere aktarılabilmektedir.<br />
                        D. IV. KİŞİSEL VERİLERİN İŞLENMESİNİN HUKUKİ SEBEBİ VE TOPLANMA<br />
                        Y&Ouml;NTEMİ<br />
                        Kişisel verileriniz, Şirketimizle hukuki ilişkinizin kurulması esnasında ve s&ouml;z konusu ilişkinin<br />
                        devamı s&uuml;resince sizlerden, &uuml;&ccedil;&uuml;nc&uuml; kişilerden ve yasal mercilerden olmak kaydıyla internet<br />
                        sitesi, muhtelif s&ouml;zleşmeler, mobil uygulamalar, elektronik posta, başvuru formları gibi ara&ccedil;lar<br />
                        &uuml;zerinden, Şirketimiz ile yapılan yazılı veya s&ouml;zl&uuml; iletişim kanalları aracılığıyla s&ouml;zl&uuml;, yazılı<br />
                        veya elektronik ortamda toplanmaktadır. Bu doğrultuda toplanan kişisel verileriniz KVKK&rsquo;nın<br />
                        5, 6, 8 ve 9. maddelerinde belirtilen ve aşağıda yer verilen hukuka uygunluk sebeplerine<br />
                        dayanılarak işlenmektedir.<br />
                        &bull; A&ccedil;ık rızanızın bulunması,
                            <br />
                        &bull; T&uuml;rk Bor&ccedil;lar Kanunu, T&uuml;rk Ticaret Kanunu olmak &uuml;zere Şirketimizin tabi<br />
                        olduğu mevzuatta a&ccedil;ık&ccedil;a &ouml;ng&ouml;r&uuml;lm&uuml;ş olması,<br />
                        &bull; Bir s&ouml;zleşmenin kurulması veya ifasıyla doğrudan doğruya ilgili olması<br />
                        kaydıyla, s&ouml;zleşmenin taraflarına ait kişisel verilerin işlenmesinin gerekli<br />
                        olması,<br />
                        &bull; Hukuki y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml;n yerine getirebilmesi i&ccedil;in zorunlu olması,<br />
                        &bull; Bir hakkın tesisi, kullanılması veya korunması i&ccedil;in veri işlemenin zorunlu<br />
                        olması,<br />
                        &bull; İlgili kişinin temel hak ve &ouml;zg&uuml;rl&uuml;klerine zarar vermemek kaydıyla, veri<br />
                        sorumlusunun meşru menfaatleri i&ccedil;in veri işlenmesinin zorunlu olması,<br />
                        &Ouml;zel nitelikli kişisel verileriniz ise a&ccedil;ık rızanın bulunması hukuka uygunluk sebeplerine<br />
                        dayanılarak toplanmakta, saklanmakta ve işlenmektedir.<br />
                        E-TEDARİK&Ccedil;İ &Ccedil;ALIŞANI<br />
                        E.I. İŞLENEN KİŞİSEL VERİLERİNİZ NELERDİR?<br />
                        Şirket ile ilişkiniz kapsamında aşağıdaki kişisel verileriniz işlenmektedir;<br />
                        ● Kimlik Bilgileriniz: Ad soyad, TCKN, imza, anne-baba adı, anne kızlık soyadı,<br />
                        pasaport numarası, uyruk, medeni hal, ehliyet, doğum tarihi/yeri,<br />
                        ● İletişim Bilgileriniz: Adres bilgileri, telefon numarası, e-posta adresi,<br />
                        ● &Ouml;zl&uuml;k Bilgileriniz: &Uuml;cret bilgisi, &ouml;zge&ccedil;miş bilgileri, , i, işe giriş-&ccedil;ıkış belgesi kayıtları,<br />
                        , profesyonel/kişisel yetkinlikler, , yan hak ve menfaat bilgileri, eğitim seviyesi,<br />
                        diploma bilgileri, gidilen kurs bilgileri, meslek i&ccedil;in eğitim bilgileri, sertifikalar, , meslek<br />
                        bilgisi, gelir seviyesi bilgileri,G&ouml;rsel ve İşitsel Kayıt Bilgileriniz: Fotoğraf, video ve<br />
                        ses kayıtları,<br />
                        ● Fiziksel Bilgileriniz: Beden &ouml;l&ccedil;&uuml;s&uuml;,<br />
                        Ara&ccedil; Bilgileriniz: Ara&ccedil; plaka bilgisi, ara&ccedil; model bilgisi, ara&ccedil; km bilgisi,<br />
                        E.II. KİŞİSEL VERİLERİNİZİN İŞLENME AMA&Ccedil;LARI<br />
                        Kişisel verileriniz, ilgili mevzuata uygun olarak aşağıdaki ama&ccedil;lar ile işlenebilecektir:<br />
                        &bull; S&ouml;zleşme s&uuml;re&ccedil;lerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; İş faaliyetlerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; G&uuml;mr&uuml;k ve nakliye s&uuml;re&ccedil;lerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Muhasebe işlemlerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Resmi Kurum ve Kuruluşlara ilişkin s&uuml;re&ccedil;lerin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Eğitim ve toplantı faaliyetinin y&uuml;r&uuml;t&uuml;lmesi.<br />
                        &bull; Bilgi Teknolojileri s&uuml;re&ccedil;lerinin y&uuml;r&uuml;t&uuml;lmesi.<br />
                        E.III. KİŞİSEL VERİLERİNİZİN AKTARIMI<br />
                        Kişisel verileriniz, KVKK&rsquo;nın kişisel verilerin yurti&ccedil;i ve yurtdışına aktarılmasına ilişkin<br />
                        h&uuml;k&uuml;mleri kapsamında işbu İşbu Politika&rsquo;nın E.II başlığı altında yer alan ama&ccedil;larla yurt
                            <br />
                        i&ccedil;indeki; resmi kurum ve kuruluşlara, iş ortaklarına, tedarik&ccedil;ilere, yurtdışındaki; iş ortaklarına<br />
                        ve tedarik&ccedil;ilere aktarılabilmektedir.<br />
                        E. IV. KİŞİSEL VERİLERİN İŞLENMESİNİN HUKUKİ SEBEBİ VE TOPLANMA<br />
                        Y&Ouml;NTEMİ<br />
                        Şirket, kişisel verilerinizi tedarik hizmeti aldığımız ya da iş ortağı olunan firmadan,<br />
                        Şirketimizde g&ouml;rev almanız sırasında doğrudan sizlerden, &uuml;&ccedil;&uuml;nc&uuml; kişilerden ve yasal<br />
                        mercilerden olmak kaydıyla internet, telefon, e-posta aracılığıyla ve fiziki, yazılı, s&ouml;zl&uuml; ve<br />
                        elektronik mecralardan yukarıda belirtilen ama&ccedil;lar dahilinde KVKK&rsquo;nın 5, 8. ve 9. madde<br />
                        h&uuml;k&uuml;mlerinde sayılan ve aşağıda yer verilen hukuka uygunluk sebeplerine dayanılarak otomatik<br />
                        ve otomatik olmayan yollarla işlenmektedir.<br />
                        ● A&ccedil;ık rızanın bulunması,<br />
                        ● Şirketimizin tabi olduğu mevzuatta a&ccedil;ık&ccedil;a &ouml;ng&ouml;r&uuml;lm&uuml;ş olması,<br />
                        ● Bir s&ouml;zleşmenin kurulması veya ifasıyla doğrudan doğruya ilgili olması kaydıyla,<br />
                        s&ouml;zleşmenin taraflarına ait kişisel verilerin işlenmesinin gerekli olması,<br />
                        ● Hukuki y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml;n yerine getirebilmesi i&ccedil;in zorunlu olması,<br />
                        ● Bir hakkın tesisi, kullanılması veya korunması i&ccedil;in veri işlemenin zorunlu olması,<br />
                        ● İlgili kişinin temel hak ve &ouml;zg&uuml;rl&uuml;klerine zarar vermemek kaydıyla, veri sorumlusunun<br />
                        meşru menfaatleri i&ccedil;in veri işlenmesinin zorunlu olması,<br />
                        F- M&Uuml;ŞTERİ<br />
                        F.I. İŞLENEN KİŞİSEL VERİLERİNİZ NELERDİR?<br />
                        Şirket ile ilişkiniz kapsamında aşağıdaki kişisel verileriniz işlenmektedir;<br />
                        ● Kimlik Bilgileriniz: Ad soyad, anne baba adı, TCKN/VKN, pasaport numarası,<br />
                        medeni hal, doğum tarihi, doğum yeri, imza,<br />
                        ● İletişim Bilgileriniz: Telefon numarası, adres bilgileri, e-posta adresi, kayıtlı elektronik<br />
                        posta adresi (KEP), sosyal medya hesapları,<br />
                        ● Lokasyon Bilginiz: Konum bilgisi,<br />
                        ● Finans Bilgileriniz: Fatura/senet/&ccedil;ek/bor&ccedil; bilgisi, banka hesabı ve kart bilgileri,<br />
                        finansal performans bilgileri, malvarlığı bilgileri,<br />
                        ● M&uuml;şteri İşlem Bilgileriniz: Talep bilgisi, sipariş bilgisi,<br />
                        ● M&uuml;şteri Memnuniyeti ve Pazarlamaya İlişkin Bilgiler: M&uuml;şteri şikayet, talep ve<br />
                        teşekk&uuml;r bilgileri,<br />
                        ● Risk Y&ouml;netimi Bilgileriniz: Tahsilat riski bilgileri, kredi ve risk bilgileri,<br />
                        ● G&ouml;rsel ve İşitsel Kayıtlara İlişkin Bilgileriniz: Fotoğraf, video, ses kaydı,<br />
                        ● Fiziksel Bilgileriniz: Beden &ouml;l&ccedil;&uuml;s&uuml;,<br />
                        ● Diğer Bilgileriniz: Kampanya &ccedil;alışmasıyla elde edilen bilgiler,<br />
                        ● Ara&ccedil; Bilgileriniz: Ara&ccedil; plaka bilgisi, ara&ccedil; model bilgisi, ara&ccedil; km bilgisi,<br />
                        F.II. KİŞİSEL VERİLERİNİZİN İŞLENME AMA&Ccedil;LARI<br />
                        Kişisel verileriniz, ilgili mevzuata uygun olarak aşağıdaki ama&ccedil;lar ile işlenebilecektir:<br />
                        &bull; İş s&uuml;rekliliğinin sağlanması,<br />
                        &bull; M&uuml;şteri bilgilerinin Şirket sistemine tanımlanması,<br />
                        &bull; S&ouml;zleşme s&uuml;re&ccedil;lerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Muhasebe işlemlerine ilişkin s&uuml;re&ccedil;lerin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Resmi Kurum ve Kuruluşlara ilişkin s&uuml;re&ccedil;lerin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Seyahat organizasyonlarının ger&ccedil;ekleştirilmesi,<br />
                        &bull; M&uuml;şterilere y&ouml;nelik ziyaretlerin ger&ccedil;ekleştirilmesi,<br />
                        &bull; Risk analizi değerlendirmelerinin yapılması,<br />
                        &bull; M&uuml;şterilere y&ouml;nelik toplantı ve eğitim faaliyetlerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Yardım faaliyetlerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Dijital ortamlar da dahil olmak &uuml;zere her t&uuml;rl&uuml; mecrada reklam ve tanıtım amacıyla<br />
                        pazarlama faaliyetlerinin yerine getirilmesi.<br />
                        &bull;<br />
                        F. III. İŞLENEN KİŞİSEL VERİLERİN AKTARIMI<br />
                        Kişisel verileriniz, KVKK&rsquo;nın kişisel verilerin yurti&ccedil;i ve yurtdışına aktarılmasına ilişkin<br />
                        h&uuml;k&uuml;mleri kapsamında işbu Politika&rsquo;nın F.II başlığı altında yer alan ama&ccedil;larla yurt i&ccedil;indeki;<br />
                        resmi kurum ve kuruluşlara, iş ortaklarına, tedarik&ccedil;ilere, topluluk şirketlerine, yurtdışındaki; iş<br />
                        ortaklarına, iştirak ve bağlı ortaklıklara, tedarik&ccedil;ilere, topluluk şirketlerine aktarılabilmektedir.<br />
                        F. IV. KİŞİSEL VERİLERİN İŞLENMESİNİN HUKUKİ SEBEBİ VE TOPLANMA<br />
                        Y&Ouml;NTEMİ<br />
                        Kişisel verileriniz, Şirketimizle hukuki ilişkinizin kurulması esnasında ve s&ouml;z konusu ilişkinin<br />
                        devamı s&uuml;resince sizlerden, &uuml;&ccedil;&uuml;nc&uuml; kişilerden ve yasal mercilerden olmak kaydıyla internet<br />
                        sitesi, muhtelif s&ouml;zleşmeler, mobil uygulamalar, elektronik posta, başvuru formları gibi ara&ccedil;lar<br />
                        &uuml;zerinden, Şirketimiz ile yapılan yazılı veya s&ouml;zl&uuml; iletişim kanalları aracılığıyla s&ouml;zl&uuml;, yazılı<br />
                        veya elektronik ortamda toplanmaktadır. Bu doğrultuda toplanan kişisel verileriniz KVKK&rsquo;nın<br />
                        5, 8 ve 9. maddelerinde belirtilen ve aşağıda yer verilen hukuka uygunluk sebeplerine<br />
                        dayanılarak işlenmektedir.<br />
                        &bull; A&ccedil;ık rızanızın bulunması,<br />
                        &bull; T&uuml;rk Bor&ccedil;lar Kanunu, T&uuml;rk Ticaret Kanunu olmak &uuml;zere Şirketimizin tabi<br />
                        olduğu mevzuatta a&ccedil;ık&ccedil;a &ouml;ng&ouml;r&uuml;lm&uuml;ş olması,<br />
                        &bull; Bir s&ouml;zleşmenin kurulması veya ifasıyla doğrudan doğruya ilgili olması<br />
                        kaydıyla, s&ouml;zleşmenin taraflarına ait kişisel verilerin işlenmesinin gerekli<br />
                        olması,<br />
                        &bull; Hukuki y&uuml;k&uuml;ml&uuml;l&uuml;ğ&uuml;n yerine getirebilmesi i&ccedil;in zorunlu olması,<br />
                        &bull; Bir hakkın tesisi, kullanılması veya korunması i&ccedil;in veri işlemenin zorunlu<br />
                        olması,<br />
                        &bull; İlgili kişinin temel hak ve &ouml;zg&uuml;rl&uuml;klerine zarar vermemek kaydıyla, veri<br />
                        sorumlusunun meşru menfaatleri i&ccedil;in veri işlenmesinin zorunlu olması,<br />
                        G- POTANSİYEL M&Uuml;ŞTERİ<br />
                        G.I. İŞLENEN KİŞİSEL VERİLERİNİZ NELERDİR?<br />
                        Şirket ile ilişkiniz kapsamında aşağıdaki kişisel verileriniz işlenmektedir;<br />
                        ● Kimlik Bilgileriniz: Ad soyad, TCKN/VKN,<br />
                        ● İletişim Bilgileriniz: Telefon numarası, adres bilgileri, e-posta adresi,<br />
                        ● Finansal Bilgileriniz: Fatura/bor&ccedil; bilgisi, finansal performans bilgileri, kredi ve risk<br />
                        bilgileri.<br />
                        ● Diğer Bilgileriniz: Kampanya &ccedil;alışmasıyla elde edilen bilgiler,<br />
                        ● Ara&ccedil; Bilgileriniz: Ara&ccedil; plaka bilgisi, ara&ccedil; model bilgisi, ara&ccedil; km bilgisi.<br />
                        G.II. KİŞİSEL VERİLERİNİZİN İŞLENME AMA&Ccedil;LARI<br />
                        Kişisel verileriniz, ilgili mevzuata uygun olarak aşağıdaki ama&ccedil;lar ile işlenebilecektir:<br />
                        &bull; Potansiyel m&uuml;şteri ziyaretlerinin ger&ccedil;ekleştirilmesi,<br />
                        &bull; Yardım faaliyetlerinin y&uuml;r&uuml;t&uuml;lmesi,<br />
                        &bull; Risk analizi değerlendirmelerinin yapılması,<br />
                        &bull; Dijital ortamlar da dahil olmak &uuml;zere her t&uuml;rl&uuml; mecrada reklam ve tanıtım amacıyla<br />
                        pazarlama faaliyetlerinin yerine getirilmesi.<br />
                        G. III. İŞLENEN KİŞİSEL VERİLERİN AKTARIMI<br />
                        Kişisel verileriniz, KVKK&rsquo;nın kişisel verilerin yurti&ccedil;i ve yurtdışına aktarılmasına ilişkin<br />
                        h&uuml;k&uuml;mleri kapsamında işbu Politika&rsquo;nın G.II başlığı altında yer alan ama&ccedil;larla yurt i&ccedil;indeki<br />
                        resmi kurum ve kuruluşlara aktarılabilmektedir.<br />
                        G. IV. KİŞİSEL VERİLERİN İŞLENMESİNİN HUKUKİ SEBEBİ VE TOPLANMA<br />
                        Y&Ouml;NTEMİ<br />
                        Kişisel verileriniz, Şirketimizle hukuki ilişkinizin kurulması esnasında ve s&ouml;z konusu ilişkinin<br />
                        devamı s&uuml;resince sizlerden, &uuml;&ccedil;&uuml;nc&uuml; kişilerden ve yasal mercilerden olmak kaydıyla internet<br />
                        sitesi, muhtelif s&ouml;zleşmeler, mobil uygulamalar, elektronik posta, başvuru formları gibi ara&ccedil;lar<br />
                        &uuml;zerinden, Şirketimiz ile yapılan yazılı veya s&ouml;zl&uuml; iletişim kanalları aracılığıyla s&ouml;zl&uuml;, yazılı<br />
                        veya elektronik ortamda toplanmaktadır. Bu doğrultuda toplanan kişisel verileriniz KVKK&rsquo;nın<br />
                        5. ve 8. maddelerinde belirtilen ve aşağıda yer verilen hukuka uygunluk sebeplerine dayanılarak<br />
                        işlenmektedir.<br />
                        &bull; A&ccedil;ık rızanızın bulunması,<br />
                        &bull; Şirketimizin tabi olduğu mevzuatta a&ccedil;ık&ccedil;a &ouml;ng&ouml;r&uuml;lm&uuml;ş olması,
                            <br />
                        &bull; Bir s&ouml;zleşmenin kurulması veya ifasıyla doğrudan doğruya ilgili olması<br />
                        kaydıyla, s&ouml;zleşmenin taraflarına ait kişisel verilerin işlenmesinin gerekli<br />
                        olması,<br />
                        &bull; Bir hakkın tesisi, kullanılması veya korunması i&ccedil;in veri işlemenin zorunlu<br />
                        olması,<br />
                        &bull; İlgili kişinin temel hak ve &ouml;zg&uuml;rl&uuml;klerine zarar vermemek kaydıyla, veri<br />
                        sorumlusunun meşru menfaatleri i&ccedil;in veri işlenmesinin zorunlu olması.<br />
                        H. KİŞİSEL VERİLERİNİZİN KORUNMASINA Y&Ouml;NELİK HAKLARINIZ<br />
                        Bu Politika&rsquo;nın &ldquo;İletişim&rdquo; b&ouml;l&uuml;m&uuml;nde yer alan y&ouml;ntemlerle Şirketimize başvurarak,<br />
                        ● Kişisel verilerinizin işlenip işlenmediğini &ouml;ğrenme,<br />
                        ● İşlenmişse buna ilişkin bilgi talep etme,<br />
                        ● Kişisel verilerinizin işlenme amacını ve bunların amacına uygun kullanılıp<br />
                        kullanılmadığını &ouml;ğrenme,<br />
                        ● Yurt i&ccedil;inde veya yurt dışında aktarıldığı &uuml;&ccedil;&uuml;nc&uuml; kişileri bilme,<br />
                        ● Kişisel verilerin eksik veya yanlış işlenmiş olması halinde bunların d&uuml;zeltilmesini<br />
                        isteme,<br />
                        ● KVKK&rsquo;da &ouml;ng&ouml;r&uuml;len şartlar &ccedil;er&ccedil;evesinde kişisel verilerinizin silinmesini veya yok<br />
                        edilmesini isteme,<br />
                        ● Yukarıda belirtilen d&uuml;zeltme, silinme ve yok edilme şeklindeki haklarınız uyarınca<br />
                        yapılan işlemlerin, kişisel verilerin aktarıldığı &uuml;&ccedil;&uuml;nc&uuml; kişilere bildirilmesini isteme,<br />
                        ● İşlenen kişisel verilerinizin m&uuml;nhasıran otomatik sistemler ile analiz edilmesi sureti ile<br />
                        aleyhinize bir sonucun ortaya &ccedil;ıkmasına itiraz etme,<br />
                        ● Kişisel verilerinizin ilgili mevzuata aykırı olarak işlenmesi sebebiyle zarara uğramanız<br />
                        h&acirc;linde zararınızın giderilmesini talep etme haklarına sahiptir.<br />
                        I. HAK VE TALEPLERİNİZ İ&Ccedil;İN İLETİŞİM<br />
                        Kişisel verilerinizle ilgili sorularınızı ve taleplerinizi, Veri Sorumlusuna Başvuru Usul ve<br />
                        Esasları hakkında Tebliğ&rsquo;de belirtilen şartlara uygun d&uuml;zenlenmiş dilek&ccedil;eyle K&uuml;&ccedil;&uuml;kbakkalk&ouml;y<br />
                        Mah. Merdivenk&ouml;y Yolu Cad. CZD Plaza No:22 K:6 D:7 Ataşehir / İstanbul adresine kimlik tespiti<br />
                        yapılmak suretiyle bizzat elden iletebilir ya da noter kanalıyla ulaştırabilirsiniz. Bunun yanında,<br />
                        motultr@hs01.kep.tr kayıtlı elektronik posta (KEP) adresine, g&uuml;venli elektronik imza ve mobil<br />
                        imza ya da tarafınızca şirketimize daha &ouml;nce bildirilen ve tarafımızca teyit edilmiş olan şirket<br />
                        sistemimizdeki elektronik posta adresini kullanmak suretiyle kvkk@tr.motul.com elektronik<br />
                        posta adresine iletebilirsiniz.<br />
                        J. POLİTİKA HAKKINDA<br />
                        Bu Politika Şirket tarafından yayımlandığı tarih itibariyle ge&ccedil;erli olacaktır. Şirket, bu<br />
                        Politika&rsquo;da, gerekli olduğu takdirde, her zaman değişiklik yapabilir. Şirket tarafından yapılacak<br />
                        değişiklikler, Politika&rsquo;nın www.motul.com.tr adresinde yayımlanmasıyla birlikte ge&ccedil;erlilik<br />
                        kazanır.
                    </p>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tamam</button>
                </div>

            </div>


        </div>
    </div>

    <% if (kullanici.IlkSifreDegistimi == false)
        { %>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#myModal").modal("show");
            $('#myModal').modal({ backdrop: 'static', keyboard: false });

        }); // ready
    </script>
    <% } %>
</asp:Content>
