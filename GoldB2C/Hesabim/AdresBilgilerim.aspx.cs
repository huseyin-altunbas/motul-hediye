﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using Goldb2cInfrastructure;

public partial class Hesabim_AdresBilgilerim : System.Web.UI.Page
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();

    Kullanici kullanici;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)Session["Kullanici"];
        }
        else
            UyelikIslemleriBLL.MusteriGirisineYonlendir(Request.Url.PathAndQuery);


        if (!IsPostBack)
        {
            //Carisi açık değilse adresi yoktur
            if (kullanici.ErpCariId > 0)
            {
                AdresleriListele(kullanici.ErpCariId);
            }
            else
                dpAdresler.Items.Insert(0, new ListItem("Seçiniz", "0"));

            dpSehir.DataSource = bllUyelikIslemleri.SehirListesi("TÜRKİYE");
            dpSehir.DataValueField = "SehirAd";
            dpSehir.DataTextField = "SehirAd";
            dpSehir.DataBind();
            dpSehir.Items.Insert(0, new ListItem("Seçiniz", "0"));

            dpIlce.Items.Insert(0, new ListItem("Seçiniz", "0"));
        }
    }

    private void AdresleriListele(int ErpCariId)
    {
        dpAdresler.DataSource = bllUyelikIslemleri.CariAdresListele(ErpCariId, 0);
        dpAdresler.DataTextField = "AdresKod";
        dpAdresler.DataValueField = "KayitId";
        dpAdresler.DataBind();
        dpAdresler.Items.Insert(0, new ListItem("Seçiniz", "0"));
    }




    protected void btnYeniAdres_Click(object sender, EventArgs e)
    {
        AdresBilgileriniTemizle();
    }

    private void AdresBilgileriniTemizle()
    {
        dpAdresler.SelectedIndex = 0;
        dpSehir.SelectedIndex = 0;
        dpIlce.SelectedIndex = 0;
        txtAdresAd.Text = string.Empty;
        txtYazismaUnvan.Text = string.Empty;
        txtAdres.Text = string.Empty;
        txtCepTel.Text = string.Empty;
        txtCepTelKod.Text = string.Empty;
        txtPostaKodu.Text = string.Empty;
        txtSabitTelKod.Text = string.Empty;
        txtSabitTel.Text = string.Empty;
        txtTcKimlikNo.Text = string.Empty;
        txtVergiDairesi.Text = string.Empty;
        txtVergiNumarasi.Text = string.Empty;
    }
    protected void btnAdresKaydet_Click(object sender, EventArgs e)
    {
        bool KayitSonuc = false;
        if (kullanici.ErpCariId <= 0)
        {
            //Cari kaydını oluştur ErpCariId güncelle
            kullanici = bllUyelikIslemleri.CariVeriOlustur(kullanici);

            if (kullanici.ErpCariId > 0)
                KayitSonuc = bllUyelikIslemleri.CariAdresKaydet(kullanici.ErpCariId, Convert.ToInt32(dpAdresler.SelectedValue), txtAdresAd.Text.Trim(), txtAdres.Text.Trim(), txtYazismaUnvan.Text.Trim(), kullanici.UlkeKod, dpSehir.SelectedValue, Request.Form[dpIlce.UniqueID], txtCepTelKod.Text.Trim() + txtCepTel.Text.Trim(), txtSabitTelKod.Text.Trim() + txtSabitTel.Text.Trim(), txtPostaKodu.Text.Trim(), txtTcKimlikNo.Text.Trim(), txtVergiDairesi.Text.Trim(), txtVergiNumarasi.Text.Trim());
        }
        else
            KayitSonuc = bllUyelikIslemleri.CariAdresKaydet(kullanici.ErpCariId, Convert.ToInt32(dpAdresler.SelectedValue), txtAdresAd.Text.Trim(), txtAdres.Text.Trim(), txtYazismaUnvan.Text.Trim(), kullanici.UlkeKod, dpSehir.SelectedValue, Request.Form[dpIlce.UniqueID], txtCepTelKod.Text.Trim() + txtCepTel.Text.Trim(), txtSabitTelKod.Text.Trim() + txtSabitTel.Text.Trim(), txtPostaKodu.Text.Trim(), txtTcKimlikNo.Text.Trim(), txtVergiDairesi.Text.Trim(), txtVergiNumarasi.Text.Trim());

        if (KayitSonuc)
        {
            AdresleriListele(kullanici.ErpCariId);
            AdresBilgileriniTemizle();
            ClientScript.RegisterStartupScript(this.GetType(), "mesaj", "<script type=\"text/javascript\">alert('Adres başarıyla kaydedildi.')</script>");
        }
        else
            ClientScript.RegisterStartupScript(this.GetType(), "mesaj", "<script type=\"text/javascript\">alert('Bir hata oluştu. Lütfen tekrar deneyin.')</script>");


    }

    [System.Web.Services.WebMethod]
    public static List<Ilce> getIlceListesi(string SehirKod)
    {
        DataTable dtIlce = new DataTable();
        dtIlce = UyelikIslemleriBLL.IlceListesi(SehirKod);
        List<Ilce> ilceler = new List<Ilce>();
        Ilce ilce;
        foreach (DataRow dr in dtIlce.Rows)
        {
            ilce = new Ilce();
            ilce.IlceAd = dr["IlceAd"].ToString();
            ilce.SehirKod = dr["SehirKod"].ToString();
            ilceler.Add(ilce);
        }

        return ilceler;
    }


    protected void dpAdresler_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtAdres = new DataTable();
        dtAdres = bllUyelikIslemleri.CariAdresListele(kullanici.ErpCariId, Convert.ToInt32(dpAdresler.SelectedValue));
        txtAdresAd.Text = dtAdres.Rows[0]["AdresKod"].ToString();
        txtAdres.Text = dtAdres.Rows[0]["AdresTanim"].ToString();
        txtYazismaUnvan.Text = dtAdres.Rows[0]["YazismaUnvan"].ToString();
        if (!string.IsNullOrEmpty(dtAdres.Rows[0]["MobilTelefon1"].ToString()) && dtAdres.Rows[0]["MobilTelefon1"].ToString().Length == 10)
        {
            txtCepTelKod.Text = dtAdres.Rows[0]["MobilTelefon1"].ToString().Substring(0, 3);
            txtCepTel.Text = dtAdres.Rows[0]["MobilTelefon1"].ToString().Substring(3, 7);
        }
        if (!string.IsNullOrEmpty(dtAdres.Rows[0]["SabitTelefon1"].ToString()) && dtAdres.Rows[0]["SabitTelefon1"].ToString().Length == 10)
        {
            txtSabitTelKod.Text = dtAdres.Rows[0]["SabitTelefon1"].ToString().Substring(0, 3);
            txtSabitTel.Text = dtAdres.Rows[0]["SabitTelefon1"].ToString().Substring(3, 7);
        }
        txtPostaKodu.Text = dtAdres.Rows[0]["PostaKod"].ToString();
        txtTcKimlikNo.Text = dtAdres.Rows[0]["TckimlikNo"].ToString();
        txtVergiDairesi.Text = dtAdres.Rows[0]["VergiDaire"].ToString();
        txtVergiNumarasi.Text = dtAdres.Rows[0]["VergiNo"].ToString();


        dpIlce.DataSource = UyelikIslemleriBLL.IlceListesi(dtAdres.Rows[0]["SehirKod"].ToString());
        dpIlce.DataValueField = "IlceAd";
        dpIlce.DataTextField = "IlceAd";
        dpIlce.DataBind();
        dpIlce.Items.Insert(0, new ListItem("Seçiniz", "0"));

        if (dpSehir.SelectedIndex >= 0)
            dpSehir.Items[dpSehir.SelectedIndex].Selected = false;
        dpSehir.Items.FindByValue(dtAdres.Rows[0]["SehirKod"].ToString()).Selected = true;

        if (dpIlce.SelectedIndex >= 0)
            dpIlce.Items[dpIlce.SelectedIndex].Selected = false;
        dpIlce.Items.FindByValue(dtAdres.Rows[0]["IlceKod"].ToString()).Selected = true;



    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">Popup('" + mesaj + "');</script>";
    }

}