﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using Goldb2cEntity;
using System.Text;
using Goldb2cInfrastructure;
using System.IO;
using System.Net;
using System.Web.Services;

public partial class Hesabim_Siparislerim : System.Web.UI.Page
{
    SiparisIslemleriBLL bllSiparisIslemleri = new SiparisIslemleriBLL();
    SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
    DataSet dsSiparis;
    DataSet dsSiparis2;
    Kullanici kullanici;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
            kullanici = (Kullanici)Session["Kullanici"];

        if (!IsPostBack)
        {
            
            if (kullanici.ErpCariId > 0)
            {
                dsSiparis = bllSiparisGoruntuleme.GenelSiparisIzleme(kullanici.ErpCariId);
                dsSiparis2 = bllSiparisGoruntuleme.GenelSiparisListeleme(kullanici.ErpCariId, 0, 0, 0, Convert.ToDateTime("01.01.1970"), Convert.ToDateTime("01.01.2900"));

                

                foreach (DataRow dr in dsSiparis.Tables["SiparisTakipMasterTT"].Rows)
                {
                    if (dr["siparis_statu"].ToString() == "Siparişiniz Onayınızı Bekliyor.")
                    {
                        dr.Delete();                        
                    }                   
                }
                dsSiparis.Tables["SiparisTakipMasterTT"].AcceptChanges();

                foreach (DataRow dr in dsSiparis.Tables["SiparisTakipMasterTT"].Rows)
                {
                    dr["siparis_statu"] = dr["siparis_statu"].ToString().Replace("Isıcam", "Motul");
                }

                foreach (DataRow dr in dsSiparis2.Tables["CariSiparisMasterTT"].Rows)
                {
                    dr["siparis_statu"] = dr["siparis_statu"].ToString().Replace("Isıcam", "Motul");
                }

                foreach (DataRow dr in dsSiparis.Tables["SiparisTakipMasterTT"].Rows)
                {
                    try
                    {
                        DataRow RowKargo = dsSiparis2.Tables["CariSiparisDetayTT"].Select("siparis_id = '" + dr["siparis_id"].ToString() + "'")[0];
                        if (!string.IsNullOrEmpty(RowKargo["siparis_statu"].ToString()) && RowKargo["siparis_statu"].ToString().IndexOf("Kargoda. ") > -1)
                        {
                            string KargoTakipNo = RowKargo["siparis_statu"].ToString().Replace("Kargoda. ", "");
                            int _kargotakipNo = 0;
                            if (KargoTakipNo.Contains("/"))
                            {
                                KargoTakipNo = KargoTakipNo.Split('/')[0];
                                KargoTakipNo = KargoTakipNo.Trim();
                            }
                            if (int.TryParse(KargoTakipNo.Substring(0, 2), out _kargotakipNo))
                            {
                                //yurtiçi
                                dr["siparis_statu"] = "Kargoda. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://selfservis.yurticikargo.com/reports/SSWDocumentDetail.aspx?DocId=" + KargoTakipNo + "\" >Kargom Nerede?</a>";
                            }
                            else
                            {
                                if (KargoTakipNo.StartsWith("1Z"))
                                {
                                    //ups
                                    dr["siparis_statu"] = "Siparişiniz Sevk Edilmiştir. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://www.ups.com.tr/WaybillSorgu.aspx?Waybill=" + KargoTakipNo + "\" >Kargom Nerede?</a>";
                                }
                                else
                                {
                                    //mng
                                    //string fat_seri = "", fat_numara = "";
                                    //foreach (char ch in KargoTakipNo)
                                    //{
                                    //    if (!char.IsDigit(ch))
                                    //        fat_seri = fat_seri + ch;
                                    //    else
                                    //        fat_numara = fat_numara + ch;

                                    //}
                                    //RowKargo["siparis_statu"] = "Kargoda. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://service.mngkargo.com.tr/iactive/takip.asp?fatseri=" + fat_seri + "&fatnumara=" + fat_numara + "&fi=2\" >Kargom Nerede?</a>";

                                    dr["siparis_statu"] = "Kargoda. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://service.mngkargo.com.tr/iactive/popup/kargotakip.asp?k=" + KargoTakipNo + "&fi=2\" >Kargom Nerede?</a>";

                                }
                            }
                        }
                    }
                    catch (Exception)
                    {

                         
                    }
                }


                //rptSiparisBilgileri.DataSource = dsSiparis.Tables["SiparisTakipMasterTT"].Select("siparis_statu not in ('Siparişiniz Onayınızı Bekliyor.')");                
                //rptSiparisBilgileri.DataBind();

                rptSiparisBilgileri.DataSource = dsSiparis.Tables["SiparisTakipMasterTT"];
                rptSiparisBilgileri.DataBind();


            }
        }
    }

    protected void rptSiparisBilgileri_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            try
            {                
                DataRowView drv = e.Item.DataItem as DataRowView;
                Repeater rptSiparisDetayBilgileri = (Repeater)e.Item.FindControl("rptSiparisDetayBilgileri");                
                rptSiparisDetayBilgileri.DataSource = dsSiparis.Tables["SiparisTakipDetayTT"].Select("siparis_id = '" + drv.Row["siparis_id"].ToString() + "'");
                rptSiparisDetayBilgileri.DataBind();
            }
            catch (Exception)
            {

            }
        }
    }
}