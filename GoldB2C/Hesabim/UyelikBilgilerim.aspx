﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Hesabim/MpHesabim.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="UyelikBilgilerim.aspx.cs" Inherits="Hesabim_UyelikBilgilerim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Üyelik Bilgilerim</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
    <span class="page-heading-title2">ÜYELİK BİLGİLERİM</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <script src="/assets/js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/assets/lib/fancyBox/jquery.fancybox.css" />
    <script type="text/javascript" src="/assets/lib/fancyBox/jquery.fancybox.js"></script>
    <!-- popup -->
    <script type="text/javascript">
        function Popup(message) {
            if ($('#popup').length > 0 && $('#popupContent').length > 0) {
                $("#popupContent").html(message).promise().done(function () {
                    $('#popup').fancybox().trigger('click');
                });
            }
        }
    </script>
    <script type="text/javascript">
        jQuery(function ($) {
            $("#<%=txtCepTelKod.ClientID %>").mask("599");
            $("#<%=txtCepTelNo.ClientID %>").mask("9999999999");
            $("#<%=txtTelKod.ClientID %>").mask("999");
            $("#<%=txtTelNo.ClientID %>").mask("9999999");
        });
    </script>
    <script type="text/javascript">
        function getIlceListesi(SehirKod) {
            $.ajax({
                type: 'POST',
                url: 'http://www.biges.com/bilgilerim/getIlceListesi',
                data: '{ "SehirKod":"' + SehirKod + '" }',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    $("#<%=dpIlce.ClientID %>").find('option').remove().end().append('<option value="0">Seçiniz</option>');
                    $.each(result.d, function (index, data) {
                        $("#<%=dpIlce.ClientID %>").append(
                            $('<option value="' + data.IlceAd + '"></option>').html(data.IlceAd));
                    });
                },
                error: function () {
                    alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                }
            });
        }
    </script>
    <a id="popup" class="fancybox" href="#popupContent" style="display: none"></a>
    <div id="popupContent" style="display: none">
    </div>
    <!-- popup -->
    <div class="page-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box-form">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="">
                                Kullanıcı Adı</label>
                            <asp:TextBox ID="txtAd" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqAd" runat="server" ControlToValidate="txtAd" ValidationGroup="grpKullaniciBilgileri"
                                    ErrorMessage="Adınızı girin."></asp:RequiredFieldValidator>
                            </label>
                        </div>
                        <%--  <div class="col-sm-6">
                            <label for="">
                                Soyadınız</label>
                            <asp:TextBox ID="txtSoyad" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqSoyad" runat="server" ControlToValidate="txtSoyad"
                                    ValidationGroup="grpKullaniciBilgileri" ErrorMessage="Soyadınızı girin."></asp:RequiredFieldValidator>
                            </label>
                        </div>--%>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="">
                                E-Posta</label>
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqEmail" runat="server" ControlToValidate="txtEmail"
                                    ValidationGroup="grpKullaniciBilgileri" ErrorMessage="E-Posta adresinizi girin."
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="rqEmailExp" runat="server" ErrorMessage="Lütfen geçerli bir E-Posta adresi girin."
                                    ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ValidationGroup="grpKullaniciBilgileri" Display="Dynamic"></asp:RegularExpressionValidator>
                            </label>
                        </div>
                        <div class="col-sm-6" style="display: none">
                            <label for="">
                                E-Posta(Tekrar)</label>
                            <asp:TextBox ID="txtEmailTekrar" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqEmailTekrar" runat="server" ControlToValidate="txtEmailTekrar"
                                    ValidationGroup="grpKullaniciBilgileri" ErrorMessage="E-Posta adresinizi tekrar girin."
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="rqEmailTekrarExp" runat="server" ErrorMessage="Lütfen geçerli bir E-Posta adresi girin."
                                    ControlToValidate="txtEmailTekrar" ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z\-])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
                                    ValidationGroup="grpKullaniciBilgileri" Display="Dynamic"></asp:RegularExpressionValidator>
                            </label>
                        </div>
                    </div>
                    <div class="row" style="display: none;">
                        <div class="col-sm-6">
                            <label for="">
                                Doğum Tarihi</label>
                            <div class="row">
                                <div class="col-xs-4" style="padding-left: 15px; padding-right: 2px;">
                                    <asp:DropDownList ID="dpDogumGun" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-xs-4" style="padding-left: 2px; padding-right: 2px;">
                                    <asp:DropDownList ID="dpDogumAy" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-xs-4" style="padding-left: 2px; padding-right: 15px;">
                                    <asp:DropDownList ID="dpDogumYil" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqdpDogumGun" InitialValue="0" runat="server" ControlToValidate="dpDogumGun"
                                    ErrorMessage="*Doğduğunuz günü seçin." ValidationGroup="grpKullaniciBilgileri"></asp:RequiredFieldValidator>&nbsp;
                                <asp:RequiredFieldValidator ID="rqdpDogumAy" InitialValue="0" runat="server" ControlToValidate="dpDogumAy"
                                    ErrorMessage="*Doğduğunuz ayı seçin." ValidationGroup="grpKullaniciBilgileri"
                                    Display="Dynamic"></asp:RequiredFieldValidator>&nbsp;
                                <asp:RequiredFieldValidator ID="rqdpDogumYil" InitialValue="0" runat="server" ControlToValidate="dpDogumYil"
                                    ErrorMessage="*Doğduğunuz yılı seçin." ValidationGroup="grpKullaniciBilgileri"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <label for="">
                                Cinsiyet</label>
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:RadioButton ID="rbKadin" Text="Kadın" runat="server" Checked="true" GroupName="Cinsiyet" />
                                </div>
                                <div class="col-xs-3">
                                    <asp:RadioButton ID="rbErkek" Text="Erkek" runat="server" GroupName="Cinsiyet" />
                                </div>
                                <div class="col-xs-6">
                                </div>
                            </div>
                            <label class="fright notification">
                            </label>
                        </div>
                    </div>
                    <div class="row" style="display: none;">
                        <div class="col-sm-6">
                            <label for="">
                                Şehir</label>
                            <asp:DropDownList ID="dpSehir" CssClass="form-control" onChange="getIlceListesi(this.value);"
                                runat="server">
                            </asp:DropDownList>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqSehir" runat="server" ControlToValidate="dpSehir"
                                    InitialValue="0" ValidationGroup="grpKullaniciBilgileri" ErrorMessage="Şehir seçin"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <label for="">
                                İlçe</label>
                            <asp:DropDownList ID="dpIlce" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                            <label class="fright notification">
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">

                            <div style="display:none;">
                                <label for="">
                                    Cep Telefonu Numarası</label><br />
                                <asp:TextBox ID="txtCepTelKod" CssClass="form-control input-sm inlineBlock" Width="60px" 
                                    MaxLength="3" runat="server"></asp:TextBox>
                            </div>

                              <label for="">
                                    Cep Telefonu Numarası</label><br />

                            <asp:TextBox ID="txtCepTelNo" CssClass="form-control input-sm inlineBlock" Width="120px" Enabled="false"
                                MaxLength="10" runat="server"></asp:TextBox>
                            <label class="fright notification">
                                <span style="visibility: hidden;">*Cep telefonu numaranızı girin.</span>
                            </label>
                        </div>
                        <div class="col-sm-6" style="display: none;">
                            <label for="">
                                Telefonu Numarası</label><br />
                            <asp:TextBox ID="txtTelKod" CssClass="form-control input-sm inlineBlock" Width="60px"
                                MaxLength="3" runat="server"></asp:TextBox>
                            <asp:TextBox ID="txtTelNo" CssClass="form-control input-sm inlineBlock" Width="120px"
                                MaxLength="10" runat="server" ReadOnly></asp:TextBox>
                            <label class="fright notification">
                                <span style="visibility: hidden;">*Telefon numaranızı girin.</span>
                            </label>
                        </div>
                    </div>
                    <%--<div class="row">
                        <div class="col-sm-6" id="DivTavsiyeEden" runat="server">
                            <label for="">
                                Tavsiye Eden Kişi Email</label><br />
                            <asp:TextBox ID="txtRefEmail" CssClass="form-control" runat="server"></asp:TextBox>
                            <label class="fright notification">
                                <asp:RegularExpressionValidator ID="rqExRefEmail" runat="server" ErrorMessage="Lütfen geçerli bir E-Posta adresi girin."
                                    ControlToValidate="txtRefEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ValidationGroup="grpKullaniciBilgileri"></asp:RegularExpressionValidator>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <ul class="check-box-list" style="margin: 5px 0px 10px 0px;">
                                <li>
                                    <asp:CheckBox ID="chkEposta" runat="server" Checked="true" />
                                    <label for="cphIcerikMetin_chkEposta">
                                        <span class="button" style="margin-top: 0px !important;"></span>Kampanyalardan e-posta
                                        ile haberdar olmak istiyorum.
                                    </label>
                                </li>
                                <li>
                                    <asp:CheckBox ID="chkSms" runat="server" Checked="true" />
                                    <label for="cphIcerikMetin_chkSms">
                                        <span class="button" style="margin-top: 0px !important;"></span>Kampanyalardan SMS
                                        ile haberdar olmak istiyorum.
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>--%>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-6" style="display:none;">
                            <div class="fleft h45 color4e mt30 ml28 w350">
                                <asp:LinkButton ID="btnGuncelle" CssClass="btn btnInput" ValidationGroup="grpKullaniciBilgileri"
                                    runat="server" OnClick="btnGuncelle_Click"><i class="fa fa-user"></i>&nbsp;Bilgilerim</asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
