﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Hesabim/MpHesabim.master" AutoEventWireup="true"
    CodeFile="AdresBilgilerim.aspx.cs" Inherits="Hesabim_AdresBilgilerim" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Üyelik Bilgilerim</span>
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Adres Bilgilerimi
        Güncelle</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
    <span class="page-heading-title2">ADRES BİLGİLERİMİ GÜNCELLE</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <script src="/assets/js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/assets/lib/fancyBox/jquery.fancybox.css" />
    <script type="text/javascript" src="/assets/lib/fancyBox/jquery.fancybox.js"></script>
    <!-- popup -->
    <script type="text/javascript">
        function Popup(message) {
            if ($('#popup').length > 0 && $('#popupContent').length > 0) {
                $("#popupContent").html(message).promise().done(function () {
                    $('#popup').fancybox().trigger('click');
                });
            }
        }
    </script>
    <a id="popup" class="fancybox" href="#popupContent" style="display: none"></a>
    <div id="popupContent" style="display: none">
    </div>
    <!-- popup -->
    <script type="text/javascript">
        jQuery(function ($) {
            $("#<%=txtCepTelKod.ClientID %>").mask("599");
            $("#<%=txtCepTel.ClientID %>").mask("9999999");
            $("#<%=txtSabitTelKod.ClientID %>").mask("999");
            $("#<%=txtSabitTel.ClientID %>").mask("9999999");
            $("#<%=txtTcKimlikNo.ClientID %>").mask("99999999999");
            $("#<%=txtPostaKodu.ClientID %>").mask("99999");
            $("#<%=txtVergiNumarasi.ClientID %>").mask("9999999999");
        });
    </script>
    <script type="text/javascript">
        function getIlceListesi(SehirKod) {
            $.ajax({
                type: 'POST',
                url: '/Hesabim/AdresBilgilerim.aspx/getIlceListesi',
                data: '{ "SehirKod":"' + SehirKod + '" }',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    $("#<%=dpIlce.ClientID %>").find('option').remove().append('<option value="0">Seçiniz</option>');
                    $.each(result.d, function (index, data) {
                        $("#<%=dpIlce.ClientID %>").append(
                      $('<option value="' + data.IlceAd + '"></option>').html(data.IlceAd));
                    });
                },
                error: function () {
                    alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                }
            });
        }
    </script>
    <div class="page-content">
        <div class="row">
            <div class="col-sm-6">
                <div class="box-form">
                    <div class="mt10">
                        <asp:LinkButton ID="btnYeniAdres" Text="Yeni Adres" OnClick="btnYeniAdres_Click"
                            CssClass="btn btnInput" runat="server"><span class="fa fa-book">
                        </span>&nbsp;Yeni Adres</asp:LinkButton>
                    </div>
                    <div class="mt10">
                        <label for="">
                            Adres Seçin</label>
                        <asp:DropDownList ID="dpAdresler" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dpAdresler_SelectedIndexChanged"
                            CssClass="form-control">
                        </asp:DropDownList>
                        <label class="fright notification">
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="">
                            Adres Başlığı</label>
                        <asp:TextBox ID="txtAdresAd" runat="server" CssClass="form-control"></asp:TextBox>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqAdresAd" runat="server" ControlToValidate="txtAdresAd"
                                ValidationGroup="grpAdres" ErrorMessage="Adres adı girin" Display="Dynamic"></asp:RequiredFieldValidator>
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="">
                            Yazışma Unvanı</label>
                        <asp:TextBox ID="txtYazismaUnvan" runat="server" CssClass="form-control"></asp:TextBox>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqYazismaUnvan" runat="server" ControlToValidate="txtYazismaUnvan"
                                ValidationGroup="grpAdres" ErrorMessage="Yazışma ünvanı girin." Display="Dynamic"></asp:RequiredFieldValidator>
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="">
                            Adres</label>
                        <asp:TextBox ID="txtAdres" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqAdres" runat="server" ControlToValidate="txtAdres"
                                ValidationGroup="grpAdres" ErrorMessage="Adres girin" Display="Dynamic"></asp:RequiredFieldValidator>
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="">
                            Şehir</label>
                        <asp:DropDownList ID="dpSehir" runat="server" AutoPostBack="False" CssClass="form-control"
                            onChange="getIlceListesi(this.value);">
                        </asp:DropDownList>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqSehir" runat="server" ControlToValidate="dpSehir"
                                InitialValue="0" ValidationGroup="grpAdres" ErrorMessage="Şehir seçin" Display="Dynamic"></asp:RequiredFieldValidator>
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="">
                            İlçe</label>
                        <asp:DropDownList ID="dpIlce" CssClass="form-control" runat="server">
                        </asp:DropDownList>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqIlce" runat="server" ControlToValidate="dpIlce"
                                InitialValue="0" ValidationGroup="grpAdres" ErrorMessage="İlçe seçin" Display="Dynamic"></asp:RequiredFieldValidator>
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="">
                            Posta Kodu</label>
                        <asp:TextBox ID="txtPostaKodu" runat="server" CssClass="form-control" MaxLength="7"></asp:TextBox>
                        <label class="fright notification">
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="">
                            Telefon(Sabit)</label><br />
                        <asp:TextBox ID="txtSabitTelKod" runat="server" CssClass="form-control input-sm inlineBlock"
                            MaxLength="3" Width="60px"></asp:TextBox>
                        <asp:TextBox ID="txtSabitTel" runat="server" CssClass="form-control input-sm inlineBlock"
                            MaxLength="7" Width="120px"></asp:TextBox>
                        <label class="fright notification">
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="">
                            Telefon(GSM)</label><br />
                        <asp:TextBox ID="txtCepTelKod" runat="server" CssClass="form-control input-sm inlineBlock"
                            MaxLength="3" Width="60px"></asp:TextBox>
                        <asp:TextBox ID="txtCepTel" runat="server" CssClass="form-control input-sm inlineBlock"
                            MaxLength="7" Width="120px"></asp:TextBox>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqCepTelKod" runat="server" ControlToValidate="txtCepTelKod"
                                ValidationGroup="grpAdres" ErrorMessage="Cep telefonu kodu girin" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rqCepTel" runat="server" ControlToValidate="txtCepTel"
                                ValidationGroup="grpAdres" ErrorMessage="Cep telefonunuzu girin" Display="Dynamic"></asp:RequiredFieldValidator>
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="">
                            Vergi Dairesi</label>
                        <asp:TextBox ID="txtVergiDairesi" runat="server" CssClass="form-control"></asp:TextBox>
                        <label class="fright notification">
                            (Kurumlar için)
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="">
                            Vergi Numarası</label>
                        <asp:TextBox ID="txtVergiNumarasi" runat="server" CssClass="form-control"></asp:TextBox>
                        <label class="fright notification">
                            (Kurumlar için)
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="">
                            T.C. Kimlik No</label>
                        <asp:TextBox ID="txtTcKimlikNo" runat="server" CssClass="form-control" MaxLength="11"></asp:TextBox>
                        <label class="fright notification">
                            (T.C. Kimlik Numaranızı öğrenmek için http://tckimlik.nvi.gov.tr adresini kullanabilirsiniz.)
                        </label>
                    </div>
                    <div class="mt10">
                        <asp:LinkButton ID="btnAdresKaydet" Text="Kaydet" OnClick="btnAdresKaydet_Click"
                            CssClass="btn btnInput" runat="server" ValidationGroup="grpAdres"><span class="fa fa-user">
                        </span>&nbsp;Kaydet</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box-form">
                    <div style="text-align: center;">
                        <h3 style="margin-bottom: 50px;">
                            <span class="colorF36">Motul Hediye </span>'ya üye olduysanız;<br />
                            <br />
                            <span class="colorF36">"Yeni Adres"</span> butonuna tıklayarak yeni adres tanımlayabilirsiniz.
                            <br />
                            Ya da<br />
                            <span class="colorF36">"Adres Seçin"</span> ile kayıtlı adresinizde değişiklik yapabilirsiniz.</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
