﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Hesabim/MpHesabim.master" AutoEventWireup="true"
    CodeFile="Siparislerim.aspx.cs" Inherits="Hesabim_Siparislerim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Satış Sonrası</span>
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Siparişlerim</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
    <span class="page-heading-title2">SİPARİŞLERİM</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <style type="text/css">
        .order_number {
            width: 80px;
        }
        .order_description {
            width: 400px;
        }
        .order_date {
            width: 80px;
        }
        .order_price {
            width: 80px;
        }
        .tableOrder #theadMaster th {
            font-size: 11px;
            font-weight: bold;
            font-family: Trebuchet MS;
            padding: 10px !important;
        }
        .tableOrder tbody td {
            font-size: 11px;
            font-weight: bold;
        }
        .tableOrderDetail {
            display: none;
        }
        .tableOrderDetail #theadDetail {
            background-color: #E73137;
        }
        .tableOrderDetail #theadDetail th {
            font-size: 9px;
            font-weight: bold;
            font-family: Trebuchet MS;
            padding: 5px !important;
            color: #fff;
        }
        .tableOrderDetail tbody td {
            font-size: 9px;
        }
        .btopF36 {
            border-top: 1px solid #a8a8a8 !important;
        }
        .btop0 {
            border-top: 0px solid #fff !important;
        }
        span.accordion {
            width: 16px;
            height: 16px;
            border: medium none;
            border-radius: 0px;
            color: #FFF;
            font-weight: bold;
            position: absolute;
            margin-left: 5px;
            cursor: pointer;
        }
        span.accordion.accordion-down {
            background: #a8a8a8 url("/assets/images/down.png") no-repeat scroll center center;
        }
        span.accordion.accordion-up {
            background: #E73137 url("/assets/images/up.png") no-repeat scroll center center;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".tableOrder").on("click", ".accordion", function () {

                $(this).closest(".tableOrder").next(".tableOrderDetail").toggle("drop", 500);

                if ($(this).hasClass("accordion-down"))
                    $(this).removeClass("accordion-down").addClass("accordion-up");
                else
                    $(this).removeClass("accordion-up").addClass("accordion-down");
            });
        });
    </script>
    <div>
        <div class="row">
            <!-- page wapper-->
            <div class="columns-container">
                <div id="columns">
                    <div class="page-order">
                        <div>
                            <asp:Repeater ID="rptSiparisBilgileri" runat="server" OnItemDataBound="rptSiparisBilgileri_ItemDataBound">
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table class="table table-bordered table-responsive cart_summary tableOrder mt10 mb0 btopF36">
                                        <thead id="theadMaster">
                                            <tr>
                                                <th class="order_number">
                                                    Sipariş No
                                                </th>
                                                <th class="order_description">
                                                    Durum
                                                </th>
                                                <th class="order_date" colspan="2">
                                                    Sipariş Tarihi
                                                </th>
                                                <th class="order_price" colspan="2">
                                                    Tutar
                                                </th>
                                                <th>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="order_number">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_no")%>
                                                </td>
                                                <td class="order_description">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_statu")%>
                                                </td>
                                                <td class="order_date" colspan="2">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_tarih","{0:dd.MM.yyyy}")%>
                                                </td>
                                                <td class="order_price" colspan="2">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_tutar")%>&nbsp;PUAN
                                                </td>
                                                <td>
                                                    Detaylar<span class="accordion accordion-down"></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-bordered table-responsive cart_summary tableOrderDetail">
                                        <asp:Repeater ID="rptSiparisDetayBilgileri" runat="server">
                                            <HeaderTemplate>
                                                <thead id="theadDetail">
                                                    <tr>
                                                        <th>
                                                            Ürün Kodu
                                                        </th>
                                                        <th>
                                                            Ürün Adı
                                                        </th>
                                                        <th>
                                                            Birim Fiyatı
                                                        </th>
                                                        <th>
                                                            Adet
                                                        </th>
                                                        <th style="display:none;">
                                                            Kazancınız
                                                        </th>
                                                        <th>
                                                            Toplam Tutar
                                                        </th>
                                                    </tr>
                                                </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tbody class="btop0">
                                                    <tr>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "urun_id")%>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "urun_ad")%>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "birim_fiyat")%>&nbsp;PUAN
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "siparis_adet")%>
                                                        </td>
                                                        <td style="display:none;">
                                                            <%#DataBinder.Eval(Container.DataItem, "indirim_tutar")%>&nbsp;PUAN
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "toplam_tutar")%>&nbsp;PUAN
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <tbody>
                                                    <tr>
                                                        <td class="address_header">
                                                            <%#DataBinder.Eval(Container.DataItem, "urun_id")%>
                                                        </td>
                                                        <td class="address_description">
                                                            <%#DataBinder.Eval(Container.DataItem, "urun_ad")%>
                                                        </td>
                                                        <td class="address_select">
                                                            <%#DataBinder.Eval(Container.DataItem, "birim_fiyat")%>&nbsp;PUAN
                                                        </td>
                                                        <td class="address_select">
                                                            <%#DataBinder.Eval(Container.DataItem, "siparis_adet")%>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "indirim_tutar")%>&nbsp;PUAN
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "toplam_tutar")%>&nbsp;PUAN
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </AlternatingItemTemplate>
                                            <FooterTemplate>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
