﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;

public partial class Hesabim_SifremiDegistir : System.Web.UI.Page
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    public Kullanici kullanici;

    protected void Page_Load(object sender, EventArgs e)
    {


        if (Session["Kullanici"] != null)
            kullanici = (Kullanici)Session["Kullanici"];
    }

    protected void btnSifremiDegistir_Click(object sender, EventArgs e)
    {
        string mevcutSifre = txtMevcutSifre.Text.Trim();
        string yeniSifre = txtYeniSifre.Text.Trim();
        string yeniSifreTekrar = txtYeniSifreTekrar.Text.Trim();

        if (string.IsNullOrEmpty(mevcutSifre) || string.IsNullOrEmpty(yeniSifre) || string.IsNullOrEmpty(yeniSifreTekrar))
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Tüm alanların dolu olması gerekmektedir."));
            return;
        }

        if (mevcutSifre == yeniSifre)
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Mevcut şifreniz ve yeni şifreniz alanlarına girilen bilgiler farklı olmalı."));
            return;
        }

        if (yeniSifre != yeniSifreTekrar)
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Şifrenizi kontrol ediniz."));
            return;
        }

        if (yeniSifre.Length < 5 || yeniSifre.Length > 20)
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Yeni şifreniz 5 ile 20 karakter arasında olmalıdır."));
            return;
        }

        if (Page.IsValid)
        {
            string message = string.Empty;
            message = bllUyelikIslemleri.GuncelleKullaniciSifre(kullanici.KullaniciId, txtMevcutSifre.Text.Trim(), txtYeniSifre.Text.Trim());
            if (message == "Şifreniz değiştirilmiştir.")
            {
                msg.Visible = false;
            }
            Response.Redirect("/");
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur(message));
        }
    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">Popup('" + mesaj + "');</script>";
    }
}