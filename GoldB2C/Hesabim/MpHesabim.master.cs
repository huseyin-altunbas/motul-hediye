﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;

public partial class Hesabim_MpHesabim : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (Session["Kullanici"] == null)
            {
                UyelikIslemleriBLL.MusteriGirisineYonlendir(Request.Url.PathAndQuery);
            }


            Kullanici kulanici = (Kullanici)Session["Kullanici"];
            string fullUrl = Request.Url.ToString();
            if (!kulanici.IlkSifreDegistimi)
            {
                if (!fullUrl.Contains("/Hesabim/SifremiDegistir.aspx"))
                { Response.Redirect("/Hesabim/SifremiDegistir.aspx"); }

            }
        }
        catch 
        {
            Response.Redirect("/");

        }
    }
}
