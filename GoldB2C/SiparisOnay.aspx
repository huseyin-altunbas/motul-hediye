﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="SiparisOnay.aspx.cs" Inherits="SiparisOnay" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <asp:Literal ID="ltrFacebookConversion" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="/css/goldBasket.css?v=2" type="text/css">
    <!-- page wapper-->
    <div class="columns-container">
        <div class="container" id="columns">
            <!-- breadcrumb -->
            <div class="breadcrumb clearfix">
                <a class="home" href="/" title="Ana Sayfa">Ana Sayfa</a> <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Sepet</span> <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Adres</span><span class="navigation-pipe">
                            &nbsp;</span> <span class="navigation_page">Onay</span>
            </div>
            <!-- ./breadcrumb -->
            <!-- page heading-->
            <%--<h2 class="page-heading no-line">
                <span class="page-heading-title2">SİPARİŞ ONAY</span>
            </h2>--%>
            <!-- ../page heading-->
            <div id="addressInfo" class="page-content page-order">
                <ul class="step">
                    <li><span>1. Sepet</span></li>
                    <li><span>2. Adres</span></li>
                    <li class="current-step"><span>3. Onay</span></li>
                </ul>
                <div class="heading-counter warning">
                    SİPARİŞ ONAY
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <h5>
                            <div id="tamamdiv" runat="server">
                                <div>
                                    <img src="/ImagesGld/siparisiniz-tamamlandi.png" />
                                </div>
                                <div style="font-weight: bold; text-align: center; margin: 30px;">
                                    <span class="colord8" style="font-size: 16px;">SİPARİŞiNİZ BAŞARILI BİR ŞEKİLDE TAMAMLANMIŞTIR.</span><br />
                                    <span style="font-size: 14px;">Ürünlerinizin kargoya teslimatı 10 iş günü içerisinde
                                        yapılacaktır. İlginiz için teşekkür ederiz.</span>
                                </div>
                            </div>
                        </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="invoiceaddress">
                            <div class="address">
                                <span class="title">Teslimat Adresi</span><br />
                                <asp:Literal ID="ltrSiparisYazismaUnvan" runat="server"></asp:Literal>
                                <br />
                                <asp:Literal ID="ltrSiparisAdres" runat="server"></asp:Literal>
                                <br />
                                <asp:Literal ID="ltrSiparisIlce" runat="server"></asp:Literal>
                                <br />
                                <asp:Literal ID="ltrSiparisSehir" runat="server"></asp:Literal>
                                <br />
                                Gsm :
                                <asp:Literal ID="ltrSiparisCepTel" runat="server"></asp:Literal>
                                <br />
                                Tel :
                                <asp:Literal ID="ltrSiparisSabitTel" runat="server"></asp:Literal>
                                <br />
                            </div>
                            <%--      <div class="address">
                                    <span class="title">Fatura Adresi</span><br />
                                    <asp:Literal ID="ltrFaturaYazismaUnvan" runat="server"></asp:Literal>
                                    <br />
                                    <asp:Literal ID="ltrFaturaAdres" runat="server"></asp:Literal>
                                    <br />
                                    <asp:Literal ID="ltrFaturaIlce" runat="server"></asp:Literal>
                                    <br />
                                    <asp:Literal ID="ltrFaturaSehir" runat="server"></asp:Literal>
                                    <br />
                                    Gsm :
                                    <asp:Literal ID="ltrFaturaCepTel" runat="server"></asp:Literal>
                                    <br />
                                    Tel :
                                    <asp:Literal ID="ltrFaturaSabitTel" runat="server"></asp:Literal>
                                    <br />
                                </div>--%>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="invoiceinfo">
                            <!-- info  -->
                            <%--  <div class="info strong">
                                    GOLD BİLGİSAYAR OTOMASYON SİST.İNŞ.TURZ.GIDA.SAN.VE TİC.A.Ş.
                                    <br />
                                    Yukarı Dudullu Mahallesi Organize Sanayi Bölgesi 1. Cadde No:13/7 Ümraniye - İSTANBUL
                                    <br />
                                    Tel: (0216) 554 75 75
                                </div>--%>
                            <!-- info  -->
                            <!-- info  -->
                            <div class="info">
                                <!-- parameter -->
                                <%-- <div class="infoparameter">
                                        <div class="infolabel">
                                            ÖRNEK FATURA :
                                        </div>
                                        <div class="infodata">
                                            Büyük Mükellefler V.D. 3960488715
                                        </div>
                                    </div>--%>
                                <!-- parameter -->
                                <!-- parameter -->
                                <div class="infoparameter">
                                    <div class="infolabel">
                                        SİPARİŞ TARİHİ :
                                    </div>
                                    <div class="infodata">
                                        <asp:Literal ID="ltrSiparisTarih" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <!-- parameter -->
                                <!-- parameter -->
                                <div class="infoparameter">
                                    <div class="infolabel">
                                        SİPARİŞ SAATİ :
                                    </div>
                                    <div class="infodata">
                                        <asp:Literal ID="ltrSiparisSaat" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <!-- parameter -->
                                <!-- parameter -->
                                <div class="infoparameter">
                                    <div class="infolabel">
                                        SİPARİŞ NO :
                                    </div>
                                    <div class="infodata">
                                        <asp:Literal ID="ltrSiparisMasterId" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <!-- parameter -->
                                <!-- parameter -->
                                <div class="infoparameter">
                                    <div class="infolabel">
                                        KARGO :
                                    </div>
                                    <div class="infodata">
                                        <asp:Literal ID="ltrKargo" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <!-- parameter -->
                            </div>
                            <!-- info  -->
                            <!-- info  -->
                            <div class="info" id="info" runat="server" style="margin: 20px 0px 20px 0px;">
                                <span class="shipment">DİKKAT!
  <br />
                               
                                    Motul Hediye sitemizden, ürün özellikleri ve teknik detayları kontrol edilerek ve beğeniye istinaden sipariş edilen ürünlerin keyfi olarak iptal ya da değişimleri yapılamamaktadır.
                                    <br /> <br />
Ürünün orijinal ambalajı içerisinde, ürünle ilgili eksiklik veya hasar varsa teslim alındığı gün Motul Hediye adresindeki hediye sitemizin “İletişim” sayfasında bulunan iletişim formunu doldurarak iletmenizi rica ederiz. Aksi halde ürünle ilgili iade ve değişim yapılmamaktadır.
                                     <br /> <br />
Ürününüzün kutusunun taşımadan dolayı hasarlı olup olmadığını lütfen kontrol ediniz. Kutuda herhangi bir nedenle hasar var ise teslimatla ilgili hiç bir belgeyi imzalamadan kargo yetkilisine tutanak tutulması için kutunuzu iade ediniz. Size tarafımızdan yeni ürünleriniz derhal gönderilecektir. Kutusu hasarlı olan ürünlerin teslim alınması durumunda içindeki ürünlerin hasarından veya eksikliğinden ANADOLU SİGORTA ve Tutkal Bilişim sorumlu değildir.
                                     <br /> <br />
Beyaz Eşya gibi ağır ürünlerin, kargo taşımacılığı apartman kapısına kadar yapılmaktadır. Kata teslim, ev veya iş yeri içerisine teslim yapılmamaktadır.
 <br /> <br />
Beyaz Eşya, TV gibi kurulum gerektiren ürünlerin paketinin Yetkili Servis tarafından açılarak kurulumun Yetkili Servis tarafından yapılması gerekmektedir. Yetkili Servis tarafından açılmayan ve kurulumu yapılmayan ürünler garanti kapsamı dışında işlem göreceğinden iadesi ve/veya değişimi kabul edilmemektedir. Bu konuda detaylı bilgiyi ilgili ürünün üreticisinden alabilirsiniz
 <br /> <br />


                                </span>
                                </span>
                            </div>
                            <!-- info  -->
                        </div>
                    </div>
                </div>
                <script>
                    function PopUpAc(pencere, adres, width, height) {
                        window.open(adres, 'yeni', 'width=' + width + ',height=' + height + ',left = 150,top = 150,resizeable=yes,scrollbars=yes');
                        pencere.target = 'yeni';
                    }
                </script>
                <div class="row">
                    <div class="col-xs-12 col-md-offset-6 col-md-6">
                        <div id="onaydiveski" runat="server" style="float: right; margin-top: 10px;">
                            <div style="padding-left: 10px;">



                                <div class="modal fade" id="modal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content modal-content-four">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Kampanya Kuralları</h4>
                                            </div>
                                            <div class="modal-body">


                                            

                                                <p><strong>Kampanya Kuralları</strong></p>
<p><strong>NASIL HEDİYE SE&Ccedil;EBİLİRİM?</strong></p>
<p>Motul Team&rsquo;de puanlarınız ile her ay g&uuml;ncellenen ve yeni &uuml;r&uuml;nlerin eklendiği Motul Hediye kataloğu &uuml;zerinden istediğiniz hediyeyi sipariş edebilirsiniz.</p>
<p>&nbsp;</p>
<p><strong>HEDİYENİZİ NASIL ALACAKSINIZ?</strong></p>
<p>Siparişiniz sonrasında, hediyeleriniz kargo ile belirttiğiniz adrese ulaştırılacaktır. Siparişinizin takibini Motul Team hediye kataloğunda bulunan &ldquo;Siparişlerim&rdquo; b&ouml;l&uuml;m&uuml;nden ger&ccedil;ekleştirilebilirsiniz. Hediyelere ait kargo bedelleri Motul tarafından karşılanacaktır.</p>
<p>&nbsp;</p>
<p><strong>DİKKAT EDİLECEK HUSUSLAR</strong></p>
<ol>
<li>Motul tarafından hazırlanan &ldquo;Motul Team&rdquo; kampanyası 01.01.2021-31.12.2021 tarihleri arasında ge&ccedil;erlidir. Motul &ouml;nceden bildirim yapmadan ve herhangi bir neden g&ouml;stermeden bu kampanyayı durdurabilir veya uzatabilir.</li>
<li>G&ouml;nderilen t&uuml;m hediyelerin garanti belgeleri orijinal kutularının i&ccedil;erisinde veya &uuml;zerinde mevcuttur. Garanti belgeleri sonradan temin edilen evrak olmadığından dolayı, hediye paketini a&ccedil;tıktan sonra i&ccedil;inin ve dışının dikkatlice kontrol edilmesi &ouml;nemlidir.</li>
<li>&Uuml;r&uuml;n&uuml;n orijinal kutusu i&ccedil;inden &ccedil;ıkan Garanti Belgesi ve irsaliyenin atılmaması gerekmektedir. Bu belgeler garanti s&uuml;resi boyunca yaşayabileceğiniz arıza durumlarında yetkili servislere ibraz edilmesi gereken belgelerdir. Teslim aldığınız &uuml;r&uuml;nlerin arızalı olması durumunda, &uuml;r&uuml;n kullanım kılavuzundaki talimatlara g&ouml;re hareket edilmeli ve ilgili &uuml;r&uuml;n&uuml;n Yetkili Servisi'ne başvuruda bulunulmalıdır.</li>
<li>&Uuml;r&uuml;nlerin garanti s&uuml;releri irsaliye tarihi ile başlar; &uuml;reticinin belirlediği garanti s&uuml;resi ile sona erer.</li>
<li>Beyaz Eşya gibi ağır &uuml;r&uuml;nlerin, kargo taşımacılığı apartman kapısına kadar yapılmaktadır. Kata teslim, ev veya iş yeri i&ccedil;erisine teslim yapılmamaktadır.</li>
<li>Beyaz Eşya, TV gibi kurulum gerektiren &uuml;r&uuml;nlerin paketinin Yetkili Servis tarafından a&ccedil;ılarak kurulumun Yetkili Servis tarafından yapılması gerekmektedir. Yetkili Servis tarafından a&ccedil;ılmayan ve kurulumu yapılmayan &uuml;r&uuml;nler garanti kapsamı dışında işlem g&ouml;receğinden iadesi ve/veya değişimi kabul edilmemektedir. Bu konuda detaylı bilgiyi ilgili &uuml;r&uuml;n&uuml;n &uuml;reticisinden alabilirsiniz.</li>
<li>Yetkili Servis tarafından a&ccedil;ılmayan ve kurulumu yapılmayan bu t&uuml;r &uuml;r&uuml;nler (6. Madde) garanti kapsamı dışında işlem g&ouml;receğinden iadesi kabul edilmemektedir. Bu konuda detaylı bilgiyi ilgili &uuml;r&uuml;n&uuml;n &uuml;reticisinden alabilirsiniz.</li>
<li>Hediye Kataloğunda yer alan &uuml;r&uuml;nler stoklarla sınırlıdır. Se&ccedil;tiğiniz hediyenin teknik nedenlerden dolayı sağlanamaması ya da stoklarının t&uuml;kenmesi durumunda eş değerde bir hediye sunulacak veya Motul Team hediye kataloğu &uuml;zerinden yeni bir se&ccedil;im yapma imkanı sağlanacaktır.</li>
<li>Se&ccedil;ilen hediyeler Tutkal Bilişim tarafından temin edildikten sonra herhangi bir değişiklik yapılamaz. Se&ccedil;tiğiniz hediyenin herhangi bir nedenle sağlanamaması durumunda, benzer &ouml;zellikleri taşıyan muadil &uuml;r&uuml;n g&ouml;nderilir.</li>
</ol>
<ol>
<li>6. Madde&rsquo; de belirtilenler haricindeki &uuml;r&uuml;nler i&ccedil;in, teslimat sırasında, kargo yetkilisinden &uuml;r&uuml;n&uuml; teslim almadan &ouml;nce &uuml;r&uuml;n paketinin a&ccedil;ılarak hediyenizin sağlamlığı ve hasarlı olup olmadığı kontrol edilmelidir. Herhangi bir hasar tespit edildiğinde, &uuml;r&uuml;n&uuml; teslim eden kargo firması tarafından tutulacak hasar tespit tutanağı ile birlikte &uuml;r&uuml;n iade edilmelidir. Teslim alındıktan sonra fark edilen hasarlı veya eksik &uuml;r&uuml;nlerin sorumluluğu Motul Hediye ve/veya Tutkal Bilişim&rsquo; e ait değildir.</li>
<li>Hediye puanlar nakde &ccedil;evrilemez.</li>
<li>Se&ccedil;ilen hediyelerin teslim adresleri T&uuml;rkiye Cumhuriyeti sınırları i&ccedil;erisinde olmalıdır.</li>
<li>Hediyelerin teslimat s&uuml;resi 10 iş g&uuml;n&uuml;d&uuml;r.</li>
<li>Her t&uuml;rl&uuml; anlaşmazlık halinde Motul Hediye ve Tutkal Bilişim&rsquo; in kayıtları kesin delil olarak esas alınacaktır.</li>
<li>Katalog baskısında oluşabilecek Tipografik ve baskı hatalarından kaynaklanan sorunlardan dolayı Motul Hediye ve/veya Tutkal Bilişim sorumlu değildir.</li>
<li>G&ouml;rseller temsilidir. G&uuml;ncel stok ve puan bilgilerini Motul Team hediye kataloğundan inceleyebilirsiniz.</li>
<li>&Uuml;r&uuml;nler stoklarla sınırlıdır. Motul, &ouml;nceden haber vermeden &uuml;r&uuml;nleri ve &uuml;r&uuml;n puanlarını değiştirme hakkı saklıdır.</li>
<li>Motul Team hediye kataloğundan hediye siparişi verilmesi, burada yer alan t&uuml;m kampanya şartlarının kabul&uuml; anlamına gelir.</li>
</ol>
<p><strong>&nbsp;</strong></p>
<p>&mdash; Hediye &uuml;r&uuml;n teslimatlarında; kargo teslim fişini imzalamadan &ouml;nce &uuml;r&uuml;n&uuml;n kargo kolisinde (&uuml;r&uuml;n&uuml;n orijinal kolisinin i&ccedil;ine konduğu dış koli) herhangi bir hasar olup olmadığını kontrol ediniz.&nbsp;<strong>Kargo kolisi hasarlı olan hediyelerinizi teslim almayıp</strong>, kargo g&ouml;revlisine iade etmenizi rica ederiz.</p>
<p><br />&mdash; Beyaz Eşya, TV gibi kurulum gerektiren &uuml;r&uuml;n teslimatlarında; Lojistik firması tarafından d&uuml;zenlenen teslim fişini imzalamadan &ouml;nce &uuml;r&uuml;n&uuml;n dış ambalajında ve kolisinde (&uuml;r&uuml;n&uuml;n orijinal kolisinin i&ccedil;ine konduğu dış koli) herhangi bir hasar olup olmadığını lojistik firma yetkilileriyle birlikte kontrol ediniz.&nbsp;<strong>Dış ambalajında hasarlı olan hediyelerinizi teslim almayıp</strong>, lojistik firma g&ouml;revlisine iade etmenizi rica ederiz.<br /><br />&mdash; Beyaz Eşya, TV gibi kurulum gerektiren &uuml;r&uuml;nlerin paketinin Yetkili Servis tarafından a&ccedil;ılarak kurulumun Yetkili Servis tarafından yapılması gerekmektedir.&nbsp;<strong>Yetkili Servis tarafından a&ccedil;ılmayan ve kurulumu yapılmayan &uuml;r&uuml;nler garanti kapsamı dışında işlem g&ouml;receğinden iadesi ve/veya değişimi kabul edilmemektedir.</strong>&nbsp;Bu konuda detaylı bilgiyi ilgili &uuml;r&uuml;n&uuml;n &uuml;reticisinden alabilirsiniz.<br /><br />&mdash; Şayet &uuml;r&uuml;n&uuml; teslim almak istiyorsanız, lojistik g&ouml;revlisinin yanında &uuml;r&uuml;n&uuml;n sağlamlığını kontrol etmeniz gerekmektedir. Kontrol sonrasında kolide herhangi bir hasarla karşılaşırsanız (ambalaj ezilmesi, yıpranma, yırtılma, ezilme vb.) mutlaka lojistik g&ouml;revlisinde bulunan&nbsp;<strong>hasar tespit tutanağının doldurulmasını talep ediniz</strong>&nbsp;ve &uuml;r&uuml;n&uuml; teslim almayınız.</p>
<p><br />&mdash; Teslim aldığınız kargonun hasarlı &ccedil;ıkması durumunda&nbsp;<strong>aynı g&uuml;n</strong>&nbsp;fotoğraflarını &ccedil;ekmenizi ve konuyu 1 iş g&uuml;n&uuml; i&ccedil;erisinde tarafımıza iletmenizi rica ederiz. 1 iş g&uuml;n&uuml; ge&ccedil;en hasarlı &uuml;r&uuml;nler i&ccedil;in maalesef işlem yapılamayacaktır.</p>
<p><br />&mdash;&nbsp;<strong>Yukarıda bildirilen y&ouml;nlendirmelerden hi&ccedil;birisini yapmadıysanız ne yazık ki &uuml;r&uuml;n&uuml;n&uuml;zle ilgili iade ve değişim yapılamayacak ve bu durumdan Motul ve Tutkal Bilişim Hizmetleri sorumlu tutulamayacaktır.</strong></p>
<p><br />&mdash; Beyaz Eşya gibi ağır &uuml;r&uuml;nlerin, kargo taşımacılığı&nbsp;<strong>apartman kapısına kadar</strong>&nbsp;yapılmaktadır. Kata teslim, ev veya iş yeri i&ccedil;erisine teslim yapılmamaktadır.</p>
<p><br />&mdash;&Uuml;r&uuml;n&uuml;n orijinal ambalajı i&ccedil;erisinde, &uuml;r&uuml;nle ilgili&nbsp;<strong>eksiklik veya hasar varsa teslim alındığı g&uuml;n</strong>&nbsp; hediye sitemizin &ldquo;İletişim&rdquo; sayfasında bulunan iletişim formunu doldurarak iletmenizi rica ederiz. Aksi halde &uuml;r&uuml;nle ilgili iade ve değişim yapılmamaktadır.</p>
<p><br />&mdash; &Uuml;r&uuml;n&uuml;n&nbsp;<strong>Garanti Belgesi'nin atılmaması</strong>&nbsp;gerekmektedir. Teslim aldığınız &uuml;r&uuml;nlerin &ccedil;alışmaması durumunda, &uuml;r&uuml;n kullanım kılavuzundaki talimatlara g&ouml;re hareket etmenizi rica ederiz. &Ccedil;alışmayan &uuml;r&uuml;nler i&ccedil;in ilgili &uuml;r&uuml;n&uuml;n Yetkili Servisi'ne başvuruda bulunabilirsiniz.<br /><br />&mdash; Hediyenin talep ettiğiniz hediyeden farklı &ccedil;ıkması durumunda kargoyu almış olduğunuz tarihten itibaren&nbsp;<strong>14 g&uuml;n i&ccedil;inde</strong>&nbsp;orijinal ambalajıyla ve iade sebebini belirterek aşağıdaki adrese g&ouml;ndermeniz gerekmektedir;<br /><br /><br /></p>
<p><strong>Not: Arızalı &ccedil;ıkan hediyeler i&ccedil;in l&uuml;tfen ilgili firmanın Yetkili Servisi'ni arayınız.</strong></p>
<p><strong><br />Tutkal Bilişim Hizmetleri A.Ş.<br />Adil Mah. Gazanfer Sok. No:6/A Sultanbeyli İstanbul<br /><br />&mdash; Değerli &Uuml;yemiz; Talep etmiş olduğunuz &uuml;r&uuml;n teknik servis kurulumu gerektiren bir ise ve &uuml;r&uuml;n&uuml;n&uuml;z&uuml;n kurulumunu ileriki bir tarihte yaptıracaksanız; &uuml;r&uuml;n&uuml; hasarsız şekilde teslim aldıktan sonra kurulum tarihine kadar &uuml;r&uuml;n&uuml; saklama koşullarından sorumlu olmaktasınız. Kurulum tarihinde &uuml;r&uuml;nde herhangi bir hasar v.b. sorun ile karşılaştığınızda &uuml;z&uuml;lerek, Isıcam ve Tutkal Bilişim&rsquo;in sorumlu olmayacağını belirtmek isteriz.</strong></p>
<p>&nbsp;</p>
 


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div id="onaydiv" runat="server">
                            <div>
                                <p data-toggle="modal" data-target="#modal4" style="cursor:pointer;" >
                                    <img class="img responsive" src="imagesgld/bullet_red6x6.png" width="6" height="6" style="vertical-align: middle;">
                                    &nbsp;Uygulama ve Koşulları okumak için tıklayınız!</p>
                                <br />
                            </div>
                            <div style="padding: 10px; background: #f5f5f5;">
                                <asp:CheckBox ID="chkUygulamaKosullar" runat="server" />
                                Uygulama ve Koşulları okudum, kabul ediyorum.

                                 <asp:Label ID="chkUyari" runat="server" Text="" />
                            </div>

                        </div>
                        <div style="padding-left: 10px;">


                            <span class="colord8" style="font-size: 12px; cursor:pointer;  ">
                                <asp:CustomValidator ID="CustomValidatorSozlesme" runat="server" ClientValidationFunction='ValidateSozlesme'
                                    OnServerValidate="CustomValidatorSozlesme_ServerValidate" ValidationGroup="grpSiparisOnay"
                                    ErrorMessage="&raquo; Lütfen Uygulama ve Koşulları kabul edin." ValidateEmptyText="True"></asp:CustomValidator>
                            </span>
                        </div>
                        <div style="float: right; padding-top: 10px;">
                            <asp:LinkButton ID="btnGonder" runat="server" CssClass="btn btnInput"
                                OnClick="btnOnayla_Click"><span class="spriteRedBtn fontTre fontSz14">
                                             Siparişi Onayla</span></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt10">
                <div class="col-lg-12">
                    <div class="order-detail-content">
                        <table class="table table-bordered table-responsive cart_summary" style="margin-bottom: 0px !important;">
                            <asp:Literal ID="ltrSiparisDetaylari" runat="server"></asp:Literal>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row mt10">
                <div class="col-md-offset-9 col-md-3">
                    <div style="float: right;">
                        <span>Genel Toplam&nbsp;:&nbsp;<asp:Literal ID="ltrGenelToplam" runat="server"></asp:Literal>&nbsp;PUAN</span>
                    </div>
                </div>
            </div>
            <div class="row mt10">
                <div class="col-lg-12">
                    <div class="order-detail-content">
                        <table class="table table-bordered table-responsive cart_summary" style="margin-bottom: 0px !important;">
                            <asp:Literal ID="ltrOdemeDetaylari" runat="server"></asp:Literal>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">

        var chkSozlesmeId = '<%=chkUygulamaKosullar.ClientID %>';

        function ValidateSozlesme(obj, args) {
            var checkbox = $("#" + chkSozlesmeId); args.IsValid = checkbox.attr('checked');
        }

        function mail_gonder() {

            $.ajax({

                type: 'POST', timeout: 1000, async: false, url: '/SiparisOnay.aspx/SiparisMailGonder',
                data: {},
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (veri) {

                }, error: function (xhr) { console.log(xhr.responseText); }
            })
        }

        <%
        if (Request.QueryString["status"] != null)
        {
            Response.Write("mail_gonder();");
        }
        %>


    </script>
</asp:Content>
