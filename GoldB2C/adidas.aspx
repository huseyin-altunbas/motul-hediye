﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master" AutoEventWireup="true" CodeFile="adidas.aspx.cs" Inherits="adidas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" Runat="Server">

        <style>
        .breadcrumb {
            visibility: hidden;
        }

        #left_column {
            width: 0px;
        }

        .left-module, .page-heading {
            visibility: hidden;
        }

        .mt10 {
            margin-top: -55px;
        }
    </style>

    <div class="mt10">
 <h1 style="text-align: center;"><span style="color: #800000;"><strong>1 Kasım&rsquo; a kadar oluşturacağınız her siparişe %15 Adidas indirim kodu hediye!</strong></span></h1>
        <br />
         <br />
<p><strong>Kampanya Koşulları;  </strong></p>
<ul>
<p style="text-align: center;">&nbsp;</p>
<ul>
<li><strong>1 Kasım 2018</strong> tarihine kadar oluşturacağınız her siparişe <strong>%15</strong> Adidas indirim kodu hediyedir.</li>
<li>5 Kasım 2018 tarihine kadar<strong> shop.adidas.com.tr</strong> sitesinden yapacağınız t&uuml;m yeni sezon alışverişinizde <strong>%15</strong> indirim sağlanacaktır.</li>
<li>shop.adidas.com.tr online mağaza &uuml;zerinde ise, &ouml;deme ekranın &uuml;zerindeki "<strong><span style="color: #800000;">Kupon kodu giriniz</span></strong>" alanına indirim kodunu girerek ilgili indirimden yararlanabilirsiniz.</li>
<li>Her tanımlanan indirim kodu ile kampanyadan 1 kez faydalanılabilir ve Alexander Wang ve Yeezy koleksiyonlarında ge&ccedil;erli değildir.</li>
<li>Bir faturada sadece bir indirim kodu kullanılabilir.</li>
<li>Adidas kampanya koşullarında değişiklik hakkını saklı tutar.</li>
</ul>
     <br />
   <img src="Images/adidas.jpg" />
    </div>



</asp:Content>

