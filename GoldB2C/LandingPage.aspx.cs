﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Xml;
using System.Data;

public partial class LandingPage : System.Web.UI.Page
{
    LandingPageBLL bllLandingPage = new LandingPageBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        string LandingHtml = "";
        string LandingStyle = "";

        int DetayId = 0;
        if (!string.IsNullOrEmpty(Request.QueryString["DetayId"]) && int.TryParse(Request.QueryString["DetayId"], out DetayId))
        {
            LandingHtml = bllLandingPage.GetLandingPage(DetayId);
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.InnerXml = LandingHtml;

            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(XmlDoc));

            lblLandingBaslik.Text = ds.Tables[0].Rows[0]["baslik"].ToString();
            ltrLandingIcerik.Text = ds.Tables[0].Rows[0]["icerik"].ToString();
            LandingStyle = ds.Tables[0].Rows[0]["style"].ToString();

        }
        else
            Response.Redirect("~/Default.aspx", false);



        ltrCssClass.Text = "<style type=\"text/css\" rel=\"stylesheet\">" + LandingStyle + "</style>";
        this.Page.Header.Controls.Add(ltrCssClass);
    }
}