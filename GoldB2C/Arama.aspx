﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="Arama.aspx.cs" Inherits="Arama" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="columns-container">
        <div class="container" id="columns">
            <!-- breadcrumb -->
            <div class="breadcrumb clearfix">
                <a class="home" href="/" title="Return to Home">Ana Sayfa</a> <span class="navigation-pipe">
                    &nbsp;</span> <span class="navigation_page">Arama</span>
            </div>
            <!-- ./breadcrumb -->
            <!-- row -->
            <div class="row">
                <!-- Center colunm-->
                <div class="center_column col-xs-12 col-sm-12" id="center_column">
                    <!-- view-product-list-->
                    <div id="view-product-list" class="view-product-list">
                        <h2 class="page-heading">
                            <span class="page-heading-title">Arama Sonuçları</span>
                        </h2>
                        <ul class="display-product-option">
                            <li class="view-as-grid selected"><span>grid</span> </li>
                            <li class="view-as-list"><span>list</span> </li>
                        </ul>
                        <!-- PRODUCT LIST -->
                        <ul class="row product-list grid">
                            <asp:Repeater ID="rptUrunler" runat="server" OnItemDataBound="rptUrunler_ItemDataBound">
                                <ItemTemplate>
                                    <li class="col-sx-12 col-sm-3">
                                        <div class="product-container">
                                            <div class="left-block">
                                                <a href="<%# DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%# DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                                                    <img src="<%# DataBinder.Eval(Container.DataItem, "resim_ad") %>" width="200" height="200"
                                                        alt="<%# DataBinder.Eval(Container.DataItem, "resim_ad") %>" /></a>
                                                <%--<div class="quick-view">
                                                    <a title="Add to my wishlist" class="heart" href="#"></a><a title="Add to compare"
                                                        class="compare" href="#"></a><a title="Quick view" class="search" href="#"></a>
                                                </div>--%>
                                                <asp:Literal ID="ltrSepeteEkle" runat="server"></asp:Literal>
                                            </div>
                                            <div class="right-block">
                                                <h5 class="product-name">
                                                    <a href="<%# DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%# DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                                                        <%# DataBinder.Eval(Container.DataItem, "urun_ad") %></a></h5>
                                                <%--<div class="product-star">
                                                    <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                                        class="fa fa-star"></i><i class="fa fa-star-half-o"></i>
                                                </div>--%>
                                                <div class="content_price">
                                                    <asp:Label ID="lblNetTutar" runat="server" Text="" CssClass="price product-price"></asp:Label>
                                                    <asp:Label ID="lblBrutTutar" runat="server" Text="" CssClass="price old-price"></asp:Label>
                                                    <asp:Label ID="lblBilgi" runat="server" Text="" CssClass="notAvailable"></asp:Label>
                                                </div>
                                                <div class="info-orther">
                                                    <div class="product-desc">
                                                        <%# DataBinder.Eval(Container.DataItem, "aciklama") %>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                        <!-- ./PRODUCT LIST -->
                    </div>
                    <!-- ./view-product-list-->
                </div>
                <!-- ./ Center colunm -->
            </div>
            <!-- ./row-->
        </div>
    </div>
</asp:Content>
