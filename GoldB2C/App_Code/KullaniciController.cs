﻿using ClosedXML.Excel;
using Goldb2cBLL;
using Goldb2cBLL.Custom;
using Goldb2cEntity;
using Goldb2cEntity.Custom;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

[ValidateModelAdminAttribute]
public class KullaniciController : ApiController
{
    UyelikIslemleriBLL uyelikIslemleriBLL = new UyelikIslemleriBLL();

    [HttpPost]
    public DataSourceResult Get(View filters)
    {
        List<Kullanici> list = uyelikIslemleriBLL.Get(filters);

        int totalRows = 0;
        if (list.Count > 0)
        {
            totalRows = list[0].totalRows;
        }

        return new DataSourceResult { Data = list, Total = totalRows };

    }


    [HttpGet]
    public HttpResponseMessage Export(string filter)
    {
        string options = JsonConvert.DeserializeObject(HttpUtility.UrlDecode(filter)).ToString();
        View filters = JsonConvert.DeserializeObject<View>(options);
        filters.Take = int.MaxValue;

        DataTable dt = uyelikIslemleriBLL.GetDataTable(filters);
        dt.Columns["CRMCustomerGUID"].ColumnName = "Üye Telefon";
        dt.Columns.Remove("totalRows");


        var stream = new MemoryStream();
        using (XLWorkbook wb = new XLWorkbook())
        {
            wb.Worksheets.Add(dt);
            wb.SaveAs(stream);
        }

        var result = new HttpResponseMessage(HttpStatusCode.OK)
        {
            Content = new ByteArrayContent(stream.ToArray())
        };
        result.Content.Headers.ContentDisposition =
            new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "Acente Listesi.xlsx"
            };
        result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-msdownload");

        return result;



    }



}
