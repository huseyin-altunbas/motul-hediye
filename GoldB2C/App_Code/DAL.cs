using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public class DAL : IDisposable
{
    private SqlConnection con = null;

    public enum pdirection
    {

        Input, OutPut
    }

    public enum commandttype
    {
        Text, StoredProcedure

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="constr">connection string </param>
    public DAL(string baglan)
    {
        try
        {
            con = new SqlConnection(baglan);
        }
        catch (Exception ex1)
        {
            System.Web.UI.Control c = new Control();

            throw new hata("Ba�lant� Hatas�");

            //System.Web.HttpContext.Current.Session["BaglantiHatasi"] = "0";

            //System.Web.HttpContext.Current.Response.Redirect(c.ResolveClientUrl("~/BAGLANTI-HATASI/"));
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public void OpenCon()
    {
        try
        {
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }

        }
        catch (Exception ex1)
        {
            System.Web.UI.Control c = new Control();

            throw new hata("Ba�lant� Hatas�");

            //System.Web.HttpContext.Current.Session["BaglantiHatasi"] = "0";

            //System.Web.HttpContext.Current.Response.Redirect(c.ResolveClientUrl("~/BAGLANTI-HATASI/"));
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void CloseCon()
    {
        if (con != null)
        {
            con.Close();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pname"> parametre ad�</param>
    /// <param name="type"> sqldbtype</param>
    /// <param name="size">0 olursa size eklenmiyor</param>
    /// <param name="p">Dal.pdirection input yada Output</param>
    /// <param name="value"> parametre de�eri</param>
    /// <returns></returns>
    public SqlParameter SetParameter(string pname, SqlDbType type, int size, pdirection p, object value)
    {
        SqlParameter prm = null;
        try
        {
            if (size == 0)
            {
                prm = new SqlParameter(pname, type);
            }
            else
            {
                prm = new SqlParameter(pname, type, size);
            }

            prm.Value = value;

            if (p == pdirection.OutPut)
            {
                prm.Direction = ParameterDirection.Output;
            }
        }
        catch (Exception ex1)
        {
            throw new hata("Parametre Hatas�");
        }

        return prm;
    }

    public SqlCommand GetCommand(string commandname, SqlParameter[] prms, commandttype type)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Parameters.Clear();
        cmd.Connection = con;
        cmd.CommandText = commandname;
        if (type == commandttype.StoredProcedure)
        {
            cmd.CommandType = CommandType.StoredProcedure;
        }
        else
        {
            cmd.CommandType = CommandType.Text;
        }
        if (prms != null)
        {
            for (int i = 0; i < prms.Length; i++)
            {
                cmd.Parameters.Add(prms[i]);
            }
        }

        return cmd;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="commandname">sotred procedure de�ilse commandtext </param>
    /// <param name="prms">parametreler set parameter ile gelecek</param>
    /// <param name="type">Stored procedure or Text</param>
    /// <returns></returns>
    public int executecmd(string commandname, SqlParameter[] prms, commandttype type)
    {
        int a = 0;
        try
        {
            OpenCon();
            SqlCommand cmd1 = GetCommand(commandname, prms, type);
            a = cmd1.ExecuteNonQuery();
            cmd1.Parameters.Clear();
            CloseCon();
            cmd1.Dispose();
        }
        catch (Exception ex1)
        {
            throw new hata(ex1.Message);
        }
        return a;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="commandname">sotred procedure de�ilse commandtext </param>
    /// <param name="prms">parametreler set parameter ile gelecek</param>
    /// <param name="type">Stored procedure or Text</param>
    /// <returns></returns>
    public int executecmdScopeIdentity(string commandname, SqlParameter[] prms, commandttype type)
    {
        int a = 0;
        try
        {
            OpenCon();
            SqlCommand cmd1 = GetCommand(commandname, prms, type);
            a = int.Parse(cmd1.ExecuteScalar().ToString());
            cmd1.Parameters.Clear();
            CloseCon();
            cmd1.Dispose();
        }
        catch (Exception ex1)
        {
            throw new hata("Scope Identity Komutu �al��t�r�lamad�");
        }
        return a;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="commandname">command name yada texti</param>
    /// <param name="prms"></param>
    /// <param name="type"></param>
    /// <param name="home">sayfalamada baslang�c </param>
    /// <param name="count">sayfalamada say�</param>
    /// <param name="tablename">tabload�</param>
    /// <returns></returns>
    public DataSet getdataset(string commandname, SqlParameter[] prms, commandttype type, int home, int count, string tablename)
    {
        DataSet ds = new DataSet();

        try
        {
            OpenCon();
            SqlCommand cmd2 = GetCommand(commandname, prms, type);
            using (SqlDataAdapter adp = new SqlDataAdapter(cmd2))
            {
                if (count == 0)
                {
                    adp.Fill(ds);
                }
                else
                {
                    adp.Fill(ds, home, count, tablename);
                }
                cmd2.Parameters.Clear();
                cmd2.Dispose();
            }
            CloseCon();
        }
        catch (Exception ex1)
        {
            throw new hata("DataSet hatas�");
        }

        return ds;
    }

    public SqlDataReader getreader(string commandname, SqlParameter[] prms, commandttype type)
    {
        SqlDataReader rd;
        OpenCon();
        SqlCommand cmd2  = GetCommand(commandname, prms, type);
        rd = cmd2.ExecuteReader();
        cmd2.Parameters.Clear();
        // closecon();
        return rd;
    }

    public DataTable getdatatable(string commandname, SqlParameter[] prms, commandttype type)
    {
        SqlDataReader rd;
        OpenCon();
        SqlCommand cmd2 = GetCommand(commandname, prms, type);
        rd = cmd2.ExecuteReader();
        cmd2.Parameters.Clear();

        DataTable dt = new DataTable();
        dt.Load(rd);
        rd.Close();
        rd.Dispose();

        // closecon();
        return dt;
    }


    public void Dispose()
    {
        con.Dispose();

        GC.SuppressFinalize(this);
    }
}
