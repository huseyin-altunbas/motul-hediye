﻿using Goldb2cEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using Kendo.DynamicLinq;
using KendoFilterCustom;
using System.IO;
using System.Xml.Serialization;
using System.Text;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.CodeDom;
using Goldb2cBLL.Custom;
using Goldb2cBLL;
using Goldb2cEntity.Custom;

/// <summary>
/// Summary description for Helper
/// </summary>
public static class Helper
{


    public static bool SepetteAkaryakitUrunuVarmi(int ErpCariId, int product)
    {
        bool kontrolYapilsinmi = false;
        if (product == 20005638)
        {
            kontrolYapilsinmi = true;
        }
        else if (product == 20005637)
        {
            kontrolYapilsinmi = true;
        }

        else if (product == 20022463)
        {
            kontrolYapilsinmi = true;
        }
        else if (product == 20022464)
        {
            kontrolYapilsinmi = true;
        }
        else if (product == 20022465)
        {
            kontrolYapilsinmi = true;
        }
        else if (product == 20022466)
        {
            kontrolYapilsinmi = true;
        }


        if (!kontrolYapilsinmi)
        {
            return false;
        }

        SepetIslemleriBLL bllSepetIslemleri = new SepetIslemleriBLL();
        DataSet dsSepetIcerik = new DataSet();
        dsSepetIcerik = bllSepetIslemleri.SepetGoruntule(ErpCariId);

        DataRow[] dtSepetDetay = dsSepetIcerik.Tables["SepetDetayTT"].Select(" urun_link like'%20005638%' or urun_link like'%20005637%' or urun_link like'%20022463%'  or urun_link like'%20022464%'  or urun_link like'%20022465%'  or urun_link like'%20022466%' ");

        if (dtSepetDetay.Count() > 0)
        {
            return true;
        }
        return false;
    }

    public static DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);

        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }


    public static int PuanSorgula()
    {
        TransactionBLL transactionBLL = new TransactionBLL();
        int result = 0;
        try
        {
            if (HttpContext.Current.Session["Kullanici"] != null)
            {
                Kullanici Kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];
                PuanView puanView = transactionBLL.KullaniciPuanGetir(Kullanici.KullaniciId);

                HttpContext.Current.Session["totalGoldPoint"] = puanView.kalan;
                result = puanView.kalan;
            }
            else
            {
                //HttpContext.Current.Response.Redirect("/",true);
            }
        }
        catch (Exception)
        {
        }

        return result;
    }

    public static List<Dictionary<string, object>> DataTableToDictionaryList(DataTable dt)
    {
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row;
        foreach (DataRow dr in dt.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in dt.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }

        return rows;

    }

    public static KendoSqlFilterHelper KendoSqlFilterHelperAdd<T>(Kendo.DynamicLinq.Filter filter, string NameCondition, T tip)
    {

        KendoSqlFilterHelper _KendoSqlFilterHelper = new KendoSqlFilterHelper(filter, NameCondition);
        foreach (PropertyInfo propertyInfo in tip.GetType().GetProperties())
        {
            try
            {
                _KendoSqlFilterHelper.AddFilterPredicate(propertyInfo.Name);
            }
            catch (Exception ex)
            {
            }
        }

        return _KendoSqlFilterHelper;
    }

    //public static Tuple<List<SqlParameter>, string> KendoWhereConditions(List<Kendo.DynamicLinq.Filter> filter)
    //{
    //    List<SqlParameter> list = new List<SqlParameter>();
    //    string sqlWhere = "";
    //    foreach (Kendo.DynamicLinq.Filter itemFilter in filter)
    //    {

    //        string _operator = "";
    //        switch (itemFilter.Operator)
    //        {
    //            case "eq": _operator = "="; break;
    //            case "gte": _operator = ">"; break;
    //            case "lte": _operator = "<"; break;
    //            case "contains": _operator = "like"; break;
    //            case "startswith": _operator = "="; break;
    //            case "endswith": _operator = "="; break;
    //            default: _operator = itemFilter.Operator; break;
    //        }
    //        if (_operator == "like")
    //        {
    //            //sqlWhere += " " + itemFilter.Field + " like '%" + itemFilter.Value + "%' And";
    //            sqlWhere += " " + itemFilter.Field + " like'%" + itemFilter.Value.ToString().Replace("T21:00:00.000Z", "") + "%' and";
    //        }
    //        else
    //        {
    //            //sqlWhere += " " + itemFilter.Field;
    //            //sqlWhere += _operator;
    //            //sqlWhere += "'" + itemFilter.Value + "' And";
    //            if (_operator == "=")
    //            {
    //                itemFilter.Value = "'" + itemFilter.Value.ToString().Replace("T21:00:00.000Z", "") + "'";
    //            }
    //            sqlWhere += " " + itemFilter.Field + _operator + "'" + itemFilter.Value.ToString().Replace("T21:00:00.000Z", "") + "'" + " and";
    //        }

    //        list.Add(new SqlParameter(itemFilter.Field, "'" + itemFilter.Value.ToString().Replace("T21:00:00.000Z" + "'", "")));
    //        //sqlSearch += " and (Certificate.InvoiceNumber=@InvoiceNumber ";


    //    }
    //    if (sqlWhere.Length > 5)
    //    { sqlWhere = sqlWhere.Remove(sqlWhere.Length - 3); }

    //    sqlWhere = sqlWhere.Replace("''", "'");
    //    sqlWhere = sqlWhere.Replace("\"", "'");
    //    return Tuple.Create(list, sqlWhere);
    //}

    //public static string KendoOrderBy(IEnumerable<Sort> sort)
    //{
    //    string result = "";
    //    foreach (Sort item in sort)
    //    {
    //        result += " " + item.Field + " " + item.Dir + ",";
    //    }

    //    result = result.Remove(result.Length - 1);
    //    return result;
    //}


    public static string ToXML<T>(T obj)
    {
        using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            xmlSerializer.Serialize(stringWriter, obj);
            return stringWriter.ToString();
        }
    }


    public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()
    {
        try
        {
            List<T> list = new List<T>();

            foreach (var row in table.AsEnumerable())
            {
                T obj = new T();

                foreach (var prop in obj.GetType().GetProperties())
                {
                    try
                    {
                        PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                    }
                    catch
                    {
                        continue;
                    }
                }

                list.Add(obj);
            }

            return list;
        }
        catch
        {
            return null;
        }
    }

    public static List<T> DataRowsToList<T>(this DataRow[] table) where T : class, new()
    {
        try
        {
            List<T> list = new List<T>();

            foreach (var row in table.AsEnumerable())
            {
                T obj = new T();

                foreach (var prop in obj.GetType().GetProperties())
                {
                    try
                    {
                        PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                    }
                    catch
                    {
                        continue;
                    }
                }

                list.Add(obj);
            }

            return list;
        }
        catch
        {
            return null;
        }
    }


    public static void createType(string name, IDictionary<string, Type> props)
    {
        var csc = new CSharpCodeProvider(new Dictionary<string, string>() { { "CompilerVersion", "v4.0" } });
        var parameters = new CompilerParameters(new[] { "mscorlib.dll", "System.Core.dll" }, "Test.Dynamic.dll", false);
        parameters.GenerateExecutable = false;

        var compileUnit = new CodeCompileUnit();
        var ns = new CodeNamespace("Test.Dynamic");
        compileUnit.Namespaces.Add(ns);
        ns.Imports.Add(new CodeNamespaceImport("System"));

        var classType = new CodeTypeDeclaration(name);
        classType.Attributes = MemberAttributes.Public;
        ns.Types.Add(classType);

        foreach (var prop in props)
        {
            var fieldName = "_" + prop.Key;
            var field = new CodeMemberField(prop.Value, fieldName);
            classType.Members.Add(field);

            var property = new CodeMemberProperty();
            property.Attributes = MemberAttributes.Public | MemberAttributes.Final;
            property.Type = new CodeTypeReference(prop.Value);
            property.Name = prop.Key;
            property.GetStatements.Add(new CodeMethodReturnStatement(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), fieldName)));
            property.SetStatements.Add(new CodeAssignStatement(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), fieldName), new CodePropertySetValueReferenceExpression()));
            classType.Members.Add(property);
        }

        var results = csc.CompileAssemblyFromDom(parameters, compileUnit);
        results.Errors.Cast<CompilerError>().ToList().ForEach(error => Console.WriteLine(error.ErrorText));
    }

    public static DataSet test()
    {
        Goldb2cBLL.SiparisGoruntulemeBLL bllSiparisGoruntuleme = new Goldb2cBLL.SiparisGoruntulemeBLL();
        System.Data.DataSet dsSiparis = bllSiparisGoruntuleme.GenelSiparisListeleme(0, 0, 0, 0, Convert.ToDateTime("01.01.1970"), Convert.ToDateTime("01.01.2900"));
        return dsSiparis;
    }



    public static StringBuilder ConvertDataTableToCsvFile(DataTable dtData)
    {
        StringBuilder data = new StringBuilder();

        //Taking the column names.
        for (int column = 0; column < dtData.Columns.Count; column++)
        {
            //Making sure that end of the line, shoould not have comma delimiter.
            if (column == dtData.Columns.Count - 1)
                data.Append(dtData.Columns[column].ColumnName.ToString().Replace(",", ";"));
            else
                data.Append(dtData.Columns[column].ColumnName.ToString().Replace(",", ";") + ',');
        }

        data.Append(Environment.NewLine);//New line after appending columns.

        for (int row = 0; row < dtData.Rows.Count; row++)
        {
            for (int column = 0; column < dtData.Columns.Count; column++)
            {
                ////Making sure that end of the line, shoould not have comma delimiter.
                if (column == dtData.Columns.Count - 1)
                    data.Append(dtData.Rows[row][column].ToString().Replace(",", ";"));
                else
                    data.Append(dtData.Rows[row][column].ToString().Replace(",", ";") + ',');
            }

            //Making sure that end of the file, should not have a new line.
            if (row != dtData.Rows.Count - 1)
                data.Append(Environment.NewLine);
        }
        return data;
    }

    //This method saves the data to the csv file. 
    public static void SaveData(StringBuilder data, string filePath)
    {
        using (StreamWriter objWriter = new StreamWriter(filePath))
        {
            objWriter.WriteLine(data);
        }
    }
}    
