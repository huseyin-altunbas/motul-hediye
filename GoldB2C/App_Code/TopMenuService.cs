﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using Goldb2cBLL;

/// <summary>
/// Summary description for TopMenuService
/// </summary>
[System.Web.Script.Services.ScriptService()]
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class TopMenuService : System.Web.Services.WebService
{

    public TopMenuService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region MenuItem
    public string item_1 = "";
    public string item_2 = "";
    public string item_3 = "";
    public string item_4 = "";
    public string item_5 = "";
    public string item_6 = "";
    public string item_7 = "";
    public string item_8 = "";

    public string item_1_name = "";
    public string item_2_name = "";
    public string item_3_name = "";
    public string item_4_name = "";
    public string item_5_name = "";
    public string item_6_name = "";
    public string item_7_name = "";
    public string item_8_name = "";

    public string item_1_AllSubCategoryHtml = "";
    public string item_2_AllSubCategoryHtml = "";
    public string item_3_AllSubCategoryHtml = "";
    public string item_4_AllSubCategoryHtml = "";
    public string item_5_AllSubCategoryHtml = "";
    public string item_6_AllSubCategoryHtml = "";
    public string item_7_AllSubCategoryHtml = "";
    public string item_8_AllSubCategoryHtml = "";

    public string item_1_Url = "";
    public string item_2_Url = "";
    public string item_3_Url = "";
    public string item_4_Url = "";
    public string item_5_Url = "";
    public string item_6_Url = "";
    public string item_7_Url = "";
    public string item_8_Url = "";

    #endregion


    [WebMethod]
    public List<string> GetMenuItemHtml()
    {
        List<string> MenuItemHtml = new List<string>();

        DataTable TopMenuItem = new DataTable();
        TopMenuBLL topMenuBLL = new TopMenuBLL();
        TopMenuItem = topMenuBLL.SelectTopMenu();

        item_1 = TopMenuItem.Rows[0]["ItemHtml"].ToString();
        item_2 = TopMenuItem.Rows[1]["ItemHtml"].ToString();
        item_3 = TopMenuItem.Rows[2]["ItemHtml"].ToString();
        item_4 = TopMenuItem.Rows[3]["ItemHtml"].ToString();
        item_5 = TopMenuItem.Rows[4]["ItemHtml"].ToString();
        item_6 = TopMenuItem.Rows[5]["ItemHtml"].ToString();
        item_7 = TopMenuItem.Rows[6]["ItemHtml"].ToString();
        item_8 = TopMenuItem.Rows[7]["ItemHtml"].ToString();

        //item_1_name = TopMenuItem.Rows[0]["ItemName"].ToString();
        //item_2_name = TopMenuItem.Rows[1]["ItemName"].ToString();
        //item_3_name = TopMenuItem.Rows[2]["ItemName"].ToString();
        //item_4_name = TopMenuItem.Rows[3]["ItemName"].ToString();
        //item_5_name = TopMenuItem.Rows[4]["ItemName"].ToString();
        //item_6_name = TopMenuItem.Rows[5]["ItemName"].ToString();
        //item_7_name = TopMenuItem.Rows[6]["ItemName"].ToString();

        //item_1_Url = TopMenuItem.Rows[0]["ItemUrl"].ToString();
        //item_2_Url = TopMenuItem.Rows[1]["ItemUrl"].ToString();
        //item_3_Url = TopMenuItem.Rows[2]["ItemUrl"].ToString();
        //item_4_Url = TopMenuItem.Rows[3]["ItemUrl"].ToString();
        //item_5_Url = TopMenuItem.Rows[4]["ItemUrl"].ToString();
        //item_6_Url = TopMenuItem.Rows[5]["ItemUrl"].ToString();
        //item_7_Url = TopMenuItem.Rows[6]["ItemUrl"].ToString();

        //item_1_AllSubCategoryHtml = TopMenuItem.Rows[0]["AllSubCategoryHtml"].ToString();
        //item_2_AllSubCategoryHtml = TopMenuItem.Rows[1]["AllSubCategoryHtml"].ToString();
        //item_3_AllSubCategoryHtml = TopMenuItem.Rows[2]["AllSubCategoryHtml"].ToString();
        //item_4_AllSubCategoryHtml = TopMenuItem.Rows[3]["AllSubCategoryHtml"].ToString();
        //item_5_AllSubCategoryHtml = TopMenuItem.Rows[4]["AllSubCategoryHtml"].ToString();
        //item_6_AllSubCategoryHtml = TopMenuItem.Rows[5]["AllSubCategoryHtml"].ToString();
        //item_7_AllSubCategoryHtml = TopMenuItem.Rows[6]["AllSubCategoryHtml"].ToString();

        MenuItemHtml.Add(item_1);
        MenuItemHtml.Add(item_2);
        MenuItemHtml.Add(item_3);
        MenuItemHtml.Add(item_4);
        MenuItemHtml.Add(item_5);
        MenuItemHtml.Add(item_6);
        MenuItemHtml.Add(item_7);
        MenuItemHtml.Add(item_8);


        return MenuItemHtml;
    }
}
