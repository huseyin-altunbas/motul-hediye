using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

public class PopupFactory
{
    private int _ID;
    private int _Tur;
    private int _Tip;
    private string _Adi;
    private string _Resim1;
    private string _Adres;
    private DateTime _YayinTarihi;
    private DateTime _SonaErmeTarihi;
    private int _EkleyenID;
    private int _Yukseklik;
    private int _Genislik;

    public int p_Yukseklik
    {
        set { _Yukseklik = value; }
        get { return _Yukseklik; }
    }

    public int p_Genislik
    {
        set { _Genislik = value; }
        get { return _Genislik; }
    }

    public int p_ID
    {
        set { _ID = value; }
        get { return _ID; }
    }

    public int p_Tur
    {
        set { _Tur = value; }
        get { return _Tur; }
    }

    public int p_Tip
    {
        set { _Tip = value; }
        get { return _Tip; }
    }

    public string p_Adi
    {
        set { _Adi = value; }
        get { return _Adi; }
    }

    public string p_Resim1
    {
        set { _Resim1 = value; }
        get { return _Resim1; }
    }

    public string p_Adres
    {
        set { _Adres = value; }
        get { return _Adres; }
    }

    public DateTime p_YayinTarihi
    {
        set { _YayinTarihi = value; }
        get { return _YayinTarihi; }
    }

    public DateTime p_SonaErmeTarihi
    {
        set { _SonaErmeTarihi = value; }
        get { return _SonaErmeTarihi; }
    }

    public int p_EkleyenID
    {
        set { _EkleyenID = value; }
        get { return _EkleyenID; }
    }

    public PopupFactory(int k_ID, int k_Tur, int k_Tip, string k_Adi, string k_Resim1, string k_Adres, DateTime k_YayinTarihi, DateTime k_SonaErmeTarihi, int k_EkleyenID, int k_Yukseklik, int k_Genislik)
    {
        _ID = k_ID;
        _Tur = k_Tur;
        _Tip = k_Tip;
        _Adi = k_Adi;
        _Resim1 = k_Resim1;
        _Adres = k_Adres;
        _YayinTarihi = k_YayinTarihi;
        _SonaErmeTarihi = k_SonaErmeTarihi;
        _EkleyenID = k_EkleyenID;
        _Yukseklik = k_Yukseklik;
        _Genislik = k_Genislik;
    }

    public PopupFactory TekilGetir(int ID)
    {
        PopupFactory obj = null;
        DAL dalim = new DAL(ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ConnectionString);
        SqlParameter[] prm = { dalim.SetParameter("@ID", SqlDbType.Int, 0, DAL.pdirection.Input, ID) };
        SqlDataReader rd = dalim.getreader("Select * from T_POPUP where ID=@ID", prm, DAL.commandttype.Text);
        if (rd.Read())
        {
            obj = new PopupFactory((int)rd["ID"], (int)rd["Tur"], (int)rd["Tip"], (string)rd["Adi"], (string)rd["Resim1"], (string)rd["Adres"], (DateTime)rd["YayinTarihi"], (DateTime)rd["SonaErmeTarihi"], (int)rd["EkleyenID"], (int)rd["Yukseklik"], (int)rd["Genislik"]);
        }
        rd.Close();
        dalim.CloseCon();
        dalim.Dispose();
        return obj;
    }

    public static PopupFactory YayindaOlanPopupiGetir(int ID)
    {
        PopupFactory obj = null;
        DAL dalim = new DAL(ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ConnectionString);
        SqlParameter[] prm = { dalim.SetParameter("@ID", SqlDbType.Int, 0, DAL.pdirection.Input, ID) };
        SqlDataReader rd = dalim.getreader("Select * from T_POPUP where ID=@ID and SonaErmeTarihi > getdate()", prm, DAL.commandttype.Text);
        if (rd.Read())
        {
            obj = new PopupFactory((int)rd["ID"], (int)rd["Tur"], (int)rd["Tip"], (string)rd["Adi"], (string)rd["Resim1"], (string)rd["Adres"], (DateTime)rd["YayinTarihi"], (DateTime)rd["SonaErmeTarihi"], (int)rd["EkleyenID"], (int)rd["Yukseklik"], (int)rd["Genislik"]);
        }
        rd.Close();
        dalim.CloseCon();
        dalim.Dispose();
        if (obj != null)
            return obj;
        else
            return new PopupFactory();
    }

    public ArrayList TumunuGetir()
    {
        ArrayList objAna = new ArrayList();
        DAL dalim = new DAL(ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ConnectionString);
        SqlDataReader rd = dalim.getreader("Select * from T_POPUP where tip=@tip", null, DAL.commandttype.Text);
        while (rd.Read())
        {
            PopupFactory obj = new PopupFactory((int)rd["ID"], (int)rd["Tur"], (int)rd["Tip"], (string)rd["Adi"], (string)rd["Resim1"], (string)rd["Adres"], (DateTime)rd["YayinTarihi"], (DateTime)rd["SonaErmeTarihi"], (int)rd["EkleyenID"], (int)rd["Yukseklik"], (int)rd["Genislik"]);
            objAna.Add(obj);
        }
        rd.Close();
        dalim.CloseCon();
        dalim.Dispose();
        return objAna;

    }

    public void Popup_Islemleri(int IslemID, int ID, int Tur, int Tip, string Adi, string Resim1, string Adres, DateTime YayinTarihi, DateTime SonaErmeTarihi, int EkleyenID, int Yukseklik, int Genislik)
    {
        int a = 0;
        DAL dalim = new DAL(ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ConnectionString);
        SqlParameter[] pr =
                            {
                                dalim.SetParameter("@IslemID", SqlDbType.Int, 0, DAL.pdirection.Input, IslemID)
                                , dalim.SetParameter("@ID", SqlDbType.Int, 0, DAL.pdirection.Input, ID)
                                , dalim.SetParameter("@Tur", SqlDbType.Int, 0, DAL.pdirection.Input, Tur)
                                , dalim.SetParameter("@Tip", SqlDbType.Int, 0, DAL.pdirection.Input, Tip)
                                , dalim.SetParameter("@Adi", SqlDbType.NVarChar, 500, DAL.pdirection.Input, Adi)
                                , dalim.SetParameter("@Resim1", SqlDbType.NVarChar, 500, DAL.pdirection.Input, Resim1)
                                , dalim.SetParameter("@Adres", SqlDbType.NVarChar, 500, DAL.pdirection.Input, Adres)
                                , dalim.SetParameter("@YayinTarihi", SqlDbType.DateTime, 0, DAL.pdirection.Input, YayinTarihi)
                                , dalim.SetParameter("@SonaErmeTarihi", SqlDbType.DateTime, 0, DAL.pdirection.Input, SonaErmeTarihi)
                                , dalim.SetParameter("@EkleyenID", SqlDbType.Int, 0, DAL.pdirection.Input, EkleyenID)
                                , dalim.SetParameter("@Yukseklik", SqlDbType.Int, 0, DAL.pdirection.Input, Yukseklik)
                                , dalim.SetParameter("@Genislik", SqlDbType.Int, 0, DAL.pdirection.Input, Genislik)
                            };
        dalim.OpenCon();
        a = dalim.executecmd("sp_popupIslemleri", pr, DAL.commandttype.StoredProcedure);
        dalim.CloseCon();
        dalim.Dispose();
        if (a == 0)
        {
            throw new hata("Güncelleme Komutu Çalıstırılamadı");
        }

    }

    public PopupFactory()
    {

    }
}