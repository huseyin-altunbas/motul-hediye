﻿using Goldb2cBLL;
using Goldb2cEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

/// <summary>
/// Summary description for ValidateModelAttribute
/// </summary>
public class ValidateModelAdminAttribute : System.Web.Http.Filters.ActionFilterAttribute
{
    public override void OnActionExecuting(HttpActionContext actionContext)
    {
        string token = (actionContext.Request.Headers.Any(x => x.Key == "Token")) ? actionContext.Request.Headers.Where(x => x.Key == "Token").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
        
        if (token == "")
        {
            token = actionContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value).Where(p=>p.Key== "Token").FirstOrDefault().Value;

            //if (actionContext.Request.qu)
        }

        UyelikIslemleriBLL uyelikIslemleriBLL = new UyelikIslemleriBLL();
        DataTable dt = uyelikIslemleriBLL.GetirKullaniciByServiceKey(token);
        if (dt.Rows.Count != 1)
        {
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Missing 'Authorization' header. Access denied.");
            return;
        }
        Kullanici kullanici = uyelikIslemleriBLL.KullaniciObjectOlustur(dt);

        if (kullanici.UyelikTipi != 1)
        {
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Missing 'Authorization' header. Access denied.");
        }




    }
}