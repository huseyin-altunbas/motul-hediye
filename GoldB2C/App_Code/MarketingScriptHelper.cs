﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

/// <summary>
/// Summary description for MarketingScriptHelper
/// </summary>
public class MarketingScriptHelper
{
    public MarketingScriptHelper()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string NewRemarketingCodeHazirla(string prodid, string pagetype, string pname, string pcat, string price)
    {
        string Result = "";
        StringBuilder sbNewRemarketingCodeParams = new StringBuilder();

        if (!string.IsNullOrEmpty(prodid))
            sbNewRemarketingCodeParams.AppendLine("prodid: " + prodid + ",");
        if (!string.IsNullOrEmpty(pagetype))
            sbNewRemarketingCodeParams.AppendLine("pagetype: \"" + pagetype + "\",");
        if (!string.IsNullOrEmpty(pname))
            sbNewRemarketingCodeParams.AppendLine("pname: \"" + pname + "\",");
        if (!string.IsNullOrEmpty(pcat))
            sbNewRemarketingCodeParams.AppendLine("pcat: \"" + pcat + "\",");
        if (!string.IsNullOrEmpty(price))
            sbNewRemarketingCodeParams.AppendLine("price: " + price + ",");

        string NewRemarketingParam = sbNewRemarketingCodeParams.ToString().TrimEnd(new char[] { '\n', '\r', ',' });

        StringBuilder sbNewRemarketingCode = new StringBuilder();

        sbNewRemarketingCode.AppendLine("<!-- Google Code - New Remarketing -->");

        sbNewRemarketingCode.AppendLine("<script type=\"text/javascript\">");
        sbNewRemarketingCode.AppendLine("var google_tag_params = {");

        sbNewRemarketingCode.AppendLine(NewRemarketingParam);

        sbNewRemarketingCode.AppendLine("};");
        sbNewRemarketingCode.AppendLine("</script>");

        sbNewRemarketingCode.AppendLine("<script type=\"text/javascript\">");
        sbNewRemarketingCode.AppendLine("/* <![CDATA[ */");
        sbNewRemarketingCode.AppendLine("var google_conversion_id = 1006880374;");
        sbNewRemarketingCode.AppendLine("var google_conversion_label = \"F07aCLKA1AUQ9oyP4AM\";");
        sbNewRemarketingCode.AppendLine("var google_custom_params = window.google_tag_params;");
        sbNewRemarketingCode.AppendLine("var google_remarketing_only = true;");

        sbNewRemarketingCode.AppendLine("/* ]]> */");
        sbNewRemarketingCode.AppendLine("</script>");
        //sbNewRemarketingCode.AppendLine("<script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion.js\">");
        sbNewRemarketingCode.AppendLine("</script>");
        sbNewRemarketingCode.AppendLine("<noscript>");
        sbNewRemarketingCode.AppendLine("<div style=\"display:inline;\">");
        sbNewRemarketingCode.AppendLine("<img height=\"1\" width=\"1\" style=\"border-style:none;\" alt=\"\" src=\"//googleads.g.doubleclick.net/pagead/viewthroughconversion/1006880374/?value=0&amp;label=F07aCLKA1AUQ9oyP4AM&amp;guid=ON&amp;script=0\"/>");
        sbNewRemarketingCode.AppendLine("</div>");
        sbNewRemarketingCode.AppendLine("</noscript>");
        sbNewRemarketingCode.AppendLine("<!-- Google Code - New Remarketing -->");

        Result = sbNewRemarketingCode.ToString();

        return Result;
    }
}