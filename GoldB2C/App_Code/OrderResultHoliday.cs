﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class OrderResultHoliday
{
    public bool Status { get; set; }
    public string Type { get; set; }
    public List<int> OrderNumbers { get; set; }
    public List<int> StockCodes { get; set; }
    public String Message { get; set; }
}
