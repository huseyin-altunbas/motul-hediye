﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for Swal
/// </summary>
public static class Swal
{
    public static void Message(Page Page, string type, string title, string text)
    {
        const string someScript = "alertMe";
        if (!Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), someScript))
        {
            Page.ClientScript.RegisterStartupScript(Page.GetType(),
                someScript, " swal('" + title + "', '" + text + "', '" + type + "')", true);
        }
    }
}