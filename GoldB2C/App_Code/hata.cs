using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for hata
/// </summary>
public class hata : Exception
{
    private string mesajim;

    public string ozellik_mesajim
    {

        get { return mesajim; }

        set { mesajim = value; }

    }

    public hata()
        : base()
    {


    }

    public hata(string mesaj)
        : base(mesaj)
    {
        mesajim = mesaj;
    }




}
