﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Configuration;
using Goldb2cInfrastructure;
using Goldb2cBLL.Custom;
using System.Data;
using Goldb2cEntity.Custom;
using CekAdmin;
using System.Data.SqlClient;
/// <summary>
/// Summary description for AjaxMethods
/// </summary>
[System.Web.Script.Services.ScriptService()]
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class AjaxMethods : System.Web.Services.WebService
{
    private static CekAdmin.LoytechCekYonetimSoapClient CekAdminService = new CekAdmin.LoytechCekYonetimSoapClient();
    private string CekAdminServiceUser = CustomConfiguration.CekAdminServiceUser;
    private string CekAdminServicePass = CustomConfiguration.CekAdminServicePass;
    private int CekAdminProjectId = CustomConfiguration.CekAdminProjectId;
    private String SmsServiceUser = CustomConfiguration.SmsServiceUser;
    private String SmsServicePass = CustomConfiguration.SmsServicePass;
    private String SmsServiceOrjinator = CustomConfiguration.SmsServiceOrjinator;
    private HediyeCekiSiparisIslemleri _HediyeCekiSiparisIslemleri = new HediyeCekiSiparisIslemleri();
    private UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    private TransactionBLL transactionBLL = new TransactionBLL();

    public AjaxMethods()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public void TiklamaKaydet(string TiklananLink)
    {
        PortalHistory portalHistory = new PortalHistory();

        string KullaniciEmail = "";
        if (HttpContext.Current.Session["Kullanici"] != null)
        {
            KullaniciEmail = ((Kullanici)HttpContext.Current.Session["Kullanici"]).Email;
        }
        portalHistory.GetTiklamaHistory(TiklananLink, KullaniciEmail);

    }



    [WebMethod(EnableSession = true)]
    public Kullanici GetKullaniciByToken(string Token)
    {
        DataTable dt = bllUyelikIslemleri.GetirKullaniciByServiceKey(Token);
        Kullanici kullanici = bllUyelikIslemleri.KullaniciSistemKayit((int)dt.Rows[0][0]);
        return kullanici;
    }

    [WebMethod(EnableSession = true)]
    public SiparisOlusturResult SiparisOlusturV2(Kullanici kullanici, string StokKod, int Adet, string Gsm, int VendorId, int PoductId)
    {

        DataTable dt = bllUyelikIslemleri.GetirKullaniciByServiceKey(kullanici.ServiceKey.ToString());
        if (dt.Rows.Count < 1)
        {
            throw new Exception();
        }

        SiparisOlusturResult _result = new SiparisOlusturResult();


        SiparisIslemleriBLL bllSiparisIslemleri = new SiparisIslemleriBLL();
        UrunListelemeBLL bllUrunListeleme = new UrunListelemeBLL();

        LogIslemleriBLL bllLogIslemleri = new LogIslemleriBLL();
        SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
        HediyeCekiSiparisIslemleri _HediyeCekiSiparisIslemleri = new HediyeCekiSiparisIslemleri();
        HediyeCekIslemleri _HediyeCekIslemleri = new HediyeCekIslemleri();
        //int resultCode = -1;
        //string resultText = "";

        if (kullanici.ErpCariId < 1)
        {
            kullanici = bllUyelikIslemleri.CariVeriOlustur(kullanici);
        }

        Tuple<int, string, bool> tupleSiparisStok = bllSiparisIslemleri.SiparisTekUrunGuncelleKaydet(kullanici.ErpCariId, 0, "", StokKod, Adet);
        int siparisMasterId = tupleSiparisStok.Item1;
        DataSet dsSiparisVerileri = bllSiparisGoruntuleme.SiparisGoruntule(siparisMasterId, kullanici.ErpCariId);
        DataTable dtSiparisDetayTT = dsSiparisVerileri.Tables["SiparisDetayTT"];
        //int siparisDetayNo;
        //Int32.TryParse(dtSiparisDetayTT.Rows[0]["sip_detayno"].ToString(), out siparisDetayNo);
        string urunId = Convert.ToString(dtSiparisDetayTT.Rows[0]["urun_id"]);
        PuanView puanView = transactionBLL.KullaniciPuanGetir(kullanici.KullaniciId);
        ProductModel Product = CekAdminService.GetProduct(CekAdminServiceUser, CekAdminServicePass, VendorId, CekAdminProjectId, PoductId);

        if (puanView.kalan < Product.Currency)
        {
            bllSiparisGoruntuleme.SiparisOnay(siparisMasterId, "Reddedildi", "Reddedildi", out siparisMasterId);
            _result.Status = false;
            _result.Message = "Puanınız yetersiz.";
            return _result;
        }

        //Burada ürün rezerve ediliyor.

        bool resultGetGiftCode = CekAdminService.GetGiftCode(CekAdminServiceUser, CekAdminServicePass, StokKod, CekAdminProjectId, siparisMasterId.ToString(), kullanici.KullaniciAd, kullanici.KullaniciSoyad, Gsm, kullanici.Email);
        //if (!resultGetGiftCode)
        //{
        //    bllSiparisGoruntuleme.SiparisOnay(siparisMasterId, "Reddedildi", "Reddedildi", out siparisMasterId);
        //    _result.Message = "Ürün seçimi için tekrar anasayfaya dönmeniz gerekmektedir.";
        //    _result.Status = false;
        //    return _result;
        //}

        HediyeCekiSiparis _HediyeCekiSiparis = _HediyeCekiSiparisIslemleri.Insert(kullanici.ErpCariId, siparisMasterId, int.Parse(StokKod), Adet, kullanici.KullaniciAd, kullanici.KullaniciSoyad, kullanici.Email, Gsm);
        //Kulanıcı hesabından puan çekiliyor.
        int EklenenPuan = 0;
        int HarcananPuan = Convert.ToInt32(Product.Currency);
        int KalanPuan = puanView.kalan - HarcananPuan;

        HarcananPuan = Convert.ToInt32("-" + HarcananPuan);
        transactionBLL.Insert(kullanici.KullaniciId, "", siparisMasterId.ToString() + " nolu sipariş verildi.", HarcananPuan, Goldb2cEntity.Custom.Enums.Transaction.TransactionType.OrderMinusPoint, Goldb2cEntity.Custom.Enums.Generic.Status.Active, DateTime.Now);
        //bllKullaniciPuanHareketleri.Kaydet(kullanici.KullaniciId, siparisMasterId, EklenenPuan, HarcananPuan, KalanPuan, siparisMasterId.ToString() + " nolu sipariş verildi.", DateTime.Now, 0);
        bllSiparisGoruntuleme.SiparisOnay(siparisMasterId, "Onaylandı", "Onaylandı", out siparisMasterId);

        HediyeCekiGonderResult hediyeCekiGonderResult = _HediyeCekIslemleri.HediyeCekiNoGonder(siparisMasterId);

        if (hediyeCekiGonderResult.IsOrderContainsGiftCode)
        {
            if (!hediyeCekiGonderResult.Status)
            {

            }
        }


        _result.OrderNo = siparisMasterId;
        _result.Status = true;
        _result.Message = "Hediye Çeki Kodunuz en kısa sürede cep telefonunuza gönderilecektir.";
        return _result;
    }

    [WebMethod(EnableSession = true)]
    public decimal GetKullaniciPuan(string Token)
    {
        DataTable dt = bllUyelikIslemleri.GetirKullaniciByServiceKey(Token);
        PuanView puanView = transactionBLL.KullaniciPuanGetir((int)dt.Rows[0][0]);
        return puanView.kalan;
    }

    [WebMethod(EnableSession = true)]
    public DataSet GetKullaniciSiparis(string Token)
    {
        SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
        DataTable dt = bllUyelikIslemleri.GetirKullaniciByServiceKey(Token);
        Kullanici kullanici = bllUyelikIslemleri.KullaniciSistemKayit((int)dt.Rows[0][0]);

        if (kullanici.KullaniciId < 1)
        { return new DataSet(); }

        DataSet dsSiparis = bllSiparisGoruntuleme.GenelSiparisIzleme(kullanici.ErpCariId);
        //string hediyecekUrunListesi = HediyeCekiUrunListesi();

        //DataRow[] siparisDetayRows = dsSiparis.Tables["SiparisTakipDetayTT"].Select("urun_id in("+ hediyecekUrunListesi + ")");

        return dsSiparis;
    }

    [WebMethod(EnableSession = true)]
    public CustomResult GiftCodeSend(string Token, int SiparisId, string StokKodu, string Gsm)
    {
        
        CustomResult customResult = new CustomResult();
        customResult.Status = false;
        SmsLogIslemleri smsLogIslemleri = new SmsLogIslemleri();
        CekAdmin.LoytechCekYonetimSoapClient cekAdmin = new CekAdmin.LoytechCekYonetimSoapClient();
        SmsService.SMSSendSoapClient smsService = new SmsService.SMSSendSoapClient();
        DataTable dt = bllUyelikIslemleri.GetirKullaniciByServiceKey(Token);
        if (dt.Rows.Count < 1)
        {
            return customResult;
        }
        List<SmsLog> smsLogList = smsLogIslemleri.GetBySiparisId(SiparisId);
        if (smsLogList.Count >= 5)
        {
            customResult.Text = "Daha önce 5 kez sms gönderimi yapıldığı için tekrar sms gönderimi yapılamaz. Detaylı bilgi için 444 1 222 - Filli Boya Danışma Merkezi’ni arayabilirsiniz.";
            return customResult;
        }
        GiftCodeByBelongModel result = cekAdmin.GetGiftCodeByBelongOrder(CekAdminServiceUser, CekAdminServicePass, CekAdminProjectId, SiparisId.ToString(), StokKodu);
        HediyeCekiSiparis _HediyeCekiSiparis = _HediyeCekiSiparisIslemleri.Get(SiparisId, int.Parse(StokKodu));
        if (result.UsageStatus == "Used")
        {
            DateTime t = (DateTime)result.ExpireDate;
            string sms = "Sn: {0} {1}, {2} siparişiniz için şifreniz {3} ’dir. İyi günlerde kullanınız. Şifreniz {4}  tarihine kadar geçerli olacaktır.";
            sms = string.Format(sms, _HediyeCekiSiparis.Ad, _HediyeCekiSiparis.Soyad, result.ProductName, result.UsedCode, t.ToString("dd/MM/yyyy"));
            int resultSms = smsService.TekilSMSGonderimi(SmsServiceUser, SmsServicePass, SmsServiceOrjinator, Gsm, sms, SiparisId.ToString());
            if (resultSms == 1)
            {
                smsLogIslemleri.Insert(SiparisId, Gsm);
                customResult.Status = true;
                customResult.Text = "Hediye çeki kodunuz " + Gsm + " numaralı cep telefonunuza gönderilmiştir.";
                return customResult;
            }
            else
            {
                return customResult;
            }

        }
        else
        {
            return customResult;
        }

    }

    public List<SmsLog> GetSmsLogList(string Token, int SiparisId)
    {
        SmsLogIslemleri smsLogIslemleri = new SmsLogIslemleri();
        DataTable dt = bllUyelikIslemleri.GetirKullaniciByServiceKey(Token);
        if (dt.Rows.Count < 1)
        {
            return new List<SmsLog>();
        }
        List<SmsLog> list = smsLogIslemleri.GetBySiparisId(SiparisId);
        return list;
    }

    private string HediyeCekiUrunListesi()
    {
        CekAdmin.LoytechCekYonetimSoapClient CekAdmin = new CekAdmin.LoytechCekYonetimSoapClient();
        List<String> list = CekAdmin.GetLogoCodeList(CekAdminServiceUser, CekAdminServicePass);
        List<int> intList = new List<int>();
        foreach (string item in list)
        {
            try
            {
                intList.Add(Convert.ToInt32(item));
            }
            catch (Exception)
            { }
        }
        string sonuc = string.Join(",", intList.ToArray());
        return sonuc;
    }


    [WebMethod(EnableSession = true)]
    public OrderResultHoliday SiparisOlusturTatil(string Token, int userId, string userType, List<string> StokKod, List<int> Adet, int ToplamTutar)
    {
        TransactionBLL transactionBLL = new TransactionBLL();
        OrderResultHoliday _result = new OrderResultHoliday();
        _result.OrderNumbers = new List<int>();
        _result.StockCodes = new List<int>();
        UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
        SiparisIslemleriBLL bllSiparisIslemleri = new SiparisIslemleriBLL();
        UrunListelemeBLL bllUrunListeleme = new UrunListelemeBLL();
        LogIslemleriBLL bllLogIslemleri = new LogIslemleriBLL();
        SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();


        if (Token != "BCD5D877-64B8-42E1-9F41-A9EC46A20411")
        {
            _result.Message = "Geçersiz token.";
            return _result;
        }

        Kullanici kullanici = bllUyelikIslemleri.KullaniciSistemKayit(userId);

        if (kullanici.ErpCariId < 1)
        {
            kullanici = bllUyelikIslemleri.CariVeriOlustur(kullanici);
        }

        List<SiparisDetayEx> orders = new List<SiparisDetayEx>();

        for (int i = 0; i < StokKod.Count; i++)
        {
            orders.Add(new SiparisDetayEx { StokKod = StokKod[i], Miktar = 1, EkAlan1 = "", EkAlan2 = "", PuanCarpan = 2, PuanTutar = ToplamTutar, StokAd = "", TLTutar = 0 });
        }

        List<int> siparisMasterIdList = new List<int>();
        //siparişler uyum'a kayıt ediliyor.

        int siparisMasterId = 0;
        try
        {
            siparisMasterId = bllSiparisIslemleri.SiparisKaydetEx(kullanici.ErpCariId, orders);
            //foreach (SiparisDetayEx item in orders)
            //{
            //    bllSiparisIslemleri.SiparisTekUrunGuncelleKaydet(kullanici.ErpCariId,0,)
            //}
        }
        catch (Exception ex)
        {

        }

        _result.OrderNumbers.Add(siparisMasterId);
        DataSet dsSiparisVerileri = bllSiparisGoruntuleme.SiparisGoruntule(siparisMasterId, kullanici.ErpCariId);
        DataTable dtSiparisDetayTT = dsSiparisVerileri.Tables["SiparisDetayTT"];
        int siparisDetayNo;
        //Int32.TryParse(dtSiparisDetayTT.Rows[0]["sip_detayno"].ToString(), out siparisDetayNo);
        string urunId = Convert.ToString(dtSiparisDetayTT.Rows[0]["urun_id"]);
        bllLogIslemleri.KullaniciSiparisLogKayit(kullanici.KullaniciId, kullanici.UyelikTipi, siparisMasterId, string.Format("Siparişteki {0} numaralı ürün  uyum'da sipariş olarak kaydedildi.", urunId) + " " + "", "");

        bllSiparisGoruntuleme.SiparisOnay(siparisMasterId, "Reddedildi", "Reddedildi", out siparisMasterId);

        int TotalPoint = 0;
        for (int i = 0; i < dtSiparisDetayTT.Rows.Count; i++)
        {
            TotalPoint += Convert.ToInt32(dtSiparisDetayTT.Rows[i]["birim_fiyat"].ToString()) * Convert.ToInt32(dtSiparisDetayTT.Rows[i]["siparis_adet"].ToString());
            TotalPoint = Convert.ToInt32("-" + TotalPoint);
        }

        PuanView puanView = transactionBLL.KullaniciPuanGetir(kullanici.KullaniciId);

        if (puanView.kalan < TotalPoint)
        {
            _result.Message = "puan yetersiz.";
            return _result;
        }

        int webserviceResult = transactionBLL.Insert(kullanici.KullaniciId, "", siparisMasterId + " nolu siparişe istinaden düşülen puan.", TotalPoint, Goldb2cEntity.Custom.Enums.Transaction.TransactionType.OrderMinusPoint, Goldb2cEntity.Custom.Enums.Generic.Status.Active, DateTime.Now);

        if (webserviceResult > 0)
        {
            bllSiparisGoruntuleme.SiparisOnay(siparisMasterId, "Onaylandı", "Onaylandı", out siparisMasterId);
            _result.Status = true;
        }
        else
        {

            _result.Message = "Hata oluştu";
        }


        return _result;
    }

    [WebMethod(EnableSession = true)]
    public DataSet GetSiparisByOrderId(List<Int32> orderIdList)
    {
        SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
        DataSet dsSiparis = bllSiparisGoruntuleme.GenelSiparisListeleme(0, 0, orderIdList[0], orderIdList[orderIdList.Count - 1], DateTime.Parse("02.01.2000"), DateTime.Parse("01.01.9999"));
        return dsSiparis;

    }

    // [WebMethod(EnableSession = true)]
    //public string TakimListesi()
    //{

    //    List<Takimlar> result = DapperManager.GetData<Takimlar>(ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ConnectionString, "select * from Takimlar order by Ad Desc");

    //    System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
    //    return jSearializer.Serialize(result);
    //}


    //  [WebMethod(EnableSession = true)]
    //  public string TakimCevapKaydet(TakimlarCevap TakimCevap)
    //  {

    //return "Kampanya katılım süresi sona ermiştir.";
    //      try
    //      {
    //          Kullanici kullanici = ((Kullanici)Session["Kullanici"]);
    //          TakimCevap.KullaniciId = kullanici.KullaniciId;
    //          List<TakimlarCevap> result = DapperManager.GetData<TakimlarCevap>(ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ConnectionString, "select *from TakimlarCevap where KullaniciId = " + TakimCevap.KullaniciId);
    //          if (result.Count > 0)
    //          {
    //              return "Anketimize daha önce katılım sağladınız.";
    //          }

    //          DapperManager.Insert<TakimlarCevap>(ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ConnectionString, TakimCevap);

    //          return "Anketimize katıldığınız için teşekkür ederiz. Bol şanslar.";
    //      }
    //       catch (Exception ex)
    //      {

    //          return ex.ToString();
    //      }
    //  }

}
