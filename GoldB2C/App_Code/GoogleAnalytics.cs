﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

public class GoogleAnalytics
{
    DataSet dsSiparisVerileri;
    public GoogleAnalytics(DataSet dsSiparisVerileri)
    {
        this.dsSiparisVerileri = dsSiparisVerileri;
    }

    public string EticaretKodOlustur()
    {
        string result = "";
        DataTable dtSiparisDetayTT = dsSiparisVerileri.Tables["SiparisDetayTT"];
        string siparis_no = dsSiparisVerileri.Tables["SiparisMasterTT"].Rows[0]["siparis_no"].ToString();
        string siparis_tutar = Convert.ToInt32(dsSiparisVerileri.Tables["SiparisMasterTT"].Rows[0]["siparis_tutar"].ToString()).ToString();
        result = SiparisBaslik(siparis_no, siparis_tutar);

        foreach (DataRow row in dtSiparisDetayTT.Rows)
        {
            string urunAdi, urunStokKodu, urunKategoriAdi = "", urunFiyat, urunAdet;
            urunAdi = row["urun_ad"].ToString();
            urunFiyat = Convert.ToInt32(row["birim_fiyat"].ToString()).ToString();
            urunAdet = row["siparis_adet"].ToString();
            urunStokKodu = row["urun_id"].ToString();
            result += SiparisIcerik(siparis_no, urunAdi, urunStokKodu, urunKategoriAdi, urunFiyat, urunAdet);
        }

        result += "ga('ecommerce:send');";
        return result;
    }

    private string SiparisBaslik(string siparisNo, string toplamTutar)
    {
        string model = @" 
           ga('ecommerce:addTransaction', {
            'id': '#siparisNo#',  // Transaction ID. Required.
            'affiliation': '',   // Affiliation or store name.
            'revenue': '#toplamTutar#',               // Grand Total.
            'shipping': '0',                  // Shipping Cargo Price.
            'tax': '0'                     // Tax.
        });";

        string result = model.Replace("#siparisNo#", siparisNo).Replace("#toplamTutar#", toplamTutar);
        return result;
    }

    private string SiparisIcerik(string siparisNo, string urunAdi, string urunStokKodu, string urunKategoriAdı, string urunFiyat, string urunAdet)
    {
        string model = @"
           ga('ecommerce:addItem', {
            'id': '#siparisNo#',                     // Transaction ID. Required.
            'name': '/#urunStokKodu#/ #urunAdi#',    // Product name. Required.
            'sku': '#urunStokKodu#',                 // SKU/code.
            'category': '#urunKategoriAdı#',         // Category or variation.
            'price': '#urunFiyat#',                 // Unit price.
            'quantity': '#urunAdet#'                   // Quantity.
          }); 




   //Action with order
    document.addEventListener('DOMContentLoaded', function (event) {
       
        AIAnalytics.socket.addEventListener('open', function(event) {
        var action = new AIAnalytics.action();
        action['title'] = 'order';
        action['orderId'] = '#siparisNo#';
        AIAnalytics.socketSendMessage(JSON.stringify(action));
    });
    });


";

        string result = model.Replace("#siparisNo#", siparisNo).Replace("#urunAdi#", urunAdi).Replace("#urunStokKodu#", urunStokKodu).Replace("#urunKategoriAdı#", urunKategoriAdı).Replace("#urunFiyat#", urunFiyat).Replace("#urunAdet#", urunAdet);
        return result;
    }
}