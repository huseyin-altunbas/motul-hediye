﻿using CekAdmin;
using Goldb2cBLL;
using Goldb2cInfrastructure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HediyeCekIslemleri
/// </summary>
public class HediyeCekIslemleri
{
    CekAdmin.LoytechCekYonetimSoapClient CekAdminService = new CekAdmin.LoytechCekYonetimSoapClient();
    SmsService.SMSSendSoapClient smsService = new SmsService.SMSSendSoapClient();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    private string CekAdminServiceUser = CustomConfiguration.CekAdminServiceUser;
    private string CekAdminServicePass = CustomConfiguration.CekAdminServicePass;
    private int CekAdminProjectId = CustomConfiguration.CekAdminProjectId;
    private Goldb2cBLL.Custom.HediyeCekiSiparisIslemleri _HediyeCekiSiparisIslemleri = new Goldb2cBLL.Custom.HediyeCekiSiparisIslemleri();
    LogIslemleriBLL bllLogIslemleri = new LogIslemleriBLL();
    public HediyeCekIslemleri()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public HediyeCekiGonderResult HediyeCekiNoGonder(int siparisId)
    {
        SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
        HediyeCekiGonderResult hediyeCekiGonderResult = new HediyeCekiGonderResult();
        hediyeCekiGonderResult.Status = false;

        DataSet dsSiparisVerileri = bllSiparisGoruntuleme.SiparisGoruntule(siparisId, 0);
        DataTable dtSiparisMasterTT = dsSiparisVerileri.Tables["SiparisMasterTT"];
        DataTable dtSiparisDetayTT = dsSiparisVerileri.Tables["SiparisDetayTT"];
        int SiparsNo = Convert.ToInt32(dtSiparisMasterTT.Rows[0]["siparis_no"].ToString());
        List<String> ListSiparisDetay = new List<string>();
        List<String> HediyeCekUrunList = HediyeCekiUrunListesi();
        List<String> HediyeCekiUrunList = new List<string>();

        foreach (DataRow Row in dtSiparisDetayTT.Rows)
        {
            ListSiparisDetay.Add(Row["urun_id"].ToString());
        }

        foreach (string item in ListSiparisDetay)
        {
            try
            {
                string result1 = HediyeCekUrunList.Single(s => s == item);
                HediyeCekiUrunList.Add(result1);
            }
            catch (Exception ex)
            { }
        }

        if (HediyeCekiUrunList.Count > 0)
        { hediyeCekiGonderResult.IsOrderContainsGiftCode = true; }

        foreach (string StokKod in HediyeCekiUrunList)
        {
            String SmsServiceUser = CustomConfiguration.SmsServiceUser;
            String SmsServicePass = CustomConfiguration.SmsServicePass;
            String SmsServiceOrjinator = CustomConfiguration.SmsServiceOrjinator;


            Goldb2cEntity.Custom.HediyeCekiSiparis _HediyeCekiSiparis = _HediyeCekiSiparisIslemleri.Get(SiparsNo, int.Parse(StokKod));
            if (!_HediyeCekiSiparis.HediyeCekiGonderildimi)
            {
                CekAdminService.GetGiftCode(CekAdminServiceUser, CekAdminServicePass, StokKod, CekAdminProjectId, siparisId.ToString(), _HediyeCekiSiparis.Ad, _HediyeCekiSiparis.Soyad, _HediyeCekiSiparis.Gsm, _HediyeCekiSiparis.Email);
                GiftCodeResultModel giftCodeResultModel = CekAdminService.UseGiftCode(CekAdminServiceUser, CekAdminServicePass, CekAdminProjectId, SiparsNo.ToString(), StokKod);
                if (giftCodeResultModel.IsSuccess)
                {
                    DateTime t = (DateTime)giftCodeResultModel.ExpireDate;

                    //string sms = "Değerli Filli Puan üyemiz, {0} siparişiniz için şifreniz {1} ’dir.İyi günlerde kullanınız. Şifreniz {2}  tarihine kadar geçerli olacaktır.";
                    string sms = "Sn: {0} {1}, {2} siparişiniz için şifreniz {3} ’dir. İyi günlerde kullanınız. Şifreniz {4}  tarihine kadar geçerli olacaktır.";
                    sms = string.Format(sms, _HediyeCekiSiparis.Ad, _HediyeCekiSiparis.Soyad, giftCodeResultModel.ProductName, giftCodeResultModel.UsedCode, t.ToString("dd/MM/yyyy"));
                    int resultSms = smsService.TekilSMSGonderimi(SmsServiceUser, SmsServicePass, SmsServiceOrjinator, _HediyeCekiSiparis.Gsm, sms, siparisId.ToString());
                    bllLogIslemleri.KullaniciSiparisLogKayit(5, Convert.ToInt32(Enums.UyelikTipi.WebAdmin), siparisId, string.Format("Siparis no {0} içindeki {1} için sms gönderimi sonucu : {2}", giftCodeResultModel.ProductName, StokKod, resultSms), string.Empty);
                    if (resultSms == 1)
                    {
                        _HediyeCekiSiparisIslemleri.Update(_HediyeCekiSiparis.Id, true);
                        hediyeCekiGonderResult.Status = true;
                    }

                }
                else
                {
                    bllLogIslemleri.KullaniciSiparisLogKayit(5, Convert.ToInt32(Enums.UyelikTipi.WebAdmin), siparisId, string.Format("{0} için sms gönderimi yapılmadı.", giftCodeResultModel.ProductName), "");
                    //Bu sipariş kesinlikle onaylanamaz. Onay aşamasında kullanıcıya bilgi verilmelidir. Çok önemli
                    if (giftCodeResultModel.ResultMessage == "Bu sipariş için daha önce gönderim/iptal yapılmış!")
                    {
                        hediyeCekiGonderResult.Status = true;
                        hediyeCekiGonderResult.IsOrderContainsGiftCode = true;
                    }

                }
            }
            else
            {
                hediyeCekiGonderResult.Status = true;
                hediyeCekiGonderResult.IsOrderContainsGiftCode = true;
            }

        }


        return hediyeCekiGonderResult;
    }

    public List<String> HediyeCekiUrunListesi()
    {
        CekAdmin.LoytechCekYonetimSoapClient CekAdmin = new CekAdmin.LoytechCekYonetimSoapClient();
        List<String> list = CekAdmin.GetLogoCodeList(CekAdminServiceUser, CekAdminServicePass);
        return list;
    }
}

