﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class OrderView
{
    public int id { get; set; }
    public int cari_id { get; set; }
    public string cari_kod { get; set; }
    public string cari_ad { get; set; }
    public string siparis_no { get; set; }
    public DateTime siparis_tarih { get; set; }
    public string siparis_saat { get; set; }
    public string siparis_statu { get; set; }
    public string siparis_detay_statu { get; set; }
    public string siparis_tutar { get; set; }
    public string onay_durum { get; set; }
    public string onay_durum2 { get; set; }
    public string kargoLink { get; set; }
}

public class OrderViewDetail
{
    public int siparis_id { get; set; }
    public int cari_id { get; set; }
    public string cari_kod { get; set; }
    public string cari_ad { get; set; }
    public string siparis_no { get; set; }
    public DateTime siparis_tarih { get; set; }
    public string siparis_saat { get; set; }
    public string siparis_statu { get; set; }
    public string siparis_detay_statu { get; set; }
    public string siparis_tutar { get; set; }
    public string onay_durum { get; set; }
    public string onay_durum2 { get; set; }
    public string kargoLink { get; set; }

    public string urun_id { get; set; }
    public string urun_ad { get; set; }
    public string siparis_adet { get; set; }
    public string birim { get; set; }
    //public string indirim  { get; set; }
    public string toplam_tutar { get; set; }

}

