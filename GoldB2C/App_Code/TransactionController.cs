﻿using ClosedXML.Excel;
using Goldb2cBLL;
using Goldb2cBLL.Custom;
using Goldb2cEntity;
using Goldb2cEntity.Custom;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

[ValidateModelAdminAttribute]
public class TransactionController : ApiController
{
    TransactionBLL transactionBLL = new TransactionBLL();
    UyelikIslemleriBLL uyelikIslemleriBLL = new UyelikIslemleriBLL();
    [HttpPost]
    public DataSourceResult Get(View filters)
    {
        
        string token = (this.ActionContext.Request.Headers.Any(x => x.Key == "Token")) ? this.ActionContext.Request.Headers.Where(x => x.Key == "Token").FirstOrDefault().Value.SingleOrDefault().Replace("Bearer ", "") : "";
        if (token == "")
        {
            token = this.ActionContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value).Where(p => p.Key == "Token").FirstOrDefault().Value;
        }

        if (token == "")
        {
            token = this.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value).Where(p => p.Key == "Token").FirstOrDefault().Value;
        }
        List<TransactionView> list = transactionBLL.Get(filters);

        int totalRows = 0;
        if (list.Count > 0)
        {
            totalRows = list[0].totalRows;
        }

        return new DataSourceResult { Data = list, Total = totalRows };

    }


    [HttpGet]
    public HttpResponseMessage Export(string filter)
    {
        string options = JsonConvert.DeserializeObject(HttpUtility.UrlDecode(filter)).ToString();
        View filters = JsonConvert.DeserializeObject<View>(options);
        filters.Take = int.MaxValue;

        List<TransactionView> list = transactionBLL.Get(filters);
        DataTable dt = Helper.ToDataTable<TransactionView>(list);

        dt.Columns.Remove("status");
        dt.Columns.Remove("updated");
        dt.Columns.Remove("totalRows");
        dt.Columns.Remove("statusText");
        dt.Columns.Remove("statusNumber");
        dt.Columns.Remove("userId");
        dt.Columns.Remove("transactionType");
        dt.Columns["CRMCustomerGUID"].ColumnName = "Üye Telefon";
        dt.Columns["transactionTypeText"].ColumnName = "Puan Tipi";
        dt.Columns["description"].ColumnName = "Açıklama";
        dt.Columns["amount"].ColumnName = "Puan";
        dt.Columns["period"].ColumnName = "Hakediş Tarihi";
        dt.Columns["created"].ColumnName = "Oluşturma Tarihi";
        dt.Columns["KullaniciAd"].ColumnName = "Üye Adı";


        var stream = new MemoryStream();
        using (XLWorkbook wb = new XLWorkbook())
        {
            wb.Worksheets.Add(dt);
            wb.SaveAs(stream);
        }

        var result = new HttpResponseMessage(HttpStatusCode.OK)
        {
            Content = new ByteArrayContent(stream.ToArray())
        };
        result.Content.Headers.ContentDisposition =
            new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "Hesap Hareketleri.xlsx"
            };
        result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-msdownload");

        return result;



    }

    [HttpPost]
    public Transaction Post(TransactionView model)
    {
        model.status = Enums.Generic.Status.Active;
        model.period = model.period.AddDays(1);
        transactionBLL.Insert(model.KullaniciId, "", model.description, model.amount, model.transactionType, model.status, model.period);
        return new Transaction();
    }

}
