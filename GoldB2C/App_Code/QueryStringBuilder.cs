﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;

/// <summary>
/// Summary description for QueryStringBuilder
/// </summary>
public class QueryStringBuilder
{
    public QueryStringBuilder()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    Dictionary<string, string[]> QueryStringParams;
    public string EkleQueryString(string key, string value, string Sayfa)
    {
        string SayfaPath = "";
        string SayfaQuery = "";
        string[] sayfaQueryPath = Sayfa.Split('?');
        if (sayfaQueryPath.Length == 2)
        {
            SayfaPath = sayfaQueryPath[0];
            SayfaQuery = "?" + sayfaQueryPath[1];
        }
        else
        {
            if (Sayfa.StartsWith("/"))
                SayfaPath = Sayfa;
            else
                SayfaPath = "/" + Sayfa;
        }
        string _key = key.ToLower();
        string QueryString = "";

        if (QueryStringParams == null)
            QueryStringParams = new Dictionary<string, string[]>();


        if (!string.IsNullOrEmpty(SayfaQuery))
        {
            NameValueCollection SayfaQStrColl = GetQueryStringCollection(SayfaQuery);
            if (SayfaQStrColl != null && SayfaQStrColl.Count > 0)
            {
                for (int i = 0; i < SayfaQStrColl.Keys.Count; i++)
                {
                    if (!QueryStringParams.ContainsKey(SayfaQStrColl.GetKey(i).ToLower()))
                    {
                        QueryStringParams.Add(SayfaQStrColl.GetKey(i).ToLower(), SayfaQStrColl.GetValues(i));
                    }

                }
            }
        }





        string[] QueryStringValue;

        //QueryStringleri dictionary e çevir
        for (int i = 0; i < HttpContext.Current.Request.QueryString.Count; i++)
        {
            if (!QueryStringParams.ContainsKey(HttpContext.Current.Request.QueryString.GetKey(i).ToLower()))
            {
                QueryStringParams.Add(HttpContext.Current.Request.QueryString.GetKey(i).ToLower(), HttpContext.Current.Request.QueryString.GetValues(i));
            }

        }

        QueryStringValue = null;
        //key var mı? Kontrol et
        if (QueryStringParams.ContainsKey(_key))
        {
            if (_key == "urungrupid" || _key == "minfiyat" || _key == "maxfiyat" || _key == "sayfano" || _key == "siralama")
            {
                QueryStringParams[_key] = new string[] { value };
            }
            else
            {
                //varsa güncelle
                QueryStringParams.TryGetValue(_key, out QueryStringValue);

                List<string> QueryValue = new List<string>();
                QueryValue = QueryStringValue.ToList();
                QueryValue.Add(value);

                QueryStringParams[_key] = QueryValue.ToArray();
            }
        }
        else
        {
            //yoksa ekle
            QueryStringParams.Add(_key, new string[] { value });
        }

        //Dictionaryden querystring oluştur
        foreach (KeyValuePair<string, string[]> DictionaryItem in QueryStringParams)
        {
            QueryStringValue = null;

            if (string.IsNullOrEmpty(QueryString))
                QueryString = "?";
            else
                QueryString = QueryString + "&";

            QueryString = QueryString + DictionaryItem.Key + "=";

            QueryStringParams.TryGetValue(DictionaryItem.Key, out QueryStringValue);

            foreach (string ItemValue in QueryStringValue)
            {
                QueryString = QueryString + ItemValue + ",";
            }

            QueryString = QueryString.TrimEnd(',');

        }

        return (SayfaPath + QueryString).TrimEnd('&');

    }

    public Dictionary<string, string[]> GetirQueryString()
    {
        Dictionary<string, string[]> QueryStringParams = new Dictionary<string, string[]>();
        //QueryStringleri dictionary e çevir
        for (int i = 0; i < HttpContext.Current.Request.QueryString.Count; i++)
        {
            if (!QueryStringParams.ContainsKey(HttpContext.Current.Request.QueryString.GetKey(i).ToLower()))
            {
                QueryStringParams.Add(HttpContext.Current.Request.QueryString.GetKey(i).ToLower(), HttpContext.Current.Request.QueryString.GetValues(i));
            }

        }

        return QueryStringParams;
    }

    public string SilQueryString(string key, string value, string Sayfa)
    {
        string SayfaPath = "";
        string SayfaQuery = "";
        string[] sayfaQueryPath = Sayfa.Split('?');
        if (sayfaQueryPath.Length == 2)
        {
            SayfaPath = sayfaQueryPath[0];
            SayfaQuery = "?" + sayfaQueryPath[1];
        }
        else
        {
            if (Sayfa.StartsWith("/"))
                SayfaPath = Sayfa;
            else
                SayfaPath = "/" + Sayfa;
        }




        string QueryString = "";
        Dictionary<string, string[]> DictQueryString = new Dictionary<string, string[]>();
        DictQueryString = GetirQueryString();



        if (!string.IsNullOrEmpty(SayfaQuery))
        {
            NameValueCollection SayfaQStrColl = GetQueryStringCollection(SayfaQuery);
            if (SayfaQStrColl != null && SayfaQStrColl.Count > 0)
            {
                for (int i = 0; i < SayfaQStrColl.Keys.Count; i++)
                {
                    if (!DictQueryString.ContainsKey(SayfaQStrColl.GetKey(i).ToLower()))
                    {
                        DictQueryString.Add(SayfaQStrColl.GetKey(i).ToLower(), SayfaQStrColl.GetValues(i));
                    }

                }
            }
        }






        string[] QueryStringValues;

        if (DictQueryString.ContainsKey(key))
        {
            DictQueryString.TryGetValue(key, out QueryStringValues);

            List<string> QueryValue = new List<string>();

            foreach (string _QueryStringVal in QueryStringValues)
            {
                string[] _dizi = _QueryStringVal.Split(',');
                foreach (string _d in _dizi)
                {
                    QueryValue.Add(_d);
                }
            }


            QueryValue.Remove(value);

            DictQueryString[key] = QueryValue.ToArray();


            if (QueryValue.Count < 1 || value == "*")
                DictQueryString.Remove(key);


        }

        if (key == "maxfiyat-minfiyat")
        {
            DictQueryString.Remove("minfiyat");
            DictQueryString.Remove("maxfiyat");
        }

        foreach (KeyValuePair<string, string[]> DictionaryItem in DictQueryString)
        {
            QueryStringValues = null;
            if (string.IsNullOrEmpty(QueryString))
                QueryString = "?";
            else
                QueryString = QueryString + "&";

            QueryString = QueryString + DictionaryItem.Key + "=";

            DictQueryString.TryGetValue(DictionaryItem.Key, out QueryStringValues);

            foreach (string ItemValue in QueryStringValues)
            {
                QueryString = QueryString + ItemValue + ",";
            }

            QueryString = QueryString.TrimEnd(',');

        }

        return SayfaPath + QueryString;


    }

    public static NameValueCollection GetQueryStringCollection(string url)
    {
        string keyValue = string.Empty;
        NameValueCollection collection = new NameValueCollection();
        string[] querystrings = url.Split('&');
        if (querystrings != null && querystrings.Count() > 0)
        {
            for (int i = 0; i < querystrings.Count(); i++)
            {
                string[] pair = querystrings[i].Split('=');

                if (pair[0].Trim('?') == "urungrupid")
                {
                    if (!collection.AllKeys.Contains("urungrupid"))
                        collection.Add(pair[0].Trim('?'), pair[1]);
                }
                else
                    collection.Add(pair[0].Trim('?'), pair[1]);



            }
        }
        return collection;
    }
}