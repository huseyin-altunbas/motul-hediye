﻿
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace Uyum
{
	[XmlRoot(ElementName="CariSiparisDetayTT")]
	public class CariSiparisDetayTT {
		[XmlElement(ElementName="siparis_id")]
		public int Siparis_id { get; set; }
		[XmlElement(ElementName="siparis_detay_id")]
		public string Siparis_detay_id { get; set; }
		[XmlElement(ElementName="sira_no")]
		public string Sira_no { get; set; }
		[XmlElement(ElementName="urun_id")]
		public int Urun_id { get; set; }
		[XmlElement(ElementName="urun_ad")]
		public string Urun_ad { get; set; }
		[XmlElement(ElementName="siparis_adet")]
		public string Siparis_adet { get; set; }
		[XmlElement(ElementName="birim_fiyat")]
		public string Birim_fiyat { get; set; }
		[XmlElement(ElementName="indirim_tutar")]
		public string Indirim_tutar { get; set; }
		[XmlElement(ElementName="toplam_tutar")]
		public string Toplam_tutar { get; set; }
		[XmlElement(ElementName="siparis_statu")]
		public string Siparis_statu { get; set; }
		[XmlElement(ElementName="onay_durum")]
		public string Onay_durum { get; set; }
		[XmlElement(ElementName="onay1_onayveren")]
		public string Onay1_onayveren { get; set; }
		[XmlElement(ElementName="onay1_onaytarih")]
		public DateTime Onay1_onaytarih { get; set; }
		[XmlElement(ElementName="onay_durum2")]
		public string Onay_durum2 { get; set; }
	}

	[XmlRoot(ElementName="CariSiparisMasterTT")]
	public class CariSiparisMasterTT {
		[XmlElement(ElementName="cari_id")]
		public int Cari_id { get; set; }
		[XmlElement(ElementName="cari_kod")]
		public string Cari_kod { get; set; }
		[XmlElement(ElementName="cari_ad")]
		public string Cari_ad { get; set; }
		[XmlElement(ElementName="siparis_no")]
		public string Siparis_no { get; set; }
		[XmlElement(ElementName="siparis_id")]
		public string Siparis_id { get; set; }
		[XmlElement(ElementName="siparis_tarih")]
		public DateTime Siparis_tarih { get; set; }
		[XmlElement(ElementName="siparis_saat")]
		public string Siparis_saat { get; set; }
		[XmlElement(ElementName="siparis_statu")]
		public string Siparis_statu { get; set; }
		[XmlElement(ElementName="siparis_tutar")]
		public string Siparis_tutar { get; set; }
		[XmlElement(ElementName="onay_durum")]
		public string Onay_durum { get; set; }
		[XmlElement(ElementName="onay1_onayveren")]
		public string Onay1_onayveren { get; set; }
		[XmlElement(ElementName="onay_durum2")]
		public string Onay_durum2 { get; set; }
		[XmlElement(ElementName="onay1_onaytarih")]
		public string Onay1_onaytarih { get; set; }

        [XmlElement(ElementName = "KargoLink")]
        public string kargoLink { get; set; }
    }

	[XmlRoot(ElementName="CariSiparisListesiDS")]
	public class CariSiparisListesiDS {
		[XmlElement(ElementName="CariSiparisDetayTT")]
		public List<CariSiparisDetayTT> CariSiparisDetayTT { get; set; }
		[XmlElement(ElementName="CariSiparisMasterTT")]
		public List<CariSiparisMasterTT> CariSiparisMasterTT { get; set; }
	}

}
