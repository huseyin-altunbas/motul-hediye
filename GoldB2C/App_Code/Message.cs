﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[Serializable]
public class Message
{
    public bool status { get; set; }
    public string messageType { get; set; }
    public string title { get; set; }
    public string message { get; set; }
}


[Serializable]
public class CustomResult
{

    public bool Status { get; set; }
    public string Text { get; set; }
    public string Type { get; set; }

}


public class SiparisOlusturResult
{
    public bool Status { get; set; }
    public string Type { get; set; }
    public int OrderNo { get; set; }
    public String Message { get; set; }
}

public class HediyeCekiGonderResult
{
    public bool IsOrderContainsGiftCode { get; set; }
    public bool Status { get; set; }
}
