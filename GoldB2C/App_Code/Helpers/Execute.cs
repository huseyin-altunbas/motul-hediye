﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Execute için özet açıklama
/// </summary>
public static class Execute
{
   public static DataTable GetStockCode(string stockCode)
    {
        string sql =String.Format("Select * from UrunStokKodlari where stokKodu='{0}'",stockCode);
        SqlCommand command = new SqlCommand(sql, Connect.Connection);
        if(Connect.Connection.State==ConnectionState.Closed)
        {
            Connect.Connection.Open();
        }
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable table = new DataTable();
        adapter.Fill(table);

        return table;
        
    }
}