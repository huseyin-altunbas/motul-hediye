﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kendo.DynamicLinq;
using KendoFilterCustom;

/// <summary>
/// Summary description for UyumFilter
/// </summary>
public class UyumFilter
{
    public int ilkCariId { get; set; }
    public int sonCariId { get; set; }
    public int ilkSiparisId { get; set; }
    public int sonSiparisId { get; set; }
    public DateTime ilkTarih { get; set; }
    public DateTime sonTarih { get; set; }

    public static UyumFilter UyumFilterPre(Kendo.DynamicLinq.Filter _filter, string ColumnName)
    {
        List<Kendo.DynamicLinq.Filter> filter = new List<Kendo.DynamicLinq.Filter>();

        if (_filter != null && _filter.Logic != null)
        {

            foreach (Kendo.DynamicLinq.Filter item in _filter.Filters)
            {
                if (item.Filters != null)
                {
                    foreach (Kendo.DynamicLinq.Filter _item in item.Filters)
                    {
                        filter.Add(_item);
                    }
                }
                else
                {
                    filter.Add(item);
                }

            }

        }


        UyumFilter uyumFilter = new UyumFilter();

        int IlkSiparisId = 0;
        int SonSiparisId = 0;
        int IlkCari_id = 0;
        int SonCari_id = 0;
        DateTime ilkTarih = Convert.ToDateTime("01.01.1970");
        DateTime sonTarih= Convert.ToDateTime("01.01.2900");


        List<Kendo.DynamicLinq.Filter> listStartswith = filter.Where(p => p.Field == ColumnName && (p.Operator == "startswith" || p.Operator == "=" || p.Operator == "eq") || p.Operator == "=>" ).ToList();
        if (listStartswith.Count > 0)
        { int.TryParse(listStartswith[0].Value.ToString(), out IlkSiparisId); }

        List<Kendo.DynamicLinq.Filter> listEndswith = filter.Where(p => p.Field == ColumnName && (p.Operator == "endswith" || p.Operator == "=" || p.Operator == "eq") || p.Operator == "=<" ).ToList();
        if (listEndswith.Count > 0)
        { int.TryParse(listEndswith[0].Value.ToString(), out SonSiparisId); }




        List<Kendo.DynamicLinq.Filter> tarihBaslangic = filter.Where(p => p.Field == "Siparis_tarih" && (p.Operator == ">=")).ToList();
        if (tarihBaslangic.Count > 0)
        { DateTime.TryParse(tarihBaslangic[0].Value.ToString(), out ilkTarih); }

        List<Kendo.DynamicLinq.Filter> tarihBitis = filter.Where(p => p.Field == "Siparis_tarih" && (p.Operator == "lte")).ToList();
        if (tarihBitis.Count > 0)
        { DateTime.TryParse(tarihBitis[0].Value.ToString(), out sonTarih); }



        List<Kendo.DynamicLinq.Filter> startswith = filter.Where(p => p.Field == "Cari_id" && (p.Operator == "startswith" || p.Operator == "=" || p.Operator == "eq")).ToList();
        if (startswith.Count > 0)
        { int.TryParse(startswith[0].Value.ToString(), out IlkCari_id); }


        List<Kendo.DynamicLinq.Filter> endswith = filter.Where(p => p.Field == "Cari_id" && (p.Operator == "startswith" || p.Operator == "=" || p.Operator == "eq")).ToList();
        if (endswith.Count > 0)
        { int.TryParse(endswith[0].Value.ToString(), out SonCari_id); }


        uyumFilter.ilkSiparisId = IlkSiparisId;
        uyumFilter.sonSiparisId = SonSiparisId;

        uyumFilter.ilkTarih = ilkTarih;
        uyumFilter.sonTarih = sonTarih;

        uyumFilter.ilkCariId = IlkCari_id;
        uyumFilter.sonCariId = SonCari_id;


        return uyumFilter;
    }



}