﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// CookieHelpers için özet açıklama
/// </summary>
public static class CookieHelpers
{
   
        private static HttpContext Context
        {
            get
            {
                return HttpContext.Current;
            }
        }

        public static void Set(string cookieName,string key, string value, int expiresAsDays = 1000000, bool isHttp = false, string domain = null, string path = null, bool secure = false)
        {
            if (string.IsNullOrEmpty(cookieName))
                return;

            var encodedKey = cookieName;

            var encodedValue = !string.IsNullOrEmpty(value)
                ? HttpUtility.UrlEncode(value)
                : string.Empty;

            var c = new HttpCookie(encodedKey)
            {
                Value = encodedValue,
                Expires = DateTime.Now.AddDays(expiresAsDays),
                HttpOnly = isHttp,
                Secure = secure
            };

            if (!string.IsNullOrEmpty(domain))
                c.Domain = domain;

            if (!string.IsNullOrEmpty(path))
                c.Path = path;

            Context.Response.Cookies.Add(c);
        }

        public static string Get(string cookieName,string key, bool isHttp = false, string domain = null, string path = null, bool secure = false)
        {
            var returnValue = string.Empty;

            if (string.IsNullOrEmpty(cookieName))
                return returnValue;

             var decodedKey = cookieName;
            var c = Context.Request.Cookies[decodedKey];
            if (c == null)
                return returnValue;

            if (isHttp != c.HttpOnly)
                return returnValue;

            if (!string.IsNullOrEmpty(domain) && !string.Equals(c.Domain, domain, StringComparison.Ordinal))
                return returnValue;

            if (!string.IsNullOrEmpty(path) && !string.Equals(c.Path, path, StringComparison.Ordinal))
                return returnValue;

            if (!string.IsNullOrEmpty(c.Value))
                returnValue = HttpUtility.UrlDecode(c.Value).Trim();

            return returnValue;
        }

        public static bool Exists(string cookieName,string key, bool isHttp = false, string domain = null, string path = null, bool secure = false)
        {
            if (string.IsNullOrEmpty(cookieName))
                return false;

            var decodedKey = cookieName;
            var c = Context.Request.Cookies[decodedKey];

            if (c == null)
                return false;

            if (isHttp != c.HttpOnly)
                return false;

            if (!string.IsNullOrEmpty(domain) && !string.Equals(c.Domain, domain, StringComparison.Ordinal))
                return false;

            if (!string.IsNullOrEmpty(path) && !string.Equals(c.Path, path, StringComparison.Ordinal))
                return false;

            return true;
        }

        public static void Delete(string cookieName,string key, bool isHttp = false, string domain = null, string path = null, bool secure = false)
        {
            if (!Exists(cookieName, key, isHttp, domain, path, secure))
                return;

            Set(cookieName,key, null, -10, isHttp, domain, path, secure);
        }

        public static void DeleteAllCookies(string cookieName,bool isHttp = false, string domain = null, string path = null, bool secure = false, bool deleteServerCookies = false)
        {
            for (var i = 0; i <= Context.Request.Cookies.Count - 1; i++)
            {
                if (Context.Request.Cookies[i] != null)
                    Delete(cookieName,Context.Request.Cookies[i].Name, isHttp, domain, path, secure);
            }

            if (deleteServerCookies)
                Context.Request.Cookies.Clear();
        }
    
}