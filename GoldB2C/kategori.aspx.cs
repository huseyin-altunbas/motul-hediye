﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;

public partial class kategori : System.Web.UI.Page
{

    UrunGrupIslemleriBLL bllUrungrup = new UrunGrupIslemleriBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        string urungrup = "";
        if (Request.QueryString["Urungrupid"] != null)
            urungrup = Request.QueryString["Urungrupid"].ToString();



        GridView1.DataSource = bllUrungrup.GetUrunGrupListesi(Convert.ToInt32(urungrup));
        GridView1.DataBind();
    }
}