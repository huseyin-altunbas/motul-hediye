﻿using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
         
            //if (!Request.IsSecureConnection)
            //{
            //    string path = string.Format("https{0}", Request.Url.AbsoluteUri.Substring(4));

            //    Response.Redirect(path);
            //}
        ////DateTime dtYilbasi = DateTime.Parse("31.12.2015 17:00");
        ////if (DateTime.Now > dtYilbasi)
        ////{
        ////    Response.Redirect("Yilbasi.aspx");
        ////}
        //
        ////Response.Write(Request.UserHostAddress);
        //
        //YayinDurdur.YayiniDurdur();
        //
        //if (Request.Url.AbsoluteUri.Contains("https"))
        //{
        //    Response.Redirect(Request.Url.AbsoluteUri.Replace("https:", "http:"));
        //}
        //
        //if (!Request.UserHostAddress.Contains("31.169.68") && !Request.UserHostAddress.Contains("10.172.101"))
        //{
        //    //Response.Redirect("~/Bakim-Asamasi/bakim_asamasi.html");
        //    //Response.Write(Request.UserHostAddress);
        //}
        //
        //Kullanici kullanici;
        //if (Session["Kullanici"] != null)
        //{
        //    Response.Redirect("Default.aspx");
        //}


        if (!IsPostBack)
        {
          //  Image1.ImageUrl = "~/Guvenlik.aspx";
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        //StreamWriter writer = new StreamWriter(Server.MapPath("Gecici-Log.txt"), true);
        //try
        //{
        //    writer.WriteLine(name.Text + " - " + password.Text);
        //}
        //catch (Exception)
        //{ }
        //finally { writer.Close(); }


        //if (this.Session["GuvenlikKodu"] == null)
        //{
        //    mesajGoster("Lütfen güvenlik kodunu kontrol ediniz.");
        //    return;
        //}
        //if (this.Session["GuvenlikKodu"].ToString() != guvenlikkodu.Text.Trim())
        //{
        //    mesajGoster("Lütfen güvenlik kodunu kontrol ediniz.");
        //    return;
        //}

        if (!string.IsNullOrEmpty(name.Text) && !string.IsNullOrEmpty(password.Text))
        {
            // şifre enkript
            //Crypt crypt = new Crypt();
            //string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
            //string Sifre = crypt.EncryptData(encryptKey, password.Text);
            //Response.Write(Sifre);

            DataTable dtKullaniciX = bllUyelikIslemleri.MusteriKullaniciGirisWithCustomerID(name.Text, password.Text);
            if (dtKullaniciX.Rows.Count > 0)
            {
                int kullaniciID = bllUyelikIslemleri.MusteriKullaniciGiris(dtKullaniciX.Rows[0]["Email"].ToString(), password.Text);
                if (kullaniciID > 0)
                {
                    DataTable dtKullanici = bllUyelikIslemleri.GetirKullanici(kullaniciID);
                    if (bool.Parse(dtKullanici.Rows[0]["Aktif"].ToString()))
                    {
                        //if (int.Parse(dtKullanici.Rows[0]["GirisSayisi"].ToString()) <= 0)
                        //{
                        //    Session["TempLoginAccount"] = dtKullanici;

                        //    //try
                        //    //{
                        //    //    writer.WriteLine("Login-Create-Password.aspx");
                        //    //}
                        //    //catch (Exception)
                        //    //{ }
                        //    //finally { writer.Close(); }

                        //    Response.Redirect("Login-Create-Password.aspx");
                        //}
                        //else
                        //{
                        //    try
                        //    {
                        //        writer.WriteLine("Giriş 0'dan büyük.");
                        //    }
                        //    catch (Exception)
                        //    { }
                        //    finally { writer.Close(); }
                        //}

                        //Create a new cookie, passing the name into the constructor
                        HttpCookie hcookie = new HttpCookie("HNetLogin");
                        //Set the cookies value
                        hcookie.Value = "true";
                        //Set the cookie to expire in 1 hour
                        DateTime dtNow = DateTime.Now;
                        TimeSpan tsHour = new TimeSpan(0, 1, 0, 0);
                        hcookie.Expires = dtNow + tsHour;
                        //Add the cookie
                        Response.Cookies.Add(hcookie);

                        Session["Email"] = dtKullanici.Rows[0]["Email"].ToString();
                        Session["AdSoyad"] = dtKullanici.Rows[0]["KullaniciAd"].ToString() + dtKullanici.Rows[0]["KullaniciSoyad"].ToString();
                        Session["Tel"] = dtKullanici.Rows[0]["CepTelKod"].ToString();

                        bllUyelikIslemleri.KullaniciSistemKayit(kullaniciID);

                        Session["AcenteKodu"] = name.Text.Trim();

                        //try
                        //{
                        //    writer.WriteLine("Giriş başarılı, yönlendirildi.");
                        //}
                        //catch (Exception)
                        //{ }
                        //finally { writer.Close(); }
                        if (Session["sonUrlV1"] != null)
                        {
                            string sonUrlV1 = Session["sonUrlV1"].ToString();
                            Session.Remove("sonUrlV1");
                            Response.Redirect(sonUrlV1);
                        }
                        else
                        {
                            Response.Redirect("Default.aspx");
                        }

                    }
                    else
                    {

                        //try
                        //{
                        //    writer.WriteLine("Acente hesabınız PASİF görünmektedir. Olası bir hata sonucu bu hatayı aldığınızı düşünüyorsanız lütfen Motul ile iletişime gerçiniz...");
                        //}
                        //catch (Exception)
                        //{ }
                        //finally { writer.Close(); }


                        mesajGoster("Hesabınız PASİF görünmektedir. Olası bir hata sonucu bu hatayı aldığınızı düşünüyorsanız lütfen Motul ile iletişime gerçiniz...");
                    }
                }
                else
                {
                    //try
                    //{
                    //    writer.WriteLine("E-postayı görmedi.");
                    //}
                    //catch (Exception)
                    //{ }
                    //finally { writer.Close(); }
                }
            }
            else
            {
                //try
                //{
                //    writer.WriteLine("Acente kodunuz ve/veya Şifreniz hatalıdır!");
                //}
                //catch (Exception)
                //{ }
                //finally { writer.Close(); }

                mesajGoster("Telefon numaranız ve/veya Şifreniz hatalıdır!");
            }
        }
        else
        {
            //try
            //{
            //    writer.WriteLine("Şifreyi görmedi");
            //}
            //catch (Exception)
            //{ }
            //finally { writer.Close(); }
        }
    }

    public void mesajGoster(string mesaj)
    {
        string strMessage = "";
        strMessage = mesaj;
        //string strScript = "<script type=\"text/javascript\">" +
        //        "$(document).ready(function(){" +
        //        "$.msgbox(\"" + strMessage + "\", { buttons: [ {type: \"submit\", value: \"Tamam\"} ] });" +
        //        "});" +
        //        "</script>";

        string strScript = "<script>alert('" + mesaj + "')</script>";
        if ((!Page.ClientScript.IsStartupScriptRegistered("clientScript")))
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "clientScript", strScript);
        }
    }
}