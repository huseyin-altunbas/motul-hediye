﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Sifremi-Unuttum.aspx.cs" Inherits="Login" %>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Login</title>
    <link rel="stylesheet" href="css/login-screen/reset.css">
    <link rel="stylesheet" href="css/login-screen/animate.css">
    <link rel="stylesheet" href="css/login-screen/styles.css">

    <script src="js/jquery-1.10.1.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <link href="css/jquery.msgbox.css" rel="stylesheet" />
    <script src="js/jquery.msgbox.min.js"></script>
</head>
<body>
    <div id="container">
        <a href="Default.aspx">
            <img style="padding: 20px 0px 0px 34px;" src="images/logo.png" alt="" border="0">
        </a>
        <form id="form1" runat="server">
            <label for="name">E-Posta Adresiniz</label>
            <asp:TextBox ID="epostaadresiniz" runat="server" ValidationGroup="a101"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="ValidationCSS1" runat="server" Display="None" ControlToValidate="epostaadresiniz" ErrorMessage="Lütfen E-Posta Adresinizi Girin!" ValidationGroup="a101"></asp:RequiredFieldValidator>
            <div style="clear: both;"></div>
            <div style="padding-right: 0px; text-align: center;">
                <asp:Image ID="Image1" ImageUrl="" runat="server" />
            </div>
            <label for="username" style="float: left;">Güvenlik Kodu:</label>
            <p>
                <asp:TextBox ID="guvenlikkodu" TextMode="Password" runat="server" ValidationGroup="a101"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="ValidationCSS1" runat="server" Display="None" ControlToValidate="guvenlikkodu" ErrorMessage="Resimde Gördüğünüz Güvenlik Kodunuz Giriniz!" ValidationGroup="a101"></asp:RequiredFieldValidator>
                <div id="lower">

                    <asp:Button ID="Button1" ValidationGroup="a101" runat="server" Text="Şifremi Gönder" Width="150" OnClick="Button1_Click" />
                    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="a101" ShowMessageBox="true" ShowSummary="false" runat="server" />
                </div>
        </form>
    </div>
</body>
</html>

