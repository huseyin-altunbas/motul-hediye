﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hammock;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using Goldb2cEntity;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data;
using Goldb2cBLL;
using Goldb2cInfrastructure;

public partial class FacebookCallback : System.Web.UI.Page
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["code"]) && !Page.IsPostBack)
        {
            VeriCek();
        }
        else
        {
            Response.Redirect("/Default.aspx");
        }
    }
    private void VeriCek()
    {
        RestClient client = new RestClient { Authority = "https://graph.facebook.com/oauth/" };
        RestRequest request = new RestRequest { Path = "access_token" };
        request.AddParameter("client_id", CustomConfiguration.Facebook_client_id);
        request.AddParameter("redirect_uri", CustomConfiguration.Facebook_redirect_url);
        request.AddParameter("client_secret", CustomConfiguration.Facebook_client_secret);
        request.AddParameter("code", Request.QueryString["code"]);
        RestResponse response = client.Request(request);
        StringDictionary result = Parse(response.Content);
        string appToken = result["access_token"];
        Detay(appToken);
    }
    private void Detay(string sToken)
    {
        RestClient client = new RestClient { Authority = "https://graph.facebook.com/" };
        RestRequest request = new RestRequest { Path = "me" };
        request.AddParameter("access_token", sToken);
        RestResponse response = client.Request(request);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        FacebookUser facebookuser = ser.Deserialize<FacebookUser>(response.Content);
        if (facebookuser != null && !string.IsNullOrEmpty(facebookuser.id))
        {
            int KullaniciId = 0;
            DataTable dtKullanici = bllUyelikIslemleri.GetirKullaniciByFacebookId(facebookuser.id);
            if (dtKullanici != null && dtKullanici.Rows.Count > 0)
            {
                KullaniciId = Convert.ToInt32(dtKullanici.Rows[0]["KullaniciId"]);
                Kullanici kullanici = bllUyelikIslemleri.KullaniciSistemKayit(KullaniciId);
            }
            else
            {
                dtKullanici = bllUyelikIslemleri.GetirKullaniciByEmail(facebookuser.email);

                if (dtKullanici != null && dtKullanici.Rows.Count > 0)
                {
                    KullaniciId = Convert.ToInt32(dtKullanici.Rows[0]["KullaniciId"]);
                    bllUyelikIslemleri.GuncelleKullaniciFacebookId(KullaniciId, facebookuser.id);
                    Kullanici kullanici = bllUyelikIslemleri.KullaniciSistemKayit(KullaniciId);
                }
                else
                {
                    Session["FacebookUser"] = facebookuser;
                    Session["FacebookEsksikBilgi"] = true;
                }
            }

            if (Session["FbConnectSonUrl"] != null && !string.IsNullOrEmpty(Session["FbConnectSonUrl"].ToString()))
                Response.Redirect(Session["FbConnectSonUrl"].ToString(), false);
            else
                Response.Redirect("~/Default.aspx");
            //Response.Write(facebookuser.name + "/" + facebookuser.id + "<br><img src=\"https://graph.facebook.com/" + facebookuser.id + "/picture\">");
        }
    }

    private StringDictionary Parse(string queryString)
    {
        queryString = queryString + "&";
        StringDictionary outc = new StringDictionary();
        Regex r = new Regex(@"(?<name>[^=&]+)=(?<value>[^&]+)&", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        IEnumerator enums = r.Matches(queryString).GetEnumerator();
        while (enums.MoveNext() && enums.Current != null)
        {
            outc.Add(((Match)enums.Current).Result("${name}"), ((Match)enums.Current).Result("${value}"));
        }
        return outc;
    }
}