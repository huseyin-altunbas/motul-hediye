﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Goldb2cBLL;
using Goldb2cInfrastructure;
using Goldb2cEntity;
using System.Xml.Linq;

public partial class Menu : System.Web.UI.Page
{
    KategoriIslemleriBLL bllKategoriIslemleri = new KategoriIslemleriBLL();
    DataTable dtMarka;
    Kullanici kullanici;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["menuitemid"] != null)
        {
            int menuItemId;
            Int32.TryParse(Request.QueryString["menuitemid"].ToString(), out menuItemId);
            dtMarka = bllKategoriIslemleri.GetirKategoriMenuItem(menuItemId);

            if (dtMarka != null && dtMarka.Rows.Count > 0)
            {
                lblMenuAd.Text = dtMarka.Rows[0]["ItemName"].ToString();

                string topLink = "<a class=\"home\" href=\"/\" title=\"Ana Sayfa\">Ana Sayfa</a> <span class=\"navigation-pipe\">"
                                + "&nbsp;</span> <span class=\"navigation_page\">Menü</span>";

                ltrAyakizi.Text = topLink;

                string divs = string.Empty;
                divs = dtMarka.Rows[0]["ItemHtml"].ToString();

                string newDivs = "<divs>" + divs + "</divs>";
                if (newDivs.Contains("&amp;"))
                    newDivs = newDivs.Replace("&amp;", "&");
                if (newDivs.Contains("&amp"))
                    newDivs = newDivs.Replace("&amp", "&");//alt satırda &amp; -> &amp;amp; olmaması için.
                newDivs = newDivs.Replace("&", "&amp;");

                XDocument xDoc = new XDocument();
                var xmlDivs = XElement.Parse(newDivs);
                xDoc.Add(xmlDivs);

                var liCategory = xDoc.Descendants("div").Where(element => element.Attribute("class").Value == "category").ToList();

                List<MenuCategory> lstMenuCategory = new List<MenuCategory>();
                MenuCategory menuCategory;
                foreach (var item in liCategory)
                {
                    var categoryImg = item.Descendants("a").Where(element => element.Attribute("class").Value == "img").ToList();
                    var categoryName = item.Descendants("a").Where(element => element.Attribute("class").Value == "categoryName").ToList();

                    menuCategory = new MenuCategory();
                    menuCategory.CategoryImage = categoryImg.Count > 0 ? categoryImg[0].ToString() : string.Empty;
                    menuCategory.CategoryName = categoryName.Count > 0 ? categoryName[0].ToString() : string.Empty;

                    lstMenuCategory.Add(menuCategory);
                }

                rptMenu.DataSource = lstMenuCategory;
                rptMenu.DataBind();
            }
            else
                Response.Redirect("/");
        }
    }
}

public class MenuCategory
{
    public string CategoryImage { get; set; }
    public string CategoryName { get; set; }
}