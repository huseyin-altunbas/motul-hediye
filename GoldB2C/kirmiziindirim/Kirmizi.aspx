﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="Kirmizi.aspx.cs" Inherits="kirmiziindirim_Kirmizi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <sscript src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></sscript>
    <script>        (function (H) { H.className = H.className.replace(/\bno-js\b/, 'js') })(document.documentElement)</script>
    <style>
        .js #features
        {
            margin-left: -12000px;
            width: 100%;
        }
    </style>
    <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame 
	   Remove this if you use the .htaccess -->
    <!--  Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS : implied media="all" -->
    <link rel="stylesheet" href="css/style.css?v=2.1">
    <link rel="stylesheet" href="wow_book/wow_book.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/preview.css">
    <script src="./js/mylibs/less-1.0.41.min.js" type="text/javascript"></script>
    <!-- Uncomment if you are specifically targeting less enabled mobile browsers
	<link rel="stylesheet" media="handheld" href="css/handheld.css?v=2">  -->
    <link href='http://fonts.googleapis.com/css?family=News+Cycle' rel='stylesheet' type='text/css'>
    <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
    <script src="js/libs/modernizr-1.6.min.js"></script>
    <div id="container">
        &nbsp;
        <div id="main">
            <div id='features'>
                <div id='cover'>
                    <img src="pictures/Agustos_gazete_insert01.jpg" alt="" />
                </div>
                <div>
                    <img src="pictures/Agustos_gazete_insert02.jpg" alt="" />
                </div>
                <div>
                    <img src="pictures/Agustos_gazete_insert03.jpg" alt="" />
                </div>
                <div>
                    <img src="pictures/Agustos_gazete_insert04.jpg" alt="" />
                </div>
                <div>
                    <img src="pictures/Agustos_gazete_insert05.jpg" alt="" />
                </div>
                <div>
                    <img src="pictures/Agustos_gazete_insert06.jpg" alt="" />
                </div>
                <div>
                    <img src="pictures/Agustos_gazete_insert07.jpg" alt="" />
                </div>
                <div id='cover'>
                    <img src="pictures/Agustos_gazete_insert08.jpg" alt="" />
                </div>
            </div>
            <!-- features -->
        </div>
        <nav>
		<ul>
			<li><a id='first' href="#" title='Başa dön'>Başa Dön</a></li>
			<li><a id='back' href="#" title='Geri'>Geri</a></li>
			<li><a id='next' href="#" title='İleri'>İleri</a></li>
			<li><a id='last' href="#" title='Son Sayfa'>Son Sayfa</a></li>
			<li><a id='zoomin' href="#" title='Büyüt'>Büyüt</a></li>
			<li><a id='zoomout' href="#" title='Küçült'>Küçült</a></li>
			<li><a id='slideshow' href="#" title='Slideshow'>Slideshow</a></li>
		</ul>
	</nav>
    </div>
    <!--! end of #container -->
    <!-- Javascript at the bottom for fast page loading -->
    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <script>        // !window.jQuery && document.write(unescape('%3Cscript src="js/libs/jquery-1.7.1.min.js"%3E%3C/script%3E'))</script>
    <script type="text/javascript" src="wow_book/wow_book.min.js"></script>
    <script type="text/javascript">
		$(document).ready(function() {
			$('#features').wowBook({
				 height : 500
				,width  : 800
				,centeredWhenClosed : true
				,hardcovers : true
				,turnPageDuration : 1000
				,numberedPages : [0]
				,transparentPages : true
				,controls : {
						zoomIn    : '#zoomin',
						zoomOut   : '#zoomout',
						next      : '#next',
						back      : '#back',
						first     : '#first',
						last      : '#last',
						slideShow : '#slideshow'
					},
			}).css({'display':'none', 'margin':'auto'}).fadeIn(1000);

			$("#cover").click(function(){
				$.wowBook("#features").advance();
			});
		});
	</script>
    <!-- scripts concatenated and minified via ant build script-->
    <script src="js/plugins.js"></script>
    <script src="js/script.js"></script>
    <!-- end concatenated and minified scripts-->
    <!--[if lt IE 7 ]>
	<script src="js/libs/dd_belatedpng.js"></script>
	<script> DD_belatedPNG.fix('img, .png_bg'); //fix any <img> or .png_bg background-images </script>
	<![endif]-->
</asp:Content>
