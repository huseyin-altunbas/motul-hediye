﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnBilgilendirme.aspx.cs"
    Inherits="Sozlesmeler_OnBilgilendirme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="font-family: Arial; font-size: 12pt; font-weight: bold">
        <b>ÖN BİLGİLENDİRME FORMU</b><br />
        <br />
        MADDE 1- SÖZLESMENIN TARAFLARI
        <br />
        SATICI:
        <br />
        Ünvanı : Lafarge Dalsan / Dalsan Alçı Sanayi ve Ticaret A.Ş.
        <br />
        Adresi : 1.cadde Sincap sokak No:12 Büyüksanayi 06060 Ankara-Türkiye
        <br />
        Ankara
        <br />
        Web Adresi : Anadoludanaspara.com<br />
        E-posta : info@Anadoludanaspara.com
        <br />
        Telefon Numarası: 0850 450 2635
        <br />
       
        <br />
        ALICI
        <br />
        Adı/Soyadı Varsa Unvanı: #AdSoyadUnvan#<br />
        <br />
        Adresi : #AliciAdres#,
        <br />
        <br />
        Telefon: #AliciTelefon#,<br />
        E-posta: #AliciEmail#
        <br />
        <br />
        #UrunBilgileri#
        <br />
        GEÇERLILIK SÜRESI:
        <br />
        <br />
        Listelenen ve siteden ilan edilen fiyatlar satış fiyatıdır.
        <br />
        İlan edilen fiyatlar ve vaatler güncelleme yapılana ve değiştirilene kadar geçerlidir.
        <br />
        Süreli olarak ilan edilen fiyatlar ise belirtilen süre sonuna kadar geçerlidir.
        <br />
        <br />
        ÖDEME SEKLI:
        <br />
        Ödemeler kredi kartı, eft, havale ve BKM Express ile yapılabilmektedir.
        <br />
        <br />
        MAL/HIZMETIN TESLIMATI:
        <br />
        Mal/hizmetin teslimi alıcının talep ettiği adreste kendisine yapılır. Alici, kendisinden
        başka birine ve de kendi adresinden başka bir adrese teslimat yapılmasını isterse,
        bu talebi doğrultusunda teslimat yapılır. Teslimat masrafları Alıcıya aittir. Satıcı,
        web sitesinde, ilan ettiği rakamın üzerinde alışveriş yapanların teslimat ücretinin
        kendisince karşılanacağını beyan etmişse, teslimat masrafı Satıcıya aittir. Mal/hizmet
        teslimatı kargo firmalarınca yapılmaktadır. Tüketici teslim almadan önce malin muayenesini
        yapmalı, ayıplı ve hasarlı mali kargo firması yetkilisinden teslim almamalıdır.
        Alici kargodan teslim alınan ürünün sağlam ve hasarsız olduğu kabul eder. Alicinin
        teslim öncesi mali muayene sorumluluğu vardır. Sipariş konusu mal/hizmetin teslimatı
        için mesafeli satış sözleşmesine elektronik ortamda gerekli teyidi vermiş olması
        ve bedelinin Alicinin tercih ettiği ödeme sekli ile ödenmiş olması şarttır. Herhangi
        bir nedenle mal/hizmet bedeli ödenmez veya banka kayıtlarında iptal edilir ise,
        Satıcı mal/hizmetin teslimi yükümlülüğünden kurtulmuş kabul edilir.
        <br />
        Siparişinizdeki bir ürünü iptal etmek istediğinizde, ürünün; sipariş numarasını,
        ürün kodunu, iptal adedini, telefon numaranızı belirterek Anadoludanaspara.com
        e-posta adresine gönderiniz veya Yukarı Dudullu Mahallesi Organize Sanayi Bölgesi
        1.Cadde No: 13/7 Ümraniye - İSTANBUL - TÜRKİYE adresine yazılı olarak gönderiniz.
        Sevkiyata başlamış ise para iadenizin yapılabilmesi için Tüketicinin Mevzuatında
        düzenlenen cayma hakkına ilişkin iade süreçlerini izlemeniz gerekecektir. İptal
        taleplerinize ilişkin kredi kartınıza yapılacak iptali, bankanıza bağlı olarak,
        iptal işlemleriniz Satıcı tarafından gerçekleştirildikten sonraki 10 iş günü içinde
        kredi kartınızda görebilirsiniz. İadeniz hangi karttan alışveriş yaptıysanız o karta
        10 gün içinde gerçekleştirilecektir.
        <br />
        İade etmek istediğiniz ürünleri, faturanızın üzerindeki iade bölümünü doldurarak,
        faturanızın bir nüshası ile birlikte Yukarı Dudullu Mahallesi Organize Sanayi Bölgesi
        1.Cadde No: 13/7 Ümraniye - İSTANBUL - TÜRKİYE adresine gönderebilirsiniz.<br />
        <br />
        <br />
        <span style="font-size: 16pt; font-weight: bold;">CAYMA HAKKI: Alici, sözleşme konusu
            mal/hizmetin kendisine veya gösterdiği adresteki kişi/kuruluşa tesliminden itibaren
            7 (yedi) gün içinde herhangi bir gerekçe göstermeksizin ve cezai şart ödemeksizin
            cayma hakkini kullanabilir. Cayma hakkının kullanılması için Cayma hakkının kullanıldığına
            ilişkin bildirimin 7 günlük süre içerisinde sürekli bir veri sağlayıcısı ile ya
            da yazılı olarak yapılması yeterli sayılmıştır. Cayma hakkı süresi, sözleşmenin
            akdedildiği günden itibaren işlemeye başlar.<br />
            SATICI'ya mevzuat hükümlerine uygun olarak bildirimde bulunulması şarttır. Cayma
            hakkının kullanılması halinde; ALICI'ya veya onun bildirdiği yukarıda bilgisi bulunan
            üçüncü kişiye teslim edilen ürünün faturasının üzerinde yer alan iade ile ilgili
            kısımların tüketici tarafından doldurulması zorunludur. 7 (Yedi) günlük süre içerisinde
            iade edilecek ürünlerin kutusu, ambalajı, varsa standart aksesuarları ile birlikte
            teslim edilmesi gerekmektedir. Ambalaj ve içeriğinin denenirken hasar görmemiş olası
            şarttır. Bu hakkin kullanılması halinde, 3. kişiye veya Alıcıya teslim edilen mal/hizmete
            ilişkin fatura aslinin iadesi zorunludur Fatura asli gönderilmezse Alıcıya KDV ve
            varsa diğer yasal yükümlülükler iade edilemez. Ürünle birlikte hediye verilen satışlarda,
            cayma hakkı kullanılır ise, verilen hediye de ALICI tarafından iade edilecektir.
            <br />
            Tüketicinin cayma hakkını kullanması halinde; satıcı veya sağlayıcının cayma bildiriminin
            kendisine ulaşmasından itibaren en geç on gün içerisinde almış olduğu toplam bedeli
            ve tüketiciyi borç altına sokan her türlü belgeyi, tüketiciye hiçbir masraf yüklemeksizin
            iade etmek ve yirmi gün içerisinde malı almakla yükümlüdür. Cayma hakki nedeni ile
            iade edilen mal/hizmetin teslimat bedeli Satıcı tarafından karşılanır.
            <br />
            <br />
            CAYMA HAKKI KULLANILAMAYACAK MAL/HIZMETLER:
            <br />
            Taraflarca aksi kararlaştırılmadıkça tüketici, aşağıdaki sözleşmelerde cayma hakkını
            kullanamaz.<br />
            a) Cayma hakkı süresi sona ermeden önce, tüketicinin onayı ile hizmetin ifasına
            başlanan hizmet sözleşmeleri.<br />
            b) Fiyatı borsa veya teşkilatlanmış diğer piyasalarda belirlenen mallara ilişkin
            sözleşmeler.<br />
            c) Tüketicinin istekleri veya açıkça onun kişisel ihtiyaçları doğrultusunda hazırlanan,
            niteliği itibariyle geri gönderilmeye elverişli olmayan ve çabuk bozulma tehlikesi
            olan veya son kullanma tarihi geçme ihtimali olan malların teslimine ilişkin sözleşmeler.<br />
            ç) Tüketici tarafından ambalajının açılmış olması şartıyla, ses veya görüntü kayıtlarına,
            yazılım programlarına ve bilgisayar sarf malzemelerine ilişkin sözleşmeler.<br />
            d) Gazete, dergi gibi süreli yayınların teslimine ilişkin sözleşmeler.
            <br />
            e) Bahis ve piyangoya ilişkin hizmetlerin ifasına ilişkin sözleşmeler.<br />
            f) Elektronik ortamda anında ifa edilen hizmetler ve tüketiciye anında teslim edilen
            gayri maddi mallara ilişkin sözleşmeler.
            <br />
        </span>
        <br />
        ILETISIM ÜCRETLERI:
        <br />
        <br />
        Her nevi iletişim bedeli Alıcıya aittir.
        <br />
        <br />
        MAL/HIZMETIN TESLIM ZAMANI:
        <br />
        Teslimat; mal bedelinin Satıcının hesabına geçmesinden sonra mümkün olan en kısa
        sürede yapılır. Doğal afetler, hava muhalefeti vs. gibi mücbir sebeplerle gecikmeler
        olabilir. Satıcı, mal/hizmetin siparişinden itibaren 30 (Otuz) gün içinde teslim
        eder ve bu süre içinde yazılı bildirimle veya sürekli veri taşıyıcısı ile ek 10
        (on) günlük süre uzatım hakkini saklı tutar.
        <br />
        <br />
        TALEP VE SIKÂYETLERIN ILETILMESI:
        <br />
        <br />
        Alici, her türlü talep ve şikâyetlerini Satıcının 1.maddede belirtilen iletişim
        adreslerine yapabilir.
        <br />
        <br />
        Tüketiciler, Satıcı ile sorunları olur ise, sorunu Satıcı'nın çözememesi durumda,
        ürün bedeli 1.031,87 TL'ye kadar olan işlemlerle ilgili şikâyet ve itiraz konularında
        başvurularını; ürünü satıl aldığı veya ikametgâhının bulunduğu yerdeki Tüketici
        Sorunları Hakem Heyetine; Ürün bedeli 1.031,87 TL'nin üzerinde olan işlemlerle ilgili
        şikâyet ve itiraz konularında başvurularını ise, ürünü satın aldığı veya ikametgâhının
        bulunduğu yerdeki Tüketici mahkemesine yapabilecektir.
        <br />
        <br />
        “Tüm bu bilgiler, kullanılan uzaktan iletişim araçlarına uygun olarak ve iyiniyet
        ilkeleri çerçevesinde ergin olmayanlar ile ayırtım gücünden yoksun ve kısıtlı erginleri
        koruyacak şekilde ticari amaçlarla verilmiştir.” İlgili yönetmeliğin 5. Maddesinin
        3.fıkrası gereği bu eklemenin yapılması zorunlu kılınmıştır.<br />
        Alici, 4822 S. K. ile değişik 4077 S.K.un M. 9/A, f.2 ve Mes. Söz. Yön. 5. ve 6.
        maddeleri gereğince Ön Bilgileri okuyup bilgi sahibi olduğunu ve elektronik ortamda
        gerekli teyidi verdiğini kabul, taahhüt ve beyan eder.
        <br />
    </div>
    </form>
</body>
</html>
