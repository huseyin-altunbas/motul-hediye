﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AvansPuan.aspx.cs" Inherits="Sozlesmeler_MesafeliSatis" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="font-family: Arial; font-size: 12pt; font-weight: bold;">
        <b>Avans Puan Kullanım Kuralları</b><br />
        <br />
        <b>Avans Puan, Alçıkart üyelerine özel bir uygulamadır. Alçıkart
         üyesi olmayan bu uygulamadan yararlanamaz. • Avans Puan uygulamasından
        yararlanmak için puan gönderiminizle birlikte hediye talebinizin de bulunması gerekmektedir.
        • Alçıkart üyeleri gönderdikleri puan toplamı kadar Avans Puan kullanmayı
        hak ederek hediye talebinde bulunabilirler. • Bir yıl içinde kullanılabilecek en
        yüksek Avans Puan miktarı, 250.000 Alçıkart  ile sınırlıdır. • Kullanılan Avans
        Puan bakiyesinin, kullanım tarihinden itibaren geçerli olacak şekilde 1 yıl içerisinde
        kapatılması gerekir. • Avans Puan kullanıldığında tekrar Avans Puan kullanılabilmesi
        için bir önceki Avans Puan bakiyesinin kapatılmış olması gerekir. • Avans Puan kullanımından
        sonra gönderilecek yeni puanlar ile öncelikle Avans Puan bakiyesi kapatılır. • Alınan
        Avans Puan, talep edilen aynı gün içinde değiştirilebilir veya iptal edilebilir.
        • 1(Bir) Avans Puan bedeli 0,005 TL’dir. • Alçıkart , 1 yıl içinde tamamlanmayan
        Avans Puan bedeli için fatura keser. • Alçıkart ödenmeyen faturalarla ilgili hukuki
        işlemleri başlatma hakkını saklı tutar. • Avans Puan uygulamasından yararlanan Alçıkart 
         üyeleri bu kuralları kabul etmiş sayılır. • Alçıkart  Avans Puan
        uygulamasında her türlü değişiklik yapma ve kaldırma hakkını saklı tutar.
    </div>
    </b>
    <p>
        <div id="AvansButtonKontrol" runat="server">
            <div id="AvansButton2" runat="server" style="float: right;">
                <asp:LinkButton ID="btnGonder0" runat="server" CssClass="link-button-RedBtn spriteRedBtn"
                    OnClick="btnIptal_Click"><span class="spriteRedBtn fontTre fontSz14">İptal</span></asp:LinkButton>
            </div>
            <div id="AvansButton1" runat="server" >
                <asp:LinkButton ID="btnGonder" runat="server" CssClass="link-button-RedBtn spriteRedBtn"
                    OnClick="btnOnayla_Click"><span class="spriteRedBtn fontTre fontSz14">
                                             Avans Puan Kullanım Kurallarını Okudum Kabul Ediyorum </span></asp:LinkButton>
            </div>
        </div>
    </p>
    </form>
</body>
</html>
