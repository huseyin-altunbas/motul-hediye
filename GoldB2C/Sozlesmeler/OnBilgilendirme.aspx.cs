﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Goldb2cBLL;
using System.Data;

public partial class Sozlesmeler_OnBilgilendirme : System.Web.UI.Page
{
    SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        /*
         * AdSoyadUnvan
         * AliciAdres
         * AliciTelefon
         * AliciEmail
         * UrunBilgileri
         * 
         */

        string _SiparisMasterId = "";

        if (!string.IsNullOrEmpty(Request.QueryString["SiparisMasterId"]))
            _SiparisMasterId = Request.QueryString["SiparisMasterId"];

        string TarihSaat = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        string AdSoyadUnvan = "",
            AliciAdres = "",
            AliciTelefon = "",
            AliciEmail = "",
            UrunBilgileri = "";

        DataTable dtSipBilgilendirmeFormu = new DataTable();
        int SiparisMasterId = 0;
        if ((Session["SiparisMasterId"] != null && int.TryParse(Session["SiparisMasterId"].ToString(), out SiparisMasterId) && SiparisMasterId > 0) || (!string.IsNullOrEmpty(_SiparisMasterId) && int.TryParse(_SiparisMasterId, out SiparisMasterId)) && SiparisMasterId > 0)
        {
            dtSipBilgilendirmeFormu = bllSiparisGoruntuleme.SiparisBilgilendirmeFormu(SiparisMasterId);
            if (dtSipBilgilendirmeFormu != null && dtSipBilgilendirmeFormu.Rows.Count > 0)
            {
                DataRow dr = dtSipBilgilendirmeFormu.Rows[0];

                if (!string.IsNullOrEmpty(dr["alici_unvan"].ToString()))
                    AdSoyadUnvan = dr["alici_unvan"].ToString();
                else if (!string.IsNullOrEmpty(dr["alici_unvan"].ToString()))
                    AdSoyadUnvan = dr["alici_adsoyad"].ToString();

                if (!string.IsNullOrEmpty(dr["alici_adres"].ToString()))
                    AliciAdres = dr["alici_adres"].ToString();

                if (!string.IsNullOrEmpty(dr["alici_telefon"].ToString()))
                    AliciTelefon = dr["alici_telefon"].ToString();

                if (!string.IsNullOrEmpty(dr["alici_email"].ToString()))
                    AliciEmail = dr["alici_email"].ToString();

                StringBuilder sbUrunBilgileri = new StringBuilder();
                foreach (DataRow drow in dtSipBilgilendirmeFormu.Rows)
                {
                    sbUrunBilgileri.AppendLine("MAL/HIZMETIN ÖZELLIKLERI:<br />");
                    sbUrunBilgileri.AppendLine("Ürünün Adı : " + drow["urun_ad"].ToString() + "<br />");
                    sbUrunBilgileri.AppendLine("Ürünün Markası : " + drow["urun_marka"].ToString() + "<br />");
                    sbUrunBilgileri.AppendLine("Ürünün Miktarı/Adedi : " + drow["satis_adet"].ToString() + "<br />");
                    sbUrunBilgileri.AppendLine("Ürünün Fiyatı : " + drow["satis_tutar"].ToString() + "TL<br />");
                    sbUrunBilgileri.AppendLine("Ürünün Vergisi : " + drow["urun_vergi"].ToString() + "TL<br />");
                    sbUrunBilgileri.AppendLine("Ürünün Özellikleri : <br />" + drow["urun_ozellik"].ToString() + "<br />");

                    sbUrunBilgileri.AppendLine("MAL/HIZMETIN TÜM VERGILER DÂHIL TESLIM FIYATI:<br />");

                    sbUrunBilgileri.AppendLine("Ürünün Kargo Ücreti : " + drow["kargo_ucret"].ToString() + "TL <br />");

                    sbUrunBilgileri.AppendLine("Ürünün Teslim Fiyatı : " + drow["satis_tutar"].ToString() + "TL <br /> <br />");
                }
                UrunBilgileri = sbUrunBilgileri.ToString();
            }

        }


        string Icerik = form1.InnerHtml;

        Icerik = Icerik.Replace("#TarihSaat#", TarihSaat);
        Icerik = Icerik.Replace("#AdSoyadUnvan#", AdSoyadUnvan);
        Icerik = Icerik.Replace("#AliciAdres#", AliciAdres);
        Icerik = Icerik.Replace("#AliciTelefon#", AliciTelefon);
        Icerik = Icerik.Replace("#AliciEmail#", AliciEmail);
        Icerik = Icerik.Replace("#UrunBilgileri#", UrunBilgileri);


        form1.InnerHtml = Icerik;




    }
}