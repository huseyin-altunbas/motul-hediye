﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using System.Text;

public partial class Sozlesmeler_MesafeliSatis : System.Web.UI.Page
{
    SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        /*
         * Replace edilecek alanlar
         * TarihSaat
         * AdSoyadUnvan
         * AliciAdres
         * AliciTelefon
         * AliciEmail
         * OdemeTur
         * FaturaAdres
         * TeslimAlacakKisi
        
         */

        string _SiparisMasterId = "";

        if (!string.IsNullOrEmpty(Request.QueryString["SiparisMasterId"]))
            _SiparisMasterId = Request.QueryString["SiparisMasterId"];



        string TarihSaat = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        string AdSoyadUnvan = "",
            AliciAdres = "",
            AliciTelefon = "",
            AliciEmail = "",
            UrunBilgileri = "",
            OdemeTur = "",
            FaturaAdres = "",
            TeslimAlacakKisi = "";

        if (!string.IsNullOrEmpty(Request.QueryString["OdemeTur"]))
        {
            OdemeTur = Request.QueryString["OdemeTur"];
            if (OdemeTur == "KK")
                OdemeTur = "Kredi Kartı";
            else if (OdemeTur == "HVL")
                OdemeTur = "Havale";
            else if (OdemeTur == "BKM")
                OdemeTur = "BKM Express";
        }

        DataTable dtSipBilgilendirmeFormu = new DataTable();
        int SiparisMasterId = 0;
        if ((Session["SiparisMasterId"] != null && int.TryParse(Session["SiparisMasterId"].ToString(), out SiparisMasterId) && SiparisMasterId > 0) || (!string.IsNullOrEmpty(_SiparisMasterId) && int.TryParse(_SiparisMasterId, out SiparisMasterId)) && SiparisMasterId > 0)
        {
            dtSipBilgilendirmeFormu = bllSiparisGoruntuleme.SiparisBilgilendirmeFormu(SiparisMasterId);
            if (dtSipBilgilendirmeFormu != null && dtSipBilgilendirmeFormu.Rows.Count > 0)
            {
                DataRow dr = dtSipBilgilendirmeFormu.Rows[0];

                if (!string.IsNullOrEmpty(dr["alici_unvan"].ToString()))
                    AdSoyadUnvan = dr["alici_unvan"].ToString();
                else if (!string.IsNullOrEmpty(dr["alici_unvan"].ToString()))
                    AdSoyadUnvan = dr["alici_adsoyad"].ToString();

                if (!string.IsNullOrEmpty(dr["alici_adres"].ToString()))
                    AliciAdres = dr["alici_adres"].ToString();

                if (!string.IsNullOrEmpty(dr["alici_telefon"].ToString()))
                    AliciTelefon = dr["alici_telefon"].ToString();

                if (!string.IsNullOrEmpty(dr["alici_email"].ToString()))
                    AliciEmail = dr["alici_email"].ToString();

                if (!string.IsNullOrEmpty(dr["fatura_adres"].ToString()))
                    FaturaAdres = dr["fatura_adres"].ToString();

                if (!string.IsNullOrEmpty(dr["teslimat_isim"].ToString()))
                    TeslimAlacakKisi = dr["teslimat_isim"].ToString();



                StringBuilder sbUrunBilgileri = new StringBuilder();
                foreach (DataRow drow in dtSipBilgilendirmeFormu.Rows)
                {
                    sbUrunBilgileri.AppendLine("<b>Malın Adı :</b>" + drow["urun_ad"].ToString() + "<br />");
                    sbUrunBilgileri.AppendLine("<b>Malın Adedi :</b>" + drow["satis_adet"].ToString() + "<br />");
                    sbUrunBilgileri.AppendLine("<b>Satış Bedeli(KDV Dahil):</b>" + drow["satis_tutar"].ToString() + "<br /><br />");
                }
                UrunBilgileri = sbUrunBilgileri.ToString();
            }

        }

        string Icerik = form1.InnerHtml;

        Icerik = Icerik.Replace("#TarihSaat#", TarihSaat);
        Icerik = Icerik.Replace("#AdSoyadUnvan#", AdSoyadUnvan);
        Icerik = Icerik.Replace("#AliciAdres#", AliciAdres);
        Icerik = Icerik.Replace("#AliciTelefon#", AliciTelefon);
        Icerik = Icerik.Replace("#AliciEmail#", AliciEmail);
        Icerik = Icerik.Replace("#UrunBilgileri#", UrunBilgileri);
        Icerik = Icerik.Replace("#OdemeTur#", OdemeTur);
        Icerik = Icerik.Replace("#FaturaAdres#", FaturaAdres);
        Icerik = Icerik.Replace("#TeslimAlacakKisi#", TeslimAlacakKisi);

        form1.InnerHtml = Icerik;

    }
}