﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cEntity;
using System.Text;
using Goldb2cBLL;
using System.IO;
using System.Data;
using Goldb2cInfrastructure;

public partial class FacebookUyeKayit : System.Web.UI.Page
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    BannerBLL bllBanner = new BannerBLL();


    string HataMesaj = "";
    string RedirectUrl = "";
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!string.IsNullOrEmpty(Request.QueryString["RedirectUrl"]))
            RedirectUrl = Request.QueryString["RedirectUrl"];
        else
            RedirectUrl = "/Default.aspx";

        if (!IsPostBack)
        {
            dpSehir.DataSource = bllUyelikIslemleri.SehirListesi("TÜRKİYE");
            dpSehir.DataValueField = "SehirAd";
            dpSehir.DataTextField = "SehirAd";
            dpSehir.DataBind();
            dpSehir.Items.Insert(0, new ListItem("Yaşadığınız Şehri Seçiniz", ""));
        }

        if (Session["FacebookUser"] != null)
        {
            FacebookUser facebookUser = new FacebookUser();
            facebookUser = (FacebookUser)Session["FacebookUser"];
            lblAdSoyad.Text = facebookUser.first_name + " " + facebookUser.last_name;
        }
        else
        {
            Session.Abandon();
            ClientScript.RegisterStartupScript(typeof(string), "mesja", "<script type=\"text/javascript\">$(document).ready(function () { unblockDivKapatYenile('" + RedirectUrl + "'); });</script>");
        }
    }

    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        UyeKayit();
    }

    private void UyeKayit()
    {


        try
        {
            if (Session["FacebookUser"] != null)
            {
                FacebookUser facebookUser = new FacebookUser();
                facebookUser = (FacebookUser)Session["FacebookUser"];
                if (!string.IsNullOrEmpty(facebookUser.id))
                {
                    string FacebookId = facebookUser.id;
                    string Email = facebookUser.email;
                    string KullaniciAd = facebookUser.first_name;
                    string KullaniciSoyad = facebookUser.last_name;
                    string Cinsiyet = "";
                    if (facebookUser.gender == "male")
                        Cinsiyet = "E";
                    else
                        Cinsiyet = "K";

                    string[] DogumTarih = facebookUser.birthday.Split('/');
                    DateTime dtDogumTarih = new DateTime(Convert.ToInt32(DogumTarih[2]), Convert.ToInt32(DogumTarih[0]), Convert.ToInt32(DogumTarih[1]));

                    string Sifre = bllUyelikIslemleri.RandomPassword();
                    bool EmailHaber = Convert.ToBoolean(chkEposta.Checked);
                    bool CepHaber = Convert.ToBoolean(chkSMS.Checked);

                    int KullaniciID = bllUyelikIslemleri.MusteriKullaniciKayit(KullaniciAd, KullaniciSoyad, Email, Sifre, dtDogumTarih, Cinsiyet, "TR", dpSehir.SelectedValue, "", string.Empty, string.Empty, string.Empty, txtCepTelKod.Text.Trim(), txtCepTelNo.Text.Trim(), CepHaber, EmailHaber, "Facebook", string.Empty, string.Empty, string.Empty,"");

                    if (KullaniciID > 0)
                    {
                        bllUyelikIslemleri.GuncelleKullaniciFacebookId(KullaniciID, FacebookId);
                        Session.Remove("FacebookEsksikBilgi");
                        // kayıt başarılı
                        // kullanıcıyı sisteme kayıt et
                        Kullanici kullanici = bllUyelikIslemleri.KullaniciSistemKayit(KullaniciID);

                        //#todo : sipariş aşamalarında carisi açılacak
                        //bllUyelikIslemleri.CariVeriOlustur(user.CepTel, user.Tel, user.Cinsiyet, user.DogumTarihi, user.Email, user.ERPKod, user.IlceKod, user.MusteriAd, user.MusteriSoyad, user.RefEmail, user.SehirKod, user.UlkeKod, user.UyelikTarihi);

                        // şifreyi e-mail ile gönder

                        if (!string.IsNullOrEmpty(kullanici.Email))
                        {
                            string strHTML = File.ReadAllText(Server.MapPath("~/MailSablon/Hosgeldin.htm"));
                            strHTML = strHTML.Replace("#adsoyad#", kullanici.KullaniciAd + " " + kullanici.KullaniciSoyad);
                            string MailBanner = "";

                            DataTable dtBanner = new DataTable();
                            dtBanner = bllBanner.GetirBanner(3);
                            if (dtBanner != null && dtBanner.Rows.Count > 0)
                            {
                                MailBanner = dtBanner.Rows[0]["BannerHtml"].ToString();
                            }

                            string[] _MailBanner = MailBanner.Split(new string[] { "##" }, StringSplitOptions.None);
                            string BannerImage = _MailBanner[0];
                            string BannerLink = _MailBanner[1];

                            StringBuilder sb = new StringBuilder();
                            sb.AppendLine("<tr>");
                            sb.AppendLine("<td colspan=\"4\" width=\"720\" height=\"120\">");
                            sb.AppendLine("<div style=\"height: 120px\">");
                            if (!string.IsNullOrEmpty(BannerLink))
                                sb.AppendLine("<a href=\"" + BannerLink + "\">");
                            sb.AppendLine("<img alt=\"kampanya\" src=\"http://www.gold.com.tr/UrunResim/MarketingResim/banner/bilgimail/" + BannerImage + "\" style=\"border:none\"  />");
                            if (!string.IsNullOrEmpty(BannerLink))
                                sb.AppendLine("</a>");
                            sb.AppendLine("</div>");
                            sb.AppendLine("</td>");
                            sb.AppendLine("</tr>");

                            if (string.IsNullOrEmpty(MailBanner))
                                strHTML = strHTML.Replace("#mailbanner#", "");
                            else
                                strHTML = strHTML.Replace("#mailbanner#", sb.ToString());

                            SendEmail mail = new SendEmail();
                            mail.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
                            mail.MailTo = new string[] { kullanici.Email };
                            mail.MailHtml = true;
                            mail.MailSubject = "Motul Hediye Üyelik Onay";

                            mail.MailBody = strHTML;
                            bool mailSent = mail.SendMail(false);
                        }

                        ClientScript.RegisterStartupScript(typeof(string), "mesja", "<script type=\"text/javascript\">$(document).ready(function () { unblockDivKapatYenile('" + RedirectUrl + "'); });</script>");

                    }
                    else
                    {
                        HataMesaj = HataMesajEkle(HataMesaj, "Bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.");
                    }
                }
            }

        }
        catch (Exception err)
        {
            HataMesaj = HataMesajEkle(HataMesaj, "Sistemde Bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.");
        }

        if (!string.IsNullOrEmpty(HataMesaj))
            ltrHataMesaj.Text = HataMesaj;

    }

    private string HataMesajEkle(string HataMesaj, string EklenecekHata)
    {
        string Mesaj = "";
        Mesaj = HataMesaj + "<p><span><img src=\"imagesGld/fbCoonect/error.png\" alt=\"Hata\" /></span>" + EklenecekHata + "</p>";
        return Mesaj;
    }
    protected void btnVazgec_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        ClientScript.RegisterStartupScript(typeof(string), "mesja", "<script type=\"text/javascript\">$(document).ready(function () {  unblockDivKapatYenile('" + RedirectUrl + "'); });</script>");
    }
}