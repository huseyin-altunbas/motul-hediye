﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="UrunDetay.aspx.cs" Inherits="UrunDetay" EnableViewState="true" %>

<%@ MasterType VirtualPath="~/MpAna.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="/assets/lib/fancyBox/jquery.fancybox.css" />
    <script type="text/javascript" src="/assets/lib/jquery.elevatezoom.js"></script>
    <script type="text/javascript" src="/assets/lib/fancyBox/jquery.fancybox.js"></script>
    <asp:Literal ID="ltrMeta" runat="server"></asp:Literal>
    <%--<link rel="stylesheet" type="text/css" href="/css/uzmanGorusu.css?v=1.0.0.0.0" />--%>
    <link rel="stylesheet" type="text/css" href="/css/kampanya.css?v=1.2.3.4.2" />
    <link rel="stylesheet" type="text/css" href="/css/complementaryProducts.css?v=1" />
    <link rel="stylesheet" type="text/css" href="/css/otherProducts.css?v=1" />
    <link rel="stylesheet" type="text/css" href="/css/smilarProducts.css" />
    <link rel="stylesheet" type="text/css" href="/css/bestsellersProducts.css" />
    <link rel="stylesheet" type="text/css" href="/css/footerAddBasket.css?v=1.0.0.2" />
    <!--  FilterChechkbox  -->
    <script src="/js/jquery.uniform.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" charset="utf-8">
        $(function () {
            $("input:not(.bultenBTN .htFacebookLogin), textarea, select, button").uniform();
        });
    </script>
    <link rel="stylesheet" href="/css/uniform.default.css" type="text/css" media="screen" />
    <!--  FilterChechkbox  -->
    <script type="text/javascript">
        function CheckedCheckBox(chkbox) {
            chkbox.checked = true;
            $(chkbox).parent().attr('class', 'checked');
        }
    </script>
    <link rel="stylesheet" type="text/css" href="/Tools/LightBoxEvo/js/lightbox/themes/evolution-dark/jquery.lightbox.css?v=1.1" />
    <!--[if IE 6]>
  <link rel="stylesheet" type="text/css" href="/Tools/LightBoxEvo/js/lightbox/themes/evolution-dark/jquery.lightbox.ie6.css" />
  <![endif]-->
    <script type="text/javascript" src="/Tools/LightBoxEvo/js/lightbox/jquery.lightbox.min.js"></script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--Sosyal Medya-->
    <!-- Facebook -->
    <script type="text/javascript">
        !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = "//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } } (document, "script", "twitter-wjs");

        window.___gcfg = { lang: 'tr' };
        (function () {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();



    </script>
    <!--Sosyal Medya-->
    <!--  BankTicker  -->
    <script type="text/javascript" src="/js/jquery.vticker-min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.productTopBar .productBankInstalment .bankcontents').vTicker({
                speed: 500,
                pause: 5000,
                animation: 'fade',
                mousePause: true,
                showItems: 1,
                direction: 'up'
            });
        });
    </script>
    <!--  BankTicker  -->
    <!-- Repeater Radiobutton -->
    <script type="text/javascript">
        function SetUniqueRadioButton(nameregex, current) {
            re = new RegExp(nameregex);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'radio') {
                    if (re.test(elm.name)) {
                        elm.checked = false;
                    }
                }
            }
            current.checked = true;
        }

    </script>
    <!-- Repeater RadioButton -->
    <!--  ProductTabs  -->
    <link rel="stylesheet" href="/css/tabifty.css" type="text/css" />
    <script src="/js/tabifty.js?v=1.0.1" type="text/javascript"></script>
    <!--  ProductTabs  -->
    <!--  ProductGallery  -->
    <link type="text/css" href="/css/productgallery.css?v=1.0.0.1" rel="stylesheet" />
    <script type="text/javascript" src="/js/jquery.productgallery.js"></script>
    <script type="text/javascript">
        $(document).ready(
		function () {
		    $("#goldme").GoldChoose({ carousel: true, carouselVertical: true });
		});
    </script>
    <!--  ProductGallery  -->
    <!--resimler script-->
    <script src="/js/productmaingallery.js" type="text/javascript"></script>
    <!--resimler script-->
    <div class="columns-container">
        <div class="container" id="columns">
            <!-- breadcrumb -->
            <div class="breadcrumb clearfix">
                <asp:Literal ID="ltrAyakizi" runat="server"></asp:Literal>
            </div>
            <!-- ./breadcrumb -->
            <!-- row -->
            <div class="row">
                <!-- Center colunm-->
                <div class="center_column col-xs-12 col-sm-9 mt10" id="center_column">
                    <!-- Product -->
                    <div id="product">
                        <div class="primary-box row">
                            <div class="pb-left-column col-xs-12 col-sm-6">
                                <!-- product-imge-->
                                <div class="product-image">
                                    <div class="product-full">
                                        <asp:Repeater ID="rptUstSolBuyukResimler" runat="server">
                                            <ItemTemplate>
                                                <img id="product-zoom" src="<%# DataBinder.Eval(Container.DataItem, "resim_ad") %>"
                                                    data-zoom-image="<%# DataBinder.Eval(Container.DataItem, "resim_ad") %>" alt="<%# DataBinder.Eval(Container.DataItem, "resim_bilgi") %>" />
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <div class="product-img-thumb" id="gallery_01">
                                        <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20"
                                            data-loop="true">
                                            <asp:Repeater ID="rptUstSolKucukResimler" runat="server">
                                                <ItemTemplate>
                                                    <li><a href="#" data-image="<%# DataBinder.Eval(Container.DataItem, "resim_ad") %>"
                                                        data-zoom-image="<%# DataBinder.Eval(Container.DataItem, "resim_ad") %>">
                                                        <img src="<%# DataBinder.Eval(Container.DataItem, "resim_ad") %>" alt="<%# DataBinder.Eval(Container.DataItem, "resim_bilgi") %>" />
                                                    </a></li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>
                                <!-- product-imge-->
                            </div>
                            <div class="pb-right-column col-xs-12 col-sm-6">
                                <h1 class="product-name">
                                    <asp:Literal ID="ltrStokAd" runat="server"></asp:Literal></h1>
                                <div class="product-comments">
                                    <%--<div class="product-star">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                            class="fa fa-star"></i><i class="fa fa-star-half-o"></i>
                                    </div>
                                    <div class="comments-advices">
                                        <a href="#">Based on 3 ratings</a> <a href="#"><i class="fa fa-pencil"></i>write a review</a>
                                    </div>--%>
                                </div>
                                <div class="product-price-group">
                                    <span class="price">
                                        <asp:Literal ID="ltrNetTutar" runat="server"></asp:Literal>&nbsp;PUAN</span>
                                    <div id="DivIndirim" runat="server">
                                        <span class="old-price">
                                            <asp:Literal ID="ltrBrutTutar" runat="server"></asp:Literal>&nbsp;PUAN</span>
                                        <span class="discount">-<asp:Literal ID="ltrIndirimOran" runat="server"></asp:Literal>%</span>
                                    </div>                                   
                                </div>
                                 <span style="color:red"><%=bshAciklama %></span>
                                <div class="form-action">
                                    <div class="button-group">
                                
                                        <% Goldb2cEntity.Kullanici kullanici = new Goldb2cEntity.Kullanici();
                                            if (Session["Kullanici"] != null)
                                            {
                                                kullanici = (Goldb2cEntity.Kullanici)Session["Kullanici"];
                                            }
                                        %>
                                       
                                        <asp:LinkButton ID="btnSepeteEkle" runat="server" OnClick="btnSepeteEkle_Click" CssClass="btn-add-cart">Sepete Ekle</asp:LinkButton>

                                    </div>
                                    <div class="button-group">
                                        <%--<a class="wishlist" href="#"><i class="fa fa-heart-o"></i>
                                            <br>
                                            Wishlist</a> <a class="compare" href="#"><i class="fa fa-signal"></i>
                                                <br>
                                                Compare</a>--%>
                                    </div>
                                </div>
                                <div class="form-share">
                                    <div class="sendtofriend-print">
                                        <%--<a href="javascript:print();"><i class="fa fa-print"></i>Print</a> <a href="mailto:info@motulteam.com">
                                            <i class="fa fa-envelope-o fa-fw"></i>Send to a friend</a>--%>
                                    </div>
                                    <div class="network-share">
                                        <div class="social">
                                            <div class="socialHub">
                                                <div class="wPrc30 mr2 fleft">
                                                    <div class="fleft">
                                                        <div class="fb-like" data-href="https://motulteam.com/<%=this.Page.Request.RawUrl %>"
                                                            data-send="false" data-layout="button_count" data-width="85px" data-show-faces="true">
                                                        </div>
                                                    </div>
                                                    <div class="fleft w100">
                                                        <a href="https://twitter.com/share" class="twitter-share-button" data-lang="tr" data-size="medium">
                                                            Tweet </a>
                                                    </div>
                                                    <div class="fleft w50">
                                                        <g:plusone size="medium"></g:plusone>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--<a href="http://friendfeed.com/goldcomputer">
                                    <img src="/imagesgld/icon_friendfeed38x34.png" alt="friendfeed" title="friendfeed" /></a>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="DivFacebookComment" class="tabreviewcontent">
                            <div id="fb-root">
                            </div>
                            <script>                                (function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s); js.id = id;
                                    js.src = "//connect.facebook.net/tr_TR/all.js#xfbml=1&appId=388600557882151";
                                    fjs.parentNode.insertBefore(js, fjs);
                                } (document, 'script', 'facebook-jssdk'));</script>
                            <%--<div class="fb-comments" data-href="<%=SayfaUrl %>" data-num-posts="5" data-width="782">
                            </div>--%>
                        </div>
                        <!-- tab product -->
                        <div class="product-tab">
                            <ul class="nav-tab">
                                <li class="active"><a aria-expanded="false" data-toggle="tab" href="#product-opinion">
                                    ÖZELLİKLER</a> </li>
                                <%--<li><a aria-expanded="true" data-toggle="tab" href="#product-properties">ÖZELLİKLER</a>--%>
                                </li>
                                <%--<li><a data-toggle="tab" href="#product-pictures">DETAYLI RESİMLER</a> </li>--%>
                            </ul>
                            <div class="tab-container">
                                <div id="product-opinion" class="tab-panel active">
                                    <div>
                                        <asp:Literal ID="ltrUzmanGorusu" runat="server"></asp:Literal></div>
                                </div>
                                <div id="product-properties" class="tab-panel">
                                    <asp:Repeater ID="rptUrunOzellik" runat="server" OnItemDataBound="rptUrunOzellik_ItemDataBound">
                                        <ItemTemplate>
                                            <asp:Repeater ID="rptUrunOzellikDetay" runat="server">
                                                <ItemTemplate>
                                                    <div class="row" style="border-bottom: 1px dotted #eee;">
                                                        <div class="col-xs-12 col-sm-3" style="background-color: #ccc;">
                                                            <span>
                                                                <%# DataBinder.Eval(Container.DataItem, "kart_kod") %></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-9">
                                                            <span>
                                                                <%# DataBinder.Eval(Container.DataItem, "veri_secim") %></span>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                                <%--<div id="product-pictures" class="tab-panel">
                                </div>--%>
                            </div>
                        </div>
                        <!-- ./tab product -->
                    </div>
                    <!-- Product -->
                </div>
                <!-- ./ Center colunm -->
                <!-- Right colunm -->
                <div class="column col-xs-12 col-sm-3 mt10" id="left_column">
                    <!-- block best sellers -->
                    <asp:Repeater ID="rptCokSatanlarIlk10" runat="server">
                        <HeaderTemplate>
                            <div class="block left-module">
                                <p class="title_block">
                                    KATEGORİNİN ÇOK SATANLARI</p>
                                <div class="block_content">
                                    <div class="owl-carousel owl-best-sell" data-loop="true" data-nav="false" data-margin="0"
                                        data-autoplaytimeout="1000" data-autoplay="true" data-autoplayhoverpause="true"
                                        data-items="1">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# (Container.ItemIndex % 3 == 0) ? "<ul class=\"products-block best-sell\">" : "" %>
                            <li>
                                <div class="products-block-left">
                                    <a href="<%#DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>"
                                        class="thumbsPic">
                                        <img src="//<%=ImageDomain %>/UrunResim/BuyukResim/<%#DataBinder.Eval(Container.DataItem, "resim_ad") %>"
                                            alt="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>" />
                                    </a>
                                </div>
                                <div class="products-block-right">
                                    <p class="product-name">
                                        <a href="<%#DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>"
                                            class="productName">
                                            <%#DataBinder.Eval(Container.DataItem, "urun_ad") %></a>
                                    </p>
                                    <p class="product-price">
                                        <a href="<%#DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                                            <%#DataBinder.Eval(Container.DataItem, "net_tutar", "{0:###,###,###}")%>&nbsp;<span>PUAN</span></a></p>
                                    <%--<p class="product-star">
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                                class="fa fa-star"></i><i class="fa fa-star-half-o"></i>
                                        </p>--%>
                                </div>
                            </li>
                            <%# ((Container.ItemIndex > 0 && (Container.ItemIndex + 1) % 3 == 0) || (Container.ItemIndex + 1) == rptCokSatanlarIlk10.Items.Count) ? "</ul>" : ""%>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div> </div> </div>
                        </FooterTemplate>
                    </asp:Repeater>
                    <!-- ./block best sellers  -->
                </div>
                <!-- ./right colunm -->
            </div>
            <!-- ./row-->
            <!-- row -->
            <div class="row">
                <div class="column col-xs-12 mt10" id="left_column">
                    <!-- block other products -->
                    <asp:Repeater ID="rptKategoridekiDigerUrunler" runat="server">
                        <HeaderTemplate>
                            <!-- block category -->
                            <div class="block left-module">
                                <p class="title_block">
                                    KATEGORİNİN DİĞER ÜRÜNLERİ</p>
                                <div class="block_content">
                                    <!-- layered -->
                                    <div class="layered layered-category">
                                        <div class="layered-content">
                                            <ul class="tree-menu">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li class="list" style="display: inline;"><span></span><a style="font-size: 10px;
                                padding-right: 16px;" href="<%#DataBinder.Eval(Container.DataItem, "urun_link") %>"
                                title="<%#DataBinder.Eval(Container.DataItem, "model_kod") %>">
                                <%#DataBinder.Eval(Container.DataItem, "model_kod") %>
                            </a></li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul></div></div>
                            <!-- ./layered -->
                            </div> </div>
                            <!-- ./block category  -->
                        </FooterTemplate>
                    </asp:Repeater>
                    <!-- block other products -->
                </div>
            </div>
            <!-- ./row-->
        </div>
    </div>
</asp:Content>
