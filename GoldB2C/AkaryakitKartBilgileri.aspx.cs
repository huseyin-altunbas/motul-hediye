﻿using Goldb2cBLL;
using Goldb2cEntity;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class AkaryakitKartBilgileri : System.Web.UI.Page
{
    SiparisIslemleriBLL bllSiparisIslemleri = new SiparisIslemleriBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
    AkaryakitIslemleriBLL bllAkaryakitIslemleri = new AkaryakitIslemleriBLL();
    Kullanici kullanici;
    public int tutar = 0;
    public Int32 akaryakitUrunId = 0;
    string sepettekiUrunAkaryakitTipi = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
            kullanici = (Kullanici)Session["Kullanici"];
        else
            UyelikIslemleriBLL.MusteriGirisineYonlendir(Request.Url.PathAndQuery);


        if (Session["KartBilgisiKaydet"] != null && Convert.ToBoolean(Session["KartBilgisiKaydet"]))
        {
            rptAkaryakit.DataSource = bllAkaryakitIslemleri.CariAkaryakitKartListele(kullanici.ErpCariId, 0);
            rptAkaryakit.DataBind();
            Session["KartBilgisiKaydet"] = null;
        }


        if (!IsPostBack)
        {
            rptAkaryakit.DataSource = bllAkaryakitIslemleri.CariAkaryakitKartListele(kullanici.ErpCariId, 0);
            rptAkaryakit.DataBind();
            if (Session["SiparisMasterId"] != null)
            {
                SiparisIdveYakitTipiBul();

            }



        }
    }

    private void SiparisIdveYakitTipiBul()
    {

        string _SiparisMasterId = Session["SiparisMasterId"].ToString();
        string[] _SiparisMasterIdArr = _SiparisMasterId.Split(',');
        foreach (string item in _SiparisMasterIdArr)
        {
            int siparisId;
            Int32.TryParse(item, out siparisId);

            DataSet dsSiparis = bllSiparisGoruntuleme.SiparisGoruntule(siparisId, kullanici.ErpCariId);
            DataTable dtSiparisDetay = dsSiparis.Tables["SiparisDetayTT"];



            foreach (DataRow drSiparisDetay in dtSiparisDetay.Rows)
            {
                DataTable dtAkaryakitUrun = bllAkaryakitIslemleri.AkaryakitKartUrunListele();

                foreach (DataRow drAkaryakitUrun in dtAkaryakitUrun.Rows)
                {
                    if (drSiparisDetay["urun_id"].ToString() == drAkaryakitUrun["UrunId"].ToString())
                    {
                        akaryakitUrunId = Convert.ToInt32(drSiparisDetay["urun_id"].ToString());
                        tutar += Convert.ToInt32(drAkaryakitUrun["Tutar"]) * Convert.ToInt32(drSiparisDetay["siparis_adet"]);
                        break;
                    }
                }
            }

            Session["AkaryakitTutar"] = tutar;
            lblSiparisAkaryakit.Text = string.Format("Siparişinizdeki toplam akaryakıt tutarı : {0} TL<br/><span style=\"font-size:14px;color:#ff0000\">Lütfen yükleme yapacağınız plakayı seçiniz.</span>", tutar.ToString());
        }

        if (akaryakitUrunId == 20022463)
        {
            ddlYakitTuru.SelectedValue = "Benzin";
            sepettekiUrunAkaryakitTipi = "Benzin";
        }
        else if (akaryakitUrunId == 20022464)
        {
            ddlYakitTuru.SelectedValue = "Benzin";
            sepettekiUrunAkaryakitTipi = "Benzin";
        }

        if (akaryakitUrunId == 20022465)
        {
            ddlYakitTuru.SelectedValue = "Dizel";
            sepettekiUrunAkaryakitTipi = "Dizel";
        }
        else if (akaryakitUrunId == 20022466)
        {
            ddlYakitTuru.SelectedValue = "Dizel";
            sepettekiUrunAkaryakitTipi = "Dizel";
        }

    }

    protected void rptAkaryakit_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (((DataRowView)e.Item.DataItem).Row["KartNo"].ToString() != string.Empty)
                ((Label)e.Item.FindControl("lblKartNo")).Text = ((DataRowView)e.Item.DataItem).Row["KartNo"].ToString();
            else
                ((Label)e.Item.FindControl("lblKartNo")).Text = "Akaryakıt Kart";

            ((Label)e.Item.FindControl("lblPlaka")).Text = ((DataRowView)e.Item.DataItem).Row["Plaka"].ToString();
            ((Label)e.Item.FindControl("lblTutar")).Text = "Yüklenecek Tutar : " + ((DataRowView)e.Item.DataItem).Row["Tutar"].ToString() + " TL";
            ((Label)e.Item.FindControl("lblTutar")).CssClass = "tutar tutar" + ((DataRowView)e.Item.DataItem).Row["KartId"].ToString();
            ((Label)e.Item.FindControl("lblYakitTipi")).Text = ((DataRowView)e.Item.DataItem).Row["YakitTuru"].ToString().Replace("Dizel", "Motorin");
            //LinkButton btnSil = ((LinkButton)e.Item.FindControl("btnSil"));

            //btnSil.Attributes.Add("KartId", ((DataRowView)e.Item.DataItem).Row["KartId"].ToString());
            //btnSil.Attributes.Add("name", btnSil.UniqueID);

        }

    }

    protected void btnGonder_Click(object sender, EventArgs e)
    {
        SiparisIdveYakitTipiBul();
        string _SiparisMasterId = Session["SiparisMasterId"].ToString();
        string[] _SiparisMasterIdArr = _SiparisMasterId.Split(',');
        int SipariseDevamEdilebilirmi = 0;
        //int chkSay = 0;
        string plakaYakitTuru = "";
        int KartId = 0;
        //foreach (RepeaterItem item in rptAkaryakit.Items)
        //{
        //    HtmlInputCheckBox chkKartTeslimat = (HtmlInputCheckBox)item.FindControl("chkKartTeslimat");
        //    if (chkKartTeslimat.Checked)
        //    {
        //        chkSay++;
        //    }
        //}
        //if (chkSay < 1)
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "mesaj", "alert('Lütfen bir plaka seçiniz.');", true);
        //    return;
        //}
        //else if (chkSay > 1)
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "mesaj", "alert('Lütfen 1 adet plaka seçiniz.');", true);
        //    return;
        //}
        if (Session["KartId"] != null)
        {
            int.TryParse(Session["KartId"].ToString(), out KartId);
            if (KartId < 1)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "mesaj", "alert('Lütfen bir plaka seçiniz.');", true);
                return;
            }
        }

        foreach (string itemMasterId in _SiparisMasterIdArr)
        {

            decimal tutar = 0;
            int SiparisMasterId = 0;
            int.TryParse(itemMasterId, out SiparisMasterId);
            StringBuilder sbKartId = new StringBuilder();
            if (SiparisMasterId > 0)
            {
                int i = 0;
                bool isPlaka = true;
                //foreach (RepeaterItem item in rptAkaryakit.Items)
                //{
                //HtmlInputCheckBox chkKartTeslimat = (HtmlInputCheckBox)item.FindControl("chkKartTeslimat");
                //if (chkKartTeslimat.Checked)
                //{
                //Label lblPlaka = (Label)item.FindControl("lblPlaka");
                //if (lblPlaka.Text == string.Empty)
                //{
                //    isPlaka = false;
                //    break;
                //}

                i++;
                KartId = Convert.ToInt32(Session["KartId"].ToString());
                sbKartId.Append(KartId.ToString() + ",");
                DataTable dt = bllAkaryakitIslemleri.CariAkaryakitKartListele(kullanici.ErpCariId, KartId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    tutar += Convert.ToDecimal(dt.Rows[0]["Tutar"]);
                    plakaYakitTuru = dt.Rows[0]["YakitTuru"].ToString();
                }

                //}
                //}

                if (plakaYakitTuru != sepettekiUrunAkaryakitTipi)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "mesaj", "alert('Daha önce bu plaka için farklı bir akaryakıt türü seçmiştiniz. Şu an seçtiğiniz akaryakıt türü ile eşleşmemektedir. Plakanıza ait yakıt türünü güncellemek için 444 98 15 numaralı danışma hattımıza ulaşabilirsiniz.');", true);
                    return;
                }
                if (!isPlaka)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "mesaj", "alert('Seçtiğiniz kart bilgisinde plaka tanımlı değildir. Lütfen Düzenle butonunu kullanarak plaka giriniz.');", true);
                }
                else if (i > 0)
                {
                    if (Session["AkaryakitTutar"] != null)
                    {
                        if (tutar == Convert.ToDecimal(Session["AkaryakitTutar"]))
                        {
                            int sonuc = bllAkaryakitIslemleri.CariAkaryakitKartSiparisKaydet(kullanici.ErpCariId, sbKartId.ToString().TrimEnd(','), SiparisMasterId);
                            if (sonuc > 0)
                            {
                                SipariseDevamEdilebilirmi++;
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "mesaj", "alert('Yönlendirilemedi. (" + kullanici.ErpCariId.ToString() + "," + sbKartId.ToString() + "," + SiparisMasterId.ToString() + ", " + sonuc.ToString() + ")');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "mesaj", "alert('Seçtiğiniz kart bilgisindeki yüklenecek tutar ile siparişinizdeki akaryakıt tutarı eşleşmemektedir.');", true);
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "mesaj", "alert('Lütfen seçim yapınız.');", true);
                }
            }
        }


        if (SipariseDevamEdilebilirmi > 0)
        {
            Session.Remove("KartId");
            Response.Redirect("~/SiparisOnay.aspx", true);
        }
    }


    private void AkaryakitKartBilgileriGetir()
    {
        Kullanici kullanici;
        if (HttpContext.Current.Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];
        }
        //rptAkaryakit.DataSource = bllAkaryakitIslemleri.CariAkaryakitKartListele(kullanici.ErpCariId, 0);
        //rptAkaryakit.DataBind();
    }

    [WebMethod]
    public static KullaniciAkaryakitKart getKartBilgileri(string KartId)
    {
        KullaniciAkaryakitKart kullaniciAkaryakitKart = new KullaniciAkaryakitKart();
        Kullanici kullanici;
        if (HttpContext.Current.Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];

            AkaryakitIslemleriBLL bllAkaryakitIslemleri = new AkaryakitIslemleriBLL();
            DataTable dtAdres = new DataTable();
            dtAdres = bllAkaryakitIslemleri.CariAkaryakitKartListele(kullanici.ErpCariId, Convert.ToInt32(KartId));
            if (dtAdres.Rows.Count > 0)
            {
                kullaniciAkaryakitKart.Plaka = dtAdres.Rows[0]["Plaka"].ToString();
                kullaniciAkaryakitKart.Tutar = Convert.ToDecimal(dtAdres.Rows[0]["Tutar"].ToString());
                kullaniciAkaryakitKart.yakitTuru = dtAdres.Rows[0]["YakitTuru"].ToString();
            }

        }

        return kullaniciAkaryakitKart;
    }

    [WebMethod]
    public static bool setKartId(string KartId)
    {
        HttpContext.Current.Session["KartId"] = KartId;


        return true;
    }

    [WebMethod]
    public static bool removeKartId()
    {
        HttpContext.Current.Session.Remove("KartId");


        return true;
    }


    [WebMethod(EnableSession = true)]
    public static void KartBilgisiKaydet(string KartId, string Plaka, string Tutar, string yakitTuru)
    {
        HttpContext.Current.Session.Remove("KartId");
        AkaryakitIslemleriBLL bllAkaryakitIslemleri = new AkaryakitIslemleriBLL();
        Kullanici kullanici;
        if (HttpContext.Current.Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];

            
            SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ConnectionString, CommandType.Text, "Update AkaryakitKart set Tutar=0 where ErpCariId=" + kullanici.ErpCariId);

            //bllAkaryakitIslemleri.CariAkaryakitKartKaydet(kullanici.ErpCariId, Convert.ToInt32(KartId), Plaka.Trim().Replace(" ", string.Empty).ToUpper(), Convert.ToDecimal(Tutar));
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["GoldB2CSqlConn"].ConnectionString);
            con.Open();
            SqlCommand cmd5 = new SqlCommand("AkaryakitKartKaydetV2", con);
            cmd5.CommandType = CommandType.StoredProcedure;
            cmd5.Parameters.AddWithValue("erpCariId", kullanici.ErpCariId);
            cmd5.Parameters.AddWithValue("kartId", Convert.ToInt32(KartId));
            cmd5.Parameters.AddWithValue("plaka", Plaka.Trim().Replace(" ", string.Empty).ToUpper());
            cmd5.Parameters.AddWithValue("tutar", Convert.ToDecimal(Tutar));
            cmd5.Parameters.AddWithValue("YakitTuru", yakitTuru);
            cmd5.ExecuteNonQuery();
            con.Close();

            HttpContext.Current.Session["KartBilgisiKaydet"] = true;

        }
    }
}

public class KullaniciAkaryakitKart
{
    public string Plaka { get; set; }
    public decimal Tutar { get; set; }
    public string yakitTuru { get; set; }
}