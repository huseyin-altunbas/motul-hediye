﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Text;
using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;

public partial class UrunDetay : System.Web.UI.Page
{

    UrunIncelemeBLL bllUrunInceleme = new UrunIncelemeBLL();
    UrunGrupIslemleriBLL bllUrunGrupIslemleri = new UrunGrupIslemleriBLL();
    TahsilatVerileriBLL bllTahsilatVerileri = new TahsilatVerileriBLL();
    OlapVeriBLL bllOlapVeri = new OlapVeriBLL();
    UrunYorumBLL bllUrunYorum = new UrunYorumBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    SepetIslemleriBLL bllSepetIslemleri = new SepetIslemleriBLL();
    MarketingScriptHelper marketingScriptHelper = new MarketingScriptHelper();
    OlapUrun bllOlapUrun = new OlapUrun();

    DataTable dtUrunKartTT;
    DataTable dtUrunBundleTT;
    DataTable dtHediyePaketiTT;
    DataTable dtUrunIndirimliTT;
    DataTable dtUrunResimleriTT;
    DataTable dtUrunOzellikTT;
    DataTable dtCaprazUrunKodTT;
    DataTable dtDigerUrunKartTT;
    DataTable dtCokSatanUrunKartTT;

    DataSet dsUrunInceleme;

    string RandomNumber = "Sayfayı Yenileyiniz";
    int UrunId;
    Kullanici kullanici;

    bool CokluBundle = false;
    public string VipProtectKod = "";
    public string VipProtectAciklama = "";
    public string SayfaUrl = "";

    public string OzelUrunId = "0";

    public static string ImageDomain = CustomConfiguration.ImageDomain;
    string[] bshUrun = new string[] { };
    //string[] bshUrun = new[]
    //{"20019101", "20019105", "20019109", "20019110", "20020534", "20020538", "20020539", "20020540", "20020723", "20020726",
    // "20020731", "20020737", "20022824", "20022834", "20022839", "20022840", "20022844", "20022854", "20022855", "20022856", "20022878",
    // "20022879", "20026798", "20020737", "20022824", "20026804", "20026805", "20026806", "20026808", "20026809", "20026810", "20026811",
    // "20026812", "20026813", "20026814", "20026819", "20026820", "20026821", "20026822", "20026823", "20026825", "20026827", "20026832",
    // "20026833", "20026834", "20026836", "20026837", "20026838", "20026839", "20026841", "20026842", "20026843", "20026844", "20026845",
    // "20026847", "20026851", "20026852", "20026854", "20026855", "20026856", "20026857", "20026858", "20026859", "20026860", "20026862",
    // "20026863", "20020586", "20026824", "20032111", "20032112", "20032114", "20032115", "20032116", "20032117", "20032118", "20032119",
    // "20032366", "20003674", "20015873", "20019133", "20019137", "20020734", "20022829", "20022831", "20022832", "20022833", "20022850",

    // "20022860", "20022874", "20030584", "20032008", "20032009", "20032010", "20032011", "20032012", "20032013", "20032014", "20032015",
    // "20032016", "20032017", "20032019", "20032019", "20032020", "20032021", "20032022", "20032023", "20032024", "20032025", "20032026",
    // "20032027", "20032028", "20032029", "20032030", "20032031", "20032032", "20032033", "20032034", "20032035", "20032036", "20032037",


    // "20032038", "20032039", "20032042", "20032043", "20032044", "20032045", "20032046", "20032047", "20032048", "20032049", "20032050",
    // "20032051", "20032052", "20032053", "20032054", "20032056", "20032057", "20032058"};


    //{ "20003670", "20019101", "20020517", "20020518", "20020522", "20020528", "20020530", "20020531", "20020532", "20020533", "20020535", "20020536", "20020541", "20020553", "20020567", "20021190", "20021197", "20021243" };
    public string bshAciklama = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        

        //if (Session["Kullanici"] == null)
        //    Response.Redirect(@"\MusteriHizmetleri\UyeGirisi.aspx");
        if (!string.IsNullOrEmpty(Request.QueryString["UrunId"]))
        {
            if (int.TryParse(Request.QueryString["UrunId"], out UrunId))
            {
                string urunKod = Request.QueryString["UrunId"];
                DataTable stockCodeTable = Execute.GetStockCode(urunKod);
                if (stockCodeTable.Rows.Count > 0)
                {
                    bshAciklama = stockCodeTable.Rows[0]["aciklama"].ToString();
                }
                if (Session["Kullanici"] != null)
                    kullanici = ((Kullanici)Session["Kullanici"]);

                if (!IsPostBack)
                {
                    int UrunGrupId = 0;
                    dsUrunInceleme = bllUrunInceleme.GetUrunIncele(UrunId);

                    dtUrunKartTT = dsUrunInceleme.Tables["UrunKartTT"];
                    dtCaprazUrunKodTT = dsUrunInceleme.Tables["CaprazUrunKartTT"];
                    dtDigerUrunKartTT = dsUrunInceleme.Tables["DigerUrunKartTT"];

                    if (dtUrunKartTT.Rows.Count > 0)
                    {
                        if (Request.UrlReferrer != null && !string.IsNullOrEmpty(Request.UrlReferrer.ToString()) && Request.UrlReferrer.AbsolutePath != "/MusteriHizmetleri/UyeGirisi.aspx")
                            Session["SonGezilenKategori"] = Request.UrlReferrer;

                        //Urungrupid boşsa ürün bundle dır anasayfaya gitsin
                        if (!int.TryParse(dtUrunKartTT.Rows[0]["urungrup_id"].ToString(), out UrunGrupId))
                            Response.Redirect("~/Default.aspx", false);

                        //Meta description title
                        if (!string.IsNullOrEmpty(dtUrunKartTT.Rows[0]["urun_ad"].ToString()))
                        {
                            Page.Title = dtUrunKartTT.Rows[0]["urun_ad"].ToString() + " - motulteam.com";
                            if (string.IsNullOrEmpty(Page.MetaDescription))
                                Page.MetaDescription = dtUrunKartTT.Rows[0]["urun_ad"].ToString();
                        }

                        dtUrunResimleriTT = dsUrunInceleme.Tables["UrunResimleriTT"];
                        dtUrunBundleTT = dsUrunInceleme.Tables["UrunBundleTT"];
                        dtHediyePaketiTT = dsUrunInceleme.Tables["HediyePaketiTT"];
                        dtUrunIndirimliTT = dsUrunInceleme.Tables["UrunIndirimliTT"];
                        dtUrunOzellikTT = dsUrunInceleme.Tables["UrunOzellikTT"];
                        ltrStokAd.Text = dtUrunKartTT.Rows[0]["urun_ad"].ToString();
                        //ltrSptBtnUrunAd.Text = dtUrunKartTT.Rows[0]["urun_ad"].ToString();


                        string DefaultResim = "";
                        if (dtUrunResimleriTT != null && dtUrunResimleriTT.Rows.Count > 0 && !string.IsNullOrEmpty(dtUrunResimleriTT.Rows[0]["kayit_id"].ToString()))
                            DefaultResim = "http://" + ImageDomain + "/UrunResim/OrtaResim/" + dtUrunResimleriTT.Rows[0]["resim_ad"].ToString();

                        if (Convert.ToBoolean(dtUrunKartTT.Rows[0]["temin_durum"]))
                            SonIncelenenUrunler(new SonIncelenenUrun
                            {
                                UrunId = dtUrunKartTT.Rows[0]["urun_id"].ToString(),
                                UrunAd = dtUrunKartTT.Rows[0]["urun_ad"].ToString(),
                                UrunResim = DefaultResim,
                                UrunLink = dtUrunKartTT.Rows[0]["urun_link"].ToString(),
                                BrutTutar = dtUrunKartTT.Rows[0]["brut_tutar"].ToString(),
                                NetTutar = dtUrunKartTT.Rows[0]["net_tutar"].ToString(),
                            });
                        else
                            SonIncelenenUrunler(new SonIncelenenUrun
                            {
                                UrunId = ""
                            });



                        decimal _baslayanTaksit;
                        if (Decimal.TryParse(dtUrunKartTT.Rows[0]["baslayan_taksit"].ToString(), out _baslayanTaksit))
                        {
                            //ltrBaslayanTaksitlerle.Text = Math.Round(_baslayanTaksit, 2).ToString();
                            //ltrSptBtnBaslayanTaksitlerle.Text = Math.Round(_baslayanTaksit, 2).ToString();
                        }


                        //ltrYorumSayisi.Text = dtUrunKartTT.Rows[0]["yorum_sayi"].ToString();

                        //if (!string.IsNullOrEmpty(dtUrunKartTT.Rows[0]["spot_kelime"].ToString()))
                        //    ltrStokAd.Text = ltrStokAd.Text + " - ";


                        //ltrUrunSpot.Text = dtUrunKartTT.Rows[0]["spot_kelime"].ToString();

                        string UcKategori = AyakIziOlustur(UrunGrupId);
                        CokSatanUrunler(UrunGrupId, UrunId);
                        IkonlariGoster();
                        TahsilatRotatorOlustur(UrunId);
                        FiyatlariYaz();
                        TablariDuzenle();
                        ResimleriDuzenle();
                        YildizlariOlustur(Convert.ToInt32(dtUrunKartTT.Rows[0]["yorum_puan"]));
                        DigerMarkalariHazirla(UrunGrupId);
                        ReviewTablariDuzenle(UrunId);
                        UrunOzellikleriHazirla();
                        RetargetKodOlustur(UrunId.ToString());
                        CaprazUrunler();
                        KategoridekiDigerUrunler();


                        SeoIcerikHazirla(dtUrunKartTT.Rows[0]["urun_keywords"].ToString(), dtUrunKartTT.Rows[0]["urun_description"].ToString(), dtUrunKartTT.Rows[0]["content_alan"].ToString(), dtUrunKartTT.Rows[0]["remarketing_alan"].ToString(), dtUrunKartTT.Rows[0]["urun_ad"].ToString(), UcKategori);



                        SayfaUrl = "http://motulteam.com" + dtUrunKartTT.Rows[0]["urun_link"].ToString();

                        string fbMetaTags = "";
                        fbMetaTags = fbMetaTags + "<meta property=\"og:url\" content=\"" + SayfaUrl + "\" />";
                        fbMetaTags = fbMetaTags + "<meta property=\"og:title\" content=\"" + dtUrunKartTT.Rows[0]["urun_ad"].ToString().Replace("\"", "").Replace("'", "") + "\" />";
                        fbMetaTags = fbMetaTags + "<meta property=\"og:image\" content=\"http://www.gold.com.tr/UrunResim/buyukresim/" + dtUrunResimleriTT.Rows[0]["resim_ad"].ToString() + "\" />";
                        fbMetaTags = fbMetaTags + "<meta property=\"og:type\" content=\"website\" />";
                        if (!string.IsNullOrEmpty(dtUrunKartTT.Rows[0]["urun_description"].ToString()))
                            fbMetaTags = fbMetaTags + "<meta property=\"og:description\" content=\"" + dtUrunKartTT.Rows[0]["urun_description"].ToString() + "\" />";
                        else
                            fbMetaTags = fbMetaTags + "<meta property=\"og:description\" content=\"" + dtUrunKartTT.Rows[0]["urun_ad"].ToString().Replace("\"", "").Replace("'", "") + "\" />";


                        ltrMeta.Text = fbMetaTags;
                    }
                    else
                    {

                        Response.Status = "301 Moved Permanently";
                        Response.StatusCode = 301;
                        Response.AddHeader("Location", "https://motulteam.com");
                        Response.End();
                    }

                    //if (Session["Kullanici"] != null)
                    //{
                    //    kullanici = (Kullanici)Session["Kullanici"];
                    //    MvYorum.ActiveViewIndex = 1;
                    //    CapchaGenerator();
                    //}
                    //else
                    //{
                    //    MvYorum.ActiveViewIndex = 0;
                    //}
                }
            }
            else
            {
                Response.Status = "301 Moved Permanently";
                Response.StatusCode = 301;
                Response.AddHeader("Location", "http://motulteam.com");
                Response.End();
            }
        }
        else
        {
            Response.Status = "301 Moved Permanently";
            Response.StatusCode = 301;
            Response.AddHeader("Location", "http://motulteam.com");
            Response.End();
        }
    }
   
    private void SonIncelenenUrunler(SonIncelenenUrun GosterilenUrun)
    {

        List<SonIncelenenUrun> sonIncelenenler = new List<SonIncelenenUrun>();

        int listSize = 6;
        string[] _sonurunler;
        HttpCookie cookie = Request.Cookies["SonIncelenenler"];

        if (cookie != null)
        {
            _sonurunler = cookie.Value.Split('|');
            SonIncelenenUrun sonUrun;
            foreach (string urun in _sonurunler)
            {
                sonUrun = new SonIncelenenUrun();
                string[] _urunBilgileri = urun.Split('#');

                if (_urunBilgileri.Length == 6)
                {
                    sonUrun.UrunId = _urunBilgileri[0];
                    sonUrun.UrunAd = _urunBilgileri[1];
                    sonUrun.UrunResim = _urunBilgileri[2];
                    sonUrun.UrunLink = _urunBilgileri[3];
                    sonUrun.NetTutar = _urunBilgileri[4];
                    sonUrun.BrutTutar = _urunBilgileri[5];

                    sonIncelenenler.Add(sonUrun);
                }

            }

            //if (sonIncelenenler.Count > 0)
            //{
            //    sonIncelenenler.Reverse();
            //    rptSonIncelenenler.Visible = true;
            //    rptSonIncelenenler.DataSource = sonIncelenenler;
            //    rptSonIncelenenler.DataBind();
            //    sonIncelenenler.Reverse();
            //}
        }
        else
            cookie = new HttpCookie("GldSonIncelenenler");


        if (!string.IsNullOrEmpty(GosterilenUrun.UrunId))
        {
            if (sonIncelenenler.Count < listSize && !sonIncelenenler.Any(p => p.UrunId.Equals(GosterilenUrun.UrunId)))
                sonIncelenenler.Add(GosterilenUrun);
            else
            {
                if (!sonIncelenenler.Any(p => p.UrunId.Equals(GosterilenUrun.UrunId)))
                {
                    sonIncelenenler.RemoveAt(0);
                    sonIncelenenler.Add(GosterilenUrun);
                }
            }
        }

        string CookieString = "";
        foreach (SonIncelenenUrun urun in sonIncelenenler)
        {
            CookieString = CookieString + urun.UrunId + "#" + urun.UrunAd + "#" + urun.UrunResim + "#" + urun.UrunLink + "#" + urun.NetTutar + "#" + urun.BrutTutar + "|";
        }

        CookieString = CookieString.TrimEnd(new char[] { '|' });

        cookie.Value = CookieString;
        cookie.Expires = DateTime.Now.AddDays(30);

        Response.Cookies.Add(cookie);

    }

    private void CokSatanUrunler(int UrunGrupId, int UrunId)
    {
        dtCokSatanUrunKartTT = bllOlapUrun.CokSatanUrun(UrunGrupId, 0, UrunId);

        if (dtCokSatanUrunKartTT != null && dtCokSatanUrunKartTT.Rows.Count > 0)
        {
            rptCokSatanlarIlk10.DataSource = dtCokSatanUrunKartTT;
            rptCokSatanlarIlk10.DataBind();
        }
        else
            rptCokSatanlarIlk10.Visible = false;
    }

    private void KategoridekiDigerUrunler()
    {
        if (dtDigerUrunKartTT != null && dtDigerUrunKartTT.Rows.Count > 0)
        {
            rptKategoridekiDigerUrunler.DataSource = dtDigerUrunKartTT;
            rptKategoridekiDigerUrunler.DataBind();
        }
    }

    private void CaprazUrunler()
    {
        //if (dtCaprazUrunKodTT != null && dtCaprazUrunKodTT.Rows.Count > 0)
        //{
        //    rptCaprazUrunler.DataSource = dtCaprazUrunKodTT;
        //    rptCaprazUrunler.DataBind();
        //}
    }



    private void UrunOzellikleriHazirla()
    {
        rptUrunOzellik.DataSource = dtUrunOzellikTT.DefaultView.ToTable(true, "grup_kod");
        rptUrunOzellik.DataBind();
    }

    private void ReviewTablariDuzenle(int UrunId)
    {
        string AcilacakDiv = "";
        string AcilacakLi = "";

        ////Yorumları Getir
        //dlYorumlar.DataSource = bllUrunYorum.GetUrunYorum(UrunId);
        //dlYorumlar.DataBind();

        if (!string.IsNullOrEmpty(dtUrunKartTT.Rows[0]["uzman_gorusu"].ToString()))
        {
            ltrUzmanGorusu.Text = HttpUtility.HtmlDecode(dtUrunKartTT.Rows[0]["uzman_gorusu"].ToString());
            string uzman_gorusu = dtUrunKartTT.Rows[0]["uzman_gorusu"].ToString();
            StringBuilder sb2 = new StringBuilder(uzman_gorusu);
            for (int i = 0; i < 1000; i++)
            {
                if (i < 1)
                { continue; }
                string brListBosluklu = "";
                string brBosluklu2 = "";
                for (int iic = 0; iic < i; iic++)
                {
                    brListBosluklu += "<br />";
                    brBosluklu2 += "<br>";
                }


                if (brListBosluklu != "")
                {
                    sb2.Replace(brListBosluklu, "");
                    sb2.Replace(brBosluklu2, "");
                }


            }

            uzman_gorusu = sb2.ToString();
            ltrUzmanGorusu.Text = uzman_gorusu;
        }
        else
        {
            //LiOpinion.Visible = false;
            //DivOpinion.Visible = false;
            //AcilacakLi = "LiProperties";
            //AcilacakDiv = "DivProperties";
        }


        ////taksit seçenekleri
        //List<int> TaksitSayilari = new List<int>();
        //StringBuilder sb = new StringBuilder();
        //DataSet dsTaksitler = bllTahsilatVerileri.GetTaksitSecenekleri(UrunId, out TaksitSayilari);
        //if (dsTaksitler != null)
        //{
        //    if (dsTaksitler.Tables["TahsilatSecenekleriTT"] != null && dsTaksitler.Tables["TahsilatSecenekleriTT"].Rows.Count > 0)
        //    {
        //        DataTable dtTahsilatSecenekleri = dsTaksitler.Tables["TahsilatSecenekleriTT"];
        //        DataTable dtTahsilatSecenekDetaylari = dsTaksitler.Tables["TahsilatSecenekDetaylariTT"];

        //        for (int i = 0; i < dtTahsilatSecenekleri.Rows.Count; i++)
        //        {
        //            if (i % 4 == 0)
        //            {
        //                sb.AppendLine("<div class=\"taksitSayi\">");
        //                sb.AppendLine("<div class=\"space\"></div>");
        //                sb.AppendLine("<div style=\"color: #141414;\" class=\"space2 taksithead\">Taksit</div>");

        //                foreach (int sayi in TaksitSayilari)
        //                {
        //                    if (sayi > 0)
        //                        sb.AppendLine("<div class=\"taksitadet\">" + sayi.ToString() + "</div>");
        //                    else
        //                        sb.AppendLine("<div class=\"taksitadet\">PUAN</div>");
        //                }


        //                sb.AppendLine("</div>");
        //            }


        //            sb.AppendLine("<div class=\"bankaTablo\">");
        //            sb.AppendLine("<div class=\"banklogo\">");
        //            sb.AppendLine("<img src=\"" + dtTahsilatSecenekleri.Rows[i]["urun_taksitimaj"].ToString() + "\" /></div>");
        //            sb.AppendLine("<div class=\"item\">");
        //            sb.AppendLine("<div class=\"taksithead " + dtTahsilatSecenekleri.Rows[i]["banka_kod"].ToString() + "head\">");
        //            sb.AppendLine("Taksit Tutarı</div>");
        //            sb.AppendLine("<div class=\"taksithead2 " + dtTahsilatSecenekleri.Rows[i]["banka_kod"].ToString() + "head\">");
        //            sb.AppendLine("Toplam Tutar</div>");
        //            sb.AppendLine("</div>");


        //            foreach (int taksitSayi in TaksitSayilari)
        //            {
        //                string taksitTutar = "";
        //                string toplamTutar = "";
        //                DataRow[] drSonuc;

        //                drSonuc = dtTahsilatSecenekDetaylari.Select("gtaksit_adet = '" + taksitSayi.ToString() + "' and master_id = '" + dtTahsilatSecenekleri.Rows[i]["master_id"].ToString() + "'");

        //                if (drSonuc != null && drSonuc.Length > 0 && !string.IsNullOrEmpty(drSonuc[0]["taksit_tutar"].ToString()))
        //                    taksitTutar = dtTahsilatSecenekDetaylari.Select("gtaksit_adet = '" + taksitSayi.ToString() + "' and master_id = '" + dtTahsilatSecenekleri.Rows[i]["master_id"].ToString() + "'")[0]["taksit_tutar"].ToString();
        //                else
        //                    taksitTutar = "-";

        //                drSonuc = dtTahsilatSecenekDetaylari.Select("gtaksit_adet = '" + taksitSayi.ToString() + "' and master_id = '" + dtTahsilatSecenekleri.Rows[i]["master_id"] + "'");
        //                if (drSonuc != null && drSonuc.Length > 0 && !string.IsNullOrEmpty(drSonuc[0]["toplam_tutar"].ToString()))
        //                    toplamTutar = dtTahsilatSecenekDetaylari.Select("gtaksit_adet = '" + taksitSayi.ToString() + "' and master_id = '" + dtTahsilatSecenekleri.Rows[i]["master_id"] + "'")[0]["toplam_tutar"].ToString();
        //                else
        //                    toplamTutar = "-";

        //                sb.AppendLine("<div class=\"item" + dtTahsilatSecenekleri.Rows[i]["banka_kod"].ToString() + "\">");
        //                sb.AppendLine("<div class=\"per\">");
        //                sb.AppendLine(taksitTutar + "</div>");
        //                sb.AppendLine("<div class=\"total\">");
        //                sb.AppendLine(toplamTutar + "</div>");
        //                sb.AppendLine("</div>");
        //            }

        //            sb.AppendLine("<div class=\"" + dtTahsilatSecenekleri.Rows[i]["banka_kod"].ToString() + "_bottom\">" + dtTahsilatSecenekleri.Rows[i]["dip_aciklama"].ToString() + "</div>");

        //            sb.AppendLine("</div>");

        //            ltrTaksitSecenekleri.Text = sb.ToString();

        //        }
        //    }
        //}

        //ClientScript.RegisterStartupScript(typeof(string), "ReviewTabDivScript", "<script type=\"text/javascript\">$(document).ready(function(){showReview('" + AcilacakLi + "','" + AcilacakDiv + "')});</script>");
    }

    private void DigerMarkalariHazirla(int UrunGrupId)
    {
        //rptDigerMarkalar.DataSource = bllOlapVeri.GetUrunGrupMarka(UrunGrupId);
        //rptDigerMarkalar.DataBind();

    }

    private void TahsilatRotatorOlustur(int UrunId)
    {
        //rptTahsilatRotator.DataSource = bllTahsilatVerileri.GetTahsilatRotator(UrunId);
        //rptTahsilatRotator.DataBind();
    }



    private void ResimleriDuzenle()
    {

        if (dtUrunResimleriTT.Rows.Count < 1)
        {
            dtUrunResimleriTT.Rows.Add(dtUrunResimleriTT.NewRow());
            dtUrunResimleriTT.Rows[0]["resim_ad"] = "ResimHazirlaniyor.jpg";
        }

        int rowsay = 0;
        foreach (DataRow row in dtUrunResimleriTT.Rows)
        {
            if (dtUrunResimleriTT.Rows[rowsay]["resim_ad"].ToString().IndexOf("http") == -1)
            {
                dtUrunResimleriTT.Rows[rowsay]["resim_ad"] = "http://cdn.gold.com.tr/UrunResim/MegaResim/" + dtUrunResimleriTT.Rows[rowsay]["resim_ad"];
            }
            rowsay = rowsay + 1;
        }
        rptUstSolKucukResimler.DataSource = dtUrunResimleriTT;
        rptUstSolKucukResimler.DataBind();

        rptUstSolBuyukResimler.DataSource = dtUrunResimleriTT.AsEnumerable().Take(1).CopyToDataTable();
        rptUstSolBuyukResimler.DataBind();

        //rptResimGaleri.DataSource = dtUrunResimleriTT;
        //rptResimGaleri.DataBind();


    }

    private void TablariDuzenle()
    {
        string TabAcScriptString = "";

        CokluBundle = Convert.ToBoolean(dsUrunInceleme.Tables["UrunKartTT"].Rows[0]["bundle_secim"]);

        Session["CokluBundle"] = CokluBundle;

        ////Bundle Ürün
        //if (dtUrunBundleTT != null && dtUrunBundleTT.Rows.Count > 0)
        //{
        //    if (CokluBundle)
        //    {
        //        rptHediyeUrunCoklu.DataSource = dtUrunBundleTT;
        //        rptHediyeUrunCoklu.DataBind();
        //    }
        //    else
        //    {
        //        rptHediyeUrunTekli.DataSource = dtUrunBundleTT;
        //        rptHediyeUrunTekli.DataBind();
        //    }

        //    liHediye.Visible = true;
        //    DivHediye.Visible = true;
        //}
        //else
        //{
        //    liHediye.Visible = false;
        //    DivHediye.Visible = false;
        //}

        ////Hediye Paketi
        //if (dtHediyePaketiTT != null && dtHediyePaketiTT.Rows.Count > 0)
        //{

        //    rptHediyePaketi.DataSource = dtHediyePaketiTT;
        //    rptHediyePaketi.DataBind();

        //    liHediyePaketi.Visible = true;
        //    DivHediyePaketi.Visible = true;
        //}
        //else
        //{
        //    liHediyePaketi.Visible = false;
        //    DivHediyePaketi.Visible = false;
        //}

        ////Özel İndirim
        //if (dtUrunIndirimliTT != null && dtUrunIndirimliTT.Rows.Count > 0)
        //{
        //    //Saç maşası kampanyası sonrası buralar düzenlenecek
        //    //Şuan sadece tek ürün gösterir

        //    DataRow[] drOzelIndirimliler = dtUrunIndirimliTT.Select("ozel_indirim = 'True'");
        //    if (drOzelIndirimliler.Length > 0 && !string.IsNullOrEmpty(drOzelIndirimliler[0]["iurun_id"].ToString()))
        //    {
        //        OzelUrunId = dtUrunIndirimliTT.Select("ozel_indirim = 'True'")[0]["iurun_id"].ToString();
        //        liAvantajlar.Visible = true;
        //        DivAvantajlar.Visible = true;
        //    }
        //    else
        //    {
        //        liAvantajlar.Visible = false;
        //        DivAvantajlar.Visible = false;
        //    }
        //}
        //else
        //{
        //    liAvantajlar.Visible = false;
        //    DivAvantajlar.Visible = false;

        //}

        ////Indirimli Ürün
        //DataRow[] drIndirimliUrun = dtUrunIndirimliTT.Select("ozel_indirim <> 'true'");
        //if (dtUrunIndirimliTT != null && drIndirimliUrun.Length > 0)
        //{
        //    rptUrunFirsatlar.DataSource = drIndirimliUrun;
        //    rptUrunFirsatlar.DataBind();
        //    liFirsat.Visible = true;
        //    DivFirsat.Visible = true;
        //}
        //else
        //{
        //    liFirsat.Visible = false;
        //    DivFirsat.Visible = false;
        //}

        ////Vip Protect
        //if (!string.IsNullOrEmpty(dtUrunKartTT.Rows[0]["protect_kod"].ToString()))
        //{
        //    VipProtectKod = dtUrunKartTT.Rows[0]["protect_kod"].ToString();

        //    if (!string.IsNullOrEmpty(dtUrunKartTT.Rows[0]["protect_aciklama"].ToString()))
        //        VipProtectAciklama = dtUrunKartTT.Rows[0]["protect_aciklama"].ToString();

        //    liNeyapmali.Visible = true;
        //    DivNeyapmali.Visible = true;
        //}
        //else
        //{
        //    liNeyapmali.Visible = false;
        //    DivNeyapmali.Visible = false;
        //}

        //if (DivHediye.Visible || DivHediyePaketi.Visible || DivNeyapmali.Visible || DivAvantajlar.Visible || DivFirsat.Visible)
        //    DivTablar.Visible = true;
        //else
        //    DivTablar.Visible = false;

        //if (liAvantajlar.Visible == true)
        //    TabAcScriptString = "<script type=\"text/javascript\">$(document).ready(function(){showit('" + liAvantajlar.ClientID + "','" + DivAvantajlar.ClientID + "')});</script>";
        //else if (liHediye.Visible == true)
        //    TabAcScriptString = "<script type=\"text/javascript\">$(document).ready(function(){showit('" + liHediye.ClientID + "','" + DivHediye.ClientID + "')});</script>";
        //else if (liHediyePaketi.Visible == true)
        //    TabAcScriptString = "<script type=\"text/javascript\">$(document).ready(function(){showit('" + liHediyePaketi.ClientID + "','" + DivHediyePaketi.ClientID + "')});</script>";
        //else if (liFirsat.Visible == true)
        //    TabAcScriptString = "<script type=\"text/javascript\">$(document).ready(function(){showit('" + liFirsat.ClientID + "','" + DivFirsat.ClientID + "')});</script>";
        //else if (liNeyapmali.Visible == true)
        //    TabAcScriptString = "<script type=\"text/javascript\">$(document).ready(function(){showit('" + liNeyapmali.ClientID + "','" + DivNeyapmali.ClientID + "')});</script>";

        //ClientScript.RegisterStartupScript(typeof(string), "TabDivScript", TabAcScriptString);

    }

    private void FiyatlariYaz()
    {
        if (Convert.ToBoolean(dtUrunKartTT.Rows[0]["temin_durum"]))
        {
            //DivFooterBasket.Visible = true;
            //btnSepeteEkleFooter.Visible = true;

            //DivUrunTeminEdilemiyor.Visible = false;
            //DivUrunTeminEdiliyor.Visible = true;

            decimal dliste_fiyat = Math.Round(Convert.ToDecimal(dtUrunKartTT.Rows[0]["dliste_fiyat"]), 2);
            decimal brut_tutar = Math.Round(Convert.ToDecimal(dtUrunKartTT.Rows[0]["brut_tutar"]), 2);
            decimal net_tutar = Math.Round(Convert.ToDecimal(dtUrunKartTT.Rows[0]["net_tutar"]), 2);
            decimal havale_tutar = Math.Round(Convert.ToDecimal(dtUrunKartTT.Rows[0]["havale_tutar"]), 2);
            decimal puan_tutar = Math.Round(Convert.ToDecimal(dtUrunKartTT.Rows[0]["puan_tutar"]), 2);
            decimal indirim_oran = Math.Round(Convert.ToDecimal(dtUrunKartTT.Rows[0]["indirim_oran"]), 2);


            //ltrDovizKod.Text = dtUrunKartTT.Rows[0]["doviz_kod"].ToString();
            //ltrDovizListeTutar.Text = dliste_fiyat.ToString();
            ltrBrutTutar.Text = brut_tutar.ToString("###,###,###");
            ltrNetTutar.Text = net_tutar.ToString("###,###,###");
            //ltrHavaleTutar.Text = havale_tutar.ToString();

            //ltrSptBtnOdenecekTutar.Text = net_tutar.ToString("###,###,###");

            //if (puan_tutar > 0)
            //{
            //    DivPuanTutar.Visible = true;
            //    ltrPuanTutar.Text = puan_tutar.ToString();
            //}
            //else
            //    DivPuanTutar.Visible = false;

            if (indirim_oran > 0)
            {
                DivIndirim.Visible = true;
                ltrIndirimOran.Text = indirim_oran.ToString();
            }
            else
                DivIndirim.Visible = false;

        }
        else
        {
            //DivFooterBasket.Visible = false;
            //btnSepeteEkleFooter.Visible = false;

            //DivUrunTeminEdilemiyor.Visible = true;
            //DivUrunTeminEdiliyor.Visible = false;

            //rptTahsilatRotator.Visible = false;
            btnSepeteEkle.Visible = false;

            //if (!string.IsNullOrEmpty(dtUrunKartTT.Rows[0]["temin_aciklama"].ToString()))
            //    ltrTeminAciklama.Text = dtUrunKartTT.Rows[0]["temin_aciklama"].ToString();
            //else
            //    ltrTeminAciklama.Text = "Bu ürün geçici olarak temin edilememektedir.";

            //if (dtCokSatanUrunKartTT != null && dtCokSatanUrunKartTT.Rows.Count > 0)
            //{

            //    int rowsay = 0;
            //    foreach (DataRow row in dtCokSatanUrunKartTT.Rows)
            //    {
            //        if (dtCokSatanUrunKartTT.Rows[rowsay]["resim_ad"].ToString().IndexOf("http") == -1)
            //        {
            //            dtCokSatanUrunKartTT.Rows[rowsay]["resim_ad"] = "http://cdn.gold.com.tr/UrunResim/KucukResim/" + dtCokSatanUrunKartTT.Rows[rowsay]["resim_ad"];
            //        }
            //        rowsay = rowsay + 1;
            //    }
            //    rptCokSatanlarIlk3.DataSource = dtCokSatanUrunKartTT.AsEnumerable().Take(3);
            //    rptCokSatanlarIlk3.DataBind();
            //}
            //else
            //    rptCokSatanlarIlk3.Visible = false;
        }

    }

    private void IkonlariGoster()
    {
        //İkonlar
        //if (Convert.ToBoolean(dtUrunKartTT.Rows[0]["indirimli"]))
        //    imgIndirimli.ImageUrl = "/imagesgld/icon_indirimliurunOn.png";
        //else
        //    imgIndirimli.ImageUrl = "/imagesgld/icon_indirimliurunOff.png";

        //if (Convert.ToBoolean(dtUrunKartTT.Rows[0]["aynigun_sevk"]))
        //{
        //    imgAyniGunSevk.ImageUrl = "/imagesgld/icon_hizlikargoOn.png";
        //    imgAyniGunKargoAlt.Visible = true;
        //}
        //else
        //{
        //    imgAyniGunSevk.ImageUrl = "/imagesgld/icon_hizlikargoOff.png";
        //    imgAyniGunKargoAlt.Visible = false;
        //}

        //if (Convert.ToBoolean(dtUrunKartTT.Rows[0]["hediyeli_urun"]))
        //    imgHediyeliUrun.ImageUrl = "/imagesgld/icon_hediyeliurunOn.png";
        //else
        //    imgHediyeliUrun.ImageUrl = "/imagesgld/icon_hediyeliurunOff.png";

        //if (Convert.ToBoolean(dtUrunKartTT.Rows[0]["kuryeile_sevk"]))
        //    imgKuryeileSevk.ImageUrl = "/imagesgld/icon_coksatanurunOn.png";
        //else
        //    imgKuryeileSevk.ImageUrl = "/imagesgld/icon_coksatanurunOff.png";

        //if (Convert.ToBoolean(dtUrunKartTT.Rows[0]["kargo_bedava"]))
        //    imgKargoBedava.ImageUrl = "/imagesgld/icon_kargobedavaOn.png";
        //else
        //    imgKargoBedava.ImageUrl = "/imagesgld/icon_kargobedavaOff.png";

        //if (Convert.ToBoolean(dtUrunKartTT.Rows[0]["yeni_urun"]))
        //    imgYeniUrun.ImageUrl = "/imagesgld/icon_yeniurunOn.png";
        //else
        //    imgYeniUrun.ImageUrl = "/imagesgld/icon_yeniurunOff.png";
    }

    private string AyakIziOlustur(int UrunGrupId)
    {
        string UcKategori = "";
        DataTable dtAyakIzi = new DataTable();
        dtAyakIzi = bllUrunGrupIslemleri.GetUrunGrupIzi(UrunGrupId);

        DataRow dr;
        if (dtAyakIzi.Rows.Count > 0)
        {
            dr = dtAyakIzi.Rows[0];

            if (!string.IsNullOrEmpty(dr["urunust_ad1"].ToString()))
            {
                UcKategori = dr["urunust_ad1"].ToString();
                ltrAyakizi.Text = "<a title=\"" + dr["urunust_ad1"].ToString() + "\" href=\"" + dr["url_rewrite1"].ToString() + "\">" + dr["urunust_ad1"].ToString() + "</a>";
            }
            if (!string.IsNullOrEmpty(dr["urunust_ad2"].ToString()))
            {
                UcKategori = dr["urunust_ad2"].ToString();
                ltrAyakizi.Text = ltrAyakizi.Text + "<span class=\"navigation-pipe\">&nbsp;</span><a title=\"" + dr["urunust_ad2"].ToString() + "\" href=\"" + dr["url_rewrite2"].ToString() + "\">" + dr["urunust_ad2"].ToString() + "</a>";
            }
            if (!string.IsNullOrEmpty(dr["urunust_ad3"].ToString()))
            {
                UcKategori = dr["urunust_ad3"].ToString();
                ltrAyakizi.Text = ltrAyakizi.Text + "<span class=\"navigation-pipe\">&nbsp;</span><a title=\"" + dr["urunust_ad3"].ToString() + "\" href=\"" + dr["url_rewrite3"].ToString() + "\">" + dr["urunust_ad3"].ToString() + "</a>";
            }
            if (!string.IsNullOrEmpty(dr["urunust_ad4"].ToString()))
            {
                UcKategori = dr["urunust_ad4"].ToString();
                ltrAyakizi.Text = ltrAyakizi.Text + "<span class=\"navigation-pipe\">&nbsp;</span><a title=\"" + dr["urunust_ad4"].ToString() + "\" href=\"" + dr["url_rewrite4"].ToString() + "\">" + dr["urunust_ad4"].ToString() + "</a>";
            }
            if (!string.IsNullOrEmpty(dr["urunust_ad5"].ToString()))
            {
                UcKategori = dr["urunust_ad5"].ToString();
                ltrAyakizi.Text = ltrAyakizi.Text + "<span class=\"navigation-pipe\">&nbsp;</span><a title=\"" + dr["urunust_ad5"].ToString() + "\" href=\"" + dr["url_rewrite5"].ToString() + "\">" + dr["urunust_ad5"].ToString() + "</a>";
            }
        }

        return UcKategori;

    }

    private string VitrinControl(string urunustAd)
    {
        if (string.IsNullOrEmpty(urunustAd))
            return "UrunListeleme.aspx";
        else
            return "Vitrin.aspx";

    }

    private void YildizlariOlustur(int YorumPuan)
    {
        string YorumYildiz1 = "~/imagesgld/star_Off15x13.png";
        string YorumYildiz2 = "~/imagesgld/star_Off15x13.png";
        string YorumYildiz3 = "~/imagesgld/star_Off15x13.png";
        string YorumYildiz4 = "~/imagesgld/star_Off15x13.png";
        string YorumYildiz5 = "~/imagesgld/star_Off15x13.png";


        switch (YorumPuan)
        {
            case 0:
                break;

            case 1:
                YorumYildiz1 = "~/imagesgld/star_OnOff15x13.png";
                break;
            case 2:
                YorumYildiz1 = "~/imagesgld/star_On15x13.png";
                break;
            case 3:
                YorumYildiz1 = "~/imagesgld/star_On15x13.png";
                YorumYildiz2 = "~/imagesgld/star_OnOff15x13.png";
                break;
            case 4:
                YorumYildiz1 = "~/imagesgld/star_On15x13.png";
                YorumYildiz2 = "~/imagesgld/star_On15x13.png";
                break;
            case 5:
                YorumYildiz1 = "~/imagesgld/star_On15x13.png";
                YorumYildiz2 = "~/imagesgld/star_On15x13.png";
                YorumYildiz3 = "~/imagesgld/star_OnOff15x13.png";
                break;
            case 6:
                YorumYildiz1 = "~/imagesgld/star_On15x13.png";
                YorumYildiz2 = "~/imagesgld/star_On15x13.png";
                YorumYildiz3 = "~/imagesgld/star_On15x13.png";
                break;
            case 7:
                YorumYildiz1 = "~/imagesgld/star_On15x13.png";
                YorumYildiz2 = "~/imagesgld/star_On15x13.png";
                YorumYildiz3 = "~/imagesgld/star_On15x13.png";
                YorumYildiz4 = "~/imagesgld/star_OnOff15x13.png";
                break;
            case 8:
                YorumYildiz1 = "~/imagesgld/star_On15x13.png";
                YorumYildiz2 = "~/imagesgld/star_On15x13.png";
                YorumYildiz3 = "~/imagesgld/star_On15x13.png";
                YorumYildiz4 = "~/imagesgld/star_On15x13.png";
                break;
            case 9:
                YorumYildiz1 = "~/imagesgld/star_On15x13.png";
                YorumYildiz2 = "~/imagesgld/star_On15x13.png";
                YorumYildiz3 = "~/imagesgld/star_On15x13.png";
                YorumYildiz4 = "~/imagesgld/star_On15x13.png";
                YorumYildiz5 = "~/imagesgld/star_OnOff15x13.png";
                break;
            case 10:
                YorumYildiz1 = "~/imagesgld/star_On15x13.png";
                YorumYildiz2 = "~/imagesgld/star_On15x13.png";
                YorumYildiz3 = "~/imagesgld/star_On15x13.png";
                YorumYildiz4 = "~/imagesgld/star_On15x13.png";
                YorumYildiz5 = "~/imagesgld/star_On15x13.png";
                break;
        }

        //imgYorumYildiz1.ImageUrl = YorumYildiz1;
        //imgYorumYildiz2.ImageUrl = YorumYildiz2;
        //imgYorumYildiz3.ImageUrl = YorumYildiz3;
        //imgYorumYildiz4.ImageUrl = YorumYildiz4;
        //imgYorumYildiz5.ImageUrl = YorumYildiz5;
    }

    protected void lbtnYorumYap_Click(object sender, EventArgs e)
    {
        //if (UrunId > 0)
        //{
        //    if (String.IsNullOrEmpty(txtYorumCaptcha.Text.Trim()) || Session["YorumCaptcha"] == null || txtYorumCaptcha.Text.Trim() != Session["YorumCaptcha"].ToString())
        //    {
        //        lblYorumHata.Text = "Lütfen güvenlik kodunu kontrol edin.";
        //        CapchaGenerator();
        //        txtYorumCaptcha.Text = "";

        //        return;
        //    }
        //    else
        //    {
        //        YorumEkle(txtYorumBaslik.Text.Trim(), txtYorum.Text.Trim(), chkYorumdaIsmimGorunsun.Checked, Convert.ToInt32(dpYorumDegerlendirme.SelectedValue));
        //    }
        //}
        //else
        //    Response.Redirect("~/Default.aspx", false);


    }

    protected void lnbtnYorumUyeGiris_Click(object sender, EventArgs e)
    {
        UyelikIslemleriBLL.MusteriGirisineYonlendir(Request.Url.PathAndQuery);

    }

    public void YorumEkle(string YorumBaslik, string Yorum, bool ChkYorumdaIsmimGorunsun, int YorumPuan)
    {
        //kullanici = (Kullanici)Session["Kullanici"];
        //string _Yorum = "";
        //string _YorumBaslik = "";
        //if (kullanici.ErpCariId < 1)
        //{
        //    kullanici = bllUyelikIslemleri.CariVeriOlustur(kullanici);
        //}

        //if (kullanici.ErpCariId > 0)
        //{
        //    _Yorum = CommonBLL.SecureString(Yorum);
        //    _YorumBaslik = CommonBLL.SecureString(YorumBaslik);
        //    ErpData.StrongTypesNS.UrunYorumTTDataTable dtUrunYorumTT = new ErpData.StrongTypesNS.UrunYorumTTDataTable();
        //    dtUrunYorumTT.Rows.Add(dtUrunYorumTT.NewRow());

        //    dtUrunYorumTT.Rows[0]["urun_id"] = UrunId;
        //    dtUrunYorumTT.Rows[0]["cari_id"] = kullanici.ErpCariId;
        //    dtUrunYorumTT.Rows[0]["isim_ciksin"] = ChkYorumdaIsmimGorunsun;
        //    dtUrunYorumTT.Rows[0]["musteri_puan"] = YorumPuan;
        //    dtUrunYorumTT.Rows[0]["aciklama"] = _Yorum;
        //    dtUrunYorumTT.Rows[0]["yorum_baslik"] = _YorumBaslik;


        //    bllUrunYorum.SetUrunYorum(dtUrunYorumTT);
        //}
    }

    protected void dlYorumlar_ItemCommand(object source, DataListCommandEventArgs e)
    {
        //if (Session["Kullanici"] != null)
        //{
        //    kullanici = (Kullanici)Session["Kullanici"];

        //    if (kullanici.ErpCariId < 1)
        //    {
        //        kullanici = bllUyelikIslemleri.CariVeriOlustur(kullanici);
        //    }

        //    int YorumId = Convert.ToInt32(dlYorumlar.DataKeys[e.Item.ItemIndex]);
        //    string OylamaSonuc = "";
        //    if (e.CommandArgument.ToString() == "Evet")
        //        OylamaSonuc = bllUrunYorum.SetYorumDegerlendir(YorumId, 1, kullanici.ErpCariId);
        //    else if (e.CommandArgument.ToString() == "Hayir")
        //        OylamaSonuc = bllUrunYorum.SetYorumDegerlendir(YorumId, 0, kullanici.ErpCariId);

        //    ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('" + OylamaSonuc + "')</script>");
        //}
        //else
        //    ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Yorumları değerlendirebilmek için lütfen üye girişi yapınız.')</script>");

        ////ReviewTablariDuzenle(UrunId);
        ////TablariDuzenle();
    }

    protected void dlYorumlar_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        //int YorumPuan = Convert.ToInt32(((DataRowView)e.Item.DataItem).Row["musteri_puan"]);
        //StringBuilder sbYorumYildiz = new StringBuilder();
        //switch (YorumPuan)
        //{
        //    case 0:
        //        break;

        //    case 1:
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_OnOff15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        break;
        //    case 2:
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        break;
        //    case 3:
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_OnOff15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        break;
        //    case 4:
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        break;
        //    case 5:
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_OnOff15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        break;
        //    case 6:
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        break;
        //    case 7:
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_OnOff15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        break;
        //    case 8:
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        break;
        //    case 9:
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_OnOff15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        break;
        //    case 10:
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        sbYorumYildiz.AppendLine("<img src=\"/imagesgld/star_On15x13.png\" width=\"15\" height=\"13\" alt=\"Yorum\" />");
        //        break;
        //}

        //((Literal)e.Item.FindControl("ltrYorumYildiz")).Text = sbYorumYildiz.ToString();

    }

    private void YorumSayfalamaOlustur(int ToplamSayfa, int SayfaNo)
    {
        //if (ToplamSayfa > 1)
        //{
        //    int BaslangicSayfaNo = 0;
        //    int BitisSayfaNo = 0;

        //    if (SayfaNo % 10 == 0)
        //    {
        //        if (SayfaNo - 4 > 0)
        //            BaslangicSayfaNo = SayfaNo - 4;
        //        else
        //            BaslangicSayfaNo = 1;

        //        if (SayfaNo + 5 <= ToplamSayfa)
        //            BitisSayfaNo = SayfaNo + 5;
        //        else
        //            BitisSayfaNo = ToplamSayfa;
        //    }
        //    else
        //    {
        //        BaslangicSayfaNo = ((SayfaNo / 10) * 10) - 4;
        //        if (BaslangicSayfaNo < 1)
        //            BaslangicSayfaNo = 1;
        //        if (BaslangicSayfaNo + 9 > ToplamSayfa)
        //            BitisSayfaNo = ToplamSayfa;
        //        else
        //            BitisSayfaNo = BaslangicSayfaNo + 9;
        //    }

        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("<a href='/UrunListeleme.aspx?SayfaNo=1' class=\"skip\" title=\"Geri\"><img src=\"imagesgld/arrow_paginationStart.png\" alt=\"Geri\" /></a> <span>| </span>");

        //    for (int i = BaslangicSayfaNo; i <= BitisSayfaNo; i++)
        //    {
        //        if (i == SayfaNo)
        //            sb.Append("<strong>" + SayfaNo.ToString() + "</strong> <span>| </span>");
        //        else
        //            sb.Append("<a href='/UrunListeleme.aspx?SayfaNo=" + i.ToString() + "'>" + i.ToString() + "</a> <span>| </span>");
        //    }

        //    sb.Append("<span>| </span><a href='/UrunListeleme.aspx?SayfaNo=" + ToplamSayfa.ToString() + "' class=\"skip\" title=\"İleri\"><img src=\"imagesgld/arrow_paginationEnd.png\" alt=\"İleri\" /></a>");

        //    ltrYorumSayfalama.Text = sb.ToString();
        //}
        //else
        //    ltrYorumSayfalama.Visible = false;
    }

    public void CapchaGenerator()
    {
        //Random RandomClass = new Random();
        //RandomNumber = RandomClass.Next(1000, 999999).ToString();
        //Session.Add("YorumCaptcha", RandomNumber);

        //string CapchaEncryptClearText = CustomConfiguration.getCapchaEncryptClearText;
        //string CapchaEncryptPassword = CustomConfiguration.getCapchaEncryptPassword;
        //string ens2 = CaptchaDotNet2.Security.Cryptography.Encryptor.Encrypt(RandomNumber, CapchaEncryptClearText, Convert.FromBase64String(CapchaEncryptPassword));
        //imgYorumCaptcha.ImageUrl = "~/Common/Components/Captcha.ashx?w=100&h=40&c=" + ens2;

    }

    protected void rptUrunOzellik_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string grupKod = ((DataRowView)e.Item.DataItem).Row["grup_kod"].ToString();
        ((Repeater)e.Item.FindControl("rptUrunOzellikDetay")).DataSource = dtUrunOzellikTT.Select("grup_kod = '" + grupKod + "'");
        ((Repeater)e.Item.FindControl("rptUrunOzellikDetay")).DataBind();
    }

    protected void btnSepeteEkle_Click(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)Session["Kullanici"];

            if (kullanici.ErpCariId < 1)
            {
                kullanici = bllUyelikIslemleri.CariVeriOlustur(kullanici);
            }

            string HediyeUrun = "";
            string HediyePaketi = "";
            string UrunFirsat = "";
            string ProtectKod = "";
            if (UrunId > 0 && kullanici.ErpCariId > 0)
            {
                if (Session["CokluBundle"] != null)
                    CokluBundle = Convert.ToBoolean(Session["CokluBundle"]);

                //Ürünün hediyeleri
                if (CokluBundle && Request["chkHediyeUrun"] != null && !string.IsNullOrEmpty(Request["chkHediyeUrun"].ToString()))
                    HediyeUrun = Request["chkHediyeUrun"].ToString();
                else if (!CokluBundle && Request["rbHediyeUrun"] != null && !string.IsNullOrEmpty(Request["rbHediyeUrun"].ToString()))
                    HediyeUrun = Request["rbHediyeUrun"].ToString();

                //Hediye Paketi
                if (Request["rbHediyePaketi"] != null && !string.IsNullOrEmpty(Request["rbHediyePaketi"].ToString()))
                    HediyePaketi = Request["rbHediyePaketi"].ToString();

                //Ürün fırsatları
                if (Request["chkUrunFirsat"] != null && !string.IsNullOrEmpty(Request["chkUrunFirsat"].ToString()))
                    UrunFirsat = Request["chkUrunFirsat"].ToString();

                //Avantajlar
                if (Request["chkAvantajlar"] != null && !string.IsNullOrEmpty(Request["chkAvantajlar"].ToString()))
                {
                    if (!string.IsNullOrEmpty(UrunFirsat))
                        UrunFirsat = UrunFirsat + "," + Request["chkAvantajlar"].ToString();
                    else
                        UrunFirsat = Request["chkAvantajlar"].ToString();
                }

                //Vip Protect
                if (Request["chkVipProtect"] != null && !string.IsNullOrEmpty(Request["chkVipProtect"].ToString()))
                    ProtectKod = Request["chkVipProtect"].ToString();

                int VipProtectId = 0;
                int.TryParse(ProtectKod, out VipProtectId);

                int HediyePaketiId = 0;
                int.TryParse(HediyePaketi, out HediyePaketiId);

                bllSepetIslemleri.SepetOlustur(kullanici.ErpCariId, UrunId, HediyeUrun, UrunFirsat, VipProtectId, HediyePaketiId, 1, 1);
                Response.Redirect("~/Sepet.aspx", false);
            }

        }
        else
            UyelikIslemleriBLL.MusteriGirisineYonlendir(Request.Url.PathAndQuery);


        //string bundle = "";
        //for (int i = 0; i < rptHediyeUrunCoklu.Items.Count; i++)
        //{
        //    CheckBox sec = (CheckBox)rptHediyeUrunCoklu.Items[i].FindControl("rbHediyeUrun");
        //    if (sec.Checked)
        //        bundle = bundle + sec.Attributes["burunid"].ToString() + ",";
        //}

    }

    protected void rptHediyeUrunCoklu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //((RadioButton)e.Item.FindControl("rbHediyeUrun")).Attributes.Add("burunid", ((DataRowView)e.Item.DataItem).Row["burun_id"].ToString());
        //((RadioButton)e.Item.FindControl("rbHediyeUrun")).Checked = true;
    }

    protected void rptUrunFirsatlar_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //((RadioButton)e.Item.FindControl("rbUrunFirsat")).Attributes.Add("burunid", ((DataRowView)e.Item.DataItem).Row["burun_id"].ToString());
        //((RadioButton)e.Item.FindControl("rbUrunFirsat")).Checked = true;
    }

    private void SeoIcerikHazirla(string UrunKeywords, string UrunDescriptions, string ContentAlan, string RemarketingAlan, string UrunAd, string UcKategoriAd)
    {
        //if (!string.IsNullOrEmpty(UrunKeywords))
        //    Page.MetaKeywords = UrunKeywords;
        //if (!string.IsNullOrEmpty(UrunDescriptions))
        //    Page.MetaDescription = UrunDescriptions;
        //else
        //    Page.MetaDescription = "En Ucuz " + UrunAd + " Özellikleri ve Uygun Ödeme Seçenekleri Gold’da. Benzer Ürünler İçin " + UcKategoriAd + " Kategorimizi İnceleyin";

        //if (!string.IsNullOrEmpty(RemarketingAlan))
        //    ltrRemarketingKod.Text = RemarketingAlan;
        //if (!string.IsNullOrEmpty(ContentAlan))
        //{
        //    HtmlGenericControl contentAlanCont = new HtmlGenericControl("link");
        //    contentAlanCont.Attributes.Add("rel", "canonical");
        //    contentAlanCont.Attributes.Add("href", ContentAlan);
        //    Page.Header.Controls.Add(contentAlanCont);
        //}

    }

    private void RetargetKodOlustur(string UrunId)
    {
        //if (!string.IsNullOrEmpty(UrunId))
        //    ltrRetargetingKod.Text = "<div class=\"struq_SL_DF_container\" title=\"detail\" style=\"display:none\"> <div class=\"struq_SL_DF\" title=\"pid\">" + UrunId + "</div></div>";
    }
}