﻿/* ---------------------------------------------
Scripts initialization
--------------------------------------------- */
$(window).load(function () {
    $("img.lazy").lazyload({
        threshold: 400
    });
    $(window).scroll();
    GetSepetBilgileri();
});
/* ---------------------------------------------
Scripts ready
--------------------------------------------- */
$(document).ready(function () {
    $(".owl-prev, .owl-next").on("click", function () {
        $("img.lazy").lazyload({
            threshold: 400
        });
    });
});

function SepetEkle(UrunId) {
    $.ajax({
        type: 'POST',
        url: '/Sepet.aspx/SepetEkle',
        data: '{"UrunId" : "' + UrunId + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (result) {
            if (result.d.toString() == "1") {
                GetSepetBilgileri();

                $('body,html').animate({ scrollTop: 0 }, 400);
                return false;
            }
            else if (result.d.toString() == "0")
                window.location.href = "/MusteriHizmetleri/UyeGirisi.aspx";
        },
        complete: function () {
        },
        error: function () {
        }
    });
}

function SepetSil(SepetMasterId, SepetDetayId) {
    $.ajax({
        type: 'POST',
        url: '/Sepet.aspx/SepetSil',
        data: '{"SepetMasterId" : "' + SepetMasterId + '","SepetDetayId" : "' + SepetDetayId + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (result) {
            if (result.d.toString() == "1") {
                if ($(location).attr('pathname').toLowerCase() == "/sepet.aspx")
                    window.location.href = "/Sepet.aspx";
                else
                    GetSepetBilgileri();
            }
        },
        complete: function () {
        },
        error: function () {
        }
    });
}

function GetSepetBilgileri() {
    var CartHtml = "";
    var CartBlock = "";
    CartHtml = '<a class=\"cart-link\" href=\"/Sepet.aspx\"><span class=\"title\">SEPETİM</span><span class=\"total\">0 ürün - 0 PUAN</span><span class=\"notify notify-left\">0</span></a><div class=\"cart-block\"><div class=\"cart-block-content\"><h5 class=\"cart-title\">0 ürün</h5><div class=\"toal-cart\"><span>Toplam</span> <span class=\"toal-price pull-right\">0 PUAN</span></div><div class=\"cart-buttons\"><a href=\"/Sepet.aspx\" class=\"btn-check-out\">SEPETİM</a></div></div></div>';
    $.ajax({
        type: 'POST',
        url: '/Sepet.aspx/GetSepetBilgileri',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $(".shopping-cart-box").html("<img src=\"/imagesGld/loading.gif\"/><span>Yükleniyor...</span>");
        },
        success: function (result) {
            if (result.d.SepetMaster != null) {
                CartHtml = "";
                CartBlock = "";
                CartHtml = CartHtml + '<a class=\"cart-link\" href=\"/Sepet.aspx\"><span class=\"title\">SEPETİM</span>'
                    + '<span class=\"total\">' + result.d.SepetMaster.UrunSayisi + ' ürün - ' + result.d.SepetMaster.GenelTutar + ' PUAN</span>'
                    + '<span class=\"notify notify-left\">' + result.d.SepetMaster.UrunSayisi + '</span>'
                    + '</a>';

                var pathName = $(location).attr('pathname').toLowerCase();
                if (pathName == "/adresbilgileri.aspx" || pathName == "/kargo.aspx" || pathName == "/odeme.aspx" || pathName == "/odemeonay.aspx" || pathName == "/siparisonay.aspx") {
                    CartBlock = "";
                }
                else {
                    CartBlock = CartBlock + '<div class=\"cart-block\">'
                    + '<div class=\"cart-block-content\">'
                    + '<h5 class=\"cart-title\">'
                    + result.d.SepetMaster.UrunSayisi + ' ürün</h5>';

                    $.each(result.d.SepetDetayList, function (index, value) {
                        CartBlock = CartBlock + '<div class=\"cart-block-list\">'
                                    + '<ul>'
                                    + '<li class=\"product-info\">'
                                    + '<div class=\"p-left\">'
                                    + '<a onclick=\"return SepetSil(' + result.d.SepetMaster.SepetMasterId + ',' + value.SepetDetayId + ');\" class=\"remove_link\"></a><a href=\"' + value.UrunLink + '\">'
                                    + '<img class=\"img-responsive\" src=\"' + value.ResimAd + '\" alt=\"p10\">'
                                    + '</a>'
                                    + '</div>'
                                    + '<div class=\"p-right\">'
                                    + '<p class=\"p-name\">'
                                    + value.UrunAd + '</p>'
                                    + '<p class=\"p-rice\">'
                                    + value.ListeNetTutar + ' PUAN</p>'
                                    + '<p>'
                                    + value.UrunAdet + ' adet</p>'
                                    + '</div>'
                                    + '</li>'
                                    + '</ul>'
                                    + '</div>';
                    });

                    CartBlock = CartBlock + '<div class=\"toal-cart\">'
                                + '<span>Toplam</span> <span class=\"toal-price pull-right\">'
                                + result.d.SepetMaster.GenelTutar + ' PUAN'
                                + '</span>'
                                + '</div>'
                                + '<div class=\"cart-buttons\">'
                                + '<a href=\"/Sepet.aspx\" class=\"btn-check-out\">SEPETİM</a>'
                                + '</div>'
                                + '</div>'
                                + '</div>';
                }
            }
        },
        complete: function () {
            $(".shopping-cart-box").fadeIn(500).html(CartHtml + CartBlock);
            $(".shopping-cart-box-ontop-content").fadeIn(500).html(CartBlock);
        },
        error: function () {
        }
    });
}