﻿/* ---------------------------------------------
Scripts initialization
--------------------------------------------- */
$(window).load(function () {
    GetTabPanel(1, '14');
    GetLatestDeal('13');
    HomeSlider();
    NavigationMenuFixed();
});
/* ---------------------------------------------
Scripts ready
--------------------------------------------- */
$(document).ready(function () {
    $("body").addClass("home");
});

function HomeSlider() {
    /** HOME SLIDE**/
        if($('#home-slider').length >0 && $('#contenhomeslider').length >0){
            var slider = $('#contenhomeslider').bxSlider(
                {
                    nextText:'<i class="fa fa-angle-right"></i>',
                    prevText:'<i class="fa fa-angle-left"></i>',
                    auto: true,
                }

            );
        }
        /** Custom page sider**/
        if($('#home-slider').length >0 && $('#contenhomeslider-customPage').length >0){
            var slider = $('#contenhomeslider-customPage').bxSlider(
                {
                    nextText:'<i class="fa fa-angle-right"></i>',
                    prevText:'<i class="fa fa-angle-left"></i>',
                    auto: true,
                    pagerCustom: '#bx-pager',
                    nextSelector: '#bx-next',
                    prevSelector: '#bx-prev',
                }

            );
        }

        if($('#home-slider').length >0 && $('#slide-background').length >0){
            var slider = $('#slide-background').bxSlider(
                {
                    nextText:'<i class="fa fa-angle-right"></i>',
                    prevText:'<i class="fa fa-angle-left"></i>',
                    auto: true,
                    onSlideNext: function ($slideElement, oldIndex, newIndex) {
                       var corlor = $($slideElement).data('background');   
                       $('#home-slider').css('background',corlor);     
                    },
                    onSlidePrev: function ($slideElement, oldIndex, newIndex) {
                       var corlor = $($slideElement).data('background');   
                       $('#home-slider').css('background',corlor);     
                    }
                }

            );
            //slider.goToNextSlide();
        }
}

function NavigationMenuFixed() {
    if ($("#navigationMenu").length) {
        var navOffset = $('#navigationMenu').offset().top;
        $(window).scroll(function () {
            var nav = $('#navigationMenu'),
             scroll = $(window).scrollTop();
            var _scroll = scroll + 750;
            console.log(_scroll + "--" + navOffset);
            if (_scroll >= navOffset) {
                nav.addClass("fixed");
                // $("#navigationMenu").css("top", "800px");
            }
            else {
                nav.removeClass("fixed");
            }
        });
    }
}

function GetTabPanel(TabPanelId, HitUrunTip) {
    if ($("#tab-" + TabPanelId).html().trim() == '') {
        var TabPanelHtml = "";
        $.ajax({
            type: 'POST',
            url: '/Default.aspx/GetHitUrun',
            data: '{"HitUrunTip" : "' + HitUrunTip + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $(".popular-tabs").find(".tab-container").find("#tab-" + TabPanelId.toString()).html("<img src=\"/imagesGld/loading.gif\"/><span>Yükleniyor...</span>").addClass("active");
            },
            success: function (result) {
                if ($(".popular-tabs").find(".tab-container").find(".tab-panel").hasClass("active")) {
                    $(".popular-tabs").find(".tab-container").find(".tab-panel").removeClass("active");
                }

                TabPanelHtml = "";
                TabPanelHtml = '<ul class=\"product-list owl-carousel\" data-dots=\"false\" data-loop=\"true\" data-nav=\"true\"'
                            + 'data-margin=\"30\" data-autoplaytimeout=\"1000\" data-autoplayhoverpause=\"true\" data-responsive=\'{\"0\":{\"items\":1},\"600\":{\"items\":3},\"1000\":{\"items\":3}}\'>';

                $.each(result.d, function (index, value) {
                    TabPanelHtml = TabPanelHtml + '<li>'
                                + '<div class=\"left-block\">'
                                    + '<a href=\"' + value.UrunLink + '\">'
                                    + '<img class=\"img-responsive\" alt=\"product\" src=\"' + value.UrunResimAd + '\"/>'
                                    + '</a>'
                    /*+ '<div class=\"quick-view\">'
                    + '<a title=\"Add to my wishlist\" class=\"heart\" href=\"#\"></a><a title=\"Add to compare\"'
                    + 'class=\"compare\" href=\"#\"></a><a title=\"Quick view\" class=\"search\" href=\"#\"></a>'
                    + '</div>'*/
                    + '<div class=\"add-to-cart\">'
                    + '<a title=\"Sepete Ekle\" href=\"#\" onclick="return SepetEkle(' + value.UrunId + ');">Sepete Ekle</a>'
                    + '</div>'
                    /*+ '<div class=\"group-price\">'
                    + '<span class=\"product-new\">NEW</span> <span class=\"product-sale\">Sale</span>'
                    + '</div>'*/
                                + '</div>'
                                + '<div class=\"right-block\">'
                                    + '<h5 class=\"product-name\">'
                                    + '<a href=\"' + value.UrunLink + '\">' + value.UrunAd + '</a></h5>'
                                    + '<div class=\"content_price\">'
                        + '<span class=\"price product-price\">' + value.UrunNetTutar + ' PUAN</span>';

                    if (value.UrunBrutTutar.toString() != value.UrunNetTutar.toString()) {
                        TabPanelHtml = TabPanelHtml + '<span class=\"price old-price\">' + value.UrunBrutTutar + ' PUAN</span>';
                    }

                    TabPanelHtml = TabPanelHtml + '</div>'
                    /*+ '<div class=\"product-star\">'
                    + '<i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i>'
                    + '<i class=\"fa fa-star\"></i><i class=\"fa fa-star-half-o\"></i>'
                    + '</div>'*/
                            + '</li>';
                });

                TabPanelHtml = TabPanelHtml + '</ul>';
            },
            complete: function () {
                $(".popular-tabs").find(".tab-container").find("#tab-" + TabPanelId.toString()).html(TabPanelHtml).promise().done(function () {
                    $(this).addClass("active").promise().done(function () {
                        /** OWL CAROUSEL**/
                        $(this).find(".owl-carousel").each(function (index, el) {
                            var config = $(this).data();
                            config.navText = ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'];
                            config.smartSpeed = "300";
                            if ($(this).hasClass('owl-style2')) {
                                config.animateOut = "fadeOut";
                                config.animateIn = "fadeIn";
                            }
                            $(this).owlCarousel(config);
                        });
                        $(".owl-carousel-vertical").each(function (index, el) {
                            var config = $(this).data();
                            config.navText = ['<span class="icon-up"></spam>', '<span class="icon-down"></span>'];
                            config.smartSpeed = "900";
                            config.animateOut = "";
                            config.animateIn = "fadeInUp";
                            $(this).owlCarousel(config);
                        });
                    });
                });
            },
            error: function () {
            }
        });
    }
}

function GetLatestDeal(HitUrunTip) {
    if ($(".latest-deals").find(".latest-deal-content").html().trim() == '') {
        var LatestDealHtml = "";
        $.ajax({
            type: 'POST',
            url: '/Default.aspx/GetHitUrun',
            data: '{"HitUrunTip" : "' + HitUrunTip + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $(".latest-deals").find(".latest-deal-content").html("<img src=\"/imagesGld/loading.gif\"/><span>Yükleniyor...</span>");
            },
            success: function (result) {
                LatestDealHtml = "";
                LatestDealHtml = '<ul class=\"product-list owl-carousel\" data-dots=\"false\" data-loop=\"true\" data-nav=\"true\"'
                            + 'data-autoplaytimeout=\"1000\" data-autoplayhoverpause=\"true\" data-responsive=\'{\"0\":{\"items\":1},\"600\":{\"items\":3},\"1000\":{\"items\":1}}\'>';

                $.each(result.d, function (index, value) {
                    LatestDealHtml = LatestDealHtml + '<li>'
                                + '<div class=\"count-down-time\" data-countdown=\"\"></div>'
                                + '<div class=\"left-block\">'
                                    + '<a href=\"' + value.UrunLink + '\">'
                                    + '<img class=\"img-responsive\" alt=\"product\" src=\"' + value.UrunResimAd + '\"/>'
                                    + '</a>'
                    /*+ '<div class=\"quick-view\">'
                    + '<a title=\"Add to my wishlist\" class=\"heart\" href=\"#\"></a><a title=\"Add to compare\"'
                    + 'class=\"compare\" href=\"#\"></a><a title=\"Quick view\" class=\"search\" href=\"#\"></a>'
                    + '</div>'*/
                    + '<div class=\"add-to-cart\">'
                    + '<a title=\"Sepete Ekle\" href=\"#\" onclick="return SepetEkle(' + value.UrunId + ');">Sepete Ekle</a>'
                    + '</div>'
                                + '</div>'
                                + '<div class=\"right-block\">'
                                    + '<h5 class=\"product-name\">'
                                    + '<a href=\"' + value.UrunLink + '\">' + value.UrunAd + '</a></h5>'
                                    + '<div class=\"content_price\">'
                        + '<span class=\"price product-price\">' + value.UrunNetTutar + ' PUAN</span>';

                    if (value.UrunBrutTutar.toString() != value.UrunNetTutar.toString()) {
                        LatestDealHtml = LatestDealHtml + '<span class=\"price old-price\">' + value.UrunBrutTutar + ' PUAN</span>';
                    }

                    LatestDealHtml = LatestDealHtml + '</div>'
                            + '</li>';
                });

                LatestDealHtml = LatestDealHtml + '</ul>';
            },
            complete: function () {
                $(".latest-deals").find(".latest-deal-content").html(LatestDealHtml);
                var config = $(".latest-deals ul").data();
                console.log(config);
                $(".latest-deals ul").owlCarousel({
                    slideSpeed: 300,
                    dots: false,
                    paginationSpeed: 400,
                    singleItem: true
                });

                $(".latest-deals .owl-item").css("width", "230px")

                //$(".latest-deals").find(".latest-deal-content").html(LatestDealHtml).promise().done(function () {
                //    /** OWL CAROUSEL**/
                //    $(this).find(".owl-carousel").each(function (index, el) {
                //        var config = $(this).data();
                //        config.navText = ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'];
                //        config.smartSpeed = "300";
                //        if ($(this).hasClass('owl-style2')) {
                //            config.animateOut = "fadeOut";
                //            config.animateIn = "fadeIn";
                //        }
                //        $(this).owlCarousel(config);
                //    });
                //    $(".owl-carousel-vertical").each(function (index, el) {
                //        var config = $(this).data();
                //        config.navText = ['<span class="icon-up"></spam>', '<span class="icon-down"></span>'];
                //        config.smartSpeed = "900";
                //        config.animateOut = "";
                //        config.animateIn = "fadeInUp";
                //        $(this).owlCarousel(config);
                //    });
                //});
            },
            error: function () {
            }
        });
    }
}