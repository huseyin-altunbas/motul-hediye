﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Text;

public partial class oci : System.Web.UI.Page
{
    SepetIslemleriBLL bllSepetIslemleri = new SepetIslemleriBLL();
    Kullanici kullanici;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Kullanici"] != null)
        {
            kullanici = ((Kullanici)Session["Kullanici"]);
            if (!IsPostBack)
            {
                bllSepetIslemleri.SepetGuncelle(kullanici.ErpCariId);
                SepetBilgileriniGetir();
            }
        }
        else
            UyelikIslemleriBLL.MusteriGirisineYonlendir(Request.Url.PathAndQuery);


    }

    private void SepetBilgileriniGetir()
    {

        DataSet dsSepetIcerik = new DataSet();
        dsSepetIcerik = bllSepetIslemleri.SepetGoruntule(kullanici.ErpCariId);

        ltrOciForm.Text = ociFormHazirla(dsSepetIcerik.Tables["SepetDetayTT"]);
    }

    private string ociFormHazirla(DataTable dtSepetDetayTT)
    {

        StringBuilder sbFormInputs = new StringBuilder();
        if (Session["OciParams"] != null)
        {
            OciParams ociParams = new OciParams();
            ociParams = (OciParams)Session["OciParams"];

            string hook_url = ociParams.HookUrl;

            sbFormInputs.AppendLine("<form action='" + hook_url + "' method='POST' target='_top' id='ociForm' >");
            sbFormInputs.AppendLine("<input type='hidden' name='~OkCode' value='ADDI'/>");
            sbFormInputs.AppendLine("<input type='hidden' name='~target' value='_top'/>");
            sbFormInputs.AppendLine("<input type='hidden' name='~CALLER' value='CTLG'/>");

            for (int i = 0; i < dtSepetDetayTT.Rows.Count; i++)
            {
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-DESCRIPTION[" + (i + 1).ToString() + "]' value = '" + dtSepetDetayTT.Rows[i]["urun_ad"].ToString() + "'/>"); //ürün adı
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-QUANTITY[" + (i + 1).ToString() + "]' value = '" + dtSepetDetayTT.Rows[i]["satis_adet"].ToString() + "'/>"); //sepetteki ürün adedi
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-UNIT[" + (i + 1).ToString() + "]' value = 'PK'/>"); //PK = paket
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-PRICE[" + (i + 1).ToString() + "]' value = '" + (Convert.ToDecimal(dtSepetDetayTT.Rows[i]["liste_nettutar"]) / Convert.ToDecimal(dtSepetDetayTT.Rows[i]["satis_adet"])).ToString().Replace(",", ".") + "'/>"); // fiyat
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-VENDOR[" + (i + 1).ToString() + "]' value = '" + ociParams.ViewPasswd + "'/>");  // duns number 
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-VENDORMAT[" + (i + 1).ToString() + "]' value ='" + dtSepetDetayTT.Rows[i]["urun_id"].ToString() + "'/>"); //uyum stok kodu
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-PRICEUNIT[" + (i + 1).ToString() + "]' value = '1'/>"); // fiyat başına adet
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-CURRENCY[" + (i + 1).ToString() + "]' value = 'TRY'/>"); //döviz kuru
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-LEADTIME[" + (i + 1).ToString() + "]' value = '2'/>");
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-SERVICE[" + (i + 1).ToString() + "]' value = 'N'/>");
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-LONGTEXT_" + (i + 1).ToString() + ":132[]' value ='" + dtSepetDetayTT.Rows[i]["urun_ad"].ToString() + "'/>");
                //FormInputs += "<input type='hidden' name='NEW_ITEM-EXT_CATEGORY_ID[" + (i + 1).ToString() + "]' value='" + UNSPSC + "'/>"; // UNSPSC kodu
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-MATGROUP[" + (i + 1).ToString() + "]' value = '3C02'/>"); // U-code
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-ATTACHMENT[" + (i + 1).ToString() + "]' value = 'http://www.gold.com.tr/UrunResim/BuyukResim/" + dtSepetDetayTT.Rows[i]["resim_ad"].ToString() + "'/>");
                sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-ATTACHMENT_PURPOSE[" + (i + 1).ToString() + "]' value = 'C' />");

                //karşılıkları tabloda tutulacak şimdilik kod içinde
                if (dtSepetDetayTT.Rows[i]["urun_grup1"].ToString() == "54101")
                    sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-MATNR[" + (i + 1).ToString() + "]' value = '1008161' />");
                else if (dtSepetDetayTT.Rows[i]["urun_grup2"].ToString() == "54256")
                    sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-MATNR[" + (i + 1).ToString() + "]' value = '1008165' />");
                else
                    sbFormInputs.AppendLine("<input type='hidden' name='NEW_ITEM-MATNR[" + (i + 1).ToString() + "]' value = '1008163' />");

            }

            sbFormInputs.AppendLine("<input type='submit' id='onay' value='Siparişi Onayla / Get OCI Return' />");
            sbFormInputs.AppendLine("</form>");

        }

        return sbFormInputs.ToString();
    }
}