﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master" AutoEventWireup="true" CodeFile="adore.aspx.cs" Inherits="ds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">

    <style>
        .breadcrumb {
            visibility: hidden;
        }

        #left_column {
            width: 0px;
        }

        .left-module, .page-heading {
            visibility: hidden;
        }

        .mt10 {
            margin-top: -55px;
        }
    </style>

    <div class="mt10">
 <img src="ImagesGld/adore1.jpg" />
        <br />
         <br />
<p><strong>ADORE Mobilya İndirim Kodu Kullanım Koşulları:</strong></p>
<ul>
<li>*Tercih ettiğiniz ADORE İndirim Kodu, www.adoremobilya.com online alışveriş sitesinde ge&ccedil;erlidir. Mağazalarda ge&ccedil;erli değildir.</li>
<li>*ADORE İndirim Kodları 31.12.2018 tarihine kadar ge&ccedil;erli olacaktır.</li>
<li>*100TL ADORE İndirim Kodu, 750TL ve &uuml;zeri alışverişlerde ge&ccedil;erlidir.</li>
<li>*200TL ADORE İndirim Kodu, 1500TL ve &uuml;zeri alışverişlerde ge&ccedil;erlidir.</li>
<li>*İndirim kodu, &uuml;r&uuml;n&uuml; sepete ekleyip kodu kullandığınızda otomatik olarak indirim şeklinde uygulanacaktır.</li>
<li>*Bir alışverişte, ancak bir ADORE İndirim kodu kullanılabilir.</li>
<li>*İndirim kodu, sitedeki diğer indirim kampanyaları ile birleştirilemez.</li>
<li>*İndirim kodu ile yapılan alışverişlerden sonra yapılacak iadelerde, İndirim kodu hakkı geri verilmeyecektir.</li>
<li>*İndirim kodu ile alınan &uuml;r&uuml;nlerde değişim, birebir &uuml;r&uuml;nlerle veya aynı fiyat tutarındaki farklı bir &uuml;r&uuml;n ile yapılabilmektedir. Bu durumda İndirim kodu kullanım hakkı devam eder.</li>
<li>*Adore Mobilya &ouml;nceden haber vermeksizin kampanyayı durdurma, kampanya koşullarını değiştirme hakkını saklı tutar.</li>
<li>*ADORE İndirim kodunuz, siparişiniz onaylandıktan sonra cep telefonunuza sms olarak g&ouml;nderilecektir.</li>
<li>*ADORE İndirim Kodu siparişleriniz onaylandıktan sonra iptal edilemez.</li>
</ul>
   <img src="ImagesGld/adore2.jpg" />
    </div>
</asp:Content>

