﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ MasterType VirtualPath="~/MpAna.master" %>
<%@ Register Src="UserControls/Category.ascx" TagName="Category" TagPrefix="uc1" %>
<%@ Register Src="UserControls/HomeSlider.ascx" TagName="HomeSlider" TagPrefix="uc2" %>
<%@ Register Src="UserControls/HomeRightBanner.ascx" TagName="HomeRightBanner" TagPrefix="uc3" %>
<%@ Register Src="UserControls/LatestDeal.ascx" TagName="LatestDeal" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" type="text/css" href="/assets/css/navigation.css" />
    <script type="text/javascript" src="/assets/js/script-home.js"></script>
    <%-- <script type="text/javascript">
        
         
          $(document).ready(function () {
           
              $("#pop1").click(function () {

                  function ppOn()
                  {
                     // alert("1");

                      $.fancybox.open([
                          {
                              href: '<%Response.Write(PopUpResimHrefUrlGetir()); %>',
                              link: '<%Response.Write(PopUpYonlenecekGetir()); %>'
                          }
                      ], {
                          beforeShow: function () {
                              $(".fancybox-image").wrap('<a href="' + this.link + '"  />')
                          },
                          padding: 2,
                          //nextEffect: 'fade',
                          //prevEffect: 'fade'
                      
                      });
                      return false;
                  }
                  setTimeout(ppOn, 1000);// ready
                  });

                  //alert("1");
                  try {
                      $("#pop1").fancybox().trigger('click');
                  }
                  catch (ex) {
                      alert(ex);
                  }
             
             
         });--%>

         
          
        
    </script>



    <!-- Home slideder-->
    <div id="home-slider">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 slider-left">
                </div>

                <div class="col-sm-9 header-top-right">
                    <div class="homeslider">
                        <uc2:HomeSlider ID="HomeSlider1" runat="server" />
                    </div>
                    <div>
                        <uc3:HomeRightBanner ID="HomeRightBanner1" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Home slideder-->
    <!-- servives -->
    <div class="container">

        <!-- END Home slideder-->
        <!-- servives -->
        <div class="container">
            <div class="service ">
                <div class="col-xs-6 col-sm-3 service-item">
                    <div class="icon">
                        <img alt="services" src="assets/data/s6.png" />
                    </div>
                    <div class="info">
                        <a href="/L70900/">
                            <h3>EV TEKSTİLİ
                            </h3>
                        </a>
                        <%--<span>Detail1</span>--%>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3 service-item">
                    <div class="icon">
                        <img alt="services" src="assets/data/s7_hırdavat.png" />
                    </div>
                    <div class="info">
                        <a href="/M11/Hırdavat">
                            <h3>HIRDAVAT ÜRÜNLERİ
                            </h3>
                        </a>
                        <%--<span>Detail2</span>--%>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3 service-item">
                    <div class="icon">
                        <img alt="services" src="assets/data/s5.png" />
                    </div>
                    <div class="info">
                        <a href="/M6/">
                            <h3>EV DEKORASYON
                            </h3>
                        </a>
                        <%--<span>Detail4</span>--%>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3 service-item">
                    <div class="icon">
                        <img alt="services" src="assets/data/s3.png" />
                    </div>
                    <div class="info">
                        <a href="/MusteriHizmetleri/Iletisim.aspx">
                            <h3>İLETİŞİM FORMU
                            </h3>
                        </a>
                        <%--<span>Detail3</span>--%>
                    </div>
                </div>
            </div>
        </div>
        <!-- end services -->

    </div>
    <!-- end services -->
    <div class="page-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9 page-top-left">
                    <div class="popular-tabs">
                        <ul class="nav-tab">
                            <li class="active"><a data-toggle="tab" href="#tab-1" onclick="return GetTabPanel(1,'14');">EDİTÖRÜN SEÇİMİ</a></li>
                            <li><a data-toggle="tab" href="#tab-2" onclick="return GetTabPanel(2,'3');">BEĞENİLENLER</a></li>
                            <li><a data-toggle="tab" href="#tab-3" onclick="return GetTabPanel(3,'2');">AYNI GÜN
                                KARGO</a></li>
                        </ul>
                        <div class="tab-container">
                            <div id="tab-1" class="tab-panel active">
                            </div>
                            <div id="tab-2" class="tab-panel">
                            </div>
                            <div id="tab-3" class="tab-panel">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 page-top-right">
                    <div class="latest-deals">
                        <h2 class="latest-deal-title">EN ÇOK İNCELENEN</h2>
                        <div class="latest-deal-content">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---->
    <div class="content-page">
        <%--<div id="navigationMenu" class="navigation-menu">
            <ul>
                <li><a class="btn-elevator Beyaz_Esya" href="#elevator-1"><i></i><span>Beyaz Eşya</span></a></li>
                 <li><a class="btn-elevator bilgisayar" href="#elevator-2"><i></i><span>Bilgisayar</span></a></li>
                 <li><a class="btn-elevator telefon" href="#elevator-3"><i></i><span>Cep Telefonu</span></a></li>
                 <li><a class="btn-elevator elektronik" href="#elevator-4"><i></i><span>Elektronik</span></a></li>
                <li><a class="btn-elevator Mutfak_Aletleri" href="#elevator-5"><i></i><span>Mutfak Gereçleri</span></a></li>
                 <li><a class="btn-elevator oyuncak" href="#elevator-6"><i></i><span>Oyuncak</span></a></li>
                <li><a class="btn-elevator Saat" href="#elevator-7"><i></i><span>Saat</span></a></li>
                <li><a class="btn-elevator tatil" href="#elevator-8"><i></i><span>Tatil</span></a></li>
                
            </ul>
        </div>--%>
        <div class="container">
            <asp:Repeater ID="rptCategories" runat="server">
                <ItemTemplate>
                    <uc1:Category ID="Category1" runat="server" CategoryMasterId="<%#Container.DataItem %>" />
                </ItemTemplate>
            </asp:Repeater>
            <!-- Baner bottom -->
            <%--<div class="row banner-bottom">
                <div class="col-sm-6">
                    <div class="banner-boder-zoom">
                        <a href="#">
                            <img alt="ads" class="img-responsive" src="/assets/images/footerbanner/1.jpg" /></a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="banner-boder-zoom">
                        <a href="#">
                            <img alt="ads" class="img-responsive" src="/assets/images/footerbanner/2.jpg" /></a>
                    </div>
                </div>
            </div>--%>
            <!-- end banner bottom -->
        </div>
    </div>

    <%--<a class="initialism slide_open btn btn-success" data-popup-ordinal="1" id="open_13091582">Add</a>
    <div runat="server" clientidmode="Static" id="slide" class="well" style="background-color: transparent;  text-align: right; z-index: 199999999; position: relative; border-color: transparent;"  >--%><%--/*width: 600px; height: 678px;*/--%>
    <%-- <button runat="server" clientidmode="Static" id="btnKapatButton" class="slide_close btn btn-default" style="float: right; position: absolute; left: 100%; margin-left: -52px; padding: 5px 5px 5px 5px !important; cursor: pointer;">Kapat</button>--%>
    <%-- <a id="btnKapatButton"  class="slide_close btn btn-default" style="float: right; position: absolute; left: 100%; margin-top: -6px; margin-left: -49px; padding: 5px 5px 5px 5px !important; cursor: pointer;">
            <img src="/imagesgld/close.png" /></a>
        <iframe id="PopupX" runat="server" clientidmode="Static" src="Popup.aspx" width="100%" height="100%"></iframe>

    </div>--%>


    <div id="popupDiv" class="popup bg-white" style="visibility: hidden; display: none; overflow: hidden;">
        <!--pop up box begins-->
        <script type="text/javascript">

</script>

        <%--<a href='<%Response.Write(PopUpYonlenecekGetir()); %>'>
            <img src='<%Response.Write(PopUpResimHrefUrlGetir()); %>' style="width:100%; height:100%;"></a>--%>

        <a id="pop1" class="fancybox-effects-a" href="<%Response.Write(PopUpResimHrefUrlGetir()); %>" />

    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="Kapat()">&times;</button>
                    <span>En çok göz atılan ürünler</span>
                </div>
                <div class="modal-body">


                    <div class="modal-body" style="/* height: 400px; */">


                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                   <%if (String.IsNullOrEmpty(UrunResim))%>
                                    <%{%>
                                          
                                    <%}%>
                                     <%else%>
                                     <%{%>
                                    <%if (UrunResim.IndexOf("http://www.bayev.net/") > -1)%>
                                        <%{%>
                                            
                                        <%}%>

                                    <%else%>
                                     <%{%>
                                    <img src="<%=UrunResim %>" style="width: 300px; height: 250px;margin-top:8px;max-width:-webkit-fill-available;border:2px solid #d3d3d3">
                                    <%}%>
                                          
                                    <%}%>
                                    
                                </div>
                                <div class="col-md-6" style="/* width: 300px; */
    /* height: 300px; */">
                                    <center style="margin-top: 15px;">
                                        <span style="font-size:14px;color:black"><%=UrunAdi %></span></center>
                                    <a href="<%=link%>&popup=1" ><button type="button" class="btn btn-primary" style="width:100%;margin-top:35px;background-color:#ff8201">Ürünü incele</button></a>
                                </a>
                                </div>


                            </div>

                           
                                <!--<button type="button" class="btn-add-cart" >
                                    Sepete Ekle</button>-->
                           
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>




    <!--pop up box ends-->

   



    <script type="text/javascript">
        $(document).ready(function () {

            
            $("#pop1").click(function () {
                $.fancybox.open([
                    {
                        href: '<%Response.Write(PopUpResimHrefUrlGetir()); %>',
                        link: '<%Response.Write(PopUpYonlenecekGetir()); %>'
                    }
                ], {
                    beforeShow: function () {
                        $(".fancybox-image").wrap('<a href="' + this.link + '" target="_blank" />')
                    },
                    padding: 2,
                    //nextEffect: 'fade',
                    //prevEffect: 'fade'
                });
                return false;
            });

                $("#pop1").fancybox().trigger('click');

            }); // ready
    </script>

    <%--    <script type="application/javascript">

        var popup_acildimi = 0;
        <% if (PopUpYonlenecekGetir().ToString().Length>4){ Response.Write("setInterval(popup_ac, 500);"); } %>
        //

        function popup_ac() {
            if (popup_acildimi < 1) {
                $.fancybox({
                    'content': $("#popupDiv").html(),
                    padding: 2,
                    onClosed: function () {
                        //$(".d2").click();
                    }

                   
                });
                
                popup_acildimi = 1;
            }
        }
    </script>--%>





    <%-- <a id="pop1" class="fancybox-effects-a" href="<%Response.Write(PopUpResimHrefUrlGetir()); %>" />
    &nbsp;



     
    <script type="text/javascript">
        $(document).ready(function () {
            $(".fancybox-thumb").fancybox({
                prevEffect: 'none',
                nextEffect: 'none',
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    thumbs: {
                        width: 50,
                        height: 50
                    }
                }
            });
        });

    </script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    overlay: {
                        speedOut: 0
                    }
                }
            });


        });
    </script>




    <script type="text/javascript">
        $(document).ready(function () {

            $('#slide').popup({
                focusdelay: 400,
                outline: true,
                vertical: 'top'
            });

            var ClickCount = 0;

            $('#btnKapatButton').click(function () {
                if (ClickCount == 0) {

                    ClickCount = 1;

                      <%-- 
                    $(document).ready(function () {
                        $("#PopupX").attr('src', 'Popup.aspx?Type=2');
                        $("#open_13091582").trigger('click');
                    });
                    --%>



                  }
              });

              $('#slide_wrapper').click(function () {
                  if (ClickCount == 0) {

                      ClickCount = 1;

                    <%--
                    $(document).ready(function () {
                        $("#PopupX").attr('src', 'Popup.aspx?Type=2');
                        $("#open_13091582").trigger('click');
                    });
                    --%>
                }
            });

              $(document).ready(function () {
                  $("#open_13091582").trigger('click');
              });

              $(document).ready(function () {
                  $("#open_13091582").trigger('click');
              });

              $("#open_13091582").hide();
          });
    </script>



    <style>
        #slide_background {
            -webkit-transition: all 0.5s 0.5s;
            -moz-transition: all 0.5s 0.5s;
            transition: all 0.5s 0.5s;
        }

        #slide,
        #slide_wrapper {
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
            transition: all 0.5s;
        }

        #slide {
            -webkit-transform: translateX(0) translateY(-40%);
            -moz-transform: translateX(0) translateY(-40%);
            -ms-transform: translateX(0) translateY(-40%);
            transform: translateX(0) translateY(-40%);
        }

        .popup_visible #slide {
            -webkit-transform: translateX(0) translateY(0);
            -moz-transform: translateX(0) translateY(0);
            -ms-transform: translateX(0) translateY(0);
            transform: translateX(0) translateY(0);
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript" src="Script/jquery.popupoverlay.js"></script>
    <script type="text/javascript" src="/fancyBox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="/fancyBox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    <style type="text/css">
        #slide {
            margin-top: 50px !important;
        }

        .slide_close .btn .btn-default {
            padding: 5px 5px 5px 5px !important;
        }
    </style>
    <style>
    </style>




</asp:Content>
