﻿<%@ WebHandler Language="C#" Class="BkmExpressYonlendir" %>

using System;
using System.Web;
using System.Web.SessionState;
using Goldb2cEntity;

public class BkmExpressYonlendir : IHttpHandler, IReadOnlySessionState
{

    public void ProcessRequest(HttpContext context)
    {
        BkmExpressParams bkmExpressParams = new BkmExpressParams();

        context.Response.ContentType = " text/html";
        if (context.Session["BkmExpressParams"] != null)
        {
            bkmExpressParams = (BkmExpressParams)context.Session["BkmExpressParams"];
        }

        if (!string.IsNullOrEmpty(bkmExpressParams.PostForm))
            context.Response.Write(bkmExpressParams.PostForm);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}