﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="AdresBilgileri.aspx.cs" Inherits="AdresBilgileri" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/goldBasket.css?v=1.1" rel="stylesheet" type="text/css" />
    <link href="/assets/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="/assets/js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.validationEngine-tr.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#form1").validationEngine({ ajaxFormValidation: true, scroll: false });
        });
            
    </script>
    <!-- page wapper-->
    <div class="columns-container">
        <div class="container" id="columns">
            <!-- breadcrumb -->
            <div class="breadcrumb clearfix">
                <a class="home" href="/" title="Ana Sayfa">Ana Sayfa</a> <span class="navigation-pipe">
                    &nbsp;</span> <span class="navigation_page">Sepet</span> <span class="navigation-pipe">
                        &nbsp;</span> <span class="navigation_page">Adres</span>
            </div>
            <!-- ./breadcrumb -->
            <!-- page heading-->
            <%--<h2 class="page-heading no-line">
                <span class="page-heading-title2">ADRES BİLGİLERİM</span>
            </h2>--%>
            <!-- ../page heading-->
            <div id="addressInfo" class="page-content page-order">
                <ul class="step">
                    <li><span>1. Sepet</span></li>
                    <li class="current-step"><span>2. Adres</span></li>
                    <li><span>3. Onay</span></li>
                </ul>
                <div class="heading-counter warning">
                    ADRES BİLGİLERİM
                </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div>
                            <asp:Repeater ID="rptAdresler" runat="server" OnItemDataBound="rptAdresler_ItemDataBound">
                                <HeaderTemplate>
                                    <div class="order-detail-content">
                                        <table class="table table-bordered table-responsive cart_summary" style="margin-bottom: 0px !important;">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Ünvan
                                                    </th>
                                                    <th>
                                                        Adres
                                                    </th>
                                                    <th>
                                                        Teslimat
                                                    </th>
                                                    <th style="display:none;">
                                                        Fatura
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tbody>
                                        <tr>
                                            <td class="address_header">
                                                <asp:Label ID="lblAdresKod" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td class="address_description">
                                                <asp:Label ID="lblAdres" runat="server" Text=""></asp:Label>
                                                <br />
                                                <asp:Label ID="lblTelefon" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td class="address_select">
                                                <input name="rbTeslimat" type="radio" value='<%# DataBinder.Eval(Container.DataItem, "KayitId") %>'
                                                    <%# Container.ItemIndex.ToString() == "0" ? "checked" : "" %> />
                                            </td>
                                            <td class="address_select" style="display:none;">
                                                <input name="rbFatura" type="radio" value='<%# DataBinder.Eval(Container.DataItem, "KayitId") %>'
                                                    <%# Container.ItemIndex.ToString() == "0" ? "checked" : "" %> />
                                            </td>
                                            <td>
                                                <a class="duzenle" onclick="DivYeniAdresAc(<%# DataBinder.Eval(Container.DataItem, "KayitId") %>,'addressInfo')"
                                                    title="Düzenle">Düzenle</a>
                                                <asp:LinkButton ID="btnSil" runat="server" CssClass="sil" OnClientClick="return confirm('Seçtiğiniz adres bilgileri silinecektir. Silmek istediğinizden emin misiniz?');"
                                                    OnClick="btnSil_Click">Sil</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </tbody>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table> </div>
                                </FooterTemplate>
                            </asp:Repeater>
                            
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                
                <div class="address_navigation">
                                <a onclick="DivYeniAdresAc(0,'addressInfo');" class="fix-btn-left"><span class="spriteRedBtn fontTre fontSz14">
                                    + Yeni Adres Ekle</span></a>
                                <asp:LinkButton ID="btnGonder" runat="server" CssClass="next-btn" OnClick="btnGonder_Click">
                                                   Adrese Gönder</asp:LinkButton>
                            </div>
                <!-- Fatura Main Bitti -->
                <div style="clear: both">
                </div>
                <br />
                <!-- Yeni Adres -->
                <div id="DivYeniAdres" class="order-detail-content" style="display: none; max-width: 580px;
                    margin: 0 auto;">
                    <table class="table table-bordered table-responsive cart_summary" style="margin-bottom: 0px !important;">
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <span style="font-weight: bold;">Adres Bilgileri</span>
                                    <div class="YeniAdrsClose" onclick="DivYeniAdresKapat();" style="float: right; color: #000;">
                                        Kapat</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="YeniAdrsIText">
                                        Adres Başlığı :</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAdresBaslik" runat="server" CssClass="YeniAdrsInpt validate[required]"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="YeniAdrsIText">
                                        İsim Soyisim <span class="redSpot">*</span> :</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtYazismaUnvan" runat="server" CssClass="YeniAdrsInpt validate[required]"></asp:TextBox>
                                    <div class="adresAlanAciklama redSpot">
                                        * Bu alana Ad Soyad veya Firma Adı yazınız</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="YeniAdrsIText">
                                        Adres :</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAdresTanim" runat="server" TextMode="MultiLine" CssClass="YeniAdrsTxtAr validate[required]"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="YeniAdrsIText">
                                        Şehir :</div>
                                </td>
                                <td>
                                    <div style="width: 350px; float: left">
                                        <asp:DropDownList ID="dpSehir" runat="server" AutoPostBack="False" CssClass="YeniAdrsCombo validate[required]"
                                            onChange="getIlceListesi(this.value,'');">
                                        </asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="YeniAdrsIText">
                                        İlçe :</div>
                                </td>
                                <td>
                                    <div style="width: 350px; float: left">
                                        <asp:DropDownList ID="dpIlce" runat="server" AutoPostBack="False" CssClass="YeniAdrsCombo validate[required]">
                                        </asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="YeniAdrsIText">
                                        Posta Kodu :</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPostaKodu" runat="server" CssClass="YeniAdrsInpt"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="YeniAdrsIText">
                                        Telefon (Sabit) :</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSabitTelKod" runat="server" CssClass="YeniAdrsInpt2"></asp:TextBox>
                                    <asp:TextBox ID="txtSabitTel" runat="server" CssClass="YeniAdrsInpt3"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="YeniAdrsIText">
                                        Telefon (GSM) :</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCepTelKod" runat="server" CssClass="YeniAdrsInpt2 validate[required]"></asp:TextBox>
                                    <asp:TextBox ID="txtCepTel" runat="server" CssClass="YeniAdrsInpt3 validate[required]"></asp:TextBox>
                                    <div style="float: left; font-size: 10px; font-style: italic;">
                                        *Lütfen cep telefonunuzu doğru giriniz. Yapacağınız alışveriş ile ilgili bilgiler
                                        cep telefonunuz ve e-posta adresiniz aracılığıyla size ulaştırlacaktır.</div>
                                </td>
                            </tr>
                            <%--<tr>
                                <td>
                                </td>
                                <td>
                                    <div class="radioSecim" style="margin: 0px !important;">
                                        <div class="radioText">
                                            <input name="rbBireyselKurumsal" type="radio" onclick="BireyselKurumsal('bireysel');"
                                                checked="checked" />Bireysel</div>
                                        <div class="radioText">
                                            <input name="rbBireyselKurumsal" type="radio" onclick="BireyselKurumsal('kurumsal');" />Kurumsal</div>
                                    </div>
                                </td>
                            </tr>--%>
                            <tr class="bireysel" id="DivBireysel" style="display:none;">
                                <td>
                                    <div class="YeniAdrsIText">
                                        TC Kimlik No :</div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTcKimlikNo" runat="server" CssClass="YeniAdrsInpt">77973212716</asp:TextBox>
                                    <div class="bireyselText">
                                        <input name="chkTcVatandasi" type="checkbox" class="bireyselCheck" />
                                        TC uyruklu değilim</div>
                                </td>
                            </tr>
                            <tr class="kurumsal" id="DivKurumsal">
                                <td>
                                    <%--<div class="YeniAdrsIText">
                                        Firma Adı :
                                    </div>--%>
                                    <div class="YeniAdrsIText">
                                        Vergi Dairesi :
                                    </div>
                                    <div class="YeniAdrsIText">
                                        Vergi No :
                                    </div>
                                </td>
                                <td>
                                    <%--<asp:TextBox ID="txtFirmaAd" runat="server" CssClass="YeniAdrsInpt"></asp:TextBox><br />--%>
                                    <asp:TextBox ID="txtVergiDairesi" runat="server" CssClass="YeniAdrsInpt"></asp:TextBox><br />
                                    <asp:TextBox ID="txtVergiNumarasi" runat="server" CssClass="YeniAdrsInpt"></asp:TextBox><br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input id="hdnAdresId" type="hidden" />
                                    <div class="address_navigation">
                                        <a onclick="ValidateForm();" class="fix-btn-right">Kaydet</a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- ./page wapper-->
    <script type="text/javascript">

        function ValidateForm() {
            if (jQuery('#form1').validationEngine('validate') == true)
                AdresKaydet();

        }

        function DivYeniAdresAc(AdresId, HrefId) {
            $('#hdnAdresId').val(AdresId);
            if (AdresId == 0) {
                $('#DivYeniAdres').delay(200).slideDown({
                    complete: function () {
                        var target = "#" + HrefId;
                        $('html, body').stop().animate({
                            'scrollTop': $(target).offset().top - 50
                        }, 500);
                    }
                });
                $("#<%=txtAdresBaslik.ClientID %>").val('');
                $("#<%=txtAdresTanim.ClientID %>").val('');
                $("#<%=txtYazismaUnvan.ClientID %>").val('');
                $("#<%=txtSabitTelKod.ClientID %>").val('');
                $("#<%=txtSabitTel.ClientID %>").val('');
                $("#<%=txtCepTelKod.ClientID %>").val('');
                $("#<%=txtCepTel.ClientID %>").val('');
                $("#<%=txtPostaKodu.ClientID %>").val('');
                $("#<%=txtVergiNumarasi.ClientID %>").val('');
                $("#<%=txtVergiDairesi.ClientID %>").val('');
                $("#<%=txtTcKimlikNo.ClientID %>").val('');
            }
            else {
                $.ajax({
                    type: 'POST',
                    url: '/AdresBilgileri.aspx/getAdresBilgileri',
                    data: '{ "AdresKod":"' + AdresId + '" }',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {

                        var SehirKod = result.d.SehirKod;
                        var IlceKod = result.d.IlceKod;
                        getIlceListesi(SehirKod, IlceKod);
                        $("#<%=dpSehir.ClientID %>").val(SehirKod);
                        $('#DivYeniAdres').delay(200).slideDown({
                            complete: function () {
                                var target = "#" + HrefId;
                                $('html, body').stop().animate({
                                    'scrollTop': $(target).offset().top - 50
                                }, 500);
                            }
                        });
                        $("#<%=txtAdresBaslik.ClientID %>").val(result.d.AdresBaslik);
                        $("#<%=txtAdresTanim.ClientID %>").val(result.d.AdresTanim);
                        $("#<%=txtYazismaUnvan.ClientID %>").val(result.d.YazismaUnvan);
                        $("#<%=txtSabitTelKod.ClientID %>").val(result.d.SabitTelKod);
                        $("#<%=txtSabitTel.ClientID %>").val(result.d.SabitTel);
                        $("#<%=txtCepTelKod.ClientID %>").val(result.d.CepTelKod);
                        $("#<%=txtCepTel.ClientID %>").val(result.d.CepTel);
                        $("#<%=txtPostaKodu.ClientID %>").val(result.d.PostaKod);
                        $("#<%=txtVergiNumarasi.ClientID %>").val(result.d.VergiNo);
                        $("#<%=txtVergiDairesi.ClientID %>").val(result.d.VergiDaire);
                        $("#<%=txtTcKimlikNo.ClientID %>").val(result.d.TcKimlikNo);





                    },
                    error: function () {
                        alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                    }
                });



            }
        }

        function DivYeniAdresKapat() {
            $('#DivYeniAdres').slideUp(1000);
        }

        function BireyselKurumsal(tiklanan) {
            if (tiklanan == 'bireysel') {
                $('#DivKurumsal').css('display', 'none');
                //$('#DivBireysel').css('display', 'block');
                $('#DivBireysel').css('display', 'table-row');
            }
            else if (tiklanan == 'kurumsal') {
                $('#DivBireysel').css('display', 'none');
                //$('#DivKurumsal').css('display', 'block');
                $('#DivKurumsal').css('display', 'table-row');
            }
        }

        function getIlceListesi(SehirKod, SecilenIlce) {

            if (SehirKod != "") {
                var controlId = "<%=dpIlce.ClientID %>";


                $.ajax({
                    type: 'POST',
                    url: '/AdresBilgileri.aspx/getIlceListesi',
                    data: '{ "SehirKod":"' + SehirKod + '" }',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        $('#' + controlId).find('option').remove().append('<option value="0">Seçiniz</option>');
                        $.each(result.d, function (index, data) {
                            $('#' + controlId).append(
                      $('<option value="' + data.IlceAd + '"></option>').html(data.IlceAd));
                        });

                        if (SecilenIlce != '')
                            $("#<%=dpIlce.ClientID %>").val(SecilenIlce);
                    },
                    error: function () {
                        alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                    }
                });
            }
        }

        function AdresKaydet() {

            var CariAdresId = $("#hdnAdresId").val();
            var AdresBaslik = $("#<%=txtAdresBaslik.ClientID %>").val();
            var AdresTanim = $("#<%=txtAdresTanim.ClientID %>").val();
            var YazismaUnvan = $("#<%=txtYazismaUnvan.ClientID %>").val();
            var SehirKod = $("#<%=dpSehir.ClientID %>").val();
            var IlceKod = $("#<%=dpIlce.ClientID %>").val();
            var MobilTelefon = $("#<%=txtCepTelKod.ClientID %>").val() + $("#<%=txtCepTel.ClientID %>").val();
            var SabitTelefon = $("#<%=txtSabitTelKod.ClientID %>").val() + $("#<%=txtSabitTel.ClientID %>").val();
            var PostaKod = $("#<%=txtPostaKodu.ClientID %>").val();
            var TckimlikNo = $("#<%=txtTcKimlikNo.ClientID %>").val();
            var VergiDaire = $("#<%=txtVergiDairesi.ClientID %>").val();
            var VergiNo = $("#<%=txtVergiNumarasi.ClientID %>").val();

            var postaKoduIlk2Karakter = PostaKod.substr(0, 2);

            if (SehirKod == 'ADANA' && postaKoduIlk2Karakter == '01') { }
            else if (SehirKod == 'ADIYAMAN' && postaKoduIlk2Karakter == '02') { }
            else if (SehirKod == 'AFYON' && postaKoduIlk2Karakter == '03') { }
            else if (SehirKod == 'AĞRI' && postaKoduIlk2Karakter == '04') { }
            else if (SehirKod == 'AMASYA' && postaKoduIlk2Karakter == '05') { }
            else if (SehirKod == 'ANKARA' && postaKoduIlk2Karakter == '06') { }
            else if (SehirKod == 'ANTALYA' && postaKoduIlk2Karakter == '07') { }
            else if (SehirKod == 'ARTVİN' && postaKoduIlk2Karakter == '08') { }
            else if (SehirKod == 'AYDIN' && postaKoduIlk2Karakter == '09') { }
            else if (SehirKod == 'BALIKESİR' && postaKoduIlk2Karakter == '10') { }
            else if (SehirKod == 'BİLECİK' && postaKoduIlk2Karakter == '11') { }
            else if (SehirKod == 'BİNGÖL' && postaKoduIlk2Karakter == '12') { }
            else if (SehirKod == 'BİTLİS' && postaKoduIlk2Karakter == '13') { }
            else if (SehirKod == 'BOLU' && postaKoduIlk2Karakter == '14') { }
            else if (SehirKod == 'BURDUR' && postaKoduIlk2Karakter == '15') { }
            else if (SehirKod == 'BURSA' && postaKoduIlk2Karakter == '16') { }
            else if (SehirKod == 'ÇANAKKALE' && postaKoduIlk2Karakter == '17') { }
            else if (SehirKod == 'ÇANKIRI' && postaKoduIlk2Karakter == '18') { }
            else if (SehirKod == 'ÇORUM' && postaKoduIlk2Karakter == '19') { }
            else if (SehirKod == 'DENİZLİ' && postaKoduIlk2Karakter == '20') { }
            else if (SehirKod == 'DİYARBAKIR' && postaKoduIlk2Karakter == '21') { }
            else if (SehirKod == 'EDİRNE' && postaKoduIlk2Karakter == '22') { }
            else if (SehirKod == 'ELAZIĞ' && postaKoduIlk2Karakter == '23') { }
            else if (SehirKod == 'ERZİNCAN' && postaKoduIlk2Karakter == '24') { }
            else if (SehirKod == 'ERZURUM' && postaKoduIlk2Karakter == '25') { }
            else if (SehirKod == 'ESKİŞEHİR' && postaKoduIlk2Karakter == '26') { }
            else if (SehirKod == 'GAZİANTEP' && postaKoduIlk2Karakter == '27') { }
            else if (SehirKod == 'GİRESUN' && postaKoduIlk2Karakter == '28') { }
            else if (SehirKod == 'GÜMÜŞHANE' && postaKoduIlk2Karakter == '29') { }
            else if (SehirKod == 'HAKKARİ' && postaKoduIlk2Karakter == '30') { }
            else if (SehirKod == 'HATAY' && postaKoduIlk2Karakter == '31') { }
            else if (SehirKod == 'ISPARTA' && postaKoduIlk2Karakter == '32') { }
            else if (SehirKod == 'MERSİN' && postaKoduIlk2Karakter == '33') { }
            else if (SehirKod == 'İSTANBUL' && postaKoduIlk2Karakter == '34') { }
            else if (SehirKod == 'İZMİR' && postaKoduIlk2Karakter == '35') { }
            else if (SehirKod == 'KARS' && postaKoduIlk2Karakter == '36') { }
            else if (SehirKod == 'KASTAMONU' && postaKoduIlk2Karakter == '37') { }
            else if (SehirKod == 'KAYSERİ' && postaKoduIlk2Karakter == '38') { }
            else if (SehirKod == 'KIRKLARELİ' && postaKoduIlk2Karakter == '39') { }
            else if (SehirKod == 'KIRŞEHİR' && postaKoduIlk2Karakter == '40') { }
            else if (SehirKod == 'KOCAELİ' && postaKoduIlk2Karakter == '41') { }
            else if (SehirKod == 'KONYA' && postaKoduIlk2Karakter == '42') { }
            else if (SehirKod == 'KÜTAHYA' && postaKoduIlk2Karakter == '43') { }
            else if (SehirKod == 'MALATYA' && postaKoduIlk2Karakter == '44') { }
            else if (SehirKod == 'MANİSA' && postaKoduIlk2Karakter == '45') { }
            else if (SehirKod == 'KAHRAMANMARAŞ' && postaKoduIlk2Karakter == '46') { }
            else if (SehirKod == 'MARDİN' && postaKoduIlk2Karakter == '47') { }
            else if (SehirKod == 'MUĞLA' && postaKoduIlk2Karakter == '48') { }
            else if (SehirKod == 'MUŞ' && postaKoduIlk2Karakter == '49') { }
            else if (SehirKod == 'NEVŞEHİR' && postaKoduIlk2Karakter == '50') { }
            else if (SehirKod == 'NİĞDE' && postaKoduIlk2Karakter == '51') { }
            else if (SehirKod == 'ORDU' && postaKoduIlk2Karakter == '52') { }
            else if (SehirKod == 'RİZE' && postaKoduIlk2Karakter == '53') { }
            else if (SehirKod == 'SAKARYA' && postaKoduIlk2Karakter == '54') { }
            else if (SehirKod == 'SAMSUN' && postaKoduIlk2Karakter == '55') { }
            else if (SehirKod == 'SİİRT' && postaKoduIlk2Karakter == '56') { }
            else if (SehirKod == 'SİNOP' && postaKoduIlk2Karakter == '57') { }
            else if (SehirKod == 'SİVAS' && postaKoduIlk2Karakter == '58') { }
            else if (SehirKod == 'TEKİRDAĞ' && postaKoduIlk2Karakter == '59') { }
            else if (SehirKod == 'TOKAT' && postaKoduIlk2Karakter == '60') { }
            else if (SehirKod == 'TRABZON' && postaKoduIlk2Karakter == '61') { }
            else if (SehirKod == 'TUNCELİ' && postaKoduIlk2Karakter == '62') { }
            else if (SehirKod == 'ŞANLIURFA' && postaKoduIlk2Karakter == '63') { }
            else if (SehirKod == 'UŞAK' && postaKoduIlk2Karakter == '64') { }
            else if (SehirKod == 'VAN' && postaKoduIlk2Karakter == '65') { }
            else if (SehirKod == 'YOZGAT' && postaKoduIlk2Karakter == '66') { }
            else if (SehirKod == 'ZONGULDAK' && postaKoduIlk2Karakter == '67') { }
            else if (SehirKod == 'AKSARAY' && postaKoduIlk2Karakter == '68') { }
            else if (SehirKod == 'BAYBURT' && postaKoduIlk2Karakter == '69') { }
            else if (SehirKod == 'KARAMAN' && postaKoduIlk2Karakter == '70') { }
            else if (SehirKod == 'KIRIKKALE' && postaKoduIlk2Karakter == '71') { }
            else if (SehirKod == 'BATMAN' && postaKoduIlk2Karakter == '72') { }
            else if (SehirKod == 'ŞIRNAK' && postaKoduIlk2Karakter == '73') { }
            else if (SehirKod == 'BARTIN' && postaKoduIlk2Karakter == '74') { }
            else if (SehirKod == 'ARDAHAN' && postaKoduIlk2Karakter == '75') { }
            else if (SehirKod == 'IĞDIR' && postaKoduIlk2Karakter == '76') { }
            else if (SehirKod == 'YALOVA' && postaKoduIlk2Karakter == '77') { }
            else if (SehirKod == 'KARABÜK' && postaKoduIlk2Karakter == '78') { }
            else if (SehirKod == 'KİLİS' && postaKoduIlk2Karakter == '79') { }
            else if (SehirKod == 'OSMANİYE' && postaKoduIlk2Karakter == '80') { }
            else if (SehirKod == 'DÜZCE' && postaKoduIlk2Karakter == '81') { }

            else {
                alert("Lütfen posta kodunu kontrol ediniz.");
                return;
            }

            if (PostaKod.length != 5) {
                alert("Lütfen posta kodunu kontrol ediniz.");
                return;
            }

            $.ajax({
                type: 'POST',
                url: '/AdresBilgileri.aspx/AdresKaydet',
                data: '{ "CariAdresId" : "' + CariAdresId + '", "AdresBaslik" : "' + AdresBaslik + '", "AdresTanim" : "' + AdresTanim + '", "YazismaUnvan" : "' + YazismaUnvan + '", "SehirKod" : "' + SehirKod + '", "IlceKod" : "' + IlceKod + '", "MobilTelefon" : "' + MobilTelefon + '", "SabitTelefon" : "' + SabitTelefon + '", "PostaKod" : "' + PostaKod + '", "TckimlikNo" : "' + TckimlikNo + '", "VergiDaire" : "' + VergiDaire + '", "VergiNo" : "' + VergiNo + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    $('#DivYeniAdres').slideUp(1000);
                    __doPostBack('<%=UpdatePanel1.ClientID %>', '');
                },
                error: function () {
                    alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                }
            });
        }

        $("#<%=txtCepTelKod.ClientID %>").mask("599");
        $("#<%=txtCepTel.ClientID %>").mask("9999999");
        $("#<%=txtSabitTelKod.ClientID %>").mask("999");
        $("#<%=txtSabitTel.ClientID %>").mask("9999999");
        $("#<%=txtTcKimlikNo.ClientID %>").mask("99999999999");
        $("#<%=txtPostaKodu.ClientID %>").mask("99999");
        $("#<%=txtVergiNumarasi.ClientID %>").mask("9999999999");
       

    
    </script>
</asp:Content>
