﻿using Goldb2cBLL;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cEntity;
using System.Data;


public partial class MpAna : System.Web.UI.MasterPage
{

    private string NewRemarketingCode;
    UyelikIslemleriBLL _bllUyelikIslemleri = new UyelikIslemleriBLL();
    public string newRemarketingCode
    {
        get { return NewRemarketingCode; }
        set { NewRemarketingCode = value; }
    }
    public string UrunId = "";
    public string UrunGrupId = "";
    public string KullaniciId = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        //if (!Request.IsSecureConnection)
        //{
        //    string path = string.Format("https{0}", Request.Url.AbsoluteUri.Substring(4));

        //    Response.Redirect(path);
        //}

        if (Session["Kullanici"] == null)
        {
            Response.Redirect("/Login.aspx");
        }
        else
        {
            Kullanici kullanici = (Kullanici)Session["Kullanici"];
            KullaniciId = kullanici.KullaniciId.ToString();
        }


        if (!string.IsNullOrEmpty(Request.QueryString["UrunId"]))
        {
            UrunId = Request.QueryString["UrunId"];
        }

        if (Request.QueryString["Urungrupid"] != null)
            UrunGrupId = Request.QueryString["Urungrupid"].ToString();

        if (GetUser_IP().Contains("213.14.32.141"))//91.93.38.23
        {

        }
        else if (GetUser_IP().Contains("192.168"))
        {

        }
        else
        {
            //  Response.Redirect("/yapim_asamasi.html");
        }


        Kullanici kulanici = (Kullanici)Session["Kullanici"];
        string fullUrl = Request.Url.ToString();
        if (!kulanici.IlkSifreDegistimi)
        {
            if (!fullUrl.Contains("/Hesabim/SifremiDegistir.aspx"))
            { Response.Redirect("/Hesabim/SifremiDegistir.aspx"); }

        }

    }

    protected string GetUser_IP()
    {
        string VisitorsIPAddr = string.Empty;
        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
        {
            VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
        }
        return VisitorsIPAddr;
    }
}
