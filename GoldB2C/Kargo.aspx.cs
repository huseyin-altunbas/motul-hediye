﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;

public partial class Kargo : System.Web.UI.Page
{
    LojistikIslemleriBLL bllLojistikIslemleri = new LojistikIslemleriBLL();
    SiparisIslemleriBLL bllSiparisIslemleri = new SiparisIslemleriBLL();

    public string FaturaYazismaUnvan;
    public string FaturaAdres;
    public string FaturaTelefon;
    public string TeslimatYazismaUnvan;
    public string TeslimatAdres;
    public string TeslimatTelefon;

    int SiparisMasterId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["SiparisMasterId"] != null)
        {
            SiparisMasterId = Convert.ToInt32(Session["SiparisMasterId"]);

            DataTable dtSiparisAdres = new DataTable();
            dtSiparisAdres = bllSiparisIslemleri.GetSiparisAdres(SiparisMasterId);
            if (dtSiparisAdres.Rows.Count > 0)
            {
                FaturaYazismaUnvan = dtSiparisAdres.Rows[0]["fyazisma_unvan"].ToString();
                FaturaAdres = dtSiparisAdres.Rows[0]["fadres_tanim"].ToString() + " " + dtSiparisAdres.Rows[0]["filce_kod"].ToString() + "/" + dtSiparisAdres.Rows[0]["fsehir_kod"].ToString();
                FaturaTelefon = "Telefon No: " + dtSiparisAdres.Rows[0]["fmobil_telefon"].ToString();
                if (!string.IsNullOrEmpty(dtSiparisAdres.Rows[0]["fsabit_telefon"].ToString()))
                    FaturaTelefon = FaturaTelefon + " - " + dtSiparisAdres.Rows[0]["fsabit_telefon"].ToString();

                TeslimatYazismaUnvan = dtSiparisAdres.Rows[0]["syazisma_unvan"].ToString();
                TeslimatAdres = dtSiparisAdres.Rows[0]["sadres_tanim"].ToString() + " " + dtSiparisAdres.Rows[0]["silce_kod"].ToString() + "/" + dtSiparisAdres.Rows[0]["ssehir_kod"].ToString();
                TeslimatTelefon = "Telefon No: " + dtSiparisAdres.Rows[0]["smobil_telefon"].ToString();
                if (!string.IsNullOrEmpty(dtSiparisAdres.Rows[0]["ssabit_telefon"].ToString()))
                    TeslimatTelefon = TeslimatTelefon + " - " + dtSiparisAdres.Rows[0]["ssabit_telefon"].ToString();

                rptKargo.DataSource = bllLojistikIslemleri.GetLojistikKart(SiparisMasterId);
                rptKargo.DataBind();
            }
            else
                Response.Redirect("~/AdresBilgileri.aspx", false);
        }
        else
            Response.Redirect("~/Sepet.aspx", false);

    }

    protected void btnIleri_Click(object sender, EventArgs e)
    {
        int KargoId = 0;
        if (SiparisMasterId > 0)
        {
            if (int.TryParse(Request["rbKargoSecim"].ToString(), out KargoId))
            {
                if (bllSiparisIslemleri.SiparisSevkiyatKaydet(ref SiparisMasterId, KargoId) > 0)
                    Response.Redirect("~/Odeme.aspx", false);
            }
        }
        else

            Response.Redirect("~/Sepet.aspx", false);

    }

    protected void btnGeri_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/AdresBilgileri.aspx");
    }
}