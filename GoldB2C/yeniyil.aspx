﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master" AutoEventWireup="true" CodeFile="yeniyil.aspx.cs" Inherits="yeniyil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" Runat="Server">


    <div class="mt10">
<h2><strong>Kampanya Katılım Koşulları </strong></h2>
<p>&nbsp;</p>
<h3><strong>Cratos Premium Hotel &amp; Casino kampanyası</strong></h3>
<p>Haziran 2020 i&ccedil;inde yapılacak olan konaklamalarda ge&ccedil;erlidir.</p>
<p>Kampanyada belirtilen puan miktarı 2 yetişkin i&ccedil;in olup , Haziran Ayı boyunca Pazar g&uuml;n&uuml; giriş &Ccedil;arşamba g&uuml;n&uuml; &ccedil;ıkış ve Pazartesi g&uuml;n&uuml; giriş Perşembe g&uuml;n&uuml; &ccedil;ıkış yapılacak şekilde 3 gece 4 g&uuml;n konaklamalarda ge&ccedil;erlidir.</p>
<p>Belirtilen puan miktarına Standart Odada Tam Pansiyon Plus konaklama, direkt noktalardan gidiş-geliş u&ccedil;ak bileti ve &ccedil;ift y&ouml;n transfer dahildir.&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p>U&ccedil;uş i&ccedil;in belirtilen direkt noktalar; İstanbul, Ankara, İzmir, Adana , G.Antep, Hatay, Antalya&rsquo;dır.</p>
<p>Belirtilen direkt u&ccedil;uş noktaları dışında katılım sağlayacak &uuml;yelerimizin aktarma u&ccedil;uşları kendilerine ait olacaktır.</p>
<p>Rezervasyon i&ccedil;in <strong>0850 450 86 87 </strong>numaralı hattan bizlere ulaşarak kayıt yaptırabilirsiniz.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3><strong>Limak Atlantis De Luxe Hotel &amp; Resort kampanyası</strong></h3>
<p>03.06.2020 tarihi itibariyle yapılacak Haziran Ayı i&ccedil;indeki konaklamalarda ge&ccedil;erlidir.</p>
<p>Kampanyada belirtilen puan miktarı 2 yetişkin i&ccedil;in olup 4 gece 5 g&uuml;n konaklamalarda ge&ccedil;erlidir.</p>
<p>Belirtilen puan miktarına Standart Odada Ultra Her Şey Dahil konaklama dahildir.</p>
<p>Haziran Ayı i&ccedil;inde se&ccedil;ilen tarihlerde m&uuml;saitlik yok ise yine aynı ay i&ccedil;inde alternatif tarihler sizlere iletilecektir.</p>
<p>Ulaşım kampanyaya dahil değildir.</p>
<p>Rezervasyon i&ccedil;in <strong>0850 450 86 87</strong> numaralı hattan bizlere ulaşarak kayıt yaptırabilirsiniz.</p>
</asp:Content>

