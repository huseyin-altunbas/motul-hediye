﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="MagazaKampanya.aspx.cs" Inherits="MagazaKampanya" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/magazalarimiz.css?v=1.1.6" rel="stylesheet" type="text/css" />
    <%--    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>--%>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#accordion").accordion({ autoHeight: false });
        });
    </script>
    <div class="rootercnav">
        <div class="rcframe">
            <div style="width: 100%;" class="rootername">
                <a href="https://motulteam.com/">Anasayfa</a> / Mağaza Kampanyaları
            </div>
        </div>
    </div>
    <div class="stores">
        <div class="storesContainer">
            <!-- Left Container Start -->
            <div class="storesLeftContainer2">
                <h1>
                    Mağaza Seçiniz</h1>
                <div class="storeCampaign">
                    <asp:Repeater ID="rptMagazalar" runat="server" OnItemDataBound="rptMagazalar_ItemDataBound">
                        <HeaderTemplate>
                            <div id="accordion">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <h3>
                                <%--    <img src="ImagesGld/campaigneActive.png" />--%><a href="#"><%#DataBinder.Eval(Container.DataItem,"magaza_ad") %></a></h3>
                            <asp:Repeater ID="rptKampanyalar" OnItemDataBound="rptKampanyalar_ItemDataBound"
                                runat="server">
                                <HeaderTemplate>
                                    <div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <a href="#" onclick="IcerikDegistir('http://<%=ImageDomain%><%#DataBinder.Eval(Container.DataItem,"imaj_Adres") %>');">
                                        <%#DataBinder.Eval(Container.DataItem,"duyuru_baslik") %></a></ItemTemplate>
                                <FooterTemplate>
                                    </div></FooterTemplate>
                            </asp:Repeater>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div></FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <!-- Left Container End -->
            <!-- Right Container Start -->
            <div class="storesRightContainer2" style="display: block;">
                <asp:Image ID="imgKampanyaIcerik" alt="Kampanyalar" runat="server" />
                <%-- <img id="imgKampanyaIcerik" runat="server" alt="Kampanyalar" />--%>
            </div>
            <!-- Right Container End -->
        </div>
    </div>
    <script type="text/javascript">
        function IcerikDegistir(Icerik) {
            $('.storesRightContainer2').fadeOut(250);
            $("#<%=imgKampanyaIcerik.ClientID%>").attr('src', Icerik);
            $('.storesRightContainer2').fadeIn(250);

            if ($("#<%=imgKampanyaIcerik.ClientID%>").attr('src') != "")
                $('.storesRightContainer2').show();
        }
    </script>
</asp:Content>
