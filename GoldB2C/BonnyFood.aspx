﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="BonnyFood.aspx.cs" Inherits="BonnyFood" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/landing/landing_bonnyfood.css" rel="stylesheet" type="text/css" />
    <div class="bonnyFood">
        <ul class="breadcrumbs">
            <li><a href="http://https://motulteam.com/">Anasayfa</a> </li>
            <li>/</li>
            <li><span>BonnyFood Kampanyası</span> </li>
        </ul>
        <div class="bonnyContainer">
            <div class="bonnyBanner">
                <div class="bonnyClear">
                </div>
            </div>
            <!-- Left Container Start -->
            <div class="bonnyLeftContainer">
                <h2>
                    Kampanya Detayları</h2>
                <ul>
                    <li>Kampanya 31 Ağustos 2012 tarihine kadar geçerlidir.</li>
                    <li>Bu indirim kodu, bonnyfood.com.tr adresinde bulunan tüm ürünlerde geçerli olmak
                        üzere 70 PUAN üzerinde alışveriş yapan müşteriler 25 PUAN olarak indirim kullanabilir.</li>
                    <li>Bu indirim kodunu elinde bulunan müşteri, siparişini www.bonnyfood.com.tr adresinden,
                       </br> 444 13 71 BonnyFood çağrı merkezinden veya şubelerinden siparişini verilebilir.</li>
                    <li>İndirim kodu tek kullanımlıktır ve diğer kampanyalar ile birleştirilemez, değiştirelemez.</li>
                    <li>Her sipariş için bir (1) fırsat kodu kullanılabilir.</li>
                    <li>Bu indirim kodu para ile kimseye satılamaz.</li>
                    <li>Anneler günü öncesi BonnyFood’un şubesi olan illerde saat 12:00’a kadar olan siparişler
                        gün içerisinde, saat 12:00’dan sonraki siparişler ertesi gün teslim edilir. Şube
                        olmayan illerde kargolu gönderim ile teslimat 48 saat içerisinde gerçekleşir. Anneler
                        gününde verilen siparişler ise şubesi olan illerde teslimat saat taahhüdü verilmeden
                        gün içerisinde teslim edilecektir. Şubesi olmayan illerde yine kargo ile teslimat
                        48 saat içerisinde gerçekleşir. Ürünlerin teslimatı BonnyFood şubelerinin olduğu
                        illerde 30 km. mesafeye kadar ücretsizdir. Kargolu gönderimlerde kargo ücreti müşteriye
                        aittir.</li>
                </ul>
                <div class="bonnyClear">
                </div>
                <label class="uiButton">
                    <a href="https://motulteam.com/" title="">Alışverişe Başla</a>
                </label>
            </div>
            <!-- Left Container End -->
            <!-- Right Container Start -->
            <div class="bonnyRightContainer">
                <div class="bonnyBranches">
                    <h3>
                        BonnyFood Şubeleri</h3>
                    <p>
                        İstanbul 5 şube, Ankara, İzmir, Kocaeli, Adana, Bursa, Antalya, Mersin, Kırşehir,
                        Malatya, Sakarya, Hatay, Kayseri, Samsun, Tekirdağ, Eskişehir.</p>
                    <a href="http://www.bonnyfood.com.tr/cms/subelerimiz/" target="_blank" title="">Tümünü
                        Göster</a>
                </div>
            </div>
            <!-- Right Container End -->
        </div>
    </div>
</asp:Content>
