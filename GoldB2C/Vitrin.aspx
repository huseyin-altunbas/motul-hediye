﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="Vitrin.aspx.cs" Inherits="Vitrin" EnableEventValidation="false" ValidateRequest="false"
    EnableViewState="true" %>

<%@ MasterType VirtualPath="~/MpAna.master" %>
<%@ Register Src="UserControls/SolKategoriKirilimMenu.ascx" TagName="SolKategoriKirilimMenu"
    TagPrefix="uc1" %>
<%@ Register Src="UserControls/SolKategoriUstGrupMenu.ascx" TagName="SolKategoriUstGrupMenu"
    TagPrefix="uc2" %>
<%@ Register Src="UserControls/DinamikBanner.ascx" TagName="DinamikBanner" TagPrefix="uc3" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="/css/bestsellersProducts.css" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="/assets/css/filtermenu.css?v=1.1" />
    <div class="rootercontainer">
        <div class="columns-container">
            <div class="container" id="columns">
                <!-- breadcrumb -->
                <div class="breadcrumb clearfix">
                    <asp:Literal ID="ltrAyakizi" runat="server"></asp:Literal>
                </div>
                <!-- ./breadcrumb -->
                <!-- row -->
                <div class="row">
                    <!-- Left colunm -->
                    <div class="column col-xs-12 col-sm-3" id="left_column">
                        <!-- leftmaster -->
                        <div class="leftmaster">
                            <div id="lmframe">
                                <!-- filter menu -->
                                <div class="filterMenu">
                                    <!-- Alt Kategoriler -->
                                    <div class="filterContainer">
                                        <%--<uc1:SolKategoriKirilimMenu ID="SolKategoriKirilimMenu1" runat="server" />--%>
                                        <br />
                                        <br />
                                        <%--<uc2:SolKategoriUstGrupMenu ID="SolKategoriUstGrupMenu1" runat="server" />--%>
                                    </div>
                                    <!-- Alt Kategoriler -->
                                </div>
                                <!-- filter menu -->
                                <!-- banner -->
                                <div class="filterBanner">
                                    <%--<img src="/imagesgld/banner_toshiba175x113.jpg" width="175" alt="TOSHIBA" />--%>
                                </div>
                                <!-- banner -->
                            </div>
                            <div runat="server" id="DivSeoContent" class="seocontent">
                                <asp:Literal ID="ltrSeoContent" runat="server"></asp:Literal>
                                <span id="spnSeoDevam" runat="server" class="curhand" onclick="ShowSeoContent(this);">
                                    Devamı</span>
                            </div>
                            <asp:HiddenField ID="hdnSeoContent" runat="server" />
                        </div>
                        <!-- leftmaster -->
                    </div>
                    <!-- ./left colunm -->
                    <!-- Center colunm-->
                    <div class="center_column col-xs-12 col-sm-9" id="center_column">
                        <!-- view-product-list-->
                        <div id="view-product-list" class="view-product-list" style="margin-top: 0px;">
                            <!-- PRODUCT LIST -->
                            <ul class="row product-list">
                                <!-- product -->
                                <asp:Repeater ID="rptUrunler" runat="server" OnItemDataBound="rptUrunler_ItemDataBound">
                                    <ItemTemplate>
                                        <li class="col-xs-12 col-sm-4">
                                            <div class="product-container">
                                                <div class="left-block">
                                                    <a href="<%# DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%# DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                                                        <img src="<%# DataBinder.Eval(Container.DataItem, "resim_ad") %>" width="200" height="200"
                                                            alt="<%# DataBinder.Eval(Container.DataItem, "urun_ad") %>" /></a>
                                                    <div class="add-to-cart">
                                                        <a title="Sepete Ekle" href="#" onclick="return SepetEkle(<%# DataBinder.Eval(Container.DataItem, "urun_id") %>);">
                                                            Sepete Ekle</a>
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <h5 class="product-name">
                                                        <a href="<%# DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%# DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                                                            <%# DataBinder.Eval(Container.DataItem, "urun_ad") %></a></h5>
                                                    <div class="content_price">
                                                        <span class="price product-price">
                                                            <asp:Label ID="lblnet_tutar" runat="server" Text=""></asp:Label>
                                                        </span><span class="price old-price">
                                                            <asp:Label ID="lblbrut_tutar" runat="server" Text=""></asp:Label>
                                                        </span><span>
                                                            <asp:Label ID="lblBilgi" CssClass="kdv" runat="server" Text=""></asp:Label></span>
                                                    </div>
                                                    <div>
                                                    </div>
                                                    <div class="info-orther">
                                                        <div class="product-desc">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <!-- product -->
                            </ul>
                            <!-- ./PRODUCT LIST -->
                        </div>
                        <!-- ./view-product-list-->
                        <!-- box product -->
                        <div class="page-product-box">
                            <asp:Repeater ID="rptCokSatanlarVitrin" runat="server">
                                <HeaderTemplate>
                                    <h3 class="heading">
                                        BU KATEGORİDE EN ÇOK SATAN ÜRÜNLER</h3>
                                    <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav="true"
                                        data-margin="30" data-autoplaytimeout="1000" data-autoplayhoverpause="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":3}}'>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <div class="product-container">
                                            <div class="left-block">
                                                <a href="<%#DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>"
                                                    class="thumbsPic">
                                                    <img src="<%#DataBinder.Eval(Container.DataItem, "resim_ad") %>" alt="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>" />
                                                </a><a href="<%#DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>"
                                                    class="productName"></a>
                                                <div class="add-to-cart">
                                                    <a title="Sepete Ekle" href="#" onclick="return SepetEkle(<%# DataBinder.Eval(Container.DataItem, "urun_id") %>);">
                                                        Sepete Ekle</a>
                                                </div>
                                            </div>
                                            <div class="right-block">
                                                <h5 class="product-name">
                                                    <a href="<%#DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>"
                                                        class="productName">
                                                        <%#DataBinder.Eval(Container.DataItem, "urun_ad") %></a></h5>
                                                <div class="salesBarBox">
                                                    <div class="salesBarFrame">
                                                        <div class="salesBarColor" style='width: <%#DataBinder.Eval(Container.DataItem, "tercih_oran") %>%;'>
                                                        </div>
                                                    </div>
                                                    <span>%<%#DataBinder.Eval(Container.DataItem, "tercih_oran") %></span><a href="<%#DataBinder.Eval(Container.DataItem, "urun_link") %>"><span>Bu
                                                        ürün
                                                        <%#DataBinder.Eval(Container.DataItem, "tercih_oran") %>
                                                        oranında tercih edilmiştir.</span></a>
                                                </div>
                                                <div class="content_price">
                                                    <p class="product-price">
                                                        <a href="<%#DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                                                            <%#DataBinder.Eval(Container.DataItem, "net_tutar", "{0:###,###,###}")%>&nbsp;<span>PUAN</span></a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <!-- ./box product -->
                    </div>
                    <!-- ./ Center colunm -->
                </div>
                <!-- ./row-->
            </div>
        </div>
    </div>
    <!-- footerbanner -->
    <%-- <div class="footerbanner">
             <img src="imagesgld/banner_panasonic186x116.jpg" alt="Panasonic" /><img src="imagesgld/banner_bonus316x116.jpg"
            alt="Bonus" /><img src="imagesgld/banner_dell480x116.jpg" alt="Dell" />
    </div>--%>
    <!-- footerbanner -->
    <script type="text/javascript">
        function ShowSeoContent(sender) {
            $("#" + sender.id).hide();
            $("#<%=DivSeoContent.ClientID %>").html($("#<%=DivSeoContent.ClientID %>").html() + $("#<%=hdnSeoContent.ClientID %>").val());
        }</script>
    <asp:Literal ID="ltrRemarketingAlan" runat="server"></asp:Literal>
</asp:Content>
