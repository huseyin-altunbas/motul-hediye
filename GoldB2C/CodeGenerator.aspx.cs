﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CodeGenerator : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GenelSiparisListeleme();
    }

    void GenelSiparisListeleme()
    {
        Goldb2cBLL.SiparisGoruntulemeBLL bllSiparisGoruntuleme = new Goldb2cBLL.SiparisGoruntulemeBLL();
        System.Data.DataSet dsSiparis = bllSiparisGoruntuleme.GenelSiparisListeleme(0, 0, 0, 0, Convert.ToDateTime("01.01.1970"), Convert.ToDateTime("01.01.2900"));

        string _path = MapPath(@"~\App_Code\PocoFolder");
        System.Data.DataTable dt = new System.Data.DataTable();
       //int iii = dt.Columns.Count;
       // foreach (System.Data.DataColumn item in dt.Columns)
       // {
       //     //item.AllowDBNull
       // }

        dsSiparis.WriteXmlSchema(_path + "\\GenelSiparisListeleme.xsd");
        dsSiparis.WriteXml(_path + "\\GenelSiparisListeleme.xml");
    }
}