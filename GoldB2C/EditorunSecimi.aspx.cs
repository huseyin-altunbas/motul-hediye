﻿using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class EditorunSecimi : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Editörün Seçimi";
        Page.MetaDescription = "Editörlerimizin en çok beğendiği ve sizin için seçtikleri birbirinden güzel ve özel ürünleri incelemeden geçmeyin";
        //string OciFirmaKod = "";
        //if (Session["Kullanici"] != null)
        //    OciFirmaKod = ((Kullanici)Session["Kullanici"]).OciFirmaKod;

        HitUrunIslemleriBLL bllHitUrunIslemleri = new HitUrunIslemleriBLL();
        DataTable dtHitUrun = bllHitUrunIslemleri.HitUrunKartListesi("14", 0, 0);
        var teminEdilenUrunler = dtHitUrun.AsEnumerable().Where(u => u.Field<bool>("temin_durum") == true && u.Field<decimal>("net_tutar") > 0);
        dtHitUrun = teminEdilenUrunler.Count() > 0 ? teminEdilenUrunler.CopyToDataTable() : dtHitUrun.Clone();

        int rowsay = 0;
        foreach (DataRow row in dtHitUrun.Rows)
        {
            if (dtHitUrun.Rows[rowsay]["resim_ad"].ToString().IndexOf("http") == -1)
            {
                dtHitUrun.Rows[rowsay]["resim_ad"] = "http://www.gold.com.tr/UrunResim/BuyukResim/" + dtHitUrun.Rows[rowsay]["resim_ad"];
            }
            rowsay = rowsay + 1;
        }

        rptEditorunSecimleri.DataSource = dtHitUrun;
        rptEditorunSecimleri.DataBind();
    }

    protected void rptEditorunSecimleri_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = e.Item.DataItem as DataRowView;
            Label lblBrutTutar = (Label)e.Item.FindControl("lblBrutTutar");
            Label lblNetTutar = (Label)e.Item.FindControl("lblNetTutar");

            if (Convert.ToBoolean(drv.Row["temin_durum"]))
            {
                decimal brut_tutar = Math.Round(Convert.ToDecimal(drv.Row["brut_tutar"]), 2);
                decimal net_tutar = Math.Round(Convert.ToDecimal(drv.Row["net_tutar"]), 2);

                if (brut_tutar > net_tutar)
                {
                    lblBrutTutar.Visible = true;
                    lblNetTutar.Visible = true;
                }
                else
                {
                    lblBrutTutar.Visible = false;
                    lblNetTutar.Visible = true;
                }

            }
        }
    }
}