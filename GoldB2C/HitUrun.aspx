﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="HitUrun.aspx.cs" Inherits="HitUrun" %>

<%@ Register Src="UserControls/HitUrunlerMenu.ascx" TagName="HitUrunlerMenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/specialCategory.css?v=1.1" rel="stylesheet" type="text/css" />
    <!-- rootercnav -->
    <div class="rootercnav">
        <div class="rcframe">
            <div class="rootername">
                <a href="https://motulteam.com/">Anasayfa</a> /
                <asp:Literal ID="ltrHitUrunBaslik" runat="server"></asp:Literal>
            </div>
           <%-- <div class="rooterbuttons">
                <!--etodo-3.2-->
                <a href="#" class="someClass" title="Yeni Ürün">
                    <img src="/imagesgld/icon_yeniurunOn.png" width="29" height="29" alt="Yeni Ürün" /></a>
                <a href="#" class="someClass" title="Kargo Bedava">
                    <img src="/imagesgld/icon_kargobedavaOn.png" width="29" height="29" alt="Kargo Bedava" /></a>
                <a href="#" class="someClass" title="İndirimli Ürün">
                    <img src="/imagesgld/icon_indirimliurunOn.png" width="29" height="29" alt="İndirimli Ürün" /></a>
                <a href="#" class="someClass" title="Hediyeli Ürün">
                    <img src="/imagesgld/icon_hediyeliurunOn.png" width="29" height="29" alt="Hediyeli Ürün" /></a>
                <a href="#" class="someClass" title="Özel Ürün">
                    <img src="/imagesgld/icon_interneteozelurunOn.png" width="29" height="29" alt="Özel Ürün" /></a>
                <a href="#" class="someClass" title="Hızlı Kargo">
                    <img src="/imagesgld/icon_hizlikargoOn.png" width="29" height="29" alt="Hızlı Kargo" /></a>
            </div>--%>
        </div>
    </div>
    <!-- rootercnav -->
    <!-- Menu Start -->
    <div class="specialCategoryLeft">
        <uc1:HitUrunlerMenu ID="HitUrunlerMenu1" runat="server" />
    </div>
    <!-- Menu End -->
    <div class="rightmaster">
        <div id="rmframe">
            <ul class="productList">
                <!-- product -->
                <asp:Repeater ID="rptUrunler" runat="server" OnItemDataBound="rptUrunler_ItemDataBound">
                    <ItemTemplate>
                        <li>
                            <div class="productContainer">
                                <div class="productPic">
                                    <a href="<%# DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%# DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                                        <img src="/UrunResim/BuyukResim/<%# DataBinder.Eval(Container.DataItem, "resim_ad") %>"
                                            width="200" height="200" alt="<%# DataBinder.Eval(Container.DataItem, "resim_ad") %>" /></a></div>
                                <div class="productName">
                                    <a href="<%# DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%# DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                                        <%# DataBinder.Eval(Container.DataItem, "urun_ad") %></a></div>
                                <div class="productPrice">
                                    <asp:Label ID="lblBrutTutar" CssClass="discount" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="lblNetTutar" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="lblBilgi" CssClass="kdv" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="productCompare">
                                    <%-- <a class="jqTransformCheckbox" href="#"></a>Karşılaştır--%></div>
                                <%--  <div class="productComment">
                                        <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                        <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                        <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                        <img src="imagesgld/star_On15x13.png" width="15" height="13" alt="Yorum" />
                                        <img src="imagesgld/star_Off15x13.png" width="15" height="13" alt="Yorum" />
                                        (142 Yorum)
                                    </div>--%>
                                <div class="productButtons">
                                    <asp:Image ID="imgIndirimli" runat="server" Width="29" Height="29" alt="İndirimli Ürün"
                                        title="İndirimli Ürün" class="someClass" />
                                    <asp:Image ID="imgAyniGunSevk" runat="server" Width="29" Height="29" alt="Aynı Gün Sevk"
                                        title="Aynı Gün Sevk" class="someClass" />
                                    <asp:Image ID="imgHediyeliUrun" runat="server" Width="29" Height="29" alt="Hediyeli Ürün"
                                        title="Hediyeli Ürün" class="someClass" />
                                    <asp:Image ID="imgKuryeileSevk" runat="server" Width="29" Height="29" alt="Kurye ile Sevk"
                                        title="Kurye ile Sevk" class="someClass" />
                                    <asp:Image ID="imgKargoBedava" runat="server" Width="29" Height="29" alt="Kargo Bedava"
                                        title="Kargo Bedava" class="someClass" />
                                    <asp:Image ID="imgYeniUrun" runat="server" Width="29" Height="29" alt="Yeni Ürün"
                                        title="Yeni Ürün" class="someClass" />
                                </div>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
                <!-- product -->
            </ul>
        </div>
    </div>
</asp:Content>
