﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="YeniYilaOzelHediyeler.aspx.cs"
    Inherits="YeniYilaOzelHediyeler" %>

<%@ Register Src="UserControls/TopMenu.ascx" TagName="TopMenu" TagPrefix="uc1" %>
<%@ Register Src="UserControls/FooterKategoriler.ascx" TagName="FooterKategoriler"
    TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Gold Bilgisayar - Türkiye'nin En Büyük Online Alışveriş Sitesi</title>
    <meta name="robots" content="index, follow" />
    <meta name="description" content="Gold Bilgisayar, notebook, cep telefonu, lcd tv, fotoğraf makinası, beyaz eşya gibi bir çok ürün Türkiye'nin teknoloji marketinde!">
    <meta name="keywords" content="bilgisayar, notebook, cep telefonu, lcd tv, fotoğraf makinası, beyaz eşya, ev aletleri, iphone, laptop, elektronik, oem parçalar" />
    <meta name="robots" content="noodp">
    <meta property="fb:app_id" content="388600557882151" />
    <meta name="verify-v1" content="DOOHLPMc15rzeJl54pjgtkpPPzdhWFL9ZaEU76S0f9c=">
    <link rel="shortcut icon" href="/imagesGld/lafarge.ico" />
    <!--  Global -->
    <link href="/css/gold.css?v=4.2.3.7" rel="stylesheet" type="text/css" />
    <link href="/css/goldHeader.css?v=1.2.4" rel="stylesheet" type="text/css" />
    <link href="/css/goldForms.css?v=1.0.2" rel="stylesheet" type="text/css" />
    <link href="/css/goldProducts.css?v=2.1.3.7" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/flashobject.js"></script>
    <script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="/js/Gold/goldGenel.js?v=2.1.4" type="text/javascript"></script>
    <script src="/js/Gold/ebultenKayit.js" type="text/javascript"></script>
    <!--[if lt IE 7.]>
<script defer type="text/javascript" src="js/pngfix.js"></script>
<![endif]-->
    <!--  Global -->
    <!--  Pagination  -->
    <link rel="stylesheet" href="/css/pagination.css" type="text/css" />
    <!--  Pagination  -->
    <!--  MainNavigation -->
    <link rel="stylesheet" type="text/css" href="/css/navigation.css?v=4.1" />
    <script type="text/javascript" src="/js/navigation.js"></script>
    <!--  MainNavigation -->
    <!--  UserNavigation -->
    <link rel="stylesheet" type="text/css" href="/css/userdropdown.css?v=1.2" />
    <script type="text/javascript">
        $(function () {
            $('.userdropdown').each(function () {
                $(this).parent().eq(0).hover(function () {
                    $('.userdropdown:eq(0)', this).show();
                }, function () {
                    $('.userdropdown:eq(0)', this).hide();
                });
            });

            $('.listdropdown').each(function () {
                $(this).parent().eq(0).hover(function () {
                    $('.listdropdown:eq(0)', this).show();
                }, function () {
                    $('.listdropdown:eq(0)', this).hide();
                });
            });


        });
    </script>
    <!--  UserNavigation -->
    <!--  jCarousel -->
    <script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/carousel.css" />
    <link rel="stylesheet" type="text/css" href="/css/carouselie7.css" />
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.first-and-second-carousel').jcarousel({
                auto: 5,
                wrap: 'last'
            });
            jQuery('#third-carousel').jcarousel({
                vertical: true
            });
        });
    </script>
    <!--  jCarousel -->
    <!--  BannerSlider -->
    <link rel="stylesheet" type="text/css" href="/css/nivo-slider.css?v=1.1" />
    <script type="text/javascript" src="/js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            $('#slider').nivoSlider({
                animSpeed: 500,
                pauseTime: 7000
            });
        });
    </script>
    <!--  BannerSlider -->
    <!--  ToolTip  -->
    <link rel="stylesheet" type="text/css" href="/css/toolTip.css" />
    <script type="text/javascript" src="/js/jquery.toolTip.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".someClass").tipTip({ maxWidth: "auto", edgeOffset: 10 });
        });
    </script>
    <!--  ToolTip  -->
    <!--  FilterMenu  -->
    <%--    <script type="text/javascript">
                $(document).ready(function () {
                    //slides the element with class "menu_body" when paragraph with class "menu_head" is clicked 
                    $("#firstpane p.menu_head").click(function () {
                        $(this).css({ backgroundImage: "url(imagesgld/arrow_filter.gif) no-repeat center left" }).next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
                        $(this).siblings().css({ backgroundImage: "url(imagesgld/arrow_filter.gif) no-repeat center left" });
                    });
                    //slides the element with class "menu_body" when mouse is over the paragraph
                    $("#secondpane p.menu_head").mouseover(function () {
                        $(this).css({ backgroundImage: "url(imagesgld/arrow_filtersub.gif) no-repeat center left" }).next("div.menu_body").slideDown(500).siblings("div.menu_body").slideUp("slow");
                        $(this).siblings().css({ backgroundImage: "url(imagesgld/arrow_filtersub.gif) no-repeat center left" });
                    });
                });
    </script>
    <script type="text/javascript" src="/js/jquery.nestedAccordion.js"></script>
    <script type="text/javascript" src="/js/expand.js"></script>
    <script type="text/javascript" src="/js/filtermenu.js"></script>
    <link rel="stylesheet" href="/css/filtermenu.css" type="text/css"> --%>
    <!--  FilterMenu  -->
    <!--  OffersCarousel  -->
    <link rel="stylesheet" href="/css/offerscarousel.css" type="text/css" />
    <script type="text/javascript" src="/js/jquery.offerscarousel.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            /*$('body').removeClass('no-js');
            $('#my-carousel-1').carousel();
            $('#my-carousel-2').carousel({
            itemsPerPage: 4,
            itemsPerTransition: 4,
            speed: 500
            });*/
            $('#my-carousel-3').carousel({
                itemsPerPage: 3,
                itemsPerTransition: 3,
                easing: 'linear',
                noOfRows: 2
            });
        });
    </script>
    <!--Footer Sizin için seçtiklerimiz-->
    <!-- ANIMATE AND EASING LIBRARIES -->
    <script type="text/javascript" src="/Tools/ShowBiz/services-plugin/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="/Tools/ShowBiz/services-plugin/js/jquery.cssAnimate.mini.js"></script>
    <!-- TOUCH AND MOUSE WHEEL SETTINGS -->
    <script type="text/javascript" src="/Tools/ShowBiz/services-plugin/js/jquery.touchwipe.min.js"></script>
    <script type="text/javascript" src="/Tools/ShowBiz/services-plugin/js/jquery.mousewheel.min.js"></script>
    <!-- jQuery SERVICES Slider  -->
    <script type="text/javascript" src="/Tools/ShowBiz/services-plugin/js/jquery.themepunch.services.js"></script>
    <link rel="stylesheet" type="text/css" href="/Tools/ShowBiz/services-plugin/css/settings.css?v=1.3"
        media="screen" />
    <!--Footer Sizin için seçtiklerimiz-->
    <!-- STYLE SHEETS -->
    <link href="/css/newYear.css" rel="stylesheet" type="text/css" />
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <!-- header -->
    <div id="header">
        <uc1:TopMenu ID="TopMenu1" runat="server" />
    </div>
    <!-- header -->
    <%--COUNT DOWN TIMER--%>
    <div id="headerCountDown">
        <div id="wrapper">
            <!-- DARK COUNTDOWN -->
            <div id="daysOne">
                <div id="days">
                </div>
                <div class="daysImg">
                </div>
            </div>
            <div id="hoursOne">
                <div id="hours">
                </div>
                <div class="hoursImg">
                </div>
            </div>
            <div id="minsOne">
                <div id="mins">
                </div>
                <div class="minsImg">
                </div>
            </div>
            <div id="secsOne">
                <div id="secs">
                </div>
                <div class="secsImg">
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <%--COUNT DOWN TIMER--%>
    <div id="wrap">
        <!-- content -->
        <div id="contentholder">
            <a href="/OK8037937/Cebe-Yarayan-Fiyatlar" class="cepTelefonlari"><span>Cep Telefonları</span></a>
            <a href="/OK8035310/Tabletlerde-Buyuk-indirim" class="tabletler"><span>Tabletler</span></a>
            <a href="/OK8037776/Dizinize-Gelen-Firsat" class="notebooklar"><span>Dizüstü Bilgisayarlar</span></a>
            <a href="/OK9714237/Gulumseyin-Fiyatlari-Cekiyoruz" class="fotografMak"><span>Fotoğraf
                Makinaları</span></a> <a href="/OK8039654/Samsung-Beyaz-Esya" class="evAletleri"><span>
                    Ev Aletleri</span></a> <a href="/OK8037777/Tv-de-Ilk" class="televizyonlar"><span>Televizyonlar</span></a>
        </div>
        <!-- content -->
    </div>
    <!-- footer -->
    <div id="footerOut">
        <uc2:FooterKategoriler ID="FooterKategoriler1" runat="server" />
    </div>
    <!-- footer -->
    <div id="footerSnow">
        <script src="/js/snow.js" type="text/javascript"></script>
    </div>
    </form>
    <!-- Ebülten kayıt -->
    <form name="ccoptin" id="ccoptin" action="http://motulteam.com/Subscribe/subscribe1.asp"
    target="_blank" method="get" enctype="multipart/form-data">
    </form>
    <script src="/js/countDown.js?v=2" type="text/javascript"></script>
    <script type="text/javascript">        CountMeDown();</script>
</body>
</html>
