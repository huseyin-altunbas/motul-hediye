﻿<%@ WebHandler Language="C#" Class="_3dYonlendir" %>

using System;
using System.Web;
using System.Web.SessionState;

public class _3dYonlendir : IHttpHandler, IReadOnlySessionState
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = " text/html";
        if (context.Session["PostData"] != null)
            context.Response.Write(context.Session["PostData"].ToString());


        context.Response.Write("<script language=\"javascript\">document.getElementById('fmBankForm').submit();</script>");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}