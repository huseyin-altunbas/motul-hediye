﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Web.Services;
using Goldb2cInfrastructure;

public partial class AdresBilgileri : System.Web.UI.Page
{
    SiparisIslemleriBLL bllSiparisIslemleri = new SiparisIslemleriBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    SepetIslemleriBLL bllSepetIslemleri = new SepetIslemleriBLL();
    Kullanici kullanici;
    AkaryakitIslemleriBLL bllAkaryakitIslemleri = new AkaryakitIslemleriBLL();
    private static object lockObject = new Object();

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)Session["Kullanici"];

            if (kullanici.UyelikTipi == Convert.ToInt32(Enums.UyelikTipi.Kullanici))
            {
                if (Session["SiparisMasterId"] != null)
                {
                    //GoldService goldService = new GoldService();
                    //TGOLDUserInfo tGoldUserInfo = goldService.UserLogin(Convert.ToInt32(Session["CRMCustomerGUID"]));

                    //if (tGoldUserInfo != null)
                    //{
                    //    Session["totalGoldPoint"] = tGoldUserInfo.Puan;
                    //    Session["CRMCustomerGUID"] = tGoldUserInfo.ID;
                    //}

                    rptAdresler.DataSource = bllUyelikIslemleri.CariAdresListele(kullanici.ErpCariId, 0);
                    rptAdresler.DataBind();
                    if (!IsPostBack)
                    {
                        dpSehir.DataSource = bllUyelikIslemleri.SehirListesi("TÜRKİYE");
                        dpSehir.DataValueField = "SehirAd";
                        dpSehir.DataTextField = "SehirAd";
                        dpSehir.DataBind();
                        dpSehir.Items.Insert(0, new ListItem("Seçiniz", ""));
                        dpIlce.Items.Insert(0, new ListItem("Seçiniz", ""));
                    }
                }
                else
                    Response.Redirect("~/Sepet.aspx");
            }
            else
                Response.Redirect("~/Default.aspx");
        }
        else
            UyelikIslemleriBLL.MusteriGirisineYonlendir(Request.Url.PathAndQuery);

    }

    protected void rptAdresler_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string Telefonlar = "Telefon : ";
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("lblAdresKod")).Text = ((DataRowView)e.Item.DataItem).Row["AdresKod"].ToString();
            ((Label)e.Item.FindControl("lblAdres")).Text = ((DataRowView)e.Item.DataItem).Row["AdresTanim"].ToString() + " " + ((DataRowView)e.Item.DataItem).Row["IlceKod"].ToString() + "/" + ((DataRowView)e.Item.DataItem).Row["SehirKod"].ToString();
            Telefonlar = Telefonlar + ((DataRowView)e.Item.DataItem).Row["MobilTelefon1"].ToString();
            if (!string.IsNullOrEmpty(((DataRowView)e.Item.DataItem).Row["SabitTelefon1"].ToString()))
                Telefonlar = Telefonlar + " - " + ((DataRowView)e.Item.DataItem).Row["SabitTelefon1"].ToString();
            ((Label)e.Item.FindControl("lblTelefon")).Text = Telefonlar;

            LinkButton btnSil = ((LinkButton)e.Item.FindControl("btnSil"));

            btnSil.Attributes.Add("KayitId", ((DataRowView)e.Item.DataItem).Row["KayitId"].ToString());
            btnSil.Attributes.Add("name", btnSil.UniqueID);

        }

    }

    protected void btnSil_Click(object sender, EventArgs e)
    {
        int KayitId;
        if (int.TryParse(((LinkButton)(sender)).Attributes["KayitId"], out KayitId))
        {
            bllUyelikIslemleri.CariAdresSil(kullanici.ErpCariId, KayitId);
            AdresleriGetir();
        }
    }

    private void AdresleriGetir()
    {
        rptAdresler.DataSource = bllUyelikIslemleri.CariAdresListele(kullanici.ErpCariId, 0);
        rptAdresler.DataBind();
    }

    protected void btnGonder_Click(object sender, EventArgs e)
    {
        DataTable dt = bllUyelikIslemleri.CariAdresListele(kullanici.ErpCariId, 0);
        if (dt.Rows.Count < 1)
        {
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Lütfen adres ekleyiniz.')</script>");
            return;
        }

        lock (lockObject)
        {
            int AkaryakitUrunuVarmi = 0;
            int TeslimatAdresId = 0;
            int FaturaAdresId = 0;
            int TotalPoint = 0; TeslimatAdresId = Convert.ToInt32(Request["rbTeslimat"]);
            //FaturaAdresId = Convert.ToInt32(Request["rbFatura"]);
           
            if (TeslimatAdresId < 1)
            {
                ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Lütfen adres seçiniz.')</script>");
                return;
            }
            string _SiparisMasterId;
            if (Session["SiparisMasterId"] != null)
            {
                _SiparisMasterId = Session["SiparisMasterId"].ToString();
                string[] _SiparisMasterIdArr = _SiparisMasterId.Split(',');
                foreach (string item in _SiparisMasterIdArr)
                {
                    int SiparisMasterId = 0;
                    int.TryParse(item, out SiparisMasterId);
                    if (SiparisMasterId > 0)
                    {
                        SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
                        DataSet dsSiparisVerileriOncesi = bllSiparisGoruntuleme.SiparisGoruntule(SiparisMasterId, kullanici.ErpCariId);
                        DataTable dtSiparisAdresTTOncesi = dsSiparisVerileriOncesi.Tables["SiparisAdresTT"];

                        DataTable dtCheck = bllUyelikIslemleri.CariAdresListele(kullanici.ErpCariId, TeslimatAdresId);
                        string postKod = dtCheck.Rows[0]["PostaKod"].ToString();
                        postKod = postKod.Replace("_____", "");
                        if (postKod.Length != 5)
                        {
                            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Lütfen seçmiş olduğunuz adres posta kodunu güncelleyiniz.')</script>");
                            return;
                        }


                        if (dtSiparisAdresTTOncesi == null || dtSiparisAdresTTOncesi.Rows.Count == 0)
                        {
                            bllSiparisIslemleri.SiparisAdresKaydet(ref SiparisMasterId, TeslimatAdresId, TeslimatAdresId);
                            DataSet dsSiparisVerileri = bllSiparisGoruntuleme.SiparisGoruntule(SiparisMasterId, kullanici.ErpCariId);
                            DataTable dtSiparisDetayTT = dsSiparisVerileri.Tables["SiparisDetayTT"];


                            DataTable dtAkaryakitUrun = bllAkaryakitIslemleri.AkaryakitKartUrunListele();

                            var sonuc = from siparisDetay in dtSiparisDetayTT.AsEnumerable()
                                        join akaryakitUrun in dtAkaryakitUrun.AsEnumerable()
                                        on siparisDetay.Field<string>("urun_id").ToString() equals akaryakitUrun.Field<string>("UrunId").ToString()
                                        select siparisDetay.Field<string>("urun_id");

                            if (sonuc.Count() > 0)
                            {
                                AkaryakitUrunuVarmi++;
                            }

                        }
                    } 
                }

                if (AkaryakitUrunuVarmi > 0)
                {
                    Response.Redirect("~/AkaryakitMesaj.aspx");
                }
                else
                {
                    Response.Redirect("~/SiparisOnay.aspx");
                }
            }
        }
    }

    [WebMethod]
    public static List<Ilce> getIlceListesi(string SehirKod)
    {
        DataTable dtIlce = new DataTable();
        dtIlce = UyelikIslemleriBLL.IlceListesi(SehirKod);
        List<Ilce> ilceler = new List<Ilce>();
        Ilce ilce;

        ilce = new Ilce();
        ilce.IlceAd = "";
        ilce.SehirKod = "";
        ilceler.Add(ilce);

        foreach (DataRow dr in dtIlce.Rows)
        {
            ilce = new Ilce();
            ilce.IlceAd = dr["IlceAd"].ToString();
            ilce.SehirKod = dr["SehirKod"].ToString();
            ilceler.Add(ilce);
        }

        return ilceler;
    }

    [WebMethod]
    public static KullaniciAdres getAdresBilgileri(string AdresKod)
    {
        KullaniciAdres kullaniciAdres = new KullaniciAdres();
        Kullanici kullanici;
        if (HttpContext.Current.Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];

            UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
            DataTable dtAdres = new DataTable();
            dtAdres = bllUyelikIslemleri.CariAdresListele(kullanici.ErpCariId, Convert.ToInt32(AdresKod));
            if (dtAdres.Rows.Count > 0)
            {
                kullaniciAdres.AdresBaslik = dtAdres.Rows[0]["AdresKod"].ToString();
                kullaniciAdres.YazismaUnvan = dtAdres.Rows[0]["YazismaUnvan"].ToString();
                kullaniciAdres.AdresTanim = dtAdres.Rows[0]["AdresTanim"].ToString();
                kullaniciAdres.SehirKod = dtAdres.Rows[0]["SehirKod"].ToString();
                kullaniciAdres.IlceKod = dtAdres.Rows[0]["IlceKod"].ToString();
                string SabitTel = dtAdres.Rows[0]["SabitTelefon1"].ToString();
                if (!string.IsNullOrEmpty(SabitTel) && SabitTel.Length == 10)
                {
                    kullaniciAdres.SabitTelKod = SabitTel.Substring(0, 3);
                    kullaniciAdres.SabitTel = SabitTel.Substring(3, 7);
                }
                string CepTel = dtAdres.Rows[0]["MobilTelefon1"].ToString();
                if (!string.IsNullOrEmpty(CepTel) && CepTel.Length == 10)
                {
                    kullaniciAdres.CepTelKod = CepTel.Substring(0, 3);
                    kullaniciAdres.CepTel = CepTel.Substring(3, 7);
                }
                kullaniciAdres.PostaKod = dtAdres.Rows[0]["PostaKod"].ToString();
                kullaniciAdres.TcKimlikNo = dtAdres.Rows[0]["TckimlikNo"].ToString();
                kullaniciAdres.VergiDaire = dtAdres.Rows[0]["VergiDaire"].ToString();
                kullaniciAdres.VergiNo = dtAdres.Rows[0]["VergiNo"].ToString();
            }

        }

        return kullaniciAdres;
    }

    [WebMethod]
    public static void AdresKaydet(int CariAdresId, string AdresBaslik, string AdresTanim, string YazismaUnvan, string SehirKod, string IlceKod, string MobilTelefon, string SabitTelefon, string PostaKod, string TckimlikNo, string VergiDaire, string VergiNo)
    {
        UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
        Kullanici kullanici;
        if (HttpContext.Current.Session["Kullanici"] != null)
        {
            if (!string.IsNullOrEmpty(AdresBaslik) && !string.IsNullOrEmpty(AdresTanim) && !string.IsNullOrEmpty(YazismaUnvan) && !string.IsNullOrEmpty(SehirKod) && !string.IsNullOrEmpty(IlceKod) && !string.IsNullOrEmpty(MobilTelefon))
            {
                kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];

                bllUyelikIslemleri.CariAdresKaydet(kullanici.ErpCariId, CariAdresId, AdresBaslik, AdresTanim, YazismaUnvan, "TR", SehirKod, IlceKod, MobilTelefon, SabitTelefon, PostaKod, TckimlikNo, VergiDaire, VergiNo);
            }
        }
    }

}