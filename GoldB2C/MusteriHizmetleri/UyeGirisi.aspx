﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="UyeGirisi.aspx.cs" Inherits="MusteriHizmetleri_UyeGirisi" %>

<%@ MasterType VirtualPath="~/MusteriHizmetleri/MpMusteriHizmetleri.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Üye Girişi</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
    <span class="page-heading-title2">ÜYE GİRİŞİ</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <script src="/assets/js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/assets/lib/fancyBox/jquery.fancybox.css" />
    <script type="text/javascript" src="/assets/lib/fancyBox/jquery.fancybox.js"></script>
    <!-- popup -->
    <script type="text/javascript">
        function Popup(message) {
            if ($('#popup').length > 0 && $('#popupContent').length > 0) {
                $("#popupContent").html(message).promise().done(function () {
                    $('#popup').fancybox().trigger('click');
                });
            }
        }
    </script>
    <a id="popup" class="fancybox" href="#popupContent" style="display: none"></a>
    <div id="popupContent" style="display: none">
    </div>
    <!-- popup -->
    <div class="page-content">
        <div class="row">
            <div class="col-sm-6">
                <div class="box-authentication">
                    <h3>
                        ÜYE GİRİŞİ</h3>
                    <label for="emmail_login">
                        E-posta Adresi</label>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    <label class="fright notification">
                        <asp:RequiredFieldValidator ID="rqtxtEmail" runat="server" ControlToValidate="txtEmail"
                            ErrorMessage="E-Posta adresinizi girin." ValidationGroup="grpUyeGiris"></asp:RequiredFieldValidator>
                    </label>
                    <label for="password_login">
                        Şifre</label>
                    <asp:TextBox ID="txtSifre" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                    <label class="fright notification">
                        <asp:RequiredFieldValidator ID="rqtxtSifre" runat="server" ControlToValidate="txtSifre"
                            ErrorMessage="Şifrenizi girin." ValidationGroup="grpUyeGiris"></asp:RequiredFieldValidator>
                    </label>
                    <ul class="check-box-list" style="margin: 5px 0px 10px 0px;">
                        <li>
                            <asp:CheckBox ID="chkBeniHatirla" runat="server" />
                            <label for="cphIcerikMetin_chkBeniHatirla">
                                <span class="button" style="margin-top: 0px !important;"></span>Beni Hatırla
                            </label>
                        </li>
                    </ul>
                    <asp:LinkButton ID="lnbGiris" runat="server" OnClick="lnbGiris_Click" CssClass="btn btnInput"
                        ValidationGroup="grpUyeGiris"><i class="fa fa-lock"></i>&nbsp;Giriş Yap</asp:LinkButton>
                    <p class="forgot-pass">
                        <a href="/MusteriHizmetleri/SifremiUnuttum.aspx">Şifremi Unuttum</a></p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box-authentication">
                    <div style="text-align: center;">
                        <h3 style="margin-bottom: 50px;">
                            Sizi de <span class="colorF36">Motul ailesi</span> üyeleri arasında görmekten
                            mutluluk duyacağız.</h3>
                        <a href="/MusteriHizmetleri/UyeKayit.aspx" class="btn btnInput"><i class="fa fa-user">
                        </i>&nbsp;Ücretsiz Kayıt Ol</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
