﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="SatinAlmaSSS.aspx.cs" Inherits="MusteriHizmetleri_SatinAlmaSSS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Sıkça Sorulan Sorular
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <h3>
        1. Anadoludanaspara.com’a Üyelik Ücretli mi ?
    </h3>
    Sitemizde üyelik ücreti talep edilmemektedir. Sitemize ücretsiz üye olarak, ilgi
    alanınızdaki ürünler ile ilgili yeniliklerden ve kampanyalardan haberdar edilirsiniz,
    ürünler hakkında yorum ve tavsiyelerde bulunabilirsiniz.
    <br />
    <br />
    <h3>
        2. Motul Hediye Şifremi Unuttum . Nasıl Giriş Yapabilirim ?</h3>
    Sitemiz ana sayfasın da sağ üst kısmındaki üye girişi linkini tıklayarak ve e-posta
    adresinizi girerek alttaki şifremi unuttum butonunu tıkladığınızda şifreniz e-posta
    adresinize otomatik olarak gönderilecektir.
    <br />
    <br />
    <h3>
        3. Motul Hediye’den nasıl alışveriş yapabilirim ?</h3>
    Öncelikle sitemize üye olmanız gerekmektedir. Üyeliğinizi ana sayfamızdaki “Üye
    Olmak İstiyorum” butonunu kullanarak yapabilirsiniz.
    <br />
    <br />
    Üyelik bilgilerinizi siteye girdikten sonra ürününüzü sepetinize eklemelisiniz.
    Sepetinizden Satın Al butonu ile bir sonraki ekrana geçebilir, Teslimat ve Fatura
    adresi bilgilerinizi doldurduktan sonra sepeti onaylama işlemine geçebilirsiniz.
    <br />
    <br />
    Ödeme ekranında kredi kartı bilgilerinizi doğru şekilde ekrana girmeyi unutmayınız.
    Kredi kartı ile ödemek istemezseniz kredi kartı seçeneğinin alt tarafındaki Havale
    veya Posta çeki ödeme seçeneklerini kullanabilirsiniz . Hesap numarası bilgilerini
    not etmeyi unutmayınız . Yada Onay ekranını yazdırabilirsiniz.
    <br />
    <br />
    <h3>
        4. Sepetime eklediğim ürünlerin sayısını nasıl arttırabilirim ?
    </h3>
    Sepete eklediğiniz ürünlerin sayısını Sepetim sayfasında ürün adetini yazan kutudan
    artırabilir ya da eksiltebilirsiniz.
    <br />
    <br />
    <h3>
        5. Sepetimden ürün çıkarmak istiyorum ne yapmalıyım ?
    </h3>
    Sepete eklediğiniz ürünlerin sayısını Sepetim sayfasında ürün adetini yazan kutudan
    eksiltebilirsiniz. Ya da Ürün adının solundaki kutuyu işaretleyip sayfanın sol altındaki
    “Sil” linkini tıklayarak seçtiğiniz ürünü sepetten silebilirsiniz.
    <br />
    <br />
    <h3>
        6. motulteam.com’da satılan ürünlerin garantisi var mı ?</h3>
    Sattığımız tüm ürünler ithalatçı veya imalatçı firma garantisi altında olup tamir
    veya değişim işlemleri garanti veren firma ve yetkili servisleri tarafından yapılmaktadır.
    <br />
    <br />
    Arızalı ürününüzü faturası ve/veya garanti belgesi ile birlikte, garanti belgesinden
    ya da kullanım kılavuzundan bulacağınız size en yakın yetkili servise başvurmak
    sureti ile garanti kapsamında tamir veya değişim yaptırabilirsiniz. Yetkili Servis
    ile problem yaşadığınızda bize başvurabilirsiniz.
    <br />
    <br />
    Arızalı ürünlerinizi doğrudan size en yakın yetkili servise göndermeniz daha hızlı
    sonuç almanızı sağlar. Bünyemizde onarım merkezi hiçbir şekilde bulunmadığından
    ürünlere müdahalemiz söz konusu değildir. Bu sebeple, motulteam.com’a gönderdiğiniz
    ürünler tarafımızdan da yetkili servislere gönderilmekte ve sonuç beklenmektedir.
    <br />
    <br />
    Bazı yetkili servisler kargo ile de ürün kabul edebilmekte yada doğrudan sizden
    ürünü aldırtabilmektedirler. Lütfen bu yönde hizmetleri olup olmadığını sorunuz.
    <br />
    <br />
    Seri numarası olan ürünlerde yetkili servisler ürünün seri numarasından garanti
    kapsamını kolaylıkla bulabilmekte ve onarım işlemlerine başlayabilmektedir.
</asp:Content>
