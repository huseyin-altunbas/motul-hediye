﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="SifremiUnuttum.aspx.cs" Inherits="MusteriHizmetleri_SifremiUnuttum" %>

<%@ MasterType VirtualPath="~/MusteriHizmetleri/MpMusteriHizmetleri.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Şifremi Unuttum</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
    <span class="page-heading-title2">ŞİFREMİ UNUTTUM</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <script src="/assets/js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/assets/lib/fancyBox/jquery.fancybox.css" />
    <script type="text/javascript" src="/assets/lib/fancyBox/jquery.fancybox.js"></script>
    <!-- popup -->
    <script type="text/javascript">
        function Popup(message) {
            if ($('#popup').length > 0 && $('#popupContent').length > 0) {
                $("#popupContent").html(message).promise().done(function () {
                    $('#popup').fancybox().trigger('click');
                });
            }
        }
    </script>
    <a id="popup" class="fancybox" href="#popupContent" style="display: none"></a>
    <div id="popupContent" style="display: none">
    </div>
    <!-- popup -->
    <div class="page-content">
        <div class="row">
            <div class="col-sm-6">
                <div class="box-form">
                    <div class="mt10">
                        <label for="emmail_login">
                            E-posta Adresi</label>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="E-Posta adresinizi yazın."
                                ControlToValidate="txtEmail" ValidationGroup="SifreGonder"></asp:RequiredFieldValidator>
                        </label>
                        <label class="fleft notification">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Lütfen doğru E-Posta adresinizi yazın."
                                ControlToValidate="txtEmail" ValidationGroup="SifreGonder" ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z\-])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"></asp:RegularExpressionValidator>
                        </label>
                    </div>
                    <div class="mt10">
                        <label for="emmail_login_repeat">
                            E-posta Adresi(Tekrar)</label>
                        <asp:TextBox ID="txtEmailTekrar" runat="server" CssClass="form-control"></asp:TextBox>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="E-Posta adresinizi tekrar yazın."
                                ControlToValidate="txtEmailTekrar" ValidationGroup="SifreGonder"></asp:RequiredFieldValidator>
                        </label>
                        <label class="fleft notification">
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="E-Posta adresleriniz eşleşmiyor."
                                ControlToCompare="txtEmail" ControlToValidate="txtEmailTekrar" ValidationGroup="SifreGonder"></asp:CompareValidator>
                        </label>
                    </div>
                    <div class="mt10">
                        <asp:LinkButton ID="btnSifremiGonder" runat="server" OnClick="btnSifremiGonder_Click"
                            CssClass="btn btnInput" ValidationGroup="SifreGonder"><i class="fa fa-lock"></i>&nbsp;Şifremi Gönder</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box-form">
                    <div style="text-align: center;">
                        <h3 style="margin-bottom: 50px;">
                            <span class="colorF36">Motul </span>'a üye olduysanız ve şifrenizi hatırlamıyorsanız;<br />
                            <br />
                            kayıtlı olan e-posta adresinizi ilgili alanlara yazarak <span class="colorF36">"Şifremi
                                Gönder"</span> tuşuna basın.
                            <br />
                            <br />
                            Şifrenizi e-posta adresinize hemen göndereceğiz.</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
