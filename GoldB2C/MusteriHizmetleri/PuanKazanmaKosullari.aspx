﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master" AutoEventWireup="true" CodeFile="PuanKazanmaKosullari.aspx.cs" Inherits="PuanKazanmaKosullari" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" Runat="Server">
     <span class="page-heading-title2">Puan Kazanma Koşulları</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" Runat="Server">

    <br />
   <div>
     •	Uygulamaya 310000-350000 kod arasında KSM ile çalışmayan acentelerimiz dahildir. <br />
•	Puan yüklemeleri aylık yapılacaktır.<br />
•	Acentelerimiz ilgili döneme ait üretim büyüklüğü ve acente karnesi puanlarına bağlı olarak  aşağıda yer alan tabloya göre puan kazanacaktır.<br />

   </div>
   <br />

    <table class="table table-bordered table-responsive cart_summary" border="0" cellpadding="0" cellspacing="0" style="mso-cellspacing: 0cm; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 0cm 0cm 0cm; font-size: 10.0pt; font-family: 'Times New Roman', serif;">
        <tr>
            <td>
                <p class="MsoNormal">
                    <span>&nbsp;<o:p></o:p></span></p>
            </td>
            <td colspan="9">
                <p class="MsoNormal">
                    <span>İlgili Ay Karne Puanı ​ ​ ​ ​ ​ ​ ​ ​<o:p></o:p></span></p>
            </td>
        </tr>
        <tr>
            <td>
                <p class="MsoNormal">
                    <span>İlgili Ay Üretim Miktarı<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>A+<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>A<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>B+<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>B<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>C+<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>C<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>D<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>E<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>F<o:p></o:p></span></p>
            </td>
        </tr>
        <tr>
            <td>
                <p class="MsoNormal">
                    <span>600.000 TL ve üzeri<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>2.250<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.750<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.500<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.250<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.000<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>750<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
        </tr>
        <tr>
            <td>
                <p class="MsoNormal">
                    <span>300.000 TL ve üzeri<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.750<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.500<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.250<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.000<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>750<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>500<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
        </tr>
        <tr>
            <td>
                <p class="MsoNormal">
                    <span>150.000 TL ve üzeri<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.500<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.250<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.000<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>750<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>500<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>400<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
        </tr>
        <tr>
            <td>
                <p class="MsoNormal">
                    <span>75.000 TL ve üzeri<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.250<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.000<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>750<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>500<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>400<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
        </tr>
        <tr>
            <td>
                <p class="MsoNormal">
                    <span>25.000 TL ve üzeri<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>1.000<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>750<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>500<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>400<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
        </tr>
        <tr>
            <td>
                <p class="MsoNormal">
                    <span>25.000 TL altı<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
            <td>
                <p class="MsoNormal">
                    <span>0<o:p></o:p></span></p>
            </td>
        </tr>
    </table>


     <br />
   <div>
•	Bağlı bulunduğu bölge müdürlüğünde en yüksek karne puanına sahip ilk 3 acentemiz kazanacağı ödüle ek olarak, sırasıyla 1.000 ASpara, 750 ASPara, 500 ASPara ile ödüllendirilecektir.(Karne puanlarının eşit olması durumunda toplam üretim miktarı dikkate alınacaktır)  
   </div>
   <br />

</asp:Content>

