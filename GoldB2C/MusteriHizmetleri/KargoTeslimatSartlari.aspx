﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="KargoTeslimatSartlari.aspx.cs" Inherits="MusteriHizmetleri_KargoTeslimatSartlari" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Kargo Teslimat
        Şartları</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
    <span class="page-heading-title2">KARGO TESLİMAT ŞARTLARI</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <div class="mt10">
        

<p><strong>Değerli &Uuml;yemiz,</strong><br /><br /></p>
<p>&nbsp;</p>
<p><br />&mdash; Hediye &uuml;r&uuml;n teslimatlarında; kargo teslim fişini imzalamadan &ouml;nce &uuml;r&uuml;n&uuml;n kargo kolisinde (&uuml;r&uuml;n&uuml;n orijinal kolisinin i&ccedil;ine konduğu dış koli) herhangi bir hasar olup olmadığını kontrol ediniz. <strong>Kargo kolisi hasarlı olan hediyelerinizi teslim almayıp</strong>, kargo g&ouml;revlisine iade etmenizi rica ederiz.</p>
<p><br />&mdash; Beyaz Eşya, TV gibi kurulum gerektiren &uuml;r&uuml;n teslimatlarında; Lojistik firması tarafından d&uuml;zenlenen teslim fişini imzalamadan &ouml;nce &uuml;r&uuml;n&uuml;n dış ambalajında ve kolisinde (&uuml;r&uuml;n&uuml;n orijinal kolisinin i&ccedil;ine konduğu dış koli) herhangi bir hasar olup olmadığını lojistik firma yetkilileriyle birlikte kontrol ediniz. <strong>Dış ambalajında hasarlı olan hediyelerinizi teslim almayıp</strong>, lojistik firma g&ouml;revlisine iade etmenizi rica ederiz.<br /><br />&mdash; Beyaz Eşya, TV gibi kurulum gerektiren &uuml;r&uuml;nlerin paketinin Yetkili Servis tarafından a&ccedil;ılarak kurulumun Yetkili Servis tarafından yapılması gerekmektedir. <strong>Yetkili Servis tarafından a&ccedil;ılmayan ve kurulumu yapılmayan &uuml;r&uuml;nler garanti kapsamı dışında işlem g&ouml;receğinden iadesi ve/veya değişimi kabul edilmemektedir.</strong> Bu konuda detaylı bilgiyi ilgili &uuml;r&uuml;n&uuml;n &uuml;reticisinden alabilirsiniz.<br /><br />&mdash; Şayet &uuml;r&uuml;n&uuml; teslim almak istiyorsanız, lojistik g&ouml;revlisinin yanında &uuml;r&uuml;n&uuml;n sağlamlığını kontrol etmeniz gerekmektedir. Kontrol sonrasında kolide herhangi bir hasarla karşılaşırsanız (ambalaj ezilmesi, yıpranma, yırtılma, ezilme vb.) mutlaka lojistik g&ouml;revlisinde bulunan <strong>hasar tespit tutanağının doldurulmasını talep ediniz</strong> ve &uuml;r&uuml;n&uuml; teslim almayınız.</p>
<p><br />&mdash; Teslim aldığınız kargonun hasarlı &ccedil;ıkması durumunda <strong>aynı g&uuml;n</strong> fotoğraflarını &ccedil;ekmenizi ve konuyu 1 iş g&uuml;n&uuml; i&ccedil;erisinde tarafımıza iletmenizi rica ederiz. 1 iş g&uuml;n&uuml; ge&ccedil;en hasarlı &uuml;r&uuml;nler i&ccedil;in maalesef işlem yapılamayacaktır.</p>
<p><br />&mdash; <strong>Yukarıda bildirilen y&ouml;nlendirmelerden hi&ccedil;birisini yapmadıysanız ne yazık ki &uuml;r&uuml;n&uuml;n&uuml;zle ilgili iade ve değişim yapılamayacak ve bu durumdan Motul ve Tutkal Bilişim Hizmetleri sorumlu tutulamayacaktır.</strong></p>
<p><br />&mdash; Beyaz Eşya gibi ağır &uuml;r&uuml;nlerin, kargo taşımacılığı <strong>apartman kapısına kadar</strong> yapılmaktadır. Kata teslim, ev veya iş yeri i&ccedil;erisine teslim yapılmamaktadır.</p>
<p><br />&mdash;&Uuml;r&uuml;n&uuml;n orijinal ambalajı i&ccedil;erisinde, &uuml;r&uuml;nle ilgili <strong>eksiklik veya hasar varsa teslim alındığı g&uuml;n</strong>&nbsp; hediye sitemizin &ldquo;İletişim&rdquo; sayfasında bulunan iletişim formunu doldurarak iletmenizi rica ederiz. Aksi halde &uuml;r&uuml;nle ilgili iade ve değişim yapılmamaktadır.</p>
<p><br />&mdash; &Uuml;r&uuml;n&uuml;n <strong>Garanti Belgesi'nin atılmaması</strong> gerekmektedir. Teslim aldığınız &uuml;r&uuml;nlerin &ccedil;alışmaması durumunda, &uuml;r&uuml;n kullanım kılavuzundaki talimatlara g&ouml;re hareket etmenizi rica ederiz. &Ccedil;alışmayan &uuml;r&uuml;nler i&ccedil;in ilgili &uuml;r&uuml;n&uuml;n Yetkili Servisi'ne başvuruda bulunabilirsiniz.<br /><br />&mdash; Hediyenin talep ettiğiniz hediyeden farklı &ccedil;ıkması durumunda kargoyu almış olduğunuz tarihten itibaren <strong>14 g&uuml;n i&ccedil;inde</strong> orijinal ambalajıyla ve iade sebebini belirterek aşağıdaki adrese g&ouml;ndermeniz gerekmektedir;<br /><br /><br /></p>
<p><strong style="color:red;">Not: Arızalı &ccedil;ıkan hediyeler i&ccedil;in l&uuml;tfen ilgili firmanın Yetkili Servisi'ni arayınız.</strong></p>
<p><strong><br />Tutkal Bilişim Hizmetleri A.Ş.<br />Adil Mah. Gazanfer Sok. No:6/A Sultanbeyli İstanbul<br /><br />&mdash; Değerli &Uuml;yemiz; Talep etmiş olduğunuz &uuml;r&uuml;n teknik servis kurulumu gerektiren bir ise ve &uuml;r&uuml;n&uuml;n&uuml;z&uuml;n kurulumunu ileriki bir tarihte yaptıracaksanız; &uuml;r&uuml;n&uuml; hasarsız şekilde teslim aldıktan sonra kurulum tarihine kadar &uuml;r&uuml;n&uuml; saklama koşullarından sorumlu olmaktasınız. Kurulum tarihinde &uuml;r&uuml;nde herhangi bir hasar v.b. sorun ile karşılaştığınızda &uuml;z&uuml;lerek, Motul ve Tutkal Bilişim&rsquo;in sorumlu olmayacağını belirtmek isteriz.<br /><br />Tutkal Bilişim Hizmetleri A.Ş.<br /><br /><br /></strong></p>
<p>&nbsp;</p>
    </div>
</asp:Content>
