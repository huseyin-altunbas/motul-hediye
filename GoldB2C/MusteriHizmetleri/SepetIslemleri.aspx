﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="SepetIslemleri.aspx.cs" Inherits="MusteriHizmetleri_SepetIslemleri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Sepet İşlemleri
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    Satın almak istediniz tüm ürünleri sepetinize ekleyebilirsiniz. Alışveriş yapabilmeniz
    için satın almak istediğiniz ürünü sepete eklemeniz gerekmektedir. Ürün inceleme
    sayfasında sağ alt köşede bulunan “sepete ekle” butonuna basarak istediğiniz ürünü
    sepete ekleyebilirsiniz. Sepete eklediğiniz ürünlerin adetlerini değiştirebilir
    ya da seçtiğiniz ürünü sepetinizden silebilirsiniz.
    <br />
    <br />
    Alışveriş Sepeti içersinde ürünlerin fiyatlarını KDV dahil TL cinsinden görebilirsiniz.
    <br />
    <br />
    Alışveriş sepetinizin içindeki aynı kategorideki ürünleri incelemek ve karşılaştırmak
    isterseniz, karşılaştırma yapmak istediğiniz ürünleri seçip “Karşılaştırma ” butonuna
    basmanız yeterlidir.
    <br />
    <br />
    Alışveriş sepetinizdeki ürünleri inceledikten sonra alışverişe kaldığınız yerden
    devam etmek için ‘Alışverişe Devam Et’ butonuna basmanız yeterli olacaktır.
    <br />
    <br />
    Alışveriş sepeti ekranından Satın alma aşamasına geçmek için ‘Satın al’ butonuna
    basmanız yeterlidir.
</asp:Content>
