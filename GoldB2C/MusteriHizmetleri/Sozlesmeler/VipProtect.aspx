﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master" AutoEventWireup="true" CodeFile="VipProtect.aspx.cs" Inherits="MusteriHizmetleri_Sozlesmeler_VipProtect" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" Runat="Server">
    VIP Protect Sözleşmesi
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" Runat="Server">
    <p>
        <strong>&bull;Satın aldıgınız VIP Protect paketi, ilgili &uuml;r&uuml;nle aynı faturada
            olmalıdır.</strong></p>
    <p>
        <strong>&bull;Satın aldıgınız &uuml;r&uuml;n&uuml;n fiyatı, VIP Protect fiyat aralıgında
            olmalıdır.</strong></p>
    <p>
        <strong>&bull;Satın aldıgınız &uuml;r&uuml;n&uuml;n ve VIP Protect hizmetinin seri nolarının
            faturada yazdıgından emin olunuz.</strong></p>
    <p>
        YASAL GARANTİ KAPSAMINDAKİ &Uuml;R&Uuml;NLER : M&uuml;şteri hizmetleri personelinin
        arıza tespitinden itibaren, maksimum 1 saat i&ccedil;erisinde eğer &uuml;r&uuml;n
        arızalı ise değişim yapılır. &Uuml;r&uuml;nler; hizmet s&uuml;resi boyunca arıza
        halinde herhangi bir kesinti yapılmadan fatura bedeli &uuml;zerinden işlem g&ouml;recektir.
        Garanti kapsamında arıza halindeki değişimlerde, &uuml;r&uuml;n&uuml;n orijinal
        enerji bağlantısı ve satın alma anında yanında verilen hediye &uuml;r&uuml;nler
        getirilmek zorundadır. Aksi durumda g&uuml;ncel fiyatlar &uuml;zerinden, eksik olan
        &uuml;r&uuml;nler orijinal fiyatları &uuml;zerinden tahsil edilecektir. &Uuml;r&uuml;n&uuml;n
        yazılımsal arızaları, hizmet paketini kapsamamaktadır. &Uuml;r&uuml;n&uuml;n değişebilir
        aparatlarının ( pil, enerji bağlantıları, lens, kumanda, vs&hellip;) arızası halinde
        değişimi orijinal aparat ile yapılır. Orijinal aparatın olmadığı durumlarda ge&ccedil;ici
        bir aparat temin edilir, en kısa s&uuml;rede orijinal aparatıyla yenilenir. &Uuml;r&uuml;n&uuml;n
        kendisi değiştirilemez. Garanti kapsamındaki &uuml;r&uuml;n&uuml;n değişim halinde
        m&uuml;şteri arzu ederse, yeni bir paket satın alarak değişen &uuml;r&uuml;n i&ccedil;in
        1 yıl daha verilen hizmetten yararlanılabilir. Aparat değişimi halinde ise s&uuml;re
        , eski hizmet paketinin s&uuml;resi boyunca devam eder.</p>
    <p>
        YASAL GARANTİ KAPSAMI DIŞINDAKİ ARIZALAR : &Ccedil;arpma d&uuml;şme sonucu olan
        kırılmalar, sıvı temasında olan arızalar, kısa devre, y&uuml;ksek voltaj akımının
        etkisinden oluşan hasarlar ve cihazın konut i&ccedil;erisinden &ccedil;alınması
        garanti kapsamına alınmıştır.(Hırsızlık Sigortası hizmeti, motulteam.com tarafından
        sağlanmaktadır ve yalnızca emtia sahibine ait konut i&ccedil;erisinde oluşacak hırsızlıklar
        i&ccedil;in ge&ccedil;erlidir.) Bu kapsamdaki &uuml;r&uuml;nler i&ccedil;in &uuml;r&uuml;n
        ve ek par&ccedil;alarının (Adapt&ouml;r, Kablo ﬁarj Cihazı vb. ) tam olarak teslimi
        halinde , teslimi takiben 15 g&uuml;n i&ccedil;erisinde &uuml;cretsiz tamiri yapılır.
        &Uuml;r&uuml;n tamir edilemiyor veyahut tamir bedeli &uuml;r&uuml;n bedelinin %60&rsquo;ın
        ge&ccedil;iyor ise, tamir yapılmaz bu durumda aşağıda belirtilen muafiyet oranları
        uygulanarak &uuml;r&uuml;n muadili ile değiştirilir.</p>
    <p>
        365 G&Uuml;N MEMNUNİYET GARANTİSİ: S&ouml;zleşmeye konu garanti paketi ile aynı
        faturada yer alan &uuml;r&uuml;nlerden memnun kalmayan t&uuml;ketici, fatura tarihinden
        itibaren 365 g&uuml;n i&ccedil;erisinde olmak &uuml;zere, aşağıdaki muafiyet oranları
        uygulanarak, faturasında mevcut &uuml;r&uuml;n ile aynı mahiyette bir kereye mahsus
        muadil &uuml;r&uuml;n değişimi yapabilir.</p>
    <p>
        PAKET KULLANIM ŞARTLARI : S&ouml;zleşmeye konu işlemler sadece; Altunizade, Bah&ccedil;elievler,
        Levent, Kadık&ouml;y, Bursa ve Tekirdağ mağazalarında yapılabilir. Garanti hizmet
        paketinin ge&ccedil;erli olabilmesi i&ccedil;in, hizmeti i&ccedil;eren &uuml;r&uuml;nle
        işbu garanti paketi ve bedeli aynı faturada yer almak zorundadır. Aksi durumda ge&ccedil;erliliği
        yoktur. &Uuml;r&uuml;nlerin, her koşuda değişimi yine aynı &uuml;r&uuml;n grubu
        ile yapılabilir. Farklı bir &uuml;r&uuml;n talep edilemez. Hizmet paketi bir kez
        kullanım (BİR &Uuml;R&Uuml;N) i&ccedil;in ge&ccedil;erlidir. Bir kez kullanıldıktan
        sonra faturada yer alsalar dahi diğer &uuml;r&uuml;n grupları i&ccedil;in kullanılamaz.
        365 g&uuml;n memnuniyet garantisi, veya yasal garanti kapsamı dışında arıza ( tam
        ziyan veya tamir yapılamaz ) durumlarında beher elektronik cihazın &ouml;denebilir
        tazminat tutarı &uuml;zerinden fatura tarihinden itibaren 0-6 ay arası kullanım
        % 30 6-12 ay arası kullanım %40 muafiyet uygulanacaktır. / Garanti paketini ancak
        hizmeti satın alan kişi kullanabilir. Başka bir kişi hizmeti satın alan ile arasındaki
        ilişkiyi resmi kurum kararıyla a&ccedil;ıklayamadığı s&uuml;rece yararlanamaz. motulteam.com
        tarafından T&uuml;keticiye hi&ccedil;bir şekilde ve hi&ccedil;bir garanti paketinde
        nakit &ouml;deme yapılmayacaktır. T&uuml;ketici işbu s&ouml;zleşmeyle ancak Muadil
        &uuml;r&uuml;n talebinde bulunabilir. Satın alınan t&uuml;m VIP PROTECT paketleri
        fatura tarihinden itibaren 1 yıl s&uuml;resince kullanılabilir .</p>
</asp:Content>

