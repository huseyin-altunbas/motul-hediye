﻿using Goldb2cEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MusteriHizmetleri_MpMusteriHizmetleri : System.Web.UI.MasterPage
{
    private string NewRemarketingCode;

    public string newRemarketingCode
    {
        get { return NewRemarketingCode; }
        set { NewRemarketingCode = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Kullanici"] == null)
        {
            Response.Redirect("/Login.aspx");
        }

        Kullanici kulanici = (Kullanici)Session["Kullanici"];
        string fullUrl = Request.Url.ToString();
        if (!kulanici.IlkSifreDegistimi)
        {
            if (!fullUrl.Contains("/Hesabim/SifremiDegistir.aspx"))
            { Response.Redirect("/Hesabim/SifremiDegistir.aspx"); }

        }
    }
}
