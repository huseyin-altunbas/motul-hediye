﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using Goldb2cEntity;
using System.Text;
using Goldb2cInfrastructure;
using System.IO;
using System.Net;
using System.Net.Mail;

public partial class MusteriHizmetleri_Iletisim : System.Web.UI.Page
{
    IletisimIslemleriBLL bllIletisimIslemleri = new IletisimIslemleriBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Redirect("/");
    }

    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {
            string ad = InputSafety.SecureString(txtAd.Text.Trim());
            string soyad = InputSafety.SecureString(txtSoyad.Text.Trim());
            string email = InputSafety.SecureString(txtEmail.Text.Trim());
            string cepTelKod = InputSafety.SecureString(txtCepTelKod.Text.Trim());
            string cepTelNo = InputSafety.SecureString(txtCepTel.Text.Trim());
            string soru = InputSafety.SecureString(txtSoru.Text.Trim());


            if (!string.IsNullOrEmpty(ad) && !string.IsNullOrEmpty(soyad) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(cepTelKod) && !string.IsNullOrEmpty(cepTelNo) && !string.IsNullOrEmpty(soru))
            {
                int sonuc = bllIletisimIslemleri.IletisimFormuKaydet(ad, soyad, email, cepTelKod, cepTelNo, string.Empty, soru);

                if (sonuc > 0)
                {
                    IletisimMailGonder(sonuc, ad, soyad, email, cepTelKod, cepTelNo, string.Empty, soru);
                    txtAd.Text = string.Empty;
                    txtSoyad.Text = string.Empty;
                    txtEmail.Text = string.Empty;
                    txtCepTelKod.Text = string.Empty;
                    txtCepTel.Text = string.Empty;
                    txtSoru.Text = string.Empty;

                    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("<br/>Öneri veya Sorunuz kaydedildi.<br/>En kısa zamanda sizinle iletişime geçilecektir."));
                }
                else
                    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("<br/>Hata oluştu daha sonra tekrar deneyiniz."));
            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("<br/>Lütfen ilgili alanları kontrol ediniz."));
            }
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("<br/>Öneri veya sorunuzun gönderilmesi için lütfen üye girişi yapınız."));
        }
    }


    private bool YandexMailGonder1(string konu, string icerik)
    {
        try
        {
            string to = "huseyin.altunbas@tutkal.com.tr,burak.senkul@tutkal.com.tr,tutkaloperasyon@tutkal.com.tr"; //To address    
            string from = "altunbas.huseyin@gmail.com"; //From address    
            MailMessage message = new MailMessage(from, to);

            string mailbody = icerik;
            message.Subject = konu;
            message.Body = mailbody;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587); //Gmail smtp    
            System.Net.NetworkCredential basicCredential1 = new
            System.Net.NetworkCredential("altunbas.huseyin@gmail.com", "Web+webmercek");
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = basicCredential1;
            client.Send(message);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public bool IletisimMailGonder(int id, string ad, string soyad, string email, string cepTelKod, string cepTelNo, string kartNo, string soru)
    {
        Kullanici kullanici = (Kullanici)Session["Kullanici"];
        try
        {
            string strHTML = File.ReadAllText(Server.MapPath("~/MailSablon/IletisimFormu.htm"));

            strHTML = strHTML.Replace("#acentekodu#", kullanici.CRMCustomerGUID.ToString());
            strHTML = strHTML.Replace("#cepTel#", cepTelKod + cepTelNo);
            strHTML = strHTML.Replace("#acenteadi#", kullanici.KullaniciAd.ToString());
            strHTML = strHTML.Replace("#ad#", ad);
            strHTML = strHTML.Replace("#soyad#", soyad);
            strHTML = strHTML.Replace("#email#", email);
            strHTML = strHTML.Replace("#kartNo#", kartNo);
            strHTML = strHTML.Replace("#soru#", soru);

            SendEmail mail = new SendEmail();
            mail.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
            mail.MailTo = new string[] { "motultr@yandex.com" };
            mail.MailCc = new string[] { "baris.ersan@tutkal.com.tr", "huseyin.altunbas@tutkal.com.tr", "burak.senkul@tutkal.com.tr", "tutkaloperasyon@tutkal.com.tr" };


            //mail.MailCc = new string[] { "pazarlama@biges.com" };
            //01.12.2017
            //mail.MailTo = new string[] { "fulya.akcora@tutkal.com.tr", "cansin.ateser@tutkal.com.tr", "bilge.aslan@tutkal.com.tr", "baris.ersan@tutkal.com.tr", "huseyin.altunbas@tutkal.com.tr" };
            //mail.MailCc = new string[] { "pazarlama@biges.com" };
            //01.12.2017

           
             
            mail.MailHtml = true;
            mail.MailSubject = "Motul Hediye" + "-" + id.ToString() + " " + "Motul Hediye iletişim formu email gönderimi.";

            mail.MailBody = strHTML;
            bool mailSent = mail.SendMail(false);

            if (mailSent)
                return true;
            else
                return false;
        

            //return YandexMailGonder("Motul Hediye" + "-" + id.ToString() + " " + "Motul Hediye iletişim formu email gönderimi.", strHTML);
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">Popup('" + mesaj + "');</script>";
    }
}