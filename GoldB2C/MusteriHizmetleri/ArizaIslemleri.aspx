﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="ArizaIslemleri.aspx.cs" Inherits="MusteriHizmetleri_ArizaIslemleri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Arıza İşlemleri
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    Sattığımız tüm ürünler ithalatçı veya imalatçı firma garantisi altındadır. Tamir
    veya değişim işlemleri garanti veren firma tarafından yapılmaktadır. Arızalı ürününüzü
    faturası veya garanti belgesi ile birlikte, garanti belgesinden ya da kullanım kılavuzundan
    bulacağınız size en yakın yetkili servisi arayarak, garanti kapsamında tamir veya
    değişim yaptırabilirsiniz. Motul Hediye’ye gönderdiğiniz ürünler de tarafımızdan
    yetkili servislere gönderilmekte ve sonuç beklenmektedir.
    <br />
    <br />
    Yetkili servis bilgisine ürün garanti belgesinden, kullanım kılavuzundan ulaşabilirsiniz.
    Yetkili Servis ile problem yaşadığınızda bize başvurabilirsiniz.
    <br />
    <br />
    Arızalı ürünlerinizi doğrudan size en yakın yetkili servise göndermeniz daha hızlı
    sonuç almanızı sağlar (ort. 4 iş günü/ 1 hafta kazanırsınız). Bünyemizde onarım
    merkezi bulunmadığından ürünlere müdahalemiz söz konusu değildir. Bu sebeple, Motul Hediye
    ’ye gönderdiğiniz ürünler tarafımızdan da yetkili servislere gönderilmekte ve sonuç
    beklenmektedir.
    <br />
    <br />
    Tarafımıza sadece bilgisayar donanım ürünlerini (hard disk, modem,ana kart,vb..)
    gönderebilirsiniz. Bu ürünlerin distribütörleri nihai tüketici ile birebir çalışmadıklarından
    bu gereklidir.
</asp:Content>
