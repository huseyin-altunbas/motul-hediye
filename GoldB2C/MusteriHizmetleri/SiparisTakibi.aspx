﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="SiparisTakibi.aspx.cs" Inherits="MusteriHizmetleri_SiparisTakibi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Sipariş Takibi
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    Sipariş takibi bölümüne ana sayfa da üstte sağ tarafta “Sipariş Takibi” linkini
    tıklayarak yada müşteri hizmetleri linkinin altından ulaşabilirsiniz.<br />
    Sipariş takibi bölümüne bugüne kadar vermiş olduğunuz siparişlerinizin ayrıntılı
    dökümü gelecektir.
</asp:Content>
