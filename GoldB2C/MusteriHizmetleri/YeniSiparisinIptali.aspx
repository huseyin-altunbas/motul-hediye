﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="YeniSiparisinIptali.aspx.cs" Inherits="MusteriHizmetleri_YeniSiparisinIptali" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Yeni Siparişin İptali
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    motulteam.com’da yeni verdiğiniz siparişini iptal etmek istediğinizde “Sipariş Takibi”
    bölümünden siparişlerinize bakınız. Sipariş listesi ekranda listelenecektir. Siparişlerinizi
    iptal etmek istediğiniz zaman siparişinize atanmış olan satış temsilcisine ulaşarak
    iptalini gerçekleştirebilirsiniz. Ürünleriniz kargoya verildiği zaman sipariş takip
    ekranınıza kargo numaranız eklenmektedir. Kargo takip numaraları takip eden iş günü
    içinde eklenmektedir.
</asp:Content>
