﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master" AutoEventWireup="true" CodeFile="sss.aspx.cs" Inherits="sss" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" Runat="Server"> <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Sık Sorulan Sorular</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" Runat="Server">

    <br />
    <br />
    <p><strong>HEDİYE SİPARİŞ ETME&nbsp;</strong><br /><br /><strong>1-&nbsp;</strong>&nbsp;<u>www.motulteam.com</u><strong>&nbsp;sitesinden Hediye Siparişi vermek istiyorum. Nasıl sipariş verebilirim?</strong></p>
<p>&nbsp;</p>
<p><u>www.motulteam.com</u>&nbsp;adresinden GSM numaranız ve şifreniz ile giriş yaptıktan sonra ana sayfa &uuml;zerinde sol tarafta bulunan kategoriler men&uuml;s&uuml;nden beğendiğiniz / talep ettiğiniz hediyenin &uuml;zerine tıklayarak &uuml;r&uuml;n detay sayfasına ulaşabilirsiniz. A&ccedil;ılan sayfada &uuml;r&uuml;n detayı g&ouml;r&uuml;nt&uuml;lenmektedir. &Uuml;r&uuml;n g&ouml;rselini b&uuml;y&uuml;tmek i&ccedil;in &uuml;r&uuml;n resminin &uuml;zerine tıklamanız yeterlidir.</p>
<p>&nbsp;</p>
<p>1.Adım: &Uuml;r&uuml;n detay sayfasında &uuml;r&uuml;n&uuml; inceleyip beğendiğiniz takdirde&nbsp;<strong>&ldquo;Sepete Ekle&rdquo;</strong>&nbsp;butonuna basarak alışverişe başlayabilirsiniz.</p>
<p>&nbsp;</p>
<p>2.Adım: Karşınıza &ccedil;ıkan yeni sayfada sepetinizde bulunan &uuml;r&uuml;nleri ve &ouml;deyeceğiniz toplam puan miktarını g&ouml;rebileceksiniz. Eğer siparişinize başka bir &uuml;r&uuml;n eklemek isterseniz <strong>&ldquo;Alışverişe Devam Et&rdquo;</strong>&nbsp;butonuna tıklayarak sol tarafta bulunan &uuml;r&uuml;n kategorilerinden se&ccedil;eceğiniz diğer &uuml;r&uuml;n&uuml; sepetinize ekleyebilirsiniz. &ldquo;Sepetim&rdquo; sayfasında siparişini vereceğiniz t&uuml;m hediye &uuml;r&uuml;nleri ve &ouml;deyeceğiniz toplam puan miktarını g&ouml;rebilecek ve&nbsp;<strong>&ldquo;Satın Al&rdquo;</strong>&nbsp;butonu ile &ldquo;<strong>Adres Bilgileri&rdquo;</strong>&nbsp;adımına ge&ccedil;eceksiniz.&nbsp;</p>
<p><br />3.Adım: Adres bilgilerinizin yer alacağı&nbsp;<strong>&ldquo;Adres Bilgileri&rdquo;</strong>&nbsp;ekranında, ister mevcut adreslerinizden birini se&ccedil;erek, isterseniz yeni teslimat adresi bilgilerinizi girip kaydettikten sonra&nbsp;<strong>&ldquo;Adrese G&ouml;nder&rdquo;</strong>&nbsp;butonuna tıklayarak işleminize devam edecek ve&nbsp;<strong>&ldquo;Sipariş Onay&rdquo;&nbsp;</strong>sayfasına y&ouml;nlendirileceksiniz.&nbsp;</p>
<p><br />4.Adım:&nbsp;<strong>&ldquo;Sipariş Onay&rdquo;&nbsp;</strong>ekranında karşınıza gelen bilgiler &uuml;yelik bilgileriniz, teslimat bilgileri ile Uygulama ve Koşullar dok&uuml;manı olacaktır. Bilgilerinizin doğruluğunu kontrol ederek, Uygulama ve Koşullar dok&uuml;manını onaylamanız durumunda&nbsp;<strong>&ldquo;Siparişi Onayla&rdquo;</strong>&nbsp;butonuna basarak siparişinizi tamamlayabilirsiniz.&nbsp;</p>
<p><u>&nbsp;</u></p>
<p><u>Hediyenizi g&uuml;zel g&uuml;nlerde kullanınız&hellip;</u><u>&nbsp;</u></p>
<p><br /><br /></p>
<p><strong>2- Hediye siparişi sırasında puanlarımın t&uuml;m&uuml;n&uuml; aynı anda kullanabilir miyim?</strong><br />Aynı g&uuml;n i&ccedil;erisinde hesabınızda harcanabilir miktar olarak g&ouml;r&uuml;nt&uuml;lenen puanlarınızın tamamını kullanabilirsiniz.&nbsp;</p>
<p><br /><br /></p>
<p><strong>3- Sepetime eklediğim &uuml;r&uuml;nleri silebilir miyim?</strong><br />Sepetinize eklediğiniz &uuml;r&uuml;nlerinizi, &uuml;r&uuml;n g&ouml;rselinin solunda bulunan kutucuğu işaretledikten sonra<strong>&nbsp;&ldquo;Sil</strong>&rdquo; butonuna tıklayarak silebilirsiniz. Ayrıca yine sepetinizdeki &uuml;r&uuml;nleri se&ccedil;tikten sonra&nbsp;<strong>&ldquo;Sepeti g&uuml;ncelle&rdquo;&nbsp;</strong>butonunu kullanarak &uuml;r&uuml;nlerinizin g&uuml;ncel puan durumlarını da g&ouml;r&uuml;nt&uuml;leyebilirsiniz.&nbsp;<br /><br /></p>
<p><strong>TESLİMAT</strong><br /><br /><strong>1- Hediye Siparişimi verdikten sonra durumunu nasıl takip ederim?</strong><br /><a href="http://www.motulteam.com">www.motulteam.com</a><strong>&nbsp;</strong>sitemize giriş yaptıktan sonra &uuml;st kısımda yer alan&nbsp;<strong>&ldquo;Siparişlerim&rdquo;&nbsp;</strong>butonuna tıklayarak, t&uuml;m siparişlerinizi takip edebilirsiniz. Ayrıca&nbsp;<strong>&ldquo;Hesabım&rdquo;</strong>&nbsp;linkinden de siparişlerinize ulaşabilirsiniz.<br /><strong><br />2- Sipariş ettiğim &uuml;r&uuml;n gecikti. Ne yapabilirim?</strong></p>
<p>Siparişinizi verdiğiniz &uuml;r&uuml;n&uuml;n takibini&nbsp;<strong>&ldquo;Siparişlerim&rdquo;</strong>&nbsp;alanından yapabilmektesiniz. Tedarik s&uuml;resini aşan siparişlerinizin tedarik durumu ya da akıbetini &ouml;ğrenmek i&ccedil;in sitemizde bulunan İletişim Formu alanından bizlere ulaşarak sorgulayabilirsiniz.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p>&nbsp;</p>
<p><strong>3- Sipariş ettiğim hediyemi iade ya da iptal ettirmek istiyorum. Puan iadem nasıl yapılacak?</strong><br />Sipariş etmiş olduğunuz &uuml;r&uuml;n &ldquo;Siparişiniz İşleme Alınmıştır&rdquo; stat&uuml;s&uuml;nde ise ve siparişiniz hen&uuml;z tedarik edilmediyse,&nbsp;talebinizi <a href="http://www.motulteam.com">www.motulteam.com</a> sitemizde bulunan iletişim formu alanından yazarak bizlere iletebilirsiniz. Siparişiniz iptal edilmeye uygun stat&uuml;deyse, sipariş iptalini sağlayabiliriz. Siparişiniz iptal edildiğinde puanınız hesabınıza iade edilir. Siparişiniz &ldquo;tedarikte&rdquo; stat&uuml;s&uuml;nde ise iade ya da iptal yapılamamaktadır. <strong>&ldquo;Siparişlerim&rdquo;</strong>&nbsp;b&ouml;l&uuml;m&uuml;nden t&uuml;m siparişlerinizin durumunu &ouml;ğrenebilirsiniz.&nbsp;</p>
<p><br /><br /></p>
<p><strong>4- Hediyemin bana ulaşmadan sevkiyatı sırasında hasar g&ouml;rd&uuml;ğ&uuml;n&uuml; fark ettim. Ne yapabilirim?</strong><br />Sevkiyat sırasında zarar g&ouml;rd&uuml;ğ&uuml;n&uuml; d&uuml;ş&uuml;nd&uuml;ğ&uuml;n&uuml;z paketleri teslim aldığınız kargo yetkilisi &ouml;n&uuml;nde a&ccedil;ıp kontrol etmeniz gerekmektedir. Eğer &uuml;r&uuml;nde bir tahribat mevcut ise kargo firmasına tutanak tutturarak &uuml;r&uuml;n&uuml; teslim almayınız. &Uuml;r&uuml;n teslim alındıktan sonra kargo firmasının g&ouml;revini tam olarak yerine getirdiğini kabul etmiş olduğunu unutmayınız.<br /><br /><strong>5- Hangi &uuml;r&uuml;nleri iade edememekteyim?</strong><br />a. &Uuml;r&uuml;n&uuml;n arızalı veya ayıplı &ccedil;ıkması halleri dışında a&ccedil;ıldıktan sonra sağlık a&ccedil;ısından tehlike arz edebilecek veya tekrar kullanılamayacak &uuml;r&uuml;nlerin (&Ouml;rneğin kullanım esnasında v&uuml;cut ile bire bir temas gerektiren her t&uuml;rl&uuml; &uuml;r&uuml;n, tek kullanımlık &uuml;r&uuml;nler).&nbsp;<br />b. Her t&uuml;rl&uuml; yazılım ve programlar, dvd, vcd, kasetler, taşınabilir bilgisayarlar, bilgisayar ve kırtasiye sarf malzemeleri (Toner, kartuş, şerit vb.), her t&uuml;rl&uuml; kozmetik &uuml;r&uuml;nler<br />c. Ambalajı / Jelatini a&ccedil;ılmış cep telefonları&nbsp;<br />d. Orijinal ambalajı zarar g&ouml;rm&uuml;ş, tahrip edilmiş, kullanılmış &uuml;r&uuml;nler.<br /><br /><strong>6-Hediyemin arızalı olması durumunda ne yapmalıyım?</strong><br />&Uuml;r&uuml;n m&uuml;şteriye ulaştıktan sonra ortaya &ccedil;ıkabilecek arızalar i&ccedil;in, &uuml;retici firmanın yetkili servislerine başvurulmalıdır. Yetkili servis bilgileri, &uuml;r&uuml;n&uuml;n&uuml;z ile birlikte gelen garanti belgesinde mevcuttur.</p>
<p>&nbsp;</p>
<p><strong style="color:red;">Not :&nbsp;</strong>Sipariş vermiş olduğunuz &uuml;r&uuml;nlerde renk ve desen garantisi bulunmamaktadır. Bu y&uuml;zden yapılan &uuml;r&uuml;n iadeleri kabul edilememektedir.&nbsp;</p>
<p><strong>&nbsp;</strong></p>
<p><strong>KARGO</strong><br /><br /><strong>1- Ne kadar kargo &uuml;creti &ouml;deyeceğim?</strong><br />Kargo &uuml;creti &ouml;demeniz gerekmemektedir.<br /><br /><strong>2- Kargo takibimi veya teslimat bilgilerimi nereden &ouml;ğreneceğim?</strong><br />Siparişini verdiğiniz &uuml;r&uuml;nler kargo aracılığı ile sipariş verirken bildirdiğiniz teslimat adresinize g&ouml;nderilecektir. &Uuml;ye girişinize m&uuml;teakip&nbsp;<strong>&ldquo;Siparişlerim&rdquo;</strong>&nbsp;linkine tıkladıktan sonra&nbsp;<u>kargoya verilen siparişlerinizin durumunu</u>&nbsp;<strong>&ldquo;Kargom Nerede&rdquo;&nbsp;</strong>butonuna basarak takip edebilirsiniz.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>




</asp:Content>

