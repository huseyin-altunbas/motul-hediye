﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="GuvenliTicaretSSS.aspx.cs" Inherits="MusteriHizmetleri_SSS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Sıkça Sorulan Sorular
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <h3>
        1. Motul Hediye'de Veri Güvenliği ve Gizliliği Nasıldır ?
    </h3>
    Tüm sistem web tabanlı olduğundan kullanıcıların zahmetsizce tanınması ve veri güvenliğinin
    hızla onaylanabilmesi için kullanıcıların web tarayıcılarına "cookie" adı verilen
    veriler gönderilmekte ve müşteri bilgisayarlarında saklanan bu cookieler belli aralıklarla
    güncellenerek optimal sistem performansı sağlanmaktadır.
    <br />
    <br />
    <h3>
        Veri Güvenliği:</h3>
    Kullanıcılardan derlenen tüm hassas ve kişisel bilgiler, en yüksek elektronik ve
    fiziksel güvenlik standartlarında saklanmakta, yalnızca yetki sahibi olan personelin
    ve ancak zorunlu durumlarda kullanıcı onayı altında ulaşabileceği bir sistemle,
    mevcut T.C. kanunları ve uluslararası kanunlar çerçevesinde kullanılmaktadır.
    <br />
    <br />
Motul Hediye kullanıcılardan derlediği kişisel ve hassas bilgileri kullanıcı
    onayı olmaksızın hiçbir isim altında, hiçbir nedenle ve hiçbir muhatap nezdinde
    açıklamamayı, söz konusu bilgileri üçüncü şahıslarla paylaşmamayı ve hiçbir surette
    kötüye kullanmamayı taahhüt ve garanti eder.
    <br />
    <br />
    <h3>
        Veri Paylaşımı:</h3>
Motul Hediye yukarıda sözü edilen kişisel içerikli bilgiler dışındaki genel kullanıcı
    bilgilerini, sunduğu hizmetleri daha verimli kılmak ve müşteri memnuniyetini en
    yüksek seviyede tutabilmek amacıyla analiz ederek yorumlamak ve seçkin iş ortakları
    ile paylaşmak hakkını saklı tutar.
    <br />
    <br />
    <h3>
        2. Online Alışverişlerde Önemli Güvenlik Unsurları ?
    </h3>
    Sanılanın aksine, kullanıcıların kredi kartı bilgileri çoğu zaman internet üzerinden
    yapılan alışverişlerde değil fiziki ortamlarda çalınmaktadır. Bu nedenle kredi kartınızı
    günlük hayatta kullanırken dikkatli olun, kredi kartı numaranızı, son kullanma tarihini
    ve CVC (Güvenlik numarası) numarasını başkalarına vermeyin. Restoran, mağaza gibi
    fiziki ortamlarda çalınan kredi kartı bilgileriyle alışveriş yapmaya çalışan sahtekarlara
    karşı önlemler alan e-ticaret siteleri bu sahtekarları yakaladıkları anda kredi
    kartının gerçek sahibiyle irtibata geçmektedirler, bu gibi durumlarda işbirliği
    yapın. Ay sonu gelen ekstre bilgilerini dikkatli kontrol edin ve size bir şey ifade
    etmeyen alışverişler için firmalarla irtibata geçin.
    <br />
    <br />
    İnternetten alışveriş yaparken bildiğiniz firmaları tercih edin. Bilgi hattı veya
    müşteri hizmetleri servisi bulunan siteleri tercih edin. Bu sayede aklınıza takılan
    bütün konular hakkında detaylı bilgi alabilirsiniz. Internet sitesinde firmanın
    açık adresinin ve telefonun yer almasına dikkat edin. Bilmediğiniz bir firmadan
    alışveriş yapacaksanız alışverişinizi yapmadan ürünü aldığınız mağazanın bütün telefon
    / adres bilgilerini not edin. Eğer güvenmiyorsanız alışverişten önce telefon ederek
    teyit edin.
    <br />
    <br />
    <h3>
        3. Kredi Kartı Provizyon Aksaklıkları ?</h3>
    Siparişiniz sırasında kredi kartınız farklı nedenlerden ötürü onaylanamayabilir:
    <br />
    <br />
    Bu nedenler arasında başlıcaları şunlardır;<br />
    1. Kredi kartınızın kullanılabilir limitinin yetersiz olması,
    <br />
    2. Kart numarasını hatalı girmeniz,
    <br />
    3. Kartınızın son kullanma tarihinin geçmiş olması,
    <br />
    4. Kullandığınız kart bir ek kart ise, ana kart sahibi tarafından iptal edilmiş
    olması,
    <br />
    5. Onay işlemi sırasında geçici bağlantı sorunları yaşanması,
    <br />
    6. Kayıp ihbarı yaptığınız bir kartı yeniden kullanmaya çalışmanız,
    <br />
    7. Kullanmaya çalıştığınız kartın bir kredi kartı değil, ATM kartı olması
    <br />
    sayılabilir.
    <br />
    Kredi kartı onay sorunlarınızda yardım almak için kartı aldığınız banka ile görüşmeniz
    gerekmektedir.
    <br />
    <br />
    <h3>
        4. Siparişim elime ne zaman ulaşır ?
    </h3>
    Siparişlerinizin teslimat süresi ödeme onayınızın alınmasıyla başlar ve siparişleriniz
    tedarikçiden temininin sağlanmasıyla kargoya verilir.Kargo firmalarının yurtiçi
    teslimat süreleri 1 ila 5 gün arasında değişiklik göstermektedir.
    <br />
    <br />
    <h3>
        5. Mesai saati dışı ve hafta sonu siparişleri ?
    </h3>
    Mesai saatlerimiz hafta içi 09:00 - 19:00 , Cumartesi 09:00 - 17:00 olduğundan bu
    çalışma saatleri dışında siz değerli müşterilerimizin verdiği siparişler bir sonraki
    gün mesai saati başlangıcı ile işleme alınmaktadır. Hafta sonu vermiş olduğunuz
    siparişler Pazar günleri çalışmamamız nedeniyle pazartesi günü işleme alınacaktır.
    <br />
    <br />
    <h3>
        6. Teslimat ve Fatura adreslerim farklı olabilir mi ?
    </h3>
    Satın alma adımlarında karşınıza gelecek olan Teslimat ve Fatura adreslerinizi farklı
    olarak girebilirsiniz.
    <br />
    <br />
    <h3>
       Motul Hediye'de Kargo Ücretli mi?
    </h3>
    Kargo bölümünde siparişinizi size ulaştıracak kargo firması UPS kargodur. Sitemiz
    üzerinden satın aldığınız 750 TL üzeri ürünlerinizin kargo ücreti firmamız tarafından
    ödenmektedir. Kampanyalı alışverişler de kargo ücreti müşteri tarafından ödenmektedir.
    <br />
    <br />
    <ul style="margin-left: 25px;">
        <li>Siparişinizin kurye tarafından size ulaştırılmasını istediğiniz taktirde İstanbul
            içi siparişleriniz Kuryetel (www.kuryetel.com) tarafından en fazla 3 saat içinde
            kapınızda olacaktır.</li>
        <li>Kurye ücreti alıcı tarafından ürün teslimi sırasında yapılmaktadır.</li>
        <li>Kurye ücreti teslimatın yapılacağı yere göre değişiklik göstermektedir.</li>
        <li>Kurye ücreti hakkında detaylı bilgi almak için 444 9 000 numaralı telefonu arayabilirsiniz
            yada www.kuryetel.com web sitesini ziyaret edebilirsiniz.</li>
    </ul>
    <br />
    <h3>
        8. Yurtdışına teslimat imkanınız var mı?
    </h3>
 Motul Hediye üzerinden yapılan alışverişlerde yurtdışına ve Kıbrıs’a teslimat
    imkanı şuan bulunmamaktadır.
    <br />
    <br />
    <h3>
        9. Teslimat sadece belirtilen adrese mi olmalı?
    </h3>
    Sitemiz üzerinde yapılan tüm gönderiler sipariş esnasında belirtilen adrese teslim
    edilmektedir. Kargo şubesinden kesinlikle mal teslimatı yapılamaz. Alıcının adreste
    olmaması durumunda ikinci defa teslimat için ilgili adrese uğrama yapılır. Teslimat
    adresinin değiştirilmesi isteniyorsa satış temsilcileri ile irtibata geçilerek gerekli
    prosedürler yerine getirilir. Ancak alıcı ismi değişemez.
    <br />
    <br />
    <h3>
        10. Siparişim bana ulaşamaz ise iptal edilir mi?
    </h3>
    Üyelik kaydı yapılırken ve adres bilgileriniz girilirken özellikle telefon numaralarınızın
    dikkatli ve doğru bir biçimde kayıt edilmesi gerekir. Güvenliği sağlamak için sizinle
    irtibat kurulamaması durumunda siparişiniz iptal edilerek ücret iadeniz gerçekleştirilir.
</asp:Content>
