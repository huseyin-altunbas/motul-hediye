﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="UyeKayit.aspx.cs" Inherits="MusteriHizmetleri_UyeKayit"
    EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Üye Kayıt</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
    <span class="page-heading-title2">ÜYE KAYIT</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <script src="/assets/js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/assets/lib/fancyBox/jquery.fancybox.css" />
    <script type="text/javascript" src="/assets/lib/fancyBox/jquery.fancybox.js"></script>
    <!-- popup -->
    <script type="text/javascript">
        function Popup(message) {
            if ($('#popup').length > 0 && $('#popupContent').length > 0) {
                $("#popupContent").html(message).promise().done(function () {
                    $('#popup').fancybox().trigger('click');
                });
            }
        }
    </script>
    <a id="popup" class="fancybox" href="#popupContent" style="display: none"></a>
    <div id="popupContent" style="display: none">
    </div>
    <!-- popup -->
    <script type="text/javascript">

        var chkSozlesmeId = '<%=chkUyelikSozlesmesi.ClientID %>';

        function ValidateSozlesme(obj, args) {
            var checkbox = $("#" + chkSozlesmeId); args.IsValid = checkbox.is(':checked');
        }

        
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtCepTelKod.ClientID %>").mask("599");
            $("#<%=txtCepTel.ClientID %>").mask("9999999");

            if ($('#uyeliksozlesme').length > 0)
                $('#uyeliksozlesme').fancybox();
        });
    </script>
    <div class="page-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box-form">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="">
                                Adınız</label>
                            <asp:TextBox ID="txtAd" runat="server" CssClass="form-control"></asp:TextBox>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqtxtAd" runat="server" ControlToValidate="txtAd"
                                    ErrorMessage="Adınızı girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <label for="">
                                Soyadınız</label>
                            <asp:TextBox ID="txtSoyad" runat="server" CssClass="form-control"></asp:TextBox>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqtxtSoyad" runat="server" ControlToValidate="txtSoyad"
                                    ErrorMessage="Soyadınızı girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="">
                                E-Posta</label>
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqtxtEmail" runat="server" ControlToValidate="txtEmail"
                                    ErrorMessage="E-Posta adresinizi girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regextxtEmail" runat="server" ErrorMessage="E-Posta adresinizi kontrol edin."
                                    ControlToValidate="txtEmail" Display="Dynamic" ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z\-])[-\.\w]*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
                                    ValidationGroup="grpUyelik"></asp:RegularExpressionValidator>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <label for="">
                                E-Posta(Tekrar)</label>
                            <asp:TextBox ID="txtEmailTekrar" runat="server" CssClass="form-control"></asp:TextBox>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqtxtEmailTekrar" runat="server" ControlToValidate="txtEmailTekrar"
                                    ErrorMessage="E-Posta adresinizi girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cmptxtEmailTekrar" runat="server" ErrorMessage="E-Posta adresinizi tekrar girin."
                                    ControlToValidate="txtEmailTekrar" ControlToCompare="txtEmail" Display="Dynamic"
                                    ValidationGroup="grpUyelik"></asp:CompareValidator>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="">
                                Şifreniz</label>
                            <asp:TextBox ID="txtSifre" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqtxtSifre" runat="server" ControlToValidate="txtSifre"
                                    ErrorMessage="Şifrenizi girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <label for="">
                                Şifreniz(Tekrar)</label>
                            <asp:TextBox ID="txtSifreTekrar" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqSifreTekrar" runat="server" ControlToValidate="txtSifreTekrar"
                                    ErrorMessage="Şifrenizi girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cmptxtSifreTekrar" runat="server" ErrorMessage="Şifrenizi tekrar girin."
                                    ControlToValidate="txtSifreTekrar" ControlToCompare="txtSifre" Display="Dynamic"
                                    ValidationGroup="grpUyelik"></asp:CompareValidator>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="">
                                Doğum Tarihi</label>
                            <div class="row">
                                <div class="col-xs-4" style="padding-left: 15px; padding-right: 2px;">
                                    <asp:DropDownList ID="dpDogumGun" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-xs-4" style="padding-left: 2px; padding-right: 2px;">
                                    <asp:DropDownList ID="dpDogumAy" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-xs-4" style="padding-left: 2px; padding-right: 15px;">
                                    <asp:DropDownList ID="dpDogumYil" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqdpDogumGun" InitialValue="0" runat="server" ControlToValidate="dpDogumGun"
                                    ErrorMessage="Doğduğunuz günü seçin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>&nbsp;
                                <asp:RequiredFieldValidator ID="rqdpDogumAy" InitialValue="0" runat="server" ControlToValidate="dpDogumAy"
                                    ErrorMessage="Doğduğunuz ayı seçin." ValidationGroup="grpUyelik" Display="Dynamic"></asp:RequiredFieldValidator>&nbsp;
                                <asp:RequiredFieldValidator ID="rqdpDogumYil" InitialValue="0" runat="server" ControlToValidate="dpDogumYil"
                                    ErrorMessage="Doğduğunuz yılı seçin." ValidationGroup="grpUyelik" Display="Dynamic"></asp:RequiredFieldValidator>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <label for="">
                                Cinsiyet</label>
                            <div class="row">
                                <div class="col-xs-3">
                                    <asp:RadioButton ID="rbKadin" Text="Kadın" runat="server" Checked="true" GroupName="Cinsiyet" />
                                </div>
                                <div class="col-xs-3">
                                    <asp:RadioButton ID="rbErkek" Text="Erkek" runat="server" GroupName="Cinsiyet" />
                                </div>
                                <div class="col-xs-6">
                                </div>
                            </div>
                            <label class="fright notification">
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="">
                                Şehir</label>
                            <asp:DropDownList ID="dpSehir" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqdpSehir" InitialValue="0" runat="server" ControlToValidate="dpSehir"
                                    ErrorMessage="Şehrinizi seçin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <label for="">
                                Cep Telefonu Numarası</label><br />
                            <asp:TextBox ID="txtCepTelKod" CssClass="form-control input-sm inlineBlock" Width="60px"
                                MaxLength="3" runat="server"></asp:TextBox>
                            <asp:TextBox ID="txtCepTel" CssClass="form-control input-sm inlineBlock" Width="120px"
                                MaxLength="7" runat="server"></asp:TextBox>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqtxtCepTelKod" runat="server" ControlToValidate="txtCepTelKod"
                                    ErrorMessage="Cep Telefonu kodunuzu yazın." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rqtxtCepTel" runat="server" ControlToValidate="txtCepTel"
                                    ErrorMessage="Cep Telefonu numaranızı yazın." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="">
                                Güvenlik Kodu</label><br />
                            <asp:TextBox ID="txtGuvenlikKodu" CssClass="form-control inlineBlock" runat="server"
                                Width="180px"></asp:TextBox>
                            &nbsp;<a href="/MusteriHizmetleri/UyeKayit.aspx">
                                <asp:Image ID="imgCaptcha" runat="server" AlternateText="Güvenlik Resmi" />
                            </a>
                            <label class="fright notification">
                                <asp:RequiredFieldValidator ID="rqtxtGuvenlikKodu" runat="server" ControlToValidate="txtGuvenlikKodu"
                                    ErrorMessage="Güvenlik kodunu girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <label for="">
                            </label>
                            <ul class="check-box-list" style="margin: 5px 0px 10px 0px;">
                                <li>
                                    <asp:CheckBox ID="chkUyelikSozlesmesi" runat="server" />
                                    <label for="cphIcerikMetin_chkUyelikSozlesmesi">
                                        <span class="button" style="margin-top: 0px !important;"></span>Üyelik Sözleşmesini
                                        kabul ediyorum.
                                    </label>
                                </li>
                                <li>&raquo;&nbsp;<a id="uyeliksozlesme" class="fancybox" href="#uyeliksozlesmeicerik"
                                    style="font-size: 12px; font-weight: bold;">Üyelik Sözleşmesini okumak için tıklayın.</a>
                                    <div id="uyeliksozlesmeicerik" style="display: none">
                                        <iframe width="640" height="360" src="/Sozlesmeler/UyelikSozlesmesi.htm" frameborder="0"
                                            allowfullscreen></iframe>
                                    </div>
                                </li>
                            </ul>
                            <label class="fright notification">
                                <asp:CustomValidator ID="CustomValidatorSozlesme" runat="server" ClientValidationFunction='ValidateSozlesme'
                                    OnServerValidate="CustomValidatorSozlesme_ServerValidate" ValidationGroup="grpUyelik"
                                    ErrorMessage="Lütfen üyelik sözleşmesini kabul edin." ValidateEmptyText="True"></asp:CustomValidator>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="check-box-list" style="margin: 5px 0px 10px 0px;">
                                <li>
                                    <asp:CheckBox ID="chkEmailHaber" runat="server" Checked="true" />
                                    <label for="cphIcerikMetin_chkEmailHaber">
                                        <span class="button" style="margin-top: 0px !important;"></span>Kampanyalardan e-posta
                                        ile haberdar olmak istiyorum.
                                    </label>
                                </li>
                                <li>
                                    <asp:CheckBox ID="chkCepHaber" runat="server" Checked="true" />
                                    <label for="cphIcerikMetin_chkCepHaber">
                                        <span class="button" style="margin-top: 0px !important;"></span>Kampanyalardan SMS
                                        ile haberdar olmak istiyorum.
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <div class="fleft h45 color4e mt30 ml28 w350">
                                <asp:LinkButton ID="lnbUyeKayit" CssClass="btn btnInput" ValidationGroup="grpUyelik"
                                    runat="server" OnClick="lnbUyeKayit_Click"><i class="fa fa-user"></i>&nbsp;Ücretsiz Kayıt Ol</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
