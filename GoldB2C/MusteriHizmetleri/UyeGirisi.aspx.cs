﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using Goldb2cBLL;
using Goldb2cInfrastructure;
using Goldb2cEntity;
using System.IO;


public partial class MusteriHizmetleri_UyeGirisi : System.Web.UI.Page
{
    MarketingScriptHelper marketingScriptHelper = new MarketingScriptHelper();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {

            Response.Redirect("~/");
        }

        else
        {
            Response.Redirect("/");
        }

    }

    protected void lnbGiris_Click(object sender, EventArgs e)
    {
    }

    private void KullaniciGiris(string SessionId, string Sifre, bool BeniHatirla)
    {
        //string YonlenecekUrl = "";

        //try
        //{
        //    GoldService goldService = new GoldService();
        //    TGOLDUserInfo tGoldUserInfo = goldService.UserLogin(Convert.ToInt32(SessionId));

        //    int kullaniciID = bllUyelikIslemleri.MusteriKullaniciGiris(tGoldUserInfo.ID.ToString(), Sifre);
        //    if (kullaniciID > 0)
        //    {
        //        //Create a new cookie, passing the name into the constructor
        //        HttpCookie hcookie = new HttpCookie("HNetLogin");
        //        //Set the cookies value
        //        hcookie.Value = "true";
        //        //Set the cookie to expire in 1 hour
        //        DateTime dtNow = DateTime.Now;
        //        TimeSpan tsHour = new TimeSpan(0, 1, 0, 0);
        //        hcookie.Expires = dtNow + tsHour;
        //        //Add the cookie
        //        Response.Cookies.Add(hcookie);

        //        // kullanıcı sisteme giriş yapabilir
        //        // kullanıcıyı sisteme kayıt et

        //        Kullanici kullanici = bllUyelikIslemleri.KullaniciSistemKayit(kullaniciID);

        //        /* Adres Kaydediliyor */
        //        /* bool KayitSonuc = false; */
        //        if (kullanici.ErpCariId <= 0)
        //        {
        //            kullanici = bllUyelikIslemleri.CariVeriOlustur(kullanici);

        //            Session["totalGoldPoint"] = tGoldUserInfo.Puan;
        //            Session["CRMCustomerGUID"] = tGoldUserInfo.ID;

        //        }
        //        else
        //        {
        //            Session["totalGoldPoint"] = tGoldUserInfo.Puan;
        //            Session["CRMCustomerGUID"] = tGoldUserInfo.ID;
        //        }

        //        if (BeniHatirla == true)
        //        {
        //            Crypt crypt = new Crypt();
        //            string encryptKey = CustomConfiguration.getEncryptKey;

        //            Goldb2cBLL.Cookie cookie = new Goldb2cBLL.Cookie();
        //            cookie.SetCookie("SessionId", crypt.EncryptData(encryptKey, tGoldUserInfo.ID.ToString()));
        //            cookie.SetCookie("Sifre", crypt.EncryptData(encryptKey, Sifre));
        //            HttpContext.Current.Response.Cookies.Add(cookie.GetCookie("GoldComTrUser"));
        //        }

        //        // sonuç sayfasına yönlendir
        //        if (Session["SonUrl"] != null)
        //            YonlenecekUrl = Session["SonUrl"].ToString();
        //        else
        //            YonlenecekUrl = "/MusteriHizmetleri/Hosgeldiniz.aspx";

        //        Session["SonUrl"] = null;
        //        Response.Redirect(YonlenecekUrl, false);
        //    }
        //    else if (kullaniciID == -1)// email-sessionid doğru, şifre hatalı
        //    {
        //        YonlenecekUrl = "~/Default.aspx";
        //        Response.Redirect(YonlenecekUrl, false);
        //        //ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Şifreniz hatalıdır. Lütfen kontrol edip yeniden deneyiniz."));
        //    }
        //    else if (kullaniciID == -2)// email-sessionid ve şifre hatalı            
        //    {
        //        DateTime dtDogumTarih = new DateTime(1753, 1, 1);
        //        int kId = bllUyelikIslemleri.MusteriKullaniciKayit(tGoldUserInfo.Adi, tGoldUserInfo.Soyadi, tGoldUserInfo.ID.ToString(), "Lafarge", dtDogumTarih, string.Empty, "TR", "İSTANBUL", "ÜSKÜDAR", string.Empty, string.Empty, string.Empty, "555", "5555555", false, false, "B2CLFG", string.Empty, string.Empty, string.Empty, "");

        //        if (kId > 0)
        //        {
        //            LogIslemleriBLL bllLogIslemleri = new LogIslemleriBLL();
        //            try
        //            {
        //                bllLogIslemleri.KullaniciOlusturLogKayit(kId, 0, "Yeni bir kullanıcı eklendi.", "", kId);
        //            }
        //            catch (Exception)
        //            {
        //                //
        //            }
        //        }

        //        YonlenecekUrl = "/MusteriHizmetleri/UyeGirisi.aspx?sessionid=" + tGoldUserInfo.ID.ToString();
        //        Response.Redirect(YonlenecekUrl, false);
        //    }
        //    else
        //    {
        //        YonlenecekUrl = "~/Default.aspx";
        //        Response.Redirect(YonlenecekUrl, false);
        //        //ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sistemde bir hata oluştu. Lütfen daha sonra yeniden deneyiniz."));
        //    }
        //}
        //catch
        //{
        //    YonlenecekUrl = "~/Default.aspx";
        //    Response.Redirect(YonlenecekUrl, false);
        //    //ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sistemde bir hata oluştu. Lütfen daha sonra yeniden deneyiniz."));
        //}
    }

    private string MesajScriptOlustur(string Mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<table width=\"200\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td><table width=\"200\" align=\"center\"><tr><td align=\"center\">" + Mesaj + "</td></tr></table></td></tr><tr><td></br> <a onclick=\"unblockDivKapat()\" style=\"cursor:hand\"><b>Kapat</b></a></td></tr></table>');</script>";
    }

    protected void btnFacebookConnect_Click(object sender, EventArgs e)
    {
        //string FacebookAppID = CustomConfiguration.Facebook_client_id;
        //string FacebookCallbackURL = CustomConfiguration.Facebook_redirect_url;
        //string serviceURL = string.Format("https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}&scope=email,user_birthday",
        //FacebookAppID, FacebookCallbackURL);
        //if (HttpContext.Current.Session["SonUrl"] != null)
        //    Session["FbConnectSonUrl"] = HttpContext.Current.Session["SonUrl"].ToString();
        //else
        //    Session["FbConnectSonUrl"] = Request.UrlReferrer.OriginalString;
        //Response.Redirect(serviceURL);
    }
}