﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;

public partial class MusteriHizmetleri_Hosgeldiniz : System.Web.UI.Page
{
    MarketingScriptHelper marketingScriptHelper = new MarketingScriptHelper();
    Kullanici kullanici;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)Session["Kullanici"];
            lblKullaniciAdSoyad.Text = ((Kullanici)Session["Kullanici"]).KullaniciAd + " " + ((Kullanici)Session["Kullanici"]).KullaniciSoyad;

        }
        else
            Response.Redirect("/", false);


    }
}