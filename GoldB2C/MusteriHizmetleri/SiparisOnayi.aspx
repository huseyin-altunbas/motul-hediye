﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="SiparisOnayi.aspx.cs" Inherits="MusteriHizmetleri_SiparisOnayi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Sipariş Onayı
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    Onay ekranı siparişlerinizin tüm ayrıntılarını görebileceğiniz ve siparişinizin
    onaylanıp onaylanmadığını gördüğünüz son ekrandır.
</asp:Content>
