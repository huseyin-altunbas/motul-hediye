﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using System.IO;
using System.Text;
using System.Data;


public partial class MusteriHizmetleri_UyeKayit : System.Web.UI.Page
{
    BannerBLL bllBanner = new BannerBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    string RandomNumber = "Sayfayı Yenileyiniz";

    protected void Page_Load(object sender, EventArgs e)
    {
        //Kapalı..
        Response.Redirect("/");

        //if (!IsPostBack)
        //{
        //    CapchaGenerator();

        //    dpSehir.DataSource = bllUyelikIslemleri.SehirListesi("TÜRKİYE");
        //    dpSehir.DataValueField = "SehirAd";
        //    dpSehir.DataTextField = "SehirAd";
        //    dpSehir.DataBind();
        //    dpSehir.Items.Insert(0, new ListItem("Seçiniz", "0"));

        //    dpDogumGun.Items.Add(new ListItem("Gün", "0"));
        //    for (int i = 1; i <= 31; i++)
        //    {
        //        dpDogumGun.Items.Add(new ListItem(i.ToString(), i.ToString()));
        //    }

        //    dpDogumAy.Items.Add(new ListItem("Ay", "0"));
        //    for (int i = 1; i <= 12; i++)
        //    {
        //        dpDogumAy.Items.Add(new ListItem(new System.Globalization.CultureInfo("tr-TR").DateTimeFormat.GetMonthName(i).Substring(0, 3), i.ToString()));
        //    }

        //    dpDogumYil.Items.Add(new ListItem("Yıl", "0"));
        //    for (int i = 1900; i <= DateTime.Now.Year; i++)
        //    {
        //        dpDogumYil.Items.Add(new ListItem(i.ToString(), i.ToString()));
        //    }
        //}
    }

    protected void lnbUyeKayit_Click(object sender, EventArgs e)
    {

        //if (String.IsNullOrEmpty(txtGuvenlikKodu.Text.Trim()) || Session["UyelikCaptcha"] == null || txtGuvenlikKodu.Text.Trim() != Session["UyelikCaptcha"].ToString())
        //{
        //    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen Güvenlik Kodunu kontrol ediniz."));
        //    CapchaGenerator();
        //    txtGuvenlikKodu.Text = "";

        //    return;

        //}

        //if (string.IsNullOrEmpty(txtAd.Text) || string.IsNullOrEmpty(txtEmail.Text) || string.IsNullOrEmpty(txtSifre.Text) || string.IsNullOrEmpty(txtSifreTekrar.Text))
        //{
        //    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen alanlarınız kontrol ediniz."));
        //    CapchaGenerator();
        //    txtGuvenlikKodu.Text = "";
        //    return;
        //}
        //if (txtSifreTekrar.Text != txtSifre.Text)
        //{
        //    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen şifrenizi kontrol ediniz."));
        //    CapchaGenerator();
        //    txtGuvenlikKodu.Text = "";
        //    return;
        //}

        //if (string.IsNullOrEmpty(txtCepTelKod.Text) || txtCepTelKod.Text.Length != 3)
        //{
        //    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen GSM operatörünüzü kontrol ediniz."));
        //    CapchaGenerator();
        //    txtGuvenlikKodu.Text = "";
        //    return;
        //}

        //if (string.IsNullOrEmpty(txtCepTel.Text) || txtCepTel.Text.Length != 7)
        //{
        //    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen GSM numaranızı kontrol ediniz."));
        //    CapchaGenerator();
        //    txtGuvenlikKodu.Text = "";
        //    return;
        //}

        //if (txtSifre.Text.Length < 5 || txtSifre.Text.Length > 20)
        //{
        //    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Şifreniz 5 ile 20 karakter arasında olmalıdır."));
        //    CapchaGenerator();
        //    txtGuvenlikKodu.Text = "";
        //    return;
        //}

        //if (txtSifre.Text != txtSifreTekrar.Text)
        //{
        //    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Şifrenizi kontrol ediniz."));
        //    CapchaGenerator();
        //    txtGuvenlikKodu.Text = "";
        //    return;
        //}

        //if (this.IsValid)
        //{
        //    try
        //    {
        //        string musteriAd = txtAd.Text; // ad
        //        string musteriSoyad = txtSoyad.Text; // soyad
        //        string email = txtEmail.Text; // email
        //        string sifre = txtSifre.Text; // şifre
        //        bool cepHaber = chkCepHaber.Checked;   // cep haber
        //        bool emailHaber = true; //chkEmailHaber.Checked;   // email haber
        //        string SehirKod = "";
        //        if (dpSehir.SelectedValue != "0")
        //            SehirKod = dpSehir.SelectedValue;

        //        DateTime dtDogumTarih = new DateTime(1753, 1, 1);
        //        if (dpDogumGun.SelectedValue != "0" && dpDogumAy.SelectedValue != "0" && dpDogumYil.SelectedValue != "0")
        //        {
        //            int _gun, _ay, _yil;

        //            if (int.TryParse(dpDogumGun.SelectedValue, out _gun) && int.TryParse(dpDogumAy.SelectedValue, out _ay) && int.TryParse(dpDogumYil.SelectedValue, out _yil))
        //            {
        //                dtDogumTarih = new DateTime(_yil, _ay, _gun);
        //            }

        //        }

        //        string cinsiyet;

        //        if (rbErkek.Checked)
        //            cinsiyet = "E";
        //        else
        //            cinsiyet = "K";

        //        int kullaniciID = bllUyelikIslemleri.MusteriKullaniciKayit(musteriAd, musteriSoyad, email, sifre, dtDogumTarih, cinsiyet /*Tanımsız*/, "TR" /*Tr*/, SehirKod, "", string.Empty, string.Empty, string.Empty, txtCepTelKod.Text, txtCepTel.Text, cepHaber, emailHaber, "GoldB2C", string.Empty, string.Empty, string.Empty);
        //        if (kullaniciID == 0)
        //        {
        //            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sistemde bir hata oluştu. Lütfen daha sonra yeniden deneyin."));
        //            CapchaGenerator();
        //            txtGuvenlikKodu.Text = "";
        //        }
        //        else if (kullaniciID == -1)
        //        {
        //            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Bu e-mail adresi sistemimize kayıtlıdır."));
        //            CapchaGenerator();
        //            txtGuvenlikKodu.Text = "";
        //        }
        //        else if (kullaniciID == -2)
        //        {
        //            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Tavsiye eden üyenin e-mail adresi sistemimize kayıtlı değildır. Lütfen kontrol ediniz."));
        //            CapchaGenerator();
        //            txtGuvenlikKodu.Text = "";
        //        }
        //        else
        //        {
        //            // kayıt başarılı
        //            // kullanıcıyı sisteme kayıt et
        //            Kullanici kullanici = bllUyelikIslemleri.KullaniciSistemKayit(kullaniciID);

        //            //#todo : sipariş aşamalarında carisi açılacak
        //            //bllUyelikIslemleri.CariVeriOlustur(user.CepTel, user.Tel, user.Cinsiyet, user.DogumTarihi, user.Email, user.ERPKod, user.IlceKod, user.MusteriAd, user.MusteriSoyad, user.RefEmail, user.SehirKod, user.UlkeKod, user.UyelikTarihi);

        //            // şifreyi e-mail ile gönder

        //            if (!string.IsNullOrEmpty(kullanici.Email))
        //            {
        //                string strHTML = File.ReadAllText(Server.MapPath("~/MailSablon/Hosgeldin.htm"));
        //                strHTML = strHTML.Replace("#adsoyad#", kullanici.KullaniciAd + " " + kullanici.KullaniciSoyad);
        //                string MailBanner = "";

        //                DataTable dtBanner = new DataTable();
        //                dtBanner = bllBanner.GetirBanner(3);
        //                if (dtBanner != null && dtBanner.Rows.Count > 0)
        //                {
        //                    MailBanner = dtBanner.Rows[0]["BannerHtml"].ToString();
        //                }

        //                string BannerImage = "";
        //                string BannerLink = "";
        //                string[] _MailBanner = MailBanner.Split(new string[] { "##" }, StringSplitOptions.None);
        //                if (_MailBanner.Length > 1)
        //                {
        //                    BannerImage = _MailBanner[0];
        //                    BannerLink = _MailBanner[1];
        //                }

        //                StringBuilder sb = new StringBuilder();
        //                sb.AppendLine("<tr>");
        //                sb.AppendLine("<td colspan=\"4\" width=\"720\" height=\"120\">");
        //                sb.AppendLine("<div style=\"height: 120px\">");
        //                if (!string.IsNullOrEmpty(BannerLink))
        //                    sb.AppendLine("<a href=\"" + BannerLink + "\">");
        //                sb.AppendLine("<img alt=\"kampanya\" src=\"http://www.motulteam.com/UrunResim/MarketingResim/banner/bilgimail/" + BannerImage + "\" style=\"border:none\"  />");
        //                if (!string.IsNullOrEmpty(BannerLink))
        //                    sb.AppendLine("</a>");
        //                sb.AppendLine("</div>");
        //                sb.AppendLine("</td>");
        //                sb.AppendLine("</tr>");

        //                if (string.IsNullOrEmpty(MailBanner))
        //                    strHTML = strHTML.Replace("#mailbanner#", "");
        //                else
        //                    strHTML = strHTML.Replace("#mailbanner#", sb.ToString());

        //                SendEmail mail = new SendEmail();
        //                mail.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
        //                mail.MailTo = new string[] { kullanici.Email };
        //                mail.MailHtml = true;
        //                mail.MailSubject = "motulteam.com Üyelik Onay";

        //                mail.MailBody = strHTML;
        //                bool mailSent = mail.SendMail(false);
        //            }

        //            // sonuç sayfasına yönlendir
        //            Response.Redirect("~/MusteriHizmetleri/Hosgeldiniz.aspx", false);
        //        }
        //    }
        //    catch
        //    {
        //        ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sistemde bir hata oluştu. Lütfen daha sonra yeniden deneyin."));
        //        CapchaGenerator();
        //        txtGuvenlikKodu.Text = "";
        //    }
        //}
        //else
        //{

        //    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen tüm alanları kontrol edip yeniden deneyin."));
        //    CapchaGenerator();
        //    txtGuvenlikKodu.Text = "";

        //}

    }

    public void CapchaGenerator()
    {
        Random RandomClass = new Random();
        RandomNumber = RandomClass.Next(1000, 999999).ToString();
        Session.Add("UyelikCaptcha", RandomNumber);

        string CapchaEncryptClearText = CustomConfiguration.getCapchaEncryptClearText;
        string CapchaEncryptPassword = CustomConfiguration.getCapchaEncryptPassword;
        string ens2 = CaptchaDotNet2.Security.Cryptography.Encryptor.Encrypt(RandomNumber, CapchaEncryptClearText, Convert.FromBase64String(CapchaEncryptPassword));
        imgCaptcha.ImageUrl = "~/Common/Components/Captcha.ashx?w=100&h=40&c=" + ens2;

    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">Popup('" + mesaj + "');</script>";
    }

    protected void CustomValidatorSozlesme_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = chkUyelikSozlesmesi.Checked;
    }
}