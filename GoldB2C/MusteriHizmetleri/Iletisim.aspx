﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="Iletisim.aspx.cs" Inherits="MusteriHizmetleri_Iletisim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">İletişim</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
    <span class="page-heading-title2">İLETİŞİM</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <script src="/assets/js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/assets/lib/fancyBox/jquery.fancybox.css" />
    <script type="text/javascript" src="/assets/lib/fancyBox/jquery.fancybox.js"></script>
    <!-- popup -->
    <script type="text/javascript">
        function Popup(message) {
            if ($('#popup').length > 0 && $('#popupContent').length > 0) {
                $("#popupContent").html(message).promise().done(function () {
                    $('#popup').fancybox().trigger('click');
                });
            }
        }
    </script>
    <a id="popup" class="fancybox" href="#popupContent" style="display: none"></a>
    <div id="popupContent" style="display: none">
    </div>
    <!-- popup -->
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtCepTelKod.ClientID %>").mask("599");
            $("#<%=txtCepTel.ClientID %>").mask("9999999");
        });
    </script>
    <script type="text/javascript">
        $(document).on('keyup blur', '#<%=txtSoru.ClientID %>', function () {
            var maxlength = "1000";
            var val = $(this).val();
            if (val.length > maxlength) {
                $(this).val(val.slice(0, maxlength));
            }
        });
    </script>
    <style type="text/css">
        #contact .contact-form-box .form-selector {
            padding-bottom: 5px;
        }
    </style>
    <div id="contact" class="page-content page-contact">
        <div id="message-box-conact">
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h3 class="page-subheading">
                    İLETİŞİM FORMU</h3>
                <div class="contact-form-box">
                    Öneri ve Yorumlarınız bizim için değerli <br /> <br />
                    <div class="form-selector">
                        <label>
                            Adınız</label>
                        <asp:TextBox ID="txtAd" CssClass="form-control input-sm" runat="server"></asp:TextBox>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqAd" runat="server" ControlToValidate="txtAd" ValidationGroup="grpKullaniciBilgileri"
                                ErrorMessage="Adınızı girin."></asp:RequiredFieldValidator></label>
                    </div>
                    <div class="form-selector">
                        <label>
                            Soyadınız</label>
                        <asp:TextBox ID="txtSoyad" CssClass="form-control input-sm" runat="server"></asp:TextBox>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqSoyad" runat="server" ControlToValidate="txtSoyad"
                                ValidationGroup="grpKullaniciBilgileri" ErrorMessage="Soyadınızı girin."></asp:RequiredFieldValidator></label>
                    </div>
                    <div class="form-selector">
                        <label>
                            E-Posta Adresiniz</label>
                        <asp:TextBox ID="txtEmail" CssClass="form-control input-sm" runat="server"></asp:TextBox>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqEmail" runat="server" ControlToValidate="txtEmail"
                                ValidationGroup="grpKullaniciBilgileri" ErrorMessage="E-Posta adresinizi girin."
                                Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rqEmailExp" runat="server" ErrorMessage="Lütfen geçerli bir E-Posta adresi girin."
                                ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ValidationGroup="grpKullaniciBilgileri" Display="Dynamic"></asp:RegularExpressionValidator></label>
                    </div>
                    <div class="form-selector">
                        <label>
                            Cep Telefonu Numaranız</label>
                        <br />
                        <asp:TextBox ID="txtCepTelKod" CssClass="form-control input-sm inlineBlock" Width="50px"
                             runat="server" type="tel"></asp:TextBox>
                        <asp:TextBox ID="txtCepTel" CssClass="form-control input-sm inlineBlock" Width="170px"
                          runat="server"  type="tel"></asp:TextBox>
                        <br />
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqtxtCepTelKod" runat="server" ControlToValidate="txtCepTelKod"
                                ErrorMessage="Cep Telefonu kodunuzu girin." ValidationGroup="grpKullaniciBilgileri"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rqtxtCepTel" runat="server" ControlToValidate="txtCepTel"
                                ErrorMessage="Cep Telefonu numaranızı girin." ValidationGroup="grpKullaniciBilgileri"></asp:RequiredFieldValidator></label>
                    </div>
                    <div class="form-selector">
                        <label>
                            Öneri veya Sorunuz</label>
                        <asp:TextBox ID="txtSoru" CssClass="form-control input-sm" runat="server" TextMode="MultiLine"
                            MaxLength="1000"></asp:TextBox>
                        <span style="font-style: italic; font-size: 10px;">(En fazla 1000 karakter)</span>
                        <label class="fright notification">
                            <asp:RequiredFieldValidator ID="rqAdres" runat="server" ControlToValidate="txtSoru"
                                ValidationGroup="grpKullaniciBilgileri" ErrorMessage="Öneri veya Sorunuzu girin."></asp:RequiredFieldValidator></label>
                    </div>
                    <div class="form-selector">
                        <asp:LinkButton ID="btnKaydet" CssClass="btn btnInput" runat="server" ValidationGroup="grpKullaniciBilgileri"
                            OnClick="btnKaydet_Click"><span>Gönder</span></asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6" id="contact_form_map" style="display:none;">
                <h3 class="page-subheading">
                    İLETİŞİM BİlGİLERİMİZ</h3>
                <p>
                    Genel Müdürlük ve İç Anadolu Bölge Satış Müdürlüğü
                    <br />
                    Adres: Dalsan Alçı Sanayi ve Ticaret A.Ş. 1. cadde, Sincap sokak No: 12, Büyüksanayi,
                    06060, Ankara TÜRKİYE
                </p>
                <br />
                <ul class="store_info">
                    <li><i class="fa fa-home"></i>Dalsan Alçı Sanayi ve Ticaret A.Ş. 1. cadde, Sincap sokak
                        No: 12, Büyüksanayi, 06060, Ankara TÜRKİYE</li>
                    <li><i class="fa fa-phone"></i>Tel: <span>(90) 312 303 49 49 </span></li>
                    <li><i class="fa fa-phone"></i>Fax: <span>(90) 312 341 25 69 </span></li>
                    <li><i class="fa fa-envelope"></i>E-Posta: <span><a href="">
                        </a></span></li>
                </ul>
                <img src="../ImagesGld/kroki.jpg" width="300" style="float: left" />
            </div>
        </div>
    </div>
</asp:Content>
