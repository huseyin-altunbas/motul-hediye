﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="KampanyaKurallari.aspx.cs" Inherits="MusteriHizmetleri_KampanyaKurallari" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Kampanya Kuralları</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
    <span class="page-heading-title2">KAMPANYA KURALLARI</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
   
    <strong><p>NASIL HEDİYE SE&Ccedil;EBİLİRİM?</p></strong> 
<p>Motul da puanlarınız ile her ay g&uuml;ncellenen ve yeni &uuml;r&uuml;nlerin eklendiği https://motulteam.com/&nbsp; web portalındaki hediye kataloğu &uuml;zerinden istediğiniz hediyeyi sipariş edebilirsiniz.</p>
<br /><p><strong> HEDİYENİZİ NASIL ALACAKSINIZ?</strong> </p>
<p>Siparişiniz sonrasında Motul tarafından onaylanan hediyeleriniz kargo ile belirttiğiniz adrese ulaştırılacaktır. Siparişinizin takibini https://motulteam.com/ web portalı &uuml;zerinden, &ldquo;Siparişlerim&rdquo; b&ouml;l&uuml;m&uuml;nden ger&ccedil;ekleştirilebilirsiniz. Hediyelere ait kargo bedelleri Motul tarafından karşılanacaktır.</p>
<p></p><br />
<p> <strong>DİKKAT EDİLECEK HUSUSLAR</strong></p><p></p><br />
   
 
<ol>
<li> <strong>1- </strong>Motul tarafından hazırlanan &ldquo;Motul&rdquo; kampanyası 03.01.2019-31.12.2019 tarihleri arasında ge&ccedil;erli olup, Motul &ouml;nceden bildirim yapmadan ve herhangi bir neden g&ouml;stermeden bu kampanyayı durdurabilir veya uzatabilir.</li>
<li> <strong>2- </strong>G&ouml;nderilen t&uuml;m hediyelerin garanti belgeleri orijinal kutularının i&ccedil;erisinde veya &uuml;zerinde mevcuttur. Garanti belgeleri sonradan temin edilen evrak olmadığından dolayı, hediye paketini a&ccedil;tıktan sonra i&ccedil;inin ve dışının dikkatlice kontrol edilmesi &ouml;nemlidir.</li>
<li> <strong>3- </strong>&Uuml;r&uuml;n&uuml;n orijinal kutusu i&ccedil;inden &ccedil;ıkan Garanti Belgesi ve irsaliyenin atılmaması gerekmektedir. Bu belgeler garanti s&uuml;resi boyunca yaşayabileceğiniz arıza durumlarında yetkili servislere ibraz edilmesi gereken belgelerdir. Teslim aldığınız &uuml;r&uuml;nlerin arızalı olması durumunda, &uuml;r&uuml;n kullanım kılavuzundaki talimatlara g&ouml;re hareket edilmeli ve ilgili &uuml;r&uuml;n&uuml;n Yetkili Servisi'ne başvuruda bulunulmalıdır.</li>
<li> <strong>4-</strong>&Uuml;r&uuml;nlerin garanti s&uuml;releri irsaliye tarihi ile başlar; &uuml;reticinin belirlediği garanti s&uuml;resi ile sona erer.</li>
<li> <strong>5- </strong>Beyaz Eşya gibi ağır &uuml;r&uuml;nlerin, kargo taşımacılığı apartman kapısına kadar yapılmaktadır. Ev veya iş yeri i&ccedil;erisine veya bulunduğu kata teslim yapılmamaktadır.</li>
<li> <strong>6- </strong>Beyaz Eşya, TV gibi kurulum gerektiren &uuml;r&uuml;nlerin Yetkili Servis tarafından kurulumunun yapılması gerektiğinden, kurulum gerektiren &uuml;r&uuml;nler i&ccedil;in, &uuml;r&uuml;n alındıktan sonra (orijinal ambalajı a&ccedil;ılmadan) ilgili servis hattının aranması gerekmektedir.</li>
<li> <strong>7- </strong>Yetkili Servis tarafından a&ccedil;ılmayan ve kurulumu yapılmayan bu t&uuml;r &uuml;r&uuml;nler (6. Madde) garanti kapsamı dışında işlem g&ouml;receğinden iadesi kabul edilmemektedir. Bu konuda detaylı bilgiyi ilgili &uuml;r&uuml;n&uuml;n &uuml;reticisinden alabilirsiniz.</li>
<li> <strong>8- </strong>Hediye Kataloğunda yer alan &uuml;r&uuml;nler stoklarla sınırlıdır. Se&ccedil;tiğiniz hediyenin teknik nedenlerden dolayı sağlanamaması ya da stoklarının t&uuml;kenmesi durumunda eş değerde bir hediye sunulacak veya https://motulteam.com/ portalindeki hediye kataloğu &uuml;zerinden yeni bir se&ccedil;im yapma imkanı sağlanacaktır.</li>
<li> <strong>9- </strong>Se&ccedil;ilen hediyeler Tutkal Bilişim tarafından temin edildikten sonra herhangi bir değişiklik yapılamaz. Se&ccedil;tiğiniz hediyenin herhangi bir nedenle sağlanamaması durumunda, benzer &ouml;zellikleri taşıyan muadil &uuml;r&uuml;n g&ouml;nderilir.</li>
<li> <strong>10- </strong> 6. Madde&rsquo; de belirtilenler haricindeki &uuml;r&uuml;nler i&ccedil;in, teslimat sırasında, kargo yetkilisinden &uuml;r&uuml;n&uuml; teslim almadan &ouml;nce &uuml;r&uuml;n paketinin a&ccedil;ılarak hediyenizin sağlamlığı ve hasarlı olup olmadığı kontrol edilmelidir. Herhangi bir hasar tespit edildiğinde, &uuml;r&uuml;n&uuml; teslim eden kargo firması tarafından tutulacak hasar tespit tutanağı ile birlikte &uuml;r&uuml;n iade edilmelidir. Teslim alındıktan sonra fark edilen hasarlı veya eksik &uuml;r&uuml;nlerin sorumluluğu Motul ve/veya Tutkal Bilişim&rsquo; e ait değildir.</li>
<li> <strong>11- </strong>Hediye puanlar nakde &ccedil;evrilemez.</li>
<li> <strong>12- </strong>Se&ccedil;ilen hediyelerin teslim adresleri T&uuml;rkiye Cumhuriyeti sınırları i&ccedil;erisinde olmalıdır.</li>
<li> <strong>13- </strong>Hediyelerin teslimat s&uuml;resi 10 iş g&uuml;n&uuml;d&uuml;r.</li>
<li> <strong>14- </strong>Her t&uuml;rl&uuml; anlaşmazlık halinde Motul ve Tutkal Bilişim&rsquo; in kayıtları kesin delil olarak esas alınacaktır.</li>
<li> <strong>15- </strong>Katalog baskısında oluşabilecek Tipografik ve baskı hatalarından kaynaklanan sorunlardan dolayı Motul ve/veya Tutkal Bilişim sorumlu değildir.</li>
<li> <strong>16- </strong>G&ouml;rseller temsilidir. G&uuml;ncel stok ve puan bilgilerini https://motulteam.com/ web sitemizden inceleyebilirsiniz.</li>
<li> <strong>17- </strong>&Uuml;r&uuml;nler stoklarla sınırlıdır. Anadoludanaspara.com&rsquo; un &ouml;nceden haber vermeden &uuml;r&uuml;nleri ve &uuml;r&uuml;n puanlarını değiştirme hakkı saklıdır.</li>
<li> <strong>18- </strong>https://motulteam.com/ web portalındaki hediye kataloğundan hediye siparişi verilmesi, burada yer alan t&uuml;m kampanya şartlarının kabul&uuml; anlamına gelir.</li>
</ol>
<p>&nbsp;</p>
<p><strong>TATİL KATILIM ŞARTLARI</strong></p>
<p>* Tatil taleplerinin tatil başlamadan en az 45 g&uuml;n &ouml;nceden belirtilmesi gerekmektedir.</p>
<p>* Tatil tarihlerinde talep edenin istediği tarih &ouml;ncelikli olmakla birlikte, anlaşmalar &ccedil;er&ccedil;evesinde tur şirketi tarafından farklı tarihler &ouml;nerilebilir.</p>
<p>* Tatil taleplerinde iptal m&uuml;mk&uuml;n değildir.</p>
<p>* Yurtdışı tatillerde vize, alan vergileri, yurt dışı &ccedil;ıkış har&ccedil;ları vs. ekstra &ouml;demeler ve işlemler, tatil talebinde bulunan kişi/kişiler'e aittir. Bunlardan dolayı doğabilecek sorunlarda tatil talebi iptal edilemez.</p>
<p>* Yurtdışı tatillerde ulaşım &uuml;creti puanlara dahil olup, yurti&ccedil;i tatillerde ulaşım ise isteğe bağlı olarak değişmektedir.</p>
<p>* Tatil talepleri yılbaşı, Noel, Paskalya ve benzeri dini ve milli bayram tarihleri haricinde kullanılabilir.</p><br />
<p><strong>ARA&Ccedil; BİLGİLENDİRME</p></strong>
<p>* Motorlu taşıtlarda teslim tarihindeki g&uuml;ncel fiyatlar ge&ccedil;erlidir.</p>
<p>* &Uuml;r&uuml;n puanı verildiği g&uuml;n itibari ile ve stoklarla sınırlıdır, gelecek vergi ve fiyat artışları sipariş ge&ccedil;en &uuml;ye tarafından karşılanacaktır.</p>
<p>* Plaka, ruhsat, muameleci vb. masraflar aracı alan kişiye aittir.</p>
<p>&nbsp;</p>
<p><strong>Kargo teslimat şartları</strong></p>
<p>Değerli &Uuml;yemiz,</p>
<p>Hediye &uuml;r&uuml;n teslimatlarında; kargo teslim fişini imzalamadan &ouml;nce &uuml;r&uuml;n&uuml;n kargo kolisinde herhangi bir hasar olup olmadığını kontrol ediniz. Kargo kolisi hasarlı olan hediyelerinizi teslim almayıp, kargo g&ouml;revlisine iade etmenizi rica ederiz.</p>
<p>&mdash; Beyaz Eşya gibi ağır &uuml;r&uuml;nlerin, kargo taşımacılığı apartman kapısına kadar yapılmaktadır. Kata teslim, ev veya iş yeri i&ccedil;erisine teslim yapılmamaktadır.</p>
<p>&mdash; &Uuml;r&uuml;n&uuml;n orijinal ambalajı i&ccedil;erisinde, &uuml;r&uuml;nle ilgili eksiklik veya hasar varsa aynı g&uuml;n https://motulteam.com/ adresindeki hediye sitemizin &ldquo;İletişim&rdquo; sayfasında bulunan iletişim formunu doldurarak iletmenizi rica ederiz. Aksi halde &uuml;r&uuml;nle ilgili iade ve değişim yapılmamaktadır.</p>
<p>&mdash; Beyaz Eşya, TV gibi kurulum gerektiren &uuml;r&uuml;nlerin Yetkili Servis tarafından kurulumunun yapılması gerekmektedir. Yetkili Servis tarafından a&ccedil;ılmayan ve kurulumu yapılmayan &uuml;r&uuml;nler garanti kapsamı dışında işlem g&ouml;receğinden iadesi kabul edilmemektedir. Bu konuda detaylı bilgiyi ilgili &uuml;r&uuml;n&uuml;n &uuml;reticisinden alabilirsiniz.</p>
<p>&mdash; &Uuml;r&uuml;n&uuml;n Garanti Belgesi'nin atılmaması gerekmektedir. Teslim aldığınız &uuml;r&uuml;nlerin &ccedil;alışmaması durumunda, &uuml;r&uuml;n kullanım kılavuzundaki talimatlara g&ouml;re hareket etmenizi rica ederiz. &Ccedil;alışmayan &uuml;r&uuml;nler i&ccedil;in ilgili &uuml;r&uuml;n&uuml;n Yetkili Servisi'ne başvuruda bulunabilirsiniz.</p>
<p>Hediyenin talep ettiğiniz hediyeden farklı &ccedil;ıkması durumunda kargoyu almış olduğunuz tarihten itibaren 14 g&uuml;n i&ccedil;inde orijinal ambalajıyla ve iade sebebini belirterek aşağıdaki adrese g&ouml;ndermeniz gerekmektedir;</p>
<p><span style="text-decoration: underline;">Not: Arızalı &ccedil;ıkan hediyeler i&ccedil;in l&uuml;tfen ilgili firmanın Yetkili Servisi'ni arayınız.</span></p>




</asp:Content>
