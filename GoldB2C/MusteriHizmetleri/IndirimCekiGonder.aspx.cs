﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using Goldb2cEntity;
using System.Text;
using Goldb2cInfrastructure;
using System.IO;
using System.Net;
using System.Net.Mail;

public partial class MusteriHizmetleri_IndirimCekiGonder : System.Web.UI.Page
{
    CekIslemleriBLL bllCekIslemleri = new CekIslemleriBLL();
    Kullanici kullanici;
    private static object lockObjectCek = new Object();
    int FirmaKod;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {
            if (Request.QueryString["FirmaKod"] != null)
            {
                kullanici = (Kullanici)Session["Kullanici"];
                Int32.TryParse(Request.QueryString["FirmaKod"].ToString(), out FirmaKod);
                if (FirmaKod > 0)
                {
                    DataTable dtFirma = bllCekIslemleri.IndirimCekleriFirmaGetir(FirmaKod);
                    if (dtFirma.Rows.Count > 0 && dtFirma.Rows[0]["FirmaIndirimAcik"] != DBNull.Value && dtFirma.Rows[0]["FirmaIndirimAcik"] != null && Convert.ToBoolean(dtFirma.Rows[0]["FirmaIndirimAcik"]))
                    {
                        if (dtFirma.Rows[0]["FirmaAciklama"] != DBNull.Value && dtFirma.Rows[0]["FirmaAciklama"] != null)
                        {
                            lblFirmaAciklama.Text = dtFirma.Rows[0]["FirmaAciklama"].ToString();
                        }
                        else
                        {
                            lblFirmaAciklama.Text = string.Empty;
                        }

                        rptIndirimCekleri.DataSource = bllCekIslemleri.IndirimCekleriKullaniciGetir(kullanici.KullaniciId, FirmaKod);
                        rptIndirimCekleri.DataBind();
                    }
                    else
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
        }
        else
        {
            rptIndirimCekleri.DataSource = null;
            rptIndirimCekleri.DataBind();

            Response.Redirect("/");
        }

    }

    protected void btnGonder_Click(object sender, EventArgs e)
    {
        lock (lockObjectCek)
        {
            if (Session["Kullanici"] != null)
            {
                if (txtEmailAdresiniz.Text != string.Empty && IsValidMail(txtEmailAdresiniz.Text.Trim()))
                {
                    string emailAdres = InputSafety.SecureString(txtEmailAdresiniz.Text.Trim());
                    kullanici = (Kullanici)Session["Kullanici"];

                    if (bllCekIslemleri.IndirimCekleriKullaniciGetir(kullanici.KullaniciId, FirmaKod).Rows.Count == 0)
                    {
                        int sonuc = bllCekIslemleri.IndirimCekleriKullaniciKaydet(kullanici.KullaniciId, emailAdres, FirmaKod);
                        if (sonuc > 0)
                        {
                            DataTable dt = bllCekIslemleri.IndirimCekleriKullaniciGetir(kullanici.KullaniciId, FirmaKod);

                            rptIndirimCekleri.DataSource = dt;
                            rptIndirimCekleri.DataBind();

                            bool mailSonuc = IndirimCekiMailGonder(dt.Rows[0]["CekNo"].ToString(), emailAdres, kullanici.KullaniciAd, kullanici.KullaniciSoyad);
                            if (mailSonuc)
                                Page.ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("İndirim çeki kaydedildi.Ayrıca mail adresinize de bilgi maili gönderildi."));
                            else
                                Page.ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("İndirim çeki kaydedildi.Ancak mail adresinize gönderiminde bir sorun oluştu."));
                        }
                        else if (sonuc == 0)
                            Page.ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Hata ile karşılaşıldı.Daha sonra tekrar deneyiniz."));
                        else if (sonuc == -1)
                            Page.ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sadece bir İndirim hediye çeki alabilirsiniz."));
                        else if (sonuc == -2)
                            Page.ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("İndirim çeki tanımlanamadı.Daha sonra tekrar deneyiniz."));
                    }
                    else
                        Page.ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sadece bir indirim hediye çeki alabilirsiniz."));
                }
                else
                    Page.ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen geçerli ve kullandığınız bir email adresi giriniz."));
            }
            else
            {
                Response.Redirect("http://www.alcikart.com.tr/?returnPath=http://www.motulteam.com");
            }

        }
    }

    private string MesajScriptOlustur(string Mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + Mesaj + "</td></tr></table></div>');</script>";
    }

    public static bool IsValidMail(string emailaddress)
    {
        try
        {
            MailAddress m = new MailAddress(emailaddress);
            return true;
        }
        catch (FormatException)
        {
            return false;
        }
    }

    public bool IndirimCekiMailGonder(string cekNo, string email, string kullaniciAd, string kullaniciSoyad)
    {
        try
        {
            string strHTML = File.ReadAllText(Server.MapPath("~/MailSablon/IndirimCekleri.htm"));

            strHTML = strHTML.Replace("#adsoyad#", kullaniciAd + " " + kullaniciSoyad);
            strHTML = strHTML.Replace("#cekno#", cekNo);

            DataTable dtIndirim = bllCekIslemleri.IndirimCekleriFirmaGetir(FirmaKod);
            if (dtIndirim.Rows[0]["FirmaAd"] != DBNull.Value && dtIndirim.Rows[0]["FirmaAd"] != null)
                strHTML = strHTML.Replace("#firma#", dtIndirim.Rows[0]["FirmaAd"].ToString());

            SendEmail mail = new SendEmail();
            mail.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
            mail.MailTo = new string[] { email };
            mail.MailHtml = true;
            mail.MailSubject = "Motul Hediye sitesi aracılığıyla indirim hediye çeki.";

            mail.MailBody = strHTML;
            bool mailSent = mail.SendMail(false);

            if (mailSent)
                return true;
            else
                return false;

        }
        catch (Exception ex)
        {
            return false;
        }
    }
}