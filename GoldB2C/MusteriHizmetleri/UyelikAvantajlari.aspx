﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="UyelikAvantajlari.aspx.cs" Inherits="MusteriHizmetleri_UyelikAvantajlari" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Üyelik Avantajları
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    motulteam.com’ da üye olarak sadece üyelere sunduğumuz avantajlardan anında yararlanmaya
    başlayabilirsiniz. Düzenlenecek ortak kampanyalardan, promosyonlardan ilk siz haberdar
    olur ve size özel avantajlardan yararlanabilirsiniz.
    <br />
    <br />
    Size özel kampanya ürünlerini ve yine size özel garaj ürünlerini incelemek ve faydalanabilmek
    için üye girişi yapmanız yeterlidir. Bu bölümlerden sadece üyelerimiz yararlanabilmektedir.
    <br />
    <br />
    Ziyaretleriniz sırasında seçtiğiniz ürünleri kayıtlı sepetinizde tutabilir ve sonraki
    ziyaretlerinizde gözden geçirebilirsiniz. Bu sayede, ilginizi çeken ancak o anda
    satın almak istemediğiniz ürünleri sadece sizin erişebileceğiniz şekilde siz kayıtlı
    sepetinizi silene kadar saklanır.
    <br />
    <br />
    Siparişinizin hangi aşamada olduğunu takip edebilir, eski siparişlerinizi görüntüleyebilirsiniz.
</asp:Content>
