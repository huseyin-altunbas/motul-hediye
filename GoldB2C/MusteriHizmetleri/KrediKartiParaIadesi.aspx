﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="KrediKartiParaIadesi.aspx.cs" Inherits="MusteriHizmetleri_KrediKartiParaIadesi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Kredi Kartı Para İadesi
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <h3>
        DEĞERLİ MÜŞTERİLERİMİZİN DİKKATİNE ! ! !
    </h3>
    İade talebinde bulunmuş olduğunuz kredi kartı işlemleri bankalar birliğinin almış
    olduğu kararlar ve yasal prosedür gereği 3 iş günü içerisinde kart hamilinin kredi
    kartına iade edilmektedir. Bu süreç bankaların tüm üye işyerleri ve bireysel müşterilerine
    uygulamakta olduğu standart süreç olup; iade işlemleri aynı gün içerisinde kredi
    kartına geri yansıtılmamaktadır. Gün içerisinde yapılan işlemler iptal olarak yine
    gün içerisinde karta yansıtılır. Firmamız ulaşan iade talepleri 24 saat içerisinde
    gerçekleştirilmekte olup, iade sürecinin geri kalan kısmı ise banka tarafından devam
    etmekte ve sonuçlandırılmaktadır .İşleyiş ve iade süreci hakkında ayrıntılı bilgiyi
    tüm detayları ile birlikte bankaların müşteri hizmetleri servislerinden öğrenebilirsiniz.
    İade talebinde bulunmuş olduğunuz işlem;<br />
    ÖRN: 10/09/2007 tarihinde 60 TL lik 6 taksitli bir işlem gerçekleştirdiniz (10 TL
    X 6 Taksit)
    <br />
    <br />
    Ardından 12/09/2007 tarihinde bu işlemin iadesini talep ettiniz, işlem tutarı 60
    TL 6 taksit li bir işlemde kredi kartınıza 6 ay taksitli olarak geri yansıtılır.
    Kartınızın bakiyesi 60 TL bloke edilmiş olur ve aylık 10 TL banka tarafından bloke
    açılır duruma getirilir. İşlem taksitli işlem olduğundan işlemin aylık hesap ekstrenize
    yansıması ;
    <br />
    <br />
    İşlem Tarihi Dönem İçi İşlemler Kalan Borç/Taksit Tutar (TL)
    <br />
    10 Ekim 2007 Firma Ünvanı 10 X 6 -1. Taksit 10,00<br />
    10 Ekim 2007 Firma Ünvanı 10 X 6 -1 .taksit -10,00<br />
    <br />
    İptal işlemlerinde ise;
    <br />
    <br />
    Örn: 11.07.2011 tarihinde sipariş verildi, tahsilat gerçekleşti. Gün içerisinde
    işlemin iptali ile bilgi verildiği takdirde işlem aynı gün iptal edilir. Tutar kartına
    anında yansıtılır ve müşterimizin kartından tutar bloke olmaz. İşlemin taksitli
    ya da tek çekim olması önemli değil. Yani limitinizden düşürülen 60 TL yeniden aktif
    hale getirilir ve bu tutar kadar alış-veriş yapmanıza banka tarafından yeniden imkan
    tanınır.
</asp:Content>
