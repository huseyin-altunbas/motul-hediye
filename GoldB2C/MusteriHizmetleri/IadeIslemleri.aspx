﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="IadeIslemleri.aspx.cs" Inherits="MusteriHizmetleri_IadeIslemleri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    İade İşlemleri
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    Siparişinizdeki ürünleri veya ürünü iade etmek istediğinizde uyulması gerekli ana
    kural aldığınız ürünü teslimat tarihinden itibaren 3 iş günü içerisinde motulteam.com’
    un düzenlemiş olduğu faturanız ve iade sebebinizi içeren bir not eki ile iade etmenizdir.
    (Ürünün iade sebebini belirten bir notu faturanın yanına koymanız büyük önem arz
    etmektedir.) İade etmek istediğiniz ürünün faturası kurumsal ise, geri iade ederken
    kurumun düzenlemiş olduğu iade faturası ile birlikte göndermeniz gerekmektedir.
    İade faturası, kargo payı dahil edilmeden ( ürün birim fiyatı + KDV şeklinde ) kesilmelidir.
    Faturası kurumlar adına düzenlenen sipariş iadeleri İADE FATURASI kesilmediği takdirde
    tamamlanamayacaktır.
    <br />
    <br />
    Genel iade şartları aşağıdaki gibidir;
    <br />
    <ul style="margin-left: 20px">
        <li>İadeler mutlak surette orijinal kutu veya ambalajı ile birlikte yapılmalıdır.</li>
        <li>Orijinal kutusu/ambalajı bozulmuş (örnek: orijinal kutu üzerine kargo etiketi yapıştırılmış
            ve kargo koli bandı ile bantlanmış ürünler kabul edilmez) tekrar satılabilirlik
            özelliğini kaybetmiş, başka bir müşteri tarafından satın alınamayacak durumda olan
            ürünlerin iadesi kabul edilmemektedir.</li>
        <li>İade etmek istediğiniz ürün ile birlikte orijinal fatura (sizdeki bütün kopyaları)
            ve iade sebebini içeren bir dilekçe göndermeniz gerekmektedir.</li>
        <li>İade etmek istediğiniz ürün / ürünler ayıplı ise kargo ücreti firmamız tarafından
            karşılanmaktadır. Bu durumda da YURTİÇİ Kargo ile gönderim yapmanız gerekir. Diğer durumlarda
            ise kargo ücreti size aittir.</li>
        <li>Ürün seçiminde uyumsuzluk problemi yaşanabilecek ürünler için, mutlaka uyum konusunda
            teknik destek alarak sipariş vermelisiniz. Bu tip ürünlerin orijinal ambalajları
            açıldığında iade işlemleri mümkün olmamaktadır. (Örn : hafıza kartları, bilgisayar
            donanım ve yazılım ürünleri )</li>
    </ul>
    <br />
    Özel Ürünler için iade şartları
    <br />
    <br />
    <h3>
        TELEFON iadesinde;
    </h3>
    *Ürün ekranında yer alan koruma bandı çıkarılmamış olmalıdır. Ekranda ve telefonun
    diğer yerlerinde çizik, hasar vs. olmamalıdır. *Bütün standart aksesuarları hasarsız
    ve tam olarak orijinal paketleri ile birlikte gönderilmelidir.
    <br />
    <br />
    Diğer Ürünlerin iadesinde; Niteliği itibarıyla iade edilemeyecek ürünler ( ürünün
    arızalı veya ayıplı çıkması halleri dışında, açıldıktan sonra sağlık açısından tehlike
    arz edebilen ürünler örn : kullanım esnasında vücut ile birebir temas gerektiren
    ürünler,kulak içi veya kulak üstü kulaklık vs. ), tek kullanımlık ürünler, kopyalanabilir
    yazılım ve programlar, hızlı bozulan veya son kullanım tarihi geçen ürünler ve iadesi
    mümkün değildir.
    <br />
    <br />
    Aşağıdaki ürünlerin iade edilebilmesi, ürünün ambalajının açılmamış, bozulmamış
    ve ürünün kullanılmamış olması şartına bağlıdır.
    <br />
    <br />
    *DVD, VCD, CD ve kasetler *Taşınabilir bilgisayarlar *Bilgisayar ve kırtasiye sarf
    malzemeleri (toner, kartuş, şerit v.b) *Her türlü kozmetik ürünleri, kişisel bakım,
    sağlık ve güzellik ürünleri
    <br />
    <br />
    <h3>
        İade İşlemleri
    </h3>
    Gelen ürün öncelikle İade Bölümü tarafından incelenir, gerekir ise ürünün yetkili
    servisine ya da tedarikçi firmaya test etmeleri için gönderilir. Yukarıdaki şartlara
    uygun ise iade işlemi başlatılır. Bu işlemin süresi 3 ile 7 gün arasında değişmektedir.
    İade işlemi onaylandığında size telefon açılarak bilgi verilecek ve isteğinize bağlı
    olarak yeni bir sipariş verebilmeniz için değişim belgesi , farklı ürün alınabilir
    belgesi yada para iadesi yapılacaktır.
</asp:Content>
