﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="TeslimatFaturaBilgisi.aspx.cs" Inherits="MusteriHizmetleri_TeslimatFaturaBilgisi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Teslimat/Fatura Bilgisi
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    Teslimat adresi siparişinizin kargo tarafından teslim edileceği adrestir. Her siparişinizde
    teslimat adresinizi tekrar girmek zorundasınız. Sistemimiz tarafından teslimat adresleriniz
    kayıt altına alınmamaktadır. Sadece fatura adresiniz üyelik bilgi formumuzda kayıtlıdır.
    Daha önce sipariş vermediyseniz kayıtlı bir adresiniz olmayacaktır. Teslim alacak
    kişi bölümü, paketlerinizin kargo personeli tarafından teslim edileceği kişidir
    ve kargo personeli bu kişi haricinde kargo paketlerinizi güvenlik açısından başka
    kimseye teslim etmeyecektir. Kaydetmiş olduğunuz fatura adreslerinizi Hesabım bölümünden
    değiştirebilir ya da silebilirsiniz.
    <br />
    <br />
    <b>Önemli Not:</b> Teslimat adresi olarak lütfen kargo şubelerinin adreslerini yazmayınız.
    Bu tarz toplu mal teslimi yapılan adreslerden siparişlerinizin teslimatı yapılmamaktadır.
</asp:Content>
