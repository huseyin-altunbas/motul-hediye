﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="Hosgeldiniz.aspx.cs" Inherits="MusteriHizmetleri_Hosgeldiniz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Hoş geldiniz</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
    <span class="page-heading-title2">HOŞ GELDİNİZ</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <div class="mt10">
        <div>
            Sayın
            <asp:Label ID="lblKullaniciAdSoyad" CssClass="strong" runat="server" Text=""></asp:Label>
            &nbsp;Motul Hediye’yea hoş geldiniz.
        </div>
        <div>
            <a href="#"><span class="colord8 tdunder strong">Hesabım</span></a>
            bölümünden kullanıcı bilgilerinizi güncelleyebilirsiniz veya <a href="/Default.aspx">
                <span class="colord8 tdunder strong">Alışverişe Devam</span></a> edebilirsiniz.
        </div>
    </div>
</asp:Content>
