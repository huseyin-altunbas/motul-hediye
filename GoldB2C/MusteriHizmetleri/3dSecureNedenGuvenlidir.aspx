﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="3dSecureNedenGuvenlidir.aspx.cs" Inherits="MusteriHizmetleri_3dSecureNedenGuvenlidir" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    3-D Secure Neden Güvenlidir?
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    3-D Secure sisteminde , sanal ödeme işlemi gerçekleşirken, banka tarafından kart
    sahibine sadece kendisinin bildiği ödeme şifresi sorulmakta ve kart sahibinin kimliği
    doğrulanmaktadır.Yetkisiz kişilerin kartlarını internet ortamında kullanılmasını
    engellemektedir.
    <br />
    <br />
    Motul Hediye çok yakın bir zamanda Türkiye’nin büyük bankalarının yapmış
    olduğu çalışmalar tamamlanır tamamlanmaz tüm bankaların 3-D Secure hizmetleri sitemizde
    siz müşterilerimizin güvenliği için devrede olacaktır. Şu anda Garanti Bankasının
    kredi kartları ile 3-D Secure alışverişler sistemimizde tanımlıdır. Kısa süre içerisinde
    Akbank,Yapı Kredi,Türkiye İş Bankası kredi kartlarına şifre tanımlaması yapılarak
    müşterilerimizden şifre 3-D Secure sistemiyle şifre sorgulaması sistemine geçilecektir.
</asp:Content>
