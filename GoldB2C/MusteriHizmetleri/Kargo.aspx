﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="Kargo.aspx.cs" Inherits="MusteriHizmetleri_Kargolama" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Kargolama
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    Kargo bölümünde siparişinizi size ulaştıracak kargo firması UPS kargodur. Kampanyalı
    alışverişler de kargo ücreti müşteri tarafından ödenmektedir.
    <ul style="margin: 20px">
        <li>Siparişinizin kurye tarafından size ulaştırılmasını istediğiniz taktirde İstanbul
            içi siparişleriniz Kuryetel (www.kuryetel.com) tarafından en fazla 3 saat içinde
            kapınızda olacaktır.</li>
        <li>Kurye ücreti alıcı tarafından ürün teslimi sırasında yapılmaktadır.</li>
        <li>Kurye ücreti teslimatın yapılacağı yere göre değişiklik göstermektedir.</li>
        <li>Kurye ücreti hakkında detaylı bilgi almak için 444 9 000 numaralı telefonu arayabilirsiniz
            yada www.kuryetel.com web sitesini ziyaret edebilirsiniz.</li>
    </ul>
</asp:Content>
