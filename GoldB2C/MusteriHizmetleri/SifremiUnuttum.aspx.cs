﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using System.Text;
using System.IO;
using Goldb2cInfrastructure;

public partial class MusteriHizmetleri_SifremiUnuttum : System.Web.UI.Page
{
    BannerBLL bllBanner = new BannerBLL();
    UyelikIslemleriBLL bllUyeIslemleri = new UyelikIslemleriBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Kapalı..
        Response.Redirect("~/Default.aspx", false);

        //if (Session["Kullanici"] != null)
        //    Response.Redirect("~/Default.aspx", false);

        //txtEmailTekrar.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnSifremiGonder.ClientID + "').click();return false;}} else {return true;} ");

    }

    protected void btnSifremiGonder_Click(object sender, EventArgs e)
    {
        try
        {
            string Email = "";
            Email = txtEmail.Text.Trim();
            if (!string.IsNullOrEmpty(Email))
            {
                DataRow dr = bllUyeIslemleri.SifremiUnuttum(Email);
                if (dr != null)
                {
                    string MailBanner = "";

                    DataTable dtBanner = new DataTable();
                    dtBanner = bllBanner.GetirBanner(3);
                    if (dtBanner != null && dtBanner.Rows.Count > 0)
                    {
                        MailBanner = dtBanner.Rows[0]["BannerHtml"].ToString();
                    }

                    string strHTML = File.ReadAllText(Server.MapPath("~/MailSablon/SifremiUnuttum.htm"));

                    // şifreyi e-mail ile gönder
                    strHTML = strHTML.Replace("#adsoyad#", dr["KullaniciAd"].ToString() + " " + dr["KullaniciSoyad"]);
                    strHTML = strHTML.Replace("#email#", dr["Email"].ToString());
                    strHTML = strHTML.Replace("#sifre#", dr["Sifre"].ToString());

                    string BannerImage = "";
                    string BannerLink = "";
                    string[] _MailBanner = MailBanner.Split(new string[] { "##" }, StringSplitOptions.None);
                    if (_MailBanner.Length > 1)
                    {
                        BannerImage = _MailBanner[0];
                        BannerLink = _MailBanner[1];
                    }

                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("<tr>");
                    sb.AppendLine("<td colspan=\"4\" width=\"720\" height=\"120\">");
                    sb.AppendLine("<div style=\"height: 120px\">");
                    if (!string.IsNullOrEmpty(BannerLink))
                        sb.AppendLine("<a href=\"" + BannerLink + "\">");
                    sb.AppendLine("<img alt=\"kampanya\" src=\"http://www.motulteam.com/UrunResim/MarketingResim/banner/bilgimail/" + BannerImage + "\" style=\"border:none\"  />");
                    if (!string.IsNullOrEmpty(BannerLink))
                        sb.AppendLine("</a>");
                    sb.AppendLine("</div>");
                    sb.AppendLine("</td>");
                    sb.AppendLine("</tr>");

                    if (string.IsNullOrEmpty(MailBanner))
                        strHTML = strHTML.Replace("#mailbanner#", "");
                    else
                        strHTML = strHTML.Replace("#mailbanner#", sb.ToString());

                    SendEmail mail = new SendEmail();
                    mail.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
                    mail.MailTo = new string[] { Email };
                    mail.MailHtml = true;
                    mail.MailSubject = "motulteam.com Sifre Hatirlatma";

                    mail.MailBody = strHTML;
                    bool mailSent = mail.SendMail(true);

                    // e-mail gönderildi
                    if (mailSent)
                        ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Şifreniz e-mail adresinize gönderilmiştir."));

                    else
                        ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sistemde bir hata oluştu. Lütfen daha sonra yeniden deneyiniz."));
                }
                else
                {
                    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("E-mail adresi sistemimizde kayıtlı değildir. Lütfen e-mail adresinizi kontrol edip yeniden deneyiniz."));
                }
            }
        }
        catch (Exception err)
        { }
        finally
        {
            txtEmail.Text = string.Empty;
            txtEmailTekrar.Text = string.Empty;
        }


    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">Popup('" + mesaj + "');</script>";
    }
}