﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="KargoProseduru.aspx.cs" Inherits="MusteriHizmetleri_KargoProseduru" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Kargo Prosedürü
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
   Motul’dan verdiğiniz siparişlerinizin tümü siz müşterilerimize kargo aracılığı
    ile gönderilmektedir. Kargo firması siparişinizi teslim ederken dikkat edilmesi
    gereken birkaç konu vardır.<br />
    <br />
    <h3>
        Kargo paketini nasıl teslim almalıyım?</h3>
    Kargo firması zamanla yarıştığı için ürünü teslim edip hemen gitmek ister , ancak
    hassas olan bir konu vardır . Kargo paketinizi teslim almadan kargo formuna imza
    atılmamalıdır. Kargo tutanağını imzalamadan önce ürününüzün kutusunun taşımadan
    dolayı hasarlı olup olmadığını lütfen kontrol ediniz. Kutuda herhangi bir nedenle
    hasar var ise teslimatla ilgili hiç bir belgeyi imzalamadan kargo yetkilisine tutanak
    tutulması için kutunuzu iade ediniz. Size tarafımızdan yeni ürünleriniz derhal gönderilecektir.
    Kutusu hasarlı olan ürünlerin teslim alınması durumunda içindeki ürünlerin hasarından
    veya eksikliğinden Motul sorumlu değildir.
    <br />
    <br />
    <h3>
        Teslim edilen pakette deformasyon var mı?
    </h3>
    Kargo firmasının getirdiği pakette herhangi bir deformasyon veya içindeki ürünün
    zarar görebileceğini size düşündüren herhangi bir şüphe var ise kargo formunu imzalamadan
    kargo görevlisinin yanında paketi açıp içindeki siparişlerinizin fiziksel durumunu
    kontrol ediniz. Bir sıkıntı görmüyorsanız kargo formunu imzalayınız. Kutu içersindeki
    ürünlerin herhangi bir şekilde zarar gördüğünü düşünüyorsanız ürünleri teslim almayıp
    kargo görevlisinde kargo hasar tutanağı istediğinizi talep ediniz.(Tutanak tutulan
    ürünleri lütfen teslim almayınız. motulteam.com’a ürünlerin geri gelmesi gerekmektedir.)
    Tutanak tutulan ürünlerin değişimi yapılarak tarafınıza yeniden kargolanacaktır.
    <br />
    <br />
    <h3>
        Kargo firması paketi teslim ettikten sonra kutunun ezik,yırtık ve darbeli olduğunu
        farkettik ne yapmalıyız?</h3>
    Kargo firmasının görevi Anadoludanaspara.com’dan yaptığınız alışverişi güvenli bir taşımacılık
    sunarak doğru kişiye güvenle teslim etmektir. Üründe bir hasar var ise kargo şubesine
    giderek zabıt tutturabilirsiniz. Tuttuğunuz tutanakla beraber ürün bize ulaştıktan
    sonra değişim işleminiz derhal yapılacaktır.
    <br />
    <br />
    Sitemiz üzerinden yapılan alışverişlerde şuan yurtdışı ve Kıbrıs'a teslimat bulunmamaktadır.
</asp:Content>
