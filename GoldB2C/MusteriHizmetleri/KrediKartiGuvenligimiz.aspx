﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="KrediKartiGuvenligimiz.aspx.cs" Inherits="MusteriHizmetleri_KrediKartiGuvenligimiz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Kredi Kartı Güvenliğimiz
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    Motul sitesinden alışveriş yapan kredi kartı sahiplerinin güvenliğini
    ilk planda tutmaktadır. Motul'da bu amaçla kurulmuş, verilen siparişlerin
    ödeme/fatura/teslimat adresi bilgilerinin kontrolünü gerçekleştiren bir Kredi Kartları
    Sahtekarlığı (Fraud) Departmanı mevcuttur. Bu yüzden, Motul'dan ilk defa
    sipariş veren müşterilerin siparişlerinin tedarik ve teslimat aşamasına gelebilmesi
    için öncelikle finansal bilgilerin banka tarafından doğruluğunun onaylanması gerekmektedir
    , bu bilgilerin kontrolü için gerekirse kredi kartı sahibi ile irtibata geçilmektedir.
    <br />
    <br />
    GlobalSign sertifikalı Motul, Türkiye'nin en yaygın online ödeme altyapısı
    Garanti e-ticaret Sanal POS sisteminin yanı sıra Finans Bank, Maximum Card, Akbank
    ve Yapı Kredi Sanal POS'larını da kullanmaktadır. Alışveriş sırasında kullanılan
    kredi kartı ile ilgili bilgiler siteden bağımsız olarak GlobalSign tarafından 128
    bit SSL (Secure Sockets Layer) protokolü ile şifrelenip sorgulanmak üzere ilgili
    bankaya ulaşır. Kartın kullanılabilirliği onaylandığı takdirde alışverişe devam
    edilir. Kartla ilgili hiçbir bilgi site tarafından görüntülenemediğinden ve kaydedilmediğinden,
    üçüncü şahısların herhangi bir koşulda bu bilgileri ele geçirmesi engellenmiş olur.
    <br />
    <br />
    Kredi Kartı bilgilerinizi girdiğiniz sayfanın sağ alt köşesinde bulunan kilit resmi
    bu sayfanın SSL ile şifrelendiğini gösterir ve üzerine tıkladığınızda şifrelemenin
    hangi firmaya ait olduğunu belirtir.
</asp:Content>
