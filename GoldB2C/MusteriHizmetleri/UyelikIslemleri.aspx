﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="UyelikIslemleri.aspx.cs" Inherits="MusteriHizmetleri_UyelikIslemleri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Üyelik İşlemleri
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    Sitemizden alışveriş yapabilmek ve farklı avantajlardan yararlanabilmek için sitemize
    üye olunması gerekmektedir. Bu üyelik aynı zamanda sizin bizim sistemimiz üzerinde
    bir Cari hesabınızın oluşmasını sağlayacak ve eski siparişleriniz , kayıtlı sepetiniz
    gibi site özelliklerini yönetip bir çok konuda alışverişlerinizde size yardımcı
    olacaktır. Üye olabilmek için motulteam.com’ a girmeniz ve ana sayfada Kategorilerin
    hemen üzerindeki sağ taraftaki "Üye Olmak İstiyorum" linkine tıklamanız gerekmektedir.
    Açılan ekrandaki bilgileri doğru şekilde doldurduktan sonra "Hesabımı Oluştur" linkine
    tıklayarak sisteme üyeliğinizi gerçekleştirebilirsiniz. Üyelik esnasında sizden
    alınan Cep telefonu, mail adresi gibi bilgiler alıverişleriniz hakkındaki tüm bilgileri
    sizinle paylaşacağımız irtibat noktaları olacağı için yanlış bilgi girilmediğinden
    emin olunuz.
</asp:Content>
