﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="3dSecureNedir.aspx.cs" Inherits="MusteriHizmetleri_3dSecureNedir" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    3D Secure Nedir?
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    3-D Secure, internette alışveriş işlemlerinin güvenli bir şekilde yapılabilmesi
    için kart kuruluşları tarafından geliştirilmiş olan bir kimlik doğrulama sistemidir.
    Sistemin Visa kredi kartı kullanımı için hazırlanan uygulamasına ” Verified by Visa”
    MasterCard kredi kartı kullanımı için hazırlanan uygulamasına ise “SecureCode” isimleri
    verilmektedir.
</asp:Content>
