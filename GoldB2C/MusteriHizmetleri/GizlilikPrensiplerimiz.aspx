﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="GizlilikPrensiplerimiz.aspx.cs" Inherits="MusteriHizmetleri_GizlilikPrensiplerimiz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    <span class="navigation-pipe">&nbsp;</span> <span class="navigation_page">Gizlilik Prensiplerimiz</span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
    <span class="page-heading-title2">GİZLİLİK PRENSİPLERİMİZ</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <div class="mt10">
      Hediye sitemize kaydetmiş olduğunuz kişisel bilgilerinizin güvenliği bizler için her şeyden önemlidir. Kampanya hediye sitemizden alışveriş yapma ayrıcalığına sahip tüm üyelerimizin ürün siparişi esnasında paylaştıkları gönderim bilgilerinin(Teslimat/Fatura adresleri,Tel Numaraları vs.) tamamı gizli tutulacak ve başka kurum ve kuruluşlarla paylaşılmayacaktır. Sistemlerimizde sadece sizin sipariş esnasında kolay ve hızlı giriş yapmanızı sağlayacak olan bilgiler saklanmaktadır. 
        <br /><br />
Üyelik esnasında bizimle paylaştığınız cep telefonu ve mail adresleriniz vermiş olduğunuz siparişlerinizle ilgili bilgilerin size hızlı ve güvenli bir şekilde ulaşmasını sağlamak için ve Motul Hediye'nin kampanya bilgilendirmelerinde siz değerli müşterilerimize öncelik sağlamak açısından kullanılacaklardır.
    </div>
</asp:Content>
