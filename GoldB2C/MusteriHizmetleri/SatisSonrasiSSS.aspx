﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="SatisSonrasiSSS.aspx.cs" Inherits="MusteriHizmetleri_SatisSonrasiSSS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Sıkça Sorulan Sorular
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <h3>
        1. Satın aldığım ürünü iade etmek istersem ne yapmalıyım ?</h3>
    Aldığınız ürünü teslimat tarihinden itibaren 3 iş günü içerisinde faturanız ve iade
    sebebinizi içeren bir not eki ile iade edebilirsiniz. İade etmek istediğiniz ürünün
    faturası kurumsal ise, geri iade ederken kurumun düzenlemiş olduğu iade faturası
    ile birlikte göndermeniz gerekmektedir. İade faturası ürün birim fiyat + kdv şeklinde
    kesilmelidir.
    <br />
    <br />
    <h3>
        2. İade süresi kaç gün?</h3>
    Aldığınız ürünü teslimat tarihinden itibaren 3 iş günü içerisinde, düzenlemiş olduğumuz
    faturanız ve iade sebebinizi içeren bir not eki ile iade edebilirsiniz.
    <br />
    <br />
    <h3>
        3. İade şartları nelerdir?
    </h3>
    İadeler kesinlikle orijinal kutu veya ambalajı ile birlikte yapılmalıdır. Orijinal
    kutusu, ambalajı bozulmuş ( orijinal kutu üzerine kargo etiketi yapıştırılmış ve
    kargo koli bandı ile bantlanmış ürünler kabul edilemez ) satılabilirlilik özelliğini
    kaybetmiş durumda olan ürünlerin iadesi kabul edilmeyecektir. İade etmek istediğiniz
    ürün, ürünler ayıplı ise kargo ücreti tarafımızdan karşılanmaktadır. Bu gibi durumlarda
    anlaşmalı olduğumuz kargo şirketi ile gönderim yapmanız gerekmektedir. Aksi durumda
    kargo ücreti size aittir. İade etmek istediğiniz ürün ile birlikte orijinal fatura
    (sizdeki bütün kopyaları) ve iade sebebini içeren bir dilekçe göndermeniz gerekmektedir.
    Ürün seçiminde uyumsuzluk problemi yaşanabilecek ürünler için, mutlaka uyum konusunda
    teknik destek alarak sipariş vermelisiniz. Bu tip ürünlerin orijinal ambalajları
    açıldığında iade işlemleri mümkün olmamaktadır. (Örn : hafıza kartları, bilgisayar
    donanım ve yazılım ürünleri )
    <br />
    <br />
    <br />
    <h3>
        Özel Ürünler için iade şartları
    </h3>
    <br />
    <h3>
        Telefon iadesinde;
    </h3>
    Cep telefonunun 3 günlük süre içerisinde iade edilebilmesi için Genel İade Şartlarına
    ek olarak aşağıdaki şartlara dikkat edilmesi gerekmektedir: Ürünün ekranında var
    olan şeffaf koruma bandının çıkarılmamış olması gerekmektedir. Ürünün orijinal kutusu,
    ambalajı varsa standart aksesuarlarının eksiksiz ve hasarsız olması gerekmektedir.
    <br />
    <br />
    <h3>
        Diğer Ürünlerin iadesinde;</h3>
    Niteliği itibarıyla iade edilemeyecek ürünler ( ürünün arızalı veya ayıplı çıkması
    halleri dışında, açıldıktan sonra sağlık açısından tehlike arz edebilen ürünler
    örn : kullanım esnasında vücut ile birebir temas gerektiren ürünler,kulak içi veya
    kulak üstü kulaklık vs.), tek kullanımlık ürünler, kopyalanabilir yazılım ve programlar,
    hızlı bozulan veya son kullanım tarihi geçen ürünlerin iadesi mümkün değildir.Ürünün
    arızalı olması dışında hijyen ve sağlık sorunları nedeni ile ambalajı açılmış ürünlerin
    hiçbir şekilde iadesi kabul edilmemektedir.
    <br />
    <br />
    <ul style="margin-left: 20px">
        <li>DVD, VCD, CD ve kasetler</li>
        <li>Taşınabilir bilgisayarlar</li>
        <li>Bilgisayar ve kırtasiye sarf malzemeleri (toner, kartuş, şerit v.b)</li>
        <li>Her türlü kozmetik ürünleri vs gibi.</li>
    </ul>
</asp:Content>
