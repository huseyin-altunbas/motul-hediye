﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="IndirimCekiGonder.aspx.cs" Inherits="MusteriHizmetleri_IndirimCekiGonder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    <link href="/css/gold.css?v=1.2.1" rel="stylesheet" type="text/css" />
    <link href="/css/goldHeader.css?v=1.2.6" rel="stylesheet" type="text/css" />
    <link href="/css/goldForms.css?v=2" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/js/flashobject.js"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="/js/Gold/goldGenel.js?v=1.1" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/css/navigation.css?v=4.3.3" />
    <script type="text/javascript" src="/js/navigation.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/userdropdown.css?v=1.2" />
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <link href="../css/goldAccount.css?v=3" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }
    </script>
    <div style="float: left;">
        <div>
            <b style="font-size: 16px;">
                <asp:Label ID="lblFirmaAciklama" runat="server"></asp:Label></b>
        </div>
        <div class="w764 mAuto clearfix mb50 fontTre fleft">
            <div class="formTxt w764 mt20 fleft">
                <div class="w708 fontSz12 color4e ml28 mt10 fleft">
                    <span class="colord8 tdunder strong">İndirim Kodu</span><br />
                    <br />
                    <asp:Panel ID="pnlGonder" runat="server" DefaultButton="btnGonder">
                        <span>Email Adresiniz : </span>&nbsp;&nbsp;<span>
                            <asp:TextBox ID="txtEmailAdresiniz" runat="server" Width="300px"></asp:TextBox>
                        </span>&nbsp;&nbsp;<span>
                            <asp:Button ID="btnGonder" runat="server" Text="Gönder" OnClick="btnGonder_Click"
                                ValidationGroup="ValidationEmail" /></span>
                    </asp:Panel>
                    <br />
                    <asp:RequiredFieldValidator ID="rqEmailRequired" runat="server" ErrorMessage=" *** Mail adresinizi giriniz."
                        ControlToValidate="txtEmailAdresiniz" ForeColor="#3366FF" ValidationGroup="ValidationEmail"></asp:RequiredFieldValidator><br />
                    <asp:RegularExpressionValidator ID="rqEmailExpress" runat="server" ErrorMessage=" *** Geçerli bir mail adresi giriniz."
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmailAdresiniz"
                        ForeColor="#3366FF" ValidationGroup="ValidationEmail"></asp:RegularExpressionValidator>
                </div>
            </div>
        </div>
        <div class="w764 mAuto clearfix mb50 fontTre fleft">
            <div class="formTxt w764 mt20 fleft">
                <div class="w708 fontSz12 color4e ml28 mt10 fleft">
                    <span class="colord8 tdunder strong">Gönderilen indirim kodları :</span><br />
                    <br />
                    <asp:Repeater ID="rptIndirimCekleri" runat="server">
                        <HeaderTemplate>
                            <div class="ordertitle" style="width: 730px;">
                                <div class="title4" style="width: 150px;">
                                    Ad-Soyad</div>
                                <div class="title4" style="width: 200px;">
                                    İndirim Kodu</div>
                                <div class="title4" style="width: 300px;">
                                    Gönderilen Email Adresi</div>
                                <div class="title4" style="width: 50px;">
                                    Tarih</div>
                            </div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="orderchooes" style="width: 730px;">
                                <div class="chooes4" style="width: 150px;">
                                    <%#DataBinder.Eval(Container.DataItem, "KullaniciAdSoyad")%></div>
                                <div class="chooes4" style="width: 200px;">
                                    <%#DataBinder.Eval(Container.DataItem, "CekNo")%></div>
                                <div class="chooes4" style="width: 300px;">
                                    <%#DataBinder.Eval(Container.DataItem, "GonderilenEmailAdresi")%></div>
                                <div class="chooes4" style="width: 50px;">
                                    <%#DataBinder.Eval(Container.DataItem, "GonderilmeTarihi", "{0:dd.MM.yyyy}")%></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
