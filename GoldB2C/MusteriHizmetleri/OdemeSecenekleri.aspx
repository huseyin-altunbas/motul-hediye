﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MusteriHizmetleri/MpMusteriHizmetleri.master"
    AutoEventWireup="true" CodeFile="OdemeSecenekleri.aspx.cs" Inherits="MusteriHizmetleri_OdemeSecenekleri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSayfaBaslik" runat="Server">
    Ödeme Seçenekleri
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphIcerikBaslik" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIcerikMetin" runat="Server">
    Anadoludanaspara.com’dan yapmış olduğunuz alışverişinizin ödemesini ne şekilde yapmak
    istediğini seçtiğiniz ekrandır.<br />
    <br />
    <h3>
        Standart Ödeme
    </h3>
    Standart ödeme bölümünde sipariş tutarınızın tamamını kredi kartınızdan tek çekim
    veya taksitli olarak yada banka hesabımıza havale yaparak ödeyebilirsiniz.
    <br />
    <br />
    <h3>
        Kredi Kartı
    </h3>
    <br />
    Kredi kartıyla yapacağınız ödemelerde kullanacağınız ekrandır. Öncelikli olarak
    hangi banka kartıyla ödeme yapacağınızı belirlemeniz gerekmektedir. Banka seçimini
    yaptıktan sonra taksit adetini seçiniz. Kredi kartı bilgilerinizi doğru ve eksiksiz
    girmeniz gerekmektedir.
    <br />
    <br />
    Ödenecek tutar bölümünde gördüğünüz tutar seçmiş olduğunuz banka veya taksit adetine
    göre değişiklik göstere bilmektedir. Peşin fiyatına taksitli satın alma yapıyorsanız
    sayfanın üst tarafındaki sipariş tutarıyla ödenecek tutar aynı olacaktır.
    <br />
    <br />
    Ödenecek tutar kredi kartınızdan ne kadar para çekileceğini göstermektedir. ”Ödemeyi
    Onayla” butonuna basarak siparişinizi sonlandıracaksınız ve karşınıza gelen ekranda
    tüm siparişinizin onayını veren ve ayrıntılı dökümünü veren fatura örneği karşınıza
    gelecektir.
    <br />
    <br />
    <h3>
        Havale
    </h3>
    Havale ile ödeme yapmak istediğiniz bankayı seçtikten sora lütfen motulteam.com’un
    banka hesap numarasını bir yere not alın veya bir sonraki adımda çıkacak olan “Onay”
    penceresini yazıcıdan bastırın.
    <br />
    <br />
    Havale yapacak olan kişinin adını yazınız. Banka ekranlarında sizden gelen havaleyi
    hızlı bir şekilde görebilmemiz için havale yapacak kişinin adının doğru yazılması
    büyük önem taşımaktadır.Havale terciğini kullanarak sadece TL havalesi yapılabilmektedir.
    <br />
    <br />
    Banka hesabına 2 iş günü içinde Havalenin yapılması gerekmektedir.
    <br />
    <br />
    Aksi durumda siparişiniz iptal edilecektir.
</asp:Content>
