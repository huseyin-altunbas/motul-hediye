﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true" CodeFile="AkaryakitKartBilgileri.aspx.cs" Inherits="AkaryakitKartBilgileri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="/css/goldBasket.css?v=1.1" rel="stylesheet" type="text/css" />
    <link href="/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine-tr.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#form1").validationEngine({ ajaxFormValidation: true, scroll: false });
        });

    </script>
    <div class="columns-container">
        <div class="container" id="columns">
            <div class="deliveryinvoicecontainer_">
                <h2>Akaryakıt Bilgisi</h2>
                <hr />
                <!-- Fatura Main -->
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="FaturaMain_">
                            <div class="table-responsive">
                                <h3>
                                    <asp:Label runat="server" ID="lblSiparisAkaryakit"></asp:Label></h3>
                                <table class="table table-striped">
                                    <thead style="background-color: #ff8201; color: #fff;">
                                        <tr>
                                            <th style="padding:7px;"><b>Akaryakıt Bilgilerinizi Seçin</b></th>
                                            <th style="padding:7px;"></th>
                                            <th style="padding:7px;"><b>Yakıt Tipi</b></th>
                                            <th style="padding:7px;"><b>Seçiniz</b></th>
                                            <th style="padding:7px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <asp:Repeater ID="rptAkaryakit" runat="server" OnItemDataBound="rptAkaryakit_ItemDataBound">
                                            <ItemTemplate>

                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblKartNo" runat="server" Text=""></asp:Label></td>
                                                    <td>
                                                        <asp:Label ID="lblPlaka" runat="server" Text=""></asp:Label>
                                                        <br />
                                                        <asp:Label ID="lblTutar" runat="server" Text="" CssClass=""></asp:Label>

                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblYakitTipi" runat="server" Text=""></asp:Label></td>
                                                    <td>
                                                        <input id="chkKartTeslimat" name="chkTeslimat" class="chkKartTeslimat" type="checkbox" runat="server" value='<%# DataBinder.Eval(Container.DataItem, "KartId") %>' disabled="true" data-my="7474" /></td>
                                                    <td>
                                                        <a class="duzenle" onclick="DivYeniAdresAc(<%# DataBinder.Eval(Container.DataItem, "KartId") %>)"
                                                            title="Düzenle" style="color: Red;">Bu Plakaya Seçim Yap</a>
                                                        <%--<asp:LinkButton ID="btnSil" runat="server" CssClass="sil" OnClientClick="return confirm('Seçtiğiniz kart bilgileri silinecektir. Silmek istediğinizden emin misiniz?');"
                                                            OnClick="btnSil_Click">Sil</asp:LinkButton>--%>
                                                    </td>
                                                </tr>

                                            </ItemTemplate>


                                        </asp:Repeater>
                                        
                                        

                                    </tbody>
                                </table>
                            </div>


                            <script type="text/javascript">

                                window.onload = function (e) {

                                    chkKartTeslimat();
                                }

                                setTimeout(100, chkKartTeslimat);
                                
                                function chkKartTeslimat()
                                {
                                    $('.chkKartTeslimat').change(function () {
                                         
                                        //DivYeniAdresAc($(this).val());
                                        var id = $(this).attr("id");
                                        $(".chkKartTeslimat").each(function () {
                                            var _id = $(this).attr("id");
                                            if (id === _id) {
                                            }
                                            else {
                                                $("#" + _id).attr("checked", false);
                                            }
                                        });
                                    });
                                }

                            </script>


                            <div class="TabloBottom" style="height: 52px;">
                                <div class="YeniAdres">
                                    <a onclick="DivYeniAdresAc(0);" class="button">+ Yeni Kart İstiyorum</a>
                                </div>
                                <div class="Gonder" style="width: 149px;">
                                    <asp:LinkButton ID="btnGonder" runat="server" CssClass="button"
                                        OnClick="btnGonder_Click">
                                                   Kabul Ediyorum</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Fatura Main Bitti -->
                <div style="clear: both" class="clearfix">
                </div>
                <!-- Yeni Adres -->

                <div class="row" id="DivYeniAdres" style="display: none;">
                    <div class="col-md-12">
                        <div class="heading" style="background-color: #ce0b00; color: white; padding: 5px;">
                            <h3>Yeni Kart İstiyorum</h3>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <br />
                        Akaryakıt siparişlerinizde göndermiş olduğumuz anahtarlıkları aynı plakaya bir sonraki siparişinizde de kullanabilirsiniz.
                             Yeni anahtarlık gönderilmeyecek olup, aynı plakaya geçtiğiniz siparişlerde mevcut anahtarlığınıza sipariş tutarı kadar yükleme yapılacaktır.
                    </div>
                    <div class="col-md-6">
                        <br />
                        <div class="contact-form-box">
                            <div class="form-selector">
                                <label>Yüklenecek Plaka</label>
                                <asp:TextBox ID="txtPlaka" runat="server" MaxLength="10" CssClass="form-control input-sm validate[required]"></asp:TextBox>
                            </div>

                            <div class="form-selector">
                                <label>Yüklenecek Tutar</label>
                                <div class="input-group">
                                    <asp:TextBox ID="txtTutar" runat="server" CssClass="form-control input-sm validate[required]" onkeypress="return numbersonly(event)" onkeyup="return limitlength(this, 7)" ReadOnly></asp:TextBox>
                                    <span class="input-group-addon">.00</span>
                                </div>

                            </div>

                            <div class="form-selector">
                                <label>Yakıt Türü</label>
                                <asp:DropDownList ID="ddlYakitTuru" runat="server" CssClass="form-control input-sm" Enabled="false">
                                    <asp:ListItem Selected="True" Value="Seçiniz" Text="Seçiniz" />
                                    <asp:ListItem Value="Benzin" Text="Benzin" />
                                    <asp:ListItem Value="Dizel" Text="Motorin" />
                                </asp:DropDownList>
                            </div>

                            <div class="form-selector">
                                <label></label>
                                <input id="hdnKartId" type="hidden" />
                                <div class="col-md-2" style="padding-left: 0px;"><a onclick="ValidateForm();" class="button">Kaydet</a> </div>
                                <div class="col-md-2"><a onclick="DivYeniAdresKapat();" class="button">Kapat</a> </div>
                            </div>

                        </div>
                    </div>

                </div>


                <!-- Yeni Adres Bitti -->
            </div>
        </div>
    </div>



    <script type="text/javascript">
        window.onbeforeunload = function () {

            removeKartId();
        }; 
        function ValidateForm() {
            if (jQuery('#form1').validationEngine('validate') == true)
                AdresKaydet();

        }

        function removeKartId() {
            $.ajax({
                type: 'POST',
                url: '/AkaryakitKartBilgileri.aspx/removeKartId',
                data: {},
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {


                },
                error: function () {

                }
            });
        }

        function setKartId(KartId) {
            $.ajax({
                type: 'POST',
                url: '/AkaryakitKartBilgileri.aspx/setKartId',
                data: '{ "KartId":"' + KartId + '" }',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {

                    
                    },
                    error: function () {
                         
                    }
             });
        }

        function DivYeniAdresAc(KartId) {
            var tutar = "<%=tutar.ToString()%>";
            $('#hdnKartId').val(KartId);
            if (KartId == 0) {

                $("#<%=txtPlaka.ClientID %>").prop("disabled", false);
                $("#<%=txtPlaka.ClientID %>").css({ "background-color": "#f9f9f9", "color": "#676767" });

                $('#DivYeniAdres').slideDown(400);
                $("#<%=txtPlaka.ClientID %>").val('');
                $("#<%=txtTutar.ClientID %>").val(tutar);
                //$("#<%=ddlYakitTuru.ClientID %>").val('');
            }
            else {

                $("#<%=txtPlaka.ClientID %>").prop("disabled", true);
                $("#<%=txtPlaka.ClientID %>").css({ "background-color": "#ff0000", "color": "#fff" });

                $.ajax({
                    type: 'POST',
                    url: '/AkaryakitKartBilgileri.aspx/getKartBilgileri',
                    data: '{ "KartId":"' + KartId + '" }',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {

                        //$('#DivYeniAdres').delay(200).slideDown(400);
                       
                        //$(":checkbox[value=" + KartId + "]").attr("checked", "true");
                        $("#<%=txtPlaka.ClientID %>").val(result.d.Plaka);
                        $("#<%=txtTutar.ClientID %>").val(tutar);
                        //$("#<%=ddlYakitTuru.ClientID %>").val(result.d.yakitTuru);
                        AdresKaydet();
                        $(":checkbox").removeAttr("checked");
                        
                        $(":checkbox[value=" + KartId + "]").removeAttr("disabled");
                        $(":checkbox[value=" + KartId + "]").click();
                        $(":checkbox[value=" + KartId + "]").prop("disabled", true);
                        $(":checkbox[value=" + KartId + "]").prop('checked', true);
                    },
                    error: function () {
                        alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                    }
                });



            }
        }

        function DivYeniAdresKapat() {
            $('#DivYeniAdres').slideUp(400);
        }

        function AdresKaydet() {
            debugger;
            var KartId = $("#hdnKartId").val();
            var Plaka = $("#<%=txtPlaka.ClientID %>").val();
            var Tutar = $("#<%=txtTutar.ClientID %>").val();
            var yakitTuru = $("#<%=ddlYakitTuru.ClientID %>").val();
            if (yakitTuru == "Seçiniz") {
                alert("Lütfen yakıt türü seçiniz.")
                return "";
            }
            $.ajax({
                type: 'POST',
                url: '/AkaryakitKartBilgileri.aspx/KartBilgisiKaydet',
                data: '{ "KartId" : "' + KartId + '", "Plaka" : "' + Plaka + '", "Tutar" : "' + Tutar + '", "yakitTuru" : "' + yakitTuru + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    $('#DivYeniAdres').slideUp(400);
                    if ($('#hdnKartId').val() === "0") {
                        __doPostBack('<%=UpdatePanel1.ClientID %>', '');
                    }
                    $(".tutar").html("Yüklenecek Tutar : 0,00 TL");
                    $(".tutar" + KartId + "").html("Yüklenecek Tutar : " + Tutar + " TL");
                    setKartId(KartId);
                    
                    if (KartId === "0") {
                        window.location.href = "/AkaryakitKartBilgileri.aspx";

                    }
                },
                error: function () {
                    alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                }
            });
        }

        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8) { //if the key isn't the backspace key (which we should allow)
                if (unicode < 48 || unicode > 57) //if not a number
                    return false //disable key press
            }
        }

        function limitlength(obj, length) {
            var maxlength = length
            if (obj.value.length > maxlength)
                obj.value = obj.value.substring(0, maxlength)
        }

    </script>
    <style type="text/css">
        .Tutar {
            text-align: right !important;
            width: 60px !important;
        }

        .Kurus {
            text-align: right !important;
            width: 20px !important;
            background-color: #fff !important;
        }

        .YeniAdrsBgNew {
            height: 223px !important;
        }

        .YeniAdresMainNew {
            height: 282px !important;
        }

        #DivYeniAdres .form-selector {
            padding-bottom: 20px;
        }
    </style>
</asp:Content>

