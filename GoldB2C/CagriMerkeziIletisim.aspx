﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CagriMerkeziIletisim.aspx.cs"
    Inherits="CagriMerkeziIletisim" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/css/gold.css?v=1.2.1" rel="stylesheet" type="text/css" />
    <link href="/css/goldHeader.css?v=1.2.6" rel="stylesheet" type="text/css" />
    <link href="/css/goldForms.css?v=2" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/js/flashobject.js"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="/js/Gold/goldGenel.js?v=1.1" type="text/javascript"></script>
    <!--[if lt IE 7.]>
<script defer type="text/javascript" src="js/pngfix.js"></script>
<![endif]-->
    <!--  Global -->
    <!--  MainNavigation -->
    <link rel="stylesheet" type="text/css" href="/css/navigation.css?v=4.3.3" />
    <script type="text/javascript" src="/js/navigation.js"></script>
    <!--  MainNavigation -->
    <!--  UserNavigation -->
    <link rel="stylesheet" type="text/css" href="/css/userdropdown.css?v=1.2" />
    <script type="text/javascript">
        $(function () {
            $('.userdropdown').each(function () {
                $(this).parent().eq(0).hover(function () {
                    $('.userdropdown:eq(0)', this).show();
                }, function () {
                    $('.userdropdown:eq(0)', this).hide();
                });
            });
        });
    </script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine-tr.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(function ($) {

            $("#<%=txtCepTelKod.ClientID %>").mask("599");
            $("#<%=txtCepTel.ClientID %>").mask("9999999");

        }); 
    </script>
    <script type="text/javascript">
        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }
    </script>
    <script type="text/javascript">
        $("#<%=txtSoru.ClientID %>").live('keyup blur', function () {
            var maxlength = "1000";
            var val = $(this).val();

            if (val.length > maxlength) {
                $(this).val(val.slice(0, maxlength));
            }
        });
    </script>
</head>
<body style="background: none !important">
    <form id="form1" runat="server">
    <asp:Panel ID="pnlIletisim" runat="server">
        <br />
        <div style="font-weight: bold; font-size: 16px; padding-left: 25px;">
            Değerli Üyemiz,<br />
            Kurban Bayramı nedeniyle bayram süresince kargo firmaları çalışmayacağından dolayı
            02.10-07.10.2014 tarihleri arasında girmiş olduğunuz siparişlerin kargo gönderileri
            yapılamayacaktır. Sizlere daha iyi hizmet sunabilmek adına taleplerinizi aşağıdaki
            mesaj alanına yazarak bize iletebilirsiniz. En kısa sürede sizlerle iletişime geçeceğiz.<br />
            <br />
            İyi bayramlar dileriz. Motul Hediye Ekibi.
        </div>
        <br />
        <div style="font-weight: bold; font-size: 12px; padding-left: 25px;">
            Öneri ve Sorularınız için lütfen aşağıdaki formu doldurunuz.</div>
        <div class="contentform">
            <div class="fleft h45 color4e mt20 ml28 w350">
                <div class="fleft w350 h23 strong">
                    <span class="fleft mt3 fontSz13">Adınız</span>
                </div>
                <div class="fleft w230 clearfix">
                    <asp:TextBox ID="txtAd" CssClass="w230" runat="server"></asp:TextBox>
                </div>
                <div class="fleft w240 h23 colorb0 fontTre">
                    <span class="fright notification">
                        <asp:RequiredFieldValidator ID="rqAd" runat="server" ControlToValidate="txtAd" ValidationGroup="grpKullaniciBilgileri"
                            ErrorMessage="Adınızı girin."></asp:RequiredFieldValidator></span>
                </div>
            </div>
            <div class="fleft h45 color4e mt20 ml28 w350">
                <div class="fleft w350 h23 strong">
                    <span class="fleft mt3 fontSz13">Soyadınız</span>
                </div>
                <div class="fleft w230 clearfix">
                    <asp:TextBox ID="txtSoyad" CssClass="w230 pr5 mr8" runat="server"></asp:TextBox>
                </div>
                <div class="fleft w240 h23 colorb0 fontTre">
                    <span class="fright notification">
                        <asp:RequiredFieldValidator ID="rqSoyad" runat="server" ControlToValidate="txtSoyad"
                            ValidationGroup="grpKullaniciBilgileri" ErrorMessage="Soyadınızı girin."></asp:RequiredFieldValidator></span>
                </div>
            </div>
            <div class="fleft h45 color4e mt20 ml28 w350">
                <div class="fleft w350 h23 strong">
                    <span class="fleft mt3 fontSz13">E-Posta adresiniz</span>
                </div>
                <div class="fleft w230 clearfix">
                    <asp:TextBox ID="txtEmail" CssClass="w230" runat="server"></asp:TextBox>
                </div>
                <div class="fleft w240 h23 colorb0 fontTre">
                    <span class="fright notification">
                        <asp:RequiredFieldValidator ID="rqEmail" runat="server" ControlToValidate="txtEmail"
                            ValidationGroup="grpKullaniciBilgileri" ErrorMessage="E-Posta adresinizi girin."
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="rqEmailExp" runat="server" ErrorMessage="Lütfen geçerli bir E-Posta adresi girin."
                            ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ValidationGroup="grpKullaniciBilgileri" Display="Dynamic"></asp:RegularExpressionValidator>
                    </span>
                </div>
            </div>
            <div class="fleft h45 color4e mt20 ml28 w350">
                <div class="fleft w350 h23 strong">
                    <span class="fleft mt3 fontSz13">Cep Telefonu Numaranız</span>
                </div>
                <div class="fleft w350 clearfix">
                    <div class="fleft w35">
                        <asp:TextBox ID="txtCepTelKod" CssClass="fleft w35 pr5 mr8 ml8" Width="35px" MaxLength="3"
                            runat="server"></asp:TextBox>
                    </div>
                    <div class="fleft ml5 mr5 mt3 pt4">
                        /</div>
                    <div class="fleft w170">
                        <asp:TextBox ID="txtCepTel" CssClass="fleft w170 pr5 mr8" Width="170px" MaxLength="7"
                            runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="fleft w240 h23 colorb0 fontTre">
                    <span class="fright notification">
                        <asp:RequiredFieldValidator ID="rqtxtCepTelKod" runat="server" ControlToValidate="txtCepTelKod"
                            ErrorMessage="*Cep Telefonu kodunuzu yazın." ValidationGroup="grpKullaniciBilgileri"></asp:RequiredFieldValidator><br />
                        <asp:RequiredFieldValidator ID="rqtxtCepTel" runat="server" ControlToValidate="txtCepTel"
                            ErrorMessage="*Cep Telefonu numaranızı yazın." ValidationGroup="grpKullaniciBilgileri"></asp:RequiredFieldValidator>
                    </span>
                </div>
            </div>
            <div class="fleft h45 color4e mt20 ml28 w350">
                <div class="fleft w350 h23 strong">
                    <span class="fleft mt3 fontSz13">Öneri veya Sorunuz</span>
                </div>
                <div class="fleft w230 clearfix">
                    <asp:TextBox ID="txtSoru" CssClass="w230" runat="server" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div class="fleft w240 h23 colorb0 fontTre">
                    <span class="fright notification">
                        <asp:RequiredFieldValidator ID="rqAdres" runat="server" ControlToValidate="txtSoru"
                            ValidationGroup="grpKullaniciBilgileri" ErrorMessage="Öneri veya Sorunuzu girin."></asp:RequiredFieldValidator></span>
                </div>
            </div>
            <div style="clear: both;">
            </div>
            <br />
            <br />
            <br />
            <div class="fleft h45 color4e mt20 ml28 w350">
                <asp:LinkButton ID="LinkButton1" CssClass="link-button-RedBtn spriteRedBtn" runat="server"
                    ValidationGroup="grpKullaniciBilgileri" OnClick="btnKaydet_Click"><span class="spriteRedBtn fontTre fontSz16">
                Kaydet</span></asp:LinkButton>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
