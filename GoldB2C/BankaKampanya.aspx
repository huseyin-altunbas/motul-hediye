﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="BankaKampanya.aspx.cs" Inherits="BankaKampanya" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="/css/BankCampaigns.css?v=1.2" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="rootercnav">
        <div class="rcframe">
            <div style="width: 100%;" class="rootername">
                <a href="https://motulteam.com/">Anasayfa</a> / Banka Kampanyaları
            </div>
        </div>
    </div>
    <div class="containerOut982">
        <%--BONUS START--%>
        <div class="bankCampaigns">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader bonus">
                    <img src="ImagesGld/BankCampaigns/bonus-Logo.png" class="logo" alt="Bonus Kart" />
                    <asp:Image ID="ImgTaksitBonus" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%-- <asp:Literal ID="ltrKampanyaBaslikBonus" runat="server"></asp:Literal>--%>
                    <asp:Literal ID="ltrKampanyaIcerikBonus" runat="server"></asp:Literal>
                    <img src="ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--BONUS END--%>
        <%--MAXIMUM START--%>
        <div class="bankCampaigns">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader maximum">
                    <img src="ImagesGld/BankCampaigns/maximum-Logo.png" class="logo" alt="Maximum Kart" />
                    <asp:Image ID="ImgTaksitMaximum" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:Literal ID="ltrKampanyaBaslikMaximum" runat="server"></asp:Literal>--%>
                    <asp:Literal ID="ltrKampanyaIcerikMaximum" runat="server"></asp:Literal>
                    <img src="ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--MAXIMUM END--%>
        <%--AXESS START--%>
        <div class="bankCampaigns">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader axess">
                    <img src="ImagesGld/BankCampaigns/axess-Logo.png" class="logo" alt="Axess" />
                    <asp:Image ID="ImgTaksitAxess" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:Literal ID="ltrKampanyaBaslikAxess" runat="server"></asp:Literal>--%>
                    <asp:Literal ID="ltrKampanyaIcerikAxess" runat="server"></asp:Literal>
                    <img src="ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--AXESS END--%>
        <%--WORLD START--%>
        <div class="bankCampaigns">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader world">
                    <img src="ImagesGld/BankCampaigns/world-Logo.png" class="logo" alt="World Kart" />
                    <asp:Image ID="ImgTaksitWorld" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:Literal ID="ltrKampanyaBaslikWorld" runat="server"></asp:Literal>--%>
                    <asp:Literal ID="ltrKampanyaIcerikWorld" runat="server"></asp:Literal>
                    <img src="ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--WORLD END--%>
        <%--CARD FİNANS START--%>
        <div class="bankCampaigns">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader cardFinans">
                    <img src="ImagesGld/BankCampaigns/cardFinans-Logo.png" class="logo" alt="Card Finans" />
                    <asp:Image ID="ImgTaksitCardFinans" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:Literal ID="ltrKampanyaBaslikCardFinans" runat="server"></asp:Literal>--%>
                    <asp:Literal ID="ltrKampanyaIcerikCardFinans" runat="server"></asp:Literal>
                    <img src="ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--CARD FİNANS END--%>
        <%--VAKIFBANK WORLD START--%>
        <div class="bankCampaigns">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader vakifbankWorld">
                    <img src="ImagesGld/BankCampaigns/vakifWorld-Logo.png" class="logo" alt="Vakıfbank World Kart" />
                    <asp:Image ID="ImgTaksitVakifWorld" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:Literal ID="ltrKampanyaBaslikVakifWorld" runat="server"></asp:Literal>--%>
                    <asp:Literal ID="ltrKampanyaIcerikVakifWorld" runat="server"></asp:Literal>
                    <img src="ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--VAKIFBANK WORLD END--%>
        <%--PARAF START--%>
        <div class="bankCampaigns">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader paraf">
                    <img src="ImagesGld/BankCampaigns/paraf-Logo.png" class="logo" alt="Paraf Kart" />
                    <asp:Image ID="ImgTaksitParaf" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:Literal ID="ltrKampanyaBaslikParaf" runat="server"></asp:Literal>--%>
                    <asp:Literal ID="ltrKampanyaIcerikParaf" runat="server"></asp:Literal>
                    <img src="ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--PARAF END--%>
        <%--ADVANTAGE START--%>
        <div class="bankCampaigns">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader advantage">
                    <img src="ImagesGld/BankCampaigns/advantage-Logo.png" class="logo" alt="Advantage" />
                    <asp:Image ID="ImgTaksitAdvantage" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:Literal ID="ltrKampanyaBaslikAdvantage" runat="server"></asp:Literal>--%>
                    <asp:Literal ID="ltrKampanyaIcerikAdvantage" runat="server"></asp:Literal>
                    <img src="ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--ADVANTAGE END--%>
        <%--ASYA CARD START--%>
        <div class="bankCampaigns">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader asyaCard">
                    <img src="ImagesGld/BankCampaigns/asyaCard-Logo.png" class="logo" alt="Asya Card" />
                    <asp:Image ID="ImgTaksitAsyaCard" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%-- <asp:Literal ID="ltrKampanyaBaslikAsyaCard" runat="server"></asp:Literal>--%>
                    <asp:Literal ID="ltrKampanyaIcerikAsyaCard" runat="server"></asp:Literal>
                    <img src="ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--ASYA CARD END--%>
    </div>
</asp:Content>
