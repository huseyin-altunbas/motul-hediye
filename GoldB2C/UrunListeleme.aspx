﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="UrunListeleme.aspx.cs" Inherits="UrunListeleme" EnableViewState="true" %>

<%@ MasterType VirtualPath="~/MpAna.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="/css/bestsellersProducts.css" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="/css/jquery.ui.slider.css?v=1.1" />
    <link rel="stylesheet" href="/css/jqtransform.css?v=1.2" />
    <link rel="stylesheet" type="text/css" href="/css/msdropdown/dd.css" />
    <script src="/js/msdropdown/jquery.dd.min.js" type="text/javascript"></script>
    <style type="text/css">
	#demo-frame > div.demo { padding: 10px !important; };
</style>
    <div class="columns-container">
        <div class="container" id="columns">
            <!-- breadcrumb -->
            <div class="breadcrumb clearfix">
                <asp:Literal ID="ltrAyakizi" runat="server"></asp:Literal>
            </div>
            <!-- ./breadcrumb -->
            <!-- row -->
            <div class="row">
                <!-- Left colunm -->
                <div class="column col-xs-12 col-sm-3" id="left_column">
                    <!-- block filter -->
                    <div class="block left-module">
                        <p class="title_block">
                            PUANINA GÖRE</p>
                        <div class="block_content">
                            <!-- layered -->
                            <div class="layered layered-filter-price">
                                <!-- filter price -->
                                <div class="layered_subtitle">
                                </div>
                                <div class="layered-content slider-range">
                                    <asp:Literal ID="ltrRangePrice" runat="server"></asp:Literal>
                                </div>
                                <!-- ./filter price -->
                            </div>
                            <!-- ./layered -->
                        </div>
                    </div>
                    <!-- ./block filter  -->
                    <!-- block best sellers -->
                    <asp:Repeater ID="rptCokSatanlarListeleme" runat="server">
                        <HeaderTemplate>
                            <div class="block left-module">
                                <p class="title_block">
                                    KATEGORİNİN ÇOK SATANLARI</p>
                                <div class="block_content">
                                    <div class="owl-carousel owl-best-sell" data-loop="true" data-nav="false" data-margin="0"
                                        data-autoplaytimeout="1000" data-autoplay="true" data-autoplayhoverpause="true"
                                        data-items="1">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# (Container.ItemIndex % 3 == 0) ? "<ul class=\"products-block best-sell\">" : "" %>
                            <li>
                                <div class="products-block-left">
                                    <a href="<%#DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>"
                                        class="thumbsPic">
                                        <img src="//<%=ImageDomain %>/UrunResim/BuyukResim/<%#DataBinder.Eval(Container.DataItem, "resim_ad") %>"
                                            alt="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>" />
                                    </a>
                                </div>
                                <div class="products-block-right">
                                    <p class="product-name">
                                        <a href="<%#DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>"
                                            class="productName">
                                            <%#DataBinder.Eval(Container.DataItem, "urun_ad") %></a>
                                    </p>
                                    <p class="product-price">
                                        <a href="<%#DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%#DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                                            <%#DataBinder.Eval(Container.DataItem, "net_tutar", "{0:###,###,###}")%>&nbsp;<span>PUAN</span></a></p>
                                    <%--<p class="product-star">
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                                class="fa fa-star"></i><i class="fa fa-star-half-o"></i>
                                        </p>--%>
                                </div>
                            </li>
                            <%# ((Container.ItemIndex > 0 && (Container.ItemIndex + 1) % 3 == 0) || (Container.ItemIndex + 1) == rptCokSatanlarListeleme.Items.Count) ? "</ul>" : ""%>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div> </div></div>
                        </FooterTemplate>
                    </asp:Repeater>
                    <!-- ./block best sellers  -->
                </div>
                <!-- ./left colunm -->
                <!-- Center colunm-->
                <div class="center_column col-xs-12 col-sm-9" id="center_column">
                    <!-- subcategories -->
                    <asp:Literal ID="ltrCategory" runat="server"></asp:Literal>
                    <!-- ./subcategories -->
                    <div class="sortPagiBar">
                        <%--<div class="show-product-item">
                            <div class="rooterbuttons">
                                <div class="offerbutton2">
                                    <asp:LinkButton ID="btnKarsilastir" runat="server" OnClick="btnKarsilastir_Click"
                                        CssClass="btngray2">Karşılaştır</asp:LinkButton>
                                </div>
                            </div>
                        </div>--%>
                        <div class="sort-product">
                            <asp:DropDownList ID="dpSiralama" CssClass="dpSiralama" runat="server" onchange="UrunleriSirala(this.value);"
                                Width="185px">
                                <asp:ListItem data-image="/ImagesGld/sort.png" Text="Ürünleri Sıralayın" Value=""></asp:ListItem>
                                <asp:ListItem data-image="/ImagesGld/fiyat-artan.png" Text="Artan Puan" Value="fiyatartan"></asp:ListItem>
                                <asp:ListItem data-image="/ImagesGld/fiyat-azalan.png" Text="Azalan Puan" Value="fiyatazalan"></asp:ListItem>
                                <asp:ListItem data-image="/ImagesGld/alfabe.png" Text="Alfabetik (A'dan Z'ye)" Value="adanzye"></asp:ListItem>
                                <asp:ListItem data-image="/ImagesGld/alfabeters.png" Text="Alfabetik (Z'den A'ya)"
                                    Value="zdenaya"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <!-- view-product-list-->
                    <div id="view-product-list" class="view-product-list">
                        <h2 class="page-heading">
                            <span class="page-heading-title">
                                <asp:Label ID="lblUrunGrupAd" runat="server" Text=""></asp:Label></span>
                        </h2>
                        <ul class="display-product-option">
                            <li class="view-as-grid selected"><span>grid</span> </li>
                            <li class="view-as-list"><span>list</span> </li>
                        </ul>
                        <!-- PRODUCT LIST -->
                        <ul class="row product-list grid">
                            <!-- product -->
                            <asp:Repeater ID="rptUrunler" runat="server" OnItemDataBound="rptUrunler_ItemDataBound">
                                <ItemTemplate>
                                    <li class="col-xs-12 col-sm-4">
                                        <div class="product-container">
                                            <div class="left-block">
                                                <%--<div class="productCompare2">
                                                    <ul class="check-box-list" style="padding-top: 2px;">
                                                        <li>
                                                            <input type="checkbox" name="compare" id="compare<%#Container.ItemIndex %>" onchange="KarsilastirmaKaydetSil(this);"
                                                                value="<%#DataBinder.Eval(Container.DataItem,"urun_id") %>">
                                                            <label for="compare<%#Container.ItemIndex %>">
                                                                <span class="button"></span>Karşılaştır
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>--%>
                                                <a href="<%# DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%# DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                                                    <img src="<%# DataBinder.Eval(Container.DataItem, "resim_ad") %>" width="200" height="200"
                                                        alt="<%# DataBinder.Eval(Container.DataItem, "urun_ad") %>" /></a>
                                                <div class="add-to-cart">
                                                    <a title="Sepete Ekle" href="#" onclick="return SepetEkle(<%# DataBinder.Eval(Container.DataItem, "urun_id") %>);">
                                                        Sepete Ekle</a>
                                                </div>
                                            </div>
                                            <div class="right-block">
                                                <h5 class="product-name">
                                                    <a href="<%# DataBinder.Eval(Container.DataItem, "urun_link") %>" title="<%# DataBinder.Eval(Container.DataItem, "urun_ad") %>">
                                                        <%# DataBinder.Eval(Container.DataItem, "urun_ad") %></a></h5>
                                                <div class="content_price">
                                                    <span class="price product-price">
                                                        <asp:Label ID="lblNetTutar" runat="server" Text=""></asp:Label>
                                                    </span><span class="price old-price">
                                                        <asp:Label ID="lblBrutTutar" runat="server" Text=""></asp:Label>
                                                    </span><span>
                                                        <asp:Label ID="lblBilgi" CssClass="kdv" runat="server" Text=""></asp:Label></span>
                                                </div>
                                                <div>
                                                </div>
                                                <div class="info-orther">
                                                    <div class="product-desc">
                                                        <%# DataBinder.Eval(Container.DataItem, "aciklama") %>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                            <!-- product -->
                        </ul>
                        <!-- ./PRODUCT LIST -->
                    </div>
                    <!-- ./view-product-list-->
                    <div class="sortPagiBar">
                        <div class="bottom-pagination">
                            <nav>
                              <ul class="pagination">
                               <asp:Literal ID="ltrSayfalama" runat="server"></asp:Literal>
                              </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- ./ Center colunm -->
            </div>
            <!-- ./row-->
        </div>
    </div>
    <!--  FilterPriceSlider  -->
    <!-- footerbanner -->
    <div class="footerbanner">
    </div>
    <!-- footerbanner -->
    <script type="text/javascript">
        $('.slider-range-price').each(function () {
            var min = $(this).data('min');
            var max = $(this).data('max');
            var unit = $(this).data('unit');
            var value_min = $(this).data('value-min');
            var value_max = $(this).data('value-max');
            var label_reasult = $(this).data('label-reasult');
            var t = $(this);
            $(this).slider({
                range: true,
                min: min,
                max: max,
                values: [value_min, value_max],
                slide: function (event, ui) {
                    var result = label_reasult + " " + ui.values[0] + unit + ' - ' + ui.values[1] + unit;
                    console.log(t);
                    t.closest('.slider-range').find('.amount-range-price').html(result);
                },
                change: function (event, ui) {
                    FiyatFiltrele(ui.values[0], ui.values[1]);
                }
            });
            t.closest('.slider-range').find('.amount-range-price').html(label_reasult + " " + $(this).slider("values", 0) + unit + ' - ' + $(this).slider("values", 1) + unit);
        })

        function FiyatFiltrele(min, max) {
            if ($(".slider-range-price").slider("option", "max") == max && $(".slider-range-price").slider("option", "min") == min) {
                min = 0;
                max = 0;
            }
            $.ajax({
                type: 'POST', url: '/UrunListeleme.aspx/FiyatFiltrele',
                data: '{"minFiyat":"' + min + '", "maxFiyat" :"' + max + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    window.location.href = result.d;
                },
                error: function (result) {
                    alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                }
            });
        }


        function UrunleriSirala(SiralamaKriter) {
            $.ajax({
                type: 'POST', url: '/UrunListeleme.aspx/UrunleriSirala',
                data: '{"SiralamaKriter":"' + SiralamaKriter + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    window.location.href = result.d;
                },
                error: function (result) {
                    alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                }
            });
        }

        function ExtraOzellikFiltrele(secim, kbilesenid) {
            var secimDurum = $(secim).attr('class');

            if (secimDurum == 'passive') {
                secimDurum = 'Ekle';
            }
            else if (secimDurum == 'selected') {
                secimDurum = 'Cikart';
            }

            $.ajax({
                type: 'POST', url: '/UrunListeleme.aspx/ExtraOzellikFiltrele',
                data: '{"kbilesenid":"' + kbilesenid + '","durum":"' + secimDurum + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    window.location.href = result.d;
                },
                error: function (result) {
                    alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                }
            });
        }

        function ExtraOzellikCikart(kbilesenid) {

            $.ajax({
                type: 'POST', url: '/UrunListeleme.aspx/ExtraOzellikFiltrele',
                data: '{"kbilesenid":"' + kbilesenid + '","durum":"Cikart"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    window.location.href = result.d;
                },
                error: function (result) {
                    alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                }
            });
        } 
    </script>
    <!-- FilterChechkbox -->
    <script src="/js/jquery.uniform.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" charset="utf-8">        $(function () {
            $("input:not(.bultenBTN .htFacebookLogin), textarea, select:not(.dpSiralama), button").uniform();
        }); 
    </script>
    <link rel="stylesheet" href="/css/uniform.default.css" type="text/css" media="screen">
    <!-- FilterChechkbox -->
    <script type="text/javascript">
        function ShowMore(sender, senderContainer, cId) {
            if ($("#" + cId).css("display") == "none") {
                $("#" + senderContainer).delay(500).fadeOut(400);
                $("#" + sender).delay(800).slideUp(250);
                $("#" + cId).slideDown(500);
            }
            else {
                $("#" + cId).slideUp('fast');
                $("#" + sender).html('Diger');
            }
        }

        <%-- 
        function ShowSeoContent(sender) {
            $("#" + sender.id).hide();
            $("#<%=DivSeoContent.ClientID %>").html($("#<%=DivSeoContent.ClientID %>").html() + $("#<%=hdnSeoContent.ClientID %>").val());
        }
        --%>

        function SayfaYonlendir(sayfaNo) {
            $.ajax({ type: 'POST', url: '/UrunListeleme.aspx/SayfalamaYonlendir',
                data: '{ "SayfaNo" : "' + sayfaNo + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    location.href = result.d;
                },
                error:
    function () {
        alert('Talep esnasında sorun oluştu. Yeniden deneyin');
    }
            });
        }

        function KarsilastirmaKaydetSil(chkKarsilastir) {
            var UrunId = $(chkKarsilastir).val();
            var Kod = "";
            if ($(chkKarsilastir).is(':checked'))
                Kod = "kaydet";
            else Kod = "sil";
            $.ajax({
                type: 'POST',
                url: '/UrunListeleme.aspx/KarsilastirmaKaydetSil',
                data: '{ "UrunId" : "' + UrunId + '", "Kod" : "' + Kod + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    if (result.d != "")
                        location.href = "/MusteriHizmetleri/UyeGirisi.aspx";
                }, error: function (err) {
                    ;
                }
            });
        } </script>
    <asp:Literal ID="ltrRemarketingAlan" runat="server"></asp:Literal>
    <!------------SIRALA------------->
    <script type="text/javascript">
        $(document).ready(function (e) {
            try {
                $("#<%=dpSiralama.ClientID %>").msDropDown();
            } catch (e) {
                alert(e.message);
            }
        });
    </script>
</asp:Content>
