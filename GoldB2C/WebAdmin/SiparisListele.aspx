﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebAdmin/Admin.master" AutoEventWireup="true"
    CodeFile="SiparisListele.aspx.cs" Inherits="WebAdmin_SiparisListele" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .cariLink {
            color: red;
        }
        .cariLink:hover {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine-tr.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".accordionContainer .accordionBtn:first").addClass("active");
            $(".accordionContainer .accordionDetail:not(:first)").hide();

            $(".accordionContainer .accordionBtn").click(function () {
                $(this).next(".accordionDetail").slideToggle("slow")
		.siblings(".accordionDetail:visible").slideUp("slow");
                $(this).toggleClass("active");
                $(this).siblings(".accordionBtn").removeClass("active");
            });
        });



        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

        $(function () {
            $("#<%=txtTarih1.ClientID%>").datepicker();
            $("#<%=txtTarih2.ClientID%>").datepicker();
        });


        jQuery(function ($) {
            $.datepicker.regional['tr'] = {
                closeText: 'kapat',
                prevText: '&#x3c;geri',
                nextText: 'ileri&#x3e',
                currentText: 'bugün',
                monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran',
                'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
                monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz',
                'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
                dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
                dayNamesShort: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                dayNamesMin: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                weekHeader: 'Hf',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['tr']);
        });


    </script>
    <div style="float: left;">
        <asp:Panel ID="pnlCariId" runat="server">
            <asp:LinkButton ID="lnkCariAd" runat="server" CssClass="cariLink" OnClick="lnkCariAd_Click">Cari Ad 'a göre işleme devam et.</asp:LinkButton>&nbsp;&nbsp;<br />
            <br />
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblCariId1" runat="server" Text="Cari Id"> 
                </asp:Label></div>
            <asp:DropDownList ID="drpCariId1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpCariId1_SelectedIndexChanged">
            </asp:DropDownList>
            ve
            <asp:Label ID="lblCariId2" runat="server" Text="">
            </asp:Label>
            <asp:DropDownList ID="drpCariId2" runat="server">
            </asp:DropDownList>
            arasında&nbsp;&nbsp;
            <asp:CheckBox ID="chkCariIdAll" runat="server" Text="Tüm Cari ID 'ler" AutoPostBack="True"
                OnCheckedChanged="chkCariIdAll_CheckedChanged" />&nbsp;&nbsp; &nbsp;&nbsp;<br />
        </asp:Panel>
        <asp:Panel ID="pnlCariAd" runat="server" Visible="false">
            <asp:LinkButton ID="lnkCariId" runat="server" CssClass="cariLink" OnClick="lnkCariId_Click">Cari ID 'ye göre işleme devam et.</asp:LinkButton>&nbsp;&nbsp;<br />
            <br />
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblCariAd" runat="server" Text="Cari Ad"></asp:Label></div>
            <asp:DropDownList ID="drpCariAd" runat="server">
            </asp:DropDownList>
            &nbsp;&nbsp; &nbsp;&nbsp;<br />
        </asp:Panel>
        <asp:Panel ID="pnlSiparisNo" runat="server">
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblSiparisId1" runat="server" Text="Siparis No">
                </asp:Label></div>
            <asp:TextBox ID="txtSiparisId1" runat="server"></asp:TextBox>
            ve
            <asp:Label ID="lblSiparisId2" runat="server" Text="">
            </asp:Label>
            <asp:TextBox ID="txtSiparisId2" runat="server"></asp:TextBox>
            arasında&nbsp;&nbsp;<br />
        </asp:Panel>
        <asp:Panel ID="pnlTarih" runat="server">
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblTarih1" runat="server" Text="Tarih">
                </asp:Label></div>
            <asp:TextBox ID="txtTarih1" runat="server"></asp:TextBox>
            ve
            <asp:Label ID="lblTarih2" runat="server" Text="">
            </asp:Label>
            <asp:TextBox ID="txtTarih2" runat="server"></asp:TextBox>
            arasında&nbsp;&nbsp;<br />
        </asp:Panel>
    </div>
    <div style="float: right;">
        <asp:Label ID="lblSort" runat="server" Text="Sıralama ">
        </asp:Label>
        <asp:DropDownList ID="drpSort" runat="server">
            <asp:ListItem Value="siparis_tarih" Text=""></asp:ListItem>
            <asp:ListItem Value="cari_id">Cari Id</asp:ListItem>
            <asp:ListItem Value="cari_ad">Cari Ad</asp:ListItem>
            <asp:ListItem Value="siparis_id">Sipariş No</asp:ListItem>
            <asp:ListItem Value="siparis_tarih">Sipariş Tarihi</asp:ListItem>
            <asp:ListItem Value="siparis_tutar">Puan</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btnGetir" runat="server" Text="Getir" class="testEtBTN" OnClick="btnGetir_Click" />
        &nbsp;&nbsp;
        <asp:Button ID="btnExcelSend" runat="server" Text="Excel'e Aktar" class="testEtBTN"
            OnClick="btnExcelSend_Click" />
    </div>
    <br />
    <br />
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <!-- Place updatable markup and controls here. -->

    
    <table class="accountRoot" style="width: 1040px;">
        <%--<div class="acountTitle">
            Siparişlerim</div>--%>
        <!-- accordion content -->
        <tr class="accordionContainer" style="width: 1040px;">
            <td>
                <asp:Repeater ID="rptSiparisBilgileri" runat="server" OnItemDataBound="rptSiparisBilgileri_ItemDataBound" OnItemCommand="rptSiparisBilgileri_ItemCommand">
                    <HeaderTemplate>
                        <!-- title -->
                        <table>
                            <tr class="ordertitle" style="width: 1040px;">
                                <th class="title4">
                                    Ad
                                </th>
                                <th class="title2">
                                    Cari Id
                                </th>
                                <th class="title2">
                                    Cari Kod
                                </th>
                                <th class="title2">
                                    Sipariş Tarihi
                                </th>
                                <th class="title3">
                                    Sipariş No
                                </th>
                                <th class="title2">
                                    Puan
                                </th>
                                <th class="title4">
                                    Durum
                                </th>
                            </tr>
                            <!-- title -->
                    </HeaderTemplate>
                    <ItemTemplate>
                        <!-- chooes -->
                        <tr class="orderchooes" style="width: 1040px; border: 1px solid #ccc;">
                            <td class="chooes4">
                                <%#DataBinder.Eval(Container.DataItem, "cari_ad")%>
                            </td>
                            <td class="chooes2">
                                <%#DataBinder.Eval(Container.DataItem, "cari_id")%>
                            </td>
                            <td class="chooes2">
                                <%#DataBinder.Eval(Container.DataItem, "cari_kod")%>
                            </td>
                            <td class="chooes2">
                                <%#DataBinder.Eval(Container.DataItem, "siparis_tarih", "{0:dd.MM.yyyy}")%>
                            </td>
                            <td class="chooes3">
                                <%#DataBinder.Eval(Container.DataItem, "siparis_no")%>
                            </td>
                            <td class="chooes2">
                                <%#DataBinder.Eval(Container.DataItem, "siparis_tutar", "{0:###,###,###}")%>
                            </td>
                            <td class="chooes4">
                                <%#DataBinder.Eval(Container.DataItem, "siparis_statu")%>
                            </td>
                            <td class="accordionBtn">
                                Detaylar
                            </td>
                            <!-- accordion detail -->
                            <td class="accordionDetail">
                                <script>
                                    function ConfirmOnay() {
                                        if (confirm("Siparişi onaylamak istiyormusunuz?"))
                                            return true;
                                        else
                                            return false;
                                    }

                                    function ConfirmRet() {
                                        if (confirm("Siparişi reddetmek istiyormusunuz?"))
                                            return true;
                                        else
                                            return false;
                                    }
                                </script>
                                <!-- description -->
                                <%--    <div style="background-color: #e0e0e0; color: #cc0000; width: 724px; height: 21px;
                                margin-bottom: 5px; padding-top: 5px; text-align: center;">
                            </div>--%>
                                <!-- description -->
                                <asp:Repeater ID="rptSiparisDetayBilgileri" runat="server" OnItemCommand="rptSiparisDetayBilgileri_ItemCommand">
                                    <HeaderTemplate>
                                        <!-- detail -->
                                        <table class="orderdetail">
                                            <!-- title -->
                                            <tr class="odtitle" style="width: 1040px;">
                                                <th class="title1" style="width: 89px;">
                                                    Ürün Kodu
                                                </th>
                                                <th class="title2" style="width: 300px;">
                                                    Ürün Adı
                                                </th>
                                                <th class="title3" style="width: 71px;">
                                                    Birim Puanı
                                                </th>
                                                <th class="title4">
                                                    Adet
                                                </th>
                                                <%--<th class="title5">
                                                Kazancınız</th>--%>
                                                <th class="title6">
                                                    Toplam Puan
                                                </th>
                                                <th class="title3">
                                                    Onay Tarihi
                                                </th>

                                                 <th class="title10">
                                                    Durum
                                                </th>

                                                 <th class="title10">
                                                    OnayLa
                                                </th>
                                                <th class="title10">
                                                    Reddet
                                                </th>

                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <!-- chooes -->
                                        <tr class="odchooes" style="width: 1040px;">
                                            <td class="chooes1" style="width: 89px;">
                                                <%#ImagePath(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "siparis_id")),Convert.ToInt32(DataBinder.Eval(Container.DataItem, "urun_id")))%>
                                            </td>
                                            <td class="chooes2" style="width: 300px;">
                                                <%#DataBinder.Eval(Container.DataItem, "urun_ad")%>
                                            </td>
                                            <td class="chooes3" style="width: 71px;">
                                                <%#DataBinder.Eval(Container.DataItem, "birim_fiyat", "{0:###,###,###}")%>
                                            </td>
                                            <td class="chooes4">
                                                <%#DataBinder.Eval(Container.DataItem, "siparis_adet")%>
                                            </td>
                                            <%--<td class="chooes5">
                                            <%#DataBinder.Eval(Container.DataItem, "indirim_tutar", "{0:###,###,###}")%>&nbsp;</td>--%>
                                            <td class="chooes6">
                                                <%#DataBinder.Eval(Container.DataItem, "toplam_tutar", "{0:###,###,###}")%>
                                            </td>
                                            <td class="chooes3">
                                                <%# DataBinder.Eval(Container.DataItem, "onay1_onaytarih", "{0:dd.MM.yyyy}") == "01.01.0001" ? "" : DataBinder.Eval(Container.DataItem, "onay1_onaytarih", "{0:dd.MM.yyyy}")%> 
                                            </td>
                                            <td class="chooes10">
                                               <%# DataBinder.Eval(Container.DataItem, "onay_durum") %>
                                            </td>
                                             <td class="chooes10">
                                                 <asp:Button ID="btnOnayV2" runat="server" Text="Onay" class="" OnClientClick="return  ConfirmOnay()" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "siparis_detay_id") %>' CommandName="Onay"/>
                                            </td>
                                            <td class="chooes10">
                                                 <asp:Button ID="btnReddetV2" runat="server" Text="Reddet" class="" OnClientClick="return  ConfirmRet()" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "siparis_detay_id") %>' CommandName="Reddet"/>
                                            </td>
                                        </tr>
                                        <!-- chooes -->
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <!-- chooes -->
                                        <tr class="odchooes" style="width: 1040px;">
                                            <td class="chooes1 bgcolorff" style="width: 89px;">
                                                <%#ImagePath(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "siparis_id")),Convert.ToInt32(DataBinder.Eval(Container.DataItem, "urun_id")))%>
                                            </td>
                                            <td class="chooes2 bgcolorff" style="width: 300px;">
                                                <%#DataBinder.Eval(Container.DataItem, "urun_ad")%>
                                            </td>
                                            <td class="chooes3 bgcolorff" style="width: 71px;">
                                                <%#DataBinder.Eval(Container.DataItem, "birim_fiyat", "{0:###,###,###}")%>
                                            </td>
                                            <td class="chooes4 bgcolorff">
                                                <%#DataBinder.Eval(Container.DataItem, "siparis_adet")%>
                                            </td>
                                            <%--<td class="chooes5 bgcolorff">
                                            <%#DataBinder.Eval(Container.DataItem, "indirim_tutar", "{0:###,###,###}")%>&nbsp;</td>--%>
                                            <td class="chooes6 bgcolorff">
                                                <%#DataBinder.Eval(Container.DataItem, "toplam_tutar", "{0:###,###,###}")%>
                                            </td>
                                            <td class="chooes3">
                                                <%# DataBinder.Eval(Container.DataItem, "onay1_onaytarih", "{0:dd.MM.yyyy}") == "01.01.0001" ? "" : DataBinder.Eval(Container.DataItem, "onay1_onaytarih", "{0:dd.MM.yyyy}")%>
                                           
                                            </td>

                                            <td class="chooes10">
                                                 <%# DataBinder.Eval(Container.DataItem, "onay_durum") %>
                                            </td>

                                            <td class="chooes10">
                                                 <asp:Button ID="btnOnay" runat="server" Text="Onay" class="" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "siparis_detay_id") %>' CommandName="Onay" />
                                            </td>
                                            <td class="chooes10">
                                                   <asp:Button ID="btnReddet" runat="server" Text="Reddet" class=""  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "siparis_detay_id") %>' CommandName="Reddet" />
                                            </td>

                                        </tr>
                                        <!-- chooes -->
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                        <!-- detail -->
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                            <!-- accordion detail -->
                        </tr>
                        <!-- chooes -->
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <!-- accordion content -->
        <!-- order button -->
        <%--<div class="orderbuttons">
        </div>--%>
        <!-- order button -->
    </table>
    <div id="divExcelBilgileri" style="display: none;">
        <table class="accountRoot" style="width: 1040px; display: none;">
            <%--<div class="acountTitle">
            Siparişlerim</div>--%>
            <!-- accordion content -->
            <tr class="accordionContainer" style="width: 1040px;">
                <td>
                    <asp:Repeater ID="rptExcelBilgileri" runat="server" OnItemDataBound="rptExcelBilgileri_ItemDataBound">
                        <HeaderTemplate>
                            <!-- title -->
                            <table>
                                <tr class="ordertitle" style="width: 1040px;">
                                    <td class="accordionDetail">
                                        <table class="orderdetail">
                                            <tr class="odtitle" style="width: 1040px;">
                                                <th class="title4">
                                                    Ad
                                                </th>
                                                <th class="title2">
                                                    Usta Id
                                                </th>
                                                <th class="title2">
                                                    Usta Kod
                                                </th>
                                                <th class="title2">
                                                    Sipariş Tarihi
                                                </th>
                                                <th class="title3">
                                                    Sipariş No
                                                </th>
                                                <th class="title2">
                                                    Puan
                                                </th>
                                                <th class="title4">
                                                    Durum
                                                </th>
                                                <th class="accordionBtn">
                                                    Detaylar
                                                </th>
                                            </tr>
                                        </table>
                                    </td>
                                    <th class="title1" style="width: 89px;">
                                        Ürün Kodu
                                    </th>
                                    <th class="title2" style="width: 300px;">
                                        Ürün Adı
                                    </th>
                                    <th class="title3" style="width: 71px;">
                                        Birim Puanı
                                    </th>
                                    <th class="title4">
                                        Adet
                                    </th>
                                    <%--<th class="title5">
                         Kazancınız</th>--%>
                                    <th class="title6">
                                        Toplam Puan
                                    </th>
                                    <th class="title3">
                                        Onay Tarihi
                                    </th>
                                </tr>
                                <!-- title -->
                        </HeaderTemplate>
                        <ItemTemplate>
                            <!-- chooes -->
                            <tr class="orderchooes" style="width: 1040px; border: 1px solid #ccc;">
                                <!-- accordion detail -->
                                <td class="accordionDetail">
                                    <!-- description -->
                                    <%--    <div style="background-color: #e0e0e0; color: #cc0000; width: 724px; height: 21px;
                                margin-bottom: 5px; padding-top: 5px; text-align: center;">
                            </div>--%>
                                    <!-- description -->
                                    <asp:Repeater ID="rptExcelDetayBilgileri" runat="server">
                                        <HeaderTemplate>
                                            <!-- detail -->
                                            <table class="orderdetail">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <!-- chooes -->
                                            <tr class="odchooes" style="width: 1040px; border: 1px solid #ccc;">
                                                <td class="chooes4">
                                                    <%#DataBinder.Eval(Container.DataItem, "cari_ad")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "cari_id")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "cari_kod")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_tarih", "{0:dd.MM.yyyy}")%>
                                                </td>
                                                <td class="chooes3">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_no")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_tutar", "{0:###,###,###}")%>
                                                </td>
                                                <td class="chooes4">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_statu")%>
                                                </td>
                                                <td class="accordionBtn">
                                                    Detaylar
                                                </td>
                                            </tr>
                                            <!-- chooes -->
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="odchooes" style="width: 1040px; border: 1px solid #ccc;">
                                                <td class="chooes4">
                                                    <%#DataBinder.Eval(Container.DataItem, "cari_ad")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "cari_id")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "cari_kod")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_tarih", "{0:dd.MM.yyyy}")%>
                                                </td>
                                                <td class="chooes3">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_no")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_tutar", "{0:###,###,###}")%>
                                                </td>
                                                <td class="chooes4">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_statu")%>
                                                </td>
                                                <td class="accordionBtn">
                                                    Detaylar
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                            <!-- detail -->
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </td>
                                <td class="chooes1" style="width: 89px;">
                                    <%#DataBinder.Eval(Container.DataItem, "urun_id")%>
                                </td>
                                <td class="chooes2" style="width: 300px;">
                                    <%#DataBinder.Eval(Container.DataItem, "urun_ad")%>
                                </td>
                                <td class="chooes3" style="width: 71px;">
                                    <%#DataBinder.Eval(Container.DataItem, "birim_fiyat", "{0:###,###,###}")%>
                                </td>
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "siparis_adet")%>
                                </td>
                                <%--<td class="chooes5">
                        <%#DataBinder.Eval(Container.DataItem, "indirim_tutar", "{0:###,###,###}")%>&nbsp;</td>--%>
                                <td class="chooes6">
                                    <%#DataBinder.Eval(Container.DataItem, "toplam_tutar", "{0:###,###,###}")%>
                                </td>
                                <td class="chooes3">
                                    <%# DataBinder.Eval(Container.DataItem, "onay1_onaytarih", "{0:dd.MM.yyyy}") == "01.01.0001" ? "" : DataBinder.Eval(Container.DataItem, "onay1_onaytarih", "{0:dd.MM.yyyy}")%>
                                </td>
                                <!-- accordion detail -->
                            </tr>
                            <!-- chooes -->
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </div>

            </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
