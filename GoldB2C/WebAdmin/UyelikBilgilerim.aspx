﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebAdmin/Admin.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="UyelikBilgilerim.aspx.cs" Inherits="WebAdmin_UyelikBilgilerim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/goldForms.css?v=2" rel="stylesheet" type="text/css" />
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">
        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }
    </script>
    <div class="contentform">
        <!-- loginforms -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Adınız</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtAd" CssClass="w230" runat="server" Enabled="false"></asp:TextBox>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqAd" runat="server" ControlToValidate="txtAd" ValidationGroup="grpKullaniciBilgileri"
                        ErrorMessage="Adınızı girin."></asp:RequiredFieldValidator></span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Soyadınız</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtSoyad" CssClass="w230 pr5 mr8" runat="server" Enabled="false"></asp:TextBox>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqSoyad" runat="server" ControlToValidate="txtSoyad"
                        ValidationGroup="grpKullaniciBilgileri" ErrorMessage="Soyadınızı girin."></asp:RequiredFieldValidator></span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">E-Posta</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtEmail" CssClass="w230" runat="server" Enabled="false"></asp:TextBox>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqEmail" runat="server" ControlToValidate="txtEmail"
                        ValidationGroup="grpKullaniciBilgileri" ErrorMessage="E-Posta adresinizi girin."
                        Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rqEmailExp" runat="server" ErrorMessage="Lütfen geçerli bir E-Posta adresi girin."
                        ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ValidationGroup="grpKullaniciBilgileri" Display="Dynamic"></asp:RegularExpressionValidator>
                </span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">E-Posta (tekrar)</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtEmailTekrar" CssClass="w230 pr5 mr8" runat="server" Enabled="false"></asp:TextBox>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqEmailTekrar" runat="server" ControlToValidate="txtEmailTekrar"
                        ValidationGroup="grpKullaniciBilgileri" ErrorMessage="E-Posta adresinizi tekrar girin."
                        Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rqEmailTekrarExp" runat="server" ErrorMessage="Lütfen geçerli bir E-Posta adresi girin."
                        ControlToValidate="txtEmailTekrar" ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z\-])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
                        ValidationGroup="grpKullaniciBilgileri" Display="Dynamic"></asp:RegularExpressionValidator>
                </span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Doğum Tarihi</span>
            </div>
            <div class="fleft w350 clearfix">
                <div class="fleft">
                    <asp:DropDownList ID="dpDogumGun" CssClass="w60" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="fleft mr5 mt3 pt4">
                    /</div>
                <div class="fleft">
                    <asp:DropDownList ID="dpDogumAy" CssClass="w60" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="fleft mr5 mt3 pt4">
                    /</div>
                <div class="fleft">
                    <asp:DropDownList ID="dpDogumYil" CssClass="w60" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="fleft w350 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqdpDogumGun" InitialValue="0" runat="server" ControlToValidate="dpDogumGun"
                        ErrorMessage="*Doğduğunuz günü seçin." ValidationGroup="grpKullaniciBilgileri"></asp:RequiredFieldValidator>&nbsp;
                    <asp:RequiredFieldValidator ID="rqdpDogumAy" InitialValue="0" runat="server" ControlToValidate="dpDogumAy"
                        ErrorMessage="*Doğduğunuz ayı seçin." ValidationGroup="grpKullaniciBilgileri"
                        Display="Dynamic"></asp:RequiredFieldValidator>&nbsp;
                    <asp:RequiredFieldValidator ID="rqdpDogumYil" InitialValue="0" runat="server" ControlToValidate="dpDogumYil"
                        ErrorMessage="*Doğduğunuz yılı seçin." ValidationGroup="grpKullaniciBilgileri"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Cinsiyet</span>
            </div>
            <div class="fleft w230 clearfix">
                <table border="0">
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbKadin" Text="Kadın" runat="server" Checked="true" GroupName="Cinsiyet" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rbErkek" Text="Erkek" runat="server" GroupName="Cinsiyet" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification"><span style="visibility: hidden;">*Cinsiyetinizi girin.</span>
                </span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Şehir</span>
            </div>
            <div class="fleft w350 clearfix">
                <div class="fleft w350 h29">
                    <asp:DropDownList ID="dpSehir" CssClass="w240" onChange="getIlceListesi(this.value);"
                        runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqSehir" runat="server" ControlToValidate="dpSehir"
                        InitialValue="0" ValidationGroup="grpKullaniciBilgileri" ErrorMessage="Şehir seçin"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">İlçe</span>
            </div>
            <div class="fleft w350 clearfix">
                <div class="fleft w350 h29">
                    <asp:DropDownList ID="dpIlce" CssClass="w240" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification"></span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Cep Telefonu Numarası</span>
            </div>
            <div class="fleft w350 clearfix">
                <div class="fleft w50">
                    <asp:TextBox ID="txtCepTelKod" CssClass="fleft w35 pr5 mr8 ml8" runat="server"></asp:TextBox>
                </div>
                <div class="fleft ml5 mr5 mt3 pt4">
                    /</div>
                <div class="fleft w170">
                    <asp:TextBox ID="txtCepTelNo" CssClass="fleft w165 pr5 mr8" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification"><span style="visibility: hidden;">*Cep telefonu numaranızı
                    girin.</span> </span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Telefonu Numarası</span>
            </div>
            <div class="fleft w350 clearfix">
                <div class="fleft w50">
                    <asp:TextBox ID="txtTelKod" CssClass="fleft w35 pr5 mr8 ml8" runat="server"></asp:TextBox>
                </div>
                <div class="fleft ml5 mr5 mt3 pt4">
                    /</div>
                <div class="fleft w170">
                    <asp:TextBox ID="txtTelNo" CssClass="fleft w165 pr5 mr8" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification"></span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <%-- <div id="DivTavsiyeEden" runat="server" class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Tavsiye Eden Kişi Email </span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtRefEmail" CssClass="w230" runat="server"></asp:TextBox>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RegularExpressionValidator ID="rqExRefEmail" runat="server" ErrorMessage="Lütfen geçerli bir E-Posta adresi girin."
                        ControlToValidate="txtRefEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ValidationGroup="grpKullaniciBilgileri"></asp:RegularExpressionValidator>
                </span>
            </div>
        </div>--%>
        <!-- input -->
        <%-- <div class="fleft w764 mt40 ml28">
            <div class="trans">
                <asp:CheckBox ID="chkEposta" CssClass="fontSz12 chcktxt" Text="Kampanyalardan e-posta ile haberdar olmak istiyorum"
                    runat="server" />
            </div>
        </div>
        <div class="fleft w764 mt10 ml28">
            <div class="trans">
                <asp:CheckBox ID="chkSms" CssClass="fontSz12 chcktxt" Text="Kampanyalardan SMS ile haberdar olmak istiyorum"
                    runat="server" />
            </div>
        </div>--%>
        <!-- button -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <asp:LinkButton ID="btnGuncelle" CssClass="link-button-RedBtn spriteRedBtn" runat="server"
                OnClick="btnGuncelle_Click" ValidationGroup="grpKullaniciBilgileri"><span class="spriteRedBtn fontTre fontSz16">
                Bilgilerimi Güncelle</span></asp:LinkButton>
        </div>
        <!-- button -->
        <!-- loginforms -->
    </div>
    <script type="text/javascript">
        jQuery(function ($) {
            $("#<%=txtCepTelKod.ClientID %>").mask("599");
            $("#<%=txtCepTelNo.ClientID %>").mask("9999999");
            $("#<%=txtTelKod.ClientID %>").mask("999");
            $("#<%=txtTelNo.ClientID %>").mask("9999999");
        });
    </script>
    <script type="text/javascript">
        function getIlceListesi(SehirKod) {
            $.ajax({
                type: 'POST',
                url: 'http://www.biges.com/bilgilerim/getIlceListesi',
                data: '{ "SehirKod":"' + SehirKod + '" }',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    $("#<%=dpIlce.ClientID %>").find('option').remove().end().append('<option value="0">Seçiniz</option>');
                    $.each(result.d, function (index, data) {
                        $("#<%=dpIlce.ClientID %>").append(
                      $('<option value="' + data.IlceAd + '"></option>').html(data.IlceAd));
                    });
                },
                error: function () {
                    alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                }
            });
        }
    </script>
</asp:Content>
