﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Net.Mail;


public partial class WebAdmin_KullaniciPuanLog : System.Web.UI.Page
{
    SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    DataSet dsSiparis;
    DataView dvSorted;
    DataTable sortedDT;

    DataTable dtLogBilgileri;
    LogIslemleriBLL bllLogIslemleri = new LogIslemleriBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            drpKullaniciAdDoldur();

        }

    }


    protected void btnGetir_Click(object sender, EventArgs e)
    {

        int siparisId;
        string crmCustomerGUID;

        Int32.TryParse(txtSiparisId.Text.ToString(), out siparisId);
        crmCustomerGUID = drpKullaniciAd.SelectedValue.ToString();


        dtLogBilgileri = bllLogIslemleri.KullaniciPuanLogBilgileriGetir(crmCustomerGUID, siparisId);

        
        rptLogBilgileri.DataSource = dtLogBilgileri;
        rptLogBilgileri.DataBind();


    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + mesaj + "</td></tr></table></div>');</script>";
    }

    public void drpKullaniciAdDoldur()
    {
        DataTable dtUyeBilgileri = new DataTable();

        dtUyeBilgileri = bllUyelikIslemleri.GetirKullaniciAll();

        var sonuc = (from k in dtUyeBilgileri.AsEnumerable()
                     where k.Field<int>("UyelikTipi") != 1//Admin kullanıcılarını getirme!..
                     orderby k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad")
                     select new
                     {
                         CRMCustomerGUID = k.Field<string>("CRMCustomerGUID"),
                         KullaniciAdSoyad = string.Format("{0} {1}", k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad"))
                     }).ToList();


        drpKullaniciAd.DataTextField = "KullaniciAdSoyad";
        drpKullaniciAd.DataValueField = "CRMCustomerGUID";
        drpKullaniciAd.DataSource = sonuc;
        drpKullaniciAd.DataBind();
        drpKullaniciAd.Items.Insert(0, (new ListItem(string.Empty, "-1")));

    }



    public bool IsValidMail(string emailaddress)
    {
        try
        {
            MailAddress m = new MailAddress(emailaddress);
            return true;
        }
        catch (FormatException)
        {
            return false;
        }
    }
    
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=PuanLog.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";

        Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
        Response.Charset = "windows-1254";

        string header = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title></title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1254\" />\n<style>\n</style>\n</head>\n<body>\n";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        rptLogBilgileri.RenderControl(htmlWrite);
        Response.Write(header + stringWrite.ToString());
        Response.End();
    }
}