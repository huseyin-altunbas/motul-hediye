﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebAdmin/Admin.master" AutoEventWireup="true"
    CodeFile="Sorular.aspx.cs" Inherits="WebAdmin_AnketGoster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/goldHeader.css?v=1.2.6" rel="stylesheet" type="text/css" />
    <link href="/css/goldForms.css?v=2" rel="stylesheet" type="text/css" />
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <script src="../js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="../js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="../js/jquery.validationEngine-tr.js" type="text/javascript"></script>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <link href="../uploadify/uploadify.css" rel="stylesheet" type="text/css" />
    <script src="../uploadify/jquery.uploadify.js" type="text/javascript" />
    <script src="../uploadify/jquery.uploadify.min.js" type="text/javascript" />
    <script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtSoru.ClientID %>").live('keyup blur', function () {
                var maxlength = "750";
                var val = $(this).val();

                if (val.length > maxlength) {
                    $(this).val(val.slice(0, maxlength));
                }
            });

            $("#<%=txtSoruAra.ClientID %>").live('keyup blur', function () {
                var maxlength = "200";
                var val = $(this).val();

                if (val.length > maxlength) {
                    $(this).val(val.slice(0, maxlength));
                }
            });

            $("#divSoruAra").click(function () {
                $("#<%=divSoruAraIcerik.ClientID %>").slideToggle("slow", function () {
                    $("#<%=divSoruKaydetIcerik.ClientID %>").slideUp("slow");
                });
            });

            $("#divSoruKaydet").click(function () {
                $("#<%=divSoruKaydetIcerik.ClientID %>").slideToggle("slow", function () {
                    $("#<%=divSoruAraIcerik.ClientID %>").slideUp("slow");
                });
            });

        });

        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

        function SoruAcKapat(soruId, durum) {
            var message = "";
            if (durum)
                message = "Soruyu açmak istiyor musunuz?";
            else
                message = "Soruyu kapatmak istiyor musunuz?";

            var con = confirm(message);
            if (con) {

                $.ajax({
                    type: 'POST',
                    url: '/WebAdmin/Sorular.aspx/SoruAcKapat',
                    data: '{ "soruId":' + soruId + ',"durum":' + durum + '}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        if (result.d == "1")
                            window.location.href = "/WebAdmin/Sorular.aspx";
                        else if (result.d == "0")
                            window.location.href = "/WebAdmin/UyeGirisi.aspx";
                        else if (result.d == "-1") {
                            $("div.loading").hide();
                            $("body").css({ "opacity": "" });
                            alert("Soruyu açmak ya da  kapatmak için yetkiniz bulunmamaktadır.");
                        }
                    },
                    beforeSend: function () {
                        $("body").css({ "opacity": "0.5" });
                        $("div.loading").show();
                    },
                    error: function (result) {
                        $("div.loading").hide();
                        $("body").css({ "opacity": "" });
                    }
                });
            }
        }
        

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            GetFiles($("#<%=hdnId.ClientID %>").val());
            $("#file_upload").uploadify(
            {
                'swf': '../uploadify/uploadify.swf',
                'uploader': '../Common/TicketUploadQuestions.ashx',
                'auto': true,
                'method': 'post',
                'multi': true,
                'buttonText': 'Dosya Seç',
                'buttonClass': 'link-button-RedBtn spriteRedBtn',
                'onUploadStart': function (file) {
                    $("#file_upload").uploadify('settings', 'formData', { 'FolderId': $("#<%=hdnId.ClientID %>").val() });
                },
                'onUploadSuccess': function (file, data, response) {
                    var dataS = data.split("&");
                    var FolderId = dataS[0].toString();
                    var FileName = dataS[1].toString();
                    if ($("#<%=hdnId.ClientID %>").val() == "") {
                        $("#<%=hdnId.ClientID %>").val(FolderId);
                    }

                    GetFiles(FolderId);

                }
            });

        });

        function GetFiles(folderId) {

            var folderHtml = "";
            $("#file_upload_success").html(folderHtml);

            if (folderId != "") {
                $.ajax({
                    type: 'POST',
                    url: '/WebAdmin/Sorular.aspx/GetFiles',
                    data: '{ "folderId":"' + folderId + '"}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        $.each(result.d, function (index, value) {
                            var _fileName = value.FileName.split("=")[1].toString();
                            folderHtml = folderHtml + "<div style=\"margin:10px;font-size:16px;\"><span>" + _fileName + "</span><a style=\"margin:10px;color:Red;font-size:16px;\" target=\"_blank\" href=\"../TicketImage/TicketQuestions/" + value.FolderId + "/" + value.FileName + "\"> Önizleme</a>-<a style=\"color:#0099cc;cursor:pointer\" OnClick=\"FileDelete('" + value.FolderId + "','" + value.FileName + "');return false;\" >Sil</a></div>";
                            $("#file_upload_success").html(folderHtml);

                        });
                    }
                });
            }
        }

        function FileDelete(folderId, fileName) {
            var con = confirm('Silmek istediğinize emin misiniz?');
            if (con) {
                if (folderId != "" && fileName != "") {
                    $.ajax({
                        type: 'POST',
                        url: '/WebAdmin/Sorular.aspx/FileDelete',
                        data: '{ "folderId":"' + folderId + '","fileName":"' + fileName + '"}',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function (result) {
                            if (result.d == "1")
                                GetFiles(folderId);
                        }
                    });
                }
            }
        }
        
    </script>
    <asp:HiddenField ID="hdnId" runat="server" />
    <div class="contentform" style="float: left; width: 850px;">
        <strong>Ticket</strong>
    </div>
    <br />
    <br />
    <div id="divSSS" class="contentform" style="float: left; width: 940px; background-color: #ccc;
        cursor: pointer; height: 20px;">
        <img src="images/sss.gif" />
    </div>
    <div id="divSSSIcerik" class="contentform" style="float: left; width: 940px; border: 1px solid #ccc;">
        • <strong>Gönderilen ürünün içerisinde neden garanti belgesi bulunmamaktadır?</strong>
        <br />
        <div style="padding-left: 20px; width: 800px;">
            - Gönderilen tüm ürünlerin garanti belgeleri orijinal kutularının içerisinde mevcuttur.</div>
        <div style="padding-left: 20px; width: 800px;">
            - Garanti belgeleri sonradan temin edilen evraklar olmadığından dolayı; Koliyi açtıktan
            sonra bir süre atmamanızı ve içini dikkatlice kontrol etmenizi önemle rica ediyoruz.</div>
    </div>
    <div style="clear: both;">
    </div>
    <br />
    <div id="divSoruAra" class="contentform" style="float: left; width: 940px; background-color: #ccc;
        cursor: pointer; height: 20px;">
        <img src="images/soruAra.gif" />
    </div>
    <div id="divSoruAraIcerik" class="contentform" style="float: left; width: 940px;
        border: 1px solid #ccc;" runat="server">
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Soru No</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtAraSoruNo" CssClass="w230" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Sipariş No</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtAraSiparisNo" CssClass="w230" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Kullanıcı</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:DropDownList ID="drpAraKullanici" CssClass="w230" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Soru Başlık</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:DropDownList ID="drpAraBaslik" CssClass="w230" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Soru İçerisinde Ara</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtSoruAra" CssClass="w230" runat="server"></asp:TextBox><span style="color: #00965E;
                    font-style: italic; font-size: 10px;">(En fazla 200 karakter)</span>
            </div>
        </div>
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Soru (Aktif-Pasif)</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:DropDownList ID="drpAktifPasif" CssClass="w230" runat="server">
                    <asp:ListItem Selected="True" Value="-1">Hepsi</asp:ListItem>
                    <asp:ListItem Value="1">Aktif</asp:ListItem>
                    <asp:ListItem Value="0">Pasif</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div style="clear: both;">
        </div>
        <div class="fleft h45 mt20 ml28 color4e" style="margin-top: 22px;">
            <asp:LinkButton ID="lnkSoruAra" CssClass="link-button-RedBtn spriteRedBtn" runat="server"
                ValidationGroup="grpSoruAra" OnClick="lnkSoruAra_Click"><span class="spriteRedBtn fontTre fontSz16">
                Soru Ara</span></asp:LinkButton>
        </div>
        <div style="clear: both;">
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <br />
    <div id="divSoruKaydet" class="contentform" style="float: left; width: 940px; background-color: #ccc;
        cursor: pointer; height: 20px;">
        <img src="images/soruKaydet.gif" />
    </div>
    <div id="divSoruKaydetIcerik" class="contentform" style="float: left; width: 940px;
        border: 1px solid #ccc; display: none;" runat="server">
        <div class="contentform" style="float: left; width: 850px;">
            <div class="fleft h45 color4e mt20 ml28 w350">
                <div class="fleft w350 h23 strong">
                    <span class="fleft mt3 fontSz13">Sipariş No</span>
                </div>
                <div class="fleft w230 clearfix">
                    <asp:TextBox ID="txtSiparisNo" CssClass="w230" runat="server"></asp:TextBox>
                </div>
                <div class="fleft w240 h23 colorb0 fontTre">
                    <span class="fright notification">
                        <asp:RequiredFieldValidator ID="rqSiparisNo" runat="server" ControlToValidate="txtSiparisNo"
                            ValidationGroup="grpSoru" ErrorMessage="Sipariş numaranızı girin."></asp:RequiredFieldValidator></span>
                </div>
            </div>
        </div>
        <div class="contentform" style="float: left; width: 850px;">
            <div class="fleft h45 color4e mt20 ml28 w350">
                <div class="fleft w350 h23 strong">
                    <span class="fleft mt3 fontSz13">Kullanıcı</span>
                </div>
                <div class="fleft w230 clearfix">
                    <asp:DropDownList ID="drpKullanici" CssClass="w230" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="fleft w240 h23 colorb0 fontTre">
                    <span class="fright notification">
                        <asp:RequiredFieldValidator ID="rqKullanici" InitialValue="-1" runat="server" ControlToValidate="drpKullanici"
                            ValidationGroup="grpSoru" ErrorMessage="Kullanıcı seçin."></asp:RequiredFieldValidator></span>
                </div>
            </div>
        </div>
        <div class="contentform" style="float: left; width: 850px;">
            <div class="fleft h45 color4e mt20 ml28 w350">
                <div class="fleft w350 h23 strong">
                    <span class="fleft mt3 fontSz13">Başlık</span>
                </div>
                <div class="fleft w230 clearfix">
                    <asp:DropDownList ID="drpBaslik" CssClass="w230" runat="server" OnSelectedIndexChanged="drpBaslik_SelectedIndexChanged"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
                <div class="fleft w240 h23 colorb0 fontTre">
                    <span class="fright notification">
                        <asp:RequiredFieldValidator ID="rqBaslik" InitialValue="-1" runat="server" ControlToValidate="drpBaslik"
                            ValidationGroup="grpSoru" ErrorMessage="Soru başlığı seçin."></asp:RequiredFieldValidator></span>
                </div>
            </div>
        </div>
        <div class="contentform" style="float: left; width: 850px;">
            <div class="fleft h45 color4e mt20 ml28 w350">
                <div class="fleft w350 h23 strong">
                    <span class="fleft mt3 fontSz13">Soru</span>
                </div>
                <div class="fleft w230 clearfix">
                    <asp:TextBox ID="txtSoru" CssClass="w230" runat="server" TextMode="MultiLine" Width="600"
                        Height="65"></asp:TextBox><span style="color: #00965E; font-style: italic; font-size: 10px;">(En
                            fazla 750 karakter)</span>
                </div>
                <div class="fleft w240 h23 colorb0 fontTre">
                    <span class="fright notification">
                        <asp:RequiredFieldValidator ID="rqSoru" runat="server" ControlToValidate="txtSoru"
                            ValidationGroup="grpSoru" ErrorMessage="Sorunuzu girin."></asp:RequiredFieldValidator></span>
                </div>
            </div>
            <div style="clear: both;">
            </div>
            <br />
            <div class="upload-wrap" style="margin: 60px 20px 20px 20px;">
                <input id="file_upload" type="file" name="file_upload" />
            </div>
            <div id="file_upload_success">
            </div>
            <div class="fleft h45 color4e mt20 ml28 w350">
                <asp:LinkButton ID="lnkKaydet" CssClass="link-button-RedBtn spriteRedBtn" runat="server"
                    ValidationGroup="grpSoru" OnClick="lnkKaydet_Click"><span class="spriteRedBtn fontTre fontSz16">
                Kaydet</span></asp:LinkButton>
            </div>
        </div>
    </div>
    <br />
    <div style="clear: both;">
    </div>
    <br />
    <table class="accountRoot" style="width: 940px;">
        <tr class="accordionContainer" style="width: 940px;">
            <td>
                <asp:Repeater ID="rptSorular" runat="server">
                    <HeaderTemplate>
                        <table>
                            <tr class="ordertitle" style="width: 940px; height: 40px;">
                                <th class="title3" style="width: 40px; font-size: 12px;">
                                    Soru No
                                </th>
                                <th class="title3" style="width: 110px; font-size: 12px;">
                                    Soru Açma Tarihi<br />
                                    Soru Kapatma Tarihi
                                </th>
                                <th class="title3" style="width: 120px; font-size: 12px;">
                                    Soruyu Açan<br />
                                    Soruyu Kapatan
                                </th>
                                <th class="title5" style="width: 100px; font-size: 12px;">
                                    Soru Başlığı
                                </th>
                                <th class="title5" style="width: 260px; font-size: 12px;">
                                    Soru
                                </th>
                                <th class="title3" style="width: 30px;">
                                    Aktif
                                </th>
                                <th class="title3" style="width: 40px; font-size: 12px;">
                                    Sipariş No
                                </th>
                                <th class="title3" style="width: 100px; font-size: 12px;">
                                    Kullanıcı
                                </th>
                                <th class="title3" style="width: 60px; font-size: 12px;">
                                    Cevaplar
                                </th>
                            </tr>
                            <tr class="ordertitle" style="width: 940px; height: 40px; margin-top: 0px; padding-top: 0px;">
                                <th style="width: 900px; font-size: 12px; margin-top: 0px; padding-top: 0px;">
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id="trOrderChooes" runat="server" class="orderchooes" style="width: 940px; border: 1px solid #ccc;
                            background-color: White; margin-bottom: 0px;">
                            <td class="chooes3" style="width: 40px; font-size: 12px;">
                                <%#DataBinder.Eval(Container.DataItem, "SoruId")%>
                            </td>
                            <td class="chooes3" style="width: 110px; font-size: 12px;">
                                <%#DataBinder.Eval(Container.DataItem, "SoruTarihi", "{0:dd.MM.yyyy HH:mm}")%><br />
                                <%#string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "SoruKapatmaTarihi").ToString()) ? "---": DataBinder.Eval(Container.DataItem, "SoruKapatmaTarihi", "{0:dd.MM.yyyy HH:mm}")%>
                            </td>
                            <td class="chooes3" style="width: 120px; font-size: 12px;">
                                <%#string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "SoruAcanKullaniciAdSoyad").ToString()) ? "" : DataBinder.Eval(Container.DataItem, "SoruAcanKullaniciAdSoyad")%><br />
                                <%#string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "SoruKapatanKullaniciAdSoyad").ToString()) ? "---" : DataBinder.Eval(Container.DataItem, "SoruKapatanKullaniciAdSoyad")%>
                            </td>
                            <td class="chooes5" style="width: 100px; font-size: 12px;">
                                <%#DataBinder.Eval(Container.DataItem, "SoruBaslik")%>
                            </td>
                            <td class="chooes5" style="width: 260px; font-size: 12px;">
                                <%#DataBinder.Eval(Container.DataItem, "Soru")%>
                            </td>
                            <td class="chooes3" style="width: 30px;">
                                <%#DataBinder.Eval(Container.DataItem, "AktifPasifIslem")%>
                            </td>
                            <td class="chooes3" style="width: 40px; font-size: 12px;">
                                <%#DataBinder.Eval(Container.DataItem, "SiparisId")%>
                            </td>
                            <td class="chooes3" style="width: 100px; font-size: 12px;">
                                <%#DataBinder.Eval(Container.DataItem, "KullaniciAdSoyad")%>
                            </td>
                            <td class="chooes3" style="width: 60px; font-weight: bold; text-transform: uppercase;
                                font-size: 12px;">
                                <a style="color: #0099cc;" href='/WebAdmin/Cevaplar.aspx?SoruId=<%#DataBinder.Eval(Container.DataItem, "SoruId")%>'>
                                    Cevaplar<br />
                                    (<%#DataBinder.Eval(Container.DataItem, "CevapSayisi")%>)</a>
                            </td>
                        </tr>
                        <tr id="trAdd" runat="server" style="width: 940px; background-color: #FFF7ED; margin-bottom: 20px;
                            padding-top: 0px;">
                            <td style="width: 900px; font-size: 12px; margin-top: 0px; padding-top: 0px;">
                                Dosyalar :&nbsp;
                                <%#DataBinder.Eval(Container.DataItem, "DosyaYolu")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
    <div id="divLoad" class="loading" style="display: none;">
        <img src="../ImagesGld/ajax-loader.gif" style="position: fixed; left: 50%; top: 50%;" />
    </div>
</asp:Content>
