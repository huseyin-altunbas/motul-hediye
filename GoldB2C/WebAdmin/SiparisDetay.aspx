﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SiparisDetay.aspx.cs" Inherits="WebAdmin_SiparisDetay" %>

<div id="grid_detay"></div>
<script>
    $(document).ready(function () {
             

        $("#grid_detay").kendoGrid({
            height: 500,
            columns: [
                   
                { field: "Urun_id", width: "50px" },
                { field: "Urun_ad", width: "50px" },
                { field: "Siparis_adet", width: "50px" },
                { field: "Birim_fiyat", width: "80px" },
                { field: "Toplam_tutar", width: "80px" },
                { field: "Birim_fiyat", width: "80px" },
                { field: "Onay1_onaytarih", width: "50px", format: "{0:dd/MM/yyyy}" },
                { field: "Onay_durum", width: "50px" },
                
                {
                    command: [
                                  { text: "Onayla", click: Onayla, class: "btn btn-success" },
                                  { text: "Reddet", click: Iptal, class: "btn btn-success" }
                    ],
                    title: "İşlemler",
                    width: "15%"
                }


            ],

            editable: false, // enable editing
            pageable: true,
            sortable: true,
            filterable: true,
            resizable: true,
            filterMenuInit: filterMenu,
            //toolbar: ["create", "save", "cancel"], // specify toolbar commands
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 10,
                filter: [{ "field": "Siparis_id", "operator": "startswith", "value": <%=Request.QueryString["siparis_id"]%> },{ "field": "Siparis_id", "operator": "endswith", "value": <%=Request.QueryString["siparis_id"]%> }],
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "Siparis_id",
                            fields: {
                                Urun_id: { type: "number" },
                                Urun_ad: { type: "string" },
                                Siparis_adet: { type: "number" },
                                Birim_fiyat: { type: "nımber"},
                                Siparis_tutar: { type: "number" },
                                Toplam_tutar: { type: "number" },
                                Onay1_onaytarih: { type: "date", editable: false, nullable: true },
                                Siparis_statu: { type: "string" },
                                Onay_durum: { type: "string" },
                                Onay_durum2: { type: "string" },
                            }
                        }
                    },
                    batch: false, // enable batch editing - changes will be saved when the user clicks the "Save changes" button
                    transport: {
                        create: {
                            url: "/AjaxMethods.asmx/Create", //specify the URL which should create new records. This is the Create method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                        },
                        read: {
                            url: "/AjaxMethods.asmx/SiparisDetay", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                        },
                        update: {
                            url: "/AjaxMethods.asmx/Update", //specify the URL from which should update the records. This is the Update method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                        },
                        destroy: {
                            url: "/AjaxMethods.asmx/Destroy", //specify the URL which should destroy records. This is the Destroy method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                        },
                        parameterMap: function(data, operation) {
                            if (operation != "read") {
                                // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                                return JSON.stringify({ products: data.models })
                            } else {
                                // web services need default values for every parameter
                                data = $.extend({ sort:null, filter: null }, data);

                                return JSON.stringify(data);
                            }
                        }
                    }
                }

            });

               function filterMenu(e) {
                   if (e.field == "Siparis_tarih") {
                       var beginOperator = e.container.find("[data-role=dropdownlist]:eq(0)").data("kendoDropDownList");
                       beginOperator.value("gte");
                       beginOperator.trigger("change");

                       var endOperator = e.container.find("[data-role=dropdownlist]:eq(2)").data("kendoDropDownList");
                       endOperator.value("lte");
                       endOperator.trigger("change");
                       debugger;
                       e.container.find(".k-dropdown").hide()
                   }


                   if (e.field == "Siparis_no" || e.field == "Cari_id") {
                       var beginOperator = e.container.find("[data-role=dropdownlist]:eq(0)").data("kendoDropDownList");
                       beginOperator.value("startswith");
                       beginOperator.trigger("change");

                       var endOperator = e.container.find("[data-role=dropdownlist]:eq(2)").data("kendoDropDownList");
                       endOperator.value("endswith");
                       endOperator.trigger("change");
                       debugger;
                       e.container.find(".k-dropdown").hide()
                   }

               
               }

          
               function Iptal(e) {
                   e.preventDefault();
                   var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                   var detailID = dataItem.Siparis_id;
                   if (detailID > 0) {
                       swal({
                           title: "Emin misiniz?",
                           text: "Ürünü iptal etmek istediğinize emin misiniz?",
                           type: "warning",
                           showCancelButton: true,
                           confirmButtonColor: "#DD6B55",
                           confirmButtonText: "Evet, Onaylıyorum.",
                           cancelButtonText: "Vazgeç",
                           closeOnConfirm: false,
                           closeOnCancel: false,
                           showLoaderOnConfirm: true,
                       },
                   function (isConfirm) {
                       if (isConfirm) {
   
                           setTimeout(function(){
                               $.ajax({
                                   type: 'POST',
                                   url: '/AjaxMethods.asmx/SiparisOnaylaReddet',
                                   data: '{ "Siparis_id" : "' + detailID + '",  "Type" : 0}',
                                   contentType: 'application/json; charset=utf-8',
                                   dataType: 'json',
                                   success: function (result) {
                                       CustomSwal(result.d);
                                   },
                                   error: function (err) {
                                   }
                               });
                           }, 2000);
                          


                       } else {
                           $(this).prop('checked', false);
                           swal("İptal Edildi", "Onaylama işlemi iptal edildi", "error");
                       }
                   });
                   }
               }

               function Onayla(e) {
                   e.preventDefault();
                   var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                   var detailID = dataItem.Siparis_id;
                   if (detailID > 0) {
                       swal({
                           title: "Emin misiniz?",
                           text: "Ürünü onaylamak istediğinize emin misiniz?",
                           type: "warning",
                           showCancelButton: true,
                           confirmButtonColor: "#DD6B55",
                           confirmButtonText: "Evet, Onaylıyorum.",
                           cancelButtonText: "Vazgeç",
                           closeOnConfirm: false,
                           closeOnCancel: false,
                           showLoaderOnConfirm: true,
                       },
                   function (isConfirm) {
                       if (isConfirm) {
   
                           setTimeout(function(){
                               $.ajax({
                                   type: 'POST',
                                   url: '/AjaxMethods.asmx/SiparisOnaylaReddet',
                                   data: '{ "Siparis_id" : "' + detailID + '",  "Type" : 1}',
                                   contentType: 'application/json; charset=utf-8',
                                   dataType: 'json',
                                   success: function (result) {
                                       CustomSwal(result.d);
                                   },
                                   error: function (err) {
                                   }
                               });
                           }, 2000);

                          


                       } else {
                           $(this).prop('checked', false);
                           swal("İptal Edildi", "Onaylama işlemi iptal edildi", "error");
                       }
                   });
                   }
               }

           
           });

</script>
