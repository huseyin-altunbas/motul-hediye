﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Net.Mail;


public partial class WebAdmin_AnketDetay : System.Web.UI.Page
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    AnketIslemleriBLL bllAnketIslemleri = new AnketIslemleriBLL();
    DataView dvSorted;
    DataTable sortedDT;

    DataTable dtAnketBilgileri;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            drpKullaniciAdDoldur();
            drpAnketSorulariDoldur();
        }

    }


    protected void btnGetir_Click(object sender, EventArgs e)
    {

        int kullaniciId, anketId, aciklama;

        Int32.TryParse(drpKullaniciAd.SelectedValue.ToString(), out kullaniciId);
        Int32.TryParse(drpAnketSorulari.SelectedValue.ToString(), out anketId);
        Int32.TryParse(drpAciklama.SelectedValue.ToString(), out aciklama);

        string oyVermeTarih1 = txtTarih1.Text.ToString().Trim();
        string oyVermeTarih2 = txtTarih2.Text.ToString().Trim();

        if (oyVermeTarih1 == string.Empty && oyVermeTarih2 == string.Empty)
        {
            oyVermeTarih1 = "01.01.2000";
            oyVermeTarih2 = "30.12.9999";
        }
        else if (oyVermeTarih1 != string.Empty && oyVermeTarih2 == string.Empty)
        {
            oyVermeTarih2 = oyVermeTarih1;
        }
        else if (oyVermeTarih1 == string.Empty && oyVermeTarih2 != string.Empty)
        {
            oyVermeTarih1 = "01.01.2000";
        }

        dtAnketBilgileri = bllAnketIslemleri.AnketDetayGetir(kullaniciId, anketId, aciklama, oyVermeTarih1, oyVermeTarih2);

        string sorted = drpSort.SelectedValue.ToString();
        if (sorted == "KullaniciId")
            sorted = sorted + " desc";
        else
            sorted = sorted + " desc" + ", KullaniciId desc";

        if (dtAnketBilgileri.Rows.Count > 0)
        {
            dvSorted = dtAnketBilgileri.DefaultView;
            dvSorted.Sort = sorted;
            sortedDT = dvSorted.ToTable();

            rptAnketBilgileri.DataSource = sortedDT;
            rptAnketBilgileri.DataBind();

            rptExcelBilgileri.DataSource = sortedDT;
            rptExcelBilgileri.DataBind();
        }
        else
        {
            rptAnketBilgileri.DataSource = null;
            rptAnketBilgileri.DataBind();

            rptExcelBilgileri.DataSource = null;
            rptExcelBilgileri.DataBind();
        }
    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + mesaj + "</td></tr></table></div>');</script>";
    }

    public void drpKullaniciAdDoldur()
    {
        DataTable dtUyeBilgileri = new DataTable();

        dtUyeBilgileri = bllUyelikIslemleri.GetirKullaniciAll();

        var sonuc = (from k in dtUyeBilgileri.AsEnumerable()
                     orderby k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad")
                     select new
                     {
                         KullaniciId = k.Field<int>("KullaniciId"),
                         KullaniciAdSoyad = string.Format("{0} {1}", k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad"))
                     }).ToList();


        drpKullaniciAd.DataTextField = "KullaniciAdSoyad";
        drpKullaniciAd.DataValueField = "KullaniciId";
        drpKullaniciAd.DataSource = sonuc;
        drpKullaniciAd.DataBind();
        drpKullaniciAd.Items.Insert(0, (new ListItem(string.Empty, "-1")));

    }


    public void drpAnketSorulariDoldur()
    {
        DataTable dtAnketSorulari = new DataTable();
        dtAnketSorulari = bllAnketIslemleri.AnketGetir();

        drpAnketSorulari.DataTextField = "AnketSorusu";
        drpAnketSorulari.DataValueField = "AnketId";
        drpAnketSorulari.DataSource = dtAnketSorulari;
        drpAnketSorulari.DataBind();
        drpAnketSorulari.Items.Insert(0, (new ListItem(string.Empty, "-1")));

    }


    protected void btnExcel_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=AnketDetay.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";

        Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
        Response.Charset = "windows-1254";

        string header = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title></title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1254\" />\n<style>\n</style>\n</head>\n<body>\n";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        rptExcelBilgileri.RenderControl(htmlWrite);
        Response.Write(header + stringWrite.ToString());
        Response.End();
    }

    protected void rptAnketBilgileri_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = e.Item.DataItem as DataRowView;
            Repeater rptAnketDetayBilgileri = (Repeater)e.Item.FindControl("rptAnketDetayBilgileri");
            Repeater rptAnketKullaniciBilgileri = (Repeater)e.Item.FindControl("rptAnketKullaniciBilgileri");


            DataRow[] dr = dtAnketBilgileri.Select("OyId = '" + drv.Row["OyId"].ToString() + "'");
            rptAnketDetayBilgileri.DataSource = dr.Length > 0 ? dr.CopyToDataTable() : dtAnketBilgileri.Clone();
            rptAnketDetayBilgileri.DataBind();
        }
    }
}