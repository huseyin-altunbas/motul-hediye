﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.IO;
using Goldb2cInfrastructure;

public partial class WebAdmin_AnketGoster : System.Web.UI.Page
{
    enum SoruBaslik
    {
        ArizaliUrun = 12,
        Diger = 13,
        ManuelSiparis = 15
    }

    SoruCevapIslemleriBLL bllSoruCevapIslemleri = new SoruCevapIslemleriBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            drpKullaniciAdDoldur();
            drpSoruBaslikDoldur();

            DataTable dt = bllSoruCevapIslemleri.SoruGetir();

            DataColumn dc = new DataColumn("AktifPasifIslem", typeof(string));
            dt.Columns.Add(dc);

            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToBoolean(dr["AktifPasif"]))
                    dr["AktifPasifIslem"] = "<img onclick=\"SoruAcKapat(" + dr["SoruId"].ToString() + ",false);return false;\" src=\"images/SoruAcik.png\" style=\"width:20px;cursor:pointer;\" title=\"Soruyu Kapat\" />";
                else
                    dr["AktifPasifIslem"] = "<img onclick=\"SoruAcKapat(" + dr["SoruId"].ToString() + ",true);return false;\" src=\"images/SoruKapali.png\" style=\"width:20px;cursor:pointer;\" title=\"Soruyu Aç\" />";

                if (dr["DosyaYolu"] != null && dr["DosyaYolu"].ToString() != string.Empty)
                {
                    string path = HttpContext.Current.Server.MapPath(dr["DosyaYolu"].ToString());
                    if (Directory.Exists(path))
                    {
                        DirectoryInfo d = new DirectoryInfo(path);
                        string files = string.Empty;
                        foreach (FileInfo file in d.GetFiles())
                        {
                            if (file.Name.Contains("="))
                            {
                                string _fileName = file.Name.Split('=')[1].ToString();
                                string[] folder = dr["DosyaYolu"].ToString().Split('/');
                                string _folderId = folder[folder.Length - 1].ToString();
                                files += "<div style=\"margin:10px;font-size:12px;\"><a style=\"margin:10px;color:Red;font-size:12px;\" target=\"_blank\" href=\"../TicketImage/TicketQuestions/" + _folderId + "/" + file.Name + "\"> Önizleme</a> - <span>" + _fileName + "</span></div>";
                            }
                        }

                        dr["DosyaYolu"] = files.ToString();
                    }
                    else
                    {
                        dr["DosyaYolu"] = string.Empty;
                    }

                }

            }

            rptSorular.DataSource = dt;
            rptSorular.DataBind();
        }
    }
    protected void lnkKaydet_Click(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {
            Kullanici kullanici = (Kullanici)Session["Kullanici"];
            string soru = InputSafety.SecureString(txtSoru.Text.Trim());
            int siparisId, kullaniciId, soruBaslikId;
            Int32.TryParse(InputSafety.SecureString(txtSiparisNo.Text.Trim()), out siparisId);
            Int32.TryParse(drpKullanici.SelectedValue.ToString(), out kullaniciId);
            Int32.TryParse(drpBaslik.SelectedValue.ToString(), out soruBaslikId);

            string filePath = "~/TicketImage/TicketQuestions/" + hdnId.Value;

            if (soru.Length <= 750)
            {
                int sonuc = bllSoruCevapIslemleri.SoruKaydet(soru, kullanici.KullaniciId, siparisId, kullaniciId, soruBaslikId, filePath);

                if (sonuc > 0)
                {
                    hdnId.Value = "";

                    try
                    {
                        var vSoru = (from s in bllSoruCevapIslemleri.SoruGetir().AsEnumerable()
                                     where s.Field<int>("SoruId") == sonuc
                                     select new { SoruTarihi = s.Field<DateTime>("SoruTarihi"), Soru = s.Field<string>("Soru"), SoruAcanKullaniciAdSoyad = s.Field<string>("SoruAcanKullaniciAdSoyad") }).SingleOrDefault();

                        if (vSoru != null)
                        {
                            SoruAcildiMailYolla(vSoru.SoruTarihi.ToShortDateString() + " " + "tarihinde" + "<br/>"
                                + vSoru.SoruAcanKullaniciAdSoyad + " " + "tarafından" + "<br/>" + "<br/>"
                                + vSoru.Soru + " " + "<br/>" + "<br/>" + "sorusu açılmıştır.", sonuc);
                        }

                    }
                    catch (Exception)
                    {
                        //
                    }

                    Response.Redirect("~/WebAdmin/Sorular.aspx");
                }
            }
            else
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("En fazla 750 karakter girebilirsiniz"));

        }
        else
        {
            Response.Redirect("~/WebAdmin/UyeGirisi.aspx");
        }
    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + mesaj + "</td></tr></table></div>');</script>";
    }

    [WebMethod]
    public static string SoruAcKapat(int soruId, bool durum)
    {
        if (HttpContext.Current.Session["Kullanici"] != null)
        {
            Kullanici kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];
            if (kullanici.UyelikTipi == 1)
            {
                SoruCevapIslemleriBLL bllSoruCevapIslemleri = new SoruCevapIslemleriBLL();
                if (durum)
                {
                    int sonuc = bllSoruCevapIslemleri.SoruAktifPasif(soruId, durum, kullanici.KullaniciId);
                    if (sonuc > 0)
                    {
                        try
                        {
                            var vSoru = (from s in bllSoruCevapIslemleri.SoruAktifPasifGetir().AsEnumerable()
                                         where s.Field<int>("SoruId") == soruId
                                         && s.Field<bool>("AktifPasif") == true
                                         orderby s.Field<int>("Id") descending
                                         select new { SoruAcmaTarihi = s.Field<DateTime>("EklenmeTarihi"), Soru = s.Field<string>("Soru"), SoruAcanKullaniciAdSoyad = s.Field<string>("KullaniciAdSoyad") }).Take(1).SingleOrDefault();

                            if (vSoru != null)
                            {
                                SoruAcildiMailYolla(vSoru.SoruAcmaTarihi.ToShortDateString() + " " + "tarihinde" + "<br/>"
                                + vSoru.SoruAcanKullaniciAdSoyad + " " + "tarafından" + "<br/>" + "<br/>"
                                + vSoru.Soru + " " + "<br/>" + "<br/>" + "sorusu tekrar açılmıştır.", -1);
                            }

                        }
                        catch (Exception)
                        {
                            //
                        }
                        return "1";
                    }
                    else
                        return "2";
                }
                else
                {
                    int sonuc = bllSoruCevapIslemleri.SoruAktifPasif(soruId, durum, kullanici.KullaniciId);
                    if (sonuc > 0)
                    {
                        try
                        {
                            var vSoru = (from s in bllSoruCevapIslemleri.SoruGetir().AsEnumerable()
                                         where s.Field<int>("SoruId") == soruId
                                         select new { SoruKapatmaTarihi = s.Field<DateTime>("SoruKapatmaTarihi"), Soru = s.Field<string>("Soru"), SoruKapatanKullaniciAdSoyad = s.Field<string>("SoruKapatanKullaniciAdSoyad") }).SingleOrDefault();

                            if (vSoru != null)
                            {
                                SoruKapandiMailYolla(vSoru.SoruKapatmaTarihi.ToShortDateString() + " " + "tarihinde" + "<br/>"
                                + vSoru.SoruKapatanKullaniciAdSoyad + " " + "tarafından" + "<br/>" + "<br/>"
                                + vSoru.Soru + " " + "<br/>" + "<br/>" + "sorusu kapatılmıştır.");
                            }

                        }
                        catch (Exception)
                        {
                            //
                        }
                        return "1";
                    }
                    else
                        return "2";
                }
            }
            else
                return "-1";
        }
        else
            return "0";
    }

    private static void SoruKapandiMailYolla(string message)
    {
        BannerBLL bllBanner = new BannerBLL();
        string strHTML = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/MailSablon/SoruMail.htm"));

        strHTML = strHTML.Replace("#Mesaj#", message);

        SendEmail mail = new SendEmail();
        mail.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
        mail.MailTo = new string[] { "tutkaloperasyon@tutkal.com.tr" };
        //01.12.2017
        //mail.MailTo = new string[] { "fulya.akcora@tutkal.com.tr", "bilge.aslan@tutkal.com.tr", "baris.ersan@tutkal.com.tr",   "huseyin.altunbas@tutkal.com.tr" };
        //01.12.2017
        mail.MailCc = new string[] { "pazarlama@biges.com" };
        mail.MailHtml = true;
        mail.MailSubject = "motulteam.com WebAdmin Sorusu Kapandı.";

        mail.MailBody = strHTML;
        bool mailSent = mail.SendMail(false);
    }

    private static void SoruAcildiMailYolla(string message, int soruId)
    {
        BannerBLL bllBanner = new BannerBLL();
        SoruCevapIslemleriBLL bllSoruCevapIslemleri = new SoruCevapIslemleriBLL();
        string strHTML = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/MailSablon/SoruMail.htm"));

        strHTML = strHTML.Replace("#Mesaj#", message);

        SendEmail mail = new SendEmail();
        mail.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
        mail.MailTo = new string[] { "tutkaloperasyon@tutkal.com.tr" };
        //01.12.2017
        //mail.MailTo = new string[] { "fulya.akcora@tutkal.com.tr","bilge.aslan@tutkal.com.tr", "baris.ersan@tutkal.com.tr",  "huseyin.altunbas@tutkal.com.tr" };
        //01.12.2017
        mail.MailCc = new string[] {"pazarlama@biges.com" };
        mail.MailHtml = true;
        mail.MailSubject = "motulteam.com WebAdmin Sorusu Açıldı.";

        if (soruId > 0)
        {
            var vDosyaYolu = (from s in bllSoruCevapIslemleri.SoruGetir().AsEnumerable()
                              where s.Field<int>("SoruId") == soruId
                              select new { DosyaYolu = s.Field<string>("DosyaYolu") }).SingleOrDefault();

            if (vDosyaYolu != null)
            {
                string path = HttpContext.Current.Server.MapPath(vDosyaYolu.DosyaYolu.ToString());
                if (Directory.Exists(path))
                {
                    System.Net.Mail.Attachment attachment;
                    mail.MailAttach = new List<System.Net.Mail.Attachment>();

                    DirectoryInfo d = new DirectoryInfo(path);
                    foreach (FileInfo file in d.GetFiles())
                    {
                        if (file.Name.Contains("="))
                        {
                            attachment = new System.Net.Mail.Attachment(path + "/" + file.Name);
                            attachment.Name = file.Name.Split('=')[1] != null ? file.Name.Split('=')[1].ToString() : "";
                            mail.MailAttach.Add(attachment);
                        }
                    }
                }
            }
        }

        mail.MailBody = strHTML;
        bool mailSent = mail.SendMail(false);
    }
    protected void lnkSoruAra_Click(object sender, EventArgs e)
    {
        string soruAraText = InputSafety.SecureString(txtSoruAra.Text.Trim());
        int _soruId, _siparisId, _kullaniciId, _soruBaslikId, _aktifPasif;
        Int32.TryParse(InputSafety.SecureString(txtAraSoruNo.Text.Trim()), out _soruId);
        Int32.TryParse(InputSafety.SecureString(txtAraSiparisNo.Text.Trim()), out _siparisId);
        Int32.TryParse(drpAraKullanici.SelectedValue.ToString(), out _kullaniciId);
        Int32.TryParse(drpAraBaslik.SelectedValue.ToString(), out _soruBaslikId);
        Int32.TryParse(drpAktifPasif.SelectedValue.ToString(), out _aktifPasif);

        if (soruAraText.Length <= 200)
        {
            DataTable dt = bllSoruCevapIslemleri.SoruAra(soruAraText, _soruId, _siparisId, _kullaniciId, _soruBaslikId, _aktifPasif);

            DataColumn dc = new DataColumn("AktifPasifIslem", typeof(string));
            dt.Columns.Add(dc);

            foreach (DataRow dr in dt.Rows)
            {

                if (dr["Soru"] != null && !string.IsNullOrEmpty(dr["Soru"].ToString()))
                {
                    int soruAraTextLength = soruAraText.Length;
                    string soru = dr["Soru"].ToString();
                    int i = 0;
                    if (soruAraTextLength > 0)
                    {
                        while ((i = soru.IndexOf(soruAraText, i, StringComparison.OrdinalIgnoreCase)) != -1)
                        {
                            string htmlBaslangic = "<span style='background-color:#00FF33;'>";
                            soru = soru.Insert(i, htmlBaslangic);
                            i += htmlBaslangic.Length + soruAraTextLength;

                            string htmlBitis = "</span>";
                            soru = soru.Insert(i, htmlBitis);
                            i += htmlBitis.Length;
                        }
                    }
                    dr["Soru"] = soru;
                }


                if (Convert.ToBoolean(dr["AktifPasif"]))
                    dr["AktifPasifIslem"] = "<img onclick=\"SoruAcKapat(" + dr["SoruId"].ToString() + ",false);return false;\" src=\"images/SoruAcik.png\" style=\"width:20px;cursor:pointer;\" title=\"Soruyu Kapat\" />";
                else
                    dr["AktifPasifIslem"] = "<img onclick=\"SoruAcKapat(" + dr["SoruId"].ToString() + ",true);return false;\" src=\"images/SoruKapali.png\" style=\"width:20px;cursor:pointer;\" title=\"Soruyu Aç\" />";


                if (dr["DosyaYolu"] != null && dr["DosyaYolu"].ToString() != string.Empty)
                {
                    string path = HttpContext.Current.Server.MapPath(dr["DosyaYolu"].ToString());
                    if (Directory.Exists(path))
                    {
                        DirectoryInfo d = new DirectoryInfo(path);
                        string files = string.Empty;
                        foreach (FileInfo file in d.GetFiles())
                        {
                            if (file.Name.Contains("="))
                            {
                                string _fileName = file.Name.Split('=')[1].ToString();
                                string[] folder = dr["DosyaYolu"].ToString().Split('/');
                                string _folderId = folder[folder.Length - 1].ToString();
                                files += "<div style=\"margin:10px;font-size:12px;\"><a style=\"margin:10px;color:Red;font-size:12px;\" target=\"_blank\" href=\"../TicketImage/TicketQuestions/" + _folderId + "/" + file.Name + "\"> Önizleme</a> - <span>" + _fileName + "</span></div>";
                            }
                        }

                        dr["DosyaYolu"] = files.ToString();
                    }
                    else
                    {
                        dr["DosyaYolu"] = string.Empty;
                    }

                }

            }

            rptSorular.DataSource = dt;
            rptSorular.DataBind();
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("En fazla 200 karakter girebilirsiniz"));
        }
    }
    public void drpKullaniciAdDoldur()
    {
        DataTable dtUyeBilgileri = new DataTable();

        dtUyeBilgileri = bllUyelikIslemleri.GetirKullaniciAll();

        var sonuc = (from k in dtUyeBilgileri.AsEnumerable()
                     where k.Field<int>("UyelikTipi") != 1//Admin kullanıcılarını getirme!..
                     orderby k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad")
                     select new
                     {
                         KullaniciId = k.Field<int>("KullaniciId"),
                         KullaniciAdSoyad = string.Format("{0} {1}", k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad"))
                     }).ToList();


        drpKullanici.DataTextField = "KullaniciAdSoyad";
        drpKullanici.DataValueField = "KullaniciId";
        drpKullanici.DataSource = sonuc;
        drpKullanici.DataBind();
        drpKullanici.Items.Insert(0, (new ListItem(string.Empty, "-1")));

        drpAraKullanici.DataTextField = "KullaniciAdSoyad";
        drpAraKullanici.DataValueField = "KullaniciId";
        drpAraKullanici.DataSource = sonuc;
        drpAraKullanici.DataBind();
        drpAraKullanici.Items.Insert(0, (new ListItem(string.Empty, "-1")));

    }
    public void drpSoruBaslikDoldur()
    {
        DataTable dt = bllSoruCevapIslemleri.SoruBaslikGetir();

        drpBaslik.DataTextField = "SoruBaslik";
        drpBaslik.DataValueField = "SoruBaslikId";
        drpBaslik.DataSource = dt;
        drpBaslik.DataBind();
        drpBaslik.Items.Insert(0, (new ListItem(string.Empty, "-1")));

        drpAraBaslik.DataTextField = "SoruBaslik";
        drpAraBaslik.DataValueField = "SoruBaslikId";
        drpAraBaslik.DataSource = dt;
        drpAraBaslik.DataBind();
        drpAraBaslik.Items.Insert(0, (new ListItem(string.Empty, "-1")));
    }
    protected void drpBaslik_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(drpBaslik.SelectedValue))
        {
            int drpBaslikValue = Convert.ToInt32(drpBaslik.SelectedValue);
            if (drpBaslikValue == Convert.ToInt32(SoruBaslik.ArizaliUrun) || drpBaslikValue == Convert.ToInt32(SoruBaslik.Diger) || drpBaslikValue == Convert.ToInt32(SoruBaslik.ManuelSiparis))
            {
                //Arızalı Ürün - Diğer
                rqSiparisNo.Enabled = false;
                rqKullanici.Enabled = false;

                divSoruKaydetIcerik.Style.Add("display", "inline");
                divSoruAraIcerik.Style.Add("display", "none");
            }
            else
            {
                rqSiparisNo.Enabled = true;
                rqKullanici.Enabled = true;

                divSoruKaydetIcerik.Style.Add("display", "inline");
                divSoruAraIcerik.Style.Add("display", "none");
            }
        }
        else
        {
            rqSiparisNo.Enabled = true;
            rqKullanici.Enabled = true;

            divSoruKaydetIcerik.Style.Add("display", "inline");
            divSoruAraIcerik.Style.Add("display", "none");
        }
    }
    [WebMethod]
    public static List<Ticket> GetFiles(string folderId)
    {
        List<Ticket> lstTicket = new List<Ticket>();
        Ticket ticket;
        string path = HttpContext.Current.Server.MapPath("~/TicketImage/TicketQuestions/" + folderId);

        DirectoryInfo d = new DirectoryInfo(path);

        foreach (FileInfo file in d.GetFiles())
        {
            ticket = new Ticket();
            ticket.FolderId = folderId;
            ticket.FileName = file.Name;

            lstTicket.Add(ticket);
        }

        return lstTicket;
    }

    [WebMethod]
    public static string FileDelete(string folderId, string fileName)
    {
        string path = HttpContext.Current.Server.MapPath("~/TicketImage/TicketQuestions/" + folderId + "/" + fileName);

        if (File.Exists(path))
        {
            File.Delete(path);
            return "1";
        }
        return "0";
    }
}