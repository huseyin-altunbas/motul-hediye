﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AnketSonucu.aspx.cs" Inherits="WebAdmin_AnketSonucu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="font-weight: bold; font-family: Trebuchet MS; color: Silver;">
        <span style="color: #00965E;">Anket Sorusu :</span>&nbsp;
        <asp:Label ID="lblAnketSorusu" runat="server"></asp:Label>
    </div>
    <div>
        <asp:Repeater ID="rptAnketSonucu" runat="server">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 200px; font-weight: bold; color: #00965E; font-family: Trebuchet MS;">
                            Anket Seçenekleri
                        </td>
                        <td style="width: 200px; font-weight: bold; color: #00965E; font-family: Trebuchet MS;">
                        </td>
                        <td style="width: 50px; font-weight: bold; color: #00965E; font-family: Trebuchet MS;
                            text-align: right;">
                            Oy Sayısı
                        </td>
                        <td style="width: 50px; font-weight: bold; color: #00965E; font-family: Trebuchet MS;
                            text-align: right;">
                            Oy Yüzdesi
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="width: 200px; font-family: Trebuchet MS;">
                        <%#DataBinder.Eval(Container.DataItem, "SecenekMetni")%>
                    </td>
                    <td style="width: 200px; font-family: Trebuchet MS; border: 1px solid #ccc;">
                        <img id="imgOy" width='<%#DataBinder.Eval(Container.DataItem, "imgWidth")%>' height="20"
                            src="../WebAdmin/images/loading-bar.png" />
                    </td>
                    <td style="width: 50px; font-family: Trebuchet MS; text-align: right;">
                        <%#DataBinder.Eval(Container.DataItem, "ToplamOy")%>
                    </td>
                    <td style="width: 50px; font-family: Trebuchet MS; text-align: right;">
                        (%<%#DataBinder.Eval(Container.DataItem, "AnketOyYuzdesi")%>)
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div style="clear: both;">
    </div>
    <br />
    <div style="font-weight: bold; font-family: Trebuchet MS; color: Silver;">
        <span style="color: #00965E;">Toplam Oy Sayısı :</span>&nbsp;
        <asp:Label ID="lblAnketToplamOySayisi" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
