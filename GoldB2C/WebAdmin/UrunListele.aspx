﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebAdmin/Admin.master" AutoEventWireup="true"
    CodeFile="UrunListele.aspx.cs" Inherits="WebAdmin_UrunListele" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .cariLink {
            color: red;
        }
        .cariLink:hover {
            text-decoration: underline;
        }
        .cssPanel {
            margin-bottom: 2px;
        }
        .cssDrpSort {
            width: 130px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".accordionContainer .accordionBtn:first").addClass("active");
            $(".accordionContainer .accordionDetail:not(:first)").hide();

            $(".accordionContainer .accordionBtn").click(function () {
                $(this).next(".accordionDetail").slideToggle("slow")
		.siblings(".accordionDetail:visible").slideUp("slow");
                $(this).toggleClass("active");
                $(this).siblings(".accordionBtn").removeClass("active");
            });
        });

        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

        $(function () {
            $("#<%=txtSiparisTarih.ClientID%>").datepicker();
            $("#<%=txtSiparisTarih2.ClientID%>").datepicker();
            $("#<%=txtOnayTarih.ClientID%>").datepicker();
            $("#<%=txtOnayTarih2.ClientID%>").datepicker();
        });


        jQuery(function ($) {
            $.datepicker.regional['tr'] = {
                closeText: 'kapat',
                prevText: '&#x3c;geri',
                nextText: 'ileri&#x3e',
                currentText: 'bugün',
                monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran',
                'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
                monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz',
                'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
                dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
                dayNamesShort: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                dayNamesMin: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                weekHeader: 'Hf',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['tr']);
        });

    </script>
    <div style="float: left; width: 940px">
        <asp:Panel ID="pnlCariId" runat="server" CssClass="cssPanel">
            <asp:LinkButton ID="lnkCariAd" runat="server" CssClass="cariLink" OnClick="lnkCariAd_Click">Cari Ad 'a göre işleme devam et.</asp:LinkButton>&nbsp;&nbsp;<br />
            <br />
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblCariId1" runat="server" Text="Cari Id"> 
                </asp:Label></div>
            <asp:DropDownList ID="drpCariId1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpCariId1_SelectedIndexChanged">
            </asp:DropDownList>
            ve
            <asp:Label ID="lblCariId2" runat="server" Text="">
            </asp:Label>
            <asp:DropDownList ID="drpCariId2" runat="server">
            </asp:DropDownList>
            arasında&nbsp;&nbsp;
            <asp:CheckBox ID="chkCariIdAll" runat="server" Text="Tüm Cari ID 'ler" AutoPostBack="True"
                OnCheckedChanged="chkCariIdAll_CheckedChanged" />&nbsp;&nbsp; &nbsp;&nbsp;<br />
        </asp:Panel>
        <asp:Panel ID="pnlCariAd" runat="server" Visible="false" CssClass="cssPanel">
            <asp:LinkButton ID="lnkCariId" runat="server" CssClass="cariLink" OnClick="lnkCariId_Click">Cari ID 'ye göre işleme devam et.</asp:LinkButton>&nbsp;&nbsp;<br />
            <br />
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblCariAd" runat="server" Text="Cari Ad"></asp:Label></div>
            <asp:DropDownList ID="drpCariAd" runat="server">
            </asp:DropDownList>
            &nbsp;&nbsp; &nbsp;&nbsp;<br />
        </asp:Panel>
        <asp:Panel ID="pnlSiparisNo" runat="server" CssClass="cssPanel">
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblSiparisId1" runat="server" Text="Sipariş No">
                </asp:Label></div>
            <asp:TextBox ID="txtSiparisId1" runat="server"></asp:TextBox>
            ve
            <asp:Label ID="lblSiparisId2" runat="server" Text="">
            </asp:Label>
            <asp:TextBox ID="txtSiparisId2" runat="server"></asp:TextBox>
            arasında&nbsp;&nbsp;<br />
        </asp:Panel>
        <div style="margin-bottom: 20px;">
            <div style="margin-bottom: 2px;">
                <div style="width: 90px; float: left;">
                    <asp:Label ID="lblSiparisTarih" runat="server" Text="Sipariş Tarihi"> 
                    </asp:Label></div>
                <asp:TextBox ID="txtSiparisTarih" runat="server"></asp:TextBox>
                ve
                <asp:Label ID="lblSiparisTarih2" runat="server" Text=""> 
                </asp:Label>
                <asp:TextBox ID="txtSiparisTarih2" runat="server"></asp:TextBox>
                arasında&nbsp;&nbsp;
            </div>
            <div style="clear: both;">
            </div>
            <div style="margin-top: 2px;">
                <div style="width: 90px; float: left;">
                    <asp:Label ID="lblOnayTarih" runat="server" Text="Onay Tarihi"> 
                    </asp:Label></div>
                <asp:TextBox ID="txtOnayTarih" runat="server"></asp:TextBox>
                ve
                <asp:Label ID="lblOnayTari2" runat="server" Text=""> 
                </asp:Label>
                <asp:TextBox ID="txtOnayTarih2" runat="server"></asp:TextBox>
                arasında&nbsp;&nbsp;
            </div>
            <div style="clear: both;">
            </div>
            <div style="margin-top: 2px; padding-bottom: 10px;">
                <div style="width: 90px; float: left;">
                    <asp:Label ID="lblOnayDurum" runat="server" Text="Onay Durumu"> 
                    </asp:Label></div>
                <div style="width: 150px; float: left;">
                    <asp:DropDownList ID="drpOnayDurum" runat="server">
                        <asp:ListItem Selected="True" Value="" Text=""></asp:ListItem>
                        <asp:ListItem Value="Onaylandı">Onaylandı</asp:ListItem>
                        <asp:ListItem Value="Bekliyor">Bekliyor</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <hr />
        <asp:Panel ID="pnlStokGrup" runat="server" CssClass="cssPanel">
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblStokGrup" runat="server" Text="Ürün Grubu">
                </asp:Label></div>
            <asp:DropDownList ID="drpStokGrup" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpStokGrup_SelectedIndexChanged">
            </asp:DropDownList>
            &nbsp;&nbsp;<br />
        </asp:Panel>
        <asp:Panel ID="pnlStokKod" runat="server" CssClass="cssPanel">
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblStokKod1" runat="server" Text="Ürün Ad">
                </asp:Label></div>
            <asp:DropDownList ID="drpUrunListesi" runat="server">
            </asp:DropDownList>
            &nbsp;&nbsp;<br />
        </asp:Panel>
        <asp:Panel ID="pnlStokKod2" runat="server" CssClass="cssPanel">
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblStokKod2" runat="server" Text="Ürün Kod">
                </asp:Label></div>
            <asp:DropDownList ID="drpUrunKod2" runat="server" Enabled="false">
            </asp:DropDownList>
            &nbsp;
            <asp:CheckBox ID="chkStokKod2" runat="server" Text="Ürün Koduna Göre" AutoPostBack="True"
                OnCheckedChanged="chkStokKod2_CheckedChanged" />
            &nbsp;&nbsp;<br />
        </asp:Panel>
        &nbsp;&nbsp;<br />
    </div>
    <hr />
    <div style="float: left; margin-top: -10px; width: 460px;">
        <div style="width: 90px; float: left;">
            <asp:Label ID="lblSort" runat="server" Text="Sıralama ">
            </asp:Label></div>
        <asp:DropDownList ID="drpSort" runat="server" CssClass="cssDrpSort">
            <asp:ListItem Value="siparis_tarih" Text=""></asp:ListItem>
            <%--<asp:ListItem Value="cari_id">Cari Id</asp:ListItem>--%>
            <asp:ListItem Value="cari_ad">Cari Ad</asp:ListItem>
            <asp:ListItem Value="siparis_no">Sipariş No</asp:ListItem>
            <asp:ListItem Value="siparis_tarih">Sipariş Tarihi</asp:ListItem>
            <asp:ListItem Value="urun_ad">Ürün Ad</asp:ListItem>
            <asp:ListItem Value="siparis_adet">Sipariş Adet</asp:ListItem>
            <asp:ListItem Value="birim_fiyat">Birim Puan</asp:ListItem>
            <asp:ListItem Value="toplam_tutar">Toplam Puan</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btnGetir" runat="server" Text="Getir" class="testEtBTN" OnClick="btnGetir_Click" />
        &nbsp;&nbsp;
        <asp:Button ID="btnExcelSend" runat="server" Text="Excel'e Aktar" class="testEtBTN"
            OnClick="btnExcelSend_Click" />
    </div>
    <div style="float: right; margin-top: -10px;">
        <asp:Button ID="btnCariIdGrup" runat="server" Text="Ürün Bazında Toplam Sipariş Adet"
            class="testEtBTN" OnClick="btnCariIdGrup_Click" />
    </div>
    <br />
    <br />
    <table class="accountRoot" style="width: 940px;">
        <tr class="accordionContainer" style="width: 940px;">
            <td>
                <asp:Repeater ID="rptSiparisBilgileri" runat="server" OnItemDataBound="rptSiparisBilgileri_ItemDataBound">
                    <HeaderTemplate>
                        <!-- title -->
                        <table>
                            <tr class="ordertitle" style="width: 940px; height: 50px; background-color: #E8E8E8;">
                                <th class="title3" style="width: 60px;">
                                    Siparis No
                                </th>
                                <th class="title3" style="width: 60px;">
                                    Üye Ad
                                </th>
                                <th class="title3" style="width: 60px;">
                                    Ürün Kodu
                                </th>
                                <th class="title3" style="width: 140px;">
                                    Ürün Ad
                                </th>
                                <th class="title3" style="width: 30px;">
                                    Adet
                                </th>
                                <th class="title3" style="width: 60px;">
                                    Birim Puan
                                </th>
                                <th class="title3" style="width: 60px;">
                                    Toplam Puan
                                </th>
                                <th class="title3" style="width: 40px;">
                                    Birim TL
                                </th>
                                <th class="title3" style="width: 60px;">
                                    Toplam TL
                                </th>
                                <th class="title3" style="width: 30px;">
                                    KDV
                                </th>
                                <%--<th class="title3" style="width: 40px;">
                                    Cari Id
                                </th>
                                <th class="title3" style="width: 60px;">
                                    Cari Kod
                                </th>--%>
                                <th class="title3" style="width: 70px;">
                                    Sipariş Tarihi
                                </th>
                                <th class="title3" style="width: 70px;">
                                    Onay Tarihi
                                </th>
                                <th class="title3" style="width: 60px;">
                                    Durum
                                </th>
                            </tr>
                            <!-- title -->
                    </HeaderTemplate>
                    <ItemTemplate>
                        <!-- chooes -->
                        <tr id="trOrderChooes" runat="server" class="orderchooes" style="width: 940px; border: 1px solid #ccc;">
                            <td class="chooes3" style="width: 60px;">
                                <%#DataBinder.Eval(Container.DataItem, "siparis_no")%>
                            </td>
                            <td class="chooes3" style="width: 60px;">
                                <%#DataBinder.Eval(Container.DataItem, "cari_ad")%>
                            </td>
                            <td class="chooes3" style="width: 60px;">
                                <%#DataBinder.Eval(Container.DataItem, "urun_id")%>
                            </td>
                            <td class="chooes3" style="width: 140px;">
                                <%#DataBinder.Eval(Container.DataItem, "urun_ad")%>
                            </td>
                            <td class="chooes3" style="width: 30px;">
                                <%#DataBinder.Eval(Container.DataItem, "siparis_adet")%>
                            </td>
                            <td class="chooes3" style="width: 60px;">
                                <%#DataBinder.Eval(Container.DataItem, "birim_fiyat", "{0:###,###,###}")%>
                            </td>
                            <td class="chooes3" style="width: 60px;">
                                <%#DataBinder.Eval(Container.DataItem, "toplam_tutar", "{0:###,###,###}")%>
                            </td>
                            <td class="chooes3" style="width: 40px;">
                                <%#DataBinder.Eval(Container.DataItem, "liste_fiyat", "{0:###,###,###}")%>
                            </td>
                            <td class="chooes3" style="width: 60px;">
                                <%#DataBinder.Eval(Container.DataItem, "topliste_fiyat", "{0:###,###,###}")%>
                            </td>
                            <td class="chooes3" style="width: 30px;">
                                <%#DataBinder.Eval(Container.DataItem, "kdv_yuzde")%>&nbsp;
                            </td>
                            <%--<td class="chooes3" style="width: 40px;" >
                                <%#DataBinder.Eval(Container.DataItem, "cari_id")%>&nbsp;
                            </td>
                            <td class="chooes3" style="width: 60px;">
                                <%#DataBinder.Eval(Container.DataItem, "cari_kod")%>&nbsp;
                            </td>--%>
                            <td class="chooes3" style="width: 70px;">
                                <%#DataBinder.Eval(Container.DataItem, "siparis_tarih", "{0:dd.MM.yyyy}")%>
                            </td>
                            <td class="chooes3" style="width: 70px;">
                                <%#DataBinder.Eval(Container.DataItem, "onay1_onaytarih", "{0:dd.MM.yyyy}")%>
                            </td>
                            <td class="chooes3" style="width: 60px;">
                                <%#DataBinder.Eval(Container.DataItem, "siparis_statu")%>
                            </td>
                            <td class="accordionBtn" style="width: 40px;">
                                Adres
                            </td>
                            <td class="accordionDetail">
                                <!-- description -->
                                <%--    <div style="background-color: #e0e0e0; color: #cc0000; width: 724px; height: 21px;
                                margin-bottom: 5px; padding-top: 5px; text-align: center;">
                            </div>--%>
                                <!-- description -->
                                <asp:Repeater ID="rptSiparisDetayBilgileri" runat="server">
                                    <HeaderTemplate>
                                        <!-- detail -->
                                        <table class="orderdetail">
                                            <!-- title -->
                                            <tr class="odtitle" style="width: 925px;">
                                                <th class="title1" style="width: 150px;">
                                                    Ünvan
                                                </th>
                                                <th class="title2" style="width: 350px;">
                                                    Adres
                                                </th>
                                                <th class="title3" style="width: 120px;">
                                                    İlçe
                                                </th>
                                                <th class="title4">
                                                    Şehir
                                                </th>
                                                <%--<th class="title5">
                                                Kazancınız</th>--%>
                                                <th class="title6">
                                                    Mobil Telefon
                                                </th>
                                                <th class="title6">
                                                    Sabit Telefon
                                                </th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <!-- chooes -->
                                        <tr class="odchooes" style="width: 925px;">
                                            <td class="chooes1" style="width: 150px;">
                                                <%#DataBinder.Eval(Container.DataItem, "syazisma_unvan").ToString().ToUpper()%>
                                            </td>
                                            <td class="chooes2" style="width: 350px;">
                                                <%#DataBinder.Eval(Container.DataItem, "sadres_tanim").ToString().ToUpper()%>
                                            </td>
                                            <td class="chooes3" style="width: 120px;">
                                                <%#DataBinder.Eval(Container.DataItem, "silce_kod").ToString().ToUpper()%>
                                            </td>
                                            <td class="chooes4">
                                                <%#DataBinder.Eval(Container.DataItem, "ssehir_kod").ToString().ToUpper()%>
                                            </td>
                                            <%--<td class="chooes5">
                                            <%#DataBinder.Eval(Container.DataItem, "indirim_tutar", "{0:###,###,###}")%>&nbsp;</td>--%>
                                            <td class="chooes6">
                                                <%#DataBinder.Eval(Container.DataItem, "smobil_telefon")%>
                                            </td>
                                            <td class="chooes6">
                                                <%#DataBinder.Eval(Container.DataItem, "ssabit_telefon")%>
                                            </td>
                                        </tr>
                                        <!-- chooes -->
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <!-- chooes -->
                                        <tr class="odchooes" style="width: 925px;">
                                            <td class="chooes1 bgcolorff" style="width: 150px;">
                                                <%#DataBinder.Eval(Container.DataItem, "syazisma_unvan")%>
                                            </td>
                                            <td class="chooes2 bgcolorff" style="width: 350px;">
                                                <%#DataBinder.Eval(Container.DataItem, "sadres_tanim")%>
                                            </td>
                                            <td class="chooes3 bgcolorff" style="width: 120px;">
                                                <%#DataBinder.Eval(Container.DataItem, "silce_kod")%>
                                            </td>
                                            <td class="chooes4 bgcolorff">
                                                <%#DataBinder.Eval(Container.DataItem, "ssehir_kod")%>
                                            </td>
                                            <%--<td class="chooes5 bgcolorff">
                                            <%#DataBinder.Eval(Container.DataItem, "indirim_tutar", "{0:###,###,###}")%>&nbsp;</td>--%>
                                            <td class="chooes6 bgcolorff">
                                                <%#DataBinder.Eval(Container.DataItem, "smobil_telefon")%>
                                            </td>
                                            <td class="chooes6">
                                                <%#DataBinder.Eval(Container.DataItem, "ssabit_telefon")%>
                                            </td>
                                        </tr>
                                        <!-- chooes -->
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                        <!-- detail -->
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <!-- chooes -->
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
    <table class="accountRoot" style="width: 940px;">
        <tr class="accordionContainer" style="width: 940px;">
            <td>
                <asp:Repeater ID="rptCariIdGrup" runat="server" OnItemDataBound="rptSiparisBilgileri_ItemDataBound"
                    Visible="false">
                    <HeaderTemplate>
                        <!-- title -->
                        <table>
                            <tr class="ordertitle" style="width: 940px;">
                                <th class="title4">
                                    Ürün Kodu
                                </th>
                                <th class="title5" style="width: 300px;">
                                    Ürün Ad
                                </th>
                                <th class="title4">
                                    Toplam Sipariş Adet
                                </th>
                            </tr>
                            <!-- title -->
                    </HeaderTemplate>
                    <ItemTemplate>
                        <!-- chooes -->
                        <tr class="orderchooes" style="width: 940px; border-bottom: 1px dashed #ccc;">
                            <td class="chooes4">
                                <%#DataBinder.Eval(Container.DataItem, "urun_id")%>
                            </td>
                            <td class="chooes5">
                                <%#DataBinder.Eval(Container.DataItem, "urun_ad")%>
                            </td>
                            <td class="chooes4">
                                <%#DataBinder.Eval(Container.DataItem, "toplam_siparisadet", "{0:###,###,###}")%>
                            </td>
                            <!-- accordion detail -->
                        </tr>
                        <!-- chooes -->
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
</asp:Content>
