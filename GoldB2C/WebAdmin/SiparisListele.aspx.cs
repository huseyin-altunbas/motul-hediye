﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Globalization;
using System.IO;



public partial class WebAdmin_SiparisListele : System.Web.UI.Page
{
    SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    DataSet dsSiparis;
    DataTable dtCariId;
    DataTable dtExcelSorted;
    DataView dvSorted;
    DataTable sortedDT;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dtCariId = bllUyelikIslemleri.GetirCariId();
            drpCariId1.DataTextField = "ErpCariId";
            drpCariId1.DataValueField = "ErpCariId";
            drpCariId1.DataSource = dtCariId;
            drpCariId1.DataBind();
            drpCariId1.Items.Insert(0, (new ListItem(string.Empty, string.Empty)));


            drpCariAdDoldur();

            pnlCariId.Visible = true;
            pnlCariAd.Visible = false;
        }

    }


    protected void btnGetir_Click(object sender, EventArgs e)
    {
        string cariId1 = drpCariId1.SelectedValue.ToString();
        string cariId2 = drpCariId2.SelectedValue.ToString();

        string cariAd = drpCariAd.SelectedValue;

        string siparisId1 = txtSiparisId1.Text.ToString();
        string siparisId2 = txtSiparisId2.Text.ToString();

        string tarih1 = txtTarih1.Text.ToString();
        string tarih2 = txtTarih2.Text.ToString();

        int _cariId1;
        int _cariId2;
        int _cariAd;
        int _siparisId1;
        int _siparisId2;
        DateTime _time1 = Convert.ToDateTime("01.01.2000");
        DateTime _time2 = Convert.ToDateTime("31.12.9999");

        if (cariId1 == string.Empty)
            _cariId1 = 0;
        else
            _cariId1 = Convert.ToInt32(cariId1);

        if (cariId2 == string.Empty)
            _cariId2 = _cariId1;
        else
            _cariId2 = Convert.ToInt32(cariId2);

        if (siparisId1 == string.Empty)
            _siparisId1 = 0;
        else
            _siparisId1 = Convert.ToInt32(siparisId1);

        if (siparisId2 == string.Empty)
            _siparisId2 = _siparisId1;
        else
            _siparisId2 = Convert.ToInt32(siparisId2);

        if (tarih1 == string.Empty && tarih2 == string.Empty)
        {
            _time1 = Convert.ToDateTime("01.01.2000");
            _time2 = Convert.ToDateTime("31.12.9999");
        }
        else if (tarih1 != string.Empty && tarih2 == string.Empty)
        {
            _time1 = Convert.ToDateTime(tarih1);
            _time2 = _time1;
        }
        else if (tarih1 == string.Empty && tarih2 != string.Empty)
        {
            _time1 = Convert.ToDateTime("01.01.2000");
            _time2 = Convert.ToDateTime(tarih2);
        }
        else if (tarih1 != string.Empty && tarih2 != string.Empty)
        {
            _time1 = Convert.ToDateTime(tarih1);
            _time2 = Convert.ToDateTime(tarih2);
        }


        if (cariAd == string.Empty)
            _cariAd = 0;
        else
            _cariAd = Convert.ToInt32(cariAd);

        DataTable dtMinMaxCariId;
        int minCariId, maxCariId;

        if (chkCariIdAll.Checked)
        {
            dtMinMaxCariId = bllUyelikIslemleri.GetirMinMaxCariId();
            minCariId = dtMinMaxCariId.Rows[0][0] == DBNull.Value ? 0 : Convert.ToInt32(dtMinMaxCariId.Rows[0][0]);
            maxCariId = dtMinMaxCariId.Rows[0][1] == DBNull.Value ? 0 : Convert.ToInt32(dtMinMaxCariId.Rows[0][1]);

            _cariId1 = minCariId;
            _cariId2 = maxCariId;
        }

        if (pnlCariId.Visible)
            dsSiparis = bllSiparisGoruntuleme.GenelSiparisListeleme(_cariId1, _cariId2, _siparisId1, _siparisId2, _time1, _time2);
        else if (pnlCariAd.Visible)
            dsSiparis = bllSiparisGoruntuleme.GenelSiparisListeleme(_cariAd, _cariAd, _siparisId1, _siparisId2, _time1, _time2);


        //#region CariSiparisMasterTT içinden başarılı şekilde oluşmayan siparişler kaldırılıyor. 
        //DataRow[] CariSiparisMasterTTRows = dsSiparis.Tables["CariSiparisMasterTT"].Copy().Select("siparis_statu not in('Sipariş İptal Edildi.')");
        //DataRow[] Datarows = dsSiparis.Tables["CariSiparisMasterTT"].Select();
        //for (int i = 0; i < Datarows.Count(); i++)
        //{
        //    dsSiparis.Tables["CariSiparisMasterTT"].Rows.Remove(Datarows[i]);
        //}
        //foreach (DataRow row in CariSiparisMasterTTRows)
        //{
        //    dsSiparis.Tables["CariSiparisMasterTT"].ImportRow(row);
        //}
        //#endregion

        dsSiparis.Tables["CariSiparisMasterTT"].DefaultView.Sort = "siparis_id desc";
        dsSiparis.Tables["CariSiparisDetayTT"].DefaultView.Sort = "siparis_id desc";

        
        


        foreach (DataRow dr in dsSiparis.Tables["CariSiparisMasterTT"].Rows)
        {
            DataRow RowKargo = dsSiparis.Tables["CariSiparisDetayTT"].Select("siparis_id = '" + dr["siparis_id"].ToString() + "'")[0];
            if (!string.IsNullOrEmpty(RowKargo["siparis_statu"].ToString()) && RowKargo["siparis_statu"].ToString().IndexOf("Kargoda. ") > -1)
            {
                string KargoTakipNo = RowKargo["siparis_statu"].ToString().Replace("Kargoda. ", "");
                int _kargotakipNo = 0;
                if (KargoTakipNo.Contains("/"))
                {
                    KargoTakipNo = KargoTakipNo.Split('/')[0];
                    KargoTakipNo = KargoTakipNo.Trim();
                }
                if (KargoTakipNo != "")
                {
                    if (int.TryParse(KargoTakipNo.Substring(0, 2), out _kargotakipNo))
                    {
                        //yurtiçi
                        dr["siparis_statu"] = "Kargoda. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://selfservis.yurticikargo.com/reports/SSWDocumentDetail.aspx?DocId=" + KargoTakipNo + "\" >Kargom Nerede?</a>";
                    }
                    else
                    {
                        if (KargoTakipNo.StartsWith("1Z"))
                        {
                            //ups
                            dr["siparis_statu"] = "Siparişiniz Sevk Edilmiştir. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://www.ups.com.tr/WaybillSorgu.aspx?Waybill=" + KargoTakipNo + "\" >Kargom Nerede?</a>";
                        }
                        else
                        {
                            //mng
                            //string fat_seri = "", fat_numara = "";
                            //foreach (char ch in KargoTakipNo)
                            //{
                            //    if (!char.IsDigit(ch))
                            //        fat_seri = fat_seri + ch;
                            //    else
                            //        fat_numara = fat_numara + ch;

                            //}
                            //RowKargo["siparis_statu"] = "Kargoda. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://service.mngkargo.com.tr/iactive/takip.asp?fatseri=" + fat_seri + "&fatnumara=" + fat_numara + "&fi=2\" >Kargom Nerede?</a>";

                            dr["siparis_statu"] = "Kargoda. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://service.mngkargo.com.tr/iactive/popup/kargotakip.asp?k=" + KargoTakipNo + "&fi=2\" >Kargom Nerede?</a>";

                        }
                    }
                }

            }
        }

        SortedRepeaterBind(drpSort.SelectedValue);

    }

    public void SortedRepeaterBind(string sorted)
    {
        dtExcelSorted = new DataTable();
        dtExcelSorted = dsSiparis.Tables["CariSiparisDetayTT"].Clone();

        if (sorted == "siparis_id")
            sorted = sorted + " desc";
        else
            sorted = sorted + " desc" + ", siparis_id desc";


        dvSorted = dsSiparis.Tables["CariSiparisMasterTT"].DefaultView;
        dvSorted.Sort = sorted;
        sortedDT = dvSorted.ToTable();


        rptSiparisBilgileri.DataSource = null;
        rptSiparisBilgileri.DataSource = sortedDT;
        rptSiparisBilgileri.DataBind();

        foreach (DataRow dr in sortedDT.Rows)
        {
            foreach (DataRow drS in dsSiparis.Tables["CariSiparisDetayTT"].Select("siparis_id = '" + dr["siparis_id"].ToString() + "'"))
            {
                dtExcelSorted.ImportRow(drS);
            }
        }

        rptExcelBilgileri.DataSource = null;
        rptExcelBilgileri.DataSource = dtExcelSorted;
        rptExcelBilgileri.DataBind();

    }


    protected void rptSiparisBilgileri_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = e.Item.DataItem as DataRowView;
            Repeater rptSiparisDetayBilgileri = (Repeater)e.Item.FindControl("rptSiparisDetayBilgileri");

            DataRow[] drRowSiparisDetay = dsSiparis.Tables["CariSiparisDetayTT"].Select("siparis_id = '" + drv.Row["siparis_id"].ToString() + "'");

            DateTime new_onay1_onaytarih = new DateTime(1, 1, 1);
            foreach (DataRow dr in drRowSiparisDetay)
            {
                if (dr["onay1_onaytarih"] == DBNull.Value)
                    dr["onay1_onaytarih"] = new_onay1_onaytarih;
            }

            rptSiparisDetayBilgileri.DataSource = drRowSiparisDetay;
            rptSiparisDetayBilgileri.DataBind();
        }
    }


    protected void rptExcelBilgileri_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = e.Item.DataItem as DataRowView;
            Repeater rptExcelDetayBilgileri = (Repeater)e.Item.FindControl("rptExcelDetayBilgileri");


            foreach (DataRow dr in dsSiparis.Tables["CariSiparisMasterTT"].Select("siparis_id = '" + drv.Row["siparis_id"].ToString() + "'"))
            {
                if (!string.IsNullOrEmpty(dr["siparis_statu"].ToString()) && dr["siparis_statu"].ToString().IndexOf("Siparişiniz Sevk Edilmiştir. Kargo Kodu : ") > -1)
                {
                    string KargoTakipNo = dr["siparis_statu"].ToString().Replace("Siparişiniz Sevk Edilmiştir. Kargo Kodu : ", "");
                    int _kargotakipNo = 0;
                    if (KargoTakipNo.Contains("/"))
                    {
                        KargoTakipNo = KargoTakipNo.Split('/')[0];
                        KargoTakipNo = KargoTakipNo.Trim();
                    }
                    if (int.TryParse(KargoTakipNo.Substring(0, 2), out _kargotakipNo))
                    {
                        //yurtiçi
                        dr["siparis_statu"] = "Siparişiniz Sevk Edilmiştir. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://selfservis.yurticikargo.com/reports/SSWDocumentDetail.aspx?DocId=" + KargoTakipNo + "\" >Kargom Nerede?</a>";
                    }
                    else
                    {
                        if (KargoTakipNo.StartsWith("1Z"))
                        {
                            //ups
                            dr["siparis_statu"] = "Siparişiniz Sevk Edilmiştir. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://www.ups.com.tr/WaybillSorgu.aspx?Waybill=" + KargoTakipNo + "\" >Kargom Nerede?</a>";
                        }
                        else
                        {
                            //mng
                            //string fat_seri = "", fat_numara = "";
                            //foreach (char ch in KargoTakipNo)
                            //{
                            //    if (!char.IsDigit(ch))
                            //        fat_seri = fat_seri + ch;
                            //    else
                            //        fat_numara = fat_numara + ch;

                            //}
                            //dr["siparis_statu"] = "Siparişiniz Sevk Edilmiştir. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://www.mngkargo.com.tr/iactive/takip.asp?fatseri=" + fat_seri + "&fatnumara=" + fat_numara + "&fi=2\" >Kargom Nerede?</a>";

                            dr["siparis_statu"] = "Kargoda. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://service.mngkargo.com.tr/iactive/popup/kargotakip.asp?k=" + KargoTakipNo + "&fi=2\" >Kargom Nerede?</a>";

                        }
                    }


                }
            }

            rptExcelDetayBilgileri.DataSource = dsSiparis.Tables["CariSiparisMasterTT"].Select("siparis_id = '" + drv.Row["siparis_id"].ToString() + "'");
            rptExcelDetayBilgileri.DataBind();
        }
    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + mesaj + "</td></tr></table></div>');</script>";
    }
    protected void drpCariId1_SelectedIndexChanged(object sender, EventArgs e)
    {
        string drpCari1 = drpCariId1.SelectedValue;

        int drp1 = drpCari1 != string.Empty ? Convert.ToInt32(drpCariId1.SelectedValue.ToString()) : 0;

        dtCariId = bllUyelikIslemleri.GetirCariId();

        var rows = dtCariId.Select("ErpCariId >" + drp1);

        dtCariId = rows.Any() ? rows.CopyToDataTable() : dtCariId.Clone();

        drpCariId2.DataTextField = "ErpCariId";
        drpCariId2.DataValueField = "ErpCariId";
        drpCariId2.DataSource = dtCariId;
        drpCariId2.DataBind();
        drpCariId2.Items.Insert(0, (new ListItem(string.Empty, string.Empty)));
    }
    protected void btnExcelSend_Click(object sender, EventArgs e)
    {

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=siparisListesi.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";

        Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
        Response.Charset = "windows-1254";

        string header = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title></title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1254\" />\n<style>\n</style>\n</head>\n<body>\n";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        rptExcelBilgileri.RenderControl(htmlWrite);
        Response.Write(header + stringWrite.ToString());
        Response.End();
    }
    protected void chkCariIdAll_CheckedChanged(object sender, EventArgs e)
    {
        if (chkCariIdAll.Checked)
        {
            if (drpCariId1.Items.Count > 0)
                drpCariId1.SelectedIndex = 0;
            if (drpCariId2.Items.Count > 0)
                drpCariId2.SelectedIndex = 0;
            drpCariId1.Enabled = false;
            drpCariId2.Enabled = false;
        }
        else
        {
            drpCariId1.Enabled = true;
            drpCariId2.Enabled = true;
        }
    }

    protected void lnkCariAd_Click(object sender, EventArgs e)
    {
        pnlCariAd.Visible = true;
        pnlCariId.Visible = false;
    }
    protected void lnkCariId_Click(object sender, EventArgs e)
    {
        pnlCariAd.Visible = false;
        pnlCariId.Visible = true;
    }

    public void drpCariAdDoldur()
    {
        DataTable dtMinMaxCariId;
        DataTable dtUyeBilgileri = new DataTable();

        int minCariId, maxCariId;

        dtMinMaxCariId = bllUyelikIslemleri.GetirMinMaxCariId();
        minCariId = dtMinMaxCariId.Rows[0][0] == DBNull.Value ? 0 : Convert.ToInt32(dtMinMaxCariId.Rows[0][0]);
        maxCariId = dtMinMaxCariId.Rows[0][1] == DBNull.Value ? 0 : Convert.ToInt32(dtMinMaxCariId.Rows[0][1]);

        dtUyeBilgileri = new DataTable();
        dtUyeBilgileri = bllUyelikIslemleri.CariPuanListesi(minCariId, maxCariId);

        DataView dvUyeBilgileri = new DataView(dtUyeBilgileri);
        dvUyeBilgileri.Sort = "cari_ad";

        drpCariAd.DataTextField = "cari_ad";
        drpCariAd.DataValueField = "cari_id";
        drpCariAd.DataSource = dvUyeBilgileri;
        drpCariAd.DataBind();
        drpCariAd.Items.Insert(0, (new ListItem(string.Empty, string.Empty)));

    }

    public string ImagePath(int siparisId, int urunId)
    {
        string returnPath = "";
        string href = "<a target=\"_blank\" style=\"color:#ff0000\" href='/UrunResim/SiparisUrunResimleri/" + siparisId.ToString() + "/";
        string subPath = "~/UrunResim/SiparisUrunResimleri/" + siparisId.ToString();

        bool isExists = System.IO.Directory.Exists(Server.MapPath(subPath));

        if (isExists)
        {
            string[] fileList = System.IO.Directory.GetFiles(Server.MapPath(subPath), "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".gif") || s.EndsWith(".png") || s.EndsWith(".jpeg") || s.EndsWith(".jpg")).ToArray<string>();

            foreach (string file in fileList)
            {
                string[] _file1 = file.Split('-');
                string[] _file2 = _file1[1].Split('.');

                if (_file2[0].ToString() == urunId.ToString())
                {
                    returnPath = href + siparisId.ToString() + "-" + _file2[0] + "." + _file2[1] + "'" + ">" + urunId.ToString() + "</a>";
                    return returnPath;
                }
            }
        }
        return urunId.ToString();
    }

    protected void rptSiparisDetayBilgileri_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        dsSiparis = bllSiparisGoruntuleme.GenelSiparisListeleme(0, 0, 0, 0, Convert.ToDateTime("01.01.2000"), Convert.ToDateTime("01.01.9999"));
        Int32 siparisUrunDetayId = Convert.ToInt32(e.CommandArgument);
        DataRow[] dr = dsSiparis.Tables["CariSiparisDetayTT"].Select("siparis_detay_id = '" + siparisUrunDetayId.ToString() + "'");
        int toplamTutar = int.Parse(dr[0]["toplam_tutar"].ToString());

        if (dr[0]["onay_durum"].ToString() == "Bekliyor")
        {
            Int32 siparis_id = Convert.ToInt32(dr[0]["siparis_id"]);
            Int32 siparis_detay_id = Convert.ToInt32(dr[0]["siparis_detay_id"]);
            Int32 sonuc = 0;
            if (e.CommandName == "Onay")
            {
                //sonuc = SiparisOnay.urunOnay(siparis_id.ToString(), siparis_detay_id);
                //if (sonuc > 0)
                //{
                //    btnGetir_Click(null, null);
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Ürün onaylandı.');", true);
                //}

            }
            if (e.CommandName == "Reddet")
            {
                //sonuc = SiparisOnay.urunIptal(siparis_id.ToString(), siparis_detay_id, toplamTutar);
                //if (sonuc > 0)
                //{
                //    btnGetir_Click(null, null);
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Ürün reddedildi.');", true);
                //}

            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Ürün durumu değiştirilemez!');", true);
        }

    }

    protected void rptSiparisBilgileri_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }



}