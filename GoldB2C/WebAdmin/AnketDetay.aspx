﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebAdmin/Admin.master" AutoEventWireup="true"
    CodeFile="AnketDetay.aspx.cs" Inherits="WebAdmin_AnketDetay" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .cariLink { color: red; }
        .cariLink:hover { text-decoration: underline; }
        div.loading { position: fixed; top: 0; left: 0; right: 0; z-index: 1; margin: 0 auto; text-align: center; display: block; width: 100%; height: 100%; display: none; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine-tr.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".accordionContainer .accordionBtn:first").addClass("active");
            $(".accordionContainer .accordionDetail:not(:first)").hide();

            $(".accordionContainer .accordionBtn").click(function () {
                $(this).next(".accordionDetail").slideToggle("slow")
		.siblings(".accordionDetail:visible").slideUp("slow");
                $(this).toggleClass("active");
                $(this).siblings(".accordionBtn").removeClass("active");
            });

        });



        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

        $(function () {
            $("#<%=txtTarih1.ClientID%>").datepicker();
            $("#<%=txtTarih2.ClientID%>").datepicker();
        });

        jQuery(function ($) {
            $.datepicker.regional['tr'] = {
                closeText: 'kapat',
                prevText: '&#x3c;geri',
                nextText: 'ileri&#x3e',
                currentText: 'bugün',
                monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran',
                'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
                monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz',
                'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
                dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
                dayNamesShort: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                dayNamesMin: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                weekHeader: 'Hf',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['tr']);
        });

    </script>
    <div style="font-weight: bold;">
        Anket Detayları
    </div>
    <div style="clear: both;">
    </div>
    <br />
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblKullaniciAd" runat="server" Text="Kullanıcı"> 
            </asp:Label></div>
        <asp:DropDownList ID="drpKullaniciAd" runat="server">
        </asp:DropDownList>
    </div>
    <div style="clear: both;">
    </div>
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblAnketSorulari" runat="server" Text="Anket Soruları"> 
            </asp:Label></div>
        <asp:DropDownList ID="drpAnketSorulari" runat="server">
        </asp:DropDownList>
    </div>
    <div style="clear: both;">
    </div>
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblAciklama" runat="server" Text="Açıklama"> 
            </asp:Label></div>
        <asp:DropDownList ID="drpAciklama" runat="server">
            <asp:ListItem Selected="True" Text='' Value="-1"></asp:ListItem>
            <asp:ListItem Value="1">Açıklama eklenen anket cevapları</asp:ListItem>
            <asp:ListItem Value="2">Açıklama eklenmeyen anket cevapları</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div style="clear: both;">
    </div>
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblTarih1" runat="server" Text="Oy Verme Tarihi">
            </asp:Label></div>
        <asp:TextBox ID="txtTarih1" runat="server"></asp:TextBox>
        ve
        <asp:Label ID="lblTarih2" runat="server" Text="">
        </asp:Label>
        <asp:TextBox ID="txtTarih2" runat="server"></asp:TextBox>
        arasında&nbsp;&nbsp;<br />
    </div>
    <div style="clear: both;">
    </div>
    <br />
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblSort" runat="server" Text="Sıralama ">
            </asp:Label></div>
        <asp:DropDownList ID="drpSort" runat="server">
            <asp:ListItem Value="KullaniciId" Text=""></asp:ListItem>
            <asp:ListItem Value="KullaniciAdSoyad">Kullanıcı Ad</asp:ListItem>
            <asp:ListItem Value="AnketTarihi">Anket Tarihi</asp:ListItem>
            <asp:ListItem Value="OyTarihi">Oy Tarihi</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btnGetir" runat="server" Text="Getir" class="testEtBTN" OnClick="btnGetir_Click" />
        &nbsp;&nbsp;
        <asp:Button ID="btnExcel" runat="server" Text="Excel'e Aktar" class="testEtBTN" OnClick="btnExcel_Click" />
        &nbsp;&nbsp;
    </div>
    <br />
    <br />
    <table class="accountRoot" style="width: 940px;">
        <tr class="accordionContainer" style="width: 940px;">
            <td>
                <asp:Repeater ID="rptAnketBilgileri" runat="server" OnItemDataBound="rptAnketBilgileri_ItemDataBound">
                    <HeaderTemplate>
                        <table>
                            <tr class="ordertitle" style="width: 940px; height: 30px;">
                                <th class="title3">
                                    Anket Tarihi
                                </th>
                                <th class="title5" style="width: 250px;">
                                    Anket Sorusu
                                </th>
                                <th class="title3" style="width: 150px;">
                                    Oy Veren Kullanıcı
                                </th>
                                <th class="title5" style="width: 250px;">
                                    Anket Cevabı
                                </th>
                                <th class="title3">
                                    Oy Tarihi
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <!-- chooes -->
                        <tr id="trOrderChooes" runat="server" class="orderchooes" style="width: 940px; border: 1px solid #ccc;">
                            <td class="chooes3">
                                <%#DataBinder.Eval(Container.DataItem, "AnketTarihi", "{0:dd.MM.yyyy}")%>
                            </td>
                            <td class="chooes5" style="width: 250px;">
                                <%#DataBinder.Eval(Container.DataItem, "AnketSorusu")%>
                            </td>
                            <td class="chooes3" style="width: 150px;">
                                <%#DataBinder.Eval(Container.DataItem, "UyelikTipi").ToString() == "1" ? "Admin" : DataBinder.Eval(Container.DataItem, "KullaniciAdSoyad")%>
                            </td>
                            <td class="chooes5" style="width: 250px;">
                                <%#DataBinder.Eval(Container.DataItem, "Text2").ToString() == string.Empty ? DataBinder.Eval(Container.DataItem, "SecenekMetni") : DataBinder.Eval(Container.DataItem, "Text2")%>
                            </td>
                            <td class="chooes3">
                                <%#DataBinder.Eval(Container.DataItem, "OyTarihi", "{0:dd.MM.yyyy}")%>
                            </td>
                            <td class="accordionBtn" style="color: #DB2020; float: right; width: 10px;">
                            </td>
                            <td class="accordionDetail" style="width: 925px;">
                                <asp:Repeater ID="rptAnketDetayBilgileri" runat="server">
                                    <HeaderTemplate>
                                        <!-- detail -->
                                        <table class="orderdetail">
                                            <!-- title -->
                                            <tr class="odtitle" style="width: 940px;">
                                                <th class="title5" style="width: 200px;">
                                                    Kullanıcı Ad-Soyad
                                                </th>
                                                <th class="title5">
                                                    Ev Tel
                                                </th>
                                                <th class="title5">
                                                    Cep Tel
                                                </th>
                                                <th class="title5">
                                                    İl
                                                </th>
                                                <th class="title5">
                                                    İlçe
                                                </th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <!-- chooes -->
                                        <tr class="odchooes" style="width: 940px;">
                                            <td class="chooes5" style="width: 200px;">
                                                <%#DataBinder.Eval(Container.DataItem, "UyelikTipi").ToString() == "1" ? "Admin" : DataBinder.Eval(Container.DataItem, "KullaniciAdSoyad")%>
                                            </td>
                                            <td class="chooes5">
                                                <%#DataBinder.Eval(Container.DataItem, "UyelikTipi").ToString() == "1" ? "***" : DataBinder.Eval(Container.DataItem, "EvTel")%>
                                            </td>
                                            <td class="chooes5">
                                                <%#DataBinder.Eval(Container.DataItem, "UyelikTipi").ToString() == "1" ? "***" : DataBinder.Eval(Container.DataItem, "CepTel")%>
                                            </td>
                                            <td class="chooes5">
                                                <%#DataBinder.Eval(Container.DataItem, "UyelikTipi").ToString() == "1" ? "***" : DataBinder.Eval(Container.DataItem, "SehirKod")%>
                                            </td>
                                            <td class="chooes5">
                                                <%#DataBinder.Eval(Container.DataItem, "UyelikTipi").ToString() == "1" ? "***" : DataBinder.Eval(Container.DataItem, "IlceKod")%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
    <div id="divExcelBilgileri" style="display: none;">
        <table class="accountRoot" style="width: 940px;">
            <tr class="accordionContainer" style="width: 940px;">
                <td>
                    <asp:Repeater ID="rptExcelBilgileri" runat="server">
                        <HeaderTemplate>
                            <table>
                                <tr class="ordertitle" style="width: 940px; height: 60px;">
                                    <th class="title3">
                                        Anket Tarihi
                                    </th>
                                    <th class="title5" style="width: 250px;">
                                        Anket Sorusu
                                    </th>
                                    <th class="title3" style="width: 150px;">
                                        Oy Veren Kullanıcı
                                    </th>
                                    <th class="title5" style="width: 250px;">
                                        Anket Cevabı
                                    </th>
                                    <th class="title3">
                                        Oy Tarihi
                                    </th>
                                    <th class="title5" style="width: 200px;">
                                        Kullanıcı Ad-Soyad
                                    </th>
                                    <th class="title5">
                                        Ev Tel
                                    </th>
                                    <th class="title5">
                                        Cep Tel
                                    </th>
                                    <th class="title5">
                                        İl
                                    </th>
                                    <th class="title5">
                                        İlçe
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <!-- chooes -->
                            <tr id="trOrderChooes" runat="server" class="orderchooes" style="width: 940px; border: 1px solid #ccc;">
                                <td class="chooes3">
                                    <%#DataBinder.Eval(Container.DataItem, "AnketTarihi", "{0:dd.MM.yyyy}")%>
                                </td>
                                <td class="chooes5" style="width: 250px;">
                                    <%#DataBinder.Eval(Container.DataItem, "AnketSorusu")%>
                                </td>
                                <td class="chooes3" style="width: 150px;">
                                    <%#DataBinder.Eval(Container.DataItem, "UyelikTipi").ToString() == "1" ? "Admin" : DataBinder.Eval(Container.DataItem, "KullaniciAdSoyad")%>
                                </td>
                                <td class="chooes5" style="width: 250px;">
                                    <%#DataBinder.Eval(Container.DataItem, "Text2").ToString() == string.Empty ? DataBinder.Eval(Container.DataItem, "SecenekMetni") : DataBinder.Eval(Container.DataItem, "Text2")%>
                                </td>
                                <td class="chooes3">
                                    <%#DataBinder.Eval(Container.DataItem, "OyTarihi", "{0:dd.MM.yyyy}")%>
                                </td>
                                <td class="chooes5" style="width: 200px;">
                                    <%#DataBinder.Eval(Container.DataItem, "UyelikTipi").ToString() == "1" ? "Admin" : DataBinder.Eval(Container.DataItem, "KullaniciAdSoyad")%>
                                </td>
                                <td class="chooes5">
                                    <%#DataBinder.Eval(Container.DataItem, "UyelikTipi").ToString() == "1" ? "***" : DataBinder.Eval(Container.DataItem, "EvTel")%>
                                </td>
                                <td class="chooes5">
                                    <%#DataBinder.Eval(Container.DataItem, "UyelikTipi").ToString() == "1" ? "***" : DataBinder.Eval(Container.DataItem, "CepTel")%>
                                </td>
                                <td class="chooes5">
                                    <%#DataBinder.Eval(Container.DataItem, "UyelikTipi").ToString() == "1" ? "***" : DataBinder.Eval(Container.DataItem, "SehirKod")%>
                                </td>
                                <td class="chooes5">
                                    <%#DataBinder.Eval(Container.DataItem, "UyelikTipi").ToString() == "1" ? "***" : DataBinder.Eval(Container.DataItem, "IlceKod")%>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
