﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebAdmin/Admin.master" AutoEventWireup="true"
    CodeFile="LogListele.aspx.cs" Inherits="WebAdmin_LogListele" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .cariLink
        {
            color: red;
        }
        .cariLink:hover
        {
            text-decoration: underline;
        }
        div.loading
        {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 1;
            margin: 0 auto;
            text-align: center;
            display: block;
            width: 100%;
            height: 100%;
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine-tr.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".accordionContainer .accordionBtn:first").addClass("active");
            $(".accordionContainer .accordionDetail:not(:first)").hide();

            $(".accordionContainer .accordionBtn").click(function () {
                $(this).next(".accordionDetail").slideToggle("slow")
		.siblings(".accordionDetail:visible").slideUp("slow");
                $(this).toggleClass("active");
                $(this).siblings(".accordionBtn").removeClass("active");
            });

        });



        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

        $(function () {
            $("#<%=txtLogTarih1.ClientID%>").datepicker();
            $("#<%=txtLogTarih2.ClientID%>").datepicker();
        });


        jQuery(function ($) {
            $.datepicker.regional['tr'] = {
                closeText: 'kapat',
                prevText: '&#x3c;geri',
                nextText: 'ileri&#x3e',
                currentText: 'bugün',
                monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran',
                'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
                monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz',
                'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
                dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
                dayNamesShort: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                dayNamesMin: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                weekHeader: 'Hf',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['tr']);
        });

    </script>
    <div style="font-weight: bold;">
        Sipariş İptal ve Sipariş Onay Log Kayıtları
    </div>
    <div style="clear: both;">
    </div>
    <br />
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblSiparisId" runat="server" Text="Sipariş No"> 
            </asp:Label></div>
        <asp:TextBox ID="txtSiparisId" runat="server"></asp:TextBox>
    </div>
    <div style="clear: both;">
    </div>
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblKullaniciAd" runat="server" Text="İşlemi Yapan Kullanıcı"> 
            </asp:Label></div>
        <asp:DropDownList ID="drpKullaniciAd" runat="server" OnSelectedIndexChanged="drpKullaniciAd_SelectedIndexChanged"
            AutoPostBack="True">
        </asp:DropDownList>
    </div>
    <div style="clear: both;">
    </div>
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblKullaniciUyelikTip" runat="server" Text="Üyelik Tipi"> 
            </asp:Label></div>
        <asp:DropDownList ID="drpKullaniciUyelikTip" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpKullaniciUyelikTip_SelectedIndexChanged">
            <asp:ListItem Value="-1" Text=""></asp:ListItem>
            <asp:ListItem Value="0">Üye</asp:ListItem>
            <asp:ListItem Value="10">WebAdmin</asp:ListItem>
            <asp:ListItem Value="1">Admin</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div style="clear: both;">
    </div>
    <div style="margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblLogTarih1" runat="server" Text="Log Tarih"> 
            </asp:Label></div>
        <asp:TextBox ID="txtLogTarih1" runat="server"></asp:TextBox>
        ve
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblLogTarih2" runat="server" Text=""> 
            </asp:Label></div>
        <asp:TextBox ID="txtLogTarih2" runat="server"></asp:TextBox>
        arasında
    </div>
    <div style="clear: both;">
    </div>
    <br />
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblSort" runat="server" Text="Sıralama ">
            </asp:Label></div>
        <asp:DropDownList ID="drpSort" runat="server">
            <asp:ListItem Value="LogTarih" Text=""></asp:ListItem>
            <asp:ListItem Value="SiparisId">Sipariş No</asp:ListItem>
            <asp:ListItem Value="KullaniciAdSoyad">Kullanıcı Ad</asp:ListItem>
            <asp:ListItem Value="LogTarih">Log Tarih</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btnGetir" runat="server" Text="Getir" class="testEtBTN" OnClick="btnGetir_Click" />
        &nbsp;&nbsp;
        <asp:Button ID="btnExcelSend" runat="server" Text="Excel'e Aktar" class="testEtBTN"
            OnClick="btnExcelSend_Click" />
    </div>
    <br />
    <br />
    <table class="accountRoot" style="width: 940px;">
        <tr class="accordionContainer" style="width: 940px;">
            <td>
                <asp:Repeater ID="rptLogBilgileri" runat="server" OnItemDataBound="rptLogBilgileri_ItemDataBound">
                    <HeaderTemplate>
                        <table>
                            <tr class="ordertitle" style="width: 940px; height: 60px;">
                                <th class="title3">
                                    İşlemi Yapan Kullanıcı
                                </th>
                                <th class="title4">
                                    Email
                                </th>
                                <th class="title3">
                                    Log anındaki Üyelik Tipi
                                </th>
                                <th class="title3" style="width: 40px;">
                                    Sipariş No
                                </th>
                                <th class="title4">
                                    Açıklama
                                </th>
                                <th class="title3">
                                    Hata Mesajı
                                </th>
                                <th class="title3">
                                    Log Tarih
                                </th>
                                <th class="title3">
                                    CRMCustomerGUID
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <!-- chooes -->
                        <tr class="orderchooes" style="width: 940px; border: 1px solid #ccc;">
                            <td class="chooes3">
                                <%#DataBinder.Eval(Container.DataItem, "KullaniciUyelikTip").ToString() == "1" ? "Admin" : DataBinder.Eval(Container.DataItem, "KullaniciAdSoyad")%>
                            </td>
                            <td class="chooes4">
                                <%#DataBinder.Eval(Container.DataItem, "KullaniciUyelikTip").ToString() == "1" ? "***" : (IsValidMail(DataBinder.Eval(Container.DataItem, "Email").ToString())==true ? DataBinder.Eval(Container.DataItem, "Email") : "")%>
                            </td>
                            <td class="chooes3">
                                <%# DataBinder.Eval(Container.DataItem, "KullaniciUyelikTip").ToString() == "0" ? "Üye" : (DataBinder.Eval(Container.DataItem, "KullaniciUyelikTip").ToString() == "10" ? "WebAdmin" : "Admin")%>
                            </td>
                            <td class="chooes3" style="width: 40px;">
                                <%#DataBinder.Eval(Container.DataItem, "SiparisId")%>
                            </td>
                            <td class="chooes4">
                                <%#DataBinder.Eval(Container.DataItem, "Aciklama")%>
                            </td>
                            <td class="chooes3">
                                <%#DataBinder.Eval(Container.DataItem, "HataMesaj")%>
                            </td>
                            <td class="chooes3">
                                <%#DataBinder.Eval(Container.DataItem, "LogTarih", "{0:dd.MM.yyyy}")%>
                            </td>
                            <td class="chooes3">
                                <%#DataBinder.Eval(Container.DataItem, "CRMCustomerGUID")%>
                            </td>
                            <td class="accordionBtn" style="width: 1px; float: right;">
                            </td>
                            <td class="accordionDetail" style="width: 925px;">
                                <asp:Repeater ID="rptSiparisBilgileri" runat="server">
                                    <HeaderTemplate>
                                        <table class="orderdetail">
                                            <tr class="odtitle" style="width: 940px;">
                                                <th class="title2">
                                                    Sipariş Geçilen Kullanıcı
                                                </th>
                                                <th class="title3">
                                                    Usta Id
                                                </th>
                                                <th class="title3">
                                                    Sipariş Tarihi
                                                </th>
                                                <th class="title3">
                                                    Sipariş No
                                                </th>
                                                <th class="title3">
                                                    Puan
                                                </th>
                                                <th class="title2">
                                                    Durum
                                                </th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="odchooes" style="width: 940px;">
                                            <td class="chooes2">
                                                <%#DataBinder.Eval(Container.DataItem, "cari_ad")%>
                                            </td>
                                            <td class="chooes3">
                                                <asp:Label ID="lblCariId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "cari_id")%>'></asp:Label>
                                            </td>
                                            <td class="chooes3">
                                                <%#DataBinder.Eval(Container.DataItem, "siparis_tarih", "{0:dd.MM.yyyy}")%>
                                            </td>
                                            <td class="chooes3">
                                                <%#DataBinder.Eval(Container.DataItem, "siparis_no")%>
                                            </td>
                                            <td class="chooes3">
                                                <%#DataBinder.Eval(Container.DataItem, "siparis_tutar", "{0:###,###,###}")%>
                                            </td>
                                            <td class="chooes2">
                                                <%#DataBinder.Eval(Container.DataItem, "siparis_statu")%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
</asp:Content>
