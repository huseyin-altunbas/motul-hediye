﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class WebAdmin_AnketGoster : System.Web.UI.Page
{
    AnketIslemleriBLL bllAnketIslemleri = new AnketIslemleriBLL();
    DataTable dtAnket;

    protected void Page_Load(object sender, EventArgs e)
    {
        dtAnket = bllAnketIslemleri.AnketGetir();
        rptAnketGoster.DataSource = dtAnket;
        rptAnketGoster.DataBind();
    }
}