﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;

public partial class WebAdmin_UrunListele : System.Web.UI.Page
{
    SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    UrunGrupIslemleriBLL bllUrunGrupIslemleri = new UrunGrupIslemleriBLL();
    UrunListelemeBLL bllUrunListeleme = new UrunListelemeBLL();
    DataSet dsSiparis;
    DataTable dtCariId;
    DataTable dtUrunGrup;
    DataSet dsUrunListele;
    DataTable dtUrun;
    DataView dvSorted;
    DataTable sortedDT;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dtCariId = bllUyelikIslemleri.GetirCariId();
            drpCariId1.DataTextField = "ErpCariId";
            drpCariId1.DataValueField = "ErpCariId";
            drpCariId1.DataSource = dtCariId;
            drpCariId1.DataBind();
            drpCariId1.Items.Insert(0, (new ListItem(string.Empty, string.Empty)));


            drpCariAdDoldur();

            pnlCariId.Visible = true;
            pnlCariAd.Visible = false;

            drpUrunGrupDoldur();


        }

    }


    protected void btnGetir_Click(object sender, EventArgs e)
    {
        rptCariIdGrup.DataSource = null;
        rptCariIdGrup.DataBind();
        rptCariIdGrup.Visible = false;

        rptSiparisBilgileri.Visible = true;

        string cariId1 = drpCariId1.SelectedValue.ToString();
        string cariId2 = drpCariId2.SelectedValue.ToString();

        string cariAd = drpCariAd.SelectedValue;
        string urunAd = drpUrunListesi.SelectedValue;
        string urunKod = drpUrunKod2.SelectedValue;
        string urunGrupKod = drpStokGrup.SelectedValue;

        string siparisId1 = txtSiparisId1.Text.ToString();
        string siparisId2 = txtSiparisId2.Text.ToString();

        if (siparisId1 == string.Empty)
            siparisId1 = "";
        else
            siparisId1 = txtSiparisId1.Text.ToString().Trim();

        if (siparisId2 == string.Empty)
            siparisId2 = siparisId1;
        else
            siparisId2 = txtSiparisId2.Text.ToString().Trim();

        int _cariId1;
        int _cariId2;
        int _cariAd;
        int _urunAd;
        int _urunKod;
        int _urunGrupKod;
        string siparisTarih1 = txtSiparisTarih.Text.ToString();
        string siparisTarih2 = txtSiparisTarih2.Text.ToString();
        DateTime _siparisTarih1 = Convert.ToDateTime("01.01.2000");
        DateTime _siparisTarih2 = Convert.ToDateTime("31.12.9999");
        string onayTarih1 = txtOnayTarih.Text.ToString();
        string onayTarih2 = txtOnayTarih2.Text.ToString();
        DateTime _onayTarih1 = Convert.ToDateTime("01.01.2000");
        DateTime _onayTarih2 = Convert.ToDateTime("31.12.9999");
        string onayDurum = string.Empty;

        //int _siparisId1;
        //int _siparisId2;

        if (cariId1 == string.Empty)
            _cariId1 = 0;
        else
            _cariId1 = Convert.ToInt32(cariId1);

        if (cariId2 == string.Empty)
            _cariId2 = _cariId1;
        else
            _cariId2 = Convert.ToInt32(cariId2);


        if (cariAd == string.Empty)
            _cariAd = 0;
        else
            _cariAd = Convert.ToInt32(cariAd);

        if (urunAd == string.Empty)
            _urunAd = 0;
        else
            _urunAd = Convert.ToInt32(urunAd);

        if (urunKod == string.Empty)
            _urunKod = 0;
        else
            _urunKod = Convert.ToInt32(urunKod);

        //if (siparisId1 == string.Empty)
        //    _siparisId1 = 0;
        //else
        //    _siparisId1 = Convert.ToInt32(siparisId1);

        //if (siparisId2 == string.Empty)
        //    _siparisId2 = _siparisId1;
        //else
        //    _siparisId2 = Convert.ToInt32(siparisId2);


        int ToplamSayfa = 0;
        int ToplamUrun = 0;
        int[] fiyatAralık;
        int[] InpfiyatAralık = { 0, 0 };


        if (urunGrupKod == string.Empty)
            _urunGrupKod = 0;
        else
            _urunGrupKod = Convert.ToInt32(urunGrupKod);


        if (siparisTarih1 == string.Empty && siparisTarih2 == string.Empty)
        {
            _siparisTarih1 = Convert.ToDateTime("01.01.2000");
            _siparisTarih2 = Convert.ToDateTime("31.12.9999");
        }
        else if (siparisTarih1 != string.Empty && siparisTarih2 == string.Empty)
        {
            _siparisTarih1 = Convert.ToDateTime(siparisTarih1);
            _siparisTarih2 = _siparisTarih1;
        }
        else if (siparisTarih1 == string.Empty && siparisTarih2 != string.Empty)
        {
            _siparisTarih1 = Convert.ToDateTime("01.01.2000");
            _siparisTarih2 = Convert.ToDateTime(siparisTarih2);
        }
        else if (siparisTarih1 != string.Empty && siparisTarih2 != string.Empty)
        {
            _siparisTarih1 = Convert.ToDateTime(siparisTarih1);
            _siparisTarih2 = Convert.ToDateTime(siparisTarih2);
        }

        if (onayTarih1 == string.Empty && onayTarih2 == string.Empty)
        {
            _onayTarih1 = Convert.ToDateTime("01.01.2000");
            _onayTarih2 = Convert.ToDateTime("31.12.9999");
        }
        else if (onayTarih1 != string.Empty && onayTarih2 == string.Empty)
        {
            _onayTarih1 = Convert.ToDateTime(onayTarih1);
            _onayTarih2 = _onayTarih1;
        }
        else if (onayTarih1 == string.Empty && onayTarih2 != string.Empty)
        {
            _onayTarih1 = Convert.ToDateTime("01.01.2000");
            _onayTarih2 = Convert.ToDateTime(onayTarih2);
        }
        else if (onayTarih1 != string.Empty && onayTarih2 != string.Empty)
        {
            _onayTarih1 = Convert.ToDateTime(onayTarih1);
            _onayTarih2 = Convert.ToDateTime(onayTarih2);
        }

        onayDurum = drpOnayDurum.SelectedValue.Trim();
        onayDurum = (onayDurum == "Bekliyor" || onayDurum == "Onaylandı") ? onayDurum : string.Empty;


        DataTable dtUrunGrup;

        DataTable dtMinMaxCariId;
        int minCariId, maxCariId;

        if (chkCariIdAll.Checked)
        {
            dtMinMaxCariId = bllUyelikIslemleri.GetirMinMaxCariId();
            minCariId = dtMinMaxCariId.Rows[0][0] == DBNull.Value ? 0 : Convert.ToInt32(dtMinMaxCariId.Rows[0][0]);
            maxCariId = dtMinMaxCariId.Rows[0][1] == DBNull.Value ? 0 : Convert.ToInt32(dtMinMaxCariId.Rows[0][1]);

            _cariId1 = minCariId;
            _cariId2 = maxCariId;
        }

        if (pnlCariId.Visible)
        {
            if (_urunGrupKod > 0 && _urunKod == 0 && _urunAd == 0)
            {
                dsUrunListele = bllUrunListeleme.GetUrunListele(_urunGrupKod, 0, 0, string.Empty, string.Empty, out ToplamSayfa, out ToplamUrun, out fiyatAralık, InpfiyatAralık);
                dsSiparis = bllSiparisGoruntuleme.GenelStokListeleme(-1, -1, -1, -1, "-1", "-1", _siparisTarih1, _siparisTarih2, _onayTarih1, _onayTarih2, onayDurum);
                dtUrunGrup = dsSiparis.Tables["CariStokListesiTT"].Clone();

                foreach (DataRow dr in dsUrunListele.Tables["UrunKartTT"].Rows)
                {
                    dsSiparis = bllSiparisGoruntuleme.GenelStokListeleme(Convert.ToInt32(dr["urun_id"]), Convert.ToInt32(dr["urun_id"]), _cariId1, _cariId2, siparisId1, siparisId2, _siparisTarih1, _siparisTarih2, _onayTarih1, _onayTarih2, onayDurum);


                    if (dtUrunGrup.Rows.Count == 0 && dsSiparis.Tables["CariStokListesiTT"].Rows.Count > 0)
                        dtUrunGrup = dsSiparis.Tables["CariStokListesiTT"].AsEnumerable().CopyToDataTable();
                    else if (dtUrunGrup.Rows.Count > 0 && dsSiparis.Tables["CariStokListesiTT"].Rows.Count > 0)
                        dtUrunGrup = dtUrunGrup.AsEnumerable().Union(dsSiparis.Tables["CariStokListesiTT"].AsEnumerable()).CopyToDataTable();

                }
                dsSiparis.Tables["CariStokListesiTT"].Clear();
                foreach (DataRow dr in dtUrunGrup.Rows)
                {

                    dsSiparis.Tables["CariStokListesiTT"].ImportRow(dr);
                }


            }
            else
            {
                if (_urunAd > 0)
                    dsSiparis = bllSiparisGoruntuleme.GenelStokListeleme(_urunAd, _urunAd, _cariId1, _cariId2, siparisId1, siparisId2, _siparisTarih1, _siparisTarih2, _onayTarih1, _onayTarih2, onayDurum);
                else if (_urunKod > 0)
                    dsSiparis = bllSiparisGoruntuleme.GenelStokListeleme(_urunKod, _urunKod, _cariId1, _cariId2, siparisId1, siparisId2, _siparisTarih1, _siparisTarih2, _onayTarih1, _onayTarih2, onayDurum);
                else
                    dsSiparis = bllSiparisGoruntuleme.GenelStokListeleme(_urunAd, _urunAd, _cariId1, _cariId2, siparisId1, siparisId2, _siparisTarih1, _siparisTarih2, _onayTarih1, _onayTarih2, onayDurum);
            }
        }
        else if (pnlCariAd.Visible)
        {
            if (_urunGrupKod > 0 && _urunKod == 0 && _urunAd == 0)
            {
                dsUrunListele = bllUrunListeleme.GetUrunListele(_urunGrupKod, 0, 0, string.Empty, string.Empty, out ToplamSayfa, out ToplamUrun, out fiyatAralık, InpfiyatAralık);
                dsSiparis = bllSiparisGoruntuleme.GenelStokListeleme(-1, -1, -1, -1, "-1", "-1", _siparisTarih1, _siparisTarih2, _onayTarih1, _onayTarih2, onayDurum);
                dtUrunGrup = dsSiparis.Tables["CariStokListesiTT"].Clone();


                foreach (DataRow dr in dsUrunListele.Tables["UrunKartTT"].Rows)
                {
                    dsSiparis = bllSiparisGoruntuleme.GenelStokListeleme(Convert.ToInt32(dr["urun_id"]), Convert.ToInt32(dr["urun_id"]), _cariAd, _cariAd, siparisId1, siparisId2, _siparisTarih1, _siparisTarih2, _onayTarih1, _onayTarih2, onayDurum);


                    if (dtUrunGrup.Rows.Count == 0 && dsSiparis.Tables["CariStokListesiTT"].Rows.Count > 0)
                        dtUrunGrup = dsSiparis.Tables["CariStokListesiTT"].AsEnumerable().CopyToDataTable();
                    else if (dtUrunGrup.Rows.Count > 0 && dsSiparis.Tables["CariStokListesiTT"].Rows.Count > 0)
                        dtUrunGrup = dtUrunGrup.AsEnumerable().Union(dsSiparis.Tables["CariStokListesiTT"].AsEnumerable()).CopyToDataTable();

                }
                dsSiparis.Tables["CariStokListesiTT"].Clear();
                foreach (DataRow dr in dtUrunGrup.Rows)
                {

                    dsSiparis.Tables["CariStokListesiTT"].ImportRow(dr);
                }


            }
            else
            {
                if (_urunAd > 0)
                    dsSiparis = bllSiparisGoruntuleme.GenelStokListeleme(_urunAd, _urunAd, _cariAd, _cariAd, siparisId1, siparisId2, _siparisTarih1, _siparisTarih2, _onayTarih1, _onayTarih2, onayDurum);
                else if (_urunKod > 0)
                    dsSiparis = bllSiparisGoruntuleme.GenelStokListeleme(_urunKod, _urunKod, _cariAd, _cariAd, siparisId1, siparisId2, _siparisTarih1, _siparisTarih2, _onayTarih1, _onayTarih2, onayDurum);
                else
                    dsSiparis = bllSiparisGoruntuleme.GenelStokListeleme(_urunAd, _urunAd, _cariAd, _cariAd, siparisId1, siparisId2, _siparisTarih1, _siparisTarih2, _onayTarih1, _onayTarih2, onayDurum);
            }
        }

        if (!string.IsNullOrEmpty(drpOnayDurum.SelectedValue))
        {
            DataRow[] drTempCariStokListesiTT = dsSiparis.Tables["CariStokListesiTT"].Select("onay_durum='" + drpOnayDurum.SelectedValue + "'");
            DataTable dtTempCariStokListesiTT = drTempCariStokListesiTT.Length > 0 ? drTempCariStokListesiTT.CopyToDataTable() : dsSiparis.Tables["CariStokListesiTT"].Clone();

            dsSiparis.Tables["CariStokListesiTT"].Clear();
            foreach (DataRow dr in dtTempCariStokListesiTT.Rows)
            {
                dsSiparis.Tables["CariStokListesiTT"].ImportRow(dr);
            }

        }

        foreach (DataRow dr in dsSiparis.Tables["CariStokListesiTT"].Rows)
        {
            if (!string.IsNullOrEmpty(dr["siparis_statu"].ToString()) && dr["siparis_statu"].ToString().IndexOf("Kargoda. ") > -1)
            {
                string KargoTakipNo = dr["siparis_statu"].ToString().Replace("Kargoda. ", "");
                int _kargotakipNo = 0;

                if (!string.IsNullOrEmpty(KargoTakipNo))
                {
                    if (int.TryParse(KargoTakipNo.Substring(0, 2), out _kargotakipNo))
                    {
                        //yurtiçi
                        dr["siparis_statu"] = "Kargoda. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://selfservis.yurticikargo.com/reports/SSWDocumentDetail.aspx?DocId=" + KargoTakipNo + "\" >Kargom Nerede?</a>";
                    }
                    else
                    {
                        //mng
                        //string fat_seri = "", fat_numara = "";
                        //foreach (char ch in KargoTakipNo)
                        //{
                        //    if (!char.IsDigit(ch))
                        //        fat_seri = fat_seri + ch;
                        //    else
                        //        fat_numara = fat_numara + ch;

                        //}
                        //dr["siparis_statu"] = "Kargoda. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://service.mngkargo.com.tr/iactive/takip.asp?fatseri=" + fat_seri + "&fatnumara=" + fat_numara + "&fi=2\" >Kargom Nerede?</a>";

                        dr["siparis_statu"] = "Kargoda. " + "<a style=\"color:#ff0000\" target=\"_blank\" href=\"http://service.mngkargo.com.tr/iactive/popup/kargotakip.asp?k=" + KargoTakipNo + "&fi=2\" >Kargom Nerede?</a>";

                    }
                }

            }
        }

        SortedRepeaterBind(drpSort.SelectedValue);
    }


    public void SortedRepeaterBind(string sorted)
    {

        if (sorted == "siparis_no")
            sorted = sorted + " desc";
        else
            sorted = sorted + " desc" + ", siparis_no desc";

        dvSorted = dsSiparis.Tables["CariStokListesiTT"].DefaultView;
        dvSorted.Sort = sorted;
        sortedDT = dvSorted.ToTable();

        rptSiparisBilgileri.DataSource = null;
        rptSiparisBilgileri.DataSource = sortedDT;
        rptSiparisBilgileri.DataBind();

    }


    protected void rptSiparisBilgileri_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = e.Item.DataItem as DataRowView;

            if (drv != null)
            {
                if (drv["onay1_onaytarih"] != DBNull.Value && drv["onay1_onaytarih"] != null && Convert.ToDateTime(drv["onay1_onaytarih"]).ToShortDateString() == DateTime.Now.ToShortDateString())
                {
                    HtmlTableRow htmlRow = (HtmlTableRow)e.Item.FindControl("trOrderChooes");
                    htmlRow.Attributes.Add("style", "border:1px solid #EF8200;width: 940px;");
                }

                Repeater rptSiparisDetayBilgileri = (Repeater)e.Item.FindControl("rptSiparisDetayBilgileri");

                rptSiparisDetayBilgileri.DataSource = dsSiparis.Tables["SiparisCariAdresTT"].Select("siparis_no = '" + drv.Row["siparis_no"].ToString() + "'");
                rptSiparisDetayBilgileri.DataBind();
            }

        }

    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + mesaj + "</td></tr></table></div>');</script>";
    }
    protected void drpCariId1_SelectedIndexChanged(object sender, EventArgs e)
    {
        string drpCari1 = drpCariId1.SelectedValue;

        int drp1 = drpCari1 != string.Empty ? Convert.ToInt32(drpCariId1.SelectedValue.ToString()) : 0;

        dtCariId = bllUyelikIslemleri.GetirCariId();

        var rows = dtCariId.Select("ErpCariId >" + drp1);

        dtCariId = rows.Any() ? rows.CopyToDataTable() : dtCariId.Clone();

        drpCariId2.DataTextField = "ErpCariId";
        drpCariId2.DataValueField = "ErpCariId";
        drpCariId2.DataSource = dtCariId;
        drpCariId2.DataBind();
        drpCariId2.Items.Insert(0, (new ListItem(string.Empty, string.Empty)));
    }
    protected void btnExcelSend_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=urunListesi-" + DateTime.Now.ToShortDateString() + ".xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";

        Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
        Response.Charset = "windows-1254";

        string header = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title></title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1254\" />\n<style>\n</style>\n</head>\n<body>\n";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        if (rptSiparisBilgileri.Visible)
            rptSiparisBilgileri.RenderControl(htmlWrite);
        else if (rptCariIdGrup.Visible)
            rptCariIdGrup.RenderControl(htmlWrite);
        Response.Write(header + stringWrite.ToString());
        Response.End();
    }
    protected void chkCariIdAll_CheckedChanged(object sender, EventArgs e)
    {
        if (chkCariIdAll.Checked)
        {
            if (drpCariId1.Items.Count > 0)
                drpCariId1.SelectedIndex = 0;
            if (drpCariId2.Items.Count > 0)
                drpCariId2.SelectedIndex = 0;
            drpCariId1.Enabled = false;
            drpCariId2.Enabled = false;
        }
        else
        {
            drpCariId1.Enabled = true;
            drpCariId2.Enabled = true;
        }
    }

    protected void lnkCariAd_Click(object sender, EventArgs e)
    {
        pnlCariAd.Visible = true;
        pnlCariId.Visible = false;
    }
    protected void lnkCariId_Click(object sender, EventArgs e)
    {
        pnlCariAd.Visible = false;
        pnlCariId.Visible = true;
    }

    public void drpCariAdDoldur()
    {
        DataTable dtMinMaxCariId;
        DataTable dtUyeBilgileri = new DataTable();

        int minCariId, maxCariId;

        dtMinMaxCariId = bllUyelikIslemleri.GetirMinMaxCariId();
        minCariId = dtMinMaxCariId.Rows[0][0] == DBNull.Value ? 0 : Convert.ToInt32(dtMinMaxCariId.Rows[0][0]);
        maxCariId = dtMinMaxCariId.Rows[0][1] == DBNull.Value ? 0 : Convert.ToInt32(dtMinMaxCariId.Rows[0][1]);

        dtUyeBilgileri = new DataTable();
        dtUyeBilgileri = bllUyelikIslemleri.CariPuanListesi(minCariId, maxCariId);

        DataView dvUyeBilgileri = new DataView(dtUyeBilgileri);
        dvUyeBilgileri.Sort = "cari_ad";

        drpCariAd.DataTextField = "cari_ad";
        drpCariAd.DataValueField = "cari_id";

        drpCariAd.DataSource = dvUyeBilgileri;
        drpCariAd.DataBind();
        drpCariAd.Items.Insert(0, (new ListItem(string.Empty, string.Empty)));

    }


    public void drpUrunGrupDoldur()
    {

        dtUrunGrup = bllUrunGrupIslemleri.GetProductGrupListesi(0);

        DataView dvUrunBilgileri = new DataView(dtUrunGrup);
        dvUrunBilgileri.Sort = "urun_ad";

        drpStokGrup.DataTextField = "urun_ad";
        drpStokGrup.DataValueField = "urun_id";

        drpStokGrup.DataSource = dvUrunBilgileri;
        drpStokGrup.DataBind();
        drpStokGrup.Items.Insert(0, (new ListItem(string.Empty, string.Empty)));

    }


    public void drpUrunDoldur()
    {
        int ToplamSayfa = 0;
        int ToplamUrun = 0;

        int stokGrup = 0;
        if (drpStokGrup.SelectedValue == string.Empty)
            stokGrup = 0;
        else
            stokGrup = Convert.ToInt32(drpStokGrup.SelectedValue.ToString());



        int[] fiyatAralık;
        int[] InpfiyatAralık = { 0, 0 };

        dsUrunListele = bllUrunListeleme.GetUrunListele(stokGrup, 0, 0, string.Empty, string.Empty, out ToplamSayfa, out ToplamUrun, out fiyatAralık, InpfiyatAralık);


        //ürün ad
        DataView dvUrunBilgileri = new DataView(dsUrunListele.Tables["UrunKartTT"]);
        dvUrunBilgileri.Sort = "urun_ad";

        drpUrunListesi.DataTextField = "urun_ad";
        drpUrunListesi.DataValueField = "urun_id";

        drpUrunListesi.DataSource = dvUrunBilgileri;
        drpUrunListesi.DataBind();
        drpUrunListesi.Items.Insert(0, (new ListItem(string.Empty, string.Empty)));


        //ürün kod
        DataView dvUrunBilgileri2 = new DataView(dsUrunListele.Tables["UrunKartTT"]);
        dvUrunBilgileri2.Sort = "urun_id";

        drpUrunKod2.DataTextField = "urun_id";
        drpUrunKod2.DataValueField = "urun_id";

        drpUrunKod2.DataSource = dvUrunBilgileri2;
        drpUrunKod2.DataBind();
        drpUrunKod2.Items.Insert(0, (new ListItem(string.Empty, string.Empty)));

    }



    protected void chkStokKod2_CheckedChanged(object sender, EventArgs e)
    {
        if (chkStokKod2.Checked)
        {
            if (drpUrunListesi.Items.Count > 0)
                drpUrunListesi.SelectedIndex = 0;

            drpUrunListesi.Enabled = false;

            drpUrunKod2.Enabled = true;
        }
        else
        {
            if (drpUrunKod2.Items.Count > 0)
                drpUrunKod2.SelectedIndex = 0;

            drpUrunKod2.Enabled = false;

            drpUrunListesi.Enabled = true;
        }
    }

    protected void btnCariIdGrup_Click(object sender, EventArgs e)
    {
        rptSiparisBilgileri.DataSource = null;
        rptSiparisBilgileri.DataBind();
        rptSiparisBilgileri.Visible = false;

        rptCariIdGrup.Visible = true;

        DateTime tarih1 = Convert.ToDateTime("01.01.2000");
        DateTime tarih2 = Convert.ToDateTime("31.12.9999");

        dsSiparis = bllSiparisGoruntuleme.GenelStokListeleme(0, 0, 0, 0, string.Empty, string.Empty, tarih1, tarih2, tarih1, tarih2, string.Empty);
        var sonuc = from b in dsSiparis.Tables["CariStokListesiTT"].AsEnumerable()
                    group b by new { urun_id = b.Field<string>("urun_id"), urun_ad = b.Field<string>("urun_ad") } into g
                    orderby int.Parse(g.Key.urun_id)
                    select new { urun_id = g.Key.urun_id, urun_ad = g.Key.urun_ad, toplam_siparisadet = g.Sum(x => x.Field<decimal>("siparis_adet")) };

        rptCariIdGrup.DataSource = sonuc.AsEnumerable();
        rptCariIdGrup.DataBind();

    }
    protected void drpStokGrup_SelectedIndexChanged(object sender, EventArgs e)
    {
        drpUrunDoldur();

    }


}