﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SiparisExport.aspx.cs" Inherits="WebAdmin_SiparisExport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
       <asp:Repeater ID="rptExcelBilgileri" runat="server" OnItemDataBound="rptExcelBilgileri_ItemDataBound">
                        <HeaderTemplate>
                            <!-- title -->
                            <table>
                                <tr class="ordertitle" style="width: 1040px;">
                                    <td class="accordionDetail">
                                        <table class="orderdetail">
                                            <tr class="odtitle" style="width: 1040px;">
                                                <th class="title4">
                                                    Ad
                                                </th>
                                                <th class="title2">
                                                    Erp Cari Id
                                                </th>
                                                <th class="title2">
                                                    Cari Kod
                                                </th>
                                                <th class="title2">
                                                    Sipariş Tarihi
                                                </th>
                                                <th class="title3">
                                                    Sipariş No
                                                </th>
                                                <th class="title2">
                                                    Puan
                                                </th>
                                                <th class="title4">
                                                    Durum
                                                </th>
                                                <th class="accordionBtn">
                                                    Detaylar
                                                </th>
                                            </tr>
                                        </table>
                                    </td>
                                    <th class="title1" style="width: 89px;">
                                        Ürün Kodu
                                    </th>
                                    <th class="title2" style="width: 300px;">
                                        Ürün Adı
                                    </th>
                                    <th class="title3" style="width: 71px;">
                                        Birim Puanı
                                    </th>
                                    <th class="title4">
                                        Adet
                                    </th>
                                    <%--<th class="title5">
                         Kazancınız</th>--%>
                                    <th class="title6">
                                        Toplam Puan
                                    </th>
                                    <th class="title3">
                                        Onay Tarihi
                                    </th>
                                </tr>
                                <!-- title -->
                        </HeaderTemplate>
                        <ItemTemplate>
                            <!-- chooes -->
                            <tr class="orderchooes" style="width: 1040px; border: 1px solid #ccc;">
                                <!-- accordion detail -->
                                <td class="accordionDetail">
                                    <!-- description -->
                                    <%--    <div style="background-color: #e0e0e0; color: #cc0000; width: 724px; height: 21px;
                                margin-bottom: 5px; padding-top: 5px; text-align: center;">
                            </div>--%>
                                    <!-- description -->
                                    <asp:Repeater ID="rptExcelDetayBilgileri" runat="server">
                                        <HeaderTemplate>
                                            <!-- detail -->
                                            <table class="orderdetail">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <!-- chooes -->
                                            <tr class="odchooes" style="width: 1040px; border: 1px solid #ccc;">
                                                <td class="chooes4">
                                                    <%#DataBinder.Eval(Container.DataItem, "cari_ad")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "cari_id")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "cari_kod")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_tarih", "{0:dd.MM.yyyy}")%>
                                                </td>
                                                <td class="chooes3">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_no")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_tutar", "{0:###,###,###}")%>
                                                </td>
                                                <td class="chooes4">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_statu")%>
                                                </td>
                                                <td class="accordionBtn">
                                                    Detaylar
                                                </td>
                                            </tr>
                                            <!-- chooes -->
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="odchooes" style="width: 1040px; border: 1px solid #ccc;">
                                                <td class="chooes4">
                                                    <%#DataBinder.Eval(Container.DataItem, "cari_ad")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "cari_id")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "cari_kod")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_tarih", "{0:dd.MM.yyyy}")%>
                                                </td>
                                                <td class="chooes3">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_no")%>
                                                </td>
                                                <td class="chooes2">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_tutar", "{0:###,###,###}")%>
                                                </td>
                                                <td class="chooes4">
                                                    <%#DataBinder.Eval(Container.DataItem, "siparis_statu")%>
                                                </td>
                                                <td class="accordionBtn">
                                                    Detaylar
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                            <!-- detail -->
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </td>
                                <td class="chooes1" style="width: 89px;">
                                    <%#DataBinder.Eval(Container.DataItem, "urun_id")%>
                                </td>
                                <td class="chooes2" style="width: 300px;">
                                    <%#DataBinder.Eval(Container.DataItem, "urun_ad")%>
                                </td>
                                <td class="chooes3" style="width: 71px;">
                                    <%#DataBinder.Eval(Container.DataItem, "birim_fiyat", "{0:###,###,###}")%>
                                </td>
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "siparis_adet")%>
                                </td>
                                <%--<td class="chooes5">
                        <%#DataBinder.Eval(Container.DataItem, "indirim_tutar", "{0:###,###,###}")%>&nbsp;</td>--%>
                                <td class="chooes6">
                                    <%#DataBinder.Eval(Container.DataItem, "toplam_tutar", "{0:###,###,###}")%>
                                </td>
                                <td class="chooes3">
                                    <%# DataBinder.Eval(Container.DataItem, "onay1_onaytarih", "{0:dd.MM.yyyy}") == "01.01.0001" ? "" : DataBinder.Eval(Container.DataItem, "onay1_onaytarih", "{0:dd.MM.yyyy}")%>
                                </td>
                                <!-- accordion detail -->
                            </tr>
                            <!-- chooes -->
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
    </form>
</body>
</html>
