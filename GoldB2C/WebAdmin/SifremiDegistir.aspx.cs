﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;

public partial class WebAdmin_SifremiDegistir : System.Web.UI.Page
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    Kullanici kullanici;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
            kullanici = (Kullanici)Session["Kullanici"];
    }

    protected void btnSifremiDegistir_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            lblSonuc.Text = bllUyelikIslemleri.GuncelleKullaniciSifre(kullanici.KullaniciId, txtMevcutSifre.Text.Trim(), txtYeniSifre.Text.Trim());
        }
    }
}