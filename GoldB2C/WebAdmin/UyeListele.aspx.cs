﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using System.Data.SqlClient;

public partial class WebAdmin_UyeListele : System.Web.UI.Page
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    DataTable dtUyeler;
    DataView dvSorted;
    string sorted;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }
    protected void btnGetir_Click(object sender, EventArgs e)
    {
        string ad = txtAd.Text.ToString().Trim();
        string soyad = txtSoyad.Text.ToString().Trim();

        #region uyelikTarih
        string uyelikTarih1 = txtUyelikTarih1.Text.ToString().Trim();
        string uyelikTarih2 = txtUyelikTarih2.Text.ToString().Trim();
        DateTime _uyelikTarih1 = Convert.ToDateTime("01.01.2000 00:00:00");
        DateTime _uyelikTarih2 = Convert.ToDateTime("31.12.9999 23:59:59");

        if (uyelikTarih1 == string.Empty && uyelikTarih2 == string.Empty)
        {
            _uyelikTarih1 = Convert.ToDateTime("01.01.2000 00:00:00");
            _uyelikTarih2 = Convert.ToDateTime("31.12.9999 23:59:59");
        }
        else if (uyelikTarih1 != string.Empty && uyelikTarih2 == string.Empty)
        {
            _uyelikTarih1 = Convert.ToDateTime(uyelikTarih1);
            _uyelikTarih2 = DateTime.ParseExact(uyelikTarih1 + " 23:59:59", "dd.MM.yyyy HH:mm:ss", null);
        }
        else if (uyelikTarih1 == string.Empty && uyelikTarih2 != string.Empty)
        {
            _uyelikTarih1 = Convert.ToDateTime("01.01.2000 00:00:00");
            _uyelikTarih2 = DateTime.ParseExact(uyelikTarih2 + " 23:59:59", "dd.MM.yyyy HH:mm:ss", null);
        }
        else if (uyelikTarih1 != string.Empty && uyelikTarih2 != string.Empty)
        {
            _uyelikTarih1 = Convert.ToDateTime(uyelikTarih1);
            _uyelikTarih2 = DateTime.ParseExact(uyelikTarih2 + " 23:59:59", "dd.MM.yyyy HH:mm:ss", null);
        }
        #endregion

        #region sonGirisTarih

        string sonGirisTarih1 = txtSonGirisTarih1.Text.ToString().Trim();
        string sonGirisTarih2 = txtSonGirisTarih2.Text.ToString().Trim();
        DateTime _sonGirisTarih1 = Convert.ToDateTime("01.01.2000 00:00:00");
        DateTime _sonGirisTarih2 = Convert.ToDateTime("31.12.9999 23:59:59");

        if (sonGirisTarih1 == string.Empty && sonGirisTarih2 == string.Empty)
        {
            _sonGirisTarih1 = Convert.ToDateTime("01.01.2000 00:00:00");
            _sonGirisTarih2 = Convert.ToDateTime("31.12.9999 23:59:59");
        }
        else if (sonGirisTarih1 != string.Empty && sonGirisTarih2 == string.Empty)
        {
            _sonGirisTarih1 = Convert.ToDateTime(sonGirisTarih1);
            _sonGirisTarih2 = DateTime.ParseExact(sonGirisTarih1 + " 23:59:59", "dd.MM.yyyy HH:mm:ss", null);
        }
        else if (sonGirisTarih1 == string.Empty && sonGirisTarih2 != string.Empty)
        {
            _sonGirisTarih1 = Convert.ToDateTime("01.01.2000 00:00:00");
            _sonGirisTarih2 = DateTime.ParseExact(sonGirisTarih2 + " 23:59:59", "dd.MM.yyyy HH:mm:ss", null);
        }
        else if (sonGirisTarih1 != string.Empty && sonGirisTarih2 != string.Empty)
        {
            _sonGirisTarih1 = Convert.ToDateTime(sonGirisTarih1);
            _sonGirisTarih2 = DateTime.ParseExact(sonGirisTarih2 + " 23:59:59", "dd.MM.yyyy HH:mm:ss", null);
        }
        #endregion



        dtUyeler = bllUyelikIslemleri.UyeListesiRaporu(ad, soyad, _uyelikTarih1, _uyelikTarih2, _sonGirisTarih1, _sonGirisTarih2);

        if (drpSort.SelectedValue == "KullaniciAd")
            sorted = "KullaniciAd";
        else
            sorted = drpSort.SelectedValue + " desc , " + "KullaniciAd";

        dvSorted = dtUyeler.DefaultView;
        dvSorted.Sort = sorted;

        rptUyeBilgileri.DataSource = dvSorted.ToTable();
        rptUyeBilgileri.DataBind();
    }


    protected void btnExcelSend_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=siparisListesi.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";

        Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
        Response.Charset = "windows-1254";

        string header = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title></title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1254\" />\n<style>\n</style>\n</head>\n<body>\n";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        rptUyeBilgileri.RenderControl(htmlWrite);
        Response.Write(header + stringWrite.ToString());
        Response.End();
    }
}