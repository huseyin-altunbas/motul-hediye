﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebAdmin/Admin.master" AutoEventWireup="true" CodeFile="SiparisListeleV2.aspx.cs" Inherits="WebAdmin_SiparisListeleV2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
            <%/*<script src="http://code.jquery.com/jquery-1.9.1.js"></script>*/ %>
   <script src="js/jquery-1.10.2.min.js"></script>
<link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
    

<link rel="stylesheet" href="http://kendo.cdn.telerik.com/2016.1.412/styles/kendo.common.min.css" />
<link rel="stylesheet" href="http://kendo.cdn.telerik.com/2016.1.412/styles/kendo.rtl.min.css" />
<link rel="stylesheet" href="http://kendo.cdn.telerik.com/2016.1.412/styles/kendo.silver.min.css" />
<link rel="stylesheet" href="http://kendo.cdn.telerik.com/2016.1.412/styles/kendo.mobile.all.min.css" />

<script src="http://kendo.cdn.telerik.com/2016.1.412/js/kendo.all.min.js"></script>
<script src="http://cdn.kendostatic.com/2014.3.1029/js/jszip.min.js"></script>


   
     <script src="/WebAdmin/js/kendo.tr-TR.js"></script>

      <div style="float: left; width: 1094px;">
   <div id="grid"></div>
       <script>
           var ttt = "";
           $(function () {
               $(".adminContainer").css("width", "1070px");
               $(".adminContainer").css("margin-top", "-19px");

            $("#grid").kendoGrid({
               
                pageable: {
                    refresh: true,
                    pageSizes: true
                },
                columns: [
                   
                    { field: "Siparis_no", width: "50px" },
                    { field: "Cari_id", width: "50px" },
                    { field: "Cari_kod", width: "50px" },
                    { field: "Cari_ad", width: "80px" },
                    { field: "Siparis_tarih", width: "50px", format: "{0:dd/MM/yyyy}" },
                    { field: "Siparis_tutar", width: "50px" },
                    //{ field: "Siparis_statu", width: "50px" },
                    { field: "Siparis_statu", title: "Durum", width: "80px", template: '#=Siparis_statu#<br><a target="_blank" style="color:red;" href="#=kargoLink#">#=isnull(kargoLink)#</a>' },
                     {
                         command: [
                                       { text: "Detay", click: Detay }

                         ],
                         title: "İşlemler",
                         width: "12%"
                     }

                ],
                resizable: true,
                filterable: true,
                sortable: true,
                pageable: true,
                dataBound: resizeGrid,
                filterMenuInit: filterMenu,
                toolbar: [{ name: 'excelv2', text: "Excel'e Aktar", target: '_blank' }],
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 100,
                    filter: [{ "field": "Siparis_tarih", "operator": "gte", "value": "<%=DateTime.Now.AddYears(-5).ToString()%>" }, { "field": "Siparis_tarih", "operator": "lte", "value": "<%=DateTime.Now.AddDays(1).ToString()%>" }],
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "siparis_id",
                            fields: {
                                Siparis_no: { editable: false, nullable: true },
                                Cari_ad: { type: "string" },
                                Cari_id: { type: "number" },
                                Cari_kod: { type: "string" },
                                Siparis_tarih: { type: "date", editable: false, nullable: true },
                                Siparis_tutar: { type: "number" },
                                Siparis_statu: { type: "string" }
                            }
                        }
                    },
                    batch: false, // enable batch editing - changes will be saved when the user clicks the "Save changes" button
                    transport: {
                        create: {
                            url: "/AjaxMethods.asmx/Create", //specify the URL which should create new records. This is the Create method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                        },
                        read: {
                            url: "/AjaxMethods.asmx/SiparisListesi", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (data) {
                                var exportQueryGuid = data.responseJSON.d.Custom;
                                $(".k-grid-excelv2").attr("href", 'SiparisExport.aspx?exportQueryGuid=' + exportQueryGuid)
                                $('.k-grid-excelv2').attr('target', '_blank');
                            }
                        },
                        update: {
                            url: "/AjaxMethods.asmx/Update", //specify the URL from which should update the records. This is the Update method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                        },
                        destroy: {
                            url: "/AjaxMethods.asmx/Destroy", //specify the URL which should destroy records. This is the Destroy method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                        },
                        parameterMap: function(data, operation) {
                            if (operation != "read") {
                                // web service method parameters need to be send as JSON. The Create, Update and Destroy methods have a "products" parameter.
                                return JSON.stringify({ products: data.models })
                            } else {
                                // web services need default values for every parameter
                                data = $.extend({ sort:null, filter: null }, data);

                                return JSON.stringify(data);
                            }
                        }
                    }
                }

            });

            function filterMenu(e) {
                if (e.field == "Siparis_tarih") {
                    var beginOperator = e.container.find("[data-role=dropdownlist]:eq(0)").data("kendoDropDownList");
                    beginOperator.value("gte");
                    beginOperator.trigger("change");

                    var endOperator = e.container.find("[data-role=dropdownlist]:eq(2)").data("kendoDropDownList");
                    endOperator.value("lte");
                    endOperator.trigger("change");
                    debugger;
                    e.container.find(".k-dropdown").hide()
                }


                //if (e.field == "Siparis_no" || e.field == "Cari_id") {
                //    var beginOperator = e.container.find("[data-role=dropdownlist]:eq(0)").data("kendoDropDownList");
                //    beginOperator.value("startswith");
                //    beginOperator.trigger("change");

                //    var endOperator = e.container.find("[data-role=dropdownlist]:eq(2)").data("kendoDropDownList");
                //    endOperator.value("endswith");
                //    endOperator.trigger("change");
                //    debugger;
                //    e.container.find(".k-dropdown").hide()
                //}

               
            }

            function Detay(e) {
                e.preventDefault();
                //Get Data Info
                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                var detailID = dataItem.id;
                $("#ProductId").val(detailID);
                $("#Action").val("edit");
                detailID = dataItem.Siparis_no;
                //alert(detailID);
                if (detailID > 0) {
                    $(document).ready(function () {
                        var myWindow = $("#window"),
                            undo = $("#undo");

                        undo.click(function () {
                            myWindow.data("kendoWindow").open();
                            undo.fadeOut();
                        });

                        function onClose() {
                            undo.fadeIn();
                        }

                        myWindow.kendoWindow({
                            width: "65%",
                            height: "75%",
                            top: "0px",
                            title: "Detay",
                            content: 'siparisDetay.aspx?siparis_id='+detailID+'',
                            visible: false,
                            modal: true,
                            actions: [
                                "Pin",
                                "Minimize",
                                "Maximize",
                                "Close"
                            ],
                            close: onClose
                        }).data("kendoWindow").center().open();
                    });
                } // Enf If
            }


          
           
           });


           function resizeGrid() {
               //var gridElement = $(".k-grid-content");
               //gridElement.css("height", "500px");
               //var gridElement = $("#grid");
               //gridElement.css("height", "300px");
               //var dataArea = gridElement.find(".k-grid-content");
               //var newHeight = gridElement.parent().innerHeight() - 2;
               //var diff = gridElement.innerHeight() - dataArea.innerHeight();
               //gridElement.height(newHeight);
               //dataArea.height(newHeight - diff);
           }

          
           function isnull(a) {
               //debugger;
               if (a != null) {
                   return "Kargom Nerede";
               }
               else {
                   return "";
               }
           }

           function fn_color(str) {
               if (str == "Siparişiniz Isıcam Onayı Beklemektedir.") {
                   //return "<span style='color:yellow; background-color:gray;'>" + str + "<span>";
               }
               if (str == "Siparişiniz Isıcam Tarafından Reddedildi.") {
                   return "<span style='color:red;'>" + str + "<span>";
               }
               if (str == "Siparişiniz Tedarik Aşamasındadır.") {
                   return "<span style='color:green;'>" + str + "<span>";
               }
               if (str == "Kargoda. ") {
                   return "<span style='color:green;'>" + str + "<span>";
               }
               return str
           }

    </script>

      </div>

</asp:Content>

