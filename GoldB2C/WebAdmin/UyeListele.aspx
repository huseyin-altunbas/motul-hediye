﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebAdmin/Admin.master" AutoEventWireup="true"
    CodeFile="UyeListele.aspx.cs" Inherits="WebAdmin_UyeListele" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .cariLink
        {
            color: red;
        }
        .cariLink:hover
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine-tr.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".accordionContainer .accordionBtn:first").addClass("active");
            $(".accordionContainer .accordionDetail:not(:first)").hide();

            $(".accordionContainer .accordionBtn").click(function () {
                $(this).next(".accordionDetail").slideToggle("slow")
		.siblings(".accordionDetail:visible").slideUp("slow");
                $(this).toggleClass("active");
                $(this).siblings(".accordionBtn").removeClass("active");
            });
        });

        

        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

        $(function () {
            $("#<%=txtUyelikTarih1.ClientID%>").datepicker();
            $("#<%=txtUyelikTarih2.ClientID%>").datepicker();
            $("#<%=txtSonGirisTarih1.ClientID%>").datepicker();
            $("#<%=txtSonGirisTarih2.ClientID%>").datepicker();
        });


        jQuery(function ($) {
            $.datepicker.regional['tr'] = {
                closeText: 'kapat',
                prevText: '&#x3c;geri',
                nextText: 'ileri&#x3e',
                currentText: 'bugün',
                monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran',
                'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
                monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz',
                'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
                dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
                dayNamesShort: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                dayNamesMin: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                weekHeader: 'Hf',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['tr']);
        });


    </script>
    <div style="float: left;">
        <asp:Panel ID="pnlAdSoyad" runat="server">
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblAd" runat="server" Text="Ad">
                </asp:Label></div>
            <asp:TextBox ID="txtAd" runat="server"></asp:TextBox>
            &nbsp;&nbsp;<br />
        </asp:Panel>
        <asp:Panel ID="pnlSoyad" runat="server">
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblSoyad" runat="server" Text="Soyad">
                </asp:Label></div>
            <asp:TextBox ID="txtSoyad" runat="server"></asp:TextBox>
            &nbsp;&nbsp;<br />
        </asp:Panel>
        <asp:Panel ID="pnlUyelikTarih" runat="server">
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblUyelikTarih1" runat="server" Text="Üyelik Tarih">
                </asp:Label></div>
            <asp:TextBox ID="txtUyelikTarih1" runat="server"></asp:TextBox>
            ve
            <asp:Label ID="lblUyelikTarih2" runat="server" Text="">
            </asp:Label>
            <asp:TextBox ID="txtUyelikTarih2" runat="server"></asp:TextBox>
            arasında&nbsp;&nbsp;<br />
        </asp:Panel>
        <asp:Panel ID="pnlSonGirisTarih" runat="server">
            <div style="width: 90px; float: left;">
                <asp:Label ID="lblSonGirisTarih1" runat="server" Text="Son Giriş Tarih">
                </asp:Label></div>
            <asp:TextBox ID="txtSonGirisTarih1" runat="server"></asp:TextBox>
            ve
            <asp:Label ID="lblSonGirisTarih2" runat="server" Text="">
            </asp:Label>
            <asp:TextBox ID="txtSonGirisTarih2" runat="server"></asp:TextBox>
            arasında&nbsp;&nbsp;<br />
        </asp:Panel>
    </div>
    <div style="float: right;">
        <asp:Label ID="lblSort" runat="server" Text="Sıralama ">
        </asp:Label>
        <asp:DropDownList ID="drpSort" runat="server">
            <asp:ListItem Value="UyelikTarihi" Text=""></asp:ListItem>
            <asp:ListItem Value="KullaniciAd">Ad</asp:ListItem>
            <asp:ListItem Value="KullaniciSoyad">Soyad</asp:ListItem>
            <asp:ListItem Value="UyelikTarihi">Üyelik Tarihi</asp:ListItem>
            <asp:ListItem Value="SonGirisTarihi">Son Giriş Tarihi</asp:ListItem>
            <asp:ListItem Value="GirisSayisi">Giriş Sayısı</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btnGetir" runat="server" Text="Getir" class="testEtBTN" OnClick="btnGetir_Click" />
        &nbsp;&nbsp;
        <asp:Button ID="btnExcelSend" runat="server" Text="Excel'e Aktar" 
            class="testEtBTN" onclick="btnExcelSend_Click" />
    </div>
    <br />
    <br />
    <table class="accountRoot" style="width: 940px;">
        <%--<div class="acountTitle">
            Siparişlerim</div>--%>
        <!-- accordion content -->
        <tr class="accordionContainer" style="width: 940px;">
            <td>
                <asp:Repeater ID="rptUyeBilgileri" runat="server">
                    <HeaderTemplate>
                        <!-- title -->
                        <table>
                            <tr class="ordertitle" style="width: 940px;">
                                <th class="title4" style="width: 120px;">
                                    Ad
                                </th>
                                <th class="title4" style="width: 120px;">
                                    Soyad
                                </th>
                                <th class="title4" style="width: 200px;">
                                    Email
                                </th>
                                <th class="title2">
                                    Cep Tel Kod
                                </th>
                                <th class="title2">
                                    Cep Tel
                                </th>
                                <th class="title2">
                                    Üyelik Tarihi
                                </th>
                                <th class="title2">
                                    Son Giriş Tarihi
                                </th>
                                <th class="title2">
                                    Giriş Sayısı
                                </th>
                            </tr>
                            <!-- title -->
                    </HeaderTemplate>
                    <ItemTemplate>
                        <!-- chooes -->
                        <tr class="orderchooes" style="width: 940px;">
                            <td class="chooes4" style="width: 120px;">
                                <%#DataBinder.Eval(Container.DataItem, "KullaniciAd")%>
                            </td>
                            <td class="chooes4" style="width: 120px;">
                                <%#DataBinder.Eval(Container.DataItem, "KullaniciSoyad")%>
                            </td>
                            <td class="chooes4" style="width: 200px;">
                                <%#DataBinder.Eval(Container.DataItem, "Email")%>
                            </td>
                            <td class="chooes2">
                                <%#DataBinder.Eval(Container.DataItem, "CepTelKod")%>
                            </td>
                            <td class="chooes2">
                                <%#DataBinder.Eval(Container.DataItem, "CepTelNo")%>
                            </td>
                            <td class="chooes2">
                                <%#DataBinder.Eval(Container.DataItem, "UyelikTarihi", "{0:dd.MM.yyyy}")%>
                            </td>
                            <td class="chooes2">
                                <%#DataBinder.Eval(Container.DataItem, "SonGirisTarihi", "{0:dd.MM.yyyy}")%>
                            </td>
                            <td class="chooes2">
                                <%#DataBinder.Eval(Container.DataItem, "GirisSayisi")%>
                            </td>
                            <%--<td class="accordionBtn">
                                Detaylar
                            </td>--%>
                        </tr>
                        <!-- chooes -->
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <!-- accordion content -->
        <!-- order button -->
        <%--<div class="orderbuttons">
        </div>--%>
        <!-- order button -->
    </table>
</asp:Content>
