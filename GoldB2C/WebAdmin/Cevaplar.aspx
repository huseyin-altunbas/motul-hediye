﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebAdmin/Admin.master" AutoEventWireup="true"
    CodeFile="Cevaplar.aspx.cs" Inherits="WebAdmin_AnketGoster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/goldHeader.css?v=1.2.6" rel="stylesheet" type="text/css" />
    <link href="/css/goldForms.css?v=2" rel="stylesheet" type="text/css" />
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <script src="../js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="../js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="../js/jquery.validationEngine-tr.js" type="text/javascript"></script>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
    <link href="../uploadify/uploadify.css" rel="stylesheet" type="text/css" />
    <script src="../uploadify/jquery.uploadify.js" type="text/javascript" />
    <script src="../uploadify/jquery.uploadify.min.js" type="text/javascript" />
    <script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtCevap.ClientID %>").live('keyup blur', function () {
                var maxlength = "2000";
                var val = $(this).val();

                if (val.length > maxlength) {
                    $(this).val(val.slice(0, maxlength));
                }
            });
        });

        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            GetFiles($("#<%=hdnId.ClientID %>").val());
            $("#file_upload").uploadify(
            {
                'swf': '../uploadify/uploadify.swf',
                'uploader': '../Common/TicketUploadAnswers.ashx',
                'auto': true,
                'method': 'post',
                'multi': true,
                'buttonText': 'Dosya Seç',
                'buttonClass': 'link-button-RedBtn spriteRedBtn',
                'onUploadStart': function (file) {
                    $("#file_upload").uploadify('settings', 'formData', { 'FolderId': $("#<%=hdnId.ClientID %>").val() });
                },
                'onUploadSuccess': function (file, data, response) {
                    var dataS = data.split("&");
                    var FolderId = dataS[0].toString();
                    var FileName = dataS[1].toString();
                    if ($("#<%=hdnId.ClientID %>").val() == "") {
                        $("#<%=hdnId.ClientID %>").val(FolderId);
                    }

                    GetFiles(FolderId);

                }
            });

        });

        function GetFiles(folderId) {

            var folderHtml = "";
            $("#file_upload_success").html(folderHtml);

            if (folderId != "") {
                $.ajax({
                    type: 'POST',
                    url: '/WebAdmin/Cevaplar.aspx/GetFiles',
                    data: '{ "folderId":"' + folderId + '"}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        $.each(result.d, function (index, value) {
                            var _fileName = value.FileName.split("=")[1].toString();
                            folderHtml = folderHtml + "<div style=\"margin:10px;font-size:16px;\"><span>" + _fileName + "</span><a style=\"margin:10px;color:Red;font-size:16px;\" target=\"_blank\" href=\"../TicketImage/TicketAnswers/" + value.FolderId + "/" + value.FileName + "\"> Önizleme</a>-<a style=\"color:#0099cc;cursor:pointer\" OnClick=\"FileDelete('" + value.FolderId + "','" + value.FileName + "');return false;\" >Sil</a></div>";
                            $("#file_upload_success").html(folderHtml);

                        });
                    }
                });
            }
        }

        function FileDelete(folderId, fileName) {
            var con = confirm('Silmek istediğinize emin misiniz?');
            if (con) {
                if (folderId != "" && fileName != "") {
                    $.ajax({
                        type: 'POST',
                        url: '/WebAdmin/Cevaplar.aspx/FileDelete',
                        data: '{ "folderId":"' + folderId + '","fileName":"' + fileName + '"}',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function (result) {
                            if (result.d == "1")
                                GetFiles(folderId);
                        }
                    });
                }
            }
        }
        
    </script>
    <asp:HiddenField ID="hdnId" runat="server" />
    <div style="float: right;">
        <asp:LinkButton ID="lnkSorularaGit" CssClass="link-button-RedBtn spriteRedBtn" runat="server"
            OnClick="lnkSorularaGit_Click"><span class="spriteRedBtn fontTre fontSz16"> Sorulara Git</span></asp:LinkButton>
    </div>
    <div style="float: right;">
        <asp:LinkButton ID="lnkCevaplariYenile" CssClass="link-button-RedBtn spriteRedBtn"
            runat="server" OnClick="lnkCevaplariYenile_Click"><span class="spriteRedBtn fontTre fontSz16"> Cevapları Yenile</span></asp:LinkButton>
    </div>
    <div style="clear: both;">
    </div>
    <div>
        <strong>Soru No:</strong>&nbsp; <span style="font-style: italic; font-size: 14px;">
            <asp:Label ID="lblSoruId" runat="server"></asp:Label></span>
    </div>
    <div>
        <strong>Sipariş No:</strong>&nbsp; <span style="font-style: italic; font-size: 14px;">
            <asp:Label ID="lblSiparisId" runat="server"></asp:Label></span>
    </div>
    <div>
        <strong>Kullanici Adı:</strong>&nbsp; <span style="font-style: italic; font-size: 14px;">
            <asp:Label ID="lblKullaniciAdSoyad" runat="server"></asp:Label></span>
    </div>
    <br />
    <div>
        <strong>Soru:</strong><br />
        <span style="font-style: italic; font-size: 14px;">
            <asp:Label ID="lblSoru" runat="server"></asp:Label></span>
    </div>
    <div class="contentform">
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Cevap</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtCevap" CssClass="w230" runat="server" TextMode="MultiLine" Width="600"
                    Height="60"></asp:TextBox><span style="color: #00965E; font-style: italic; font-size: 10px;">(En
                        fazla 2000 karakter)</span>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqCevap" runat="server" ControlToValidate="txtCevap"
                        ValidationGroup="grpCevap" ErrorMessage="Cevabınızı girin."></asp:RequiredFieldValidator></span>
            </div>
        </div>
        <div style="clear: both;">
        </div>
        <div class="upload-wrap" style="margin: 60px 20px 20px 20px;">
            <input id="file_upload" type="file" name="file_upload" />
        </div>
        <div id="file_upload_success">
        </div>
        <div class="fleft h45 color4e mt20 ml28 w350">
            <asp:LinkButton ID="lnkKaydet" CssClass="link-button-RedBtn spriteRedBtn" runat="server"
                ValidationGroup="grpCevap" OnClick="lnkKaydet_Click"><span class="spriteRedBtn fontTre fontSz16"> Kaydet</span></asp:LinkButton>
        </div>
    </div>
    <table class="accountRoot" style="width: 940px;">
        <tr class="accordionContainer" style="width: 940px;">
            <td>
                <asp:Repeater ID="rptCevaplar" runat="server">
                    <HeaderTemplate>
                        <table>
                            <tr class="ordertitle" style="width: 940px;">
                                <th class="title3" style="width: 60px;">
                                    Soru No
                                </th>
                                <th class="title3">
                                    Cevap Tarihi
                                </th>
                                <th class="title5" style="width: 530px;">
                                    Cevap
                                </th>
                                <th class="title3" style="width: 200px;">
                                    Cevap Veren Kullanıcı
                                </th>
                            </tr>
                            <tr class="ordertitle" style="width: 940px; height: 40px; margin-top: 0px; padding-top: 0px;">
                                <th style="width: 900px; font-size: 12px; margin-top: 0px; padding-top: 0px;">
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id="trOrderChooes" runat="server" class="orderchooes" style="width: 940px; border: 1px solid #ccc;
                            background-color: White; margin-bottom: 0px;">
                            <td class="chooes3" style="width: 60px;">
                                <%#DataBinder.Eval(Container.DataItem, "SoruId")%>
                            </td>
                            <td class="chooes3">
                                <%#DataBinder.Eval(Container.DataItem, "CevapTarihi", "{0:dd.MM.yyyy HH:mm}")%>
                            </td>
                            <td class="chooes5" style="width: 530px;">
                                <%#DataBinder.Eval(Container.DataItem, "Cevap")%>
                            </td>
                            <td class="chooes3" style="width: 200px;">
                                <%#string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "CevapVerenKullaniciAdSoyad").ToString()) ? "" : DataBinder.Eval(Container.DataItem, "CevapVerenKullaniciAdSoyad")%>
                            </td>
                        </tr>
                        <tr id="trAdd" runat="server" style="width: 940px; background-color: #FFF7ED; margin-bottom: 20px;
                            padding-top: 0px;">
                            <td style="width: 900px; font-size: 12px; margin-top: 0px; padding-top: 0px;">
                                Dosyalar :&nbsp;
                                <%#DataBinder.Eval(Container.DataItem, "DosyaYolu")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
</asp:Content>
