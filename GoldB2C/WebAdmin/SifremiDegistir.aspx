﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebAdmin/Admin.master" AutoEventWireup="true"
    CodeFile="SifremiDegistir.aspx.cs" Inherits="WebAdmin_SifremiDegistir" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/goldForms.css?v=2" rel="stylesheet" type="text/css" />
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">
        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }
    </script>
    <div class="contentForm">
        <!-- parameter -->
        <div class="formparameter">
            <div class="formlabel">
                Mevcut Şifreniz
            </div>
            <div class="formdata">
                <asp:TextBox ID="txtMevcutSifre" runat="server" TextMode="Password" CssClass="w230" oncopy="return false" onpaste="return false" oncut="return false" ></asp:TextBox>
            </div>
            <div class="formnotification">
                <asp:RequiredFieldValidator ID="rqMevcutSifre" runat="server" ErrorMessage="Mevcut şifrenizi giriniz."
                    ControlToValidate="txtMevcutSifre" ValidationGroup="grpSifremiDegistir" CssClass="w230"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- parameter -->
        <!-- parameter -->
        <div class="formparameter">
            <div class="formlabel">
                Yeni Şifreniz
            </div>
            <div class="formdata">
                <asp:TextBox ID="txtYeniSifre" runat="server" TextMode="Password" CssClass="w230" oncopy="return false" onpaste="return false" oncut="return false" ></asp:TextBox>
            </div>
            <div class="formnotification">
                <asp:RequiredFieldValidator ID="rqYeniSifre" runat="server" ErrorMessage="Yeni şifrenizi giriniz."
                    ControlToValidate="txtYeniSifre" ValidationGroup="grpSifremiDegistir" CssClass="w230"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- parameter -->
        <!-- parameter -->
        <div class="formparameter">
            <div class="formlabel">
                Yeni Şifreniz Tekrar
            </div>
            <div class="formdata">
                <asp:TextBox ID="txtYeniSifreTekrar" runat="server" TextMode="Password" CssClass="w230" oncopy="return false" onpaste="return false" oncut="return false" ></asp:TextBox>
            </div>
            <div class="formnotification">
                <asp:RequiredFieldValidator ID="rqYeniSifreTekrar" runat="server" ErrorMessage="Yeni şifrenizi tekrar giriniz."
                    ControlToValidate="txtYeniSifreTekrar" ValidationGroup="grpSifremiDegistir" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <!-- parameter -->
        <!-- button -->
        <div class="formbutton">
            <asp:LinkButton ID="btnSifremiDegistir" CssClass="link-button-RedBtn spriteRedBtn"
                ValidationGroup="grpSifremiDegistir" runat="server" OnClick="btnSifremiDegistir_Click"><span class="spriteRedBtn fontTre fontSz14">
                        Şifremi Değiştir</span></asp:LinkButton>
        </div>
        <!-- button -->
        <!-- request -->
        <div class="formnotification">
            <asp:Label ID="lblSonuc" runat="server" Text=""></asp:Label>
        </div>
        <!-- request -->
    </div>
</asp:Content>
