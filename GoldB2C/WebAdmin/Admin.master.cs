﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cEntity;
using Goldb2cBLL;

public partial class WebAdmin_Admin : System.Web.UI.MasterPage
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        Kullanici kullanici;
        if (Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)Session["Kullanici"];

            if (kullanici.Email!= "hazal.aydemir@tutkal.com.tr")
            {
                Session.Abandon();
                HttpContext.Current.Response.Redirect("~/WebAdmin/UyeGirisi.aspx", false);
            }
        }
        else
        {
            //UyelikIslemleriBLL.MusteriGirisineYonlendir(Request.Url.PathAndQuery);
            HttpContext.Current.Session["SonUrl"] = Request.Url.PathAndQuery;
            HttpContext.Current.Response.Redirect("~/WebAdmin/UyeGirisi.aspx", false);
        }
    }

    protected void lnbCikis_Click(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {
            bllUyelikIslemleri.MusteriKullaniciCikis(((Kullanici)Session["Kullanici"]).KullaniciId);
            Session.Abandon();
        }

        Response.Redirect("/", false);
    }
}
