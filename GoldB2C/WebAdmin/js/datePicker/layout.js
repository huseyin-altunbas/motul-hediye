(function ($) {
    var initLayout = function () {
        $('#clearSelection').bind('click', function () {
            $('#tarihAralik').DatePickerClear();
            return false;
        });
        $('#tarihAralik').DatePicker({
            flat: true,
            date: ['', ''],
            current: '',
            calendars: 3,
            mode: 'range',
            starts: 1,
            format: 'd-m-Y'
        });

        var now3 = new Date();
        now3.addDays(-4);
        var now4 = new Date()
        $('#widgetCalendar').DatePicker({
            flat: true,
            format: 'd B, Y',
            date: [new Date(now3), new Date(now4)],
            calendars: 3,
            mode: 'range',
            starts: 1,
            onChange: function (formated) {
                $('#widgetField span').get(0).innerHTML = formated.join(' &divide; ');
            }
        });
        var state = false;
        $('#widgetField>a').bind('click', function () {
            $('#widgetCalendar').stop().animate({ height: state ? 0 : $('#widgetCalendar div.datepicker').get(0).offsetHeight }, 500);
            state = !state;
            return false;
        });
        $('#widgetCalendar div.datepicker').css('position', 'absolute');
    };

    EYE.register(initLayout, 'init');
})(jQuery)


//date: ['2009-12-28','2010-01-23'],
//current: '2010-01-01',