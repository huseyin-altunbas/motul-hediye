﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true" CodeFile="AkaryakitMesaj.aspx.cs" Inherits="AkaryakitMesaj" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="columns-container">
        <div class="container" id="columns" style="padding-left: 15px;">
           <div class="row">
                 <div>
             
			 <p><h3 style="color:#800000;"> <b>ÖNEMLİ UYARI:</b> </h3></p>
                     <br />
                    <%-- <h4 style="color:#000000" > 23.12.2019-05.01.2020 tarihleri arasında limit aktarımları sebebi ile PO İstasyonlarından akaryakıt alımı yapılamayacaktır. Alımlara 06.01.2020 tarihi itibari ile başlayabilirsiniz. </h4>--%>
			 <h3><strong><span style="color: #800000;"> <br>

                 PETROL OFİSİ akaryakıt kartları, plakaya özel oluşturulmakta ve kart üzerinde araç plakası yer almaktadır.  <br /><br />

PETROL OFİSİ akaryakıt kartları tek kullanımlık değildir. Yeni siparişlerinizde, aynı plakanız için oluşturulan PETROL OFİSİ kartınıza sipariş tutarı bakiye olarak yüklenecektir. Bu nedenle kartınızı ATMAYINIZ. <br /><br />

PETROL OFİSİ kartları tüm Türkiye’de bulunan ve sisteme dahil olan PO istasyonlarında geçerlidir. <br /><br />

Almış olduğunuz Akaryakıt kartı pasiftir. Kargo teslimatı tarafınıza sağlandıktan sonra aktivasyon işlemi gerçekleşecektir. Ancak kargo bilgisi 1 gün sonra ulaşacağından acil işlemleriniz için ilgili müşteri danışma hattını arayarak aktivasyon işleminizi gerçekleştirebilirsiniz. <br /><br />


			             </span></strong></h3>

<hr />
<h3>&nbsp;</h3>
			  
			  
			  &nbsp;</p>
			 
			 
			 <p><strong>Değerli  Üyemiz,</strong></p>
               <p>Akaryakıt talebiniz için Akaryakıt kategorisini tıklayarak giriş yapabilirsiniz.
Seçmek istediğiniz akaryakıt kartını sepete ekleyin.</p>
                     <p style="color:red;">
                         Dikkat: Seçtiğiniz akaryakıt ürününü, yakıt türüne göre seçmiş olmalısınız! Ürünlerin isminde ve ürün görsellerinde yakıt türlerine göre ayrım mevcuttur.
                     </p>
             <p>&nbsp;</p>
<p>Akaryakıt Bilgisi alanına geldiğinizde, daha önce hiç yakıt kart almadıysanız veya kayıtlı plakalarınızdan başka bir plaka için sipariş geçecekseniz “Yeni Kart İstiyorum” butonunu tıklayarak plakanızı yazınız. Bu plakaya açtığınız siparişin tutarı ve siparişi oluştururken seçtiğiniz aracın akaryakıt türü otomatik olarak doldurulmuş gelecektir. Devam ederseniz değiştirme işlemi yapamayacağınızdan, seçtiğiniz akaryakıt ürününün, akaryakıt türünü (benzin / motorin) kontrol ediniz. Yüklenecek tutar bölümü, siparişinizdeki toplam akaryakıt tutarını geçemez. (Bu alanda seçeceğiniz plaka sisteme kayıt olacak ve sonraki siparişlerinizde aynı plakaya yükleyeceğiniz bakiyeler için size kolaylık sağlayacaktır.)</p>
<p>&nbsp;</p>
<p> Kaydettiğiniz plakanın seçimini yapın     “Kabul Ediyorum” butonunu tıklayın. </p>
                     <p> Mevcut plakalarınıza sipariş oluşturduysanız, kayıtlı olan plakanızın yanında bulunan “BU PLAKAYA SEÇİM YAP” butonunu tıklayın ve kaydedin. Plakanıza bakiye yüklemesini, plakanızın yanında bulunan alandan kontrol ettikten sonra “KABUL EDİYORUM”  butonunu tıklayın. Sonraki adımda, sipariş onayını vererek siparişi tamamlayabilirsiniz. </p>
<p>&nbsp;</p>
                   

<p><strong><em>Önemli bilgiler: </em></strong></p>
  <ul style="color:red;">
    <li>
        Almış olduğunuz Akaryakıt kartı pasiftir. Kargo teslimatı tarafınıza sağlandıktan sonra aktivasyon işlemi gerçekleşecektir. Ancak kargo bilgisi 1 gün sonra ulaşacağından acil işlemleriniz için ilgili müşteri danışma hattını arayarak aktivasyon işleminizi gerçekleştirebilirsiniz.  <br />
      <br />
    </li>
    <%--<li> Akaryakıt kartınıza, Lpg alımlarında kullanılamaz. Sadece benzin ve motorin alımlarınızda kullanabilirsiniz. <br /><br />
      <div class="row">
          <div class="col-md-12">
          <img src="images/1a.jpg" style="border:1px solid #ccc;" />
      </div>
      </div>
      <br />
    </li>--%>
    <li>Her Akaryakıt kartınıza 1 plaka tanımlaması yapılmaktadır.  Aynı karta ikinci bir plaka tanımlanamaz. Aynı plakaya geçeceğiniz ikinci siparişleriniz için, tarafınıza daha önceden gönderilen yakıt kartına bakiye eklemesi yapılacağından, mevcut kartlarınızı atmayınız. <br /><br />
      <div class="row">
           <div class="col-md-12"> <img src="images/2a.jpg" style="border:1px solid #ccc;" /> </div>
      </div>
      <br />
      <br />
          <p>Mevcut plakalarınıza sipariş oluşturduysanız, kayıtlı olan plakanızın yanında bulunan “BU PLAKAYA SEÇİM YAP” butonunu tıklayın. Plakanıza bakiye yüklemesini, plakanızın yanında bulunan alandan kontrol ettikten sonra “KABUL EDİYORUM” butonunu tıklayın. Sonraki adımda, sipariş onayını vererek siparişi tamamlayabilirsiniz.</p>
        <br />
          <div class="row">
           <div class="col-md-12"> <img src="images/tutar-onizleme.jpg" style="border:1px solid #ccc;" /> </div>
      </div>
      <br />

          <div class="row">
           <div class="col-md-12"> <img src="images/4a.PNG" style="border:1px solid #ccc;" /> </div>
      </div>
      <br />

    </li>
    <% /*
<li>Aynı plakaya geçeceğiniz ikinci siparişleriniz için, tarafınıza daha önceden gönderilen yakıt kartınıza bakiye eklemesi yapılacağından, mevcut kartlarınızı atmayınız.<br />
<img src="images/3.png" width="449" height="219" /><br />
<br />
<br />
</li>*/ %>


    <li> Akaryakıt Kartınız ile ilgili tüm sorularınız için proje müşteri danışma hattımızı arayınız.</li>
  </ul>


                       <p>&nbsp;</p>
                       <p><strong><em>Önemli Uyarı: </em></strong></p>
                      <p style="font-size: 16px;">
                 Sistem güncellemesi nedeni ile yeni dönem Akaryakıt siparişlerinizi oluştururken, her siparişinize 1 plaka tanımlaması yapabilirsiniz.<br />
                    İkinci bir plaka tanımlamak isterseniz yeni sipariş oluşturmanız gerekmektedir.<br />

                    Daha önceden mevcut kartınız varsa tekrar aynı plaka ile işlem yapabilirsiniz.

              </p>

                     <p>&nbsp;</p>

  <p>Teşekkürler</p>
  <p>&nbsp;</p>
  
           </div>


            <div class="TabloBottom">
                                   <% if (Request.QueryString["btngizle"] == null)
    { %>
                                    <div class="Gonder">
                                        <a class="button" href="/AkaryakitKartBilgileri.aspx"> Devam </a>
                                    </div>
                <%} %>
                                </div>
           </div>
        </div>
    </div>
</asp:Content>

