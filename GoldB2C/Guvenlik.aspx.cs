﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;

public partial class Guvenlik : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Rastgele bir kod oluşturduk ve Session nesnesinde sakladık.

        this.Session["GuvenlikKodu"] = CodeOlustur();

        // Session da saklanan kod dan güvenlik resmi oluşturma.

        // Karakter sayısına göre oluşturulacak olan güvenlik resminin genişliği ayarlanmalıdır.

        GuvenlikOlustur ci = new GuvenlikOlustur(this.Session["GuvenlikKodu"].ToString(), 200, 60);

        // Çıktıyı JPEG görüntüsüne dönüştürdük.

        this.Response.Clear();

        this.Response.ContentType = "image/jpeg";



        ci.Image.Save(this.Response.OutputStream, ImageFormat.Jpeg);

        // Güvenlik Resmini görüntüye aktardık.

        ci.Dispose();
    }

    private string CodeOlustur()
    {

        int karaktersayisi = 5;

        Random r = new Random();

        string s = "";

        for (int j = 0; j < karaktersayisi; j++)
        {

            int i = r.Next(3);

            int ch;

            switch (i)
            {

                case 1:

                    ch = r.Next(0, 9);

                    s = s + ch.ToString();

                    break;

                case 2:

                    ch = r.Next(65, 90);

                    s = s + Convert.ToChar(ch).ToString();

                    break;

                case 3:

                    ch = r.Next(97, 122);

                    s = s + Convert.ToChar(ch).ToString();

                    break;

                default:

                    ch = r.Next(97, 122);

                    s = s + Convert.ToChar(ch).ToString();

                    break;

            }

            r.NextDouble();

            r.Next(100, 1999);

        }

        return s;

    }
}