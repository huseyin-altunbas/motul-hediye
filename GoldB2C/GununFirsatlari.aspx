﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="GununFirsatlari.aspx.cs" Inherits="GununFirsatlari" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/GununFirsatlari.css" rel="stylesheet" type="text/css" />
    <%--<script type="text/javascript">
        debugger;
        $(document).ready(function () {
            $(".rootercontainer:ul li").each(function () {
                alert('sfdf');
                $(this).addClass("foo");
            });
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="rootercnav">
        <div class="rcframe">
            <div style="width: 100%;" class="rootername">
                <a href="http://https://motulteam.com/">Anasayfa</a> / Günün Fırsatları
            </div>
        </div>
    </div>
    <asp:Repeater ID="rptGununFirsatlari" runat="server">
        <HeaderTemplate>
            <div class="rootercontainer">
                <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <%# Container.ItemIndex % 4 == 0 ? "<li  class=\"no-padding\">" : "<li>" %>
            <div>
                <a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>" class="motto" title="<%# DataBinder.Eval(Container.DataItem,"spot_kelime") %>">
                    <%# DataBinder.Eval(Container.DataItem,"spot_kelime") %>
                </a><a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>" class="thumbPicture"
                    title="<%# DataBinder.Eval(Container.DataItem,"firsat_aciklama") %>">
                    <img src="//<%=ImageDomain %>/urunresim/BuyukResim/<%# DataBinder.Eval(Container.DataItem,"resim_ad") %>"
                        alt="<%# DataBinder.Eval(Container.DataItem,"firsat_aciklama") %>" />
                </a><a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>" class="motto2"
                    title="<%# DataBinder.Eval(Container.DataItem,"firsat_aciklama") %>">
                    <%# DataBinder.Eval(Container.DataItem,"firsat_aciklama") %>
                </a><a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>" class="productName"
                    title="<%# DataBinder.Eval(Container.DataItem,"firsat_ekbilgi") %>">
                    <%# DataBinder.Eval(Container.DataItem,"firsat_ekbilgi") %>
                </a><a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>" class="burut"
                    title="<%# DataBinder.Eval(Container.DataItem,"firsat_aciklama") %>">
                    <%# DataBinder.Eval(Container.DataItem,"liste_fiyat") %>&nbsp;TL</a> <a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>"
                        class="net" title="<%# DataBinder.Eval(Container.DataItem,"firsat_aciklama") %>">
                        <%# DataBinder.Eval(Container.DataItem,"indirimli_fiyat") %>&nbsp;TL</a>
            </div>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul> </div></FooterTemplate>
    </asp:Repeater>
    <%--<li class="no-padding">--%>
</asp:Content>
