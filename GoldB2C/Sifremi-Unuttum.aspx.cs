﻿using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Image1.ImageUrl = "~/Guvenlik.aspx";
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (this.Session["GuvenlikKodu"] != null && this.Session["GuvenlikKodu"].ToString() == guvenlikkodu.Text.Trim())
        {
            if (!string.IsNullOrEmpty(epostaadresiniz.Text) && !string.IsNullOrEmpty(guvenlikkodu.Text))
            {
                DataTable kullanici = bllUyelikIslemleri.GetirKullaniciByEmail(epostaadresiniz.Text.Trim());
                if (kullanici.Rows.Count > 0)
                {
                    bool AktifMi = bool.Parse(kullanici.Rows[0]["Aktif"].ToString());
                    if (!AktifMi)
                    {
                        mesajGoster("Acente üyeliğiniz pasife alındığı için şifremi unuttum seçeneği kullanılamaz. Detaylı bilgi için Acente İlişkileri ve Satış Yönetimi Müdürlüğü.");
                        return;
                    }

                    string MevcutSifre = kullanici.Rows[0]["Sifre"].ToString();

                    Crypt crypt = new Crypt();
                    string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
                    MevcutSifre = crypt.DecryptData(encryptKey, MevcutSifre);

                    string KullaniciSifre = DateTime.Now.ToString("ddHHmmss");

                    bllUyelikIslemleri.GuncelleKullaniciSifre(int.Parse(kullanici.Rows[0]["KullaniciId"].ToString()), MevcutSifre, KullaniciSifre);

                    System.IO.StreamReader reader = new System.IO.StreamReader(Server.MapPath("MailSablon/SifremiUnuttum.htm"));
                    string Reader = reader.ReadToEnd();
                    


                    //MailMessage mail = new MailMessage();
                    //mail.From = new MailAddress(gondericiAdresi, "AnadoludanASPara.Com - Şifre Değiştirme Talebi");
                    //mail.To.Add(kullanici.Rows[0]["Email"].ToString());

                    //mail.Subject = mailBaslik;
                    //mail.IsBodyHtml = true;

                    //SmtpClient mailClient = new SmtpClient();
                    //mailClient.Host = mailServerName;
                    //mailClient.Port = smtpPort;
                    //mailClient.EnableSsl = true;
                    //mailClient.Credentials = new System.Net.NetworkCredential(gondericiAdresi, sifre.ToString());

                    string message = Reader.Replace("#adsoyad#", kullanici.Rows[0]["KullaniciAd"].ToString() + " Yetkilisi")
                                .Replace("#email#", kullanici.Rows[0]["CRMCustomerGuID"].ToString())
                                .Replace("#sifre#", KullaniciSifre);

                    //mail.Body = message;

                    //mail.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");

                    //try
                    //{
                    //    mailClient.Send(mail);
                    //    Session["MailSended"] = true;
                    //    Button1.Enabled = false;
                    //}
                    //catch (Exception)
                    //{
                    //    throw;
                    //}

                    //mail.Dispose();


                    SendEmail mail2 = new SendEmail();
                    mail2.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
                    mail2.MailTo = new string[] { kullanici.Rows[0]["Email"].ToString() };
                    mail2.MailBcc = new string[] { "altunbas.huseyin@gmail.com" };

                    mail2.MailHtml = true;
                    mail2.MailSubject = "motulteam.com Şifre Değiştirme Talebi";

                    mail2.MailBody = message;
                    bool sonuc = mail2.SendMail(false);


                    mesajGoster("Şifreniz E-Posta adresinize başarıyla gönderildi. Lütfen e-posta kutunuzu kontrol ediniz...");
                }
                else
                {
                    mesajGoster("E-Posta Adresiniz sistemimizde bulunamadı. Eğer E-Posta adresinizi hatırlamıyorsanız Motul ile görüşebilirsiniz.");
                }
            }
            else
            {
                mesajGoster("Lütfen E-Posta Adresinizi ve Güvenlik Kodunu girin!");
            }
        }
        else
        {
            mesajGoster("Güvenlik Kodu Hatalı!");
        }
    }

    public void mesajGoster(string mesaj)
    {
        string strMessage = "";
        strMessage = mesaj;
        //string strScript = "<script type=\"text/javascript\">" +
        //        "$(document).ready(function(){" +
        //        "$.msgbox(\"" + strMessage + "\", { buttons: [ {type: \"submit\", value: \"Tamam\"} ] });" +
        //        "});" +
        //        "</script>";

        string strScript = "<script> alert('"+ strMessage + "') </script>";

        if ((!Page.ClientScript.IsStartupScriptRegistered("clientScript")))
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "clientScript", strScript);
        }
    }
}