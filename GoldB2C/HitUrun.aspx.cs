﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;

public partial class HitUrun : System.Web.UI.Page
{
   
    HitUrunIslemleriBLL bllHitUrunIslemleri = new HitUrunIslemleriBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        string HitUrunTip = "";

        if (!IsPostBack)
        {

            if (!string.IsNullOrEmpty(Request.QueryString["HitUrunTip"]))
            {
                DataTable dtHitUrunKategoriler = new DataTable();
                HitUrunTip = Request.QueryString["HitUrunTip"].ToString();
                dtHitUrunKategoriler = bllHitUrunIslemleri.HitUrunKategoriListesi(HitUrunTip);

                if (dtHitUrunKategoriler != null && dtHitUrunKategoriler.Rows.Count > 0)
                {

                    int SeciliUstGrupId = 1;
                    string AyakIzi = "";
                    DataTable dtUrunler = new DataTable();
                    int UstGrupId, AltGrupId;
                    if (Request.QueryString["UstGrupId"] != null && int.TryParse(Request.QueryString["UstGrupId"], out UstGrupId) && Request.QueryString["AltGrupId"] != null && int.TryParse(Request.QueryString["AltGrupId"], out AltGrupId))
                    {
                        DataRow[] dr = dtHitUrunKategoriler.Select("ustgrup_id = '" + UstGrupId.ToString() + "' and altgrup_id = '" + AltGrupId.ToString() + "'");
                        dtUrunler = bllHitUrunIslemleri.HitUrunKartListesi(HitUrunTip, UstGrupId, AltGrupId);
                        if (dr != null & dr.Length > 0)
                            AyakIzi = dr[0]["hit_baslik"].ToString() + " / " + dr[0]["altgrup_ad"].ToString();
                        SeciliUstGrupId = UstGrupId;
                    }
                    else
                    {
                        AltGrupId = Convert.ToInt32(dtHitUrunKategoriler.Rows[0]["altgrup_id"]);
                        UstGrupId = Convert.ToInt32(dtHitUrunKategoriler.Rows[0]["ustgrup_id"]);
                        dtUrunler = bllHitUrunIslemleri.HitUrunKartListesi(HitUrunTip, UstGrupId, AltGrupId);
                        AyakIzi = dtHitUrunKategoriler.Rows[0]["hit_baslik"].ToString() + " / " + dtHitUrunKategoriler.Rows[0]["altgrup_ad"].ToString();
                    }


                    HitUrunlerMenu1.MenuOlustur(dtHitUrunKategoriler, SeciliUstGrupId.ToString());

                    ltrHitUrunBaslik.Text = AyakIzi;

                    rptUrunler.DataSource = dtUrunler;
                    rptUrunler.DataBind();


                }
            }
        }
        DataTable dt = new DataTable();
        dt = bllHitUrunIslemleri.HitUrunKategoriListesi("En Çok Satan");



    }

    protected void rptUrunler_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //Ürün Fiyatları
        DataRowView drv = e.Item.DataItem as DataRowView;
        Label lblBrutTutar = (Label)e.Item.FindControl("lblBrutTutar");
        Label lblNetTutar = (Label)e.Item.FindControl("lblNetTutar");
        Label lblBilgi = (Label)e.Item.FindControl("lblBilgi");

        if (Convert.ToBoolean(drv.Row["temin_durum"]))
        {
            decimal brut_tutar = Math.Round(Convert.ToDecimal(drv.Row["brut_tutar"]), 2);
            decimal net_tutar = Math.Round(Convert.ToDecimal(drv.Row["net_tutar"]), 2);

            decimal onikiTaksit = Math.Round((Convert.ToDecimal(drv.Row["net_tutar"]) / 12), 2);

            //lblNetTutar.Text = onikiTaksit.ToString() + " PUAN";
            //lblBilgi.Text = "x 12 Taksit";

            lblBilgi.Text = "KDV Dahil";
            if (brut_tutar > net_tutar)
            {
                lblBrutTutar.Text = brut_tutar.ToString() + "PUAN";
                lblNetTutar.Text = net_tutar.ToString() + "PUAN";
            }
            else
            {
                lblBrutTutar.Visible = false;
                lblNetTutar.Text = brut_tutar.ToString() + "PUAN";
            }
        }
        else
        {
            lblBilgi.Text = "Ürün geçici olarak temin edilemiyor.";
        }

        //İkonlar
        //if (Convert.ToBoolean(drv.Row["indirimli"]))
        //    ((Image)e.Item.FindControl("imgIndirimli")).ImageUrl = "~/imagesgld/icon_indirimliurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgIndirimli")).ImageUrl = "~/imagesgld/icon_indirimliurunOff.png";

        //if (Convert.ToBoolean(drv.Row["aynigun_sevk"]))
        //    ((Image)e.Item.FindControl("imgAyniGunSevk")).ImageUrl = "~/imagesgld/icon_hizlikargoOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgAyniGunSevk")).ImageUrl = "~/imagesgld/icon_hizlikargoOff.png";

        //if (Convert.ToBoolean(drv.Row["hediyeli_urun"]))
        //    ((Image)e.Item.FindControl("imgHediyeliUrun")).ImageUrl = "~/imagesgld/icon_hediyeliurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgHediyeliUrun")).ImageUrl = "~/imagesgld/icon_hediyeliurunOff.png";

        //if (Convert.ToBoolean(drv.Row["kuryeile_sevk"]))
        //    ((Image)e.Item.FindControl("imgKuryeileSevk")).ImageUrl = "~/imagesgld/icon_coksatanurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgKuryeileSevk")).ImageUrl = "~/imagesgld/icon_coksatanurunOff.png";

        //if (Convert.ToBoolean(drv.Row["kargo_bedava"]))
        //    ((Image)e.Item.FindControl("imgKargoBedava")).ImageUrl = "~/imagesgld/icon_kargobedavaOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgKargoBedava")).ImageUrl = "~/imagesgld/icon_kargobedavaOff.png";

        //if (Convert.ToBoolean(drv.Row["yeni_urun"]))
        //    ((Image)e.Item.FindControl("imgYeniUrun")).ImageUrl = "~/imagesgld/icon_yeniurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgYeniUrun")).ImageUrl = "~/imagesgld/icon_yeniurunOff.png";

    }
}