﻿using Goldb2cEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tatil : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {
            Kullanici kullanici = (Kullanici)Session["Kullanici"];
            //string url = "http://localhost:52931/user/index?token=" + kullanici.ServiceKey;
            string url = "https://tatil.tutkal.com.tr/AnadoluSigorta/User/?token=" + kullanici.ServiceKey;
            Response.Redirect(url);
        }
        else
        {
            Response.Redirect("http://anadolu-sigorta-test.tutkal.com.tr/");
        }
    }
}