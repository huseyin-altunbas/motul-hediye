﻿using Goldb2cBLL;
using Goldb2cEntity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HediyeCek : System.Web.UI.Page
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {

            Kullanici kullanici = (Kullanici)Session["Kullanici"];
            //var client = new RestClient("http://localhost:60613/AnadoluSigorta/User/Index/"+kullanici.ServiceKey);
            //var request = new RestRequest(Method.POST);
            //request.AddHeader("Content-type", "application/json");
            //request.AddHeader("Token", kullanici.ServiceKey);


            //IRestResponse response = client.Execute(request);
            //dynamic data = JObject.Parse(response.Content);
            //string link = data.Link;
            //Response.Redirect(link);

            Response.Clear();
            //Response.Write(
            //        string.Format(
            //            @"<form action='{0}' id='test' method='POST'><input type='hidden' name='Token' value={1} /></form>
            //      <script type='text/javascript'>
            //         document.getElementById('test').submit();
            //      </script> ",
            //            "http://hediyeceki.tutkal.com.tr/AnadoluSigorta/User/Index/", kullanici.ServiceKey));

            Response.Write(
                    string.Format(
                        @"<form action='{0}' id='test' method='POST'><input type='hidden' name='Token' value={1} /></form>
                  <script type='text/javascript'>
                     document.getElementById('test').submit();
                  </script> ",
                        "https://hediyeceki.tutkal.com.tr/Motul/User/Index/", kullanici.ServiceKey));
            Response.End();

        }
        else
        {
            Session["sonUrlV1"] = Request.Url.ToString();
            Response.Redirect("/login.aspx");
        }
    }

}