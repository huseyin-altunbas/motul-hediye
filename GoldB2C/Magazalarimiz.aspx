﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="Magazalarimiz.aspx.cs" Inherits="Magazalarimiz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/magazalarimiz.css?v=1.2" rel="stylesheet" type="text/css" />
    <div class="rootercnav">
        <div class="rcframe">
            <div style="width: 100%;" class="rootername">
                <a href="https://motulteam.com/">Anasayfa</a> / Mağaza Arama
            </div>
        </div>
    </div>
    <div class="stores">
        <div class="storesContainer">
            <!-- Left Container Start -->
            <div class="storesLeftContainer">
                <h1>
                    Lafarge Alçı Kart Mağaza Arama</h1>
                <p>
                    Adres ve ulaşım bilgilerini görmek istediğiniz şubemiz için;</p>
                <div class="storeAddressForm">
                    <asp:DropDownList ID="dpSehirler" runat="server" onchange="MagazaListesi(this.value);">
                    </asp:DropDownList>
                    <select name="" multiple="multiple" id="lstMagazaListe" class="storeNames" onchange="MagazaBilgileri(this);">
                    </select>
                </div>
            </div>
            <!-- Left Container End -->
            <!-- Right Container Start -->
            <div id="MagazalarAna" class="storesRightContainer">
                <div id="LoadingMagazaAna" class="storesLoading" style="display: none">
                </div>
                <div class="storesBanner">
                </div>
            </div>
            <!-- Right Container End -->
            <!-- Right Container Start -->
            <div id="MagazalarDetay" class="storesRightContainer" style="display: none;">
                <div id="LoadingMagazaDetay" class="storesLoading" style="display: none;">
                </div>
                <div class="storesDetail" style="height: 344px">
                    <h2 id="magazaBaslik">
                    </h2>
                    <dl>
                        <dt><span class="storeFormTitle">Adres</span> <span class="storeDotted">:</span></dt>
                        <dd id="magazaAdres">
                        </dd>
                        <dt><span class="storeFormTitle">İlçe - İl</span> <span class="storeDotted">:</span></dt>
                        <dd id="magazaIlceIl">
                        </dd>
                        <dt><span class="storeFormTitle">Telefon</span> <span class="storeDotted">:</span></dt>
                        <dd id="magazaTelefon">
                        </dd>
                        <dt><span class="storeFormTitle">Fax</span> <span class="storeDotted">:</span></dt>
                        <dd id="magazaFax">
                        </dd>
                        <dt><span class="storeFormTitle">Çalışma</span> <span class="storeDotted">:</span>
                        </dt>
                        <dd id="magazaAcilisKapanis">
                        </dd>
                    </dl>
                    <%-- <div class="storesSmallMaps">
                    </div>--%>
                    <div class="storesClear">
                    </div>
                    <!-- Photo Gallery Start -->
                </div>
            </div>
            <!-- Right Container End -->
        </div>
        <div class="storesContainer">
            <a href="/MagazaKampanya.aspx" title="Mağazalardaki kampanyalarımız">
                <img src="/ImagesGld/magaza_kampanya.gif" alt="Mağazalarımızdaki kampanyalara görmek için tıklayınız!..." />
            </a>
            
            <center>
                <a href="/BankaKampanya.aspx" title="Avantajlı Ödeme Koşulları"><img alt="Bonus'a Özel! Şimdi al Haziran'da 12 Taksitle Öde" src="ImagesGld/sepet/bonusHaziran.png"></a>
            </center>

        </div>
    </div>
    <script type="text/javascript">

        function MagazaBilgileri(_MagazaAd) {
            var MagazaAd = $(_MagazaAd).val();
            if (MagazaAd.length > 1)
                MagazaAd = MagazaAd[0];

            var SehirAd = $("#<%=dpSehirler.ClientID %>").val();

            if (MagazaAd != null && MagazaAd != "" && SehirAd != "0") {
                $.ajax({
                    type: 'POST',
                    url: '/Magazalarimiz.aspx/MagazaBilgileri',
                    data: '{ "SehirAd":"' + SehirAd + '", "MagazaAd":"' + MagazaAd + '" }',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {

                        $('#magazaBaslik').text(result.d.MagazaAd + ' - ' + result.d.MagazaIlce + ' / ' + result.d.MagazaSehir);
                        $('#magazaAdres').text(result.d.MagazaAdres1 + ' ' + result.d.MagazaAdres2 + ' ' + result.d.MagazaAdres3);
                        $('#magazaIlceIl').text(result.d.MagazaIlce + ' - ' + result.d.MagazaSehir);
                        $('#magazaTelefon').text(result.d.MagazaTelefon);
                        $('#magazaFax').text(result.d.MagazaFax);
                        $('#magazaAcilisKapanis').text(result.d.MagazaAcilisKapanisSaat);

                        if ($('#MagazalarAna').is(":visible")) {
                            $('#MagazalarAna').hide();
                            $('#MagazalarDetay').show();
                            $('#LoadingMagazalarAna').hide();
                        }
                        else if ($('#MagazalarDetay').is(":visible")) {

                            $('#LoadingMagazaDetay').hide();
                        }
                    },
                    error: function () {
                        alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                    },
                    beforeSend: function () {
                        if ($('#MagazalarAna').is(":visible"))
                            $('#LoadingMagazaAna').show();
                        else if ($('#MagazalarDetay').is(":visible"))
                            $('#LoadingMagazaDetay').show();

                    }
                });
            }
        }

        function MagazaListesi(SehirAd) {
            $.ajax({
                type: 'POST',
                url: '/Magazalarimiz.aspx/MagazaListesi',
                data: '{ "SehirAd":"' + SehirAd + '" }',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    $("#lstMagazaListe").find('option').remove();
                    $.each(result.d, function (index, data) {
                        $("#lstMagazaListe").append(
                      $('<option value="' + result.d[index] + '" ></option>').html(result.d[index]));
                    });

                },
                error: function () {
                    alert('Talep esnasında sorun oluştu. Yeniden deneyin');
                }
            });
        }

  
       

    </script>
</asp:Content>
