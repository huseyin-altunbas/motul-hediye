﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using Goldb2cEntity;
using System.Web.Services;
using System.Text;
using Goldb2cInfrastructure;
using System.Net.Http;
using System.Net;
//using Biges;

public partial class Sepet : System.Web.UI.Page
{
    SepetIslemleriBLL bllSepetIslemleri = new SepetIslemleriBLL();
    SiparisIslemleriBLL bllSiparisIslemleri = new SiparisIslemleriBLL();
    Kullanici kullanici;
    int SepetMasterId;

    decimal toplampuan = 0;
    decimal toplamavanspuan = 0;
    decimal toplamsiparispuan = 0;
    decimal toplamkalanpuan = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        Helper.PuanSorgula();

        if (Session["Kullanici"] != null)
        {
            if (!IsPostBack)
            {
                SepetBilgileriniGetir();
            }
        }
        else
        {
            UyelikIslemleriBLL.MusteriGirisineYonlendir(Request.Url.PathAndQuery);
        }

    }

    private void SepetBilgileriniGetir()
    {
        if (Session["Kullanici"] != null)
            kullanici = (Kullanici)Session["Kullanici"];

        DataSet dsSepetIcerik = new DataSet();
        dsSepetIcerik = bllSepetIslemleri.SepetGoruntule(kullanici.ErpCariId);

        DataTable dtSepetMaster = new DataTable();
        dtSepetMaster = dsSepetIcerik.Tables["SepetMasterTT"];

        using (var client = new HttpClient())
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var result = client.GetAsync("https://havuz-yeni.tutkal.com.tr/altin.aspx").Result;
        }

      


        if (dtSepetMaster.Rows.Count > 0)
        {
            SepetMasterId = Convert.ToInt32(dtSepetMaster.Rows[0]["sepet_masterid"]);
            Session["SepetMasterId"] = SepetMasterId;
            DataTable dtSepetDetay = new DataTable();
            dtSepetDetay = dsSepetIcerik.Tables["SepetDetayTT"];

            int rowsay = 0;
            foreach (DataRow row in dtSepetDetay.Rows)
            {
                SepetiGuncelle(Convert.ToInt32(row["urun_id"]), Convert.ToInt32(row["satis_adet"]));
                if (dtSepetDetay.Rows[rowsay]["resim_ad"].ToString().IndexOf("http") == -1)
                {
                    dtSepetDetay.Rows[rowsay]["resim_ad"] = "http://cdn.gold.com.tr/urunresim/kucukresim/" + dtSepetDetay.Rows[rowsay]["resim_ad"];
                }
                rowsay = rowsay + 1;
            }

            //lblToplam.Text = dtSepetMaster.Rows[0]["liste_tutar"].ToString() + " PUAN";
            //lblIndirimTutar.Text = dtSepetMaster.Rows[0]["indirim_tutar"].ToString() + " PUAN";
            //lblKdvTutar.Text = dtSepetMaster.Rows[0]["kdv_tutar"].ToString() + " PUAN";
            lblGenelToplam.Text = Convert.ToDecimal(dtSepetMaster.Rows[0]["genel_tutar"]).ToString("###,###,###") + " PUAN";

            //if (Convert.ToDecimal(dtSepetMaster.Rows[0]["genel_tutar"]) > Convert.ToInt32(Session["totalGoldPoint"]))
            //{
            //    lblFarkAvansPuan.Text = (Convert.ToDecimal(dtSepetMaster.Rows[0]["genel_tutar"]) - Convert.ToInt32(Session["totalGoldPoint"])).ToString("###,###,###");
            //    //aradiv.Visible = true;
            //}
            //else
            //{
            //    lblFarkAvansPuan.Text = "0";
            //}

            dlSepet.DataSource = dtSepetDetay;
            dlSepet.DataBind();
        }
        else
        {
            //lblToplam.Text = "0 PUAN";
            //lblIndirimTutar.Text = "0 PUAN";
            //lblKdvTutar.Text = "0 PUAN";
            lblGenelToplam.Text = "0 PUAN";
        }
    }

    [WebMethod]
    public static SepetToplamlar SepetToplamlarHesapla()
    {
        SepetIslemleriBLL bllSepetIslemleri = new SepetIslemleriBLL();
        SepetToplamlar sepetToplamlar = new SepetToplamlar();
        DataSet dsSepetIcerik = new DataSet();

        if (HttpContext.Current.Session["Kullanici"] != null)
        {
            DataTable dtSepetMaster = new DataTable();
            Kullanici kullanici = new Kullanici();
            kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];
            dsSepetIcerik = bllSepetIslemleri.SepetGoruntule(kullanici.ErpCariId);
            dtSepetMaster = dsSepetIcerik.Tables["SepetMasterTT"];

            if (dtSepetMaster != null && dtSepetMaster.Rows.Count > 0)
            {
                sepetToplamlar.ListeTutar = Convert.ToDecimal(dtSepetMaster.Rows[0]["liste_tutar"]);
                sepetToplamlar.IndirimTutar = Convert.ToDecimal(dtSepetMaster.Rows[0]["indirim_tutar"]);
                sepetToplamlar.KdvTutar = Convert.ToDecimal(dtSepetMaster.Rows[0]["kdv_tutar"]);
                sepetToplamlar.GenelTutar = Convert.ToDecimal(dtSepetMaster.Rows[0]["genel_tutar"]);
            }
        }

        return sepetToplamlar;
    }

    [WebMethod]
    public static string AvansPuanHesapla(string GenelToplam)
    {
        double _genelTutar = Convert.ToDouble(GenelToplam.Replace(",", "").Replace(",", "").Replace(" PUAN", ""));
        string result = "0";

        if (_genelTutar > Convert.ToDouble(HttpContext.Current.Session["totalGoldPoint"]))
        {
            if (Convert.ToDouble(HttpContext.Current.Session["totalGoldPoint"]) <= 0)
                result = "0"; //(Convert.ToDouble(HttpContext.Current.Session["totalGoldPoint"]) - _genelTutar).ToString("###,###,###");
            else

                result = (_genelTutar - Convert.ToDouble(HttpContext.Current.Session["totalGoldPoint"])).ToString("###,###,###");
        }
        return result;
    }

    [WebMethod]
    public static void SepetiGuncelle(int UrunId, int UrunAdet)
    {
        Kullanici kullanici = new Kullanici();
        if (HttpContext.Current.Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];

            SepetIslemleriBLL bllSepetIslemleri = new SepetIslemleriBLL();
            bllSepetIslemleri.SepetOlustur(kullanici.ErpCariId, UrunId, "", "", 0, 0, UrunAdet, 2);
        }
    }

    [WebMethod]
    public static string KazanciGuncelle(string txtControlId, int Adet, int UrunId)
    {
        string KazancinizLabel = "";
        string kazanciniz = "";
        string lblControlId = txtControlId.Replace("txtUrunAdet", "lblKazanciniz");
        DataSet dsSepetIcerik = new DataSet();

        SepetIslemleriBLL bllSepetIslemleri = new SepetIslemleriBLL();
        Kullanici kullanici = new Kullanici();
        if (HttpContext.Current.Session["Kullanici"] != null)
            kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];

        DataTable dtSepetMaster = new DataTable();
        DataTable dtSepetDetay = new DataTable();
        dsSepetIcerik = bllSepetIslemleri.SepetGoruntule(kullanici.ErpCariId);
        dtSepetMaster = dsSepetIcerik.Tables["SepetMasterTT"];
        if (dtSepetMaster.Rows.Count > 0)
        {
            dtSepetDetay = dsSepetIcerik.Tables["SepetDetayTT"];
            DataRow[] drSepetDetay = dtSepetDetay.Select("urun_id = '" + UrunId + "'");
            decimal listeTutar = 0;
            decimal listeNetTutar = 0;

            if (drSepetDetay.Length > 0)
            {
                listeTutar = Convert.ToDecimal(drSepetDetay[0]["liste_tutar"]);
                listeNetTutar = Convert.ToDecimal(drSepetDetay[0]["liste_nettutar"]);
            }

            if (listeTutar != listeNetTutar)
            {
                kazanciniz = (listeTutar - listeNetTutar).ToString();
                KazancinizLabel = lblControlId;
            }
        }

        return KazancinizLabel + "||" + kazanciniz;
    }

    protected void dlSepet_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //int UrunId = Convert.ToInt32(dlSepet.DataKeys[e.Item.ItemIndex]);
            int UrunId = Convert.ToInt32(((DataRowView)(e.Item.DataItem)).Row["urun_id"]);
            if (Convert.ToInt32(((DataRowView)(e.Item.DataItem)).Row["anaurun_detayno"]) > 0)
                ((TextBox)e.Item.FindControl("txtUrunAdet")).Enabled = false;
            else
                ((TextBox)e.Item.FindControl("txtUrunAdet")).Attributes.Add("onblur", "SepetiGuncelle(" + UrunId.ToString() + ", this);");

            int UrunAdet = Convert.ToInt32(((System.Data.DataRowView)(e.Item.DataItem)).Row["satis_adet"]);
            decimal ListeTutar = Convert.ToDecimal(((System.Data.DataRowView)(e.Item.DataItem)).Row["liste_tutar"]);
            decimal ListeNettutar = Convert.ToDecimal(((System.Data.DataRowView)(e.Item.DataItem)).Row["liste_nettutar"]);

            Label lblListeTutar = ((Label)e.Item.FindControl("lblListeTutar"));
            Label lblListeNetTutar = (Label)e.Item.FindControl("lblListeNetTutar");
            Label lblKazanciniz = ((Label)e.Item.FindControl("lblKazanciniz"));

            if (ListeTutar == ListeNettutar)
            {
                //e.Item.FindControl("DivKazanciniz").Visible = false;
                //lblListeTutar.Text = "";
                //lblListeTutar.Visible = false;
                lblListeNetTutar.Text = ListeNettutar.ToString("###,###,###") + " PUAN";

            }
            else
            {
                decimal IndirimTutar = ListeTutar - ListeNettutar;
                //lblListeTutar.Text = ListeTutar.ToString() + " PUAN";
                lblListeNetTutar.Text = ListeNettutar.ToString("###,###,###") + " PUAN";
                //lblKazanciniz.Text = IndirimTutar.ToString() + " PUAN";
                //e.Item.FindControl("DivKazanciniz").Visible = true;
            }

        }
    }

    protected void btnGuncelle_Click(object sender, EventArgs e)
    {
        //Response.Redirect("/sepet.aspx");
        SepetBilgileriniGetir();


        //string a;
        //for (int i = 0; i < rptSepet.Items.Count; i++)
        //{
        //    CheckBox sec = (CheckBox)rptSepet.Items[i].FindControl("chkUrun");
        //    if (sec.Checked)
        //        a = sec.Attributes["UrunId"].ToString();
        //}

    }

    protected void btnSil_Click(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
            kullanici = (Kullanici)Session["Kullanici"];

        if (Session["SepetMasterId"] != null)
            SepetMasterId = Convert.ToInt32(Session["SepetMasterId"]);

        string sepetDetayId = "";

        //DataSet dsSepetIcerik = new DataSet();
        //dsSepetIcerik = bllSepetIslemleri.GetSepetIcerik(kullanici.ErpCariId);
        //DataTable dtSepetMaster = new DataTable();
        //dtSepetMaster = dsSepetIcerik.Tables["SepetMasterTT"];
        //if (dtSepetMaster.Rows.Count > 0)
        //    SepetMasterId = Convert.ToInt32(dtSepetMaster.Rows[0]["sepet_masterid"]);

        for (int i = 0; i < dlSepet.Items.Count; i++)
        {
            CheckBox sec = (CheckBox)dlSepet.Items[i].FindControl("chkUrun");
            if (sec.Checked)
                sepetDetayId = sepetDetayId + sec.Attributes["SepetDetayId"].ToString() + ",";
        }

        sepetDetayId = sepetDetayId.TrimEnd(',');
        bllSepetIslemleri.UrunSil(SepetMasterId, sepetDetayId);
        SepetBilgileriniGetir();

    }

    protected void btnSatinAl_Click(object sender, EventArgs e)
    {
         

        string SiparisMasterId;

        if (Session["SepetMasterId"] != null && Convert.ToInt32(Session["SepetMasterId"]) > 0 && Session["Kullanici"] != null)
        {
            if (dlSepet.Items.Count > 0)
            {
                Helper.PuanSorgula();

                SepetIslemleriBLL bllSepetIslemleri = new SepetIslemleriBLL();
                SepetToplamlar sepetToplamlar = new SepetToplamlar();
                DataSet dsSepetIcerik = new DataSet();
                DataTable dtSepetMaster = new DataTable();
                DataTable dtSepetDetay = new DataTable();
                Kullanici kullanici = new Kullanici();
                kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];
                dsSepetIcerik = bllSepetIslemleri.SepetGoruntule(kullanici.ErpCariId);
                dtSepetMaster = dsSepetIcerik.Tables["SepetMasterTT"];
                dtSepetDetay = dsSepetIcerik.Tables["SepetDetayTT"];

                int teminSuresiGecSayisi = 0;
                teminSuresiGecSayisi = TeminSuresiGecKontrol(dtSepetDetay);
                if (dtSepetDetay.Rows.Count == teminSuresiGecSayisi || dtSepetDetay.Rows.Count == dtSepetDetay.Rows.Count - teminSuresiGecSayisi)
                {
                    //if (Convert.ToInt64(Session["totalGoldPoint"]) > Convert.ToInt64(dtSepetMaster.Rows[0]["genel_tutar"]))
                    //{
                    //    SiparisMasterId = bllSiparisIslemleri.SiparisKaydet(Convert.ToInt32(Session["SepetMasterId"]), kullanici.ErpCariId);
                    //    if (SiparisMasterId > 0)
                    //    {
                    //        Session["SiparisMasterId"] = SiparisMasterId;
                    //        if (kullanici.Email == "ilkay.saglam@sap.com" || kullanici.Email == "turktelekom@turktelekom.com.tr")
                    //            Response.Redirect("~/Oci.aspx", false);
                    //        else
                    //            Response.Redirect("~/AdresBilgileri.aspx", false);
                    //    }
                    //}
                    //else
                    //{

                    if (Convert.ToInt64(Session["totalGoldPoint"]) <= 0)
                    {
                        ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Puanınız Bulunmamaktadır.')</script>");
                    }
                    else if (Convert.ToInt64(Session["totalGoldPoint"]) < Convert.ToInt64(dtSepetMaster.Rows[0]["genel_tutar"]))
                    {
                        ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Puanınız Sepetinizdeki ürünleri almaya yeterli değildir.')</script>");
                    }
                    else if (Convert.ToInt64(Session["totalGoldPoint"]) >= Convert.ToInt64(dtSepetMaster.Rows[0]["genel_tutar"]))
                    {
                        SiparisMasterId = bllSiparisIslemleri.SiparisKaydet(Convert.ToInt32(Session["SepetMasterId"]), kullanici.ErpCariId);
                        if (SiparisMasterId.Length > 5) //Burası çok önemli
                        {

                            Session["SiparisTarihi"] = DateTime.Now;
                            Session["SiparisMasterId"] = SiparisMasterId;
                            Response.Redirect("~/AdresBilgileri.aspx", false);
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Puanınız Sepetinizdeki ürünleri almaya yeterli değildir.')</script>");
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Vestel/Beko marka ürünlerin temin süresi 15 iş günü olduğu için tek sipariş geçilmelidir. Farklı marka ürünlerle birlikte sipariş geçilmemesi gerekmektedir.')</script>");
                }
            }
        }
    }

  

    protected void btnAlisveriseDevam_Click(object sender, EventArgs e)
    {
        if (Session["SonGezilenKategori"] != null && !string.IsNullOrEmpty(Session["SonGezilenKategori"].ToString()))
        {
            Response.Redirect(Session["SonGezilenKategori"].ToString());
        }
        else
            Response.Redirect("/Default.aspx");
    }

    private int TeminSuresiGecKontrol(DataTable dt)
    {
        return 0;
        List<int> lstExVestelUrunKod = new List<int> { 20008447, 20008448, 20008509, 20008510, 20008686, 20008511 };
        List<int> lstExBekoUrunKod = new List<int> { 20006796, 20006766, 20006767, 20006803, 20006757 };
        List<int> lstUrunKod = dt.AsEnumerable().Select(u => u.Field<int>("urun_id")).ToList();

        int sepetTeminSuresiGecSayisi = 0;

        foreach (DataRow dr in dt.Rows)
        {
            if (dr["urun_ad"] != null && dr["urun_id"] != null)
            {
                if (dr["urun_ad"].ToString().ToLower().Contains("vestel") && !(lstExVestelUrunKod.Where(l => l == Convert.ToInt32(dr["urun_id"])).Any()))
                    sepetTeminSuresiGecSayisi++;

                if (dr["urun_ad"].ToString().ToLower().Contains("beko") && !(lstExBekoUrunKod.Where(l => l == Convert.ToInt32(dr["urun_id"])).Any()))
                    sepetTeminSuresiGecSayisi++;
            }
        }

        return sepetTeminSuresiGecSayisi;
    }

    [WebMethod]
    public static SepetBilgileri GetSepetBilgileri()
    {
        SepetIslemleriBLL bllSepetIslemleri = new SepetIslemleriBLL();
        SepetBilgileri sepetBilgileri = new SepetBilgileri();
        Kullanici kullanici;
        if (HttpContext.Current.Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];

            DataSet dsSepetIcerik = new DataSet();
            dsSepetIcerik = bllSepetIslemleri.SepetGoruntule(kullanici.ErpCariId);

            DataTable dtSepetMaster = new DataTable();
            dtSepetMaster = dsSepetIcerik.Tables["SepetMasterTT"];

            if (dtSepetMaster.Rows.Count == 1)
            {
                DataTable dtSepetDetay = new DataTable();
                dtSepetDetay = dsSepetIcerik.Tables["SepetDetayTT"];

                SepetMaster sepetMaster = new SepetMaster();
                sepetMaster.SepetMasterId = Convert.ToInt32(dtSepetMaster.Rows[0]["sepet_masterid"]);
                sepetMaster.ListeTutar = Convert.ToDecimal(dtSepetMaster.Rows[0]["liste_tutar"]);
                sepetMaster.IndirimTutar = Convert.ToDecimal(dtSepetMaster.Rows[0]["indirim_tutar"]);
                sepetMaster.KdvTutar = Convert.ToDecimal(dtSepetMaster.Rows[0]["kdv_tutar"]);
                sepetMaster.GenelTutar = Convert.ToDecimal(dtSepetMaster.Rows[0]["genel_tutar"]);
                sepetMaster.UrunSayisi = dtSepetDetay.Rows.Count;

                List<SepetDetay> sepetDetayList = new List<SepetDetay>();
                SepetDetay sepetDetay;
                foreach (DataRow dr in dtSepetDetay.Rows)
                {
                    sepetDetay = new SepetDetay();
                    sepetDetay.SepetDetayId = Convert.ToInt32(dr["sepet_detayid"]);
                    sepetDetay.SepetMasterId = Convert.ToInt32(dr["sepet_masterid"]);
                    sepetDetay.UrunId = dr["urun_id"].ToString();
                    sepetDetay.UrunAd = dr["urun_ad"].ToString();
                    sepetDetay.UrunAdet = Convert.ToInt32(dr["satis_adet"]);
                    sepetDetay.UrunLink = dr["urun_link"].ToString();
                    sepetDetay.ListeTutar = Convert.ToDecimal(dr["liste_tutar"]);
                    sepetDetay.ListeNetTutar = Convert.ToDecimal(dr["liste_nettutar"]);
                    if (dr["resim_ad"].ToString().IndexOf("http") == -1)
                    {
                        dr["resim_ad"] = "http://cdn.gold.com.tr/urunresim/buyukresim/" + dr["resim_ad"];
                    }
                    sepetDetay.ResimAd = dr["resim_ad"].ToString();

                    sepetDetayList.Add(sepetDetay);
                }

                sepetBilgileri = new SepetBilgileri { SepetDetayList = sepetDetayList, SepetMaster = sepetMaster };
            }
        }
        return sepetBilgileri;
    }

    [WebMethod]
    public static string SepetSil(int SepetMasterId, int SepetDetayId)
    {
        SepetIslemleriBLL bllSepetIslemleri = new SepetIslemleriBLL();
        if (HttpContext.Current.Session["Kullanici"] != null)
        {
            Kullanici kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];

            DataSet dsSepetIcerik = new DataSet();
            dsSepetIcerik = bllSepetIslemleri.SepetGoruntule(kullanici.ErpCariId);

            DataTable dtSepetMaster = new DataTable();
            dtSepetMaster = dsSepetIcerik.Tables["SepetMasterTT"];

            if (dtSepetMaster.Rows.Count == 1)
            {
                DataTable dtSepetDetay = new DataTable();
                dtSepetDetay = dsSepetIcerik.Tables["SepetDetayTT"];

                bool isKullaniciSepet = dtSepetDetay.AsEnumerable().Any(k => k.Field<int>("sepet_masterid") == SepetMasterId && k.Field<int>("sepet_detayid") == SepetDetayId);

                if (isKullaniciSepet)
                {
                    bllSepetIslemleri.UrunSil(SepetMasterId, SepetDetayId.ToString());
                    return "1";
                }
            }
        }
        return "0";
    }

    [WebMethod]
    public static string SepetEkle(int UrunId)
    {
         

        UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
        SepetIslemleriBLL bllSepetIslemleri = new SepetIslemleriBLL();
        if (HttpContext.Current.Session["Kullanici"] != null)
        {
            Kullanici kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];

            if (kullanici.ErpCariId < 1)
            {
                kullanici = bllUyelikIslemleri.CariVeriOlustur(kullanici);
            }

            bool result = Helper.SepetteAkaryakitUrunuVarmi(kullanici.ErpCariId, UrunId);
            if (result)
            {
                //ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Sepetinizde bir adet akaryakıt ürünü bulunmaktadır.')</script>");
                return "1";
            }

            if (UrunId > 0 && kullanici.ErpCariId > 0)
            {
                bllSepetIslemleri.SepetOlustur(kullanici.ErpCariId, UrunId, string.Empty, string.Empty, 0, 0, 1, 1);
                return "1";
            }
            return "-1";
        }
        else
            return "0";
    }
}