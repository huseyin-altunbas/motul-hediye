﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="OdemeSecenekleri.aspx.cs" Inherits="OdemeSecenekleri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/goldBasket.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/css/jqtransform.css" type="text/css">
    <script type="text/javascript" src="/js/jquery.jqtransform.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).jqTransform({ imgPath: 'imagesgld/' });
        });
    </script>

    <!-- basket container -->
    <div class="containerOut982">
        <div class="containerIn960">
            <!-- basket root -->
            <div class="basketroot">
                <!-- basket navigation -->
                <div class="basketnav">
                    <ul>
                        <li class="menu1"><a class="amenu1 cursordef" href="#">Sepetim</a> </li>
                        <li class="menu2"><a class="amenu2 cursordef" href="#">Teslimat
                            <br />
                            Fatura</a> </li>
                        <li class="menu3"><a class="amenu3 cursordef" href="#">Kargo</a> </li>
                        <li class="menu4 menu4selected real"><a href="#" class="amenu4 selected cursordef">Ödeme
                            <br />
                            Seçenekleri</a></li>
                        <li class="menu5"><a class="amenu5 cursordef" href="#">Ödeme
                            <br />
                            Onay</a> </li>
                    </ul>
                </div>
                <!-- basket navigation -->
                <!-- payment options content -->
                <div class="paymentoptionscontainer">
                    <h6>
                        Ödeme Seçenekleri</h6>
                    <!-- payment options root -->
                    <div class="paymentoptionsroot">
                        <h5>
                            Ödeme Türü Seçiniz</h5>
                        <!-- payment price -->
                        <div class="paymentprice">
                            <div class="price1">
                                Sipariş Tutarı: <span class="colord8">
                                    <asp:Literal ID="ltrSiparisTutar" runat="server"></asp:Literal>&nbsp;TL</span></div>
                            <div class="price2">
                                Ödenen Tutar: <span class="colord8">0<asp:Literal ID="ltrOdenenTutar" runat="server"></asp:Literal>&nbsp;TL</span></div>
                            <div class="price3">
                                Kalan Tutar: <span class="colord8">
                                    <asp:Literal ID="ltrKalanTutar" runat="server"></asp:Literal>&nbsp;TL</span></div>
                        </div>
                        <!-- payment price -->
                        <!-- payment chooes -->
                        <div class="paymentchooes">
                            <!-- chooes -->
                            <div class="chooes1">
                                <div class="options">
                                    <input type="radio" name="radio" id="radio2" value="radio" onclick="javascript:location.href='/KrediKarti.aspx';" />
                                    Kredi Kartı</div>
                                <div class="detail">
                                    <img src="imagesgld/logo_sifreliodeme.jpg" width="154" height="50" alt="Şifreli Ödeme" />
                                    <br />
                                    <br />
                                    İnternet üzerinden kredi kartı ve banka kartıyla yapılan alışveriş işlemlerinin
                                    güvenliğinin artırılması için geliştirilmiş bir sistemdir. Visa ve MasterCard'ın
                                    geliştirdiği bu "güvenli sanal alışveriş" çözümleri ile hem kart sahipleri hem de
                                    üye isyerleri sahtekarlıklara karsı güvence altına alınmıstır. Verified by Visa
                                    ve MasterCard SecureCode logoları bulunan sanal işyerlerinden yapılan online alişverişler
                                    Visa ve MasterCard güvencesi altındadır.
                                </div>
                                <div class="button">
                                    <img src="imagesgld/arrow_filter.gif" width="5" height="8" />
                                    Ayrıntılı bilgi almak için <a href="#">tıklayın.</a></div>
                            </div>
                            <!-- chooes -->
                            <!-- chooes -->
                            <%--          <div class="chooes2">
                                <div class="options">
                                    <input type="radio" name="radio" id="radio2" value="radio" />
                                    BonusPay</div>
                                <div class="detail">
                                    BonusPay ile ödeme
                                </div>
                                <div class="button">
                                    <img src="imagesgld/arrow_filter.gif" width="5" height="8" />
                                    BonusPay <a href="#">nasıl</a> kullanılır?</div>
                            </div>--%>
                            <!-- chooes -->
                            <!-- chooes -->
                            <div class="chooes3">
                                <div class="options">
                                    <input type="radio" name="radio" id="radio2" value="radio" onclick="javascript:location.href='/Havale.aspx';" />
                                    Havale</div>
                                <div class="detail">
                                    Havale ile ödeme
                                </div>
                                <div class="button">
                                    &nbsp;</div>
                            </div>
                            <!-- chooes -->
                            <!-- chooes -->
                            <div class="chooes4">
                                <div class="options">
                                    <input type="radio" name="radio" id="radio2" value="radio" onclick="javascript:location.href='/PostaCeki.aspx';" />
                                    Posta Çeki</div>
                                <div class="detail">
                                    Posta Çeki ile ödeme
                                </div>
                                <div class="button">
                                    &nbsp;</div>
                            </div>
                            <!-- chooes -->
                            <!-- chooes -->
                            <div class="chooes5">
                                <div class="options">
                                    <input type="radio" name="radio" id="radio2" value="radio" onclick="javascript:location.href='/Puan.aspx';" />
                                    Puan</div>
                                <div class="title1">
                                    İade Puan</div>
                                <div class="detail">
                                    İade etmiş olduğunuz ürünlerin tutarlarını isterseniz iade puan olarak hesabınıza
                                    yükletebilir iade çeki mantıgıyla yeni alışverişlerinizde kullanabilirsiniz.
                                </div>
                                <div class="title2">
                                    Hediye Puan</div>
                                <div class="detail">
                                    Hediye puan Siz üyelerimize Hizlial.com’un sunmus oldugu, daha ekonomik alışveriş
                                    yapmanızı saglayacak indirim puanıdır. Belli dönemlerde hesabınıza yüklenmiş olan
                                    Hediye Puanlarınızla ödemenizi gerçekleştirebilirsiniz.
                                </div>
                                <div class="title3">
                                    Gold Puan</div>
                                <div class="detail">
                                    Gold puan sitemizden yapmış olduğunuz alışverişlerinizde her aldığınız üründen kazandığınız
                                    puandır.
                                </div>
                            </div>
                            <!-- chooes -->
                        </div>
                        <!-- payment chooes -->
                    </div>
                    <!-- payment options root -->
                    <!-- form buttons -->
                    <div class="formbuttons">
                        <div class="buttonleft">
                            <a class="link-button-RedBtn spriteRedBtn" href="#"><span class="spriteRedBtn fontTre fontSz14">
                                ◄ Geri</span> </a>
                        </div>
                    </div>
                    <!-- form buttons -->
                </div>
                <!-- payment options content -->
            </div>
            <!-- basket root -->
        </div>
    </div>
    <!-- basket container -->
</asp:Content>
