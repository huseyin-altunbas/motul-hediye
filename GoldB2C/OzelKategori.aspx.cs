﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Goldb2cBLL;
using System.Text;

public partial class OzelKategori : System.Web.UI.Page
{
    OzelKategoriBLL bllOzelKategori = new OzelKategoriBLL();
    MarketingScriptHelper marketingScriptHelper = new MarketingScriptHelper();
    protected void Page_Load(object sender, EventArgs e)
    {
        DivOKBanner.Visible = false;

        DataTable dtOzelKategoriMaster = new DataTable();
        DataTable dtOzelKategoriDetay = new DataTable();
        int OzelKategoriMasterId = 0;
        int OzelKategoriDetayId = 0;
        if (Request.QueryString["KategoriId"] != null && int.TryParse(Request.QueryString["KategoriId"], out OzelKategoriMasterId))
        {
            if (Request.QueryString["KategoriDetayId"] != null && int.TryParse(Request.QueryString["KategoriDetayId"], out OzelKategoriDetayId))
            {
                dtOzelKategoriMaster = bllOzelKategori.OzelKategoriMaster(OzelKategoriMasterId);
                dtOzelKategoriDetay = bllOzelKategori.OzelKategoriDetay(OzelKategoriMasterId, OzelKategoriDetayId);

                if (dtOzelKategoriMaster != null && dtOzelKategoriMaster.Rows.Count > 0)
                {
                    ltrOzelKategoriBaslik.Text = dtOzelKategoriMaster.Rows[0]["kategori_ad"].ToString();



                    OzelKategoriMenuV2_1.MenuOlustur(dtOzelKategoriMaster);
                }
                rptUrunler.DataSource = dtOzelKategoriDetay;
                rptUrunler.DataBind();
            }
            else
            {

                Random rnd = new Random();

                dtOzelKategoriMaster = bllOzelKategori.OzelKategoriMaster(OzelKategoriMasterId);


                if (dtOzelKategoriMaster != null && dtOzelKategoriMaster.Rows.Count > 0)
                    ltrOzelKategoriBaslik.Text = dtOzelKategoriMaster.Rows[0]["kategori_ad"].ToString();

                OzelKategoriMenuV2_1.MenuOlustur(dtOzelKategoriMaster);


                if (dtOzelKategoriMaster != null && dtOzelKategoriMaster.Rows.Count > 0)
                    OzelKategoriDetayId = Convert.ToInt32(dtOzelKategoriMaster.Rows[rnd.Next(0, dtOzelKategoriMaster.Rows.Count)]["detay_id"]);

                dtOzelKategoriDetay = bllOzelKategori.OzelKategoriDetay(OzelKategoriMasterId, OzelKategoriDetayId);
                rptUrunler.DataSource = dtOzelKategoriDetay;
                rptUrunler.DataBind();
            }

            if (dtOzelKategoriMaster != null && dtOzelKategoriMaster.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(dtOzelKategoriMaster.Rows[0]["kategori_keywords"].ToString()))
                    Page.MetaKeywords = dtOzelKategoriMaster.Rows[0]["kategori_keywords"].ToString();

                if (!string.IsNullOrEmpty(dtOzelKategoriMaster.Rows[0]["kategori_title"].ToString()))
                    Page.Title = dtOzelKategoriMaster.Rows[0]["kategori_title"].ToString() + " - motulteam.com";

                if (!string.IsNullOrEmpty(dtOzelKategoriMaster.Rows[0]["kategori_description"].ToString()))
                    Page.MetaDescription = dtOzelKategoriMaster.Rows[0]["kategori_description"].ToString();

                if (!string.IsNullOrEmpty(dtOzelKategoriMaster.Rows[0]["kategori_banner"].ToString()))
                {
                    DivOKBanner.Visible = true;

                    ltrBanner.Text = dtOzelKategoriMaster.Rows[0]["kategori_banner"].ToString();

                }

            }



        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }

    }

    protected void rptUrunler_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //Ürün Fiyatları
        DataRowView drv = e.Item.DataItem as DataRowView;
        Label lblBrutTutar = (Label)e.Item.FindControl("lblBrutTutar");
        Label lblNetTutar = (Label)e.Item.FindControl("lblNetTutar");
        Label lblBilgi = (Label)e.Item.FindControl("lblBilgi");

        if (Convert.ToBoolean(drv.Row["temin_durum"]))
        {
            decimal brut_tutar = Math.Round(Convert.ToDecimal(drv.Row["brut_tutar"]), 2);
            decimal net_tutar = Math.Round(Convert.ToDecimal(drv.Row["net_tutar"]), 2);

            decimal onikiTaksit = Math.Round((Convert.ToDecimal(drv.Row["net_tutar"]) / 12), 2);

            //lblNetTutar.Text = onikiTaksit.ToString() + " PUAN";
            //lblBilgi.Text = "x 12 Taksit";

            lblBilgi.Text = "KDV Dahil";
            if (brut_tutar > net_tutar)
            {
                lblBrutTutar.Text = brut_tutar.ToString() + "PUAN";
                lblNetTutar.Text = net_tutar.ToString() + "PUAN";
            }
            else
            {
                lblBrutTutar.Visible = false;
                lblNetTutar.Text = brut_tutar.ToString() + "PUAN";
            }
        }
        else
        {
            lblBilgi.Text = "Ürün geçici olarak temin edilemiyor.";
        }

        ////İkonlar
        //if (Convert.ToBoolean(drv.Row["indirimli"]))
        //    ((Image)e.Item.FindControl("imgIndirimli")).ImageUrl = "~/imagesgld/icon_indirimliurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgIndirimli")).ImageUrl = "~/imagesgld/icon_indirimliurunOff.png";

        //if (Convert.ToBoolean(drv.Row["aynigun_sevk"]))
        //    ((Image)e.Item.FindControl("imgAyniGunSevk")).ImageUrl = "~/imagesgld/icon_hizlikargoOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgAyniGunSevk")).ImageUrl = "~/imagesgld/icon_hizlikargoOff.png";

        //if (Convert.ToBoolean(drv.Row["hediyeli_urun"]))
        //    ((Image)e.Item.FindControl("imgHediyeliUrun")).ImageUrl = "~/imagesgld/icon_hediyeliurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgHediyeliUrun")).ImageUrl = "~/imagesgld/icon_hediyeliurunOff.png";

        //if (Convert.ToBoolean(drv.Row["kuryeile_sevk"]))
        //    ((Image)e.Item.FindControl("imgKuryeileSevk")).ImageUrl = "~/imagesgld/icon_coksatanurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgKuryeileSevk")).ImageUrl = "~/imagesgld/icon_coksatanurunOff.png";

        //if (Convert.ToBoolean(drv.Row["kargo_bedava"]))
        //    ((Image)e.Item.FindControl("imgKargoBedava")).ImageUrl = "~/imagesgld/icon_kargobedavaOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgKargoBedava")).ImageUrl = "~/imagesgld/icon_kargobedavaOff.png";

        //if (Convert.ToBoolean(drv.Row["yeni_urun"]))
        //    ((Image)e.Item.FindControl("imgYeniUrun")).ImageUrl = "~/imagesgld/icon_yeniurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgYeniUrun")).ImageUrl = "~/imagesgld/icon_yeniurunOff.png";

    }
}