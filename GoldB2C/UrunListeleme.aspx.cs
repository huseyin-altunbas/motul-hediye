﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Goldb2cBLL;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using Goldb2cEntity;
using System.IO;
using Goldb2cInfrastructure;

public partial class UrunListeleme : System.Web.UI.Page
{
    QueryStringBuilder QueryStrBuilder = new QueryStringBuilder();
    UrunListelemeBLL bllUrunListeleme = new UrunListelemeBLL();
    UrunGrupIslemleriBLL bllUrunGrupIslemleri = new UrunGrupIslemleriBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    KarsilastirmaIslemleriBLL bllKarsilastirmaIslemleri = new KarsilastirmaIslemleriBLL();
    MarketingScriptHelper marketingScriptHelper = new MarketingScriptHelper();

    DataSet dsUrunListeleme;

    DataTable dtExtraOzellikTT;
    DataTable dtExtraOzellikGrup;
    DataTable dtExtraOzellikGrupSecim;
    DataTable dtUrunGrupKod;

    public static string OzellikSecimClass;
    OlapUrun bllOlapUrun = new OlapUrun();
    public static string ImageDomain = CustomConfiguration.ImageDomain;
    string SiralamaKriter = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["Kullanici"] == null)
        //    Response.Redirect(@"\MusteriHizmetleri\UyeGirisi.aspx");
        if (!string.IsNullOrEmpty(Request.QueryString["siralama"]))
            SiralamaKriter = Request.QueryString["siralama"].ToLower();

        if (dpSiralama.Items.FindByValue(SiralamaKriter) != null)
        {
            dpSiralama.ClearSelection();
            dpSiralama.Items.FindByValue(SiralamaKriter).Selected = true;
        }


        int[] FiyatAralik;
        int[] InpFiyatAralik = { 0, 0 };
        string inpExtraVeri = "";
        int UrunGrupId = 0;

        if (!string.IsNullOrEmpty(Request.QueryString["UrunGrupId"]))
        {
            if (Request.QueryString["urungrupid"].IndexOf(',') > -1)
                int.TryParse(Request.QueryString["urungrupid"].Split(',')[0], out UrunGrupId);
            else
                int.TryParse(Request.QueryString["UrunGrupId"], out UrunGrupId);
        }

        if (!IsPostBack)
        {
            int ToplamSayfa = 0;
            int ToplamUrun = 0;
            int SayfaNo = 1;
            int rowsay = 0;

            if (UrunGrupId > 0)
            {
                CokSatanUrunler(UrunGrupId);

                DataTable dtUrunGrupBilgisi = bllUrunGrupIslemleri.GetUrunGrupBilgisi(UrunGrupId);
                if (dtUrunGrupBilgisi != null && dtUrunGrupBilgisi.Rows.Count > 0)
                {
                    if (dtUrunGrupBilgisi.Rows[0]["urun_seviye"] != null && dtUrunGrupBilgisi.Rows[0]["urun_seviye"].ToString() != string.Empty)
                    {
                        string urunAd = dtUrunGrupBilgisi.Rows[0]["urun_ad"].ToString();
                        string urlRewrite = dtUrunGrupBilgisi.Rows[0]["url_rewrite"].ToString();
                        string urunSeviye = dtUrunGrupBilgisi.Rows[0]["urun_seviye"].ToString();

                        lblUrunGrupAd.Text = urunAd;

                        //Sub Category
                        if (urunSeviye == "1-Grup")
                        {
                            dtUrunGrupKod = bllUrunGrupIslemleri.GetUrunGrupKod(UrunGrupId.ToString(), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                            var vUrunGrupKod = dtUrunGrupKod.AsEnumerable().Where(k => k.Field<string>("urun_seviye") == "2-Grup");
                            dtUrunGrupKod = vUrunGrupKod.Count() > 0 ? vUrunGrupKod.CopyToDataTable() : dtUrunGrupKod.Clone();
                        }
                        else if (urunSeviye == "2-Grup")
                        {
                            dtUrunGrupKod = bllUrunGrupIslemleri.GetUrunGrupKod(string.Empty, UrunGrupId.ToString(), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                            var vUrunGrupKod = dtUrunGrupKod.AsEnumerable().Where(k => k.Field<string>("urun_seviye") == "3-Grup");
                            dtUrunGrupKod = vUrunGrupKod.Count() > 0 ? vUrunGrupKod.CopyToDataTable() : dtUrunGrupKod.Clone();
                        }
                        else if (urunSeviye == "3-Grup")
                        {
                            dtUrunGrupKod = bllUrunGrupIslemleri.GetUrunGrupKod(string.Empty, string.Empty, UrunGrupId.ToString(), string.Empty, string.Empty, string.Empty, string.Empty);
                            var vUrunGrupKod = dtUrunGrupKod.AsEnumerable().Where(k => k.Field<string>("urun_seviye") == "4-Grup");
                            dtUrunGrupKod = vUrunGrupKod.Count() > 0 ? vUrunGrupKod.CopyToDataTable() : dtUrunGrupKod.Clone();
                        }
                        else if (urunSeviye == "4-Grup")
                        {
                            dtUrunGrupKod = bllUrunGrupIslemleri.GetUrunGrupKod(string.Empty, string.Empty, string.Empty, UrunGrupId.ToString(), string.Empty, string.Empty, string.Empty);
                            var vUrunGrupKod = dtUrunGrupKod.AsEnumerable().Where(k => k.Field<string>("urun_seviye") == "5-Grup");
                            dtUrunGrupKod = vUrunGrupKod.Count() > 0 ? vUrunGrupKod.CopyToDataTable() : dtUrunGrupKod.Clone();
                        }
                        else if (urunSeviye == "5-Grup")
                        {
                            dtUrunGrupKod = bllUrunGrupIslemleri.GetUrunGrupKod(string.Empty, string.Empty, string.Empty, string.Empty, UrunGrupId.ToString(), string.Empty, string.Empty);
                            var vUrunGrupKod = dtUrunGrupKod.AsEnumerable().Where(k => k.Field<string>("urun_seviye") == "6-Grup");
                            dtUrunGrupKod = vUrunGrupKod.Count() > 0 ? vUrunGrupKod.CopyToDataTable() : dtUrunGrupKod.Clone();
                        }
                        else if (urunSeviye == "6-Grup")
                        {
                            dtUrunGrupKod = bllUrunGrupIslemleri.GetUrunGrupKod(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, UrunGrupId.ToString(), string.Empty);
                            var vUrunGrupKod = dtUrunGrupKod.AsEnumerable().Where(k => k.Field<string>("urun_seviye") == "7-Grup");
                            dtUrunGrupKod = vUrunGrupKod.Count() > 0 ? vUrunGrupKod.CopyToDataTable() : dtUrunGrupKod.Clone();
                        }

                        if (dtUrunGrupKod != null && dtUrunGrupKod.Rows.Count > 0)
                        {
                            string category = string.Empty;

                            category += "<div class=\"subcategories\"><ul>";
                            category += "<li class=\"current-categorie\"><a href=\"" + urlRewrite + "\">" + urunAd + "</a></li>";
                            foreach (DataRow dr in dtUrunGrupKod.Rows)
                            {
                                category += "<li><a href=\"" + dr["url_rewrite"].ToString().Replace("V","L") + "\">" + dr["urun_ad"].ToString() + "</a></li>";
                            }
                            category += "</ul></div>";

                            ltrCategory.Text = category;
                        }
                    }
                }
            }

            Dictionary<string, string[]> QueryStringDict = QueryStrBuilder.GetirQueryString();
            if (QueryStringDict.ContainsKey("kbilesen_id"))
            {

                string[] QueryStringKbilesenId = QueryStringDict["kbilesen_id"];
                foreach (string kbilesenId in QueryStringKbilesenId)
                {
                    inpExtraVeri = inpExtraVeri + kbilesenId;
                }
            }
            if (QueryStringDict.ContainsKey("minfiyat") && QueryStringDict.ContainsKey("maxfiyat"))
            {
                int minFiyat, maxFiyat;
                string _minfiyat = QueryStringDict["minfiyat"][0];
                string _maxfiyat = QueryStringDict["maxfiyat"][0];

                if (int.TryParse(_minfiyat, out minFiyat) && int.TryParse(_maxfiyat, out maxFiyat))
                {
                    InpFiyatAralik[0] = minFiyat;
                    InpFiyatAralik[1] = maxFiyat;
                }
            }

            if (Request.QueryString["sayfano"] != null && int.TryParse(Request.QueryString["sayfano"], out SayfaNo) && SayfaNo > 0)
            {

                dsUrunListeleme = bllUrunListeleme.GetUrunListele(UrunGrupId, 0, SayfaNo, SiralamaKriter, inpExtraVeri, out ToplamSayfa, out ToplamUrun, out FiyatAralik, InpFiyatAralik);

                dtExtraOzellikTT = dsUrunListeleme.Tables["ExtraOzellikTT"];
                rowsay = 0;
                foreach (DataRow row in dsUrunListeleme.Tables["UrunKartTT"].Rows)
                {

                    if (dsUrunListeleme.Tables["UrunKartTT"].Rows[rowsay]["resim_ad"].ToString().IndexOf("http") == -1)
                    {
                        dsUrunListeleme.Tables["UrunKartTT"].Rows[rowsay]["resim_ad"] = "http://cdn.gold.com.tr/UrunResim/BuyukResim/" + dsUrunListeleme.Tables["UrunKartTT"].Rows[rowsay]["resim_ad"];
                    }
                    rowsay = rowsay + 1;

                }
                rptUrunler.DataSource = dsUrunListeleme.Tables["UrunKartTT"];
                rptUrunler.DataBind();
            }
            else
            {
                int MarkaId = 0;
                if (QueryStringDict.ContainsKey("markaid"))
                    int.TryParse(QueryStringDict["markaid"][0], out MarkaId);


                dsUrunListeleme = bllUrunListeleme.GetUrunListele(UrunGrupId, MarkaId, 1, SiralamaKriter, inpExtraVeri, out ToplamSayfa, out ToplamUrun, out FiyatAralik, InpFiyatAralik);

                if (dsUrunListeleme != null && dsUrunListeleme.Tables.Count > 0 && dsUrunListeleme.Tables["UrunKartTT"].Rows.Count > 0)
                {
                    dtExtraOzellikTT = dsUrunListeleme.Tables["ExtraOzellikTT"];
                    rowsay = 0;
                    foreach (DataRow row in dsUrunListeleme.Tables["UrunKartTT"].Rows)
                    {

                        if (dsUrunListeleme.Tables["UrunKartTT"].Rows[rowsay]["resim_ad"].ToString().IndexOf("http") == -1)
                        {
                            dsUrunListeleme.Tables["UrunKartTT"].Rows[rowsay]["resim_ad"] = "http://cdn.gold.com.tr/UrunResim/BuyukResim/" + dsUrunListeleme.Tables["UrunKartTT"].Rows[rowsay]["resim_ad"];
                        }
                        rowsay = rowsay + 1;

                    }
                    rptUrunler.DataSource = dsUrunListeleme.Tables["UrunKartTT"];
                    rptUrunler.DataBind();
                }
                else if (dsUrunListeleme.Tables["ExtraOzellikTT"].Rows.Count > 0)
                {
                    dtExtraOzellikTT = dsUrunListeleme.Tables["ExtraOzellikTT"];
                    rowsay = 0;
                    foreach (DataRow row in dsUrunListeleme.Tables["UrunKartTT"].Rows)
                    {

                        if (dsUrunListeleme.Tables["UrunKartTT"].Rows[rowsay]["resim_ad"].ToString().IndexOf("http") == -1)
                        {
                            dsUrunListeleme.Tables["UrunKartTT"].Rows[rowsay]["resim_ad"] = "http://cdn.gold.com.tr/UrunResim/BuyukResim/" + dsUrunListeleme.Tables["UrunKartTT"].Rows[rowsay]["resim_ad"];
                        }
                        rowsay = rowsay + 1;

                    }
                    rptUrunler.DataSource = dsUrunListeleme.Tables["UrunKartTT"];
                    rptUrunler.DataBind();
                }
                else
                    Response.Redirect("~/Default.aspx", true);
            }

            FiyatSliderOlustur(FiyatAralik[0], FiyatAralik[1], InpFiyatAralik[0], InpFiyatAralik[1]);
            ExtraOzellikSecimleriHazirla();
            SayfalamaOlustur(ToplamSayfa, SayfaNo);
            AyakIziOlustur(UrunGrupId);
        }

        if (dsUrunListeleme != null)
            dsUrunListeleme.Dispose();


    }

    private void ExtraOzellikSecimleriHazirla()
    {
        /*
        DataView dvwDataView = new DataView(dtExtraOzellikTT);
        dtExtraOzellikGrup = dvwDataView.ToTable(true, "veri_kod");

        dtExtraOzellikGrup.Columns.Add(new DataColumn("urunsayi"));

        for (int i = 0; i < dtExtraOzellikGrup.Rows.Count; i++)
        {
            if (i % 2 == 0)
                dtExtraOzellikGrup.Rows[i]["urunsayi"] = 4;
            if (i % 3 == 0)
                dtExtraOzellikGrup.Rows[i]["urunsayi"] = 6;
            if (i % 5 == 0)
                dtExtraOzellikGrup.Rows[i]["urunsayi"] = 8;
            if (i % 7 == 0)
                dtExtraOzellikGrup.Rows[i]["urunsayi"] = 12;
        }

        rptUrunOzellikGrup.DataSource = dtExtraOzellikGrup;
        rptUrunOzellikGrup.DataBind();



        //Seçilen Özellikleri sırala
        DataRow[] _SecilenOzellikler = dtExtraOzellikTT.Select("secim_durum = 'True'");
        if (_SecilenOzellikler.Length > 0)
        {

            DataView dvwDataViewSecimler = new DataView(_SecilenOzellikler.CopyToDataTable());
            dtExtraOzellikGrupSecim = dvwDataViewSecimler.ToTable(true, "veri_kod");

            rptSecilenOzellikGrup.DataSource = dtExtraOzellikGrupSecim;
            rptSecilenOzellikGrup.DataBind();

        }
        */


        /* 
       
         DataView dvwDataView = new DataView(dtExtraOzellikTT);
         dtExtraOzellikGrup = dvwDataView.ToTable(true, "veri_kod");

         if (dtExtraOzellikGrup.Rows.Count > 3)
         {
             var ExtraOzellikGenel = (from extraozellik in dtExtraOzellikGrup.AsEnumerable()
                                      select extraozellik).Take(3);
             var ExtraOzellikDiger = (from extraozellik in dtExtraOzellikGrup.AsEnumerable()
                                      select extraozellik).Skip(3);

             rptExtraOzellik.DataSource = ExtraOzellikGenel.CopyToDataTable();
             rptExtraOzellik.DataBind();

             rptExtraOzellikGenelDiger.DataSource = ExtraOzellikDiger.CopyToDataTable();
             rptExtraOzellikGenelDiger.DataBind();
         }
         else
         {
             rptExtraOzellik.DataSource = dtExtraOzellikGrup;
             rptExtraOzellik.DataBind();
         }

         */

    }

    protected void rptSecilenOzellikGrup_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string veri_kod = ((DataRowView)e.Item.DataItem).Row["veri_kod"].ToString().Replace("'", "''");
            Repeater rptSecilenOzellikSecenek = (Repeater)e.Item.FindControl("rptSecilenOzellikSecenek");
            rptSecilenOzellikSecenek.DataSource = dtExtraOzellikTT.Select("veri_kod = '" + veri_kod + "' and secim_durum = 'True'");
            rptSecilenOzellikSecenek.DataBind();
        }
    }

    protected void rptUrunOzellikGrup_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        /*
        string veri_kod = dtExtraOzellikGrup.Rows[e.Item.ItemIndex]["veri_kod"].ToString();
        Repeater rptUrunOzellikSecenek = (Repeater)e.Item.FindControl("rptUrunOzellikSecenek");
        var ozellikSecenek = dtExtraOzellikTT.AsEnumerable().Where(row => row.Field<string>("veri_kod") == veri_kod);

        //  rptUrunOzellikSecenek.DataSource = dtExtraOzellikTT.Select("veri_kod = '" + veri_kod + "'");
        rptUrunOzellikSecenek.DataSource = ozellikSecenek.CopyToDataTable();
        rptUrunOzellikSecenek.DataBind();
        */
    }

    protected void chkExtraOzellik_CheckedChanged(object sender, EventArgs e)
    {

        string Sayfa = HttpContext.Current.Request.Url.PathAndQuery;
        QueryStrBuilder.EkleQueryString("SayfaNo", "1", Sayfa);
        string kbilesenId = ((CheckBox)sender).Attributes["kbilesen_id"];
        if (((CheckBox)sender).Checked)
        {
            Response.Redirect(QueryStrBuilder.EkleQueryString("kbilesen_id", kbilesenId, Sayfa));

        }
        else
        {
            Response.Redirect(QueryStrBuilder.SilQueryString("kbilesen_id", kbilesenId, Sayfa));
        }


    }

    //protected void rptExtraOzellik_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    string veri_kod = dtExtraOzellikGrup.Rows[e.Item.ItemIndex]["veri_kod"].ToString();
    //    Repeater rptExtraOzellikDetay = (Repeater)e.Item.FindControl("rptExtraOzellikDetay");
    //    rptExtraOzellikDetay.DataSource = dtExtraOzellikTT.Select("veri_kod = '" + veri_kod + "'");
    //    rptExtraOzellikDetay.DataBind();
    //}

    //protected void rptExtraOzellikDetay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    CheckBox chkExtraOzellik = (CheckBox)e.Item.FindControl("chkExtraOzellik");
    //    chkExtraOzellik.Text = ((ErpData.StrongTypesNS.UrunListelemeDSDataSet.ExtraOzellikTTRow)(e.Item.DataItem)).veri_secim.ToString() + " (" + ((ErpData.StrongTypesNS.UrunListelemeDSDataSet.ExtraOzellikTTRow)(e.Item.DataItem)).urun_adet.ToString() + ")";
    //    chkExtraOzellik.Attributes.Add("kbilesen_id", ((ErpData.StrongTypesNS.UrunListelemeDSDataSet.ExtraOzellikTTRow)(e.Item.DataItem)).kbilesen_id.ToString());
    //    chkExtraOzellik.Checked = ((ErpData.StrongTypesNS.UrunListelemeDSDataSet.ExtraOzellikTTRow)(e.Item.DataItem)).secim_durum;
    //}

    private void AyakIziOlustur(int UrunGrupId)
    {
        DataTable dtAyakIzi = new DataTable();
        dtAyakIzi = bllUrunGrupIslemleri.GetUrunGrupIzi(UrunGrupId);
        string UcKategori = "";
        DataRow dr;
        if (dtAyakIzi.Rows.Count > 0)
        {
            dr = dtAyakIzi.Rows[0];

            //Meta description title
            if (!string.IsNullOrEmpty(dr["urun_title"].ToString()))
                Page.Title = dr["urun_title"].ToString();
            if (!string.IsNullOrEmpty(dr["urun_keywords"].ToString()))
                Page.MetaKeywords = dr["urun_keywords"].ToString();
            if (!string.IsNullOrEmpty(dr["urun_description"].ToString()))
                Page.MetaDescription = dr["urun_description"].ToString();


            if (!string.IsNullOrEmpty(dr["urunust_ad1"].ToString()))
            {
                UcKategori = dr["urunust_ad1"].ToString();
                ltrAyakizi.Text = "<a title=\"" + dr["urunust_ad1"].ToString() + "\" href=\"" + dr["url_rewrite1"].ToString().Replace("V", "l") + "\">" + dr["urunust_ad1"].ToString() + "</a>";
            }
            if (!string.IsNullOrEmpty(dr["urunust_ad2"].ToString()))
            {
                UcKategori = dr["urunust_ad2"].ToString();
                ltrAyakizi.Text = ltrAyakizi.Text + "<span class=\"navigation-pipe\">&nbsp;</span><a title=\"" + dr["urunust_ad2"].ToString() + "\" href=\"" + dr["url_rewrite2"].ToString().Replace("V", "l") + "\">" + dr["urunust_ad2"].ToString() + "</a>";
            }
            if (!string.IsNullOrEmpty(dr["urunust_ad3"].ToString()))
            {
                UcKategori = dr["urunust_ad3"].ToString();
                ltrAyakizi.Text = ltrAyakizi.Text + "<span class=\"navigation-pipe\">&nbsp;</span><a title=\"" + dr["urunust_ad3"].ToString() + "\" href=\"" + dr["url_rewrite3"].ToString() + "\">" + dr["urunust_ad3"].ToString() + "</a>";
            }
            if (!string.IsNullOrEmpty(dr["urunust_ad4"].ToString()))
            {
                UcKategori = dr["urunust_ad4"].ToString();
                ltrAyakizi.Text = ltrAyakizi.Text + "<span class=\"navigation-pipe\">&nbsp;</span><a title=\"" + dr["urunust_ad4"].ToString() + "\" href=\"" + dr["url_rewrite4"].ToString() + "\">" + dr["urunust_ad4"].ToString() + "</a>";
            }
            if (!string.IsNullOrEmpty(dr["urunust_ad5"].ToString()))
            {
                UcKategori = dr["urunust_ad5"].ToString();
                ltrAyakizi.Text = ltrAyakizi.Text + "<span class=\"navigation-pipe\">&nbsp;</span><a title=\"" + dr["urunust_ad5"].ToString() + "\" href=\"" + dr["url_rewrite5"].ToString() + "\">" + dr["urunust_ad5"].ToString() + "</a>";
            }

            //Content Remarketing
            ContentRemarketingAlan(dr["content_alan"].ToString(), dr["remarketing_alan"].ToString());



            //Seçilen Ürün Grubunu Yakala
            string UrunGrubuAd = "";
            if (!string.IsNullOrEmpty(dr["urunust_ad5"].ToString()))
                UrunGrubuAd = dr["urunust_ad5"].ToString();
            else if (!string.IsNullOrEmpty(dr["urunust_ad4"].ToString()))
                UrunGrubuAd = dr["urunust_ad4"].ToString();
            else if (!string.IsNullOrEmpty(dr["urunust_ad3"].ToString()))
                UrunGrubuAd = dr["urunust_ad3"].ToString();
            else if (!string.IsNullOrEmpty(dr["urunust_ad2"].ToString()))
                UrunGrubuAd = dr["urunust_ad2"].ToString();
            else if (!string.IsNullOrEmpty(dr["urunust_ad1"].ToString()))
                UrunGrubuAd = dr["urunust_ad1"].ToString();
        }

    }

    private string VitrinControl(string urunustId, int UrunGrupId)
    {
        if (urunustId == UrunGrupId.ToString())
            return "UrunListeleme.aspx";
        else
            return "Vitrin.aspx";

    }

    private void SayfalamaOlustur(int ToplamSayfa, int SayfaNo)
    {

        Dictionary<string, string[]> QueryStringDict = QueryStrBuilder.GetirQueryString();
        string inpExtraVeri = "";
        if (QueryStringDict.ContainsKey("kbilesen_id"))
        {
            string[] QueryStringKbilesenId = QueryStringDict["kbilesen_id"];
            foreach (string kbilesenId in QueryStringKbilesenId)
            {
                if (string.IsNullOrEmpty(inpExtraVeri))
                    inpExtraVeri = "&kbilesen_id=" + kbilesenId;
                else
                    inpExtraVeri = inpExtraVeri + kbilesenId;
            }
        }
        int BaslangicSayfaNo = 0;
        int BitisSayfaNo = 0;

        if (SayfaNo % 10 == 0)
        {
            if (SayfaNo - 4 > 0)
                BaslangicSayfaNo = SayfaNo - 4;
            else
                BaslangicSayfaNo = 1;

            if (SayfaNo + 5 <= ToplamSayfa)
                BitisSayfaNo = SayfaNo + 5;
            else
                BitisSayfaNo = ToplamSayfa;
        }
        else
        {
            BaslangicSayfaNo = Convert.ToInt32(Math.Round((Convert.ToDecimal(SayfaNo) / Convert.ToDecimal(10)), 1) * 10) - 4;
            if (BaslangicSayfaNo < 1)
                BaslangicSayfaNo = 1;
            if (BaslangicSayfaNo + 9 > ToplamSayfa)
                BitisSayfaNo = ToplamSayfa;
            else
                BitisSayfaNo = BaslangicSayfaNo + 9;
        }

        StringBuilder sb = new StringBuilder();

        sb.Append("<li><a onclick=\"SayfaYonlendir('1');\" aria-label=\"İlk\"><span aria-hidden=\"true\">&laquo;</span></a></li>");

        for (int i = BaslangicSayfaNo; i <= BitisSayfaNo; i++)
        {
            if (i == SayfaNo)
                sb.Append("<li class=\"active\"><a href=\"#\">" + SayfaNo.ToString() + "</a></li>");
            else
                sb.Append("<li><a onclick=\"SayfaYonlendir('" + i.ToString() + "');\">" + i.ToString() + "</a></li>");
        }

        sb.Append("<li><a onclick=\"SayfaYonlendir('" + ToplamSayfa.ToString() + "');\" aria-label=\"Son\"><span aria-hidden=\"true\">&raquo;</span></a></li>");

        ltrSayfalama.Text = sb.ToString();

    }

    protected void rptUrunler_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //Ürün Fiyatları
        DataRowView drv = e.Item.DataItem as DataRowView;
        Label lblBrutTutar = (Label)e.Item.FindControl("lblBrutTutar");
        Label lblNetTutar = (Label)e.Item.FindControl("lblNetTutar");
        Label lblBilgi = (Label)e.Item.FindControl("lblBilgi");

        if (Convert.ToBoolean(drv.Row["temin_durum"]))
        {
            decimal brut_tutar = Math.Round(Convert.ToDecimal(drv.Row["brut_tutar"]), 2);
            decimal net_tutar = Math.Round(Convert.ToDecimal(drv.Row["net_tutar"]), 2);

            decimal onikiTaksit = Math.Round((Convert.ToDecimal(drv.Row["net_tutar"]) / 12), 2);

            //lblBrutTutar.Text = brut_tutar.ToString("###,###,###") + " <span>PUAN</span>";

            //lblNetTutar.Text = onikiTaksit.ToString() + " PUAN";
            //lblBilgi.Text = "x 12 Taksit";

            //lblBilgi.Text = "KDV Dahil";
            if (brut_tutar > net_tutar)
            {
                lblBrutTutar.Text = brut_tutar.ToString("###,###,###") + " <span>PUAN</span>";
                lblNetTutar.Text = net_tutar.ToString("###,###,###") + " <span>PUAN</span>";
            }
            else
            {
                lblBrutTutar.Visible = false;
                lblNetTutar.Text = brut_tutar.ToString("###,###,###") + " <span>PUAN</span>";
            }

        }
        else
        {
            if (!string.IsNullOrEmpty(drv.Row["temin_aciklama"].ToString()))
                lblBilgi.Text = drv.Row["temin_aciklama"].ToString();
            else
                lblBilgi.Text = "Ürün geçici olarak temin edilemiyor.";
        }

        //İkonlar
        //if (Convert.ToBoolean(drv.Row["indirimli"]))
        //    ((Image)e.Item.FindControl("imgIndirimli")).ImageUrl = "~/imagesgld/icon_indirimliurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgIndirimli")).ImageUrl = "~/imagesgld/icon_indirimliurunOff.png";

        //if (Convert.ToBoolean(drv.Row["aynigun_sevk"]))
        //    ((Image)e.Item.FindControl("imgAyniGunSevk")).ImageUrl = "~/imagesgld/icon_hizlikargoOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgAyniGunSevk")).ImageUrl = "~/imagesgld/icon_hizlikargoOff.png";

        //if (Convert.ToBoolean(drv.Row["hediyeli_urun"]))
        //    ((Image)e.Item.FindControl("imgHediyeliUrun")).ImageUrl = "~/imagesgld/icon_hediyeliurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgHediyeliUrun")).ImageUrl = "~/imagesgld/icon_hediyeliurunOff.png";

        //if (Convert.ToBoolean(drv.Row["kuryeile_sevk"]))
        //    ((Image)e.Item.FindControl("imgKuryeileSevk")).ImageUrl = "~/imagesgld/icon_coksatanurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgKuryeileSevk")).ImageUrl = "~/imagesgld/icon_coksatanurunOff.png";

        //if (Convert.ToBoolean(drv.Row["kargo_bedava"]))
        //    ((Image)e.Item.FindControl("imgKargoBedava")).ImageUrl = "~/imagesgld/icon_kargobedavaOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgKargoBedava")).ImageUrl = "~/imagesgld/icon_kargobedavaOff.png";

        //if (Convert.ToBoolean(drv.Row["yeni_urun"]))
        //    ((Image)e.Item.FindControl("imgYeniUrun")).ImageUrl = "~/imagesgld/icon_yeniurunOn.png";
        //else
        //    ((Image)e.Item.FindControl("imgYeniUrun")).ImageUrl = "~/imagesgld/icon_yeniurunOff.png";

    }

    private void FiyatSliderOlustur(int minFiyat, int maxFiyat, int secilenMinFiyat, int SecilenMaxFiyat)
    {
        if (secilenMinFiyat == 0 && SecilenMaxFiyat == 0)
        {
            secilenMinFiyat = minFiyat;
            SecilenMaxFiyat = maxFiyat;
        }

        int AdimAralik = Convert.ToInt32(Math.Round(((maxFiyat - minFiyat) * 0.01), 0));

        string rangePriceHtml = "<div data-label-reasult=\"\" data-min=\"" + minFiyat.ToString() + "\" data-max=\"" + maxFiyat.ToString() + "\""
                                + "data-unit=\" PUAN\" class=\"slider-range-price\""
                                + "data-value-min=\"" + secilenMinFiyat.ToString() + "\" data-value-max=\"" + SecilenMaxFiyat.ToString() + "\">"
                                + "</div>"
                                + "<div class=\"amount-range-price\">"
                                + "</div>";

        ltrRangePrice.Text = rangePriceHtml;
    }

    [WebMethod]
    public static string FiyatFiltrele(string minFiyat, string maxFiyat)
    {
        string Sayfa = HttpContext.Current.Request.UrlReferrer.PathAndQuery;
        string QueryString = "";
        QueryStringBuilder QueryStrBuilder = new QueryStringBuilder();

        if (minFiyat == "0" && maxFiyat == "0")
        {
            QueryString = QueryStrBuilder.SilQueryString("maxfiyat-minfiyat", "0", Sayfa);
        }
        else
        {
            QueryStrBuilder.EkleQueryString("minfiyat", minFiyat, Sayfa);
            QueryString = QueryStrBuilder.EkleQueryString("maxfiyat", maxFiyat, Sayfa);
        }
        return QueryString;
    }

    [WebMethod]
    public static string UrunleriSirala(string SiralamaKriter)
    {
        string Sayfa = HttpContext.Current.Request.UrlReferrer.PathAndQuery;
        string QueryString = "";
        QueryStringBuilder QueryStrBuilder = new QueryStringBuilder();

        if (string.IsNullOrEmpty(SiralamaKriter))
        {
            QueryString = QueryStrBuilder.SilQueryString("siralama", "*", Sayfa);
        }
        else
        {
            QueryString = QueryStrBuilder.EkleQueryString("siralama", SiralamaKriter, Sayfa);
        }
        return QueryString;
    }

    [WebMethod]
    public static string ExtraOzellikFiltrele(string kbilesenid, string durum)
    {
        string Sayfa = HttpContext.Current.Request.UrlReferrer.PathAndQuery;
        string QueryString = "";
        QueryStringBuilder QueryStrBuilder = new QueryStringBuilder();
        if (durum == "Ekle")
        {
            QueryStrBuilder.EkleQueryString("SayfaNo", "1", Sayfa);
            QueryString = QueryStrBuilder.EkleQueryString("kbilesen_id", kbilesenid, Sayfa);
        }
        else if (durum == "Cikart")
        {
            QueryString = QueryStrBuilder.SilQueryString("kbilesen_id", kbilesenid, Sayfa);
        }
        return QueryString;
    }

    [WebMethod]
    public static string KarsilastirmaKaydetSil(string UrunId, string Kod)
    {

        string result = "";
        int _UrunId = 0;
        if (HttpContext.Current.Session["Kullanici"] != null && int.TryParse(UrunId, out _UrunId))
        {
            Kullanici kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];

            KarsilastirmaIslemleriBLL bllKarsilastirmaIslemleri = new KarsilastirmaIslemleriBLL();
            UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();

            //Cari kaydını oluştur ErpCariId güncelle
            if (kullanici.ErpCariId < 1)
                kullanici = bllUyelikIslemleri.CariVeriOlustur(kullanici);


            if (Kod == "sil")
            {
                bllKarsilastirmaIslemleri.KarsilastirmaSil(kullanici.ErpCariId, _UrunId);
            }
            else if (Kod == "kaydet")
            {
                bllKarsilastirmaIslemleri.KarsilastirmaKaydet(kullanici.ErpCariId, _UrunId);
            }

        }
        else
        {
            HttpContext.Current.Session["SonUrl"] = HttpContext.Current.Request.UrlReferrer.PathAndQuery;
            result = "Yönlendir";
        }

        return result;


    }


    protected void rptExtraOzellik_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string veri_kod = dtExtraOzellikGrup.Rows[e.Item.ItemIndex]["veri_kod"].ToString();
        Repeater rptExtraOzellikIcerik = new Repeater();
        rptExtraOzellikIcerik = ((Repeater)e.Item.FindControl("rptExtraOzellikIcerik"));

        Repeater rptExtraOzellikIcerikDiger = new Repeater();
        rptExtraOzellikIcerikDiger = ((Repeater)e.Item.FindControl("rptExtraOzellikIcerikDiger"));

        DataTable dtExtraOzellikIcerik = new DataTable();
        DataTable dtIcerik = new DataTable();
        DataTable dtDiger = new DataTable();



        var ExtraOzellikIcerik = from ExtraOzellik in dtExtraOzellikTT.AsEnumerable()
                                 where ExtraOzellik.Field<string>("veri_kod") == veri_kod
                                 select ExtraOzellik;

        if (ExtraOzellikIcerik.Any())
            dtExtraOzellikIcerik = ExtraOzellikIcerik.CopyToDataTable();

        if (dtExtraOzellikIcerik.Rows.Count > 3 && dtExtraOzellikIcerik.Rows[0]["veri_kod"].ToString() != "Marka")
        {

            var Icerik = (from ExtraOzellik in ExtraOzellikIcerik.AsEnumerable()
                          select ExtraOzellik).Take(3);

            if (Icerik.Any())
                dtIcerik = Icerik.CopyToDataTable();

            var Diger = (from ExtraOzellik in dtExtraOzellikIcerik.AsEnumerable()
                         select ExtraOzellik).Skip(3);

            if (Diger.Any())
                dtDiger = Diger.CopyToDataTable();

            rptExtraOzellikIcerik.DataSource = dtIcerik;
            rptExtraOzellikIcerik.DataBind();

            rptExtraOzellikIcerikDiger.DataSource = dtDiger;
            rptExtraOzellikIcerikDiger.DataBind();

        }
        else
        {
            ((HtmlControl)e.Item.FindControl("DivDigerOzellikler")).Visible = false;
            rptExtraOzellikIcerik.DataSource = dtExtraOzellikIcerik;
            rptExtraOzellikIcerik.DataBind();
        }

    }


    protected void rptExtraOzellikIcerik_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkExtraOzellik = (CheckBox)e.Item.FindControl("chkExtraOzellik");
            chkExtraOzellik.Text = ((DataRowView)e.Item.DataItem).Row["veri_secim"].ToString() + " (" + ((DataRowView)e.Item.DataItem).Row["urun_adet"].ToString() + ")";
            chkExtraOzellik.Attributes.Add("kbilesen_id", ((DataRowView)e.Item.DataItem).Row["kbilesen_id"].ToString());
            chkExtraOzellik.Checked = Convert.ToBoolean(((DataRowView)e.Item.DataItem).Row["secim_durum"]);
        }
    }

    protected void rptExtraOzellikIcerikDiger_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkExtraOzellik = (CheckBox)e.Item.FindControl("chkExtraOzellik");
            chkExtraOzellik.Text = ((DataRowView)e.Item.DataItem).Row["veri_secim"].ToString() + " (" + ((DataRowView)e.Item.DataItem).Row["urun_adet"].ToString() + ")";
            chkExtraOzellik.Attributes.Add("kbilesen_id", ((DataRowView)e.Item.DataItem).Row["kbilesen_id"].ToString());
            chkExtraOzellik.Checked = Convert.ToBoolean(((DataRowView)e.Item.DataItem).Row["secim_durum"]);
        }
    }

    protected void rptExtraOzellikGenelDigerIcerik_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CheckBox chkExtraOzellik = (CheckBox)e.Item.FindControl("chkExtraOzellik");
            chkExtraOzellik.Text = ((DataRowView)e.Item.DataItem).Row["veri_secim"].ToString() + " (" + ((DataRowView)e.Item.DataItem).Row["urun_adet"].ToString() + ")";
            chkExtraOzellik.Attributes.Add("kbilesen_id", ((DataRowView)e.Item.DataItem).Row["kbilesen_id"].ToString());
            chkExtraOzellik.Checked = Convert.ToBoolean(((DataRowView)e.Item.DataItem).Row["secim_durum"]);
        }

    }

    protected void rptExtraOzellikGenelDiger_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string veri_kod = ((DataRowView)e.Item.DataItem).Row["veri_kod"].ToString();


        Repeater rptExtraOzellikGenelDigerIcerik = new Repeater();
        rptExtraOzellikGenelDigerIcerik = ((Repeater)e.Item.FindControl("rptExtraOzellikGenelDigerIcerik"));


        var ExtraOzellikIcerik = from ExtraOzellik in dtExtraOzellikTT.AsEnumerable()
                                 where ExtraOzellik.Field<string>("veri_kod") == veri_kod
                                 select ExtraOzellik;

        if (ExtraOzellikIcerik.Any())
        {
            rptExtraOzellikGenelDigerIcerik.DataSource = ExtraOzellikIcerik.CopyToDataTable();
            rptExtraOzellikGenelDigerIcerik.DataBind();
        }



    }

    [WebMethod]
    public static string SayfalamaYonlendir(string SayfaNo)
    {
        string Sayfa = HttpContext.Current.Request.UrlReferrer.PathAndQuery;
        string QueryString;
        QueryStringBuilder QueryStrBuilder = new QueryStringBuilder();

        QueryString = QueryStrBuilder.EkleQueryString("sayfano", SayfaNo, Sayfa);

        return QueryString;
    }

    private void ContentRemarketingAlan(string ContentAlan, string RemarketingAlan)
    {
        /*
        string Ayrac = "#DevamiVar#";
        if (!string.IsNullOrEmpty(ContentAlan))
        {
            int AyracIndex = ContentAlan.IndexOf(Ayrac);
            if (AyracIndex > -1)
            {
                ltrSeoContent.Text = ContentAlan.Substring(0, AyracIndex).TrimEnd();
                hdnSeoContent.Value = ContentAlan.Substring(AyracIndex + Ayrac.Length, ContentAlan.Length - (AyracIndex + Ayrac.Length)).TrimStart();
            }
            else
            {
                ltrSeoContent.Text = ContentAlan;
                hdnSeoContent.Value = "";
                spnSeoDevam.Visible = false;
            }
        }
        else
            DivSeoContent.Visible = false;

        if (!string.IsNullOrEmpty(RemarketingAlan))
        {
            ltrRemarketingAlan.Text = RemarketingAlan;
        }
        */
    }


    protected void btnKarsilastir_Click(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {
            if (Request["Compare"] != null)
            {
                string[] Urunler = Request["Compare"].ToString().Split(',');
                Kullanici kullanici = new Kullanici();
                kullanici = (Kullanici)Session["Kullanici"];

                //Cari kaydını oluştur ErpCariId güncelle
                if (kullanici.ErpCariId < 1)
                    kullanici = bllUyelikIslemleri.CariVeriOlustur(kullanici);

                foreach (string urun in Urunler)
                {
                    bllKarsilastirmaIslemleri.KarsilastirmaKaydet(kullanici.ErpCariId, Convert.ToInt32(urun));
                }
            }

            Response.Redirect("~/Karsilastir.aspx", false);

        }
        else
        {
            UyelikIslemleriBLL.MusteriGirisineYonlendir(Request.Url.PathAndQuery);
        }

    }

    private void CokSatanUrunler(int UrunGrupId)
    {
        DataTable dtCokSatanUrun = bllOlapUrun.CokSatanUrun(UrunGrupId, 0, 0);
        if (dtCokSatanUrun != null && dtCokSatanUrun.Rows.Count > 0)
        {
            rptCokSatanlarListeleme.DataSource = dtCokSatanUrun;
            rptCokSatanlarListeleme.DataBind();
        }
        else
            rptCokSatanlarListeleme.Visible = false;
    }



}