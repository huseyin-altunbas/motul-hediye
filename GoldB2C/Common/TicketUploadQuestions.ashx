﻿<%@ WebHandler Language="C#" Class="TicketUploadQuestions" %>

using System;
using System.Web;
using System.IO;
using Goldb2cBLL;

public class TicketUploadQuestions : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        context.Response.Expires = -1;
        var FolderId = context.Request["FolderId"];
        try
        {
            if (FolderId == null || FolderId.ToString() == string.Empty)
            {
                do
                {
                    FolderId = Guid.NewGuid().ToString();

                } while (Directory.Exists("~/TicketImage/TicketQuestions/" + FolderId));
            }

            string savepath = string.Empty;
            savepath = HttpContext.Current.Server.MapPath("~/TicketImage/TicketQuestions/" + FolderId);

            if (!Directory.Exists(savepath))
                Directory.CreateDirectory(savepath);

            HttpPostedFile postedFile = context.Request.Files["Filedata"];
            string filename = postedFile.FileName;
            filename = CommonBLL.ChangeTRCharacter(filename);
            filename = filename.Replace(" ", "").Replace("&", "").Replace("=", "");

            do
            {
                filename = Guid.NewGuid() + "=" + filename;

            } while (File.Exists(savepath + @"\" + filename));

            postedFile.SaveAs(savepath + @"\" + filename);


            context.Response.StatusCode = 200;
            context.Response.Write(FolderId + "&" + filename);

        }

        catch (Exception ex)
        {
            context.Response.Write("Error: " + ex.Message);
        }
    }
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}

