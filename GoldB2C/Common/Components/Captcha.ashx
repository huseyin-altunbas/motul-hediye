<%@ WebHandler Language="C#" Class="Captcha" %>

using System;
using System.Web;
using System.Drawing.Imaging;
using System.Drawing;
using CaptchaDotNet2;
using CaptchaDotNet2.Security.Cryptography;

public class Captcha : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "image/jpeg";
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.BufferOutput = false;
        string CapchaEncryptClearText = Goldb2cInfrastructure.CustomConfiguration.getCapchaEncryptClearText;
        string CapchaEncryptPassword = Goldb2cInfrastructure.CustomConfiguration.getCapchaEncryptPassword;


        // Get text
        string s = "Sayfay� Yenileyiniz";
        if (context.Request.QueryString["c"] != null &&
            context.Request.QueryString["c"] != "")
        {
            string enc = context.Request.QueryString["c"].ToString();

            // space was replaced with + to prevent error
            enc = enc.Replace(" ", "+");

            int mod4 = enc.Length % 4;
            if (mod4 > 0)
            {
                enc += new string('=', 4 - mod4);
            }

            try
            {
                s = Encryptor.Decrypt(enc, CapchaEncryptClearText, Convert.FromBase64String(CapchaEncryptPassword));

            }
            catch { }
        }
        // Get dimensions
        int w = 130;
        int h = 30;
        // Width
        if (context.Request.QueryString["w"] != null &&
            context.Request.QueryString["w"] != "")
        {
            try
            {
                w = Convert.ToInt32(context.Request.QueryString["w"]);
            }
            catch { }
        }
        // Height
        if (context.Request.QueryString["h"] != null &&
            context.Request.QueryString["h"] != "")
        {
            try
            {
                h = Convert.ToInt32(context.Request.QueryString["h"]);
            }
            catch { }
        }
        // Color
        Color Bc = new Color();
        Bc = ColorTranslator.FromHtml("#FFFFFF");
        if (context.Request.QueryString["bc"] != null &&
            context.Request.QueryString["bc"] != "")
        {
            try
            {
                string bc = context.Request.QueryString["bc"].ToString().Insert(0, "#");
                Bc = ColorTranslator.FromHtml(bc);
            }
            catch { }
        }
        // Generate image
        CaptchaImage ci = new CaptchaImage(s, Bc, w, h);

        // Return
        ci.Image.Save(context.Response.OutputStream, ImageFormat.Jpeg);
        // Dispose
        ci.Dispose();
    }

    public bool IsReusable
    {
        get
        {
            return true;
        }
    }
}