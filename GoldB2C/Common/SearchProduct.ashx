﻿<%@ WebHandler Language="C#" Class="SearchProduct" %>

using System;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using Goldb2cBLL;
using Goldb2cInfrastructure;
using System.Data;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;

public class SearchProduct : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        string prefixText = context.Request.QueryString["term"].Trim();
        string strParam = InputSafety.SecureString(prefixText).Replace("\"", "").Replace(":", "").Replace("!", "").Replace("&", "").Replace("%", "").Replace("*", "");
        List<Urun> lstUrun;

        AramaMotoruBLL bllAramaMotoru = new AramaMotoruBLL();
        DataTable dtArama = bllAramaMotoru.getAramaMotoru(prefixText);

        var teminEdilenUrunler = dtArama.AsEnumerable().Where(u => u.Field<bool>("temin_durum") == true && u.Field<decimal>("net_tutar") > 0).Take(8);
        dtArama = teminEdilenUrunler.Count() > 0 ? teminEdilenUrunler.CopyToDataTable() : dtArama.Clone();

        lstUrun = new List<Urun>();
        Urun urun;
        foreach (DataRow dr in dtArama.Rows)
        {
            urun = new Urun();
            urun.id = dr["urun_id"].ToString();
            urun.value = dr["urun_ad"].ToString();
            if (dr["resim_ad"].ToString().IndexOf("http") > -1)
                urun.image = dr["resim_ad"].ToString();
            else if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/UrunResim/Product/" + dr["resim_ad"].ToString())))
                urun.image = "/UrunResim/Product/" + dr["resim_ad"].ToString();
            else
                urun.image = "http://cdn.gold.com.tr/UrunResim/BuyukResim/" + dr["resim_ad"].ToString();
            urun.label = dr["urun_ad"].ToString();
            urun.link = dr["urun_link"].ToString();

            lstUrun.Add(urun);

        }

        JavaScriptSerializer serializer = new JavaScriptSerializer();
        string jsonString = serializer.Serialize(lstUrun);
        context.Response.Write(jsonString);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}

public class Urun
{
    public string id { get; set; }
    public string value { get; set; }
    public string image { get; set; }
    public string label { get; set; }
    public string link { get; set; }
}