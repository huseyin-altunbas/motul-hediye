﻿<%@ WebHandler Language="C#" Class="TicketUploadQuestions" %>

using System;
using System.Web;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Goldb2cBLL;

public class TicketUploadQuestions : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        context.Response.Expires = -1;

        HttpPostedFile postedFile = context.Request.Files["Filedata"];
        string filename = postedFile.FileName;

        try
        {
            string savepath = string.Empty;
            string filePath = string.Empty;

            List<string> listAllowedExtensions = new List<string> { ".gif", ".png", ".jpeg", ".jpg" };
            string fileExtension = System.IO.Path.GetExtension(filename).ToLower();

            if (listAllowedExtensions.Any(k => k == fileExtension))
            {
                savepath = HttpContext.Current.Server.MapPath("~/UrunResim/Product/");
                if (!Directory.Exists(savepath))
                    Directory.CreateDirectory(savepath);

                filePath = savepath + @"\" + filename;
                postedFile.SaveAs(filePath);

                context.Response.StatusCode = 200;
                context.Response.Write(string.Format("<div class=\"messageSuccessUpload\">{0} dosyası başarıyla yüklendi.</div>", filename));
            }
            else
            {
                context.Response.Write(string.Format("<div class=\"messageCancelUpload\">{0} dosyası yüklenemedi. Desteklenen formatlar : .gif, .png, .jpeg, .jpg</div>", filename));
            }
        }
        catch (Exception ex)
        {
            context.Response.Write(string.Format("<div class=\"messageCancelUpload\">{0} dosyası yüklenemedi. Sistem hatası.</div>", filename));
        }
    }
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}

