﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="Sepet.aspx.cs" Inherits="Sepet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- page wapper-->
    <div class="columns-container">
        <div class="container" id="columns">
            <!-- breadcrumb -->
            <div class="breadcrumb clearfix">
                <a class="home" href="/" title="Ana Sayfa">Ana Sayfa</a> <span class="navigation-pipe">
                    &nbsp;</span> <span class="navigation_page">Sepet</span>
            </div>
            <!-- ./breadcrumb -->
            <!-- page heading-->
            <%--<h2 class="page-heading no-line">
                <span class="page-heading-title2">SEPETİM</span>
            </h2>--%>
            <!-- ../page heading-->
            <div class="page-content page-order">
                <ul class="step">
                    <li class="current-step"><span>1. Sepet</span></li>
                    <li><span>2. Adres</span></li>
                    <li><span>3. Onay</span></li>
                </ul>
                <div class="heading-counter warning">
                    SEPETİM
                </div>
                <asp:UpdatePanel ID="updPnlrptSepet" runat="server">
                    <ContentTemplate>
                        <asp:Repeater ID="dlSepet" runat="server" OnItemDataBound="dlSepet_ItemDataBound">
                            <HeaderTemplate>
                                <div class="order-detail-content">
                                    <table class="table table-bordered table-responsive cart_summary" style="margin-bottom: 0px !important;">
                                        <thead>
                                            <tr>
                                                <th class="action">
                                                    <input name="" type="checkbox" onclick="CheckChanged(this,'chkUrun')" value="Tümünü Seç" />
                                                </th>
                                                <th class="cart_product">
                                                    Ürün
                                                </th>
                                                <th>
                                                    Açıklama
                                                </th>
                                                <th>
                                                    Adet
                                                </th>
                                                <th>
                                                    Tutar
                                                </th>
                                            </tr>
                                        </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <tr>
                                        <td class="action">
                                            <asp:CheckBox ID="chkUrun" SepetDetayId='<%#DataBinder.Eval(Container.DataItem,"sepet_detayid") %>'
                                                UrunId='<%#DataBinder.Eval(Container.DataItem,"urun_id") %>' runat="server" />
                                        </td>
                                        <td class="cart_product">
                                            <a href="<%#DataBinder.Eval(Container.DataItem,"urun_link") %>">
                                                <img src="<%#DataBinder.Eval(Container.DataItem,"resim_ad") %>" style="max-width: 100px;" /></a>
                                        </td>
                                        <td class="cart_description">
                                            <p class="product-name">
                                                <a href="<%#DataBinder.Eval(Container.DataItem,"urun_link") %>">
                                                    <%#DataBinder.Eval(Container.DataItem,"urun_ad") %>
                                                </a>
                                            </p>
                                            <small>
                                                <%#!string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "spot_kelime").ToString()) ? "- <span class=\"redSpot\">" + DataBinder.Eval(Container.DataItem, "spot_kelime").ToString() + "</span>" : ""%></small><br>
                                        </td>
                                        <td class="qty">
                                            <asp:TextBox ID="txtUrunAdet" CssClass="w35 tacenter qty-txt" MaxLength="4" onkeypress="rakamKontrol(this.id)"
                                                Text='<%#DataBinder.Eval(Container.DataItem,"satis_adet") %>' runat="server"></asp:TextBox>
                                            <a href="" id="qty-up" data-uid="<%#DataBinder.Eval(Container.DataItem,"urun_id") %>">
                                                <i class="fa fa-caret-up"></i></a><a href="#" id="qty-down" data-uid="<%#DataBinder.Eval(Container.DataItem,"urun_id") %>">
                                                    <i class="fa fa-caret-down"></i></a>
                                        </td>
                                        <td class="price">
                                            <span>
                                                <asp:Label ID="lblListeNetTutar" runat="server" Text=""></asp:Label></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table> </div>
                            </FooterTemplate>
                        </asp:Repeater>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSil" />
                        <asp:AsyncPostBackTrigger ControlID="btnGuncelle" EventName="Click" />
                        <asp:PostBackTrigger ControlID="btnSatinAl" />
                    </Triggers>
                </asp:UpdatePanel>
                <div class="cart_navigation cartBar">
                    <asp:LinkButton ID="btnSil" runat="server" Text="Sil" OnClick="btnSil_Click">Sil</asp:LinkButton>
                    <asp:LinkButton ID="btnGuncelle" runat="server" Text="Sepeti Güncelle" OnClick="btnGuncelle_Click">Sepeti Güncelle</asp:LinkButton>
                </div>
                <div class="cartBar2">
                    <span>Genel Toplam</span><span> : </span>
                    <asp:Label ID="lblGenelToplam" runat="server" Text=""></asp:Label>
                </div>
                <div class="cartBar2" id="aradiv" runat="server" visible="false" style="display: none">
                    <img src="imagesgld/bullet_red6x6.png" width="6" height="6" />&nbsp; <strong>Siparişte
                        Kullanacağınız Avans Puan :</strong>
                    <asp:Label ID="lblFarkAvansPuan" runat="server"></asp:Label>
                </div>
                <div class="cart_navigation cartBar3">
                    <asp:LinkButton ID="btnAlisveriseDevam" runat="server" CssClass="prev-btn" Text="Alışverişe Devam Et"
                        OnClick="btnAlisveriseDevam_Click">Alışverişe Devam Et</asp:LinkButton>
                    <asp:LinkButton ID="btnSatinAl" runat="server" CssClass="next-btn" Text="Satın Al"
                        OnClick="btnSatinAl_Click">Satın Al</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <!-- ./page wapper-->
    <script type="text/javascript">

        //        function gostergizle() {
        //            var deger = document.getElementById("lblFarkAvansPuan").value;
        //            var deger1 = document.getElementById("lblGenelToplam").value;
        //            if (deger > deger1) 
        //            {
        //            var divObject = document.getElementById(aradiv);
        //            divObject.style.display = "none";
        //            } 
        //            else 
        //            {
        //            var divObject = document.getElementById(aradiv);
        //            divObject.style.display = "block";
        //            }
        //        }   

        function rakamKontrol(contolid) {
            $('#' + contolid).keypress(function (evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            });
        }

        function SepetiGuncelle(UrunId, txtControl) {
            var UrunAdet = txtControl.value;
            var txtControlId = txtControl.id;
            $.ajax({
                type: 'POST',
                url: '/Sepet.aspx/SepetiGuncelle',
                data: '{"UrunId" :"' + UrunId + '", "UrunAdet" : "' + UrunAdet + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    __doPostBack('<%=btnGuncelle.UniqueID %>', '')
                    KazanciGuncelle(txtControlId, UrunAdet, UrunId);
                    SepetToplamlarHesapla();
                }
                //                ,
                //                error: function (result) {
                //                    alert('Talep-2 esnasında sorun oluştu. Yeniden deneyin');
                //                }
            });
        }

        function SepetToplamlarHesapla() {
            $.ajax({
                type: 'POST',
                url: '/Sepet.aspx/SepetToplamlarHesapla',
                data: '',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    $('#<%=lblGenelToplam.ClientID %>').html(result.d.GenelTutar + " PUAN");

                    var geneltoplam = result.d.GenelTutar;
                    //                    $.ajax({
                    //                        type: 'POST',
                    //                        url: '/Sepet.aspx/AvansPuanHesapla',
                    //                        data: '{"GenelToplam":"' + geneltoplam + '"}', contentType: 'application/json; charset=utf-8',
                    //                        dataType: 'json',
                    //                        success: function (result) {
                    //                            if (result.d <= "0")
                    //                                $("#<%=aradiv.ClientID %>").hide();
                    //                            else {
                    //                                $("#<%=aradiv.ClientID %>").show();
                    //                                $('#<%=lblFarkAvansPuan.ClientID %>').html(result.d);
                    //                            }
                    //                        }
                    ////                        ,
                    ////                        error: function (result) {
                    ////                            alert('avanspuan hesaplama esnasında sorun oluştu. Yeniden deneyin');
                    ////                        }
                    //                    });
                }
                //                ,
                //                error: function (result) {
                //                    alert('Talep-1 esnasında sorun oluştu. Yeniden deneyin');
                //                }
            });
        }

        function KazanciGuncelle(txtControlId, Adet, UrunId) {
            $.ajax({
                type: 'POST',
                url: '/Sepet.aspx/KazanciGuncelle',
                data: '{"txtControlId":"' + txtControlId + '","Adet":"' + Adet + '", "UrunId":"' + UrunId + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    if (result.d != "") {
                        var strings = result.d.split("||");
                        var labelControlId = '#' + strings[0];
                        $(labelControlId).html(strings[1] + " PUAN");
                    }
                }
                //                ,
                //                error: function (result) {
                //                    alert('Talep-3 esnasında sorun oluştu. Yeniden deneyin');
                //                }
            });
        }

        function CheckChanged(TumunuSec, indexOfCont) {
            var frm = document.forms[0];
            for (i = 0; i < frm.length; i++) {
                e = frm.elements[i];
                if (e.type == TumunuSec.type && e.name.indexOf(indexOfCont) != -1)
                    e.checked = TumunuSec.checked;
            }
        }

        function number_format(number, decimals, dec_point, thousands_sep) {
            number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        }

        $(document).ready()
        {
            function pageLoad(sender, args) {

                SepetToplamlarHesapla();

                //                var label1 = parseInt($("#<%=lblGenelToplam.ClientID%>").text());
                //                var label2 = parseInt($("#<%=lblFarkAvansPuan.ClientID%>").text());
                //                if (label2 > 0) {
                //                    $("#aradiv").hide();
                //                }
                //                else {
                //                    $("#aradiv").show();
                //                }

                GetSepetBilgileri();

                $(".qty").on("click", "a#qty-up", function (event) {

                    event.preventDefault();

                    var qtySelector = $(this).parent().find(".qty-txt");
                    var qtySelectorId = qtySelector.attr("id");
                    var uid = $(this).data("uid");

                    var oldValue = +qtySelector.val();
                    var newValue = oldValue + 1;

                    qtySelector.val(newValue).promise().done(function () {
                        SepetiGuncelle(uid.toString(), document.getElementById(qtySelectorId));
                    });
                });
                $(".qty").on("click", "a#qty-down", function (event) {

                    event.preventDefault();

                    var qtySelector = $(this).parent().find(".qty-txt");
                    var qtySelectorId = qtySelector.attr("id");
                    var uid = $(this).data("uid");

                    var oldValue = +qtySelector.val();
                    var newValue = oldValue - 1;

                    if (newValue > 0) {
                        qtySelector.val(newValue).promise().done(function () {
                            SepetiGuncelle(uid.toString(), document.getElementById(qtySelectorId));
                        });
                    }
                });
            }
        }

    </script>
</asp:Content>
