﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using Goldb2cEntity;
using System.Text;
using Goldb2cInfrastructure;
using System.IO;
using System.Net;
using System.Net.Mail;

public partial class CagriMerkeziIletisim : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        string ad = InputSafety.SecureString(txtAd.Text.Trim());
        string soyad = InputSafety.SecureString(txtSoyad.Text.Trim());
        string email = InputSafety.SecureString(txtEmail.Text.Trim());
        string soru = InputSafety.SecureString(txtSoru.Text.Trim());
        string cepTelKod = InputSafety.SecureString(txtCepTelKod.Text.Trim());
        string cepTelNo = InputSafety.SecureString(txtCepTel.Text.Trim());
        bool sonuc = false;

        if (!string.IsNullOrEmpty(ad) && !string.IsNullOrEmpty(soyad) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(soru) && !string.IsNullOrEmpty(cepTelKod) && !string.IsNullOrEmpty(cepTelNo))
        {
            sonuc = IletisimMailGonder(ad, soyad, email, soru, cepTelKod, cepTelNo);
            txtAd.Text = string.Empty;
            txtSoyad.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtSoru.Text = string.Empty;
            txtCepTelKod.Text = string.Empty;
            txtCepTel.Text = string.Empty;

            if (sonuc)
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("<br/>Öneri veya Sorunuz ilgili kişilere iletildi."));
            else
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("<br/>Hata oluştu.<br/>Daha sonra tekrar deneyiniz."));

        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("<br/>Lütfen ilgili alanları kontrol ediniz."));
        }

    }

    public bool IletisimMailGonder(string ad, string soyad, string email, string soru, string cepTelKod, string cepTelNo)
    {
        try
        {
            string strHTML = File.ReadAllText(Server.MapPath("~/MailSablon/IletisimFormu.htm"));

            strHTML = strHTML.Replace("#ad#", ad);
            strHTML = strHTML.Replace("#soyad#", soyad);
            strHTML = strHTML.Replace("#email#", email);
            strHTML = strHTML.Replace("#soru#", soru);
            strHTML = strHTML.Replace("#cepTelKod#", cepTelKod);
            strHTML = strHTML.Replace("#cepTelNo#", cepTelNo);

            SendEmail mail = new SendEmail();
            mail.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
            mail.MailTo = new string[] { "tutkaloperasyon@tutkal.com.tr" };
            //01.12.2017
            //mail.MailTo = new string[] { "Fulya.akcora@tutkal.com.tr", "Bilge.Aslan@tutkal.com.tr" };
            //01.12.2017
            mail.MailHtml = true;
            mail.MailSubject = "motulteam.com iletişim formu email gönderimi.";

            mail.MailBody = strHTML;
            bool mailSent = mail.SendMail(false);

            if (mailSent)
                return true;
            else
                return false;

        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<table width=\"200\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td>" + mesaj + "</td></tr><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr></table>');</script>";
    }
}