﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="AnasayfaBannerv2.aspx.cs" Inherits="Admin_AnasayfaBannerv2" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <link rel="stylesheet" type="text/css" href="/css/rotator.css?v=1.0.2" />
        <div class="slider-wrap" id="demo4" style="width: 632px; height: 346px; position: relative !important;">
            <div class="slider4-wrap">
                <asp:Literal ID="ltrBanner" runat="server"></asp:Literal>
                <div class="slider4-arrow-prev slider4-arrow">
                </div>
                <div class="slider4-arrow-next slider4-arrow">
                </div>
                <div class="sliderjs-play-pause">
                </div>
            </div>
        </div>
        <script type="text/javascript" src="/js/slider/slider.js?v=3.1.4"></script>
        <script type="text/javascript" src="/js/slider/script.js?v=3.1.4"></script>
        <script type="text/javascript">

            $("#demo4").mouseenter(function () {
                $('.sliderjs-play-pause').click();
            }).mouseleave(function () {
                $('.sliderjs-play-pause').click();
            });

            $('.slider4-ticker').mouseenter(
        function () {
            $(this).click();
        });
        </script>
    </div>
    <div class="panelBanner">
        <table>
            <tr>
                <th class="title1">
                    Sıra No
                </th>
                <th class="title2">
                    Banner Resim
                </th>
                <th class="title3">
                    Banner Link
                </th>
                <th class="title4">
                    Alternate Text / Title
                </th>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtBnr1SiraNo" Text="1" runat="server" CssClass="BannerNo"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr1Resim" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr1Link" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr1AltTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtBnr2SiraNo" Text="2" runat="server" CssClass="BannerNo"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr2Resim" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr2Link" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr2AltTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtBnr3SiraNo" Text="3" runat="server" CssClass="BannerNo"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr3Resim" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr3Link" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr3AltTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtBnr4SiraNo" Text="4" runat="server" CssClass="BannerNo"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr4Resim" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr4Link" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr4AltTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtBnr5SiraNo" Text="5" runat="server" CssClass="BannerNo"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr5Resim" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr5Link" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr5AltTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtBnr6SiraNo" Text="6" runat="server" CssClass="BannerNo"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr6Resim" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr6Link" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr6AltTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtBnr7SiraNo" Text="7" runat="server" CssClass="BannerNo"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr7Resim" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr7Link" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr7AltTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtBnr8SiraNo" Text="8" runat="server" CssClass="BannerNo"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr8Resim" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr8Link" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr8AltTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtBnr9SiraNo" Text="9" runat="server" CssClass="BannerNo"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr9Resim" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr9Link" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr9AltTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtBnr10SiraNo" Text="10" runat="server" CssClass="BannerNo"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr10Resim" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr10Link" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtBnr10AltTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>
        <asp:Button ID="btnOnizle" runat="server" Text="Önizle" OnClick="btnOnizle_Click"
            CssClass="aramaBTN fl" />
        <asp:Button ID="btnGeriAl" runat="server" Text="Geri Al" OnClick="btnGeriAl_Click"
            CssClass="aramaBTN fl" />
        <asp:Button ID="btnKaydet" runat="server" Text="Kaydet" OnClick="btnKaydet_Click"
            CssClass="aramaBTN fl" />
    </div>
    <div style="clear: both">
    </div>
    <div>
        <br />
        <div style="color: Red">
            ***Banner resim alanlarında tanımladığınız resimlerin sitede görüntülenmesi için
            resimleri (büyük ve küçük resim olarak) sunucuya ekleyiniz.
        </div>
        <br />
        <div style="color: Red">
            Resim Ekle
        </div>
        <br />
        <asp:Label ID="lblBuyukResim" runat="server" Text="Büyük Resim"></asp:Label>&nbsp;
        <asp:FileUpload ID="fuBuyukResim" runat="server" /><br />
        <asp:Label ID="lblKucukResim" runat="server" Text="Küçük Resim"></asp:Label>&nbsp;
        <asp:FileUpload ID="fuKucukResim" runat="server" /><br />
        <br />
        <asp:Button ID="btnResimKaydet" runat="server" Text="Resim Kaydet" CssClass="aramaBTN fl"
            OnClick="btnResimKaydet_Click" />
    </div>
    <br />
    <div style="clear: both">
    </div>
</asp:Content>
