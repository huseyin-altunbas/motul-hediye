﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="UyeListesi.aspx.cs" Inherits="Admin_UyeListesi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    
    <script>
        $(document).ready(function () {
           $(".adminContainer").css("width", "80%");
        });
    </script>

    <div style="font-size: 16px;">
        <h3>Üye Listesi</h3>
    </div>

    <div id="gridTrancaction"></div>


    <script>
        function RunKendoTrancaction() {
            var remoteDataSource = new kendo.data.DataSource({
                pageSize: 15,
                transport: {
                    read: {
                        url: "/api/Kullanici/Get?Token=<%=kullanici.ServiceKey%>",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json",
                        complete: function (data) {

                        }

                    },
                    create: {
                        url: "/Kullanici/Post?Token=<%=kullanici.ServiceKey%>",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json",
                    },
                    update: {
                        url: "#",
                        dataType: "json",
                        type: "PUT",
                    },
                    destroy: {
                        url: "#",
                        dataType: "json",
                        type: "DELETE"
                    },
                    parameterMap: function (options, operation) {

                        operation = operation;

                        if (operation === "read") {
                            //Export settings start
                            var filter = encodeURIComponent(JSON.stringify(options));
                            $(".k-grid-customExcelExport").attr("href", '/api/Kullanici/Export/?Token=<%=kullanici.ServiceKey%>&filter=' + filter)
                            $('.k-grid-customExcelExport').attr('target', '_blank');
                            //Export settings stop


                        }

                        if (operation !== "read" && options.model) {
                            return JSON.stringify(options.models[0]);
                        }
                        else {

                            return JSON.stringify(options);
                        }


                    }
                },
                schema: {
                    data: "Data",
                    total: "Total",
                    model: {
                        KullaniciId: "KullaniciId",
                        fields: {
                            id: { editable: false, hidden: true },
                            created: { type: "date", editable: false, nullable: true },

                        }
                    }
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                error: function (e) {
                    //console.log(e);
                    swal("", "Lütfen tüm alanları kontrol ediniz.", "info");
                }
            });

            $('#gridTrancaction').kendoGrid({
                dataSource: remoteDataSource,
                toolbar: [
                    // { name: "create", text: "MANUEL PUAN EKLE" },
                    { name: 'customExcelExport', text: 'EXCEL RAPORUNU İNDİR' },

                ],
                //editable: {
                //    mode: "popup",
                //    template: kendo.template($("#popup-editor").html())
                //},

                scrollable: true,
                sortable: true,
                edit: onEdit,
                filterMenuInit: filterMenu,
                filterable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [

                    { field: "KullaniciId", title: "Id", width: "80px" },
                    /*{ field: "CRMCustomerGUID", title: "Acente Kodu", editable: false, filterable: true, width: "80px" },*/
                    { field: "ErpCariId", title: "ErpCariId", editable: false, filterable: true, width: "80px" },
                    { field: "KullaniciAd", title: "Üye Adı", editable: false, filterable: true, width: "200px" },
                    { field: "Email", title: "Email", editable: false, filterable: true, width: "200px" },
                    { field: "CepTelNo", title: "Cep Telefonu", editable: false, filterable: true, width: "100px" },
                    { field: "AktifText", title: "Durum", editable: false, filterable: false, width: "70px", sortable: false },
                    { field: "id", title: "Puan Hareketleri", template: '<a class="btn btn-success" target="_blank" href="/Admin/PuanHareketleri.aspx?KullaniciId=#=KullaniciId#">Puan Hareketleri</a>', filterable: false, sortable: false, width: "80px" },


                    //{
                    //    command: [
                    //        { text: "Onayla", click: TransactionConfirm }

                    //    ],
                    //    title: "İşlemler",
                    //    width: "180px",
                    //    //locked: true,
                    //    //lockable: false,
                    //},

                ]
            });



         


            function onEdit(e) {

            }

            function statusFilter(element) {
                var form = element.closest("form");
                // changes the help text. (you might want to localise this)
                form.find(".k-filter-help-text:first").text("Durum seçiniz:");
                // removes the dropdown list containing the operators (contains etc)
                form.find("select").remove();
                form.find(".k-dropdown-wrap").remove();
                element.kendoDropDownList({
                    dataSource: ['Aktif', 'Pasif'],
                    optionLabel: "Durum Seçiniz",
                });
            }

            function transactionTypeFilter(element) {
                var form = element.closest("form");
                // changes the help text. (you might want to localise this)
                form.find(".k-filter-help-text:first").text("Durum seçiniz:");
                // removes the dropdown list containing the operators (contains etc)
                form.find("select").remove();
                form.find(".k-dropdown-wrap").remove();
                element.kendoDropDownList({
                    dataSource: ['Faturadan kazanılan puan.', 'Fatura iptaline istinaden silinen puan.', 'Eğitimden kazanılan puan.', 'Manuel yüklenen puan.', 'Manuel silinen puan.'],
                    optionLabel: "Tip Seçiniz",
                });

            }

            function filterMenu(e) {
                if (e.field == "created") {
                    var beginOperator = e.container.find("[data-role=dropdownlist]:eq(0)").data("kendoDropDownList");
                    beginOperator.value("gte");
                    beginOperator.trigger("change");

                    var endOperator = e.container.find("[data-role=dropdownlist]:eq(2)").data("kendoDropDownList");
                    endOperator.value("lte");
                    endOperator.trigger("change");

                    e.container.find(".k-dropdown").hide();
                }
            }

        }



        $(document).ready(function () {
            RunKendoTrancaction();

        });

    </script>


</asp:Content>

