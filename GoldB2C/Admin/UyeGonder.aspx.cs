﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using System.IO;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Web.UI.HtmlControls;

public partial class Admin_UyeGonder : System.Web.UI.Page
{
    BannerBLL bllBanner = new BannerBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void ClearRepeater()
    {
        rptFuExcel.DataSource = null;
        rptFuExcel.DataBind();
        rptAllUsers.DataSource = null;
        rptAllUsers.DataBind();
    }

    protected void btnShow_Click(object sender, EventArgs e)
    {
        rptAllUsers.Visible = false;
        rptFuExcel.Visible = true;
        ClearRepeater();

        if (fuExcel.HasFile)
        {
            try
            {
                string path = HttpContext.Current.ApplicationInstance.Server.MapPath("~/Admin/uyeGonderExcel");
                string fn = System.IO.Path.GetFileName(fuExcel.PostedFile.FileName);
                fuExcel.PostedFile.SaveAs(System.IO.Path.Combine(path, fn));

                string fileName = Server.MapPath("~/Admin/uyeGonderExcel/" + fn);


                OleDbConnection odConnExcel = new OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;data source='" + fileName + "';Extended Properties='Excel 8.0; HDR=Yes; IMEX=1'");
                odConnExcel.Open();
                string sql = "select * from [Sayfa1$] ";
                OleDbDataAdapter odDaExcel = new OleDbDataAdapter(sql, odConnExcel);
                odConnExcel.Close();

                DataTable dt = new DataTable();
                odDaExcel.Fill(dt);

                rptFuExcel.DataSource = dt;
                rptFuExcel.DataBind();
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Hata oluştu"));
                ClearRepeater();
            }
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Excel dosyası seçiniz."));
        }
    }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        ClearRepeater();
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {

        bool errorFile = false;
        bool chkState = false;
        string kullaniciAd = "";
        string kullaniciSoyad = "";
        string email = "";
        string sifre = "";

        StringBuilder sbSameEmailAddress = new StringBuilder();
        StringBuilder sbSaveError = new StringBuilder();

        DateTime dtDogumTarih = new DateTime(1753, 1, 1);
        foreach (RepeaterItem rpItem in rptFuExcel.Items)
        {
            kullaniciAd = (rpItem.FindControl("lblKullaniciAd") as Label).Text.ToString();
            kullaniciSoyad = (rpItem.FindControl("lblKullaniciSoyad") as Label).Text.ToString();
            email = (rpItem.FindControl("lblEmail") as Label).Text.ToString();
            sifre = (rpItem.FindControl("lblSifre") as Label).Text.ToString();

            CheckBox chkRowExcel = rpItem.FindControl("chkRow") as CheckBox;
            if (chkRowExcel.Checked)
            {
                int kullaniciID = bllUyelikIslemleri.MusteriKullaniciKayit(kullaniciAd, kullaniciSoyad, email, sifre, dtDogumTarih, string.Empty /*Tanımsız*/, "TR" /*Tr*/, string.Empty, "", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, true, true, "KurumsalB2C", string.Empty, string.Empty, string.Empty,"",true);
                if (kullaniciID == 0)
                {
                    sbSaveError.Append("<br/>" + ((Label)rpItem.FindControl("lblEmail")).Text);
                }
                else if (kullaniciID == -1)
                {
                    sbSameEmailAddress.Append("<br/>" + ((Label)rpItem.FindControl("lblEmail")).Text);
                }
                //else if (kullaniciID == -2)
                //{
                //    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Tavsiye eden üyenin e-mail adresi sistemimize kayıtlı değildır. Lütfen kontrol ediniz." ));
                //}

                chkState = true;
            }
        }

        if (sbSaveError.ToString() != string.Empty)
        {
            errorFile = true;
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sistemde bir hata oluştu. Lütfen daha sonra yeniden deneyin. " + sbSaveError.ToString()));
        }

        if (sbSameEmailAddress.ToString() != string.Empty)
        {
            errorFile = true;
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Bu e-mail adresi sistemimize kayıtlıdır. " + sbSameEmailAddress.ToString()));
        }

        if (!chkState)
        {
            errorFile = true;
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Herhangi bir kayıt seçiniz."));
        }

        if (!errorFile)
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Kayıt işlemi başarılı."));

        }


    }


    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + mesaj + "</td></tr></table></div>');</script>";
    }



    protected void bntGetAllUsers_Click(object sender, EventArgs e)
    {

        rptFuExcel.Visible = false;
        rptAllUsers.Visible = true;
        ClearRepeater();

        try
        {
            rptAllUsers.DataSource = bllUyelikIslemleri.GetirKullaniciAll();
            rptAllUsers.DataBind();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Hata oluştu"));
        }
    }
    protected void rptAllUsers_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int sonuc = 0;
        //TextBox txtPassword = (e.Item.FindControl("txtSifre") as TextBox);
        CheckBox chkActive = (e.Item.FindControl("chkAktif") as CheckBox);
        CheckBox chkReset = (e.Item.FindControl("chkSifreReset") as CheckBox);

        string[] arg;

        if (e.CommandName == "Update")
        {
            //txtPassword.Enabled=true;
            chkActive.Enabled = true;
            chkReset.Enabled = true;
            HtmlTableRow rowAll = (HtmlTableRow)e.Item.FindControl("trRow");
            rowAll.Attributes.Add("style", "background-color:#FF6B6B;");
            //txtPassword.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF6B6B");

        }

        if (e.CommandName == "UpdateSave")
        {
            arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');

            if (chkReset.Checked)
                sonuc = bllUyelikIslemleri.GuncelleKullaniciSifreAktif(Convert.ToInt32(arg[0].ToString()), "Lafarge123Dalsan", chkActive.Checked);
            else
                sonuc = bllUyelikIslemleri.GuncelleKullaniciSifreAktif(Convert.ToInt32(arg[0].ToString()), arg[1].ToString(), chkActive.Checked);

        }

        if (sonuc == 1)
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Güncelleme işlemi başarılı."));
            rptAllUsers.DataSource = null;
            try
            {
                rptAllUsers.DataSource = bllUyelikIslemleri.GetirKullaniciAll();
                rptAllUsers.DataBind();
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Hata oluştu"));
            }
        }

    }
    protected void btnInformation_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Excel dosyasındaki başlık isimleri :<br/>" +
                                            "KullaniciAd, KullaniciSoyad, Email, Sifre <br/>" +
                                            "sol alt taraftaki sayfa ismi de Sayfa1 olması gerekiyor.<br/>" +
                                            "(Excel 97-2003 çalışma kitabı formatında)"));
    }
}