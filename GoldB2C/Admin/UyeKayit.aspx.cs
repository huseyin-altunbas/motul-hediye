﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using System.IO;
using System.Text;
using System.Data;


public partial class Admin_UyeKayit : System.Web.UI.Page
{
    BannerBLL bllBanner = new BannerBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    string RandomNumber = "Sayfayı Yenileyiniz";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CapchaGenerator();

            //dpSehir.DataSource = bllUyelikIslemleri.SehirListesi("TÜRKİYE");
            //dpSehir.DataValueField = "SehirAd";
            //dpSehir.DataTextField = "SehirAd";
            //dpSehir.DataBind();
            //dpSehir.Items.Insert(0, new ListItem("Seçiniz", "0"));

            //dpDogumGun.Items.Add(new ListItem("Gün", "0"));
            //for (int i = 1; i <= 31; i++)
            //{
            //    dpDogumGun.Items.Add(new ListItem(i.ToString(), i.ToString()));
            //}

            //dpDogumAy.Items.Add(new ListItem("Ay", "0"));
            //for (int i = 1; i <= 12; i++)
            //{
            //    dpDogumAy.Items.Add(new ListItem(i.ToString(), i.ToString()));
            //}

            //dpDogumYil.Items.Add(new ListItem("Yıl", "0"));
            //for (int i = 1900; i <= DateTime.Now.Year; i++)
            //{
            //    dpDogumYil.Items.Add(new ListItem(i.ToString(), i.ToString()));
            //}

        }
    }
    protected void lnbUyeKayit_Click(object sender, EventArgs e)
    {

        //if (String.IsNullOrEmpty(txtGuvenlikKodu.Text.Trim()) || Session["UyelikCaptcha"] == null || txtGuvenlikKodu.Text.Trim() != Session["UyelikCaptcha"].ToString())
        //{
        //    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen Güvenlik Kodunu kontrol ediniz."));
        //    CapchaGenerator();
        //    txtGuvenlikKodu.Text = "";

        //    return;

        //}

        if (string.IsNullOrEmpty(txtAd.Text) || string.IsNullOrEmpty(txtEmail.Text) || string.IsNullOrEmpty(txtSifre.Text) || string.IsNullOrEmpty(txtSifreTekrar.Text))
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen alanlarınız kontrol ediniz."));
            CapchaGenerator();
           
            return;
        }
        if (txtSifreTekrar.Text != txtSifre.Text)
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen şifrenizi kontrol ediniz."));
            CapchaGenerator();
           
            return;
        }

        //if (string.IsNullOrEmpty(txtCepTelKod.Text) || txtCepTelKod.Text.Length != 3)
        //{
        //    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen GSM operatörünüzü kontrol ediniz."));
        //    CapchaGenerator();
        //    txtGuvenlikKodu.Text = "";
        //    return;
        //}

        //if (string.IsNullOrEmpty(txtCepTel.Text) || txtCepTel.Text.Length != 7)
        //{
        //    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen GSM numaranızı kontrol ediniz."));
        //    CapchaGenerator();
        //    txtGuvenlikKodu.Text = "";
        //    return;
        //}

        if (txtSifre.Text.Length < 5 || txtSifre.Text.Length > 20)
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Şifreniz 5 ile 20 karakter arasında olmalıdır."));
            CapchaGenerator();
           
            return;
        }

        if (txtSifre.Text != txtSifreTekrar.Text)
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Şifrenizi kontrol ediniz."));
            CapchaGenerator();
           
            return;
        }

        if (this.IsValid)
        {
            try
            {


                int uyelikTipi = 0;
                string musteriAd = txtAd.Text; // ad
                string musteriSoyad = txtSoyad.Text; // soyad
                string email = txtEmail.Text; // email
                string sifre = txtSifre.Text; // şifre
                bool cepHaber = chkCepHaber.Checked;   // cep haber
                bool emailHaber = true; //chkEmailHaber.Checked;   // email haber
                //string SehirKod = "";
                //if (dpSehir.SelectedValue != "0")
                //    SehirKod = dpSehir.SelectedValue;

                DateTime dtDogumTarih = new DateTime(1753, 1, 1);
                //if (dpDogumGun.SelectedValue != "0" && dpDogumAy.SelectedValue != "0" && dpDogumYil.SelectedValue != "0")
                //{
                //    int _gun, _ay, _yil;

                //    if (int.TryParse(dpDogumGun.SelectedValue, out _gun) && int.TryParse(dpDogumAy.SelectedValue, out _ay) && int.TryParse(dpDogumYil.SelectedValue, out _yil))
                //    {
                //        dtDogumTarih = new DateTime(_yil, _ay, _gun);
                //    }

                //}

                //string cinsiyet;

                //if (rbErkek.Checked)
                //    cinsiyet = "E";
                //else
                //    cinsiyet = "K";

                uyelikTipi = Convert.ToInt32(drpUyelikTipi.SelectedValue);

                if (uyelikTipi == 0 || uyelikTipi == 10)
                {
                    int kullaniciID = bllUyelikIslemleri.MusteriKullaniciKayitAdmin(musteriAd, musteriSoyad, email, sifre, dtDogumTarih, string.Empty /*Tanımsız*/, "TR" /*Tr*/, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, txtCepTel.Text.Trim().Replace(" ",""), cepHaber, emailHaber, "B2CLafarge", string.Empty, string.Empty, string.Empty, uyelikTipi);
                    if (kullaniciID == 0)
                    {
                        ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sistemde bir hata oluştu. Lütfen daha sonra yeniden deneyin."));
                        CapchaGenerator();
                        
                    }
                    else if (kullaniciID == -1)
                    {
                        ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Bu e-mail adresi sistemimize kayıtlıdır."));
                        CapchaGenerator();
                        
                    }
                    else if (kullaniciID == -2)
                    {
                        ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Tavsiye eden üyenin e-mail adresi sistemimize kayıtlı değildır. Lütfen kontrol ediniz."));
                        CapchaGenerator();
                       
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Kayıt işlemi başarılı."));
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Üyelik tipini kontrol ediniz."));
                }
            }
            catch
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sistemde bir hata oluştu. Lütfen daha sonra yeniden deneyin."));
                CapchaGenerator();
               
            }
        }
        else
        {

            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen tüm alanları kontrol edip yeniden deneyin."));
            CapchaGenerator();
           

        }

    }

    public void CapchaGenerator()
    {
        Random RandomClass = new Random();
        RandomNumber = RandomClass.Next(1000, 999999).ToString();
        Session.Add("UyelikCaptcha", RandomNumber);

        string CapchaEncryptClearText = CustomConfiguration.getCapchaEncryptClearText;
        string CapchaEncryptPassword = CustomConfiguration.getCapchaEncryptPassword;
        string ens2 = CaptchaDotNet2.Security.Cryptography.Encryptor.Encrypt(RandomNumber, CapchaEncryptClearText, Convert.FromBase64String(CapchaEncryptPassword));
      

    }

    public string MesajScriptOlustur(string mesaj)
    {
        //return "<script type=\"text/javascript\">UyariDivGoster('<table width=\"200\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td>" + mesaj + "</td></tr><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr></table>');</script>";
        return "<script> alert('"+mesaj+"') </script>";
    }


    protected void CustomValidatorSozlesme_ServerValidate(object source, ServerValidateEventArgs args)
    {

        //args.IsValid = (chkUyelikSozlesmesi.Checked);
        args.IsValid = true;

    }




}