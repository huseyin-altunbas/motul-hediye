﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="Advertising.aspx.cs" Inherits="Admin_Advertising" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="styles/AdvertisingStyle.css?v=1.0.0" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptAdvertisingMaster" runat="server">
        <HeaderTemplate>
            <table class="advertisingDetailEdit">
                <tr>
                    <th class="id">
                        Id
                    </th>
                    <th class="explanation">
                        Açıklama
                    </th>
                    <th class="active">
                        Aktif
                    </th>
                    <th class="empty">
                        &nbsp;
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr class="color1">
                <td class="id2">
                    <%#DataBinder.Eval(Container.DataItem,"AdvertisingMasterId") %>
                </td>
                <td class="explanation2">
                    <%#DataBinder.Eval(Container.DataItem,"Aciklama") %>
                </td>
                <td class="active2">
                    <input id="Checkbox1" type="checkbox" disabled="disabled" <%# DataBinder.Eval(Container.DataItem,"Aktif") == "true" ? "" : "checked='checked'" %> />
                </td>
                <td class="empty2">
                    <a href="<%#DataBinder.Eval(Container.DataItem,"SayfaUrl") %>" target="_blank" class="GoToPage">
                        Sayfaya Git</a> <a href="/Admin/Advertising.aspx?AdvertisingMasterId=<%#DataBinder.Eval(Container.DataItem,"AdvertisingMasterId") %>"
                            class="edit">Detaylar</a>
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr>
                <td>
                    <%#DataBinder.Eval(Container.DataItem,"AdvertisingMasterId") %>
                </td>
                <td>
                    <%#DataBinder.Eval(Container.DataItem,"Aciklama") %>
                </td>
                <td>
                    <input id="Checkbox1" type="checkbox" disabled="disabled" <%# DataBinder.Eval(Container.DataItem,"Aktif") == "true" ? "" : "checked='checked'" %> />
                </td>
                <td>
                    <a href="<%#DataBinder.Eval(Container.DataItem,"SayfaUrl") %>" target="_blank">Sayfaya
                        Git</a>
                </td>
                <td>
                    <a href="/Admin/Advertising.aspx?AdvertisingMasterId=<%#DataBinder.Eval(Container.DataItem,"AdvertisingMasterId") %>">
                        Detaylar</a>
                </td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </table></FooterTemplate>
    </asp:Repeater>
    <asp:Repeater ID="rptAdvertisingDetay" runat="server">
        <HeaderTemplate>
            <table class="advertisingDetail">
                <tr>
                    <th class="id">
                        Id
                    </th>
                    <th class="productName">
                        Ürün Adı
                    </th>
                    <th class="imageRoad">
                        Resim Yolu
                    </th>
                    <th class="productLink">
                        Ürün Link
                    </th>
                    <th class="price">
                        Fiyat
                    </th>
                    <th class="salePrice">
                        İ.Fiyat
                    </th>
                    <th class="imageLink">
                        Görsel Url
                    </th>
                    <th class="targetUrl">
                        Hedef Url
                    </th>
                    <th class="advertisingDetailActive">
                        Aktif
                    </th>
                    <th class="advertisingDetailEmpty">
                        &nbsp;
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr class="color2">
                <td class="id2">
                    <%#DataBinder.Eval(Container.DataItem,"AdvertisingDetayId") %>
                </td>
                <td class="productName2">
                    <input id="inpUrunAd" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"UrunAd") %>'
                        class="advertisingDetailInput1" />
                </td>
                <td class="imageRoad2">
                    <input id="inpUrunResim" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"UrunResim") %>'
                        class="advertisingDetailInput1" />
                </td>
                <td class="productLink2">
                    <input id="inpUrunLink" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"UrunLink") %>'
                        class="advertisingDetailInput1" />
                </td>
                <td class="price2">
                    <input id="inpUrunFiyat" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"UrunFiyat") %>'
                        class="advertisingDetailInput3" />
                </td>
                <td class="salePrice2">
                    <input id="inpUrunIndFiyat" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"UrunIndFiyat") %>'
                        class="advertisingDetailInput3" />
                </td>
                <td class="imageLink2">
                    <input id="inpGorselUrl" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"GorselUrl") %>'
                        class="advertisingDetailInput2" />
                </td>
                <td class="targetUrl2">
                    <input id="inpHedefUrl" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"HedefUrl") %>'
                        class="advertisingDetailInput2" />
                </td>
                <td class="advertisingDetailActive2">
                    <input id="chkAktif" type="checkbox" disabled="disabled" <%# DataBinder.Eval(Container.DataItem,"Aktif").ToString() == "True" ? "checked='checked'" : "" %> />
                </td>
                <td class="advertisingDetailEmpty2">
                    <a href="/Admin/Advertising.aspx?AdvertisingMasterId=<%#DataBinder.Eval(Container.DataItem,"AdvertisingMasterId") %>&AdvertisingDetayId=<%#DataBinder.Eval(Container.DataItem,"AdvertisingDetayId") %>">
                        Düzenle</a>
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="color2">
                <td class="id2">
                    <%#DataBinder.Eval(Container.DataItem,"AdvertisingDetayId") %>
                </td>
                <td class="productName2">
                    <input id="inpUrunAd" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"UrunAd") %>'
                        class="advertisingDetailInput1" />
                </td>
                <td class="imageRoad2">
                    <input id="inpUrunResim" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"UrunResim") %>'
                        class="advertisingDetailInput1" />
                </td>
                <td class="productLink2">
                    <input id="inpUrunLink" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"UrunLink") %>'
                        class="advertisingDetailInput1" />
                </td>
                <td class="price2">
                    <input id="inpUrunFiyat" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"UrunFiyat") %>'
                        class="advertisingDetailInput3" />
                </td>
                <td class="salePrice2">
                    <input id="inpUrunIndFiyat" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"UrunIndFiyat") %>'
                        class="advertisingDetailInput3" />
                </td>
                <td class="imageLink2">
                    <input id="inpGorselUrl" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"GorselUrl") %>'
                        class="advertisingDetailInput2" />
                </td>
                <td class="targetUrl2">
                    <input id="inpHedefUrl" type="text" disabled="disabled" value='<%#DataBinder.Eval(Container.DataItem,"HedefUrl") %>'
                        class="advertisingDetailInput2" />
                </td>
                <td class="advertisingDetailActive2">
                    <input id="chkAktif" type="checkbox" disabled="disabled" <%# DataBinder.Eval(Container.DataItem,"Aktif").ToString() == "True" ? "checked='checked'" : "" %> />
                </td>
                <td class="advertisingDetailEmpty2">
                    <a href="/Admin/Advertising.aspx?AdvertisingMasterId=<%#DataBinder.Eval(Container.DataItem,"AdvertisingMasterId") %>&AdvertisingDetayId=<%#DataBinder.Eval(Container.DataItem,"AdvertisingDetayId") %>">
                        Düzenle</a>
                </td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <asp:Panel ID="pnlDetayKayit" runat="server">
        <table class="advertisingEdit">
            <tr>
                <th colspan="3">
                    Düzenleme
                </th>
            </tr>
            <tr class="color3">
                <td class="column1">
                    Görsel Tür
                </td>
                <td class="column2">
                    :
                </td>
                <td class="column3">
                    <select id="dpGorselTur" onchange="TextBoxAcKapa(this.value);">
                        <option selected="selected" value="1">Ürün</option>
                        <option value="2">Banner</option>
                    </select>
                </td>
            </tr>
            <tr class="color4">
                <td colspan="3">
                    Ürün Düzenleme
                </td>
            </tr>
            <tr class="color3">
                <td class="column1">
                    Ürün Adı
                </td>
                <td class="column2">
                    :
                </td>
                <td class="column3">
                    <asp:TextBox ID="txtUrunAd" runat="server" CssClass="advertisingEditInput1"></asp:TextBox>
                </td>
            </tr>
            <tr class="color3">
                <td class="column1">
                    Resim Yolu
                </td>
                <td class="column2">
                    :
                </td>
                <td class="column3">
                    <asp:TextBox ID="txtUrunResimYol" runat="server" CssClass="advertisingEditInput1"></asp:TextBox>
                </td>
            </tr>
            <tr class="color3">
                <td class="column1">
                    Ürün Link
                </td>
                <td class="column2">
                    :
                </td>
                <td class="column3">
                    <asp:TextBox ID="txtUrunLink" runat="server" CssClass="advertisingEditInput1"></asp:TextBox>
                </td>
            </tr>
            <tr class="color3">
                <td class="column1">
                    Fiyat
                </td>
                <td class="column2">
                    :
                </td>
                <td class="column3">
                    <asp:TextBox ID="txtUrunFiyat" runat="server" CssClass="advertisingEditInput2"></asp:TextBox>,<asp:TextBox
                        ID="txtUrunFiyatKurus" runat="server" MaxLength="2" CssClass="advertisingEditInput3"></asp:TextBox>
                </td>
            </tr>
            <tr class="color3">
                <td class="column1">
                    İndirimli Fiyat
                </td>
                <td class="column2">
                    :
                </td>
                <td class="column3">
                    <asp:TextBox ID="txtIndUrunFiyat" runat="server" CssClass="advertisingEditInput2"></asp:TextBox>,<asp:TextBox
                        ID="txtIndUrunFiyatKurus" runat="server" MaxLength="2" CssClass="advertisingEditInput3"></asp:TextBox>
                </td>
            </tr>
            <tr class="color4">
                <td colspan="3">
                    Banner Düzenleme
                </td>
            </tr>
            <tr class="color3">
                <td class="column1">
                    Görsel Url
                </td>
                <td class="column2">
                    :
                </td>
                <td class="column3">
                    <asp:TextBox ID="txtGorselUrl" runat="server" Enabled="false" CssClass="advertisingEditInput1"></asp:TextBox>
                </td>
            </tr>
            <tr class="color3">
                <td class="column1">
                    Hedef Url
                </td>
                <td class="column2">
                    :
                </td>
                <td class="column3">
                    <asp:TextBox ID="txtHedefUrl" runat="server" Enabled="false" CssClass="advertisingEditInput1"></asp:TextBox>
                </td>
            </tr>
            <tr class="color3">
                <td class="column1">
                    Aktif
                </td>
                <td class="column2">
                    :
                </td>
                <td class="column3">
                    <asp:CheckBox ID="chkAktif" runat="server" />
                </td>
            </tr>
            <tr class="color3">
                <td colspan="3" class="advertisingEditBTN">
                    <asp:Button ID="btnKaydet" runat="server" Text="Kaydet" OnClick="btnKaydet_Click"
                        CssClass="advertisingEditSave" />&nbsp;
                    <asp:Button ID="btnYeni" runat="server" Text="Yeni" OnClick="btnYeni_Click" CssClass="advertisingEditNew" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdnDetayId" runat="server" />
    <script type="text/javascript">
        function TextBoxAcKapa(selectedValue) {
            if (selectedValue == "1") {
                $("#<%=txtGorselUrl.ClientID %>").attr("disabled", "disabled");
                $("#<%=txtHedefUrl.ClientID %>").attr("disabled", "disabled");

                $("#<%=txtUrunAd.ClientID %>").removeAttr("disabled");
                $("#<%=txtUrunResimYol.ClientID %>").removeAttr("disabled");
                $("#<%=txtUrunLink.ClientID %>").removeAttr("disabled");
                $("#<%=txtUrunFiyat.ClientID %>").removeAttr("disabled");
                $("#<%=txtUrunFiyatKurus.ClientID %>").removeAttr("disabled");
                $("#<%=txtIndUrunFiyat.ClientID %>").removeAttr("disabled");
                $("#<%=txtIndUrunFiyatKurus.ClientID %>").removeAttr("disabled");

            }
            else if (selectedValue == "2") {
                $("#<%=txtGorselUrl.ClientID %>").removeAttr("disabled");
                $("#<%=txtHedefUrl.ClientID %>").removeAttr("disabled");

                $("#<%=txtUrunAd.ClientID %>").attr("disabled", "disabled");
                $("#<%=txtUrunResimYol.ClientID %>").attr("disabled", "disabled");
                $("#<%=txtUrunLink.ClientID %>").attr("disabled", "disabled");
                $("#<%=txtUrunFiyat.ClientID %>").attr("disabled", "disabled");
                $("#<%=txtUrunFiyatKurus.ClientID %>").attr("disabled", "disabled");
                $("#<%=txtIndUrunFiyat.ClientID %>").attr("disabled", "disabled");
                $("#<%=txtIndUrunFiyatKurus.ClientID %>").attr("disabled", "disabled");
            }
        }
    
    </script>
</asp:Content>
