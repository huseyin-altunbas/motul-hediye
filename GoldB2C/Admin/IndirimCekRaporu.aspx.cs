﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Net.Mail;


public partial class Admin_IndirimCekRaporu : System.Web.UI.Page
{
    CekIslemleriBLL bllCekIslemleri = new CekIslemleriBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    DataTable dtCekBilgileri;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            drpKullaniciAdDoldur();
            drpIndirimFirmaDoldur();
        }

    }


    protected void btnGetir_Click(object sender, EventArgs e)
    {

        int kullaniciId;
        Int32.TryParse(drpKullaniciAd.SelectedValue.ToString(), out kullaniciId);

        int firmaKod;
        Int32.TryParse(drpIndirimFirma.SelectedValue.ToString(), out firmaKod);

        string tarih1 = txtTarih1.Text.ToString().Trim();
        string tarih2 = txtTarih2.Text.ToString().Trim();


        if (tarih1 == string.Empty && tarih2 == string.Empty)
        {
            tarih1 = "01.01.2000";
            tarih2 = "30.12.9999";
        }
        else if (tarih1 != string.Empty && tarih2 == string.Empty)
        {
            tarih2 = tarih1;
        }
        else if (tarih1 == string.Empty && tarih2 != string.Empty)
        {
            tarih1 = "01.01.2000";
        }

        dtCekBilgileri = bllCekIslemleri.IndirimCekleriAlanKullanicilar(kullaniciId, tarih1, tarih2, firmaKod);

        rptIndirimCekBilgileri.DataSource = dtCekBilgileri;
        rptIndirimCekBilgileri.DataBind();

    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + mesaj + "</td></tr></table></div>');</script>";
    }

    public void drpKullaniciAdDoldur()
    {
        DataTable dtUyeBilgileri = new DataTable();

        dtUyeBilgileri = bllUyelikIslemleri.GetirKullaniciAll();

        var sonuc = (from k in dtUyeBilgileri.AsEnumerable()
                     where k.Field<int>("UyelikTipi") != 1//Admin kullanıcılarını getirme!..
                     orderby k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad")
                     select new
                     {
                         KullaniciId = k.Field<int>("KullaniciId"),
                         KullaniciAdSoyad = string.Format("{0} {1}", k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad"))
                     }).ToList();


        drpKullaniciAd.DataTextField = "KullaniciAdSoyad";
        drpKullaniciAd.DataValueField = "KullaniciId";
        drpKullaniciAd.DataSource = sonuc;
        drpKullaniciAd.DataBind();
        drpKullaniciAd.Items.Insert(0, (new ListItem(string.Empty, "-1")));

    }



    protected void btnExcel_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=CiecekSepetiCekBilgileri.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";

        Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
        Response.Charset = "windows-1254";

        string header = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title></title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1254\" />\n<style>\n</style>\n</head>\n<body>\n";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        rptIndirimCekBilgileri.RenderControl(htmlWrite);
        Response.Write(header + stringWrite.ToString());
        Response.End();
    }

    public void drpIndirimFirmaDoldur()
    {
        DataTable dtFirmaBilgileri = new DataTable();

        dtFirmaBilgileri = bllCekIslemleri.IndirimCekleriFirmalarGetir();

        var sonuc = (from k in dtFirmaBilgileri.AsEnumerable()
                     orderby k.Field<string>("FirmaAd")
                     select new
                     {
                         FirmaKod = k.Field<int>("FirmaKod"),
                         FirmaAd = k.Field<string>("FirmaAd")
                     }).ToList();


        drpIndirimFirma.DataTextField = "FirmaAd";
        drpIndirimFirma.DataValueField = "FirmaKod";
        drpIndirimFirma.DataSource = sonuc;
        drpIndirimFirma.DataBind();
        drpIndirimFirma.Items.Insert(0, (new ListItem(string.Empty, "-1")));

    }
}