﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="BankaKampanyalari.aspx.cs" Inherits="Admin_BankaKampanyalari" EnableEventValidation="false"
    ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="/css/BankCampaigns.css?v=1.0" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="containerOut982">
        <%--BONUS START--%>
        <div class="bankCampaigns2">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader bonus">
                    <img src="/ImagesGld/BankCampaigns/bonus-Logo.png" class="logo" alt="Bonus Kart" />
                    <asp:DropDownList ID="dpTaksitBonus" runat="server">
                    </asp:DropDownList>
                    <asp:Image ID="ImgTaksitBonus" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:TextBox ID="txtKampanyaBaslikBonus" runat="server"></asp:TextBox>--%>
                    <span>
                        <asp:TextBox ID="txtKampanyaIcerikBonus" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </span>
                    <img src="/ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="/ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--BONUS END--%>
        <%--MAXIMUM START--%>
        <div class="bankCampaigns2">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader maximum">
                    <img src="/ImagesGld/BankCampaigns/maximum-Logo.png" class="logo" alt="Maximum Kart" />
                    <asp:DropDownList ID="dpTaksitMaximum" runat="server">
                    </asp:DropDownList>
                    <asp:Image ID="ImgTaksitMaximum" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:TextBox ID="txtKampanyaBaslikMaximum" runat="server"></asp:TextBox>--%>
                    <span>
                        <asp:TextBox ID="txtKampanyaIcerikMaximum" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </span>
                    <img src="/ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="/ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--MAXIMUM END--%>
        <%--AXESS START--%>
        <div class="bankCampaigns2">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader axess">
                    <img src="/ImagesGld/BankCampaigns/axess-Logo.png" class="logo" alt="Axess" />
                    <asp:DropDownList ID="dpTaksitAxess" runat="server">
                    </asp:DropDownList>
                    <asp:Image ID="ImgTaksitAxess" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:TextBox ID="txtKampanyaBaslikAxess" runat="server"></asp:TextBox>--%>
                    <span>
                        <asp:TextBox ID="txtKampanyaIcerikAxess" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </span>
                    <img src="/ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="/ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--AXESS END--%>
        <%--WORLD START--%>
        <div class="bankCampaigns2">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader world">
                    <img src="/ImagesGld/BankCampaigns/world-Logo.png" class="logo" alt="World Kart" />
                    <asp:DropDownList ID="dpTaksitWorld" runat="server">
                    </asp:DropDownList>
                    <asp:Image ID="ImgTaksitWorld" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:TextBox ID="txtKampanyaBaslikWorld" runat="server"></asp:TextBox>--%>
                    <span>
                        <asp:TextBox ID="txtKampanyaIcerikWorld" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </span>
                    <img src="/ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="/ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--WORLD END--%>
        <%--CARD FİNANS START--%>
        <div class="bankCampaigns2">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader cardFinans">
                    <img src="/ImagesGld/BankCampaigns/cardFinans-Logo.png" class="logo" alt="Card Finans" />
                    <asp:DropDownList ID="dpTaksitCardFinans" runat="server">
                    </asp:DropDownList>
                    <asp:Image ID="ImgTaksitCardFinans" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:TextBox ID="txtKampanyaBaslikCardFinans" runat="server"></asp:TextBox>--%>
                    <span>
                        <asp:TextBox ID="txtKampanyaIcerikCardFinans" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </span>
                    <img src="/ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="/ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--CARD FİNANS END--%>
        <%--VAKIFBANK WORLD START--%>
        <div class="bankCampaigns2">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader vakifbankWorld">
                    <img src="/ImagesGld/BankCampaigns/vakifWorld-Logo.png" class="logo" alt="Vakıfbank World Kart" />
                    <asp:DropDownList ID="dpTaksitVakifWorld" runat="server">
                    </asp:DropDownList>
                    <asp:Image ID="ImgTaksitVakifWorld" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:TextBox ID="txtKampanyaBaslikVakifWorld" runat="server"></asp:TextBox>--%>
                    <span>
                        <asp:TextBox ID="txtKampanyaIcerikVakifWorld" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </span>
                    <img src="/ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="/ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--VAKIFBANK WORLD END--%>
        <%--PARAF START--%>
        <div class="bankCampaigns2">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader paraf">
                    <img src="/ImagesGld/BankCampaigns/paraf-Logo.png" class="logo" alt="Paraf Kart" />
                    <asp:DropDownList ID="dpTaksitParaf" runat="server">
                    </asp:DropDownList>
                    <asp:Image ID="ImgTaksitParaf" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:TextBox ID="txtKampanyaBaslikParaf" runat="server"></asp:TextBox>--%>
                    <span>
                        <asp:TextBox ID="txtKampanyaIcerikParaf" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </span>
                    <img src="/ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="/ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--PARAF END--%>
        <%--ADVANTAGE START--%>
        <div class="bankCampaigns2">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader advantage">
                    <img src="/ImagesGld/BankCampaigns/advantage-Logo.png" class="logo" alt="Advantage" />
                    <asp:DropDownList ID="dpTaksitAdvantage" runat="server">
                    </asp:DropDownList>
                    <asp:Image ID="ImgTaksitAdvantage" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:TextBox ID="txtKampanyaBaslikAdvantage" runat="server"></asp:TextBox>--%>
                    <span>
                        <asp:TextBox ID="txtKampanyaIcerikAdvantage" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </span>
                    <img src="/ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="/ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--ADVANTAGE END--%>
        <%--ASYA CARD START--%>
        <div class="bankCampaigns2">
            <div class="campaignsArticleBox">
                <div class="campaignsHeader asyaCard">
                    <img src="/ImagesGld/BankCampaigns/asyaCard-Logo.png" class="logo" alt="Asya Card" />
                    <asp:DropDownList ID="dpTaksitAsyaCard" runat="server">
                    </asp:DropDownList>
                    <asp:Image ID="ImgTaksitAsyaCard" runat="server" CssClass="taksit" />
                </div>
                <div class="campaignsArticle">
                    <%--<asp:TextBox ID="txtKampanyaBaslikAsyaCard" runat="server"></asp:TextBox>--%>
                    <span>
                        <asp:TextBox ID="txtKampanyaIcerikAsyaCard" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </span>
                    <img src="/ImagesGld/BankCampaigns/bankRB.jpg" class="rb2" />
                    <img src="/ImagesGld/BankCampaigns/bankLB.jpg" class="lb2" />
                </div>
            </div>
        </div>
        <%--ASYA CARD END--%>
        <asp:Button ID="btnKaydet" runat="server" Text="Kaydet" OnClick="btnKaydet_Click"
            CssClass="aramaBTN save" />
    </div>
</asp:Content>
