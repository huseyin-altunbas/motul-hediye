﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="SiparisResimEkle.aspx.cs" Inherits="Admin_SiparisResimEkle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">

        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

    </script>
    <br />
    <span style="margin: 3px;">
        <asp:Button ID="btnPnl1Ac" runat="server" Text="Siparişe Resim Ekle / Sil" CssClass="testEtBTN"
            OnClick="btnPnl1Ac_Click" /></span><span style="margin: 3px;">
                <asp:Button ID="btnPnl2Ac" runat="server" Text="Siparişteki Resimleri Getir / Sil"
                    CssClass="testEtBTN" OnClick="btnPnl2Ac_Click" /></span>
    <br />
    <br />
    <br />
    <div style="clear: both;">
    </div>
    <asp:Panel ID="pnlSiparisResimEkleSil" runat="server">
        <div style="float: left; margin-bottom: 1px;">
            <div style="width: 130px; float: left; font-weight: bold;">
                <asp:Label ID="lblSiparisNo" runat="server" Text="Sipariş No"> 
                </asp:Label></div>
            <asp:TextBox ID="txtSiparisNo" runat="server"></asp:TextBox>
        </div>
        <div style="clear: both;">
        </div>
        <div style="float: left; margin-bottom: 1px;">
            <div style="width: 130px; float: left; font-weight: bold;">
                <asp:Label ID="lblUrunNo" runat="server" Text="Ürün No"> 
                </asp:Label></div>
            <asp:TextBox ID="txtUrunNo" runat="server"></asp:TextBox>
        </div>
        <div style="clear: both;">
        </div>
        <div style="float: left; margin-bottom: 1px;">
            <div style="width: 130px; float: left; font-weight: bold;">
                <asp:Label ID="lblResim" runat="server" Text="Resim"> 
                </asp:Label></div>
            <asp:FileUpload ID="fuImage" runat="server" />
            &nbsp; <span style="font-weight: bold;">(Geçerli olan resim uzantıları : .gif, .png,
                .jpeg, .jpg)</span>
        </div>
        <div style="clear: both;">
        </div>
        <div style="float: left; margin-bottom: 1px;">
            <asp:Button ID="btnKaydet" runat="server" Text="Kaydet" CssClass="testEtBTN" OnClick="btnKaydet_Click" />
        </div>
        <div style="clear: both;">
        </div>
        <br />
        <table class="accountRoot" style="width: 940px;">
            <tr class="accordionContainer" style="width: 940px;">
                <td>
                    <asp:Repeater ID="rptResim" runat="server" OnItemCommand="rptResim_ItemCommand">
                        <HeaderTemplate>
                            <table style="width: 700px;">
                                <tr class="ordertitle" style="border: 1px solid #ccc; width: 700px;">
                                    <th class="title4">
                                        <b>Sipariş No</b>
                                    </th>
                                    <th class="title4">
                                        <b>Ürün No</b>
                                    </th>
                                    <th class="title4">
                                        <b>Resim</b>
                                    </th>
                                    <th class="title4">
                                        <b>Sil</b>
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="orderchooes" style="border: 1px solid #ccc; width: 700px;">
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "siparisNo")%>
                                </td>
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "urunNo")%>
                                </td>
                                <td class="chooes4">
                                    <a target="_blank" href='<%#Eval("resimYolu")%>'>
                                        <img src='<%#Eval("resimYolu")%>' width="100px" height="100px" />
                                    </a>
                                </td>
                                <td class="chooes4">
                                    <asp:Button ID="btnDeleteImage" runat="server" CommandName="DeleteImage" OnClientClick='javascript:return confirm("Silmek istediğinize emin misiniz?")'
                                        CommandArgument='<%#DataBinder.Eval(Container.DataItem, "siparisNo")+ ";" +DataBinder.Eval(Container.DataItem, "urunNo")+ ";" +DataBinder.Eval(Container.DataItem, "resimYolu")%>'
                                        Text="Sil" CssClass="testEtBTN" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlSiparisResimGetirSil" runat="server" Visible="false">
        <div style="float: left; margin: 5px;">
            <div style="width: 130px; float: left; font-weight: bold;">
                <asp:Label ID="lblSipNo" runat="server" Text="Sipariş No"> 
                </asp:Label></div>
            <asp:TextBox ID="txtSipNo" runat="server"></asp:TextBox>
        </div>
        <div style="float: left; margin: 5px;">
            <asp:Button ID="btnGetir" runat="server" Text="Getir" CssClass="testEtBTN" OnClick="btnGetir_Click" />
        </div>
        <div style="clear: both;">
        </div>
        <table class="accountRoot" style="width: 940px;">
            <tr class="accordionContainer" style="width: 940px;">
                <td>
                    <asp:Repeater ID="rptGetirResim" runat="server" OnItemCommand="rptGetirResim_ItemCommand">
                        <HeaderTemplate>
                            <table style="width: 700px;">
                                <tr class="ordertitle" style="border: 1px solid #ccc; width: 700px;">
                                    <th class="title4">
                                        <b>Sipariş No</b>
                                    </th>
                                    <th class="title4">
                                        <b>Ürün No</b>
                                    </th>
                                    <th class="title4">
                                        <b>Resim</b>
                                    </th>
                                    <th class="title4">
                                        <b>Sil</b>
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="orderchooes" style="border: 1px solid #ccc; width: 700px;">
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "siparisNo")%>
                                </td>
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "urunNo")%>
                                </td>
                                <td class="chooes4">
                                    <a target="_blank" href='<%#Eval("resimYolu")%>'>
                                        <img src='<%#Eval("resimYolu")%>' width="100px" height="100px" />
                                    </a>
                                </td>
                                <td class="chooes4">
                                    <asp:Button ID="btnDeleteImage" runat="server" CommandName="DeleteImage" OnClientClick='javascript:return confirm("Silmek istediğinize emin misiniz?")'
                                        CommandArgument='<%#DataBinder.Eval(Container.DataItem, "siparisNo")+ ";" +DataBinder.Eval(Container.DataItem, "urunNo")+ ";" +DataBinder.Eval(Container.DataItem, "resimYolu")%>'
                                        Text="Sil" CssClass="testEtBTN" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
