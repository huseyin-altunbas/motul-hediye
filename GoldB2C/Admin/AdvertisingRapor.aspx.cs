﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;

public partial class Admin_AdvertisingRapor : System.Web.UI.Page
{
    AdminGenelBLL bllAdminGenel = new AdminGenelBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AdvertisingMasterListele();

            int AdvertisingMasterId = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["AdvertisingMasterId"]) && int.TryParse(Request.QueryString["AdvertisingMasterId"], out AdvertisingMasterId))
            {
                TiklanmaRaporlariniGetir(AdvertisingMasterId);
            }
        }
    }

    private void TiklanmaRaporlariniGetir(int AdvertisingMasterId)
    {
        rptAdvertisingMasterRapor.DataSource = bllAdminGenel.GetirAdvertisingLogRapor(AdvertisingMasterId);
        rptAdvertisingMasterRapor.DataBind();

        rptAdvertisingDetayRapor.DataSource = bllAdminGenel.GetirAdvertisingDetayLogRapor(AdvertisingMasterId);
        rptAdvertisingDetayRapor.DataBind();
    }

    private void AdvertisingMasterListele()
    {
        rptAdvertisingMaster.DataSource = bllAdminGenel.GetirAdvertisingMaster();
        rptAdvertisingMaster.DataBind();
    }


}