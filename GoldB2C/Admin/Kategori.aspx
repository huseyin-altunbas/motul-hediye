﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="Kategori.aspx.cs" Inherits="Admin_Menu" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .extraButtons
        {
            position: relative;
            width: 386px;
            bottom: 0;
        }
        [type=text]
        {
            padding: 7px 7px;
            border: 1px solid #ccc;
            border-radius: 5px;
            width: 200px;
        }
        select
        {
            padding: 7px 7px;
            border: 1px solid #ccc;
            border-radius: 5px;
            width: 285px;
        }
        td
        {
            vertical-align: middle;
        }
        .testEtBTN
        {
            padding: 10px 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            width: 150px;
            height: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">

        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

    </script>
    <br />
    <asp:Button ID="bntGetMenuItems" runat="server" CssClass="testEtBTN" Text="Kategori Güncelle"
        OnClick="bntGetMenuItems_Click" />
    <asp:Button ID="btnAddItemDiv" runat="server" CssClass="testEtBTN" Text="Marka Ekle"
        OnClick="btnAddItemDiv_Click" />
    <asp:Button ID="btnUpdateMenuItemDiv" runat="server" CssClass="testEtBTN" Text="Marka Güncelle"
        OnClick="btnUpdateMenuItemDiv_Click" />
    <asp:Button ID="btnDeleteItemDiv" runat="server" CssClass="testEtBTN" Text="Marka Sil"
        OnClick="btnDeleteItemDiv_Click" />
    <asp:Button ID="btnAllMenu" runat="server" CssClass="testEtBTN" Text="Marka Göster"
        OnClick="btnAllMenu_Click" />
    <asp:Button ID="btnClear" runat="server" CssClass="testEtBTN" Text="Sayfayı Temizle"
        OnClick="btnClear_Click" />
    <asp:Panel ID="pnlAllDiv" runat="server">
        <br />
        <br />
        <b>Menüleri Göster</b><br />
        <table width="940">
            <tr>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                    Kategori:
                </td>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                    <asp:DropDownList ID="drpKategori" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                    <asp:Button ID="btnAllDiv" runat="server" Text="Getir" CssClass="testEtBTN" OnClick="btnAllDiv_Click" />
                </td>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                </td>
            </tr>
        </table>
        <div id="navigation" style="background: none;">
            <ul class="dropmenu">
                <li>
                    <div>
                        <asp:Literal ID="ltrAllDiv" runat="server"></asp:Literal>
                    </div>
                </li>
            </ul>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlUpdateMenuItems" runat="server">
        <br />
        <br />
        <b>Kategori Güncelle</b><br />
        <br />
        <div>
            <b>Kategori Resmi Ekle</b>
            <br />
            <asp:FileUpload ID="fuKategoriImage" runat="server" />&nbsp;
            <asp:Button ID="btnKategoriImage" runat="server" CssClass="testEtBTN" Text="Kategori Resmi Kaydet"
                OnClick="btnKategoriImage_Click" />
        </div>
        <br />
        <br />
        <asp:Repeater ID="rptMenuItems" runat="server" OnItemCommand="rptMenuItems_ItemCommand">
            <HeaderTemplate>
                <table width="940">
                    <tr>
                        <th class="baslikUyeGondertd">
                            <b>Kategori Adı</b>&nbsp;&nbsp;
                        </th>
                        <th class="baslikUyeGondertd">
                            <b>Kategori Linki</b>&nbsp;&nbsp;
                        </th>
                        <th class="baslikUyeGondertd">
                            <b>Kategori Sırası</b>&nbsp;&nbsp;
                        </th>
                        <th class="baslikUyeGondertd">
                            <b>Kategori Resmi</b>&nbsp;&nbsp;
                        </th>
                        <th class="baslikUyeGondertd">
                            <b>Güncelle</b>&nbsp;&nbsp;
                        </th>
                        <th class="baslikUyeGondertd">
                            <b>Kaydet</b>&nbsp;&nbsp;
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr id="trRow" runat="server">
                    <td style="text-align: left; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                        margin-bottom: 2px; padding-top: 5px;">
                        <asp:TextBox ID="txtItemName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ItemName")%>'
                            Enabled="false"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td style="text-align: left; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                        margin-bottom: 2px; padding-top: 5px;">
                        <asp:TextBox ID="txtItemUrl" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ItemUrl")%>'
                            Enabled="false"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td style="text-align: left; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                        margin-bottom: 2px; padding-top: 5px;">
                        <asp:TextBox ID="txtItemOrder" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ItemOrder")%>'
                            Enabled="false"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td style="text-align: left; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                        margin-bottom: 2px; padding-top: 5px;">
                        <asp:TextBox ID="txtItemImage" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ItemImage")%>'
                            Enabled="false"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                        padding-top: 5px;">
                        <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Güncelle" CssClass="testEtBTN" />
                    </td>
                    <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                        padding-top: 5px;">
                        <asp:Button ID="btnUpdateSave" runat="server" CommandName="UpdateSave" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "ItemId")%>'
                            Text="Kaydet" CssClass="testEtBTN" />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <br />
    <br />
    <asp:Panel ID="pnlAddDiv" runat="server">
        <br />
        <b>Marka Ekle</b><br />
        <table width="940">
            <tr>
                <td style="text-align: left; width: 150px; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    Başlık:
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                </td>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                    <b>Örnek: Asus</b>
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 150px; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    Dosya Yolu:
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:TextBox ID="txtHrf" runat="server"></asp:TextBox>
                </td>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                    <b>Örnek: /L55555/Asus</b>
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 150px; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    Resim Klasörü:
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:DropDownList ID="drpImageFolder" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpImageFolder_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                    <b>Örnek: Kategori</b>
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 150px; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    Menu sırası:
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:TextBox ID="txtMenuNumber" runat="server"></asp:TextBox>
                </td>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                    <b>(2)</b>
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 150px; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    Resim:
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:FileUpload ID="fuImage" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 150px; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:Button ID="btnDivAdd" runat="server" Text="Ekle" CssClass="testEtBTN" OnClick="btnDivAdd_Click" />
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 150px; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;" rowspan="2">
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                </td>
            </tr>
        </table>
        <div id="navigation" style="background: none;">
            <ul class="dropmenu">
                <li>
                    <div>
                        <asp:Literal ID="ltrNewDiv" runat="server"></asp:Literal>
                    </div>
                </li>
            </ul>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDeleteDiv" runat="server">
        <br />
        <b>Marka Sil</b><br />
        <table width="940">
            <tr>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                    Kategori:
                </td>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                    <asp:DropDownList ID="drpKategoriDeleteDiv" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                    <asp:Button ID="btnGetirDeleteDiv" runat="server" Text="Getir" CssClass="testEtBTN"
                        OnClick="btnGetirDeleteDiv_Click" />
                </td>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                </td>
            </tr>
        </table>
        <asp:Repeater ID="rptDeleteDiv" runat="server" OnItemCommand="rptDeleteDiv_ItemCommand">
            <HeaderTemplate>
                <table width="940">
                    <tr>
                        <th class="baslikUyeGondertd">
                            <b>Item</b>&nbsp;&nbsp;
                        </th>
                        <th class="baslikUyeGondertd">
                            <b>Sil</b>&nbsp;&nbsp;
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="text-align: left; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                        margin-bottom: 2px; padding-top: 5px;">
                        <asp:Literal ID="ltrDeleteDiv" runat="server" Text='<%# Container.DataItem %>'></asp:Literal>
                    </td>
                    <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                        padding-top: 5px;">
                        <asp:Button ID="btnDeleteDiv" runat="server" CommandName="DeleteDiv" OnClientClick='javascript:return confirm("Silmek istediğinize emin misiniz?")'
                            Text="Sil" CssClass="testEtBTN" />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="pnlUpdateDiv" runat="server">
        <br />
        <b>Marka Güncelle</b><br />
        <table width="940">
            <tr>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                    Kategori:
                </td>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                    <asp:DropDownList ID="drpKategoriUpdateDiv" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                    <asp:Button ID="btnGetirUpdateDiv" runat="server" Text="Getir" CssClass="testEtBTN"
                        OnClick="btnGetirUpdateDiv_Click" />
                </td>
                <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                    padding-top: 5px;">
                </td>
            </tr>
        </table>
        <asp:Repeater ID="rptUpdateDiv" runat="server" OnItemCommand="rptUpdateDiv_ItemCommand">
            <HeaderTemplate>
                <table width="940">
                    <tr>
                        <th class="baslikUyeGondertd">
                            <b>Item</b>&nbsp;&nbsp;
                        </th>
                        <th class="baslikUyeGondertd">
                            <b>Text</b>&nbsp;&nbsp;
                        </th>
                        <th class="baslikUyeGondertd">
                            <b>Güncelle</b>&nbsp;&nbsp;
                        </th>
                        <th class="baslikUyeGondertd">
                            <b>Kaydet</b>&nbsp;&nbsp;
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="text-align: left; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                        margin-bottom: 2px; padding-top: 5px;">
                        <asp:Literal ID="ltrUpdateDiv" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Divs")%>'></asp:Literal>
                    </td>
                    <td style="text-align: center; border-bottom: 1px dashed #ccc; padding-bottom: 0px;
                        margin-bottom: 2px; padding-top: 5px; width: 350px;">
                        <asp:TextBox ID="txtUpdateLink" Enabled="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Link")%>'></asp:TextBox>
                    </td>
                    <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                        padding-top: 5px;">
                        <asp:Button ID="btnUpdateDiv" runat="server" CommandName="UpdateDiv" Text="Güncelle"
                            CssClass="testEtBTN" />
                    </td>
                    <td style="border-bottom: 1px dashed #ccc; padding-bottom: 0px; margin-bottom: 2px;
                        padding-top: 5px;">
                        <asp:Button ID="btnUpdateDivSave" runat="server" CommandName="UpdateDivSave" OnClientClick='javascript:return confirm("Güncellemek istediğinize emin misiniz?")'
                            Text="Kaydet" CssClass="testEtBTN" />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
</asp:Content>
