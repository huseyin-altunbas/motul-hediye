﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="Puan-Kayit-Import.aspx.cs" Inherits="Admin_UyeKayit_Import" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/goldForms.css?v=2" rel="stylesheet" type="text/css" />
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">
        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(":input").attr("autocomplete", "off");
        });
    </script>
    <div>
        <!-- fake fields are a workaround for chrome autofill getting the wrong fields -->
        <input style="display: none" type="text" name="fakeusernameremembered" />
        <input style="display: none" type="password" name="fakepasswordremembered" />
    </div>
    <!-- loginforms -->
    <div class="w764 mAuto clearfix mb50 fontTre">
        <div class="formTxt w764 fontSz13 color4e ml28 strong">
            Toplu Puan Yükleme Formu
           
        </div>
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350" style="width: 100%;">
            <div class="fleft w350 h23 strong" style="width: 100%;">
                <span class="fleft mt3 fontSz13">Lütfen Puan listesinin bulunduğu CSV dosyasını seçtikten sonra KAYDET butonuna tıklayın!</span>
            </div>

             <div class="fleft w350 h23 strong" style="width: 100%;">
                <a href="/admin/download/ornek-puan.xlsx"> Örnek Excel dosyasını indirmek için tıklayınız. </a>
            </div>

            <div class="fleft w230 clearfix" style="width: 100%;">
                <asp:FileUpload ID="flImportCSV" CssClass="w230" runat="server" />
            </div>
        </div>
        <!-- input -->
        <!-- button -->
        <div class="fleft h45 color4e mt30 ml28 w350">
          


             <asp:Button ID="Button1" runat="server" Text="Kaydet" OnClick="lnkUyeKayit_Click" />

        </div>
        <!-- button -->
    </div>
    <!-- loginforms -->
</asp:Content>
