﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using System.IO;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Xml;


public partial class Admin_Menu : System.Web.UI.Page
{
    MenuItemIslemleriBLL bllMenuItem = new MenuItemIslemleriBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            drpBind();

            pnlAddDiv.Visible = false;
            pnlAllDiv.Visible = false;
            pnlUpdateMenuItems.Visible = false;
            pnlDeleteDiv.Visible = false;
            pnlUpdateDiv.Visible = false;
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Reset();
    }

    public void Reset()
    {
        rptMenuItems.DataSource = null;
        rptMenuItems.DataBind();
        ltrAllDiv.Text = "";
        ltrNewDiv.Text = "";
        rptDeleteDiv.DataSource = null;
        rptDeleteDiv.DataBind();
        rptUpdateDiv.DataSource = null;
        rptUpdateDiv.DataBind();
    }

    protected void bntGetMenuItems_Click(object sender, EventArgs e)
    {
        Reset();
        pnlAddDiv.Visible = false;
        pnlAllDiv.Visible = false;
        pnlUpdateMenuItems.Visible = true;
        pnlDeleteDiv.Visible = false;
        pnlUpdateDiv.Visible = false;

        rptMenuItems.DataSource = bllMenuItem.GetirMenuItems();
        rptMenuItems.DataBind();
    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + mesaj + "</td></tr></table></div>');</script>";
    }

    protected void btnDivAdd_Click(object sender, EventArgs e)
    {
        string folder = "";

        if (fuImage.HasFile && txtHrf.Text != string.Empty && txtTitle.Text != string.Empty && drpImageFolder.SelectedValue != string.Empty && txtMenuNumber.Text != string.Empty)
        {
            switch (drpImageFolder.SelectedValue)
            {
                case "item_1": folder = "ustaKulubuUrunleri";
                    break;
                case "item_2": folder = "uygulamaAletleri";
                    break;
                case "item_3": folder = "evElektronigi";
                    break;
                case "item_4": folder = "beyazEsya";
                    break;
                case "item_5": folder = "evAletleri";
                    break;
                case "item_6": folder = "hirdavat";
                    break;
                case "item_7": folder = "kisiselBakim";
                    break;
                case "item_8": folder = "digerUrunler";
                    break;
                default:
                    folder = "";
                    break;
            }

            string createPath = "~/imagesgld/navigation/" + folder;

            string path = HttpContext.Current.ApplicationInstance.Server.MapPath(createPath);
            string fn = System.IO.Path.GetFileName(fuImage.PostedFile.FileName);
            fuImage.PostedFile.SaveAs(System.IO.Path.Combine(path, fn));


            string divSrc = "/imagesgld/navigation/" + folder + "/" + fn;
            string divs = "";

            divs = bllMenuItem.GetirMenuItems(drpImageFolder.SelectedValue.ToString()).Rows[0]["ItemHtml"].ToString();

            string newDivs = "<divs>" + divs + "</divs>";
            if (newDivs.Contains("&amp;"))
                newDivs = newDivs.Replace("&amp;", "&");
            if (newDivs.Contains("&amp"))
                newDivs = newDivs.Replace("&amp", "&");//alt satırda &amp; -> &amp;amp; olmaması için.
            newDivs = newDivs.Replace("&", "&amp;");


            XDocument xDoc = new XDocument();
            var xmlDivs = XElement.Parse(newDivs);
            xDoc.Add(xmlDivs);

            List<string> listDivs = new List<string>();

            foreach (XElement element in xDoc.Descendants("div"))
            {
                listDivs.Add(element.ToString());
            }

            //string extraButtons = listDivs[listDivs.Count - 1].ToString();
            //listDivs.RemoveAt(listDivs.Count - 1);
            //listDivs.Add(DivOlustur(txtTitle.Text.ToString(), txtHrf.Text.ToString(), divSrc));
            //listDivs.Add(extraButtons);

            int menuNumber = Convert.ToInt32(txtMenuNumber.Text.ToString());
            if (menuNumber > 0 && menuNumber <= listDivs.Count)
            {
                listDivs.Insert((menuNumber - 1), DivOlustur(txtTitle.Text.ToString(), txtHrf.Text.ToString(), divSrc).Replace("&", "&amp;"));
            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("1 ile " + listDivs.Count.ToString() + " arasında sayı giriniz"));
            }

            StringBuilder sbDivs = new StringBuilder();
            foreach (string strItems in listDivs)
            {
                sbDivs.Append(strItems.ToString());
            }

            int deger = 0;
            try
            {
                deger = bllMenuItem.GuncelleItemHtml(drpImageFolder.SelectedValue.ToString(), sbDivs.ToString().Replace("&amp;", "&"));
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Hata oluştu"));
            }

            if (deger == 1)
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("İşlem başarılı."));
                ltrNewDiv.Text = bllMenuItem.GetirMenuItems(drpImageFolder.SelectedValue.ToString()).Rows[0]["ItemHtml"].ToString();
            }

        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("İlgili alanları tekrar kontrol  ediniz."));
        }
    }

    public string DivOlustur(string title, string href, string src)
    {
        return "<div class=\"category\">\r\n  <a title=" + "\"" + title + "\"" + " href=" + "\"" + href + "\"" + " class=\"img\">\r\n    <img src=" + "\"" + src + "\"" + " alt=" + "\"" + title + "\"" + " width=\"120\" height=\"120\" />\r\n  </a>\r\n  <a title=" + "\"" + title + "\"" + " href=" + "\"" + href + "\"" + " class=\"categoryName\">" + title + "</a>\r\n</div>";
    }

    protected void rptMenuItems_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int sonuc = 0;
        TextBox itemName = (e.Item.FindControl("txtItemName") as TextBox);
        TextBox itemUrl = (e.Item.FindControl("txtItemUrl") as TextBox);

        if (e.CommandName == "Update")
        {
            itemName.Enabled = true;
            itemUrl.Enabled = true;
            itemName.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF6B6B");
            itemUrl.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF6B6B");
            //HtmlTableRow rowAll = (HtmlTableRow)e.Item.FindControl("trRow");
            //rowAll.Attributes.Add("style", "background-color:#FF6B6B;");
        }

        if (e.CommandName == "UpdateSave")
        {
            sonuc = bllMenuItem.GuncelleItemNameItemUrl(e.CommandArgument.ToString(), itemName.Text.ToString(), itemUrl.Text.ToString());
        }

        if (sonuc == 1)
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Güncelleme işlemi başarılı."));
            rptMenuItems.DataSource = null;
            try
            {
                drpBind();
                rptMenuItems.DataSource = bllMenuItem.GetirMenuItems();
                rptMenuItems.DataBind();
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Hata oluştu"));
            }
        }

    }
    protected void btnAddItemDiv_Click(object sender, EventArgs e)
    {
        Reset();
        pnlAddDiv.Visible = true;
        pnlUpdateMenuItems.Visible = false;
        pnlAllDiv.Visible = false;
        pnlDeleteDiv.Visible = false;
        pnlUpdateDiv.Visible = false;

    }
    protected void btnAllMenu_Click(object sender, EventArgs e)
    {
        Reset();
        pnlAddDiv.Visible = false;
        pnlUpdateMenuItems.Visible = false;
        pnlAllDiv.Visible = true;
        pnlDeleteDiv.Visible = false;
        pnlUpdateDiv.Visible = false;
    }
    protected void btnAllDiv_Click(object sender, EventArgs e)
    {
        Reset();
        string divs = "";
        if (drpKategori.SelectedValue.ToString() != string.Empty)
        {
            divs = bllMenuItem.GetirMenuItems(drpKategori.SelectedValue.ToString()).Rows[0]["ItemHtml"].ToString();
            ltrAllDiv.Text = divs;
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("İlgili alanları tekrar kontrol  ediniz."));
        }
    }


    protected void rptDeleteDiv_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        List<string> listOfDivs = ListofDivs(drpKategoriDeleteDiv.SelectedValue.ToString());
        if (e.CommandName == "DeleteDiv")
        {
            int itemIndex = e.Item.ItemIndex;

            XDocument xDoc;
            xDoc = new XDocument();
            var xmlDivs = XElement.Parse(listOfDivs[itemIndex]);
            xDoc.Add(xmlDivs);

            int extraButtonsCount = xDoc.Descendants("div").Where(element => element.Attribute("class").Value == "extraButtons").Count();

            if (extraButtonsCount == 0)//Tüm ürünleri inceleyi silme...
            {
                listOfDivs.RemoveAt(itemIndex);

                StringBuilder sbDivs = new StringBuilder();
                foreach (string strItems in listOfDivs)
                {
                    sbDivs.Append(strItems.ToString());
                }

                int result = 0;
                try
                {
                    result = bllMenuItem.GuncelleItemHtml(drpKategoriDeleteDiv.SelectedValue.ToString(), sbDivs.ToString().Replace("&amp;", "&"));
                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Hata oluştu."));
                }

                if (result == 1)
                {
                    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Silme işlemi başarılı."));
                    listOfDivs = ListofDivs(drpKategoriDeleteDiv.SelectedValue.ToString());
                    rptDeleteDiv.DataSource = listOfDivs;
                    rptDeleteDiv.DataBind();
                }
                else
                {
                    listOfDivs = ListofDivs(drpKategoriDeleteDiv.SelectedValue.ToString());
                    rptDeleteDiv.DataSource = listOfDivs;
                    rptDeleteDiv.DataBind();
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Tüm Ürünleri İncele linki silinemez."));
            }
        }
    }

    protected void btnGetirDeleteDiv_Click(object sender, EventArgs e)
    {
        Reset();
        if (!string.IsNullOrEmpty(drpKategoriDeleteDiv.SelectedValue))
        {
            List<string> listOfDivs = ListofDivs(drpKategoriDeleteDiv.SelectedValue.ToString());
            rptDeleteDiv.DataSource = listOfDivs;
            rptDeleteDiv.DataBind();
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("İlgili alanları tekrar kontrol  ediniz."));
        }
    }

    public List<string> ListofDivs(string itemId)
    {
        string divs = bllMenuItem.GetirMenuItems(itemId).Rows[0]["ItemHtml"].ToString();

        string newDivs = "<divs>" + divs + "</divs>";
        if (newDivs.Contains("&amp;"))
            newDivs = newDivs.Replace("&amp;", "&");
        if (newDivs.Contains("&amp"))
            newDivs = newDivs.Replace("&amp", "&");//alt satırda &amp; -> &amp;amp; olmaması için.
        newDivs = newDivs.Replace("&", "&amp;");


        XDocument xDoc = new XDocument();
        var xmlDivs = XElement.Parse(newDivs);
        xDoc.Add(xmlDivs);

        List<string> listDivs = new List<string>();

        foreach (XElement element in xDoc.Descendants("div"))
        {
            listDivs.Add(element.ToString());
        }

        return listDivs;
    }
    protected void btnDeleteItemDiv_Click(object sender, EventArgs e)
    {
        Reset();
        pnlAddDiv.Visible = false;
        pnlUpdateMenuItems.Visible = false;
        pnlAllDiv.Visible = false;
        pnlDeleteDiv.Visible = true;
        pnlUpdateDiv.Visible = false;
    }
    protected void drpImageFolder_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(drpImageFolder.SelectedValue))
        {
            string divs = bllMenuItem.GetirMenuItems(drpImageFolder.SelectedValue.ToString()).Rows[0]["ItemHtml"].ToString();
            ltrNewDiv.Text = divs;
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("İlgili alanları tekrar kontrol  ediniz."));
        }
    }

    public void drpBind()
    {
        DataTable dtGetMenu = new DataTable();
        dtGetMenu = bllMenuItem.GetirMenuItems();

        drpImageFolder.DataTextField = "ItemName";
        drpImageFolder.DataValueField = "ItemId";
        drpImageFolder.DataSource = dtGetMenu;
        drpImageFolder.DataBind();
        drpImageFolder.Items.Insert(0, (new ListItem(string.Empty, string.Empty)));

        drpKategori.DataTextField = "ItemName";
        drpKategori.DataValueField = "ItemId";
        drpKategori.DataSource = dtGetMenu;
        drpKategori.DataBind();
        drpKategori.Items.Insert(0, (new ListItem(string.Empty, string.Empty)));

        drpKategoriDeleteDiv.DataTextField = "ItemName";
        drpKategoriDeleteDiv.DataValueField = "ItemId";
        drpKategoriDeleteDiv.DataSource = dtGetMenu;
        drpKategoriDeleteDiv.DataBind();
        drpKategoriDeleteDiv.Items.Insert(0, (new ListItem(string.Empty, string.Empty)));

        drpKategoriUpdateDiv.DataTextField = "ItemName";
        drpKategoriUpdateDiv.DataValueField = "ItemId";
        drpKategoriUpdateDiv.DataSource = dtGetMenu;
        drpKategoriUpdateDiv.DataBind();
        drpKategoriUpdateDiv.Items.Insert(0, (new ListItem(string.Empty, string.Empty)));
    }
    protected void btnUpdateMenuItemDiv_Click(object sender, EventArgs e)
    {
        Reset();
        pnlAddDiv.Visible = false;
        pnlUpdateMenuItems.Visible = false;
        pnlAllDiv.Visible = false;
        pnlDeleteDiv.Visible = false;
        pnlUpdateDiv.Visible = true;
    }
    protected void btnGetirUpdateDiv_Click(object sender, EventArgs e)
    {
        Reset();
        if (!string.IsNullOrEmpty(drpKategoriUpdateDiv.SelectedValue))
        {
            List<string> listOfDivs = ListofDivs(drpKategoriUpdateDiv.SelectedValue.ToString());

            DataTable dtUpdateDiv = new DataTable();
            dtUpdateDiv.Columns.AddRange(new DataColumn[] { new DataColumn("Divs", typeof(string)), new DataColumn("Link", typeof(string)) });

            XDocument xDoc;

            foreach (string item in listOfDivs)
            {
                xDoc = new XDocument();
                var xmlDivs = XElement.Parse(item);
                xDoc.Add(xmlDivs);

                IEnumerable<string> links = xDoc.Descendants("a").Select(element => element.Attribute("href").Value);
                string link1 = links.FirstOrDefault().ToString();

                dtUpdateDiv.Rows.Add(new string[] { item, link1 });

            }

            rptUpdateDiv.DataSource = dtUpdateDiv;
            rptUpdateDiv.DataBind();
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("İlgili alanları tekrar kontrol  ediniz."));
        }
    }
    protected void rptUpdateDiv_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int itemIndex = e.Item.ItemIndex;

        TextBox _txtUpdateLink = (TextBox)e.Item.FindControl("txtUpdateLink");
        string updateLink = _txtUpdateLink.Text.Trim();

        List<string> listOfDivs = ListofDivs(drpKategoriUpdateDiv.SelectedValue.ToString());

        if (e.CommandName == "UpdateDiv")
        {
            _txtUpdateLink.Enabled = true;
            _txtUpdateLink.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF6B6B");

        }
        else if (e.CommandName == "UpdateDivSave")
        {
            XDocument xDoc;
            xDoc = new XDocument();
            var xmlDivs = XElement.Parse(listOfDivs[itemIndex]);
            xDoc.Add(xmlDivs);

            foreach (XElement element in xDoc.Descendants("a"))
            {
                element.Attribute("href").Value = updateLink;
            }

            listOfDivs[itemIndex] = xmlDivs.ToString();


            StringBuilder sbDivs = new StringBuilder();
            foreach (string strItems in listOfDivs)
            {
                sbDivs.Append(strItems.ToString());
            }

            int result = 0;
            try
            {
                result = bllMenuItem.GuncelleItemHtml(drpKategoriUpdateDiv.SelectedValue.ToString(), sbDivs.ToString().Replace("&amp;", "&"));
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Hata oluştu."));
            }


            DataTable dtUpdateDiv = new DataTable();
            dtUpdateDiv.Columns.AddRange(new DataColumn[] { new DataColumn("Divs", typeof(string)), new DataColumn("Link", typeof(string)) });

            XDocument xDoc2;

            foreach (string item in listOfDivs)
            {
                xDoc2 = new XDocument();
                var xmlDivs2 = XElement.Parse(item);
                xDoc2.Add(xmlDivs2);

                IEnumerable<string> links = xDoc2.Descendants("a").Select(element => element.Attribute("href").Value);
                string link1 = links.FirstOrDefault().ToString();

                dtUpdateDiv.Rows.Add(new string[] { item, link1 });

            }

            if (result == 1)
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Güncelleme işlemi başarılı."));
                listOfDivs = ListofDivs(drpKategoriUpdateDiv.SelectedValue.ToString());
                rptUpdateDiv.DataSource = dtUpdateDiv;
                rptUpdateDiv.DataBind();
            }
            else
            {
                listOfDivs = ListofDivs(drpKategoriUpdateDiv.SelectedValue.ToString());
                rptUpdateDiv.DataSource = dtUpdateDiv;
                rptUpdateDiv.DataBind();
            }
        }

    }
}