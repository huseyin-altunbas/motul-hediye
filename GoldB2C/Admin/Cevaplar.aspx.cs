﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.IO;
using Goldb2cInfrastructure;

public partial class Admin_AnketGoster : System.Web.UI.Page
{
    SoruCevapIslemleriBLL bllSoruCevapIslemleri = new SoruCevapIslemleriBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["SoruId"] != null)
            {
                int soruId;
                Int32.TryParse(Request.QueryString["SoruId"].ToString(), out soruId);
                if (soruId > 0)
                {
                    var vSoru = (from s in bllSoruCevapIslemleri.SoruGetir().AsEnumerable()
                                 where s.Field<int>("SoruId") == soruId
                                 select new
                                 {
                                     SoruId = s.Field<int>("SoruId"),
                                     Soru = s.Field<string>("Soru"),
                                     SiparisId = s.Field<int>("SiparisId"),
                                     KullaniciAdSoyad = s.Field<string>("KullaniciAdSoyad")
                                 }).SingleOrDefault();

                    if (vSoru != null)
                    {
                        lblSoruId.Text = vSoru.SoruId.ToString();
                        lblSiparisId.Text = vSoru.SiparisId.ToString();
                        lblKullaniciAdSoyad.Text = vSoru.KullaniciAdSoyad;
                        lblSoru.Text = vSoru.Soru;

                        DataTable dt = bllSoruCevapIslemleri.CevapGetir(soruId);
                        rptCevaplar.DataSource = dt;

                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["DosyaYolu"] != null && dr["DosyaYolu"].ToString() != string.Empty)
                            {
                                string path = HttpContext.Current.Server.MapPath(dr["DosyaYolu"].ToString());
                                if (Directory.Exists(path))
                                {
                                    DirectoryInfo d = new DirectoryInfo(path);
                                    string files = string.Empty;
                                    foreach (FileInfo file in d.GetFiles())
                                    {
                                        if (file.Name.Contains("="))
                                        {
                                            string _fileName = file.Name.Split('=')[1].ToString();
                                            string[] folder = dr["DosyaYolu"].ToString().Split('/');
                                            string _folderId = folder[folder.Length - 1].ToString();
                                            files += "<div style=\"margin:10px;font-size:12px;\"><a style=\"margin:10px;color:Red;font-size:12px;\" target=\"_blank\" href=\"../TicketImage/TicketAnswers/" + _folderId + "/" + file.Name + "\"> Önizleme</a> - <span>" + _fileName + "</span></div>";
                                        }
                                    }

                                    dr["DosyaYolu"] = files.ToString();
                                }
                                else
                                {
                                    dr["DosyaYolu"] = string.Empty;
                                }

                            }

                        }

                        rptCevaplar.DataBind();
                    }
                    else
                        Response.Redirect("~/Admin/Sorular.aspx");
                }
                else
                    Response.Redirect("~/Admin/Sorular.aspx");
            }
            else
                Response.Redirect("~/Admin/Sorular.aspx");
        }
    }
    protected void lnkKaydet_Click(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {
            Kullanici kullanici = (Kullanici)Session["Kullanici"];
            string cevap = InputSafety.SecureString(txtCevap.Text.Trim());

            string filePath = "~/TicketImage/TicketAnswers/" + hdnId.Value;

            if (cevap.Length <= 2000)
            {
                if (Request.QueryString["SoruId"] != null)
                {
                    int soruId;
                    Int32.TryParse(Request.QueryString["SoruId"].ToString(), out soruId);
                    if (soruId > 0)
                    {
                        int sonuc = bllSoruCevapIslemleri.CevapKaydet(soruId, cevap, kullanici.KullaniciId, filePath);

                        if (sonuc > 0)
                        {
                            hdnId.Value = "";

                            try
                            {
                                var vSoru = (from s in bllSoruCevapIslemleri.SoruGetir().AsEnumerable()
                                             where s.Field<int>("SoruId") == soruId
                                             select new { Soru = s.Field<string>("Soru") }).SingleOrDefault();

                                if (vSoru != null)
                                {
                                    CevapMailYolla(kullanici.KullaniciAd + " " + kullanici.KullaniciSoyad + " " + "tarafından" + "<br/>" + "<br/>"
                                        + vSoru.Soru + " " + "<br/>" + "<br/>" + "sorusuna cevap verilmiştir." + "<br/>" + "<br/>"
                                        + "Cevap : " + "<br/>"
                                        + cevap.ToString() + "<br/>", sonuc, soruId);
                                }
                            }
                            catch (Exception)
                            {
                                //
                            }

                            Response.Redirect("~/Admin/Cevaplar.aspx?SoruId=" + soruId);
                        }
                        else if (sonuc == -1)
                            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Kapatılan soruya cevap veremezsiniz."));
                    }
                }
            }
            else
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("En fazla 2000 karakter girebilirsiniz"));

        }
        else
        {
            Response.Redirect("~/Admin/UyeGirisi.aspx");
        }
    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + mesaj + "</td></tr></table></div>');</script>";
    }
    protected void lnkSorularaGit_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Admin/Sorular.aspx");
    }
    protected void lnkCevaplariYenile_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["SoruId"] != null)
        {
            int soruId;
            Int32.TryParse(Request.QueryString["SoruId"].ToString(), out soruId);
            if (soruId > 0)
            {
                Response.Redirect("~/Admin/Cevaplar.aspx?SoruId=" + soruId);
            }
        }
    }
    private static void CevapMailYolla(string message, int cevapId, int soruId)
    {
        BannerBLL bllBanner = new BannerBLL();
        SoruCevapIslemleriBLL bllSoruCevapIslemleri = new SoruCevapIslemleriBLL();
        string strHTML = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/MailSablon/SoruMail.htm"));

        strHTML = strHTML.Replace("#Mesaj#", message);

        SendEmail mail = new SendEmail();
        mail.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
        mail.MailTo = new string[] { "tutkaloperasyon@tutkal.com.tr" };
        mail.MailCc = new string[] { "pazarlama@biges.com" };
        //01.12.2017
        //mail.MailTo = new string[] { "cansin.ateser@tutkal.com.tr", "fulya.akcora@tutkal.com.tr", "bilge.aslan@tutkal.com.tr", "baris.ersan@tutkal.com.tr", "huseyin.altunbas@tutkal.com.tr" };
        //mail.MailCc = new string[] { "pazarlama@biges.com" };
        //01.12.2017
        mail.MailHtml = true;
        mail.MailSubject = "Motul WebAdmin Sorusuna Cevap Verildi.";

        if (cevapId > 0 && soruId > 0)
        {
            var vDosyaYolu = (from s in bllSoruCevapIslemleri.CevapGetir(soruId).AsEnumerable()
                              where s.Field<int>("CevapId") == cevapId
                              select new { DosyaYolu = s.Field<string>("DosyaYolu") }).SingleOrDefault();

            if (vDosyaYolu != null)
            {
                string path = HttpContext.Current.Server.MapPath(vDosyaYolu.DosyaYolu.ToString());
                if (Directory.Exists(path))
                {
                    System.Net.Mail.Attachment attachment;
                    mail.MailAttach = new List<System.Net.Mail.Attachment>();

                    DirectoryInfo d = new DirectoryInfo(path);
                    foreach (FileInfo file in d.GetFiles())
                    {
                        if (file.Name.Contains("="))
                        {
                            attachment = new System.Net.Mail.Attachment(path + "/" + file.Name);
                            attachment.Name = file.Name.Split('=')[1] != null ? file.Name.Split('=')[1].ToString() : "";
                            mail.MailAttach.Add(attachment);
                        }
                    }
                }
            }
        }

        mail.MailBody = strHTML;
        bool mailSent = mail.SendMail(false);
    }
    [WebMethod]
    public static List<Ticket> GetFiles(string folderId)
    {
        List<Ticket> lstTicket = new List<Ticket>();
        Ticket ticket;
        string path = HttpContext.Current.Server.MapPath("~/TicketImage/TicketAnswers/" + folderId);

        DirectoryInfo d = new DirectoryInfo(path);

        foreach (FileInfo file in d.GetFiles())
        {
            ticket = new Ticket();
            ticket.FolderId = folderId;
            ticket.FileName = file.Name;

            lstTicket.Add(ticket);
        }

        return lstTicket;
    }

    [WebMethod]
    public static string FileDelete(string folderId, string fileName)
    {
        string path = HttpContext.Current.Server.MapPath("~/TicketImage/TicketAnswers/" + folderId + "/" + fileName);

        if (File.Exists(path))
        {
            File.Delete(path);
            return "1";
        }
        return "0";
    }
}