﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="GunRaporu.aspx.cs" Inherits="Admin_GunRaporu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="styles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <%-- RequestType = log türü, RefID = Kurum Kodu, UserAgent = isim --%>
                <asp:Repeater ID="gun_rapor" runat="server">
                    <HeaderTemplate>
                        <table class="tablo_liste border1" cellspacing="1" cellpadding="4">
                            <tr class="tablo_th">
                                <th width="150">
                                    Kurum Kodu
                                </th>
                                <th width="200">
                                    Müşteri
                                </th>
                                <th width="150">
                                    İşlem
                                </th>
                                <th>
                                    Sayfa URL
                                </th>
                                <th>
                                    Saat
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="tablo_tr">
                            <td nowrap>
                                <%--<a href="KurumRaporu.aspx?kod=<%#DataBinder.Eval(Container.DataItem, "RefferrerId")%>">--%>
                                    <%#DataBinder.Eval(Container.DataItem, "RefferrerId")%><%--</a>--%>
                            </td>
                            <td nowrap>
                                <a href="KisiRaporu.aspx?id=<%#DataBinder.Eval(Container.DataItem, "CustomerId")%>"
                                    title="<%#DataBinder.Eval(Container.DataItem, "ClientIp")%>">
                                    <%#DataBinder.Eval(Container.DataItem, "UserAgent")%></a>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "RequestType")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "Url").ToString().Replace("%20"," ")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "CreateDate","{0:HH:mm}")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
            <td>
                &nbsp;
            </td>
            <td width="230">
                <asp:Calendar ID="Calendar1" runat="server" OnSelectionChanged="Calendar1_SelectionChanged"
                    SelectedDate="01.01.0001" VisibleDate="01.01.0001"></asp:Calendar>
                <br />
                Raporu görmek istediğiniz tarihi seçiniz.<br />
                <br />
                <hr />
                <span style="color: red">Kuruma göre
                    <br />
                    Sayfa Gösterimi:</span>
                <hr />
                <asp:Repeater ID="pageview_liste" runat="server">
                    <HeaderTemplate>
                        <table border="0" width="100%">
                            <tr>
                                <td>
                                    <b>Kurum</b>
                                </td>
                                <td>
                                    <b>Sayfa</b>
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "RefferrerId")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "PageView")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
</asp:Content>
