﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;

public partial class Admin_Advertising : System.Web.UI.Page
{
    AdminGenelBLL bllAdminGenel = new AdminGenelBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AdvertisingMasterListele();

            int AdvertisingMasterId = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["AdvertisingMasterId"]) && int.TryParse(Request.QueryString["AdvertisingMasterId"], out AdvertisingMasterId))
            {
                AdvertisingDetayListele(AdvertisingMasterId);
                pnlDetayKayit.Visible = true;
            }
            else
                pnlDetayKayit.Visible = false;

            int AdvertisingDetayId = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["AdvertisingDetayId"]) && int.TryParse(Request.QueryString["AdvertisingDetayId"], out AdvertisingDetayId))
            {
                BilgileriGetir(AdvertisingDetayId);
                hdnDetayId.Value = AdvertisingDetayId.ToString();
            }
        }

    }

    private void AdvertisingMasterListele()
    {
        rptAdvertisingMaster.DataSource = bllAdminGenel.GetirAdvertisingMaster();
        rptAdvertisingMaster.DataBind();
    }

    private void AdvertisingDetayListele(int AdvertisingMasterId)
    {
        rptAdvertisingDetay.DataSource = bllAdminGenel.GetirAdvertisingDetay(AdvertisingMasterId);
        rptAdvertisingDetay.DataBind();
    }

    private void BilgileriGetir(int AdvertisingDetayId)
    {
        DataTable dtAdvertisingDetay = new DataTable();
        dtAdvertisingDetay = bllAdminGenel.GetirAdvertisingDetayByDetayId(AdvertisingDetayId);

        if (dtAdvertisingDetay != null && dtAdvertisingDetay.Rows.Count > 0)
        {
            DataRow dr = dtAdvertisingDetay.Rows[0];

            txtGorselUrl.Text = dr["GorselUrl"].ToString();
            txtHedefUrl.Text = dr["HedefUrl"].ToString();

            txtUrunAd.Text = dr["UrunAd"].ToString();
            txtUrunResimYol.Text = dr["UrunResim"].ToString();
            txtUrunLink.Text = dr["UrunLink"].ToString();

            string SayiOndalikAyrac = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            string UrunFiyat = dr["UrunFiyat"].ToString();
            string IndUrunFiyat = dr["UrunIndFiyat"].ToString();

            if (dr["UrunFiyat"].ToString().IndexOf(SayiOndalikAyrac[0]) > -1)
            {
                txtUrunFiyat.Text = dr["UrunFiyat"].ToString().Split(SayiOndalikAyrac[0])[0];
                txtUrunFiyatKurus.Text = dr["UrunFiyat"].ToString().Split(SayiOndalikAyrac[0])[1];
            }
            else
            {
                txtUrunFiyat.Text = dr["UrunFiyat"].ToString();
                txtUrunFiyatKurus.Text = "00";
            }

            if (dr["UrunIndFiyat"].ToString().IndexOf(SayiOndalikAyrac[0]) > -1)
            {
                txtIndUrunFiyat.Text = dr["UrunIndFiyat"].ToString().Split(SayiOndalikAyrac[0])[0];
                txtIndUrunFiyatKurus.Text = dr["UrunIndFiyat"].ToString().Split(SayiOndalikAyrac[0])[1];
            }
            else
            {
                txtIndUrunFiyat.Text = dr["UrunIndFiyat"].ToString();
                txtIndUrunFiyatKurus.Text = "00";
            }

            chkAktif.Checked = Convert.ToBoolean(dr["Aktif"]);

        }
    }

    protected void btnKaydet_Click(object sender, EventArgs e)
    {

        string SayiOndalikAyrac = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
        decimal fiyat, indFiyat = 0;

        if (!decimal.TryParse(txtUrunFiyat.Text.Trim() + SayiOndalikAyrac + txtUrunFiyatKurus.Text.Trim(), out fiyat))
            fiyat = 0;

        if (!decimal.TryParse(txtIndUrunFiyat.Text.Trim() + SayiOndalikAyrac + txtIndUrunFiyatKurus.Text.Trim(), out indFiyat))
            indFiyat = 0;

        string Mesaj = "";
        if (!string.IsNullOrEmpty(hdnDetayId.Value))
        {
            if (bllAdminGenel.GuncelleAdvertisingDetay(Convert.ToInt32(hdnDetayId.Value), txtGorselUrl.Text.Trim().TrimStart('/'), txtHedefUrl.Text.Trim(), txtUrunAd.Text.Trim(), txtUrunResimYol.Text.Trim(), txtUrunLink.Text.Trim(), fiyat, indFiyat, chkAktif.Checked))
            {
                Mesaj = "Bilgiler Güncellendi";
                BilgileriTemizle();
            }
            else
                Mesaj = "Güncelleme sırasında bir hata oluştu. Lütfen tekrar deneyin.";
        }
        else
        {
            if (bllAdminGenel.KaydetAdvertisingDetay(Convert.ToInt32(Request.QueryString["AdvertisingMasterId"]), txtGorselUrl.Text.Trim().TrimStart('/'), txtHedefUrl.Text.Trim(), txtUrunAd.Text.Trim(), txtUrunResimYol.Text.Trim(), txtUrunLink.Text.Trim(), fiyat, indFiyat))
            {
                Mesaj = "Bilgiler Kaydedildi";
                BilgileriTemizle();
            }
            else
                Mesaj = "Kayıt sırasında bir hata oluştu. Lütfen tekrar deneyin.";
        }

        ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('" + Mesaj + "'); location.href='/Admin/Advertising.aspx?AdvertisingMasterId=" + Request.QueryString["AdvertisingMasterId"] + "'</script>");

    }

    private void BilgileriTemizle()
    {

        hdnDetayId.Value = "";
        txtGorselUrl.Text = string.Empty;
        txtHedefUrl.Text = string.Empty;
        txtUrunAd.Text = string.Empty;
    }
    protected void btnYeni_Click(object sender, EventArgs e)
    {
        BilgileriTemizle();
        Response.Redirect("/Admin/Advertising.aspx?AdvertisingMasterId=" + Request.QueryString["AdvertisingMasterId"], false);
    }
}