﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Net.Mail;


public partial class Admin_AkaryakitSiparis : System.Web.UI.Page
{
    SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    private static object lockObject = new Object();
    DataSet dsSiparis;
    DataView dvSorted;
    DataTable sortedDT;

    DataTable dtAkaryakitBilgileri;
    AkaryakitIslemleriBLL bllAkaryakitIslemleri = new AkaryakitIslemleriBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            drpKullaniciAdDoldur();

        }

    }


    protected void btnGetir_Click(object sender, EventArgs e)
    {

        int siparisId;
        int erpCariId;
        string kartNo;
        int kartYukleme;
        int gonderimDurumu;

        Int32.TryParse(txtSiparisId.Text.ToString(), out siparisId);
        Int32.TryParse(drpKullaniciAd.SelectedValue.ToString(), out erpCariId);
        kartNo = txtKartNo.Text.Trim().ToUpper();
        if (!Int32.TryParse(drpKartYukleme.SelectedValue.ToString(), out kartYukleme))
            kartYukleme = -1;
        if (!Int32.TryParse(drpGonderimDurumu.SelectedValue.ToString(), out gonderimDurumu))
            gonderimDurumu = -1;

        string ilkTarih = txtIlkTarih.Text.ToString().Trim();
        string sonTarih = txtSonTarih.Text.ToString().Trim();


        if (ilkTarih == string.Empty && sonTarih == string.Empty)
        {
            ilkTarih = "01.01.2000";
            sonTarih = "30.12.9999";
        }
        else if (ilkTarih != string.Empty && sonTarih == string.Empty)
        {
            sonTarih = ilkTarih;
        }
        else if (ilkTarih == string.Empty && sonTarih != string.Empty)
        {
            ilkTarih = "01.01.2000";
        }

        ViewState["SiparisId"] = siparisId;
        ViewState["ErpCariId"] = erpCariId;
        ViewState["KartNo"] = kartNo;
        ViewState["KartYukleme"] = kartYukleme;
        ViewState["GonderimDurumu"] = gonderimDurumu;
        ViewState["IlkTarih"] = ilkTarih;
        ViewState["SonTarih"] = sonTarih;


        AkaryakitBilgileriBind(siparisId, erpCariId, kartNo, kartYukleme, gonderimDurumu, ilkTarih, sonTarih);

    }

    protected void rptAkaryakitKartBilgileri_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string[] arg;

        if (e.CommandName == "Update")
        {
            HtmlTableRow rowAll = (HtmlTableRow)e.Item.FindControl("trOrderChooes");

            TextBox txtKartNo = (TextBox)e.Item.FindControl("txtKartNo");

            if (txtKartNo.Text.Trim() == string.Empty)
            {
                rowAll.Attributes.Add("style", "background-color:#FF6B6B; width: 1140px; margin-left: -30px; border: 1px solid #ccc;");
                txtKartNo.Enabled = true;
            }
            else
            {
                txtKartNo.Enabled = false;
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Kart numarasını değiştiremezsiniz. Sadece yeni kart numarası girebilirsiniz."));
            }

        }

        if (e.CommandName == "UpdateSave")
        {
            bool sonuc = false;
            arg = new string[4];
            arg = e.CommandArgument.ToString().Split(';');

            int id = Convert.ToInt32(arg[0].ToString());
            int erpCariId = Convert.ToInt32(arg[1].ToString());
            int siparisNo = Convert.ToInt32(arg[2].ToString());
            int kartId = Convert.ToInt32(arg[3].ToString());

            TextBox txtKartNo = (TextBox)e.Item.FindControl("txtKartNo");

            long kartNo;
            if (txtKartNo.Text.Trim().Length == 16 && long.TryParse(txtKartNo.Text.Trim(), out kartNo) && kartNo > 0)
            {
                sonuc = bllAkaryakitIslemleri.AkaryakitKartSiparisGuncelle(id, erpCariId, siparisNo, kartId, txtKartNo.Text.Trim().ToUpper(), string.Empty);
            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Kart numarası 16 haneli olmalı ve rakamlardan oluşmalı. Lütfen tekrar deneyiniz."));
            }


            int _siparisId = Convert.ToInt32(ViewState["SiparisId"]);
            int _erpCariId = Convert.ToInt32(ViewState["ErpCariId"]);
            string _kartNo = ViewState["KartNo"].ToString();
            int _kartYukleme = Convert.ToInt32(ViewState["KartYukleme"]);
            int _gonderimDurumu = Convert.ToInt32(ViewState["GonderimDurumu"]);
            string _ilkTarih = ViewState["IlkTarih"].ToString();
            string _sonTarih = ViewState["SonTarih"].ToString();

            AkaryakitBilgileriBind(_siparisId, _erpCariId, _kartNo, _kartYukleme, _gonderimDurumu, _ilkTarih, _sonTarih);

        }

    }

    public bool SiparisOnayKontrol(int siparisMasterId)
    {
        string outOnayDurum1, outOnayDurum2;
        bllSiparisGoruntuleme.SiparisOnayDurum(siparisMasterId, "Bekliyor", "Bekliyor", out  outOnayDurum1, out outOnayDurum2);

        if (outOnayDurum1 == "Onaylandı")
            return true;
        return false;
    }

    public void AkaryakitBilgileriBind(int siparisId, int erpCariId, string kartNo, int kartYukleme, int gonderimDurumu, string ilkTarih, string sonTarih)
    {
        rptAkaryakitKartBilgileri.DataSource = null;
        try
        {

            dtAkaryakitBilgileri = bllAkaryakitIslemleri.AkaryakitKartSiparisListele(erpCariId, siparisId, kartNo, kartYukleme, -1, ilkTarih, sonTarih);

            //Onaylanmış siparişleri getir.
            var sonuc = dtAkaryakitBilgileri.AsEnumerable().Where(k => SiparisOnayKontrol(k.Field<int>("SiparisNo")));
            dtAkaryakitBilgileri = sonuc.Count() > 0 ? sonuc.CopyToDataTable() : dtAkaryakitBilgileri.Clone();

            //Yeni kartları gruplandır.
            var vKartYeni = (from b in dtAkaryakitBilgileri.AsEnumerable()
                             where b.Field<bool>("KartDurumu") == false
                             group b by new { KartId = b.Field<int>("KartId") } into g
                             select new
                             {
                                 KartId = g.Key.KartId,
                                 SiparisNo = String.Join(",", g.Select(row => row.Field<int>("SiparisNo")).ToArray()),
                             }).ToList();

            int minSiparisNo = int.MaxValue;
            int maxSiparisNo = int.MinValue;
            foreach (DataRow dr in dtAkaryakitBilgileri.Rows)
            {
                int _siparisNo = dr.Field<int>("SiparisNo");
                minSiparisNo = Math.Min(minSiparisNo, _siparisNo);
                maxSiparisNo = Math.Max(maxSiparisNo, _siparisNo);
            }

            DataColumn dc = new DataColumn("KartYuklemeDurumBilgisi", typeof(string));
            dtAkaryakitBilgileri.Columns.Add(dc);
            dc = new DataColumn("KargoDurumu", typeof(string));
            dtAkaryakitBilgileri.Columns.Add(dc);
            dc = new DataColumn("IrsaliyeNo", typeof(string));
            dtAkaryakitBilgileri.Columns.Add(dc);
            dc = new DataColumn("SipNo", typeof(string));
            dtAkaryakitBilgileri.Columns.Add(dc);
            dc = new DataColumn("KartToplamYuklenen", typeof(string));
            dtAkaryakitBilgileri.Columns.Add(dc);

            DataTable dtSiparisDetay = bllSiparisGoruntuleme.GenelSiparisListeleme(0, 0, minSiparisNo, maxSiparisNo, Convert.ToDateTime("01.01.2000"), Convert.ToDateTime("31.12.9999")).Tables["CariSiparisDetayTT"];
            DataTable dtAkaryakitUrun = bllAkaryakitIslemleri.AkaryakitKartUrunListele();

            var vSiparisAkaryakit = (from siparisDetay in dtSiparisDetay.AsEnumerable()
                                     join akaryakitUrun in dtAkaryakitUrun.AsEnumerable()
                                     on siparisDetay.Field<string>("urun_id").ToString() equals akaryakitUrun.Field<string>("UrunId").ToString()
                                     select siparisDetay).ToList();

            DataTable dtSiparisAkaryakit = vSiparisAkaryakit.Count() > 0 ? vSiparisAkaryakit.CopyToDataTable() : dtSiparisDetay.Clone();

            foreach (DataRow dr in dtAkaryakitBilgileri.Rows)
            {
                if (Convert.ToBoolean(dr["KartYukleme"]))
                {
                    dr["KartYuklemeDurumBilgisi"] = "<input  type=\"checkbox\" disabled=\"disabled\" checked=\"checked\" cursor:pointer\"></input>";
                }
                else
                {
                    dr["KartYuklemeDurumBilgisi"] = "<input  type=\"checkbox\" cursor:pointer\" class=\"KartYukleme_" + dr["Id"].ToString() + "\" OnClick=\"KartYuklemeOnay(" + dr["Id"].ToString() + ");return false;\" ></input>";
                }

                string kartToplamYuklenenTutar = string.Empty;
                kartToplamYuklenenTutar = Convert.ToInt32(dr["KartToplamYuklenenTutar"]) == 0 ? "0" : dr["KartToplamYuklenenTutar"].ToString();
                dr["KartToplamYuklenen"] = "<span class=\"KartToplamYuklenen_" + dr["KartId"].ToString() + "\">" + kartToplamYuklenenTutar + "</span>";

                /*Bir kart id 'ye ait hem eski hem de yeni durum olduğunda.*/
                /*Eski kart ise aynı kartın daha önceden teslim edilip edilmediğine göre kart durumunu değiştir.*/
                if (Convert.ToBoolean(dr["KartDurumu"]))
                {
                    var vKartDurumu = vKartYeni.Where(k => k.KartId == Convert.ToInt32(dr["KartId"])).ToList();
                    if (vKartDurumu.Count == 1)
                    {
                        string sipNo = vKartDurumu.Single().SiparisNo;
                        string strQuery = "siparis_id in (" + sipNo + ")";
                        DataRow[] drSiparisDetay1 = dtSiparisDetay.Select(strQuery);
                        DataRow[] drSiparisDetay2 = dtSiparisDetay.Select(strQuery).AsEnumerable().Where(k => k.Field<DateTime?>("teslim_tarih").HasValue == false || k.Field<DateTime?>("teslim_tarih") == null || k.Field<DateTime?>("teslim_tarih").ToString() == string.Empty).ToArray();

                        if (drSiparisDetay1.Length == drSiparisDetay2.Length)
                            dr["KartDurumu"] = false;
                    }
                }


                /*Bir siparişteki akaryakıtlar tek irsaliye  olarak kesilmeli.*/
                DataRow[] drSiparisDetayIrs = dtSiparisAkaryakit.Select("siparis_id = '" + dr["SiparisNo"].ToString() + "'").AsEnumerable().Take(1).ToArray();


                /*Bir siparişteki akaryakıtlar tek irsaliye  olarak kesilmeli.*/
                if (drSiparisDetayIrs.Length == 1)
                {
                    if (drSiparisDetayIrs[0]["siparis_id"] != null && drSiparisDetayIrs[0]["siparis_id"].ToString() != string.Empty)
                    {
                        dr["IrsaliyeNo"] = drSiparisDetayIrs[0]["siparis_id"].ToString();
                    }
                    else
                    {
                        dr["IrsaliyeNo"] = string.Empty;
                    }

                    //if (drSiparisDetayIrs[0]["aciklama"] != null && drSiparisDetayIrs[0]["aciklama"].ToString() != string.Empty)
                    //{
                    //    if (drSiparisDetayIrs[0]["teslim_tarih"] != null && drSiparisDetayIrs[0]["teslim_tarih"].ToString() != string.Empty)
                    //    {
                    //        dr["KargoDurumu"] = "<div style=\"width:50px;\"><a style=\"color:#00CC00\" target=\"_blank\" href=\"http://selfservis.yurticikargo.com/reports/SSWDocumentDetail.aspx?DocId=" + drSiparisDetayIrs[0]["aciklama"] + "\" >Teslim Edildi.</a></div>";
                    //    }
                    //    else
                    //    {
                    //        dr["KargoDurumu"] = "<div style=\"width:50px;\"><a style=\"color:#ff0000\" target=\"_blank\" href=\"http://selfservis.yurticikargo.com/reports/SSWDocumentDetail.aspx?DocId=" + drSiparisDetayIrs[0]["aciklama"] + "\" >Kargoda.</a></div>";
                    //    }
                    //}
                    //else
                    //{
                    //    dr["KargoDurumu"] = string.Empty;
                    //}
                }
                else
                {
                    dr["KargoDurumu"] = string.Empty;
                    dr["IrsaliyeNo"] = string.Empty;
                }
            }

            if (gonderimDurumu.ToString() == "1")
            {
                var v1 = (from k in dtAkaryakitBilgileri.AsEnumerable()
                          where k.Field<string>("KargoDurumu").IndexOf("Teslim Edildi") > -1
                          select k).ToList();
                dtAkaryakitBilgileri = v1.Count() > 0 ? v1.CopyToDataTable() : dtAkaryakitBilgileri.Clone();
            }
            else if (gonderimDurumu.ToString() == "0")
            {
                var v2 = (from k in dtAkaryakitBilgileri.AsEnumerable()
                          where k.Field<string>("KargoDurumu").IndexOf("Teslim Edildi") == -1
                          select k).ToList();
                dtAkaryakitBilgileri = v2.Count() > 0 ? v2.CopyToDataTable() : dtAkaryakitBilgileri.Clone();
            }

            rptAkaryakitKartBilgileri.DataSource = dtAkaryakitBilgileri;
            rptAkaryakitKartBilgileri.DataBind();

            ExcelAktar(dtAkaryakitBilgileri);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Hata oluştu"));
        }
    }

    public void ExcelAktar(DataTable dt)
    {
        foreach (DataRow dr in dt.Rows)
        {
            if (Convert.ToBoolean(dr["KartYukleme"]))
            {
                dr["KartYuklemeDurumBilgisi"] = "Yapıldı.";
            }
            else
            {
                dr["KartYuklemeDurumBilgisi"] = "Yapılmadı.";
            }
        }

        rptExcelBilgileri.DataSource = dt;
        rptExcelBilgileri.DataBind();
    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + mesaj + "</td></tr></table></div>');</script>";
    }

    public void drpKullaniciAdDoldur()
    {
        DataTable dtUyeBilgileri = new DataTable();

        dtUyeBilgileri = bllUyelikIslemleri.GetirKullaniciAll();
        DataRow[] dr = dtUyeBilgileri.Select("ErpCariId is not null");
        dtUyeBilgileri = dr.Length > 0 ? dr.CopyToDataTable() : dtUyeBilgileri.Clone();

        var sonuc = (from k in dtUyeBilgileri.AsEnumerable()
                     where k.Field<int>("UyelikTipi") != 1//Admin kullanıcılarını getirme!..
                     orderby k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad")
                     select new
                     {
                         ErpCariId = k.Field<int>("ErpCariId"),
                         KullaniciAdSoyad = string.Format("{0} {1}", k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad"))
                     }).ToList();


        drpKullaniciAd.DataTextField = "KullaniciAdSoyad";
        drpKullaniciAd.DataValueField = "ErpCariId";
        drpKullaniciAd.DataSource = sonuc;
        drpKullaniciAd.DataBind();
        drpKullaniciAd.Items.Insert(0, (new ListItem(string.Empty, "-1")));

    }

    public bool IsValidMail(string emailaddress)
    {
        try
        {
            MailAddress m = new MailAddress(emailaddress);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    [WebMethod]
    public static string[] KartYuklemeOnay(int AkaryakitKartSiparisId)
    {
        AkaryakitIslemleriBLL bllAkaryakitIslemleri = new AkaryakitIslemleriBLL();
        bool sonuc = bllAkaryakitIslemleri.AkaryakitKartSiparisYuklemeDurumu(AkaryakitKartSiparisId, true);

        if (sonuc)
        {
            DataRow dr = bllAkaryakitIslemleri.AkaryakitKartSiparisListele(AkaryakitKartSiparisId);
            if (dr != null)
                return new string[] { "1", dr["KartId"].ToString(), Convert.ToInt32(dr["KartToplamYuklenenTutar"]) == 0 ? "0" : dr["KartToplamYuklenenTutar"].ToString() };
            return new string[] { "0", string.Empty, string.Empty };
        }
        return new string[] { "0", string.Empty, string.Empty };
    }

    protected void btnExcelBilgileri_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=AkaryakitKartBilgileri.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";

        Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
        Response.Charset = "windows-1254";

        string header = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title></title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1254\" />\n<style>\n</style>\n</head>\n<body>\n";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        rptExcelBilgileri.RenderControl(htmlWrite);
        Response.Write(header + stringWrite.ToString());
        Response.End();
    }

}