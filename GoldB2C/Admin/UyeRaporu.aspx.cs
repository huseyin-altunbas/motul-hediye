﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;
using Goldb2cBLL;

public partial class Admin_UyeRaporu : System.Web.UI.Page
{
    AdminGenelBLL bllAdminGenel = new AdminGenelBLL();
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    public string CreateCSV(DataTable dt)
    {

        #region Export Grid to CSV

        StringBuilder sb = new StringBuilder();

        int iColCount = dt.Columns.Count;

        for (int i = 0; i < iColCount; i++)
        {
            sb.Append(dt.Columns[i]);

            if (i < iColCount - 1)
            {
                sb.Append(";");
            }
        }

        sb.AppendLine("");
        foreach (DataRow dr in dt.Rows)
        {
            for (int i = 0; i < iColCount; i++)
            {
                if (!Convert.IsDBNull(dr[i]))
                {
                    sb.Append(dr[i].ToString());
                }
                if (i < iColCount - 1)
                {
                    sb.Append(";");
                }
            }
            sb.AppendLine("");

        }

        return sb.ToString();

        #endregion

    }
    protected void btnGetir_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hdnTarih.Value))
        {
            DateTime dtBaslangic, dtBitis;
            string[] _tarihAralik = hdnTarih.Value.Split(',');

            string[] _dtBaslangic = _tarihAralik[0].Split('-');
            string[] _dtBitis = _tarihAralik[1].Split('-');

            dtBaslangic = new DateTime(Convert.ToInt32(_dtBaslangic[2]), Convert.ToInt32(_dtBaslangic[1]), Convert.ToInt32(_dtBaslangic[0]));
            dtBitis = new DateTime(Convert.ToInt32(_dtBitis[2]), Convert.ToInt32(_dtBitis[1]), Convert.ToInt32(_dtBitis[0]));

            string DosyaAd = "UyeRaporu_" + DateTime.Now.ToShortDateString();
            string Csv = CreateCSV(bllAdminGenel.GunlukUyeRapor(dtBaslangic, dtBitis.AddHours(23).AddMinutes(59).AddSeconds(59)));
            if (!string.IsNullOrEmpty(Csv))
            {
                Response.Clear();
                Response.ContentType = "application/CSV";
                Response.AddHeader("content-disposition", "attachment; filename=\"" + DosyaAd + ".csv\"");
                Response.Write(Csv);
                Response.End();
            }
        }
        else
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Lütfen başlangıç ve bitiş tarihi seçiniz.');</script>");
    }
}