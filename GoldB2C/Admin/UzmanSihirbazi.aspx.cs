﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cEntity;
using System.Web.Services;
using Goldb2cBLL;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

public partial class Admin_UzmanSihirbazi : System.Web.UI.Page
{
    PortalAdminBLL bllPortalAdmin = new PortalAdminBLL();
    UzmanSihirbaziBLL bllUzmanSihirbazi = new UzmanSihirbaziBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack || string.IsNullOrEmpty(hdnUzmanGorusuBaslik.Value))
        {
            ClientScript.RegisterStartupScript(typeof(string), "BaslangicScript", "<script type=\"text/javascript\">  $('.photoBox').hide(); $('.styleBox').hide();</script>");
        }
    }

    protected void btnUrunAra_Click(object sender, EventArgs e)
    {
        lblUrunAd.Text = "";
        hdnUrunId.Value = "";
        hdnUrunAd.Value = "";
        hdnUrunResimAd.Value = "";




        pnlUzmanGorusuIcerik.Visible = false;
        int UrunId = 0;
        if (!string.IsNullOrEmpty(txtStokKod.Text.Trim()) && int.TryParse(txtStokKod.Text.Trim(), out UrunId))
        {


            //string Email = "";
            //if (HttpContext.Current.Session["Kullanici"] != null)
            //    Email = ((Kullanici)Session["Kullanici"]).Email;

            //bool uygun = false;
            //if (Email == "izzet.gunduz@metroelektronik.com.tr")
            //{
            //    LogEkle(Email + " |---| ", UrunId.ToString());
            //    if (UrunId == 131449)
            //        uygun = true;
            //    else
            //        uygun = false;
            //}
            //else
            //    uygun = true;

            bool UrunBulundu = false;

            //if (uygun)
            UrunBulundu = UrunSorgula(UrunId);

            if (UrunBulundu)
            {
                //Ürünü buldu uzman görüşü adımlarına başla
                lblUrunAd.Text = hdnUrunAd.Value;

            }
            else
            {
                MesajVer("Aradığınız ürün bulunamadı. Lütfen stok kodunu kontrol edip tekrar deneyiniz.");
            }
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>Hatalı stok kodu. Lütfen kontrol edip tekrar deneyin.</script>");
        }

    }

    protected void btnBasla_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hdnUrunAd.Value) && !string.IsNullOrEmpty(hdnUrunId.Value) && !string.IsNullOrEmpty(hdnUrunResimAd.Value))
        {
            int UrunId = Convert.ToInt32(hdnUrunId.Value);
            pnlUzmanGorusuIcerik.Visible = true;
            SablonSecenekleriGetir();
            ResimleriListele(UrunId);
        }
        else
        {
            pnlUzmanGorusuIcerik.Visible = false;
            dpSablonlar.Items.Clear();
        }
    }

    private void SablonSecenekleriGetir()
    {
        dpSablonlar.DataSource = bllUzmanSihirbazi.GetirUzmanGorusuSablon();
        dpSablonlar.DataValueField = "SablonId";
        dpSablonlar.DataTextField = "SablonAd";
        dpSablonlar.DataBind();
        dpSablonlar.Items.Insert(0, new ListItem("Seçiniz", ""));
    }

    private bool UrunSorgula(int UrunId)
    {
        bool Sonuc = false;
        hdnUrunId.Value = "";
        hdnUrunAd.Value = "";
        hdnUrunResimAd.Value = "";

        string[] UrunBilgisi = bllPortalAdmin.UrunBilgisi(UrunId);

        if (!string.IsNullOrEmpty(UrunBilgisi[0]))
        {
            hdnUrunId.Value = UrunId.ToString();
            hdnUrunAd.Value = UrunBilgisi[0];
            Sonuc = true;

            if (!string.IsNullOrEmpty(UrunBilgisi[1]) && UrunBilgisi[1].IndexOf('/') > -1)
            {
                hdnUrunResimAd.Value = UrunBilgisi[1].Substring(UrunBilgisi[1].LastIndexOf('/') + 1, UrunBilgisi[1].Length - UrunBilgisi[1].LastIndexOf('/') - 1);
            }
            else
            {
                hdnUrunResimAd.Value = CommonBLL.ChangeTRCharacter(UrunBilgisi[0]).Replace(" ", "-").Replace("\"", "").Replace("'", "").Replace("/", "").Replace("\\", "");
            }
        }

        return Sonuc;
    }

    private void MesajVer(string Mesaj)
    {
        ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert(" + Mesaj + ");</script>");
    }

    protected void btnResimEkle_Click(object sender, EventArgs e)
    {
        int UrunId = Convert.ToInt32(hdnUrunId.Value);
        string UrunResimAd = hdnUrunResimAd.Value;
        string Path = Server.MapPath(@"~/UrunResim/MarketingResim/UzmanGorusu/" + UrunId.ToString());

        if (resimUpload.HasFile)
        {
            if (ValidateFileDimensions())
            {
                string DosyaTipi = resimUpload.PostedFile.ContentType;
                int DosyaBoyutu = resimUpload.PostedFile.ContentLength;

                if (DosyaTipi == "image/pjpeg" || DosyaTipi == "image/jpeg")
                {
                    if (DosyaBoyutu < 5242880)
                    {
                        try
                        {
                            //Ürünün uzman görüşü klasörünü oluştur
                            if (!System.IO.Directory.Exists(Path))
                            {
                                System.IO.Directory.CreateDirectory(Path);
                            }

                            //Uzman görüşü içindeki resim klasörü
                            if (!System.IO.Directory.Exists(Path + "//Resim"))
                            {
                                System.IO.Directory.CreateDirectory(Path + "//Resim");
                            }

                            int SonDosyaNo = 0;
                            foreach (string _dosyaAd in System.IO.Directory.GetFiles(Path + "\\Resim"))
                            {
                                int _dosyaNo = 0;
                                if (int.TryParse(_dosyaAd.Substring(_dosyaAd.LastIndexOf('-') + 1, (_dosyaAd.LastIndexOf('.') - _dosyaAd.LastIndexOf('-')) - 1), out _dosyaNo))
                                {
                                    if (_dosyaNo > SonDosyaNo)
                                        SonDosyaNo = _dosyaNo;
                                }
                            }

                            SonDosyaNo = SonDosyaNo + 1;
                            UrunResimAd = hdnUrunResimAd.Value + "-" + SonDosyaNo;



                            string tempDosya = Path + "\\Resim\\" + DateTime.Now.ToLongTimeString().Replace(":", "").Replace(".", "") + ".jpg";
                            resimUpload.SaveAs(tempDosya);// İlk olarak fotoyu normal kayıt ediyoruz

                            using (Bitmap OriginalBMb = new Bitmap(tempDosya))//Kayıt ettğimiz fotoyu çağırıp üzerinde işlem yapıyoruz
                            {
                                double ResimYukseklik = OriginalBMb.Height;// yüksekliği belirtiyoruz
                                double ResimUzunluk = OriginalBMb.Width;// genişliği belirtiyoruz
                                int sabit = 300;//vermek istediğimiz oranı veriyoruz eğer foto 300 den yüksek veya geniş ise bu işlemi gerçekleştireceğiz
                                double oran = 0;
                                if (ResimUzunluk > ResimYukseklik && ResimUzunluk > sabit)
                                {
                                    oran = ResimUzunluk / ResimYukseklik;
                                    ResimUzunluk = sabit;
                                    ResimYukseklik = sabit / oran;
                                }
                                else if (ResimYukseklik > ResimUzunluk && ResimYukseklik > sabit)
                                {
                                    oran = ResimYukseklik / ResimUzunluk;
                                    ResimYukseklik = sabit;
                                    ResimUzunluk = sabit / oran;
                                }
                                Size newSizeb = new Size(Convert.ToInt32(ResimUzunluk), Convert.ToInt32(ResimYukseklik));
                                Bitmap Resizebmb = new Bitmap(OriginalBMb, newSizeb);
                                Graphics grPhoto = Graphics.FromImage(Resizebmb);
                                grPhoto.InterpolationMode = InterpolationMode.Default; // resmin kalitesini ayarlıyoruz. Burada InterpolationMode özelliklerini bulabilirsini
                                Resizebmb.Save(Path + "\\Resim\\" + UrunResimAd + ".jpg", ImageFormat.Jpeg);
                                OriginalBMb.Dispose();
                            }
                            File.Delete(tempDosya);//eski oluşturduğumuz resimi siliyoruz
                            ResimleriListele(UrunId);
                        }
                        catch (Exception err)
                        {
                            lblUrunAd.Text = err.Message;
                            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Hata :');</script>");
                        }
                    }
                    else
                    {
                        MesajVer("Dosya boyutu en fazla 5MB olabilir.");
                    }
                }
                else
                {
                    MesajVer("Sadece resim dosyası yükleyebilirsiniz.");
                }
            }
            else
            {
                MesajVer("Resim genişliği en az 250px olmalıdır.");
            }
        }
    }

    private List<entUzmanSihirbazResim> ResimleriListele(int UrunId)
    {
        List<entUzmanSihirbazResim> lstUzmanSihirbazResim = new List<entUzmanSihirbazResim>();

        if (System.IO.Directory.Exists(Server.MapPath(@"~/UrunResim/MarketingResim/UzmanGorusu/" + UrunId.ToString() + "/Resim")))
        {
            string[] Resimler = System.IO.Directory.GetFiles(Server.MapPath(@"~/UrunResim/MarketingResim/UzmanGorusu/" + UrunId.ToString() + "/Resim"));

            entUzmanSihirbazResim resim;
            for (int i = 0; i < Resimler.Length; i++)
            {
                resim = new entUzmanSihirbazResim();
                resim.ResimAd = Resimler[i].Replace(Server.MapPath(@"~/UrunResim/MarketingResim/UzmanGorusu/"), "");
                resim.ResimWebAd = Resimler[i].Replace(Server.MapPath("~/UrunResim"), "/UrunResim").Replace("\\", "/");

                lstUzmanSihirbazResim.Add(resim);
            }

            rptResimler.DataSource = lstUzmanSihirbazResim;
            rptResimler.DataBind();
        }
        else
        {
            rptResimler.DataSource = null;
            rptResimler.DataBind();
        }

        return lstUzmanSihirbazResim;

    }

    public bool ValidateFileDimensions()
    {
        using (System.Drawing.Image Resim = System.Drawing.Image.FromStream(resimUpload.PostedFile.InputStream))
        {
            return (Resim.Width >= 250);
        }
    }

    protected void rptResimler_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "ResimSil")
        {
            int UrunId = Convert.ToInt32(hdnUrunId.Value);

            if (File.Exists(Server.MapPath(@"~/UrunResim/MarketingResim/UzmanGorusu/" + e.CommandArgument.ToString())))
                File.Delete(Server.MapPath(@"~/UrunResim/MarketingResim/UzmanGorusu/" + e.CommandArgument.ToString()));

            if (File.Exists(@"\\10.100.30.30\c$\UrunResim\MarketingResim\UzmanGorusu\" + e.CommandArgument.ToString()))
                File.Exists(@"\\10.100.30.30\c$\UrunResim\MarketingResim\UzmanGorusu\" + e.CommandArgument.ToString());

            ResimleriListele(UrunId);
        }
    }

    protected void btnVazgec_Click(object sender, EventArgs e)
    {
        SecimleriTemizle();
    }

    private void SecimleriTemizle()
    {
        txtStokKod.Text = "";
        lblUrunAd.Text = "";
        hdnUrunId.Value = "";
        hdnUrunAd.Value = "";
        hdnUrunResimAd.Value = "";

        pnlUzmanGorusuIcerik.Visible = false;
    }

    [WebMethod]
    public static entUzmanGorusuSablon SablonInputHtmlGetir(int SablonId)
    {
        entUzmanGorusuSablon uzmanGorusuSablon = new entUzmanGorusuSablon();
        DataTable dtSablon = new DataTable();
        UzmanSihirbaziBLL bllUzmanSihirbazi = new UzmanSihirbaziBLL();
        dtSablon = bllUzmanSihirbazi.GetirUzmanGorusuSablonById(SablonId);
        if (dtSablon != null && dtSablon.Rows.Count > 0)
        {
            DataRow dr = dtSablon.Rows[0];
            uzmanGorusuSablon.SablonId = dr["SablonId"].ToString();
            uzmanGorusuSablon.SablonAd = dr["SablonAd"].ToString();
            uzmanGorusuSablon.SablonHtml = dr["SablonHtml"].ToString();
            uzmanGorusuSablon.SablonInputHtml = dr["SablonInputHtml"].ToString();
            uzmanGorusuSablon.SablonParametre = dr["SablonParametre"].ToString();
        }

        return uzmanGorusuSablon;
    }

    [WebMethod]
    public static string[] UzmanGorusuKaydet(string UrunId, string UzmanGorusuHtml)
    {
        string[] Sonuc = new string[] { "false", "" };
        int _UrunId = Convert.ToInt32(UrunId);

        string _UzmanGorusuHtml = HttpUtility.UrlDecode(UzmanGorusuHtml, System.Text.Encoding.GetEncoding("windows-1254"));
        _UzmanGorusuHtml = _UzmanGorusuHtml.Replace("<UL class=\"ui-sortable ui-draggable\">", "").Replace("<ul class=\"ui-sortable ui-draggable\">", "");
        _UzmanGorusuHtml = _UzmanGorusuHtml.Replace("<UL>", "").Replace("<ul>", ""); ;
        _UzmanGorusuHtml = _UzmanGorusuHtml.Replace("</UL>", "").Replace("</ul>", "");
        _UzmanGorusuHtml = _UzmanGorusuHtml.Replace("<LI>", "").Replace("<li>", "");
        _UzmanGorusuHtml = _UzmanGorusuHtml.Replace("</LI>", "").Replace("</li>", "").Replace("<li style=\"\">", "").Replace("<ul class=\"ui-sortable ui-draggable\" style=\"\">", "");
        _UzmanGorusuHtml = _UzmanGorusuHtml.Replace("style=\"Z-INDEX: 790\"", "").Replace("style=\"z-index: 790\"", "").Replace("style=\"z-index: 790;\"", "");
        _UzmanGorusuHtml = _UzmanGorusuHtml.Replace("style=\"Z-INDEX: 780\"", "").Replace("style=\"z-index: 780\"", "").Replace("style=\"z-index: 780;\"", "");
        _UzmanGorusuHtml = _UzmanGorusuHtml.Replace("style=\"Z-INDEX: 770\"", "").Replace("style=\"z-index: 770\"", "").Replace("style=\"z-index: 770;\"", "");


        PortalAdminBLL bllPortalAdmin = new PortalAdminBLL();
        Sonuc = bllPortalAdmin.UzmanGorusuEkle(_UrunId, _UzmanGorusuHtml);


        Kullanici kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];
        LogEkle(kullanici.KullaniciId, _UrunId, Sonuc);

        return Sonuc;
    }

    public static void LogEkle(int KullaniciId, int UrunId, string[] IslemSonuc)
    {
        bool _IslemSonuc = Convert.ToBoolean(IslemSonuc[0]);
        string HataMesaj = IslemSonuc[1];

        AdminGenelBLL bllAdminGenel = new AdminGenelBLL();
        bllAdminGenel.KaydetUzmanSihirbaziLog(KullaniciId, UrunId, _IslemSonuc, HataMesaj);

        /*
        DateTime simdi = DateTime.Now;
        StreamWriter sw2 = File.AppendText(@"C:\ERP\IISCode\GoldB2C\Admin\UzmanGorusu\Log\" + simdi.Day.ToString() + "-" + simdi.Month.ToString() + "-" + simdi.Year.ToString() + ".txt");
        sw2.WriteLine(simdi.ToLongTimeString() + " - " + UrunId + "   " + Email);

        sw2.Flush();
        sw2.Close();
         */
    }
}