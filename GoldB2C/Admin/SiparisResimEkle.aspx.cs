﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using System.IO;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Xml;

public partial class Admin_SiparisResimEkle : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        if (fuImage.HasFile && txtSiparisNo.Text != string.Empty && txtUrunNo.Text != string.Empty)
        {
            int siparisNo, urunNo;
            Int32.TryParse(txtSiparisNo.Text.ToString().Trim(), out siparisNo);
            Int32.TryParse(txtUrunNo.Text.ToString().Trim(), out urunNo);

            if (siparisNo > 0 && urunNo > 0)
            {
                string createPath = "~/UrunResim/SiparisUrunResimleri/";

                List<string> listAllowedExtensions = new List<string> { ".gif", ".png", ".jpeg", ".jpg" };

                string path = HttpContext.Current.ApplicationInstance.Server.MapPath(createPath);
                string fileExtension = System.IO.Path.GetExtension(fuImage.PostedFile.FileName).ToLower();

                if (listAllowedExtensions.Any(k => k == fileExtension))
                {
                    string subPath = "~/UrunResim/SiparisUrunResimleri/" + siparisNo.ToString();

                    bool isExists = System.IO.Directory.Exists(Server.MapPath(subPath));

                    if (!isExists)
                    {
                        System.IO.Directory.CreateDirectory(Server.MapPath(subPath));
                    }

                    string fn = siparisNo.ToString() + "-" + urunNo.ToString() + fileExtension;
                    fuImage.PostedFile.SaveAs(System.IO.Path.Combine(path + siparisNo.ToString(), fn));

                    List<SiparisUrunResim> listSiparis = new List<SiparisUrunResim>();


                    string[] fileList = System.IO.Directory.GetFiles(Server.MapPath(subPath), "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".gif") || s.EndsWith(".png") || s.EndsWith(".jpeg") || s.EndsWith(".jpg")).ToArray<string>();

                    foreach (string file in fileList)
                    {
                        string[] _file1 = file.Split('-');
                        string[] _file2 = _file1[1].Split('.');
                        listSiparis.Add(new SiparisUrunResim { siparisNo = siparisNo, urunNo = Convert.ToInt32(_file2[0].ToString()), resimYolu = "/UrunResim/SiparisUrunResimleri/" + siparisNo.ToString() + "/" + siparisNo.ToString() + "-" + _file1[1].ToString() });

                    }

                    rptResim.DataSource = listSiparis;
                    rptResim.DataBind();
                }
                else
                {
                    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Geçerli olan resim uzantıları : .gif, .png, .jpeg, .jpg"));
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sipariş numarası ve Ürün numarası için geçerli bir sayı giriniz."));
            }
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Tüm alanlar dolu olmalıdır."));
        }
    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + mesaj + "</td></tr></table></div>');</script>";
    }
    protected void rptResim_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "DeleteImage")
        {
            string[] arg = new string[3];
            arg = e.CommandArgument.ToString().Split(';');

            int siparisNo = Convert.ToInt32(arg[0].ToString());
            int urunNo = Convert.ToInt32(arg[1].ToString());
            string imagePath = Server.MapPath(arg[2].ToString());

            if (File.Exists(imagePath))
            {
                File.Delete(imagePath);


                string subPath = "~/UrunResim/SiparisUrunResimleri/" + siparisNo.ToString();
                List<SiparisUrunResim> listSiparis = new List<SiparisUrunResim>();

                bool isExists = System.IO.Directory.Exists(Server.MapPath(subPath));

                if (isExists)
                {

                    string[] fileList = System.IO.Directory.GetFiles(Server.MapPath(subPath), "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".gif") || s.EndsWith(".png") || s.EndsWith(".jpeg") || s.EndsWith(".jpg")).ToArray<string>();

                    foreach (string file in fileList)
                    {
                        string[] _file1 = file.Split('-');
                        string[] _file2 = _file1[1].Split('.');
                        listSiparis.Add(new SiparisUrunResim { siparisNo = siparisNo, urunNo = Convert.ToInt32(_file2[0].ToString()), resimYolu = "/UrunResim/SiparisUrunResimleri/" + siparisNo.ToString() + "/" + siparisNo.ToString() + "-" + _file1[1].ToString() });

                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sipariş numarasına ait klasör bulunamadı."));
                }

                rptResim.DataSource = listSiparis;
                rptResim.DataBind();
            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Resim klasörde bulunamadı."));
            }
        }
    }
    protected void btnGetir_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSipNo.Text))
        {
            int sipNo;
            Int32.TryParse(txtSipNo.Text.ToString().Trim(), out sipNo);

            string subPath = "~/UrunResim/SiparisUrunResimleri/" + sipNo.ToString();
            List<SiparisUrunResim> listSiparis = new List<SiparisUrunResim>();

            bool isExists = System.IO.Directory.Exists(Server.MapPath(subPath));

            if (isExists)
            {
                string[] fileList = System.IO.Directory.GetFiles(Server.MapPath(subPath), "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".gif") || s.EndsWith(".png") || s.EndsWith(".jpeg") || s.EndsWith(".jpg")).ToArray<string>();

                foreach (string file in fileList)
                {
                    string[] _file1 = file.Split('-');
                    string[] _file2 = _file1[1].Split('.');
                    listSiparis.Add(new SiparisUrunResim { siparisNo = sipNo, urunNo = Convert.ToInt32(_file2[0].ToString()), resimYolu = "/UrunResim/SiparisUrunResimleri/" + sipNo.ToString() + "/" + sipNo.ToString() + "-" + _file1[1].ToString() });

                }
            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sipariş numarasına ait klasör bulunamadı."));
            }

            rptGetirResim.DataSource = listSiparis;
            rptGetirResim.DataBind();
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sipariş no alanı boş geçilemez."));
        }
    }
    protected void btnPnl1Ac_Click(object sender, EventArgs e)
    {
        pnlSiparisResimEkleSil.Visible = true;
        pnlSiparisResimGetirSil.Visible = false;

        txtSiparisNo.Text = "";
        txtUrunNo.Text = "";
        fuImage.Dispose();

        rptResim.DataSource = null;
        rptResim.DataBind();
    }
    protected void btnPnl2Ac_Click(object sender, EventArgs e)
    {
        pnlSiparisResimEkleSil.Visible = false;
        pnlSiparisResimGetirSil.Visible = true;

        txtSipNo.Text = "";

        rptGetirResim.DataSource = null;
        rptGetirResim.DataBind();
    }
    protected void rptGetirResim_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "DeleteImage")
        {
            string[] arg = new string[3];
            arg = e.CommandArgument.ToString().Split(';');

            int siparisNo = Convert.ToInt32(arg[0].ToString());
            int urunNo = Convert.ToInt32(arg[1].ToString());
            string imagePath = Server.MapPath(arg[2].ToString());

            if (File.Exists(imagePath))
            {
                File.Delete(imagePath);


                string subPath = "~/UrunResim/SiparisUrunResimleri/" + siparisNo.ToString();
                List<SiparisUrunResim> listSiparis = new List<SiparisUrunResim>();

                bool isExists = System.IO.Directory.Exists(Server.MapPath(subPath));

                if (isExists)
                {
                    string[] fileList = System.IO.Directory.GetFiles(Server.MapPath(subPath), "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".gif") || s.EndsWith(".png") || s.EndsWith(".jpeg") || s.EndsWith(".jpg")).ToArray<string>();

                    foreach (string file in fileList)
                    {
                        string[] _file1 = file.Split('-');
                        string[] _file2 = _file1[1].Split('.');
                        listSiparis.Add(new SiparisUrunResim { siparisNo = siparisNo, urunNo = Convert.ToInt32(_file2[0].ToString()), resimYolu = "/UrunResim/SiparisUrunResimleri/" + siparisNo.ToString() + "/" + siparisNo.ToString() + "-" + _file1[1].ToString() });

                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sipariş numarasına ait klasör bulunamadı."));
                }

                rptGetirResim.DataSource = listSiparis;
                rptGetirResim.DataBind();
            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Resim klasörde bulunamadı."));
            }
        }
    }
}
public class SiparisUrunResim
{
    public int siparisNo { get; set; }
    public int urunNo { get; set; }
    public string resimYolu { get; set; }
}