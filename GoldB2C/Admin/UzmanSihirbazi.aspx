﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="UzmanSihirbazi.aspx.cs" Inherits="Admin_UzmanSihirbazi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="styles/uzmanGorusu.css?v=1.0.0.0" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.previewBox').hide();
            $('#btnUzmanGorusuEkle').hide();

            $(".draggablePhoto").draggable({
                revert: true
            });

            $("#txtUzmanGorusuBaslik").val($("#<%=hdnUzmanGorusuBaslik.ClientID %>").val());

        });      
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="adminUzmanGorusu">
        <div class="codeSearch">
            <p class="fl">
                Uzman Görüşü Oluşturacağınız Ürünün Stok Kodunu Girin</p>
            <asp:TextBox ID="txtStokKod" runat="server" CssClass="fl"></asp:TextBox>
            <asp:Button ID="btnUrunAra" runat="server" Text="Ürün Ara" CssClass="fl btn1" OnClick="btnUrunAra_Click" />
        </div>
        <div class="uzmanGorusuStart">
            <p class="fl">
                <asp:Label ID="lblUrunAd" runat="server" Text=""></asp:Label>
            </p>
            <asp:Button ID="btnBasla" runat="server" Text="Uzman Görüşünü Oluşturmaya Başla"
                CssClass="fl btn2" OnClick="btnBasla_Click" />
            <asp:Button ID="btnVazgec" runat="server" Text="Vazgeç" CssClass="fl btn1" OnClick="btnVazgec_Click" />
        </div>
        <asp:Panel ID="pnlUzmanGorusuIcerik" runat="server" Visible="false">
            <div class="uzmanGorusuTitle2">
                UZMAN GÖRÜŞÜ OLUŞTUR</div>
            <div class="titleCreate">
                <p class="fl">
                    Uzman Görüşü Başlığını Girin</p>
                <input id="txtUzmanGorusuBaslik" type="text" class="fl" />
                <input id="btnBaslikEkle" onclick="UzmanGorusuBaslikEkle();" type="button" value="Ekle"
                    class="fl btn1" />
            </div>
            <div class="photoBox">
                <div class="photosTop">
                    <p class="fl">
                        Fotoğraflar</p>
                    <asp:FileUpload ID="resimUpload" runat="server" CssClass="fl" />
                    <asp:Button ID="btnResimEkle" runat="server" Text="Ekle" CssClass="fl btn1" OnClick="btnResimEkle_Click" />
                </div>
                <div class="photosBottom">
                    <asp:Repeater ID="rptResimler" runat="server" OnItemCommand="rptResimler_ItemCommand">
                        <ItemTemplate>
                            <div id="photos">
                                <asp:Button ID="btnResimSil" CommandName="ResimSil" OnClientClick="javascript: return confirm('Resmi silmek istediğinizden emin misiniz?');"
                                    CommandArgument='<%#DataBinder.Eval(Container.DataItem, "ResimAd") %>' runat="server"
                                    Text="" CssClass="closed" />
                                <img src='<%#DataBinder.Eval(Container.DataItem, "ResimWebAd") %>' class="draggablePhoto"
                                    width="50px;" height="50px;" />
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="styleBox">
                <div class="styleTop">
                    <p class="fl">
                        Uzman görüşü içi uygun sitili seçiniz</p>
                    <asp:DropDownList ID="dpSablonlar" runat="server" CssClass="fl" onchange="SablonInputHtmlGetir(this.value);">
                    </asp:DropDownList>
                </div>
                <div class="styles">
                </div>
                <input id="btnUzmanGorusuEkle" onclick="UzmanGorusuEkle();" type="button" value="Ekle"
                    class="btn3" />
            </div>
            <!----ÖN İZLEME---->
            <div class="previewBox">
                <div id="copSepeti" class="trash fl">
                </div>
                <div class="preview fl">
                    <%--HTML UZMAN--%>
                    <div id="uzmanGorusu">
                        <div class="uzmanGorusuTitle">
                        </div>
                        <ul>
                        </ul>
                    </div>
                    <%--HTML UZMAN--%>
                </div>
                <input id="btnUzmanGorusuKaydet" onclick="UzmanGorusuKaydet();" type="button" value="Kaydet"
                    class="btn3 fr" />
            </div>
            <!----ÖN İZLEME---->
        </asp:Panel>
    </div>
    <asp:HiddenField ID="hdnUrunId" runat="server" />
    <asp:HiddenField ID="hdnUrunAd" runat="server" />
    <asp:HiddenField ID="hdnUrunResimAd" runat="server" />
    <asp:HiddenField ID="hdnUzmanGorusuBaslik" runat="server" />
    <input id="hdnSablonHtml" type="hidden" />
    <input id="hdnSablonParametre" type="hidden" />
    <script type="text/javascript">
        function UzmanGorusuKaydet() {
            var UzmanHtml = escape($('.previewBox .preview').html());
            var UrunId = $("#<%=hdnUrunId.ClientID %>").val();

            if (UzmanHtml != "undefined" && UrunId != "undefined") {
                $.ajax({
                    type: 'POST',
                    url: '/Admin/UzmanSihirbazi.aspx/UzmanGorusuKaydet',
                    data: "{ 'UrunId' : '" + UrunId + "', 'UzmanGorusuHtml' : '" + UzmanHtml + "'}",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        if (result.d[0] == "True") {
                            $("#<%=txtStokKod.ClientID %>").val('');
                            $("#<%=pnlUzmanGorusuIcerik.ClientID %>").hide();
                            $('.previewBox .preview').html('');
                            $('.previewBox').hide();
                            $("#<%=lblUrunAd.ClientID %>").text('');
                            $("#<%=hdnUrunId.ClientID %>").val('');
                            $("#<%=hdnUrunAd.ClientID %>").val('');
                            $("#<%=hdnUrunResimAd.ClientID %>").val('');
                            $("#<%=hdnUzmanGorusuBaslik.ClientID %>").val('');
                            $("#hdnSablonHtml").val('');
                            $("#hdnSablonParametre").val('');
                            $(".photoBox").hide();
                            $(".styleBox").hide();
                            alert('Uzman görüşü başarıyla kaydedildi.');
                        }
                        else
                            alert('Bir hata oluştu : ' + result.d[1]);

                    },
                    error: function (err) {
                        alert('Bir hata oluştu.');
                    }
                });
            }



        }

        function SablonInputHtmlGetir(SablonId) {
            if (SablonId != null && SablonId != "" && SablonId > 0) {
                $('.styleBox .styles').hide();
                $("#btnUzmanGorusuEkle").hide();
                $.ajax({
                    type: 'POST',
                    url: '/Admin/UzmanSihirbazi.aspx/SablonInputHtmlGetir',
                    data: '{ "SablonId" : "' + SablonId + '"}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        $('.styleBox .styles').html(result.d.SablonInputHtml);
                        $('#hdnSablonHtml').val(result.d.SablonHtml);
                        $('#hdnSablonParametre').val(result.d.SablonParametre);
                    },
                    complete: function () {
                        $('.styleBox .styles').fadeIn('600');
                        $("#btnUzmanGorusuEkle").fadeIn('600');
                        $(".droppablePhoto").droppable({
                            drop: function (event, ui) {
                                $(".droppablePhoto").attr("src", ui.draggable.attr('src'));
                            }
                        });
                    },
                    error: function (err) {
                    }
                });


                $("#btnUzmanGorusuEkle").show();
            }
            else {
                $("#btnUzmanGorusuEkle").hide();
                $('.styleBox .styles').fadeOut('400');
                $('.styleBox .styles').html('');
                $('#hdnSablonHtml').val('');
                $('#hdnSablonParametre').val('');

            }
        }

        function UzmanGorusuEkle() {
            var Parametreler = $('#hdnSablonParametre').val().split(',');
            var SablonHtml = $('#hdnSablonHtml').val();

            $('.previewBox .uzmanGorusuTitle').html($("#<%=hdnUzmanGorusuBaslik.ClientID %>").val());

            $('.styleBox .styles').slideUp(400);

            $.each(Parametreler, function (index, value) {
                if (value.indexOf("img") > -1) {
                    SablonHtml = SablonHtml.replace(new RegExp("#imgAltTitle#", "g"), $("#<%=hdnUrunAd.ClientID %>").val());
                    SablonHtml = SablonHtml.replace(new RegExp("#" + value + "#", "g"), $("#" + value).attr('src'))
                }

                if (value.indexOf("txtIcerik") > -1) {
                    SablonHtml = SablonHtml.replace(new RegExp("#" + value + "#", "g"), $("#" + value).val().replace(new RegExp("\n", "g"), "<br />"));
                }
                else
                    SablonHtml = SablonHtml.replace(new RegExp("#" + value + "#", "g"), $("#" + value).val());
            });

            $('#uzmanGorusu ul').append(SablonHtml);

            $('.styleBox .styles').html('');
            $('#hdnSablonHtml').val('');
            $('#hdnSablonParametre').val('');

            $("#copSepeti").droppable({
                //hoverClass: "trashHover",
                tolerance: 'touch',
                over: function () {
                    $(this).removeClass('trash').addClass('trashHover');
                },
                out: function () {
                    $(this).removeClass('trashHover').addClass('trash');
                },
                drop: function (event, ui) {
                    if (confirm('Silmek istediğinizden emin misiniz?'))
                        ui.draggable.remove();
                }
            });


            $(".previewBox").show('250');
            $("#<%=dpSablonlar.ClientID %>").val("0");


            $("#uzmanGorusu ul").sortable({
                scroll: false
            });

            $("#uzmanGorusu ul").draggable({
                revert: true,
                appendTo: 'body',
                helper: 'clone',
                containment: '.previewBox',
                scroll: false,
                refreshPositions: true
            });

        }

        function UzmanGorusuBaslikEkle() {
            var UzmanGorusuBaslik = $('#txtUzmanGorusuBaslik').val();
            $("#<%=hdnUzmanGorusuBaslik.ClientID %>").val(UzmanGorusuBaslik);
            if (UzmanGorusuBaslik != "") {
                $(".photoBox").show(300);
                $(".styleBox").show(300);
            }

        }

        function htmlEncode(value) {
            //create a in-memory div, set it's inner text(which jQuery automatically encodes)
            //then grab the encoded contents back out.  The div never exists on the page.
            return $('<div/>').text(value).html();
        }
    </script>
</asp:Content>
