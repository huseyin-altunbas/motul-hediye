﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="KargoTakip.aspx.cs" Inherits="Admin_KargoTakip" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="styles/datepicker.css" type="text/css" />
    <script type="text/javascript" src="js/datePicker/datepicker.js"></script>
    <script type="text/javascript" src="js/datePicker/eye.js"></script>
    <script type="text/javascript" src="js/datePicker/layout.js?ver=1.0.3"></script>
    <div class="calendarDiv">
        <div id="tarihAralik" class="newCalendar">
        </div>
        <div class="calendarBTNdiv">
            <asp:Button ID="btnGetir" runat="server" Text="Ara" OnClientClick="tarihGetir();"
                OnClick="btnGetir_Click" CssClass="aramaBTN fr" />
            <a href="#" id="clearSelection" class="aramaBTN2 fr">Seçimi Temizle</a>
        </div>
    </div>
    <div class="faturaNoDiv">
        <span class="faturaNoTitle fl">Fatura No :</span>
        <asp:TextBox ID="txtFaturaNo" runat="server" CssClass="kargoInput fl"></asp:TextBox>
        <asp:Button ID="btnGetirFaturaNo" runat="server" Text="Ara" OnClick="btnGetirFaturaNo_Click"
            CssClass="aramaBTN fl" />
    </div>
    <table width="%100">
        <tr>
            <td>
                <asp:GridView ID="grdKargoTakip" runat="server" AutoGenerateColumns="False" Width="350px">
                    <Columns>
                        <asp:BoundField DataField="belge_tarih" DataFormatString="{0:dd.MM.yyy}" HeaderText="Tarih" />
                        <asp:BoundField DataField="belge_no" HeaderText="Fatura No" />
                        <asp:BoundField DataField="kargo_takipno" HeaderText="Kargo Takip No" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnTarih" runat="server" />
    <script type="text/javascript">
        function tarihGetir() {
            $("#<%=hdnTarih.ClientID %>").val($('#tarihAralik').DatePickerGetDate(true));
        }
    </script>
</asp:Content>
