﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="UyeGonder.aspx.cs" Inherits="Admin_UyeGonder" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            var $allCheckbox = $('.allCheckbox :checkbox');
            var $checkboxes = $('.singleCheckbox :checkbox');
            $allCheckbox.change(function () {
                if ($allCheckbox.is(':checked')) {
                    $checkboxes.attr('checked', 'checked');
                }
                else {
                    $checkboxes.removeAttr('checked');
                }
            });
            $checkboxes.change(function () {
                if ($checkboxes.not(':checked').length) {
                    $allCheckbox.removeAttr('checked');
                }
                else {
                    $allCheckbox.attr('checked', 'checked');
                }
            });
        });

        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

    </script>
    <div>
        <span style="font-size: 16px;"><b>Üye bilgilerini excel ile gönder.</b> </span>
    </div>
    <br />
    <asp:Button ID="btnInformation" runat="server" CssClass="testEtBTN" Text="i" OnClick="btnInformation_Click" />
    <asp:FileUpload ID="fuExcel" runat="server" />
    <asp:Button ID="btnShow" runat="server" OnClick="btnShow_Click" CssClass="testEtBTN"
        Text="Göster" />
    <asp:Button ID="btnSend" runat="server" CssClass="testEtBTN" Text="Gönder" OnClick="btnSend_Click" />
    <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" CssClass="testEtBTN"
        Text="Temizle" />
    <asp:Button ID="bntGetAllUsers" runat="server" CssClass="testEtBTN" Text="Kayıtlı Kullanıcıları Getir"
        OnClick="bntGetAllUsers_Click" />
    <br />
    <br />
    <asp:Repeater ID="rptFuExcel" runat="server">
        <HeaderTemplate>
            <table>
                <tr>
                    <th class="allCheckbox">
                        <b>
                            <asp:CheckBox ID="chkAll" runat="server" Text="Hepsi" /></b>&nbsp;&nbsp;
                    </th>
                    <th class="baslikUyeGondertd">
                        <b>Ad</b>&nbsp;&nbsp;
                    </th>
                    <th class="baslikUyeGondertd">
                        <b>Soyad</b>&nbsp;&nbsp;
                    </th>
                    <th class="baslikUyeGondertd">
                        <b>Email</b>&nbsp;&nbsp;
                    </th>
                    <th class="baslikUyeGondertd">
                        <b>Sifre</b>&nbsp;&nbsp;
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td class="singleCheckbox" style="text-align: left; border-bottom: 1px dashed #fff;
                    padding-bottom: 0px; margin-bottom: 2px; padding-top: 5px;">
                    <asp:CheckBox ID="chkRow" runat="server" />&nbsp;&nbsp;
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #fff; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:Label ID="lblKullaniciAd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "KullaniciAd")%>'></asp:Label>&nbsp;&nbsp;
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #fff; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:Label ID="lblKullaniciSoyad" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "KullaniciSoyad")%>'></asp:Label>&nbsp;&nbsp;
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #fff; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:Label ID="lblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Email")%>'></asp:Label>&nbsp;&nbsp;
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #fff; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:Label ID="lblSifre" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Sifre")%>'></asp:Label>&nbsp;&nbsp;
                </td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
    <asp:Repeater ID="rptAllUsers" runat="server" OnItemCommand="rptAllUsers_ItemCommand">
        <HeaderTemplate>
            <table>
                <tr>
                    <th class="baslikUyeGondertd">
                        <b>Ad</b>&nbsp;&nbsp;
                    </th>
                    <th class="baslikUyeGondertd">
                        <b>Soyad</b>&nbsp;&nbsp;
                    </th>
                    <th class="baslikUyeGondertd">
                        <b>Email</b>&nbsp;&nbsp;
                    </th>
                    <th class="baslikUyeGondertd">
                        <b>Sifre Reset</b>&nbsp;&nbsp;
                    </th>
                    <th class="baslikUyeGondertd">
                        <b>Aktif</b>&nbsp;&nbsp;
                    </th>
                    <th class="baslikUyeGondertd">
                        <b>Güncelle</b>&nbsp;&nbsp;
                    </th>
                    <th class="baslikUyeGondertd">
                        <b>Kaydet</b>&nbsp;&nbsp;
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr id="trRow" runat="server">
                <td style="text-align: left; border-bottom: 1px dashed #fff; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:Label ID="lblKullaniciAd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "KullaniciAd")%>'></asp:Label>&nbsp;&nbsp;
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #fff; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:Label ID="lblKullaniciSoyad" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "KullaniciSoyad")%>'></asp:Label>&nbsp;&nbsp;
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #fff; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:Label ID="lblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Email")%>'></asp:Label>&nbsp;&nbsp;
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #fff; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:CheckBox ID="chkSifreReset" runat="server" Enabled="false" />
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #fff; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:CheckBox ID="chkAktif" runat="server" Enabled="false" Checked='<%#DataBinder.Eval(Container.DataItem, "Aktif")%>' />
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #fff; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Güncelle" CssClass="testEtBTN" />
                </td>
                <td style="text-align: left; border-bottom: 1px dashed #fff; padding-bottom: 0px;
                    margin-bottom: 2px; padding-top: 5px;">
                    <asp:Button ID="btnUpdateSave" runat="server" CommandName="UpdateSave" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "KullaniciId") + ";" +DataBinder.Eval(Container.DataItem, "Sifre")%>'
                        Text="Kaydet" CssClass="testEtBTN" />
                </td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
