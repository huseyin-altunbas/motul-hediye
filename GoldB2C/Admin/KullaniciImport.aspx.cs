﻿using Goldb2cEntity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_KullaniciImport : System.Web.UI.Page
{
    Goldb2cBLL.UyelikIslemleriBLL _bllUyelikIslemleri = new Goldb2cBLL.UyelikIslemleriBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        DateTime date = DateTime.Now;
        string path = @"C:\inetpub\wwwroot\anadoludanaspara_ftp\Import\AS_{0}{1}{2}.csv";
        path = string.Format(path, date.ToString("dd"), date.ToString("MM"), date.ToString("yyyy"));


        List<string> lines = new List<string>();
        StreamReader SW = new StreamReader(path, Encoding.GetEncoding("iso-8859-9"), false);
        string satir;
        while ((satir = SW.ReadLine()) != null)
        {
            lines.Add(satir);
        }
        SW.Close();



        foreach (string line in lines)
        {
            string[] _lines = line.Split(';');
            string acenteKodu = _lines[0];
            string acenteAdi = _lines[1];
            int kodu = Convert.ToInt32(_lines[2]);
            string telefon = _lines[3];
            string mail = _lines[4];

            bool Aktif = false;

            if (kodu < 30)
            {
                Aktif = true;
            }

            int uyeId = _bllUyelikIslemleri.MusteriKullaniciKayit(
                acenteAdi,
               "",
                mail,
                "747474",
                DateTime.Parse("01.01.1900"),
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                telefon,
                false,
                false,
                "",
                "",
                "",
                "",
                acenteKodu,
                Aktif
              );
        }



    }
}