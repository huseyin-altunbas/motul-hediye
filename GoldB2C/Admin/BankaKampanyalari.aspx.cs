﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;

public partial class Admin_BankaKampanyalari : System.Web.UI.Page
{
    AdminGenelBLL bllAdminGenel = new AdminGenelBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            TaksitDropdownDoldur();
            BankaKampanyaGetir();
        }
    }

    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        BankaKampanyaKaydet();
    }

    private void BankaKampanyaGetir()
    {
        DataTable dtBankaKampanya = new DataTable();
        dtBankaKampanya = bllAdminGenel.GetirBankaKampanya();

        ContentPlaceHolder cp = (ContentPlaceHolder)Page.Master.FindControl("ContentPlaceHolder1");

        if (dtBankaKampanya != null && dtBankaKampanya.Rows.Count > 0)
        {
            foreach (DataRow dr in dtBankaKampanya.Rows)
            {
                string BankaKod = dr["BankaKod"].ToString();

               // ((TextBox)cp.FindControl("txtKampanyaBaslik" + BankaKod)).Text = dr["KampanyaBaslik"].ToString();
                ((TextBox)cp.FindControl("txtKampanyaIcerik" + BankaKod)).Text = dr["KampanyaIcerik"].ToString();
                ((DropDownList)cp.FindControl("dpTaksit" + BankaKod)).Items.FindByValue(dr["KampanyaTaksitSayi"].ToString()).Selected = true;
                ((Image)cp.FindControl("ImgTaksit" + BankaKod)).ImageUrl = "/ImagesGld/BankCampaigns/Kampanya" + BankaKod + dr["KampanyaTaksitSayi"].ToString() + ".png";
            }
        }
    }

    private void BankaKampanyaKaydet()
    {
        string[] KampanyaBankalari = new string[] { "Paraf", "Maximum", "Bonus", "World", "CardFinans", "Axess", "AsyaCard", "Advantage", "VakifWorld" };

        ContentPlaceHolder cp = (ContentPlaceHolder)Page.Master.FindControl("ContentPlaceHolder1");
        foreach (string BankaKod in KampanyaBankalari)
        {
            //string KampanyaBaslik = ((TextBox)cp.FindControl("txtKampanyaBaslik" + BankaKod)).Text.Trim();
            string KampanyaIcerik = ((TextBox)cp.FindControl("txtKampanyaIcerik" + BankaKod)).Text.Trim();
            int KampanyaTaksitSayi = Convert.ToInt32(((DropDownList)cp.FindControl("dpTaksit" + BankaKod)).SelectedValue);
            string KampanyaSolGorsel = "/ImagesGld/BankCampaigns/Kampanya" + BankaKod + KampanyaTaksitSayi.ToString() + ".png";

            bllAdminGenel.GuncelleBankaKampanya(BankaKod, "", KampanyaIcerik, KampanyaTaksitSayi, KampanyaSolGorsel);
        }

        BankaKampanyaGetir();
    }

    public void TaksitDropdownDoldur()
    {
        for (int i = 2; i <= 18; i++)
        {
            dpTaksitParaf.Items.Add(new ListItem(i.ToString() + " Taksit", i.ToString()));
            dpTaksitMaximum.Items.Add(new ListItem(i.ToString() + " Taksit", i.ToString()));
            dpTaksitBonus.Items.Add(new ListItem(i.ToString() + " Taksit", i.ToString()));
            dpTaksitWorld.Items.Add(new ListItem(i.ToString() + " Taksit", i.ToString()));
            dpTaksitCardFinans.Items.Add(new ListItem(i.ToString() + " Taksit", i.ToString()));
            dpTaksitAxess.Items.Add(new ListItem(i.ToString() + " Taksit", i.ToString()));
            dpTaksitAsyaCard.Items.Add(new ListItem(i.ToString() + " Taksit", i.ToString()));
            dpTaksitAdvantage.Items.Add(new ListItem(i.ToString() + " Taksit", i.ToString()));
            dpTaksitVakifWorld.Items.Add(new ListItem(i.ToString() + " Taksit", i.ToString()));
        }
    }

}