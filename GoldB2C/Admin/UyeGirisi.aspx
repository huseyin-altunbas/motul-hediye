﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="UyeGirisi.aspx.cs"
    Inherits="Admin_UyeGirisi" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">
        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }
    </script>
    <title>Motul</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="/css/master.css" />
    <link rel="stylesheet" type="text/css" href="/css/stil.css" />
</head>
<body style="background-image: url('/imagesgld/girisbg.jpg'); background-color: #e5e5e5;
    background-repeat: repeat;">
    <form id="form1" runat="server">
    <div style="background-image: url('/imagesgld/avantajkulupgiriscerceve.png'); background-position: center top;
        margin-right: 15%; background-repeat: no-repeat; height: 600px; margin-left: 200px;
        margin-top: 200px;">
        <center>
            <table style="padding-left: 100px; padding-top: 50px;">
                <tr>
                    
                </tr>
                 <tr>
                    <td style="width: 100px;">
                        E-Posta Adresi
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="w230"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Şifreniz :
                    </td>
                    <td>
                        <asp:TextBox ID="txtSifre" TextMode="Password" runat="server" CssClass="w230"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--<a href="/MusteriHizmetleri/SifremiUnuttum.aspx" class="loginLink">Şifremi Unuttum</a>--%>
                    </td>
                    <td>
                        <asp:Button ID="lnbGiris" runat="server" OnClick="lnbGiris_Click" Text="Giriş Yap"
                            ValidationGroup="grpUyeGiris" />
                    </td>
                </tr>
            </table>
            <div id="lblHata" style="color: Red; padding-left: 185px; padding-top: 10px; width: 250px;">
                <div class="fleft w230 mt18" style="float: left;">
                    <div class="trans">
                        <asp:CheckBox ID="chkBeniHatirla" runat="server" />
                    </div>
                    <span class="fontSz12 chcktxt">&nbsp;Beni Hatırla</span>
                </div>
                <div class="fleft w230 h23 colorb0 fontTre" style="float: right;">
                    <span class="fright notification">
                        <asp:RequiredFieldValidator ID="rqtxtEmail" runat="server" ControlToValidate="txtEmail"
                            ErrorMessage="*E-Posta adresinizi girin." ValidationGroup="grpUyeGiris"></asp:RequiredFieldValidator>
                    </span>
                </div>
                <div class="fleft w230 h23 colorb0 fontTre" style="float: right;">
                    <span class="fright notification">
                        <asp:RequiredFieldValidator ID="rqtxtSifre" runat="server" ControlToValidate="txtSifre"
                            ErrorMessage="*Şifrenizi girin." ValidationGroup="grpUyeGiris"></asp:RequiredFieldValidator>
                    </span>
                </div>
            </div>
            <br />
            <br />
        </center>
        <br />
    </div>
    </form>
</body>
</html>
