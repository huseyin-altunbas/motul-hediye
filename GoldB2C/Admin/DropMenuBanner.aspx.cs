﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;

public partial class Admin_DropMenuBanner : System.Web.UI.Page
{
    BannerBLL BllBanner = new BannerBLL();
    TopMenuBLL BllTopMenu = new TopMenuBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            BannerlariGetir();
    }

    protected void btnKaydetItem1_Click(object sender, EventArgs e)
    {
        BannerGuncelle("item_1", txtBilgisayar.Text.Trim());
    }
    protected void btnKaydetItem2_Click(object sender, EventArgs e)
    {
        BannerGuncelle("item_2", txtTelefon.Text.Trim());
    }
    protected void btnKaydetItem3_Click(object sender, EventArgs e)
    {
        BannerGuncelle("item_3", txtEvElektronigi.Text.Trim());
    }
    protected void btnKaydetItem4_Click(object sender, EventArgs e)
    {
        BannerGuncelle("item_4", txtFotoKamera.Text.Trim());
    }
    protected void btnKaydetItem5_Click(object sender, EventArgs e)
    {
        BannerGuncelle("item_5", txtBeyazEsya.Text.Trim());
    }
    protected void btnKaydetItem6_Click(object sender, EventArgs e)
    {
        BannerGuncelle("item_6", txtDigerUrunler.Text.Trim());
    }

    private void BannerlariGetir()
    {
        DataTable dtTopMenu = new DataTable();
        dtTopMenu = BllTopMenu.SelectTopMenu();

        txtBilgisayar.Text = dtTopMenu.Select("ItemId = 'item_1'")[0]["ItemBannerField"].ToString();
        txtTelefon.Text = dtTopMenu.Select("ItemId = 'item_2'")[0]["ItemBannerField"].ToString();
        txtEvElektronigi.Text = dtTopMenu.Select("ItemId = 'item_3'")[0]["ItemBannerField"].ToString();
        txtFotoKamera.Text = dtTopMenu.Select("ItemId = 'item_4'")[0]["ItemBannerField"].ToString();
        txtBeyazEsya.Text = dtTopMenu.Select("ItemId = 'item_5'")[0]["ItemBannerField"].ToString();
        txtDigerUrunler.Text = dtTopMenu.Select("ItemId = 'item_6'")[0]["ItemBannerField"].ToString();
    }

    private void BannerGuncelle(string ItemId, string ItemBannerField)
    {
        int Sonuc = 0;
        Sonuc = BllBanner.GuncelleMenuItemBanner(ItemId, ItemBannerField);
        if (Sonuc > 0)
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Banner güncellendi.')</script>");
        else
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Bir hata oluştu tekrar deneyin.')</script>");
    }

}