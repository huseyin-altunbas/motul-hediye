﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="PuanHareketleri.aspx.cs" Inherits="Admin_PuanHareketleri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <!-- CSS goes in the document HEAD or added to your external stylesheet -->
    <style type="text/css">
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #666666;
            border-collapse: collapse;
        }

            table.gridtable th {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #dedede;
            }

            table.gridtable td {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #ffffff;
            }
    </style>




    <script>
        $(document).ready(function () {
            // $(".adminContainer").css("width", "96%");
        });
    </script>

    <div style="font-size: 16px;">
        <h3>Üye Puan  Hareketleri</h3>
    </div>



    <div style="float: left; ">
        <table class="gridtable">
            <tr>
                <td>Toplam Yüklenen
                </td>

                <td>
                    <%=puanView.yuklenenFormat %> Puan
                </td>
            </tr>


            <tr>
                <td>Toplam Harcanan
                </td>

                <td>
                    <%= puanView.harcananFormat %> Puan
                </td>
            </tr>


            <tr>
                <td>Toplam Kalan
                </td>

                <td>
                    <%=puanView.kalanFormat %> Puan
                </td>
            </tr>

         <%--   <tr>
                <td>31.12.2021 de Silinecek 
                </td>

                <td>
                    <%=puanView.silinecekFormat %> Puan
                </td>
            </tr>--%>


        </table>
    </div>

    <div style="float: left; padding-left:20px;">
        <table class="gridtable">
            <tr>
                <td>Üye Telefonu</td>
                <td><%=dt.Rows[0]["CRMCustomerGUID"] %></td>
            </tr>

            <tr>
                <td>Üye Adı</td>
                <td><%=dt.Rows[0]["KullaniciAd"] %></td>
            </tr>

           <%-- <tr>
                <td>Acente Şifresi</td>
                <td><%=Sifre %></td>
            </tr>--%>
        </table>
    </div>
    <div style="clear: both">
    </div>

    <div id="gridTrancaction"></div>


    <script id="popup-editor" type="text/x-kendo-template">


    <div class="k-edit-form-container" style="display:none;">
        <div>
            <div class="k-edit-label"><label for="KullaniciId">User Id</label></div>
            <div data-container-for="KullaniciId" class="k-edit-field">
                <input type="text" class="k-input k-textbox" id="KullaniciId" name="KullaniciId" data-bind="value:KullaniciId">
            </div>
        </div>
    </div>



    <div class="k-edit-form-container">
        <div>
            <div class="k-edit-label"><label for="transactionType">Puan Tipi</label></div>
            <div data-container-for="transactionType" class="k-edit-field">
                <select name="transactionType" class="form-control" data-bind="value:transactionType" style="max-width:148px;">
                    <option value="1" selected> Aylık puan yüklemesi </option>
                    <option value="2" class = "minusPoint"> Siparişe istinaden düşülen puan </option>
                    <option value="3"> Siparişe iptaline istinaden yüklenen puan </option>
                    <option value="4"> Kampanya için yüklenen puan </option>
                    <option value="5" class = "minusPoint"> Ceza için düşülen puan </option>
                    <option value="7"> Avans puan için yüklenen puan </option>
                    <option value="8" class = "minusPoint"> Avans puan için düşülen puan </option>
                </select>
            </div>
        </div>
    </div>



   <div class="k-edit-form-container">
        <div>
            <div class="k-edit-label"><label for="period">Puan Hakediş Tarihi</label></div>
            <div data-container-for="period" class="k-edit-field">
                <input type="text" class="k-input k-textbox hasDatepicker" name="period"  id="period" data-bind="value:period">
            </div>
        </div>
    </div>


    <div class="k-edit-form-container">
        <div>
            <div class="k-edit-label"><label for="description">Açıklama</label></div>
            <div data-container-for="description" class="k-edit-field">
                <input type="text" class="k-input k-textbox" name="description" data-bind="value:description">
            </div>
        </div>
    </div>

    <div class="k-edit-form-container">
        <div>
            <div class="k-edit-label"><label for="amount">Tutar</label></div>
            <div data-container-for="amount" class="k-edit-field">
                <input type="number" class="k-input k-textbox" name="amount" data-bind="value:amount" value="0" step=".01">
                <span style="color: red; font-weight: bold; display: none;"> Puan düşürme işleminde tutar eksi değer olmalıdır. </span>
            </div>
        </div>
    </div>


    <div class="k-edit-form-container" style="display:none;">
        <div>
            <div class="k-edit-label"><label for="status">Durum</label></div>
            <div data-container-for="status" class="k-edit-field">
                <select name="status" class="form-control" data-bind="value:status" style="max-width:148px;">
                    <option value="1" selected> Aktif </option>
                </select>
            </div>
        </div>
    </div>

    </script>


    <script>
        function RunKendoTrancaction() {
            var remoteDataSource = new kendo.data.DataSource({
                pageSize: 20,
                transport: {
                    read: {
                        url: "/api/Transaction/Get?Token=<%=kullanici.ServiceKey%>",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json",
                        complete: function (data) {

                        }

                    },
                    create: {
                        url: "/api/Transaction/Post?Token=<%=kullanici.ServiceKey%>",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json",
                    },
                    update: {
                        url: "#",
                        dataType: "json",
                        type: "PUT",
                    },
                    destroy: {
                        url: "#",
                        dataType: "json",
                        type: "DELETE"
                    },
                    parameterMap: function (options, operation) {

                        operation = operation;

                        if (operation === "read") {
                            //Export settings start
                            var filter = encodeURIComponent(JSON.stringify(options));
                            $(".k-grid-customExcelExport").attr("href", '/api/Transaction/Export/?Token=<%=kullanici.ServiceKey%>&filter=' + filter)
                            $('.k-grid-customExcelExport').attr('target', '_blank');
                            //Export settings stop
                        }

                        if (operation !== "read" && options.model) {
                            return JSON.stringify(options.models[0]);
                        }
                        else {

                            return JSON.stringify(options);
                        }


                    }
                },
                filter: [{ "field": "KullaniciId", "operator": "eq", "value": <%=Request.QueryString["KullaniciId"]%> },],
                schema: {
                    data: "Data",
                    total: "Total",
                    model: {
                        id: "id",
                        fields: {
                            id: { editable: false, hidden: true },
                            created: { type: "date", editable: false, nullable: true },
                            period: { type: "date", editable: true, nullable: true },

                        }
                    }
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                error: function (e) {
                    //console.log(e);
                    //swal("", "Lütfen tüm alanları kontrol ediniz.", "info");
                }
            });

            $('#gridTrancaction').kendoGrid({
                dataSource: remoteDataSource,
                toolbar: [
                    { name: "create", text: "MANUEL PUAN EKLE" },
                    { name: 'customExcelExport', text: 'EXCEL RAPORUNU İNDİR' },

                ],
                editable: {
                    mode: "popup",
                    template: kendo.template($("#popup-editor").html())
                },

                scrollable: true,
                sortable: true,
                edit: onEdit,
                filterMenuInit: filterMenu,
                filterable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [

                    { field: "id", title: "Id", width: "60px" },
                    //{ field: "firstname", title: "Ad", editable: false, filterable: true, width: "100px" },
                    //{ field: "lastname", title: "Soyad", editable: false, filterable: true, width: "100px" },
                    //{ field: "email", title: "Email", editable: false, filterable: true, width: "200px" },
                    { field: "description", title: "Açıklama", editable: false, filterable: true, width: "200px" },
                    //{ field: "stockName", title: "Ürün Adı", editable: false, filterable: true, width: "180px" },
                    { field: "amount", title: "Puan", editable: false, filterable: true, width: "90px" },
                    //{ field: "archiveNo", title: "Arşiv No", editable: false, filterable: true, width: "110px" },
                    //{ field: "campaignName", title: "Kampanya Adı", editable: false, filterable: true, width: "150px" },
                    { field: "transactionTypeText", title: "Tip", editable: false, filterable: { ui: transactionTypeFilter }, sortable: false },
                    { field: "period", title: "Hakedis Tarihi", format: "{0:dd/MM/yyyy}", editable: true, filterable: true, width: "150px" },
                    { field: "created", title: "Oluşturma Tarihi", format: "{0:dd/MM/yyyy}", editable: false, filterable: true, width: "150px" },
                    { field: "statusText", title: "Durum", editable: false, sortable: false, filterable: { ui: statusFilter }, width: "130px" },


                    //{
                    //    command: [
                    //        { text: "Onayla", click: TransactionConfirm }

                    //    ],
                    //    title: "İşlemler",
                    //    width: "180px",
                    //    //locked: true,
                    //    //lockable: false,
                    //},

                ]
            });
           

            function onEdit(e) {
                e.container
                    .find("[name=KullaniciId]")
                    .val(<%=Request.QueryString["KullaniciId"]%>)
                    .trigger("change");

                $("#period").kendoDatePicker({

                    // display month and year in the input
                    format: "dd/MM/yyyy",
                });
                var selectDom = document.getElementsByName('transactionType')[0];
                selectDom.addEventListener('change', function (e) {
                    var getMinusPoints = e.currentTarget.options[e.currentTarget.selectedIndex].classList.contains('minusPoint');
                    var amountAlert = document.getElementsByName('amount')[0].nextElementSibling;
                    getMinusPoints ? amountAlert.style.display = "block" : amountAlert.style.display = "none";
                })
            }

            function statusFilter(element) {
                var form = element.closest("form");
                // changes the help text. (you might want to localise this)
                form.find(".k-filter-help-text:first").text("Durum seçiniz:");
                // removes the dropdown list containing the operators (contains etc)
                form.find("select").remove();
                form.find(".k-dropdown-wrap").remove();
                element.kendoDropDownList({
                    dataSource: ['Aktif', 'Pasif'],
                    optionLabel: "Durum Seçiniz",
                });
            }

            function transactionTypeFilter(element) {
                var form = element.closest("form");
                // changes the help text. (you might want to localise this)
                form.find(".k-filter-help-text:first").text("Durum seçiniz:");
                // removes the dropdown list containing the operators (contains etc)
                form.find("select").remove();
                form.find(".k-dropdown-wrap").remove();
                element.kendoDropDownList({
                    dataSource: ['Faturadan kazanılan puan.', 'Fatura iptaline istinaden silinen puan.', 'Eğitimden kazanılan puan.', 'Manuel yüklenen puan.', 'Manuel silinen puan.'],
                    optionLabel: "Tip Seçiniz",
                });

            }

            function filterMenu(e) {
                if (e.field == "created") {
                    var beginOperator = e.container.find("[data-role=dropdownlist]:eq(0)").data("kendoDropDownList");
                    beginOperator.value("gte");
                    beginOperator.trigger("change");

                    var endOperator = e.container.find("[data-role=dropdownlist]:eq(2)").data("kendoDropDownList");
                    endOperator.value("lte");
                    endOperator.trigger("change");

                    e.container.find(".k-dropdown").hide();
                }
            }

        }

        $(document).ready(function () {
            RunKendoTrancaction();
        });

    </script>



</asp:Content>

