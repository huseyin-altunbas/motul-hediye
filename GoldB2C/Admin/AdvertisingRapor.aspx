﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="AdvertisingRapor.aspx.cs" Inherits="Admin_AdvertisingRapor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="styles/AdvertisingStyle.css?v=1.0.1" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptAdvertisingMaster" runat="server">
        <HeaderTemplate>
            <table class="advertisingDetailEdit">
                <tr>
                    <th class="id">
                        Id
                    </th>
                    <th class="explanation">
                        Açıklama
                    </th>
                    <th class="active">
                        Aktif
                    </th>
                    <th class="empty">
                        &nbsp;
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr class="color1">
                <td class="id2">
                    <%#DataBinder.Eval(Container.DataItem,"AdvertisingMasterId") %>
                </td>
                <td class="explanation2">
                    <%#DataBinder.Eval(Container.DataItem,"Aciklama") %>
                </td>
                <td class="active2">
                    <input id="Checkbox1" type="checkbox" disabled="disabled" <%# DataBinder.Eval(Container.DataItem,"Aktif") == "true" ? "" : "checked='checked'" %> />
                </td>
                <td class="empty2">
                    <a href="<%#DataBinder.Eval(Container.DataItem,"SayfaUrl") %>" target="_blank" class="GoToPage">
                        Sayfaya Git</a> <a href="/Admin/AdvertisingRapor.aspx?AdvertisingMasterId=<%#DataBinder.Eval(Container.DataItem,"AdvertisingMasterId") %>"
                            class="edit">Detaylar</a>
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr>
                <td>
                    <%#DataBinder.Eval(Container.DataItem,"AdvertisingMasterId") %>
                </td>
                <td>
                    <%#DataBinder.Eval(Container.DataItem,"Aciklama") %>
                </td>
                <td>
                    <input id="Checkbox1" type="checkbox" disabled="disabled" <%# DataBinder.Eval(Container.DataItem,"Aktif") == "true" ? "" : "checked='checked'" %> />
                </td>
                <td>
                    <a href="<%#DataBinder.Eval(Container.DataItem,"SayfaUrl") %>" target="_blank">Sayfaya
                        Git</a>
                </td>
                <td>
                    <a href="/Admin/AdvertisingRapor.aspx?AdvertisingMasterId=<%#DataBinder.Eval(Container.DataItem,"AdvertisingMasterId") %>">
                        Detaylar</a>
                </td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </table></FooterTemplate>
    </asp:Repeater>
    <asp:Repeater ID="rptAdvertisingMasterRapor" runat="server">
        <HeaderTemplate>
            <table class="advertisingDetailEdit">
                <tr>
                    <th class="date">
                        Tarih
                    </th>
                    <th class="explanation1">
                        Açıklama
                    </th>
                    <th class="click">
                        Tıklanma Sayısı
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr class="reportColor">
                <td class="date1">
                    <%#DataBinder.Eval(Container.DataItem, "LogTarih", "{0:dd.MM.yyyy}")%>
                </td>
                <td class="explanation2">
                    <%#DataBinder.Eval(Container.DataItem,"Aciklama") %>
                </td>
                <td class="click1">
                    <%#DataBinder.Eval(Container.DataItem,"TiklanmaSayi") %>
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="reportColor1">
                <td class="date1">
                    <%#DataBinder.Eval(Container.DataItem, "LogTarih", "{0:dd.MM.yyyy}")%>
                </td>
                <td class="explanation2">
                    <%#DataBinder.Eval(Container.DataItem,"Aciklama") %>
                </td>
                <td class="click1">
                    <%#DataBinder.Eval(Container.DataItem,"TiklanmaSayi") %>
                </td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </table></FooterTemplate>
    </asp:Repeater>
    <asp:Repeater ID="rptAdvertisingDetayRapor" runat="server">
        <HeaderTemplate>
            <table class="advertisingDetailEdit">
                <tr>
                    <th class="date4">
                        Tarih
                    </th>
                    <th class="click2">
                        Tıklanma Sayısı
                    </th>
                    <th class="productName">
                        Ürün Ad
                    </th>
                    <th class="productIMG">
                        Görsel
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr class="reportColor2">
                <td class="date4">
                    <%#DataBinder.Eval(Container.DataItem, "LogTarih", "{0:dd.MM.yyyy}")%>
                </td>
                <td class="click2">
                    <%#DataBinder.Eval(Container.DataItem, "TiklanmaSayi")%>
                </td>
                <td class="productName">
                    <%#DataBinder.Eval(Container.DataItem, "UrunAd")%>
                </td>
                <td class="productIMG">
                    <%#DataBinder.Eval(Container.DataItem, "GorselUrl")%>
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="reportColor3">
                <td class="date4">
                    <%#DataBinder.Eval(Container.DataItem, "LogTarih", "{0:dd.MM.yyyy}")%>
                </td>
                <td class="click2">
                    <%#DataBinder.Eval(Container.DataItem, "TiklanmaSayi")%>
                </td>
                <td class="productName">
                    <%#DataBinder.Eval(Container.DataItem, "UrunAd")%>
                </td>
                <td class="productIMG">
                    <%#DataBinder.Eval(Container.DataItem, "GorselUrl")%>
                </td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>
