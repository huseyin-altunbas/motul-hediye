﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cEntity;
using Goldb2cBLL;

public partial class Admin_Admin : System.Web.UI.MasterPage
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    public Kullanici kullanici;
    protected void Page_Load(object sender, EventArgs e)
    {
         
            //if (Request.IsSecureConnection)
            //{
            //    string path = string.Format("http{0}", Request.Url.AbsoluteUri.Substring(4));
            //    Response.Redirect(path);
            //}


        if (Session["Kullanici"] != null)
        {
            kullanici = (Kullanici)Session["Kullanici"];

            //if (kullanici.UyelikTipi != 2 || kullanici.UyelikTipi == 10)
            //    Response.Redirect("/", false);

            if (kullanici.UyelikTipi != 1)
            {
                Response.Redirect("/", false);
            }
        }
        else
        {
            //UyelikIslemleriBLL.MusteriGirisineYonlendir(Request.Url.PathAndQuery);
            HttpContext.Current.Session["SonUrl"] = Request.Url.PathAndQuery;
            HttpContext.Current.Response.Redirect("~/Admin/UyeGirisi.aspx", false);
        }

    }


    protected void lnbCikis_Click(object sender, EventArgs e)
    {
        if (Session["Kullanici"] != null)
        {
            bllUyelikIslemleri.MusteriKullaniciCikis(((Kullanici)Session["Kullanici"]).KullaniciId);
            Session.Abandon();
        }

        Response.Redirect("/", false);
    }
}
