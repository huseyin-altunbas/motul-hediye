﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="DropMenuBanner.aspx.cs" Inherits="Admin_DropMenuBanner" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="panelBanner">
        <table style="margin-bottom:10px;">               
            <tr>
                <td colspan="4" class="panbelWidth6">
                    Bilgisayar Banner - <span class="note">Image boyutları Width:281px Height:486px olmalıdır.</span>
                </td>
            </tr>         
            <tr>
                <td class="panbelWidth5">
                    <asp:TextBox ID="txtBilgisayar" runat="server" TextMode="MultiLine" CssClass="dropMenuInput"></asp:TextBox>
                </td>
            </tr>   
            <tr>
                <td colspan="4" class="panbelWidth5">
                    <asp:Button ID="btnKaydetItem1" runat="server" Text="Kaydet" OnClick="btnKaydetItem1_Click" CssClass="testEtBTN fr" />
                </td>
            </tr>
        </table>

        <table style="margin-bottom:10px;">               
            <tr>
                <td colspan="4" class="panbelWidth6">
                    Telefon Banner - <span class="note">Image boyutları Width:960px Height:142px olmalıdır.</span>
                </td>
            </tr>         
            <tr>
                <td class="panbelWidth5">
                    <asp:TextBox ID="txtTelefon" runat="server" TextMode="MultiLine" CssClass="dropMenuInput"></asp:TextBox>
                </td>
            </tr>   
            <tr>
                <td colspan="4" class="panbelWidth5">
                    <asp:Button ID="btnKaydetItem2" runat="server" Text="Kaydet" OnClick="btnKaydetItem2_Click" CssClass="testEtBTN fr" />
                </td>
            </tr>
        </table>


        <table style="margin-bottom:10px;">               
            <tr>
                <td colspan="4" class="panbelWidth6">
                    Ev Elektroniği Banner - <span class="note">Image boyutları Width:832px Height:179px olmalıdır.</span>
                </td>
            </tr>         
            <tr>
                <td class="panbelWidth5">
                    <asp:TextBox ID="txtEvElektronigi" runat="server" TextMode="MultiLine" CssClass="dropMenuInput"></asp:TextBox>
                </td>
            </tr>   
            <tr>
                <td colspan="4" class="panbelWidth5">
                    <asp:Button ID="btnKaydetItem3" runat="server" Text="Kaydet" OnClick="btnKaydetItem3_Click" CssClass="testEtBTN fr" />
                </td>
            </tr>
        </table>


        <table style="margin-bottom:10px;">               
            <tr>
                <td colspan="4" class="panbelWidth6">
                    Foto Kamera Banner - <span class="note">Image boyutları Width:315px Height:153px olmalıdır.</span>
                </td>
            </tr>         
            <tr>
                <td class="panbelWidth5">
                    <asp:TextBox ID="txtFotoKamera" runat="server" TextMode="MultiLine" CssClass="dropMenuInput"></asp:TextBox>
                </td>
            </tr>   
            <tr>
                <td colspan="4" class="panbelWidth5">
                    <asp:Button ID="btnKaydetItem4" runat="server" Text="Kaydet" OnClick="btnKaydetItem4_Click" CssClass="testEtBTN fr" />
                </td>
            </tr>
        </table>

        <table style="margin-bottom:10px;">               
            <tr>
                <td colspan="4" class="panbelWidth6">
                    Beyaz Eşya Banner - <span class="note">Image boyutları Width:189px Height:359px olmalıdır.</span>
                </td>
            </tr>         
            <tr>
                <td class="panbelWidth5">
                    <asp:TextBox ID="txtBeyazEsya" runat="server" TextMode="MultiLine" CssClass="dropMenuInput"></asp:TextBox>
                </td>
            </tr>   
            <tr>
                <td colspan="4" class="panbelWidth5">
                    <asp:Button ID="btnKaydetItem5" runat="server" Text="Kaydet" OnClick="btnKaydetItem5_Click" CssClass="testEtBTN fr" />
                </td>
            </tr>
        </table>

        <table style="margin-bottom:10px;">               
            <tr>
                <td colspan="4" class="panbelWidth6">
                    BDiğer Ürünler Banner - <span class="note">Image boyutları Width:960px Height:142px olmalıdır.</span>
                </td>
            </tr>         
            <tr>
                <td class="panbelWidth5">
                    <asp:TextBox ID="txtDigerUrunler" runat="server" TextMode="MultiLine" CssClass="dropMenuInput"></asp:TextBox>
                </td>
            </tr>   
            <tr>
                <td colspan="4" class="panbelWidth5">
                    <asp:Button ID="btnKaydetItem6" runat="server" Text="Kaydet" OnClick="btnKaydetItem6_Click" CssClass="testEtBTN fr" />
                </td>
            </tr>
        </table>

    
    </div>
</asp:Content>
