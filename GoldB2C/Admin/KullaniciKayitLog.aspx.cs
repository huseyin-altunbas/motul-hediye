﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Net.Mail;


public partial class Admin_KullaniciKayitLog : System.Web.UI.Page
{
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    DataView dvSorted;
    DataTable sortedDT;
    DataTable dtKullanici;

    DataTable dtLogBilgileri;
    LogIslemleriBLL bllLogIslemleri = new LogIslemleriBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            drpKullaniciAdDoldur();
        }

    }


    protected void btnGetir_Click(object sender, EventArgs e)
    {

        int kullaniciId, olusturulanKullaniciId, kullaniciUyelikTip;
        Int32.TryParse(drpKullaniciAd.SelectedValue.ToString(), out kullaniciId);
        Int32.TryParse(drpOlusturulanKullaniciAd.SelectedValue.ToString(), out olusturulanKullaniciId);


        try
        {
            kullaniciUyelikTip = Convert.ToInt32(drpKullaniciUyelikTip.SelectedValue.ToString());
        }
        catch (Exception)
        {
            kullaniciUyelikTip = -1;
        }

        string logTarih1 = txtLogTarih1.Text.ToString().Trim();
        string logTarih2 = txtLogTarih2.Text.ToString().Trim();


        if (logTarih1 == string.Empty && logTarih2 == string.Empty)
        {
            logTarih1 = "01.01.2000";
            logTarih2 = "30.12.9999";
        }
        else if (logTarih1 != string.Empty && logTarih2 == string.Empty)
        {
            logTarih2 = logTarih1;
        }
        else if (logTarih1 == string.Empty && logTarih2 != string.Empty)
        {
            logTarih1 = "01.01.2000";
        }


        dtKullanici = bllUyelikIslemleri.GetirKullaniciAll();


        dtLogBilgileri = bllLogIslemleri.KullaniciOlusturLogBilgileriGetir(kullaniciId, kullaniciUyelikTip, logTarih1, logTarih2, olusturulanKullaniciId);

        string sorted = drpSort.SelectedValue.ToString();
        if (sorted == "LogTarih")
            sorted = sorted + " desc";
        else
            sorted = sorted + " desc" + ", LogTarih desc";

        dvSorted = dtLogBilgileri.DefaultView;
        dvSorted.Sort = sorted;
        sortedDT = dvSorted.ToTable();

        rptLogBilgileri.DataSource = sortedDT;
        rptLogBilgileri.DataBind();


    }


    protected void rptLogBilgileri_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = e.Item.DataItem as DataRowView;
            Repeater rptOlusturulanKullaniciBilgileri = (Repeater)e.Item.FindControl("rptOlusturulanKullaniciBilgileri");

            rptOlusturulanKullaniciBilgileri.DataSource = dtKullanici.Select("KullaniciId=" + Convert.ToInt32(drv.Row["OlusturulanKullaniciId"])).CopyToDataTable();
            rptOlusturulanKullaniciBilgileri.DataBind();

        }
    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + mesaj + "</td></tr></table></div>');</script>";
    }

    public void drpKullaniciAdDoldur()
    {
        DataTable dtUyeBilgileri = new DataTable();

        dtUyeBilgileri = bllUyelikIslemleri.GetirKullaniciAll();

        var sonuc = (from k in dtUyeBilgileri.AsEnumerable()
                     orderby k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad")
                     select new
                     {
                         KullaniciId = k.Field<int>("KullaniciId"),
                         KullaniciAdSoyad = string.Format("{0} {1}", k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad"))
                     }).ToList();


        drpKullaniciAd.DataTextField = "KullaniciAdSoyad";
        drpKullaniciAd.DataValueField = "KullaniciId";
        drpKullaniciAd.DataSource = sonuc;
        drpKullaniciAd.DataBind();
        drpKullaniciAd.Items.Insert(0, (new ListItem(string.Empty, "-1")));

        drpOlusturulanKullaniciAd.DataTextField = "KullaniciAdSoyad";
        drpOlusturulanKullaniciAd.DataValueField = "KullaniciId";
        drpOlusturulanKullaniciAd.DataSource = sonuc;
        drpOlusturulanKullaniciAd.DataBind();
        drpOlusturulanKullaniciAd.Items.Insert(0, (new ListItem(string.Empty, "-1")));

    }


    protected void drpKullaniciAd_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpKullaniciAd.SelectedIndex > 0 || drpOlusturulanKullaniciAd.SelectedIndex > 0)
        {
            drpKullaniciUyelikTip.Enabled = false;
        }
        else
        {
            drpKullaniciUyelikTip.Enabled = true;
        }
    }
    protected void drpKullaniciUyelikTip_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpKullaniciUyelikTip.SelectedIndex > 0)
        {
            drpKullaniciAd.Enabled = false;
            drpOlusturulanKullaniciAd.Enabled = false;
        }
        else
        {
            drpKullaniciAd.Enabled = true;
            drpOlusturulanKullaniciAd.Enabled = true;
        }
    }
    protected void drpOlusturulanKullaniciAd_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpKullaniciAd.SelectedIndex > 0 || drpOlusturulanKullaniciAd.SelectedIndex > 0)
        {
            drpKullaniciUyelikTip.Enabled = false;
        }
        else
        {
            drpKullaniciUyelikTip.Enabled = true;
        }
    }

    public bool IsValidMail(string emailaddress)
    {
        try
        {
            MailAddress m = new MailAddress(emailaddress);
            return true;
        }
        catch (FormatException)
        {
            return false;
        }
    }
}