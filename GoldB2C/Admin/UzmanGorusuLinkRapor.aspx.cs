﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using System.Text;

public partial class Admin_UzmanGorusuLinkRapor : System.Web.UI.Page
{
    PortalAdminBLL bllPortalAdmin = new PortalAdminBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            RaporHazirla();
        }
    }

    private void RaporHazirla()
    {
        string DosyaAd = "UzmanGorusuLink_" + DateTime.Now.ToShortDateString();
        string Csv = CreateCSV(bllPortalAdmin.UzmanGorHataliLink());
        if (!string.IsNullOrEmpty(Csv))
        {
            Response.Clear();
            Response.ContentType = "application/CSV";
            Response.AddHeader("content-disposition", "attachment; filename=\"" + DosyaAd + ".csv\"");
            byte[] BOM = { 0xEF, 0xBB, 0xBF }; // UTF-8 BOM karakterleri
            Response.BinaryWrite(BOM); // Türkçe karakterlerin düzgün gözükmesi için gerekli
            Response.Write(Csv);
            Response.End();
        }
    }

    public string CreateCSV(DataTable dt)
    {

        #region Export Grid to CSV

        StringBuilder sb = new StringBuilder();

        int iColCount = dt.Columns.Count;

        for (int i = 0; i < iColCount; i++)
        {
            sb.Append(dt.Columns[i]);

            if (i < iColCount - 1)
            {
                sb.Append(";");
            }
        }

        sb.AppendLine("");
        foreach (DataRow dr in dt.Rows)
        {
            for (int i = 0; i < iColCount; i++)
            {
                if (!Convert.IsDBNull(dr[i]))
                {
                    sb.Append(dr[i].ToString());
                }
                if (i < iColCount - 1)
                {
                    sb.Append(";");
                }
            }
            sb.AppendLine("");

        }

        return sb.ToString();

        #endregion

    }
}