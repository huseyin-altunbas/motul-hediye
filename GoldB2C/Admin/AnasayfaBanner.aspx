﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="AnasayfaBanner.aspx.cs" Inherits="Admin_AnasayfaBanner" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!--  BannerSlider -->
    <link rel="stylesheet" type="text/css" href="/css/nivo-slider.css" />
    <script type="text/javascript" src="/js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            $('#slider').nivoSlider();
        });
    </script>
    <!--  BannerSlider -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="padding-left: 15px; margin-top: 15px;">
        <div class="mainbanner">
            <div class="mbleft">
                <div class="slider-wrapper theme-default">
                    <div class="ribbon">
                    </div>
                    <div id="slider" class="nivoSlider" style="width: 636px; height: 338px;">
                        <asp:Literal ID="ltrBanner" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <asp:TextBox ID="txtBanner" runat="server" TextMode="MultiLine" Width="700px" Height="200px"></asp:TextBox><br />
        <asp:Button ID="btnOnizle" runat="server" Text="Önizle" OnClick="btnOnizle_Click" />
        <asp:Button ID="btnGeriAl" runat="server" Text="Geri Al" 
            onclick="btnGeriAl_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnKaydet" runat="server" Text="Kaydet" BackColor="Red" ForeColor="WhiteSmoke"
            OnClick="btnKaydet_Click" />
    </div>
</asp:Content>
