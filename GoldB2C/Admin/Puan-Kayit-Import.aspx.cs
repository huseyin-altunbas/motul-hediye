﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using Goldb2cInfrastructure;
using System.IO;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Goldb2cBLL.Custom;
using OfficeOpenXml;

public partial class Admin_UyeKayit_Import : System.Web.UI.Page
{
    BannerBLL bllBanner = new BannerBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    string RandomNumber = "Sayfayı Yenileyiniz";
    TransactionBLL transactionBLL = new TransactionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    protected void lnkUyeKayit_Click(object sender, EventArgs e)
    {
        if (flImportCSV.HasFile)
        {
            if (Path.GetExtension(flImportCSV.FileName).ToLower() != ".xlsx")
            {
                MesajScriptOlustur("Sadece CSV uzantılı dosya yükleyebilirsiniz!");
                return;
            }

            string DosyaAdi = DateTime.Now.ToString("ddMMyyyy-HHmmss-puan") + ".xlsx";
            flImportCSV.SaveAs(Server.MapPath("~/Admin/Import/" + DosyaAdi));


            FileInfo excel = new FileInfo(Server.MapPath("~/Admin/Import/" + DosyaAdi));
            List<string> rows = new List<string>();
            using (var package = new ExcelPackage(excel))
            {
                var workbook = package.Workbook;
                var myWorksheet = workbook.Worksheets.First();


                var totalRows = myWorksheet.Dimension.End.Row;
                var totalColumns = myWorksheet.Dimension.End.Column;


                for (int rowNum = 1; rowNum <= totalRows; rowNum++) //selet starting row here
                {
                    var sb = new StringBuilder(); //this is your your data
                    var row = myWorksheet.Cells[rowNum, 1, rowNum, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
                    if (row == null)
                        continue;
                    if (row.ToString().Length < 5)
                        continue;

                    sb.AppendLine(string.Join(";", row));
                    rows.Add(sb.ToString());
                }


            }




            //StreamReader reader = new StreamReader(Server.MapPath("~/Admin/Import/" + DosyaAdi), Encoding.GetEncoding("iso-8859-9"));


            int errorCount = 0;
            try
            {
                //string[] DosyaIcerigiSatirSatir = reader.ReadToEnd().Split('\n');

                //for (int i = 0; i < DosyaIcerigiSatirSatir.Length; i++)
                //{
                //    if (!string.IsNullOrEmpty(DosyaIcerigiSatirSatir[i]))
                //    {
                //        rows.Add(DosyaIcerigiSatirSatir[i]);
                //    }
                //}


                foreach (string row in rows)
                {
                    if (row == null)
                        continue;
                    if (row.Length < 5)
                        continue;
                    string[] columns = row.Split(';');

                    string acenteKodu = columns[0];
                    int puan = Convert.ToInt32(columns[1]);
                    string aciklama = columns[2];
                    string tarih = columns[3];
                    string[] tarihArray = tarih.Split('.');
                    string yil = tarihArray[2].Replace("00:00:00\r\n", "").Trim();
                    DateTime value = new DateTime(Convert.ToInt32(yil), Convert.ToInt32(tarihArray[1]), Convert.ToInt32(tarihArray[0]));

                }


            }
            catch (Exception ex)
            {
                errorCount++;

                MesajScriptOlustur("Hata oluştu! Lütfen sistem yöneticinizle görüşün; " + ex.Message);
                return;
            }
            finally {/*reader.Close();*/  }


            try
            {
                foreach (string row in rows)
                {
                    if (row == null)
                        continue;
                    if (row.Length < 5)
                        continue;
                    string[] columns = row.Split(';');
                    string acenteKodu = columns[0];
                    int puan = Convert.ToInt32(columns[1]);
                    string aciklama = columns[2];
                    string tarih = columns[3];
                    string[] tarihArray = tarih.Split('.');
                    string yil = tarihArray[2].Replace("00:00:00\r\n", "").Trim();
                    DateTime period = new DateTime(Convert.ToInt32(yil), Convert.ToInt32(tarihArray[1]), Convert.ToInt32(tarihArray[0]));


                    DataTable dtKullanici = bllUyelikIslemleri.GetirKullanici(acenteKodu);
                    if (dtKullanici.Rows.Count == 1)
                    {

                        int KullaniciId = Convert.ToInt32(dtKullanici.Rows[0]["KullaniciId"]);
                        transactionBLL.Insert(KullaniciId, "", aciklama, puan, Goldb2cEntity.Custom.Enums.Transaction.TransactionType.MonthlyPlusPoints, Goldb2cEntity.Custom.Enums.Generic.Status.Active, period);

                    }

                }
            }
            catch (Exception ex)
            {

                MesajScriptOlustur("Hata oluştu! Lütfen sistem yöneticinizle görüşün;<br />" + ex.Message);
            }


            MesajScriptOlustur("Aktarım tamamlandı.");
        }
        else
        {
            MesajScriptOlustur("Aktarılacak dosyayı seçiniz!");
        }
    }

    void MesajScriptOlustur(string mesaj)
    {
        ClientScript.RegisterStartupScript(typeof(string), "Mesaj", "<script type=\"text/javascript\"> alert('" + mesaj + "') </script>");
    }
}