﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="UrunResimEkle.aspx.cs" Inherits="Admin_UrunResimEkle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <link href="/uploadify/uploadify.css" rel="stylesheet" type="text/css" />
    <script src="/uploadify/jquery.uploadify.js" type="text/javascript" />
    <script src="/uploadify/jquery.uploadify.min.js" type="text/javascript" />
    <script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#file_upload").uploadify(
            {
                'swf': '../uploadify/uploadify.swf',
                'uploader': '../Common/ProductUpload.ashx',
                'auto': false,
                'method': 'post',
                'multi': true,
                'buttonText': 'Dosya Seç',
                'buttonClass': 'link-button-RedBtn spriteRedBtn',
                'onUploadStart': function (file) {
                    $("#file_upload").uploadify('settings', 'formData');
                },
                'onUploadSuccess': function (file, data, response) {
                    $("#file_upload_message").append(data.toString());
                }
            });

            $("#file_upload").click(function () {
                $("#file_upload_message").html("");
            });

            $("#spMessage").click(function () {
                $("#file_upload_message").html("");
            });

        });
        
    </script>
    <style type="text/css">
        .cancelUpload {
            color: #CC0000;
            font-family: Trebuchet MS;
            font-size: 14px;
        }
        .successUpload {
            color: #00CC00;
            font-family: Trebuchet MS;
            font-size: 14px;
        }
        .messageCancelUpload {
            color: #fff;
            background-color: #CC0000;
            font-family: Trebuchet MS;
            font-size: 12px;
            width: 750px;
            margin: 5px;
            padding: 5px;
        }
        .messageSuccessUpload {
            color: #fff;
            background-color: #00CC00;
            font-family: Trebuchet MS;
            font-size: 12px;
            width: 750px;
            margin: 5px;
            padding: 5px;
        }
    </style>
    <div style="font-size: 16px;">
        ÜRÜNE RESİM EKLE (<span style="font-family: Trebuchet MS; font-size: 12px; font-style: italic">
            * Desteklenen resim uzantıları : .gif, .png, .jpeg, .jpg</span>)
    </div>
    <br />
    <div>
        <a href="javascript:$('#file_upload').uploadify('upload')" class="successUpload">Seçilen
            ilk dosyayı yükle</a> | <a href="javascript:$('#file_upload').uploadify('upload', '*')"
                class="successUpload">Seçilen tüm dosyaları yükle</a> | <a href="javascript:$('#file_upload').uploadify('cancel')"
                    class="cancelUpload">Seçilen ilk dosyayı yükleme</a> | <a href="javascript:$('#file_upload').uploadify('cancel', '*')"
                        class="cancelUpload">Seçilen hiç bir dosyayı yükleme</a>
    </div>
    <br />
    <div class="upload-wrap">
        <input id="file_upload" type="file" name="file_upload" />
    </div>
    <br />
    <div style="font-family: Trebuchet MS; font-size: 14px; text-decoration: underline;">
        Dosya Yükleme Mesajları (<span id="spMessage" style="font-family: Trebuchet MS; font-size: 14px;
            color: #CC0000; cursor: pointer;">Dosya Yükleme Mesajlarını Temizle</span>)</div>
    <div id="file_upload_message">
    </div>
</asp:Content>
