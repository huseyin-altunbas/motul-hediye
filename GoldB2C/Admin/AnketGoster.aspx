﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="AnketGoster.aspx.cs" Inherits="Admin_AnketGoster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine-tr.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <div>
        <h2>
            Anketler</h2>
    </div>
    <table class="accountRoot" style="width: 940px;">
        <tr class="accordionContainer" style="width: 940px;">
            <td>
                <asp:Repeater ID="rptAnketGoster" runat="server">
                    <HeaderTemplate>
                        <table>
                            <tr class="ordertitle" style="width: 940px;">
                                <th class="title5" style="font-weight: bold; width: 650px;">
                                    Anket Sorusu
                                </th>
                                <th class="title3">
                                    Anket Tarihi
                                </th>
                                <th class="title4">
                                    Anket Sonucu
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id="trOrderChooes" runat="server" class="orderchooes" style="width: 940px; border: 1px solid #ccc;
                            background-color: White;">
                            <td class="chooes5" style="font-weight: bold; width: 650px;">
                                <%#DataBinder.Eval(Container.DataItem, "AnketSorusu")%>
                            </td>
                            <td class="chooes3">
                                <%#DataBinder.Eval(Container.DataItem, "AnketTarihi", "{0:dd.MM.yyyy}")%>
                            </td>
                            <td class="chooes4" style="font-weight: bold; text-transform: uppercase;">
                                <a style="color: #0099cc;" href="javascript:var w=window.open('/Admin/AnketSonucu.aspx?AnketId=<%#DataBinder.Eval(Container.DataItem, "AnketId")%>','','width=700,height=400');">
                                    &raquo; Anket Sonucu </a>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
</asp:Content>
