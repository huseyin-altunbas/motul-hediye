﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
using Goldb2cBLL;
using Goldb2cInfrastructure;
using System.Net;

public partial class Admin_MailBanner : System.Web.UI.Page
{
    BannerBLL bllBanner = new BannerBLL();
    UyelikIslemleriBLL bllUyeIslemleri = new UyelikIslemleriBLL();
    string[] TestKullanicilar = new string[] { "murat.balci@entelekt.com" };
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BannerlariGetir();
        }
    }

    protected void btnGeriAl_Click(object sender, EventArgs e)
    {
        BannerlariGetir();
    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        if (bllBanner.GuncelleBanner(3, txtSifremiUnuttum.Text.Trim() + "##" + txtSifremiUnuttumLink.Text.Trim()) > 0 && bllBanner.GuncelleBanner(4, txtHosgeldiniz.Text.Trim() + "##" + txtSifremiUnuttumLink.Text.Trim()) > 0 && bllBanner.GuncelleBanner(5, txtSiparisOnay.Text.Trim() + "##" + txtSiparisOnayLink.Text.Trim()) > 0)
        {
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script type=\"text/javascript\">alert('Bannerlar başarıyla güncellendi.');</script>");
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script type=\"text/javascript\">alert('Bannerlar başarıyla güncellendi.');</script>");
        }
    }
    protected void btnSifremiUnuttumTest_Click(object sender, EventArgs e)
    {
        SifremiUnuttumMailYolla();
    }
    protected void btnHosgeldinizTest_Click(object sender, EventArgs e)
    {
        HosgeldinizMailYolla();
    }
    protected void btnSiparisOnayKaydet_Click(object sender, EventArgs e)
    {
        SiparisOnayMailYolla();
    }

    private void BannerlariGetir()
    {
        string[] _MailBanner = new string[2];
        string BannerImage = "";
        string BannerLink = "";

        DataTable dtBanner;

        dtBanner = new DataTable();
        dtBanner = bllBanner.GetirBanner(3);
        if (dtBanner != null && dtBanner.Rows.Count > 0)
        {
            _MailBanner = dtBanner.Rows[0]["BannerHtml"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            BannerImage = _MailBanner[0];
            BannerLink = _MailBanner[1];
            txtSifremiUnuttum.Text = BannerImage;
            txtSifremiUnuttumLink.Text = BannerLink;
        }

        dtBanner = new DataTable();
        dtBanner = bllBanner.GetirBanner(4);
        if (dtBanner != null && dtBanner.Rows.Count > 0)
        {
            _MailBanner = dtBanner.Rows[0]["BannerHtml"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            BannerImage = _MailBanner[0];
            BannerLink = _MailBanner[1];
            txtHosgeldiniz.Text = BannerImage;
            txtHosgeldinizLink.Text = BannerLink;
        }

        dtBanner = new DataTable();
        dtBanner = bllBanner.GetirBanner(5);
        if (dtBanner != null && dtBanner.Rows.Count > 0)
        {
            _MailBanner = dtBanner.Rows[0]["BannerHtml"].ToString().Split(new string[] { "##" }, StringSplitOptions.None);
            BannerImage = _MailBanner[0];
            BannerLink = _MailBanner[1];
            txtSiparisOnay.Text = BannerImage;
            txtSiparisOnayLink.Text = BannerLink;

        }
    }

    private void HosgeldinizMailYolla()
    {
        string mailBanner = txtHosgeldiniz.Text.Trim() + "##" + txtHosgeldinizLink.Text.Trim();

        string[] _MailBanner = mailBanner.Split(new string[] { "##" }, StringSplitOptions.None);
        string BannerImage = _MailBanner[0];
        string BannerLink = _MailBanner[1];

        string strHTML = File.ReadAllText(Server.MapPath("~/MailSablon/Hosgeldin.htm"));
        strHTML = strHTML.Replace("#adsoyad#", "test test");

        StringBuilder sb = new StringBuilder();
        sb.AppendLine("<tr>");
        sb.AppendLine("<td colspan=\"4\" width=\"720\" height=\"120\">");
        sb.AppendLine("<div style=\"height: 120px\">");
        if (!string.IsNullOrEmpty(BannerLink))
            sb.AppendLine("<a href=\"" + BannerLink + "\">");
        sb.AppendLine("<img alt=\"kampanya\" src=\"http://www.motulteam.com/UrunResim/MarketingResim/banner/bilgimail/" + BannerImage + "\" style=\"border:none\"  />");
        if (!string.IsNullOrEmpty(BannerLink))
            sb.AppendLine("</a>");
        sb.AppendLine("</div>");
        sb.AppendLine("</td>");
        sb.AppendLine("</tr>");

        if (string.IsNullOrEmpty(mailBanner))
            strHTML = strHTML.Replace("#mailbanner#", "");
        else
            strHTML = strHTML.Replace("#mailbanner#", sb.ToString());


        SendEmail mail = new SendEmail();
        mail.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
        mail.MailTo = TestKullanicilar;
        mail.MailHtml = true;
        mail.MailSubject = "motulteam.com Üyelik Onay - Test Maili";

        mail.MailBody = strHTML;
        bool mailSent = mail.SendMail(false);

        // e-mail gönderildi
        if (mailSent)
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script type=\"text/javascript\">alert('bigesclub.com Üyelik Onay - Test Maili gönderilmiştir.');</script>");

        else
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script type=\"text/javascript\">alert('Sistemde bir hata oluştu. Lütfen daha sonra yeniden deneyiniz.');</script>");
    }

    private void SifremiUnuttumMailYolla()
    {
        string mailBanner = txtSifremiUnuttum.Text.Trim() + "##" + txtSifremiUnuttumLink.Text.Trim();
        try
        {
            string[] _MailBanner = mailBanner.Split(new string[] { "##" }, StringSplitOptions.None);
            string BannerImage = _MailBanner[0];
            string BannerLink = _MailBanner[1];

            string strHTML = File.ReadAllText(Server.MapPath("~/MailSablon/SifremiUnuttum.htm"));

            // şifreyi e-mail ile gönder
            strHTML = strHTML.Replace("#adsoyad#", "test" + " " + "test");
            strHTML = strHTML.Replace("#email#", "test");
            strHTML = strHTML.Replace("#sifre#", "test");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<tr>");
            sb.AppendLine("<td colspan=\"4\" width=\"720\" height=\"120\">");
            sb.AppendLine("<div style=\"height: 120px\">");
            if (!string.IsNullOrEmpty(BannerLink))
                sb.AppendLine("<a href=\"" + BannerLink + "\">");
            sb.AppendLine("<img alt=\"kampanya\" src=\"http://www.motulteam.com/UrunResim/MarketingResim/banner/bilgimail/" + BannerImage + "\" style=\"border:none\"  />");
            if (!string.IsNullOrEmpty(BannerLink))
                sb.AppendLine("</a>");
            sb.AppendLine("</div>");
            sb.AppendLine("</td>");
            sb.AppendLine("</tr>");

            if (string.IsNullOrEmpty(mailBanner))
                strHTML = strHTML.Replace("#mailbanner#", "");
            else
                strHTML = strHTML.Replace("#mailbanner#", sb.ToString());

            SendEmail mail = new SendEmail();
            mail.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
            mail.MailTo = TestKullanicilar;
            mail.MailHtml = true;
            mail.MailSubject = "motulteam.com   Sifre Hatirlatma - Test Maili";

            mail.MailBody = strHTML;
            bool mailSent = mail.SendMail(true);

            // e-mail gönderildi
            if (mailSent)
                ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script type=\"text/javascript\">alert('motulteam.com Sifre Hatirlatma - Test Maili gönderilmiştir.');</script>");
            else
                ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script type=\"text/javascript\">alert('Sistemde bir hata oluştu. Lütfen daha sonra yeniden deneyiniz.');</script>");


        }
        catch (Exception err)
        {
        }
        finally
        {

        }

    }

    private void SiparisOnayMailYolla()
    {
        string mailBanner = txtSiparisOnay.Text.Trim() + "##" + txtSiparisOnayLink.Text.Trim();

        string OdemeTur = "";
        string[] _MailBanner = mailBanner.Split(new string[] { "##" }, StringSplitOptions.None);
        string BannerImage = _MailBanner[0];
        string BannerLink = _MailBanner[1];

        string strHTML = File.ReadAllText(Server.MapPath("~/MailSablon/SiparisOnay.htm"));
        StringBuilder sbMailUrunListe = new StringBuilder();


        for (int i = 0; i < 2; i++)
        {
            if (i % 2 == 0)
            {
                sbMailUrunListe.AppendLine("<tr>");
                sbMailUrunListe.AppendLine("<td align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #E8E8E8; text-align: center;\">");
                sbMailUrunListe.AppendLine((i + 1).ToString());
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"303\" align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px;line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #E8E8E8;\">");
                sbMailUrunListe.AppendLine("test");
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"39\" align=\"center\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #E8E8E8; text-align: center;\">");
                sbMailUrunListe.AppendLine("1");
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"147\" align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #E8E8E8; text-align: right;\">");
                sbMailUrunListe.AppendLine("150 TL");
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"148\" align=\"right\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #E8E8E8; text-align: right;\">");
                sbMailUrunListe.AppendLine("150 TL");
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("</tr>");
            }
            else
            {
                sbMailUrunListe.AppendLine("<tr>");
                sbMailUrunListe.AppendLine("<td align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #F2F2F2; text-align: center;\">");
                sbMailUrunListe.AppendLine((i + 1).ToString());
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"303\" align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px;line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #F2F2F2;\">");
                sbMailUrunListe.AppendLine("test");
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"39\" align=\"center\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #F2F2F2; text-align: center;\">");
                sbMailUrunListe.AppendLine("1");
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"147\" align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #F2F2F2; text-align: right;\">");
                sbMailUrunListe.AppendLine("200 TL");
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("<td width=\"148\" align=\"right\" valign=\"middle\" style=\"color: #4E4E4E; text-align: left; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #F2F2F2; text-align: right;\">");
                sbMailUrunListe.AppendLine("200 TL");
                sbMailUrunListe.AppendLine("</td>");
                sbMailUrunListe.AppendLine("</tr>");
            }
        }



        StringBuilder sbMailOdemeListe = new StringBuilder();

        for (int i = 0; i < 2; i++)
        {

            OdemeTur = "KK";
            if (OdemeTur.IndexOf("Havale") > -1)
                OdemeTur = "HVL";
            else
                OdemeTur = "KK";

            if (i % 2 == 0)
            {

                sbMailOdemeListe.AppendLine("<tr>");
                sbMailOdemeListe.AppendLine("<td align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: right; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #E8E8E8;\">");
                sbMailOdemeListe.AppendLine("350 TL");
                sbMailOdemeListe.AppendLine("</td>");
                sbMailOdemeListe.AppendLine("<td align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: center; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #E8E8E8;\">");
                sbMailOdemeListe.AppendLine("Kredi Kartı");
                sbMailOdemeListe.AppendLine("</td>");
                sbMailOdemeListe.AppendLine("</tr>");
            }
            else
            {
                sbMailOdemeListe.AppendLine("<tr>");
                sbMailOdemeListe.AppendLine("<td align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: right; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #F2F2F2;\">");
                sbMailOdemeListe.AppendLine("350 TL");
                sbMailOdemeListe.AppendLine("</td>");
                sbMailOdemeListe.AppendLine("<td align=\"left\" valign=\"middle\" style=\"color: #4E4E4E; text-align: center; font-size: 12px; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; height: 30px; line-height: 30px; padding-left: 5px; padding-right: 5px; background-color: #F2F2F2;\">");
                sbMailOdemeListe.AppendLine("Kredi Kartı");
                sbMailOdemeListe.AppendLine("</td>");
                sbMailOdemeListe.AppendLine("</tr>");
            }

        }


        strHTML = strHTML.Replace("#SiparisYazismaUnvan#", "test");
        strHTML = strHTML.Replace("#SiparisAdres#", "test");
        strHTML = strHTML.Replace("#SiparisIlce#", "test");
        strHTML = strHTML.Replace("#SiparisSehir#", "test");
        strHTML = strHTML.Replace("#SiparisCepTel#", "test");
        strHTML = strHTML.Replace("#SiparisSabitTel#", "test");

        strHTML = strHTML.Replace("#FaturaYazismaUnvan#", "test");
        strHTML = strHTML.Replace("#FaturaAdres#", "test");
        strHTML = strHTML.Replace("#FaturaIlce#", "test");
        strHTML = strHTML.Replace("#FaturaSehir#", "test");
        strHTML = strHTML.Replace("#FaturaCepTel#", "test");
        strHTML = strHTML.Replace("#FaturaSabitTel#", "test");

        strHTML = strHTML.Replace("#SiparisTarih#", "test");
        strHTML = strHTML.Replace("#SiparisSaat#", "test");
        strHTML = strHTML.Replace("#SiparisMasterId#", "test");
        strHTML = strHTML.Replace("#Kargo#", "test");

        strHTML = strHTML.Replace("#FaturaToplam#", "test");
        strHTML = strHTML.Replace("#KdvTutar#", "test");
        strHTML = strHTML.Replace("#IndirimTutar#", "test");
        strHTML = strHTML.Replace("#GenelToplam#", "test");
        strHTML = strHTML.Replace("#Yaziyla#", "test");



        strHTML = strHTML.Replace("#OdemeListe#", sbMailOdemeListe.ToString());
        strHTML = strHTML.Replace("#UrunListe#", sbMailUrunListe.ToString());

        string MesafeliSatisSozlesmesi = SozlesmeIcerikGetir(@"http://" + Request.Url.Authority + "/Sozlesmeler/MesafeliSatis.aspx?OdemeTur=" + OdemeTur);
        strHTML = strHTML.Replace("#MesafeliSatisSozlesmesi#", MesafeliSatisSozlesmesi);

        StringBuilder sb = new StringBuilder();
        sb.AppendLine("<tr>");
        sb.AppendLine("<td colspan=\"4\" width=\"720\" height=\"120\">");
        sb.AppendLine("<div style=\"height: 120px\">");
        if (!string.IsNullOrEmpty(BannerLink))
            sb.AppendLine("<a href=\"" + BannerLink + "\">");
        sb.AppendLine("<img alt=\"kampanya\" src=\"http://www.motulteam.com/UrunResim/MarketingResim/banner/bilgimail/" + BannerImage + "\" style=\"border:none\"  />");
        if (!string.IsNullOrEmpty(BannerLink))
            sb.AppendLine("</a>");
        sb.AppendLine("</div>");
        sb.AppendLine("</td>");
        sb.AppendLine("</tr>");

        if (string.IsNullOrEmpty(mailBanner))
            strHTML = strHTML.Replace("#mailbanner#", "");
        else
            strHTML = strHTML.Replace("#mailbanner#", sb.ToString());

        SendEmail mail = new SendEmail();
        mail.MailFrom = "motulteam.com<" + CustomConfiguration.NoReply + ">";
        mail.MailTo = TestKullanicilar;
        mail.MailHtml = true;
        mail.MailSubject = "motulteam.com Siparişiniz Tamamlanmıştır - Test Maili";

        mail.MailBody = strHTML;
        bool mailSent = mail.SendMail(false);

        // e-mail gönderildi
        if (mailSent)
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script type=\"text/javascript\">alert('motulteam.com Siparişiniz Tamamlanmıştır - Test Maili gönderilmiştir.');</script>");
        else
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script type=\"text/javascript\">alert('Sistemde bir hata oluştu. Lütfen daha sonra yeniden deneyiniz.');</script>");
    }

    public static String SozlesmeIcerikGetir(String url)
    {

        HttpWebRequest myWebRequest = null;

        HttpWebResponse myWebResponse = null;

        Stream receiveStream = null;

        Encoding encode = null;

        StreamReader readStream = null;

        string text = null;



        try
        {

            myWebRequest = HttpWebRequest.Create(url) as HttpWebRequest;



            myWebRequest.Timeout = 30000;

            myWebRequest.ReadWriteTimeout = 30000;



            myWebResponse = myWebRequest.GetResponse() as HttpWebResponse;

            receiveStream = myWebResponse.GetResponseStream();

            encode = System.Text.Encoding.GetEncoding("utf-8");

            readStream = new StreamReader(receiveStream, encode);

            text = readStream.ReadToEnd().ToLower();

            if (readStream != null) readStream.Close();

            if (receiveStream != null) receiveStream.Close();

            if (myWebResponse != null) myWebResponse.Close();

        }

        catch (Exception err)
        {

            //Do Something

        }

        finally
        {

            readStream = null;

            receiveStream = null;

            myWebResponse = null;

            myWebRequest = null;

        }

        return text;

    }


}