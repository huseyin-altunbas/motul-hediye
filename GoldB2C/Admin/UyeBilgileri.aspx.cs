﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using Goldb2cBLL;

public partial class Admin_UyeBilgileri : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Request.Url.ToString().IndexOf("entegrepuan.com") > 0)
        //{
        //    Response.Redirect("EntegreUye.aspx");
        //}

        if (!IsPostBack)
        {
            Calendar1.SelectedDate = DateTime.Today.AddDays(-30);
            Calendar2.SelectedDate = DateTime.Today;

            tarihh1.Value = Calendar1.SelectedDate.ToString("MM.dd.yyyy") + " 00:00";
            tarihh2.Value = Calendar2.SelectedDate.ToString("MM.dd.yyyy") + " 23:59";


            //if (Session["giris"].ToString() != "alcikart")
            //{
            //    //SqlConnection ocon = new SqlConnection(ConfigurationManager.ConnectionStrings["Test"].ConnectionString);
            //    //ocon.Open();
            //    //SqlCommand sql = new SqlCommand("ProjeListesi", ocon);
            //    //sql.CommandType = CommandType.StoredProcedure;
            //    //SqlDataReader dr = sql.ExecuteReader();
            //    //ddlKurumlar.DataSource = dr;
            //    //ddlKurumlar.DataTextField = "Proje";
            //    //ddlKurumlar.DataValueField = "KurumKodu";
            //    //ddlKurumlar.DataBind();


            //}
            //else
            //    ddlKurumlar.Items.Add("LD-325726");

            //ddlKurumlar.SelectedIndex = 1;

            //ListeGuncelle();

            BilgiGuncelle();
        }
    }

    private void BilgiGuncelle()
    {
        try
        {
            string kurum = "LD-325726"; //Lafarge Alçıkart  // ddlKurumlar.SelectedValue.ToString();
            SqlConnection ocon = new SqlConnection(ConfigurationManager.ConnectionStrings["Test"].ConnectionString);
            ocon.Open();
            SqlCommand sql = new SqlCommand("uye_say", ocon);
            sql.CommandType = CommandType.StoredProcedure;
            sql.Parameters.Add("@kurum", SqlDbType.NVarChar).Value = kurum;
            SqlDataReader dr = sql.ExecuteReader();

            if (dr.Read())
            {
                lblToplamUye.Text = dr["uyeSayisi"].ToString();
                lblToplamGiris.Text = (Convert.ToInt32(dr["girisSayisi"].ToString()) - Convert.ToInt32(dr["uyeSayisi"].ToString())).ToString();
                lblIlkUyelik.Text = dr["ilkUyelik"].ToString();
                lblSonUyelik.Text = dr["sonUyelik"].ToString();
                lblSonGiris.Text = dr["sonGiris"].ToString();
                lblSon30GunGiris.Text = dr["son30Gun"].ToString() + " Aktif Kullanıcı";
                lblToplamSiparis.Text = dr["toplamSiparis"].ToString() + " Adet (Son 30 Gün)";
                lblKargolananSiparis.Text = dr["kargolananSiparis"].ToString() + " Adet (Son 30 Gün)";
                lblToplamTutar.Text = dr["toplamTutar"].ToString() + " (Son 30 Gün)";

            }
        }
        catch (Exception ex)
        {
            lblToplamUye.Text = "-";
            lblToplamGiris.Text = "-";
            lblIlkUyelik.Text = "-";
            lblSonUyelik.Text = "-";
            lblSonGiris.Text = "-";
            lblSon30GunGiris.Text = "-";
            lblToplamSiparis.Text = "-";
            lblKargolananSiparis.Text = "-";
            lblToplamTutar.Text = "-";

        }

    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //***(override) Render için gerekli..
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=LD-325726_Uyeler.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        GridView1.AllowPaging = false;
        GridView1.DataBind();
        //Başlık rowlarının arka planını beyaz olarak ayarlıyoruz. 
        GridView1.HeaderRow.Style.Add("background-color", "#FFFFFF");
        //Şimdide hücre başlıklarının arka planını sarı yapıyoruz 
        GridView1.HeaderRow.Cells[0].Style.Add("background-color", "#5d7b9d");
        GridView1.HeaderRow.Cells[1].Style.Add("background-color", "#5d7b9d");
        GridView1.HeaderRow.Cells[2].Style.Add("background-color", "#5d7b9d");
        GridView1.HeaderRow.Cells[3].Style.Add("background-color", "#5d7b9d");
        GridView1.HeaderRow.Cells[4].Style.Add("background-color", "#5d7b9d");
        GridView1.HeaderRow.Cells[5].Style.Add("background-color", "#5d7b9d");
        GridView1.HeaderRow.Cells[6].Style.Add("background-color", "#5d7b9d");

        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            GridViewRow row = GridView1.Rows[i];
            //Arka plan rengini beyaz olarak ayarlıyoruz 
            row.BackColor = System.Drawing.Color.White;
            //Her row’un text özelliğine bir class atıyoruz 
            row.Attributes.Add("class", "textmode");
            //Biraz daha güzellik katmak için 2. Row’ların arka planlarına farklı bir renk veriyoruz 
            if (i % 2 != 0)
            {
                row.Cells[0].Style.Add("background-color", "#e9e9e9");
                row.Cells[1].Style.Add("background-color", "#e9e9e9");
                row.Cells[2].Style.Add("background-color", "#e9e9e9");
                row.Cells[3].Style.Add("background-color", "#e9e9e9");
                row.Cells[4].Style.Add("background-color", "#e9e9e9");
                row.Cells[5].Style.Add("background-color", "#e9e9e9");
                row.Cells[6].Style.Add("background-color", "#e9e9e9");
            }
            else
            {
                row.Cells[0].Style.Add("background-color", "#e0e0e0");
                row.Cells[1].Style.Add("background-color", "#e0e0e0");
                row.Cells[2].Style.Add("background-color", "#e0e0e0");
                row.Cells[3].Style.Add("background-color", "#e0e0e0");
                row.Cells[4].Style.Add("background-color", "#e0e0e0");
                row.Cells[5].Style.Add("background-color", "#e0e0e0");
                row.Cells[6].Style.Add("background-color", "#e0e0e0");
            }
        }
        GridView1.RenderControl(hw);
        ////Sayısal formatların bozuk çıkmaması için format belirliyoruz 
        string style = @" ";
        Response.Write(style);
        Response.Output.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }

    protected void uye_rapor_ItemDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label label = (Label)e.Row.FindControl("lblSifre");
            label.Text = SifreDonustur(label.Text);

            Label label2 = (Label)e.Row.FindControl("lblKartNo");
            label2.Text = "'" + label2.Text + "'";
        }


        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{
        //   string sifre = ((Label)e.Item.FindControl("lblSifre")).Text;
        //   ((Label)e.Item.FindControl("lblSifre")).Text = SifreDonustur(sifre);           
        //}
    }

    private string SifreDonustur(string kripto)
    {
        try
        {
            string encryptKey = "OPFBVHFAKJSDFUAEPNVCHWEFAFDUYVUHJG";  // encrypt key
            Crypt crypt = new Crypt();
            string sifre = crypt.DecryptData(encryptKey, kripto);
            return sifre;
        }
        catch (Exception ex)
        {
            //lblHata.Text += ex.Message;
            return "";
        }
    }

    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        tarihh1.Value = Calendar1.SelectedDate.ToString("MM.dd.yyyy") + " 00:00";
        tarihh2.Value = Calendar2.SelectedDate.ToString("MM.dd.yyyy") + " 23:59";
    }
    protected void Calendar2_SelectionChanged(object sender, EventArgs e)
    {
        tarihh1.Value = Calendar1.SelectedDate.ToString("MM.dd.yyyy") + " 00:00";
        tarihh2.Value = Calendar2.SelectedDate.ToString("MM.dd.yyyy") + " 23:59";
    }
}