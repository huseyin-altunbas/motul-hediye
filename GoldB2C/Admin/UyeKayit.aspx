﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="UyeKayit.aspx.cs" Inherits="Admin_UyeKayit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/css/goldForms.css?v=2" rel="stylesheet" type="text/css" />
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">
        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }
    </script>
    <!-- loginforms -->
    <div class="w764 mAuto clearfix mb50 fontTre">
        <div class="formTxt w764 fontSz13 color4e ml28 strong">
            Üyelik Formu
        </div>
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Adınız</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtAd" runat="server" CssClass="w230"></asp:TextBox>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqtxtAd" runat="server" ControlToValidate="txtAd"
                        ErrorMessage="*Adınızı girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                </span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Soyadınız</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtSoyad" runat="server" CssClass="w230 pr5 mr8"></asp:TextBox>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqtxtSoyad" runat="server" ControlToValidate="txtSoyad"
                        ErrorMessage="*Soyadınızı girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                </span>
            </div>
        </div>


        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">E-Posta</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="w230 pr5 mr8"></asp:TextBox>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqtxtEmail" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="*E-Posta adresinizi girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regextxtEmail" runat="server" ErrorMessage="*E-Posta adresinizi kontrol edin."
                        ControlToValidate="txtEmail" Display="Dynamic" ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z\-])[-\.\w]*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
                        ValidationGroup="grpUyelik"></asp:RegularExpressionValidator>
                </span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">E-Posta (tekrar)</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtEmailTekrar" runat="server" CssClass="w230 pr5 mr8"></asp:TextBox>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqtxtEmailTekrar" runat="server" ControlToValidate="txtEmailTekrar"
                        ErrorMessage="*E-Posta adresinizi girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cmptxtEmailTekrar" runat="server" ErrorMessage="*E-Posta adresinizi tekrar girin."
                        ControlToValidate="txtEmailTekrar" ControlToCompare="txtEmail" Display="Dynamic"
                        ValidationGroup="grpUyelik"></asp:CompareValidator>
                </span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Şifreniz</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtSifre" runat="server" CssClass="w230" TextMode="Password"></asp:TextBox>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqtxtSifre" runat="server" ControlToValidate="txtSifre"
                        ErrorMessage="*Şifrenizi girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                </span>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Şifreniz (tekrar)</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtSifreTekrar" runat="server" CssClass="w230 pr5 mr8" TextMode="Password"></asp:TextBox>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqSifreTekrar" runat="server" ControlToValidate="txtSifreTekrar"
                        ErrorMessage="*Şifrenizi girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cmptxtSifreTekrar" runat="server" ErrorMessage="*Şifrenizi tekrar girin."
                        ControlToValidate="txtSifreTekrar" ControlToCompare="txtSifre" Display="Dynamic"
                        ValidationGroup="grpUyelik"></asp:CompareValidator>
                </span>
            </div>
        </div>
        <!-- input -->


        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Cep Tel</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:TextBox ID="txtCepTel" runat="server" TextMode="Number" CssClass="w230 pr5 mr8"></asp:TextBox>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCepTel"
                        ErrorMessage="*Cep telefonu giriniz." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                </span>
            </div>
        </div>

        <!-- input -->
        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Üyelik Tipi</span>
            </div>
            <div class="fleft w230 clearfix">
                <asp:DropDownList ID="drpUyelikTipi" runat="server">
                    <asp:ListItem Value="0">Kullanıcı</asp:ListItem>
                    <asp:ListItem Value="10">WebAdmin</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <!-- input -->
        <!-- input -->
        <%--        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Doğum Tarihi</span>
            </div>
            <div class="fleft w350 clearfix">
                <div class="fleft">
                    <asp:DropDownList ID="dpDogumGun" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="fleft mr5 mt3 pt4">
                    /</div>
                <div class="fleft">
                    <asp:DropDownList ID="dpDogumAy" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="fleft mr5 mt3 pt4">
                    /</div>
                <div class="fleft">
                    <asp:DropDownList ID="dpDogumYil" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqdpDogumGun" InitialValue="0" runat="server" ControlToValidate="dpDogumGun"
                        ErrorMessage="*Doğduğunuz günü seçin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>&nbsp;
                    <asp:RequiredFieldValidator ID="rqdpDogumAy" InitialValue="0" runat="server" ControlToValidate="dpDogumAy"
                        ErrorMessage="*Doğduğunuz ayı seçin." ValidationGroup="grpUyelik" Display="Dynamic"></asp:RequiredFieldValidator>&nbsp;
                    <asp:RequiredFieldValidator ID="rqdpDogumYil" InitialValue="0" runat="server" ControlToValidate="dpDogumYil"
                        ErrorMessage="*Doğduğunuz yılı seçin." ValidationGroup="grpUyelik" Display="Dynamic"></asp:RequiredFieldValidator>
                </span>
            </div>
        </div>--%>
        <!-- input -->
        <!-- input -->
        <%--<div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Cinsiyet</span>
            </div>
            <div class="fleft w230 clearfix">
                <table border="0">
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbKadin" Text="Kadın" runat="server" Checked="true" GroupName="Cinsiyet" />
                        </td>
                        <td>
                            <asp:RadioButton ID="rbErkek" Text="Erkek" runat="server" GroupName="Cinsiyet" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification"></span>
            </div>
        </div>--%>
        <!-- input -->
        <!-- input -->
        <%--        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Şehir</span>
            </div>
            <div class="fleft w350 clearfix">
                <div class="fleft w350 h29">
                    <asp:DropDownList ID="dpSehir" CssClass="w210" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqdpSehir" InitialValue="0" runat="server" ControlToValidate="dpSehir"
                        ErrorMessage="*Şehrinizi seçin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                </span>
            </div>
        </div>--%>
        <!-- input -->
        <!-- input -->
        <%--        <div class="fleft h45 color4e mt20 ml28 w350">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Cep Telefonu Numarası</span>
            </div>
            <div class="fleft w350 clearfix">
                <div class="fleft w35">
                    <asp:TextBox ID="txtCepTelKod" CssClass="fleft w35 pr5 mr8 ml8" Width="35px" MaxLength="3"
                        runat="server"></asp:TextBox>
                </div>
                <div class="fleft ml5 mr5 mt3 pt4">
                    /</div>
                <div class="fleft w170">
                    <asp:TextBox ID="txtCepTel" CssClass="fleft w170 pr5 mr8" Width="170px" MaxLength="7"
                        runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqtxtCepTelKod" runat="server" ControlToValidate="txtCepTelKod"
                        ErrorMessage="*Cep Telefonu kodunuzu yazın." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator><br />
                    <asp:RequiredFieldValidator ID="rqtxtCepTel" runat="server" ControlToValidate="txtCepTel"
                        ErrorMessage="*Cep Telefonu numaranızı yazın." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                </span>
            </div>
        </div>--%>
        <!-- input -->
        <!-- input -->

      <%--  <div class="fleft h45 color4e mt20 ml28 w350" style="display:none;">
            <div class="fleft w350 h23 strong">
                <span class="fleft mt3 fontSz13">Güvenlik Kodu</span>
            </div>
            <div class="fleft clearfix">
                <asp:TextBox ID="txtGuvenlikKodu" CssClass="w130" runat="server"></asp:TextBox>
            </div>
            <div class="fleft w100 ml15 clearfix">
                <a href="/MusteriHizmetleri/UyeKayit.aspx">
                    <asp:Image ID="imgCaptcha" runat="server" AlternateText="Güvenlik Resmi" />
                </a>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:RequiredFieldValidator ID="rqtxtGuvenlikKodu" runat="server" ControlToValidate="txtGuvenlikKodu"
                        ErrorMessage="*Güvenlik kodunu girin." ValidationGroup="grpUyelik"></asp:RequiredFieldValidator>
                </span>
            </div>
        </div>--%>


        <!-- input -->
        <!-- input -->
        <%--        <div class="fleft h45 color4e mt40 ml28 w350">
            <div class="fleft clearfix">
                <div class="trans">
                    <asp:CheckBox ID="chkUyelikSozlesmesi" runat="server" />
                </div>
                <span class="fontSz12 chcktxt"><a class="curhand" onclick="PopUpAc(this,'/Sozlesmeler/UyelikSozlesmesi.htm',650,450)">
                    <b>Üyelik Sözleşmesi</b></a>ni kabul ediyorum</span>
            </div>
            <div class="fleft w240 h23 colorb0 fontTre">
                <span class="fright notification">
                    <asp:CustomValidator ID="CustomValidatorSozlesme" runat="server" ClientValidationFunction='ValidateSozlesme'
                        OnServerValidate="CustomValidatorSozlesme_ServerValidate" ValidationGroup="grpUyelik"
                        ErrorMessage="Lütfen üyelik sözleşmesini kabul edin." ValidateEmptyText="True"></asp:CustomValidator>
                </span>
            </div>
        </div>--%>
        <!-- input -->
        <!-- button -->
        <div class="fleft h45 color4e mt30 ml28 w350">
        </div>
        <div class="fleft h45 color4e mt30 ml28 w350">
            <asp:LinkButton ID="lnbUyeKayit" CssClass="link-button-RedBtn spriteRedBtn" ValidationGroup="grpUyelik"
                runat="server" OnClick="lnbUyeKayit_Click"><span class="spriteRedBtn fontTre fontSz16">
                Kaydet</span></asp:LinkButton>
        </div>
        <!-- button -->
        <div class="fleft w764 mt-10 ml28">
            <div class="trans">
                <asp:CheckBox ID="chkEmailHaber" runat="server" Checked="true" />
            </div>
            <span class="fontSz12 chcktxt">Kampanyalardan e-posta ile haberdar olmak istiyorum.</span>
        </div>
        <div class="fleft w764 mt10 ml28">
            <div class="trans">
                <asp:CheckBox ID="chkCepHaber" runat="server" Checked="true" />
            </div>
            <span class="fontSz12 chcktxt">Kampanyalardan SMS ile haberdar olmak istiyorum</span>
        </div>
    </div>
    <!-- loginforms -->
    <%--    <script type="text/javascript">

        var chkSozlesmeId = '<%=chkUyelikSozlesmesi.ClientID %>';

        function ValidateSozlesme(obj, args) {
            var checkbox = $("#" + chkSozlesmeId); args.IsValid = checkbox.attr('checked');
        }

        
    </script>--%>
    <%--    <script type="text/javascript">
        $("#<%=txtCepTelKod.ClientID %>").mask("599");
        $("#<%=txtCepTel.ClientID %>").mask("9999999");
    </script>--%>
</asp:Content>
