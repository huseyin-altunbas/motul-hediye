﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class Admin_AnketSonucu : System.Web.UI.Page
{
    AnketIslemleriBLL bllAnketIslemleri = new AnketIslemleriBLL();
    DataTable dtAnketSecenekleri;
    DataTable dtAnket;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["AnketId"]))
        {
            lblAnketToplamOySayisi.Text = "";

            dtAnket = bllAnketIslemleri.AnketGetir();
            DataRow[] drAnket = dtAnket.Select("AnketId = " + Request.QueryString["AnketId"].ToString() + "");

            lblAnketSorusu.Text = drAnket[0]["AnketSorusu"].ToString();


            dtAnketSecenekleri = bllAnketIslemleri.AnketSecenekleriGetir();
            DataRow[] drAnketSecenekleri = dtAnketSecenekleri.Select("AnketId = " + Request.QueryString["AnketId"].ToString() + "");


            dtAnketSecenekleri = drAnketSecenekleri.Length > 0 ? drAnketSecenekleri.CopyToDataTable() : dtAnketSecenekleri.Clone();

            int anketToplamOySayisi = dtAnketSecenekleri.AsEnumerable().Sum(m => m.Field<int>("ToplamOy"));

            DataColumn dcAnketOyYuzdesi = new DataColumn("AnketOyYuzdesi", typeof(decimal));
            dtAnketSecenekleri.Columns.Add(dcAnketOyYuzdesi);
            DataColumn dcImgWidth = new DataColumn("imgWidth", typeof(decimal));
            dtAnketSecenekleri.Columns.Add(dcImgWidth);

            foreach (DataRow drItem in dtAnketSecenekleri.Rows)
            {
                if (Convert.ToDecimal(drItem["ToplamOy"]) == 0)
                    drItem["AnketOyYuzdesi"] = 0;
                else
                    drItem["AnketOyYuzdesi"] = decimal.Round(Convert.ToDecimal((Convert.ToDecimal(drItem["ToplamOy"]) * 100) / (Convert.ToDecimal(anketToplamOySayisi))), 2);
                drItem["imgWidth"] = Convert.ToDecimal(drItem["AnketOyYuzdesi"]) * 2;
            }

            rptAnketSonucu.DataSource = dtAnketSecenekleri;
            rptAnketSonucu.DataBind();

            lblAnketToplamOySayisi.Text = anketToplamOySayisi.ToString();
        }
    }
}