﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_KisiRaporu : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Calendar1.SelectedDate = DateTime.Today;
            ListeGuncelle();
        }

    }

    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        ListeGuncelle();
    }

    public void ListeGuncelle()
    {
        string tarih = Calendar1.SelectedDate.ToShortDateString();
        int kisiID = Int32.Parse(Request.QueryString["id"]);

        SqlConnection ocon = new SqlConnection(ConfigurationManager.ConnectionStrings["Test"].ConnectionString);
        ocon.Open();
        SqlCommand sql = new SqlCommand("kisi_gunluk_rapor", ocon);
        sql.CommandType = CommandType.StoredProcedure;
        sql.Parameters.Add("@tarih", SqlDbType.NVarChar).Value = tarih;
        sql.Parameters.Add("@kisi", SqlDbType.NVarChar).Value = kisiID;
        SqlDataReader dr = sql.ExecuteReader();
        kisi_rapor.DataSource = dr;
        kisi_rapor.DataBind();
    }
}