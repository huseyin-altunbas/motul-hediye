﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="FooterBanner.aspx.cs" Inherits="Admin_FooterBanner" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="/css/gold.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        body
        {
            background: url(images/bg_content.png) center top;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="DefaultBottomBanner">
        <ul>
                <asp:Literal ID="ltrBanner" runat="server"></asp:Literal>
        </ul>
    </div>
        <asp:TextBox ID="txtBanner" runat="server" TextMode="MultiLine" CssClass="footerBannerInput"></asp:TextBox>
        <asp:Button ID="btnOnizle" runat="server" Text="Önizle" OnClick="btnOnizle_Click"
            CssClass="aramaBTN fl" />
        <asp:Button ID="btnGeriAl" runat="server" Text="Geri Al" OnClick="btnGeriAl_Click"
            CssClass="aramaBTN fl" />
        <asp:Button ID="btnKaydet" runat="server" Text="Kaydet" OnClick="btnKaydet_Click"
            CssClass="aramaBTN fl" />
    </div>

</asp:Content>
