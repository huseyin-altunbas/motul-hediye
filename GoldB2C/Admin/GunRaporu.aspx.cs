﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_GunRaporu : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Calendar1.SelectedDate = DateTime.Today;

        string tarih = Calendar1.SelectedDate.ToShortDateString();

        bugun_listesi(tarih);
        pageview_listesi(tarih);

    }

    public void bugun_listesi(string tarih)
    {
        SqlConnection ocon = new SqlConnection(ConfigurationManager.ConnectionStrings["Test"].ConnectionString);
        ocon.Open();
        SqlCommand sql = new SqlCommand("site_gunluk_rapor_by_RefferrerId", ocon);
        sql.CommandType = CommandType.StoredProcedure;
        sql.Parameters.Add("@tarih", SqlDbType.NVarChar).Value = tarih;
        sql.Parameters.Add("@refferrerId", SqlDbType.NVarChar).Value = "LD-325726";//Lafarge Alçıkart
        SqlDataReader dr = sql.ExecuteReader();
        gun_rapor.DataSource = dr;
        gun_rapor.DataBind();
    }

    public void pageview_listesi(string tarih)
    {
        tarih = Calendar1.SelectedDate.ToShortDateString();

        SqlConnection ocon = new SqlConnection(ConfigurationManager.ConnectionStrings["Test"].ConnectionString);
        ocon.Open();
        SqlCommand sql = new SqlCommand("kuruma_gore_pageview_by_RefferrerId", ocon);
        sql.CommandType = CommandType.StoredProcedure;
        sql.Parameters.Add("@tarih", SqlDbType.NVarChar).Value = tarih;
        sql.Parameters.Add("@refferrerId", SqlDbType.NVarChar).Value = "LD-325726";//Lafarge Alçıkart
        SqlDataReader dr = sql.ExecuteReader();
        pageview_liste.DataSource = dr;
        pageview_liste.DataBind();
    }

    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        string tarih = Calendar1.SelectedDate.ToShortDateString();

        SqlConnection ocon = new SqlConnection(ConfigurationManager.ConnectionStrings["Test"].ConnectionString);
        ocon.Open();
        SqlCommand sql = new SqlCommand("site_gunluk_rapor_by_RefferrerId", ocon);
        sql.CommandType = CommandType.StoredProcedure;
        sql.Parameters.Add("@tarih", SqlDbType.NVarChar).Value = tarih;
        sql.Parameters.Add("@refferrerId", SqlDbType.NVarChar).Value = "LD-325726";//Lafarge Alçıkart
        SqlDataReader dr = sql.ExecuteReader();
        gun_rapor.DataSource = dr;
        gun_rapor.DataBind();

        pageview_listesi(tarih);
    }
}