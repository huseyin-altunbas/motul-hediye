﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="UyeBilgileri.aspx.cs" Inherits="Admin_UyeBilgileri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="styles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <table class="tablo_arama" cellspacing="0" cellpadding="4">
        <tr class="tablo_tr font10">
            <td width="700">
                <asp:Button ID="btnExcel" runat="server" Text="Tabloyu Excele Gönder" OnClick="btnExcel_Click" />
            </td>
        </tr>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <asp:GridView ID="GridView1" OnRowDataBound="uye_rapor_ItemDataBound" AllowSorting="true"
                    AllowPaging="true" PagerSettings-NextPageText="Sonraki" PagerSettings-Mode="NumericFirstLast"
                    PagerSettings-PreviousPageText="Önceki" PageSize="50" PagerSettings-Position="TopAndBottom"
                    runat="server" DataSourceID="SD1" CellSpacing="1" CssClass="tablo_liste border1"
                    HeaderStyle-CssClass="tablo_th" RowStyle-CssClass="tablo_tr" AlternatingRowStyle-CssClass="tablo_tr2"
                    AutoGenerateColumns="False" CellPadding="4" GridLines="None" EnableViewState="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Adı Soyadı" SortExpression="MusteriAd">
                            <ItemTemplate>
                                <%--<a href='EntegreUyePuan.aspx?KartNo=<%#DataBinder.Eval(Container.DataItem, "Email")%>'>--%>
                                <%#DataBinder.Eval(Container.DataItem, "MusteriAd")%>
                                <%#DataBinder.Eval(Container.DataItem, "MusteriSoyad")%><%--</a>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="E-mail" SortExpression="Email">
                            <ItemTemplate>
                                <%--<a href='EntegreUyePuan.aspx?KartNo=<%#DataBinder.Eval(Container.DataItem, "Email")%>'>
                                <asp:Label ID="lblKartNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Email")%>'></asp:Label></a>--%>
                                <div style="padding-right: 8px;">
                                    <asp:Label ID="lblKartNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Email")%>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Cep Tel" DataField="cepNo" SortExpression="cepNo" />
                        <asp:TemplateField HeaderText="Şifre" SortExpression="Sifre">
                            <ItemTemplate>
                                <div style="padding-right: 8px;">
                                    <asp:Label ID="lblSifre" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Sifre")%>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="UyelikTarihi" HeaderText="Üyelik Tarihi" SortExpression="UyelikTarihi" />
                        <asp:BoundField DataField="SonGirisTarihi" HeaderText="Son Giriş Tarihi" SortExpression="SonGirisTarihi" />
                        <asp:BoundField DataField="GirisSayisi" HeaderText="Giriş Sayısı" SortExpression="GirisSayisi" />
                        <asp:BoundField DataField="KalanPuan" HeaderText="Kalan Puan" SortExpression="KalanPuan" />
                        <%--<asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <a href='CariLog.aspx?uid=<%#DataBinder.Eval(Container.DataItem, "KullaniciID")%>'>Extre</a>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SD1" runat="server" SelectCommand="kurum_uye_list" ConnectionString="<%$ ConnectionStrings:Test %>"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter Name="kurum" ControlID="kurumkod1" PropertyName="Value" Type="String"
                            DefaultValue="LD-325726" />
                        <asp:ControlParameter Name="tarih1" ControlID="tarihh1" PropertyName="Value" Type="String"
                            DefaultValue="01/31/1900  00:00" />
                        <asp:ControlParameter Name="tarih2" ControlID="tarihh2" PropertyName="Value" Type="String"
                            DefaultValue="12/31/9999  00:00" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:HiddenField ID="kurumkod1" Value="LD-325726" runat="server" />
                <asp:HiddenField ID="tarihh1" Value="01/01/1900 00:00" runat="server" />
                <asp:HiddenField Value="12/12/2011 00:00" ID="tarihh2" runat="server" />
                <%--<asp:Repeater ID="uye_rapor" runat="server">
                    <HeaderTemplate>
                        <table class="tablo_liste border1" cellspacing="1" cellpadding="4">
                            <tr class="tablo_th">
                                <th width="150">
                                    Adı Soyadı
                                </th>
                                <th>
                                    E-mail
                                </th>
                                <th>
                                    Üyelik Tarihi
                                </th>
                                <th>
                                    Son Giriş Tarihi
                                </th>
                                <th>
                                    Giriş Adedi
                                </th>
                                <th>
                                    Puan
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="tablo_tr">
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "MusteriAd")%>
                                <%#DataBinder.Eval(Container.DataItem, "MusteriSoyad")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "Email")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "UyelikTarihi")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "SonGirisTarihi")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "GirisSayisi")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "KalanPuan")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>--%>
            </td>
            <td>
                &nbsp;
            </td>
            <td width="230">
                <strong>Rapor Başlangıç Tarihi:</strong>
                <asp:Calendar ID="Calendar1" runat="server" SelectedDate="01.01.0001" VisibleDate="01.01.0001"
                    OnSelectionChanged="Calendar1_SelectionChanged"></asp:Calendar>
                <br />
                <strong>Rapor Bitiş Tarihi:</strong>
                <asp:Calendar ID="Calendar2" runat="server" SelectedDate="01.01.0001" VisibleDate="01.01.0001"
                    OnSelectionChanged="Calendar2_SelectionChanged"></asp:Calendar>
                <br />
                <%--<asp:Button ID="btnRaporHazirla" runat="server" OnClick="btnRaporHazirla_Click" Text="Üye Raporunu Getir"
                    Width="230px" />
                <br />--%>
                <br />
                <table class="tablo_liste border1">
                    <tr class="tablo_tr">
                        <td>
                            <b>Toplam Üye </b>
                        </td>
                        <td>
                            <asp:Literal ID="lblToplamUye" Text="-" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="tablo_tr">
                        <td>
                            <b>Toplam Giriş </b>
                        </td>
                        <td>
                            <asp:Literal ID="lblToplamGiris" Text="-" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="tablo_tr">
                        <td>
                            <b>Son 30 Gün </b>
                        </td>
                        <td>
                            <asp:Literal ID="lblSon30GunGiris" Text="-" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="tablo_tr">
                        <td>
                            <b>İlk Üyelik </b>
                        </td>
                        <td>
                            <asp:Literal ID="lblIlkUyelik" Text="-" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="tablo_tr">
                        <td>
                            <b>Son Üyelik </b>
                        </td>
                        <td>
                            <asp:Literal ID="lblSonUyelik" Text="-" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="tablo_tr">
                        <td>
                            <b>Son Giriş </b>
                        </td>
                        <td>
                            <asp:Literal ID="lblSonGiris" Text="-" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="tablo_tr">
                        <td>
                            <b>Sipariş</b>
                        </td>
                        <td>
                            <asp:Literal ID="lblToplamSiparis" Text="-" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="tablo_tr">
                        <td>
                            <b>Kargolandi</b>
                        </td>
                        <td>
                            <asp:Literal ID="lblKargolananSiparis" Text="-" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="tablo_tr">
                        <td>
                            <b>Toplam Tutar</b>
                        </td>
                        <td>
                            <asp:Literal ID="lblToplamTutar" Text="-" runat="server"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
