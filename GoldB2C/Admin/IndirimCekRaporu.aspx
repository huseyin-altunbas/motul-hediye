﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="IndirimCekRaporu.aspx.cs" Inherits="Admin_IndirimCekRaporu" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .cariLink
        {
            color: red;
        }
        .cariLink:hover
        {
            text-decoration: underline;
        }
        div.loading
        {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 1;
            margin: 0 auto;
            text-align: center;
            display: block;
            width: 100%;
            height: 100%;
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine-tr.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".accordionContainer .accordionBtn:first").addClass("active");
            $(".accordionContainer .accordionDetail:not(:first)").hide();

            $(".accordionContainer .accordionBtn").click(function () {
                $(this).next(".accordionDetail").slideToggle("slow")
		.siblings(".accordionDetail:visible").slideUp("slow");
                $(this).toggleClass("active");
                $(this).siblings(".accordionBtn").removeClass("active");
            });

        });



        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

        $(function () {
            $("#<%=txtTarih1.ClientID%>").datepicker();
            $("#<%=txtTarih2.ClientID%>").datepicker();
        });

        jQuery(function ($) {
            $.datepicker.regional['tr'] = {
                closeText: 'kapat',
                prevText: '&#x3c;geri',
                nextText: 'ileri&#x3e',
                currentText: 'bugün',
                monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran',
                'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
                monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz',
                'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
                dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
                dayNamesShort: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                dayNamesMin: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                weekHeader: 'Hf',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['tr']);
        });

    </script>
    <div style="font-weight: bold;">
        İndirim Çekleri
    </div>
    <div style="clear: both;">
    </div>
    <br />
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblTarih1" runat="server" Text="Tarih">
            </asp:Label></div>
        <asp:TextBox ID="txtTarih1" runat="server"></asp:TextBox>
        ve
        <asp:Label ID="lblTarih2" runat="server" Text="">
        </asp:Label>
        <asp:TextBox ID="txtTarih2" runat="server"></asp:TextBox>
        arasında&nbsp;&nbsp;<br />
    </div>
    <div style="clear: both;">
    </div>
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblKullaniciAd" runat="server" Text="Kullanıcı"> 
            </asp:Label></div>
        <asp:DropDownList ID="drpKullaniciAd" runat="server">
        </asp:DropDownList>
    </div>
    <div style="clear: both;">
    </div>
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblIndirimFirma" runat="server" Text="Firma"> 
            </asp:Label></div>
        <asp:DropDownList ID="drpIndirimFirma" runat="server">
        </asp:DropDownList>
    </div>
    <div style="clear: both;">
    </div>
    <br />
    <div style="float: left; margin-bottom: 2px;">
        <asp:Button ID="btnGetir" runat="server" Text="Getir" class="testEtBTN" OnClick="btnGetir_Click" />
        &nbsp;&nbsp;
        <asp:Button ID="btnExcel" runat="server" Text="Excel'e Aktar" class="testEtBTN" OnClick="btnExcel_Click" />
        &nbsp;&nbsp;
    </div>
    <br />
    <br />
    <table class="accountRoot" style="width: 940px;">
        <tr class="accordionContainer" style="width: 940px;">
            <td>
                <asp:Repeater ID="rptIndirimCekBilgileri" runat="server">
                    <HeaderTemplate>
                        <table>
                            <tr class="ordertitle" style="width: 940px; height: 60px;">
                                <th class="title4" style="width: 200px;">
                                    Ad-Soyad
                                </th>
                                <th class="title4">
                                    İndirim Kodu
                                </th>
                                <th class="title4" style="width: 250px;">
                                    Gönderilen Email Adresi
                                </th>
                                <th class="title3" style="width: 80px;">
                                    Tarih
                                </th>
                                <th class="title4" style="width: 150px;">
                                    Firma
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <!-- chooes -->
                        <tr id="trOrderChooes" runat="server" class="orderchooes" style="width: 940px; border: 1px solid #ccc;">
                            <td class="chooes4" style="width: 200px;">
                                <%#DataBinder.Eval(Container.DataItem, "KullaniciAdSoyad")%>
                            </td>
                            <td class="chooes4">
                                <%#DataBinder.Eval(Container.DataItem, "CekNo")%>
                            </td>
                            <td class="chooes4" style="width: 250px;">
                                <%#DataBinder.Eval(Container.DataItem, "GonderilenEmailAdresi")%>
                            </td>
                            <td class="chooes3" style="width: 80px;">
                                <%#DataBinder.Eval(Container.DataItem, "GonderilmeTarihi", "{0:dd.MM.yyyy}")%>
                            </td>
                            <td class="chooes4" style="width: 150px;">
                                <%#DataBinder.Eval(Container.DataItem, "FirmaAd")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
</asp:Content>
