﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="KisiRaporu.aspx.cs" Inherits="Admin_KisiRaporu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="styles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <%-- RequestType = log türü, RefID = Kurum Kodu, UserAgent = isim --%>
                <asp:Repeater ID="kisi_rapor" runat="server">
                    <HeaderTemplate>
                        <table class="tablo_liste border1" cellspacing="1" cellpadding="4">
                            <tr class="tablo_th">
                                <th width="100">
                                    IP
                                </th>
                                <th width="150">
                                    İşlem
                                </th>
                                <th>
                                    Sayfa URL
                                </th>
                                <th>
                                    Saat
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="tablo_tr">
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "ClientIp")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "RequestType")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "Url")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "CreateDate","{0:HH:mm}")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
            <td>
                &nbsp;
            </td>
            <td width="230">
                <asp:Calendar ID="Calendar1" runat="server" SelectedDate="01.01.0001" VisibleDate="01.01.0001"
                    OnSelectionChanged="Calendar1_SelectionChanged"></asp:Calendar>
                <br />
                Raporu görmek istediğiniz tarihi seçiniz.<br />
                <br />
                <hr />
                <span style="color: red">Müşteri Bilgisi:</span>
                <hr />
                <asp:Repeater ID="kisi_info" runat="server">
                    <HeaderTemplate>
                        <table border="0">
                            <tr>
                                <td>
                                    <b>Kurum</b>
                                </td>
                                <td>
                                    <b>Sayfa</b>
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "ReffererID")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "PageView")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
</asp:Content>
