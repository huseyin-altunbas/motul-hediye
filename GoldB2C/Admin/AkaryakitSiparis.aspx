﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="AkaryakitSiparis.aspx.cs" Inherits="Admin_AkaryakitSiparis" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .cariLink {
            color: red;
        }
        .cariLink:hover {
            text-decoration: underline;
        }
        div.loading {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            z-index: 1;
            margin: 0 auto;
            text-align: center;
            display: block;
            width: 100%;
            height: 100%;
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/Gold/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="/js/jquery.validationEngine-tr.js" type="text/javascript"></script>
    <script src="/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".accordionContainer .accordionBtn:first").addClass("active");
            $(".accordionContainer .accordionDetail:not(:first)").hide();

            $(".accordionContainer .accordionBtn").click(function () {
                $(this).next(".accordionDetail").slideToggle("slow")
		.siblings(".accordionDetail:visible").slideUp("slow");
                $(this).toggleClass("active");
                $(this).siblings(".accordionBtn").removeClass("active");
            });

        });



        function UyariDivGoster(mesaj) {
            $(document).ready(function () {
                $.blockUI({
                    fadeIn: 600,
                    fadeOut: 600,
                    css: {
                        cursor: 'default',
                        top: ($(window).height() - 400) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: '400px'
                    },
                    message: mesaj + '</br>'
                });
            });
        }

        function unblockDivKapat() {
            $(document).ready(function () {
                $.unblockUI();
            });
        }

        $(function () {
            $("#<%=txtIlkTarih.ClientID%>").datepicker();
            $("#<%=txtSonTarih.ClientID%>").datepicker();
        });

        jQuery(function ($) {
            $.datepicker.regional['tr'] = {
                closeText: 'kapat',
                prevText: '&#x3c;geri',
                nextText: 'ileri&#x3e',
                currentText: 'bugün',
                monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran',
                'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
                monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz',
                'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
                dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
                dayNamesShort: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                dayNamesMin: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
                weekHeader: 'Hf',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['tr']);
        });


        function KartYuklemeOnay(AkaryakitKartSiparisId) {

            if (AkaryakitKartSiparisId != "") {

                var con = confirm('Kart yükleme tamamlandı mı?');
                if (con) {
                    $("body").css({ "opacity": "0.5" });
                    $("div.loading").show();

                    $.ajax({
                        type: 'POST',
                        url: '/Admin/AkaryakitSiparis.aspx/KartYuklemeOnay',
                        data: '{ "AkaryakitKartSiparisId":' + AkaryakitKartSiparisId + '}',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function (result) {
                            $("div.loading").hide();
                            $("body").css({ "opacity": "" });
                            if (result.d[0].toString() == "1") {
                                $(".KartYukleme_" + AkaryakitKartSiparisId).attr('checked', 'checked');
                                $(".KartYukleme_" + AkaryakitKartSiparisId).prop('checked', true);
                                $(".KartYukleme_" + AkaryakitKartSiparisId).attr("onclick", "").unbind("click");
                                $(".KartYukleme_" + AkaryakitKartSiparisId).prop("onclick", null);
                                $(".KartYukleme_" + AkaryakitKartSiparisId).attr('checked', 'checked');
                                $(".KartYukleme_" + AkaryakitKartSiparisId).attr('disabled', 'disabled');
                                $(".KartYukleme_" + AkaryakitKartSiparisId).prop('disabled', true);

                                $(".KartToplamYuklenen_" + result.d[1].toString()).html(result.d[2].toString());
                            }
                            else {
                                $(".KartYukleme_" + AkaryakitKartSiparisId).prop('checked', false);
                                $("." + AkaryakitKartSiparisId).removeAttr('checked');
                            }
                        },
                        error: function (result) {
                            $("div.loading").hide();
                            $("body").css({ "opacity": "" });
                            $(".KartYukleme_" + AkaryakitKartSiparisId).prop('checked', false);
                            $(".KartYukleme_" + AkaryakitKartSiparisId).removeAttr('checked');
                        }
                    });
                }
                else {
                    $(".KartYukleme_" + AkaryakitKartSiparisId).prop('checked', false);
                    $(".KartYukleme_" + AkaryakitKartSiparisId).removeAttr('checked');
                }
            }
        }

    </script>
    <div style="font-weight: bold;">
        Onaylanmış Sparişlerdeki Akaryakıt Kart Bilgileri
    </div>
    <div style="clear: both;">
    </div>
    <br />
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblKartNo" runat="server" Text="Kart No"> 
            </asp:Label></div>
        <asp:TextBox ID="txtKartNo" runat="server"></asp:TextBox>
    </div>
    <div style="clear: both;">
    </div>
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblSiparisId" runat="server" Text="Sipariş No"> 
            </asp:Label></div>
        <asp:TextBox ID="txtSiparisId" runat="server"></asp:TextBox>
    </div>
    <div style="clear: both;">
    </div>
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblKullaniciAd" runat="server" Text="Kullanıcı"> 
            </asp:Label></div>
        <asp:DropDownList ID="drpKullaniciAd" runat="server">
        </asp:DropDownList>
    </div>
    <div style="clear: both;">
    </div>
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblKartYukleme" runat="server" Text="Kart Yükleme"> 
            </asp:Label></div>
        <asp:DropDownList ID="drpKartYukleme" runat="server">
            <asp:ListItem Selected="True" Value="-1" Text=""></asp:ListItem>
            <asp:ListItem Value="1">Yükleme Yapılanlar</asp:ListItem>
            <asp:ListItem Value="0">Yükleme Yapılmayanlar</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div style="clear: both;">
    </div>
    <div style="float: left; margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblGonderimDurumu" runat="server" Text="Gönderim Durumu"> 
            </asp:Label></div>
        <asp:DropDownList ID="drpGonderimDurumu" runat="server">
            <asp:ListItem Selected="True" Value="-1" Text=""></asp:ListItem>
            <asp:ListItem Value="1">Gönderimi Tamamlananlar</asp:ListItem>
            <asp:ListItem Value="0">Gönderimi Tamamlanmayanlar</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div style="clear: both;">
    </div>
    <div style="margin-bottom: 2px;">
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblIlkTarih" runat="server" Text="Tarih"> 
            </asp:Label></div>
        <asp:TextBox ID="txtIlkTarih" runat="server"></asp:TextBox>
        ve
        <div style="width: 130px; float: left;">
            <asp:Label ID="lblSonTarih" runat="server" Text=""> 
            </asp:Label></div>
        <asp:TextBox ID="txtSonTarih" runat="server"></asp:TextBox>
        arasında
    </div>
    <div style="clear: both;">
    </div>
    <br />
    <div style="float: left; margin-bottom: 2px;">
        <asp:Button ID="btnGetir" runat="server" Text="Getir" class="testEtBTN" OnClick="btnGetir_Click" />
        &nbsp;&nbsp;
        <asp:Button ID="btnExcelBilgileri" runat="server" Text="Excel'e Aktar" class="testEtBTN"
            OnClick="btnExcelBilgileri_Click" />
        &nbsp;&nbsp;
    </div>
    <br />
    <br />
    <table class="accountRoot" style="width: 1190px; margin-left: -40px;">
        <tr class="accordionContainer" style="width: 1190px; margin-left: -40px;">
            <td>
                <asp:Repeater ID="rptAkaryakitKartBilgileri" runat="server" OnItemCommand="rptAkaryakitKartBilgileri_ItemCommand">
                    <HeaderTemplate>
                        <table>
                            <tr class="ordertitle" style="width: 1190px; margin-left: -40px; height: 60px;">
                                <th class="title2" style="width: 40px;">
                                    Kart yükleme yapıldı mı?
                                </th>
                                <th class="title1" style="width: 40px;">
                                    CariId
                                </th>
                                <th class="title1" style="width: 40px;">
                                    Sipariş No
                                </th>
                                <th class="title4">
                                    Kullanıcı Ad
                                </th>
                                <th class="title2">
                                    Plaka
                                </th>
                                <th class="title2">
                                    Yakıt Türü
                                </th>
                                <th class="title2">
                                    Tutar
                                </th>
                                <th class="title1" style="width: 40px;">
                                    Eski - Yeni
                                </th>
                                <th class="title1" style="width: 40px;">
                                    Kart Id
                                </th>
                                <th class="title4">
                                    Kart No
                                </th>
                                <th class="title2">
                                    Karta Yüklenen Toplam Tutar
                                </th>
                                <th class="title3">
                                    İrsaliye No
                                </th>
                                <th class="title3">
                                    Gönderim
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <!-- chooes -->
                        <tr id="trOrderChooes" runat="server" class="orderchooes" style="width: 1190px; margin-left: -40px;
                            border: 1px solid #ccc;">
                            <td class="chooes2" style="width: 40px;">
                                <%#DataBinder.Eval(Container.DataItem, "KartYuklemeDurumBilgisi")%>
                            </td>
                            <td class="chooes1" style="width: 40px;">
                                <%#DataBinder.Eval(Container.DataItem, "ErpCariId")%>
                            </td>
                            <td class="chooes1" style="width: 40px;">
                                <%#DataBinder.Eval(Container.DataItem, "SiparisNo")%>
                            </td>
                            <td class="chooes4">
                                <%#DataBinder.Eval(Container.DataItem, "KullaniciAdSoyad")%>
                            </td>
                            <td class="chooes2">
                                <%#DataBinder.Eval(Container.DataItem, "Plaka")%>
                            </td>
                             <td class="chooes2">
                                <%#DataBinder.Eval(Container.DataItem, "YakitTuru")%>
                            </td>
                            <td class="chooes2">
                                <%#DataBinder.Eval(Container.DataItem, "Tutar")%>
                            </td>
                            <td class="chooes1" style="width: 40px;">
                                <%#Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "KartDurumu")) == true ? "<span style=\"color:#F00\">Eski</span>" : "<span style=\"color:#0C0\">Yeni</span>"%>
                            </td>
                            <td class="chooes1" style="width: 40px;">
                                <%#DataBinder.Eval(Container.DataItem, "KartId")%>
                            </td>
                            <td class="chooes4">
                                <asp:TextBox ID="txtKartNo" runat="server" Enabled="false" Text='<%#DataBinder.Eval(Container.DataItem, "KartNo")%>'></asp:TextBox>
                            </td>
                            <td class="chooes2">
                                <%#DataBinder.Eval(Container.DataItem, "KartToplamYuklenen")%>
                            </td>
                            <td class="chooes3">
                                <%#DataBinder.Eval(Container.DataItem, "IrsaliyeNo")%>
                            </td>
                            <td class="chooes3">
                                <%#DataBinder.Eval(Container.DataItem, "KargoDurumu")%>
                            </td>
                            <td style="float: right;">
                                <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Güncelle" CssClass="testEtBTN" />
                                &nbsp;
                                <asp:Button ID="btnUpdateSave" runat="server" CommandName="UpdateSave" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Id") + ";" +DataBinder.Eval(Container.DataItem, "ErpCariId") + ";" +DataBinder.Eval(Container.DataItem, "SiparisNo") + ";" +DataBinder.Eval(Container.DataItem, "KartId")%>'
                                    Text="Kaydet" CssClass="testEtBTN" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
    <div id="divExcelBilgileri" style="display: none;">
        <table class="accountRoot" style="width: 1190px; margin-left: -40px;">
            <tr class="accordionContainer" style="width: 1190px; margin-left: -40px;">
                <td>
                    <asp:Repeater ID="rptExcelBilgileri" runat="server" OnItemCommand="rptAkaryakitKartBilgileri_ItemCommand">
                        <HeaderTemplate>
                            <table>
                                <tr class="ordertitle">
                                    <th class="title4">
                                        Kart yükleme yapıldı mı?
                                    </th>
                                    <th class="title4">
                                        CariId
                                    </th>
                                    <th class="title4">
                                        Sipariş No
                                    </th>
                                    <th class="title4">
                                        Kullanıcı Ad
                                    </th>
                                    <th class="title4">
                                        Plaka
                                    </th>
                                    <th class="title4">
                                        Yakıt Türü
                                    </th>
                                    <th class="title4">
                                        Tutar
                                    </th>
                                    <th class="title4">
                                        Eski - Yeni
                                    </th>
                                    <th class="title4">
                                        Kart Id
                                    </th>
                                    <th class="title4">
                                        Kart No
                                    </th>
                                    <th class="title2">
                                        Karta Yüklenen Toplam Tutar
                                    </th>
                                    <th class="title4">
                                        İrsaliye No
                                    </th>
                                    <th class="title4">
                                        Gönderim
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <!-- chooes -->
                            <tr id="trOrderChooes" runat="server" class="orderchooes" style="border: 1px solid #ccc;">
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "KartYuklemeDurumBilgisi")%>
                                </td>
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "ErpCariId")%>
                                </td>
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "SiparisNo")%>
                                </td>
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "KullaniciAdSoyad")%>
                                </td>
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "Plaka")%>
                                </td>
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "YakitTuru")%>
                                </td>
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "Tutar")%>
                                </td>
                                <td class="chooes4">
                                    <%#Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "KartDurumu")) == true ? "<span style=\"color:#F00\">Eski</span>" : "<span style=\"color:#0C0\">Yeni</span>"%>
                                </td>
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "KartId")%>
                                </td>
                                <td class="chooes4">
                                    <%#"["+DataBinder.Eval(Container.DataItem, "KartNo")+"]"%>
                                </td>
                                <td class="chooes2">
                                    <%#DataBinder.Eval(Container.DataItem, "KartToplamYuklenen")%>
                                </td>
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "IrsaliyeNo")%>
                                </td>
                                <td class="chooes4">
                                    <%#DataBinder.Eval(Container.DataItem, "KargoDurumu")%>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </div>
    <div id="divLoad" class="loading" style="display: none">
        <img src="/images/ajax-loader.gif" style="position: fixed; left: 50%; top: 50%;" />
    </div>
</asp:Content>
