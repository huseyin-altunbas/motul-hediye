﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using Goldb2cBLL;
using Goldb2cInfrastructure;
using Goldb2cEntity;
using System.IO;


public partial class Admin_UyeGirisi : System.Web.UI.Page
{
    MarketingScriptHelper marketingScriptHelper = new MarketingScriptHelper();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    protected void Page_Load(object sender, EventArgs e)
    {

            //if (Request.IsSecureConnection)
            //{
            //    string path = string.Format("http{0}", Request.Url.AbsoluteUri.Substring(4));
            //    Response.Redirect(path);
            //}

        //
        //OCI Entegrasyonu
        if (!string.IsNullOrEmpty(Request.QueryString["hook_url"]) && !string.IsNullOrEmpty(Request.QueryString["view_id"]) && !string.IsNullOrEmpty(Request.QueryString["view_passwd"]) && !string.IsNullOrEmpty(Request.QueryString["email"]) && !string.IsNullOrEmpty(Request.QueryString["password"]) && !string.IsNullOrEmpty(Request.QueryString["oci_version"]))
        {
            OciParams ociParams = new OciParams();
            ociParams.HookUrl = HttpUtility.UrlDecode(Request.QueryString["hook_url"]);
            ociParams.ViewId = HttpUtility.UrlDecode(Request.QueryString["view_id"]);
            ociParams.ViewPasswd = HttpUtility.UrlDecode(Request.QueryString["view_passwd"]);
            ociParams.Email = HttpUtility.UrlDecode(Request.QueryString["email"]);
            ociParams.Password = HttpUtility.UrlDecode(Request.QueryString["password"]);
            ociParams.OciVersion = HttpUtility.UrlDecode(Request.QueryString["oci_version"]);
            ociParams.Vendor = HttpUtility.UrlDecode(Request.QueryString["vendor"]);
            Session["OciParams"] = ociParams;

            KullaniciGiris(ociParams.Email, ociParams.Password, false);
        }
        else if (!string.IsNullOrEmpty(HttpContext.Current.Request.Form.Get("hook_url")) && !string.IsNullOrEmpty(HttpContext.Current.Request.Form.Get("view_id")) && !string.IsNullOrEmpty(HttpContext.Current.Request.Form.Get("view_passwd")) && !string.IsNullOrEmpty(HttpContext.Current.Request.Form.Get("email")) && !string.IsNullOrEmpty(HttpContext.Current.Request.Form.Get("password")) && !string.IsNullOrEmpty(HttpContext.Current.Request.Form.Get("oci_version")))
        {
            try
            {
                string ociFormData = "";
                for (int i = 0; i < HttpContext.Current.Request.Form.Keys.Count; i++)
                {
                    string donenKey = HttpContext.Current.Request.Form.Keys[i].ToString();
                    ociFormData = ociFormData + donenKey + " = " + HttpContext.Current.Request.Form[donenKey].ToString() + " | ";
                }

                Stream datastream = this.Request.InputStream;
                StreamReader sr = new StreamReader(datastream);
                string postLogString = sr.ReadToEnd();
                StreamWriter sw2 = File.AppendText(@"C:\ERP\IISCode\Postlog.txt");
                sw2.WriteLine(DateTime.Now.ToString() + " - " + postLogString + " - " + Request.Browser.Browser.ToString() + " - ");
                sw2.WriteLine(Request.Url.ToString());
                sw2.WriteLine("----------------------------------------------");
                sw2.WriteLine(ociFormData);
                sw2.WriteLine("=================================================================");

                sw2.Flush();
                sw2.Close();
            }
            catch (Exception err)
            {

                throw;
            }

            OciParams ociParams = new OciParams();
            ociParams.HookUrl = HttpUtility.UrlDecode(HttpContext.Current.Request.Form.Get("hook_url"));
            ociParams.ViewId = HttpUtility.UrlDecode(HttpContext.Current.Request.Form.Get("view_id"));
            ociParams.ViewPasswd = HttpUtility.UrlDecode(HttpContext.Current.Request.Form.Get("view_passwd"));
            ociParams.Email = HttpUtility.UrlDecode(HttpContext.Current.Request.Form.Get("email"));
            ociParams.Password = HttpUtility.UrlDecode(HttpContext.Current.Request.Form.Get("password"));
            ociParams.OciVersion = HttpUtility.UrlDecode(HttpContext.Current.Request.Form.Get("oci_version"));
            ociParams.Vendor = HttpUtility.UrlDecode(HttpContext.Current.Request.Form.Get("vendor"));
            Session["OciParams"] = ociParams;

            KullaniciGiris(ociParams.Email, ociParams.Password, false);

        }
        else
        {

            txtSifre.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + lnbGiris.ClientID + "').click();return false;}} else {return true;} ");

            if (Session["Kullanici"] != null)
                Response.Redirect("~/Default.aspx", false);
        }
    }

    protected void lnbGiris_Click(object sender, EventArgs e)
    {
        if (this.IsValid)
        {
            KullaniciGiris(txtEmail.Text.Trim(), txtSifre.Text.Trim(), chkBeniHatirla.Checked);
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen tüm alanları kontrol edip yeniden deneyiniz."));
        }

    }

    private void KullaniciGiris(string Email, string Sifre, bool BeniHatirla)
    {
        string YonlenecekUrl = "";

        try
        {
            // email
            string email = Email;

            // şifre
            string sifre = Sifre;

            int kullaniciID = bllUyelikIslemleri.MusteriKullaniciGiris(email, sifre);

            if (kullaniciID > 0)
            {

                //Create a new cookie, passing the name into the constructor
                HttpCookie hcookie = new HttpCookie("HNetLogin");
                //Set the cookies value
                hcookie.Value = "true";
                //Set the cookie to expire in 1 hour
                DateTime dtNow = DateTime.Now;
                TimeSpan tsHour = new TimeSpan(0, 1, 0, 0);
                hcookie.Expires = dtNow + tsHour;
                //Add the cookie
                Response.Cookies.Add(hcookie);


                // kullanıcı sisteme giriş yapabilir
                // kullanıcıyı sisteme kayıt et
                bllUyelikIslemleri.KullaniciSistemKayit(kullaniciID);

                if (BeniHatirla == true)
                {
                    Crypt crypt = new Crypt();
                    string encryptKey = CustomConfiguration.getEncryptKey;

                    Goldb2cBLL.Cookie cookie = new Goldb2cBLL.Cookie();
                    cookie.SetCookie("Email", crypt.EncryptData(encryptKey, email));
                    cookie.SetCookie("Sifre", crypt.EncryptData(encryptKey, sifre));
                    HttpContext.Current.Response.Cookies.Add(cookie.GetCookie("GoldComTrUser"));
                }

                // sonuç sayfasına yönlendir
                if (Session["SonUrl"] != null)
                    YonlenecekUrl = Session["SonUrl"].ToString();
                else
                    YonlenecekUrl = "/Admin/Default.aspx";

                Session["SonUrl"] = null;
                Response.Redirect(YonlenecekUrl, false);
            }
            else if (kullaniciID == -1)
                // email doğru, şifre hatalı
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Şifreniz hatalıdır. Lütfen kontrol edip yeniden deneyiniz."));
            else if (kullaniciID == -2)
                // email ve şifre hatalı
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Lütfen e-mail adresi ve şifrenizi kontrol edip yeniden deneyiniz."));
            else
                ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sistemde bir hata oluştu. Lütfen daha sonra yeniden deneyiniz."));
        }
        catch
        { ClientScript.RegisterStartupScript(typeof(string), "Mesaj", MesajScriptOlustur("Sistemde bir hata oluştu. Lütfen daha sonra yeniden deneyiniz.")); }


    }


    private string MesajScriptOlustur(string Mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<table width=\"200\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td><table width=\"200\" align=\"center\"><tr><td align=\"center\">" + Mesaj + "</td></tr></table></td></tr><tr><td></br> <a onclick=\"unblockDivKapat()\" style=\"cursor:hand\"><b>Kapat</b></a></td></tr></table>');</script>";
    }
    protected void btnFacebookConnect_Click(object sender, EventArgs e)
    {
        string FacebookAppID = CustomConfiguration.Facebook_client_id;
        string FacebookCallbackURL = CustomConfiguration.Facebook_redirect_url;
        string serviceURL = string.Format("https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}&scope=email,user_birthday",
        FacebookAppID, FacebookCallbackURL);
        if (HttpContext.Current.Session["SonUrl"] != null)
            Session["FbConnectSonUrl"] = HttpContext.Current.Session["SonUrl"].ToString();
        else
            Session["FbConnectSonUrl"] = Request.UrlReferrer.OriginalString;
        Response.Redirect(serviceURL);
    }
}