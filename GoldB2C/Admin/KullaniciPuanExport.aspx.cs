﻿using Goldb2cBLL.Custom;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_KullaniciPuanExport : System.Web.UI.Page
{
    TransactionBLL transactionBLL = new TransactionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {

        DateTime date = DateTime.Now;
        string path = @"C:\inetpub\wwwroot\anadoludanaspara_ftp\Export\AS_{0}{1}{2}.csv";
        path = string.Format(path, date.ToString("dd"), date.ToString("MM"), date.ToString("yyyy"));

        if (!File.Exists(path))
        {
            DataTable dt = transactionBLL.KulanciPuanDurumlari();
            //StringBuilder str = Helper.ConvertDataTableToCsvFile(dt);
            //Helper.SaveData(str, path);
            string rows = "";
            foreach (DataRow row in dt.Rows)
            {
                string columns = "{0};{1};{2};{3} {4}";
                columns = string.Format(columns, row[0], row[1], row[2], row[3], Environment.NewLine);
                rows += columns;
            }

            File.WriteAllText(path, rows);
        }


    }
}