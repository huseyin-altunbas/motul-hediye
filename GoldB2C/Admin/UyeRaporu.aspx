﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="UyeRaporu.aspx.cs" Inherits="Admin_UyeRaporu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="styles/datepicker.css" type="text/css" />
    <script type="text/javascript" src="js/datePicker/datepicker.js"></script>
    <script type="text/javascript" src="js/datePicker/eye.js"></script>
    <script type="text/javascript" src="js/datePicker/layout.js?ver=1.0.2"></script>
    <div class="calendarDiv">
        <div id="tarihAralik" class="newCalendar">
        </div>
        <div class="calendarBTNdiv">
            <asp:Button ID="btnGetir" runat="server" Text="Ara" OnClientClick="tarihGetir();"
                OnClick="btnGetir_Click" CssClass="aramaBTN fr" />
            <a href="#" id="clearSelection" class="aramaBTN2 fr">Seçimi Temizle</a>
        </div>
    </div>
    <asp:HiddenField ID="hdnTarih" runat="server" />
    <script type="text/javascript">
        function tarihGetir() {
            $("#<%=hdnTarih.ClientID %>").val($('#tarihAralik').DatePickerGetDate(true));
        }
    </script>
</asp:Content>
