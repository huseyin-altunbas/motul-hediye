﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using Goldb2cEntity;
using System.Data;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Net.Mail;


public partial class Admin_LogListele : System.Web.UI.Page
{
    SiparisGoruntulemeBLL bllSiparisGoruntuleme = new SiparisGoruntulemeBLL();
    UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
    DataSet dsSiparis;
    DataView dvSorted;
    DataTable sortedDT;
    DataView dvSortedSiparis;
    DataTable sortedDTSiparis;

    DataTable dtLogBilgileri;
    LogIslemleriBLL bllLogIslemleri = new LogIslemleriBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            drpKullaniciAdDoldur();

        }

    }


    protected void btnGetir_Click(object sender, EventArgs e)
    {

        int kullaniciId, siparisId, kullaniciUyelikTip;
        Int32.TryParse(drpKullaniciAd.SelectedValue.ToString(), out kullaniciId);
        Int32.TryParse(txtSiparisId.Text.ToString(), out siparisId);

        try
        {
            kullaniciUyelikTip = Convert.ToInt32(drpKullaniciUyelikTip.SelectedValue.ToString());
        }
        catch (Exception)
        {
            kullaniciUyelikTip = -1;
        }

        string logTarih1 = txtLogTarih1.Text.ToString().Trim();
        string logTarih2 = txtLogTarih2.Text.ToString().Trim();


        if (logTarih1 == string.Empty && logTarih2 == string.Empty)
        {
            logTarih1 = "01.01.2000";
            logTarih2 = "30.12.9999";
        }
        else if (logTarih1 != string.Empty && logTarih2 == string.Empty)
        {
            logTarih2 = logTarih1;
        }
        else if (logTarih1 == string.Empty && logTarih2 != string.Empty)
        {
            logTarih1 = "01.01.2000";
        }


        dsSiparis = bllSiparisGoruntuleme.GenelSiparisListeleme(0, 0, siparisId, siparisId, Convert.ToDateTime("01.01.2000"), Convert.ToDateTime("31.12.9999"));

        dvSortedSiparis = dsSiparis.Tables["CariSiparisMasterTT"].DefaultView;
        dvSortedSiparis.Sort = " siparis_id desc";
        sortedDTSiparis = dvSortedSiparis.ToTable();

        dtLogBilgileri = bllLogIslemleri.KullaniciSiparisLogBilgileriGetir(kullaniciId, kullaniciUyelikTip, siparisId, logTarih1, logTarih2);

        string sorted = drpSort.SelectedValue.ToString();
        if (sorted == "SiparisId")
            sorted = sorted + " desc";
        else
            sorted = sorted + " desc" + ", SiparisId desc";

        dvSorted = dtLogBilgileri.DefaultView;
        dvSorted.Sort = sorted;
        sortedDT = dvSorted.ToTable();

        rptLogBilgileri.DataSource = sortedDT;
        rptLogBilgileri.DataBind();


    }


    protected void rptLogBilgileri_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = e.Item.DataItem as DataRowView;
            Repeater rptSiparisBilgileri = (Repeater)e.Item.FindControl("rptSiparisBilgileri");

            var vSiparis = sortedDTSiparis.AsEnumerable().Where(s => s.Field<string>("siparis_no") == drv.Row["SiparisId"].ToString());

            rptSiparisBilgileri.DataSource = vSiparis.Count() > 0 ? vSiparis.CopyToDataTable() : sortedDTSiparis.Clone();
            rptSiparisBilgileri.DataBind();

        }
    }

    public string MesajScriptOlustur(string mesaj)
    {
        return "<script type=\"text/javascript\">UyariDivGoster('<div style=\"overflow:auto;height:200px;\"><table width=\"400\" align=\"center\" bgcolor=\"#FFFFFF\"><tr><td></br><a onclick=\"unblockDivKapat();\"><b>Kapat</b></a></td></tr><tr><td>" + mesaj + "</td></tr></table></div>');</script>";
    }

    public void drpKullaniciAdDoldur()
    {
        DataTable dtUyeBilgileri = new DataTable();

        dtUyeBilgileri = bllUyelikIslemleri.GetirKullaniciAll();

        var sonuc = (from k in dtUyeBilgileri.AsEnumerable()
                     orderby k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad")
                     select new
                     {
                         KullaniciId = k.Field<int>("KullaniciId"),
                         KullaniciAdSoyad = string.Format("{0} {1}", k.Field<string>("KullaniciAd"), k.Field<string>("KullaniciSoyad"))
                     }).ToList();


        drpKullaniciAd.DataTextField = "KullaniciAdSoyad";
        drpKullaniciAd.DataValueField = "KullaniciId";
        drpKullaniciAd.DataSource = sonuc;
        drpKullaniciAd.DataBind();
        drpKullaniciAd.Items.Insert(0, (new ListItem(string.Empty, "-1")));

    }


    protected void drpKullaniciAd_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpKullaniciAd.SelectedIndex > 0)
        {
            drpKullaniciUyelikTip.Enabled = false;
        }
        else
        {
            drpKullaniciUyelikTip.Enabled = true;
        }
    }
    protected void drpKullaniciUyelikTip_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpKullaniciUyelikTip.SelectedIndex > 0)
        {
            drpKullaniciAd.Enabled = false;
        }
        else
        {
            drpKullaniciAd.Enabled = true;
        }
    }

    public bool IsValidMail(string emailaddress)
    {
        try
        {
            MailAddress m = new MailAddress(emailaddress);
            return true;
        }
        catch (FormatException)
        {
            return false;
        }
    }

    protected void btnExcelSend_Click(object sender, EventArgs e)
    {

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=SiparisIptalOnayLog.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";

        Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
        Response.Charset = "windows-1254";

        string header = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title></title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1254\" />\n<style>\n</style>\n</head>\n<body>\n";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);


        rptLogBilgileri.RenderControl(htmlWrite);
        Response.Write(header + stringWrite.ToString());
        Response.End();
    }
}