﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="MailBanner.aspx.cs" Inherits="Admin_MailBanner" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="panelBanner">

        <table>                
            <tr>
                <td class="panbelWidth1">
                    <strong>Şifremi Unuttum |</strong>
                </td>
                <td class="panbelWidth2">
                    MarketingResim\banner\bilgimail\
                    <asp:TextBox ID="txtSifremiUnuttum" runat="server" CssClass="panelInput2"></asp:TextBox>
                </td>
                <td class="panbelWidth3">
                    URL :
                    <asp:TextBox ID="txtSifremiUnuttumLink" runat="server" CssClass="panelInput3"></asp:TextBox>
                </td>
                <td class="panbelWidth4">
                    <asp:Button ID="btnSifremiUnuttumTest" runat="server" Text="Test Et" OnClick="btnSifremiUnuttumTest_Click" CssClass="testEtBTN" />
                </td>
            </tr>
            <tr>
                <td class="panbelWidth1">
                    <strong>Hoşgeldiniz |</strong>
                </td>
                <td class="panbelWidth2">
                    MarketingResim\banner\bilgimail\
                    <asp:TextBox ID="txtHosgeldiniz" runat="server" CssClass="panelInput2"></asp:TextBox>
                </td>
                <td class="panbelWidth3">
                    URL :
                    <asp:TextBox ID="txtHosgeldinizLink" runat="server" CssClass="panelInput3"></asp:TextBox>
                </td>
                <td class="panbelWidth4">
                    <asp:Button ID="btnHosgeldinizTest" runat="server" Text="Test Et" OnClick="btnSifremiUnuttumTest_Click" CssClass="testEtBTN" />
                </td>
            </tr>

            <tr>
                <td class="panbelWidth1">
                    <strong>Sipariş Onay |</strong>
                </td>
                <td class="panbelWidth2">
                    MarketingResim\banner\bilgimail\
                    <asp:TextBox ID="txtSiparisOnay" runat="server" CssClass="panelInput2"></asp:TextBox>
                </td>
                <td class="panbelWidth3">
                    URL :
                    <asp:TextBox ID="txtSiparisOnayLink" runat="server" CssClass="panelInput3"></asp:TextBox>
                </td>
                <td class="panbelWidth4">
                    <asp:Button ID="btnGeriAl" runat="server" Text="Test Et" OnClick="btnSifremiUnuttumTest_Click" CssClass="testEtBTN" />
                </td>
            </tr>
            <tr>
                <td colspan="4" class="panbelWidth5">
                    <asp:Button ID="btnKaydet" runat="server" Text="Kaydet" OnClick="btnKaydet_Click"  CssClass="testEtBTN fr"  />
                </td>
            </tr>
        </table>
    
    </div>
</asp:Content>
