﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using System.Text;
using Goldb2cEntity;

public partial class Admin_AnasayfaBannerv2 : System.Web.UI.Page
{
    BannerBLL bllBanner = new BannerBLL();
    DataTable dtBanner;
    protected void Page_Load(object sender, EventArgs e)
    {
        dtBanner = new DataTable();

        if (!IsPostBack)
        {
            BannerHtmlHazirla();
        }

    }

    private void BannerHtmlHazirla()
    {
        string AnaSayfaBanner = "";

        dtBanner = bllBanner.GetirBanner(1);
        if (dtBanner.Rows.Count > 0)
            AnaSayfaBanner = dtBanner.Rows[0]["BannerHtml"].ToString();

        List<entAnaSayfaBanner> lstAnaSayfaBanner = new List<entAnaSayfaBanner>();
        if (!string.IsNullOrEmpty(AnaSayfaBanner))
        {
            entAnaSayfaBanner AnaSayfaBannerEnt;
            string[] Bannerlar = AnaSayfaBanner.Split(new string[] { "||" }, StringSplitOptions.None);
            foreach (string banner in Bannerlar)
            {
                AnaSayfaBannerEnt = new entAnaSayfaBanner();
                string[] _banner = banner.Split(new string[] { "##" }, StringSplitOptions.None);
                AnaSayfaBannerEnt.BannerResim = _banner[0];
                AnaSayfaBannerEnt.BannerLink = _banner[1];
                AnaSayfaBannerEnt.AlternateTextTitle = _banner[2];

                lstAnaSayfaBanner.Add(AnaSayfaBannerEnt);
            }

            StringBuilder sbBanner = new StringBuilder();

            //Bannerlar

            int Say = 1;
            foreach (entAnaSayfaBanner banner in lstAnaSayfaBanner)
            {
                sbBanner.AppendLine("<div class=\"slider4-slide\">");
                if (string.IsNullOrEmpty(banner.BannerLink.Trim()))
                    sbBanner.AppendFormat("<img class=\"bigCampaign\" src=\"/UrunResim/MarketingResim/banner/anasayfa/{0}\" alt=\"{1}\" />", banner.BannerResim.Trim(), banner.AlternateTextTitle.Trim());
                else
                    sbBanner.AppendFormat("<a href=\"{0}\" title=\"{1}\" > <img class=\"bigCampaign\" src=\"/UrunResim/MarketingResim/banner/anasayfa/{2}\" alt=\"{3}\" /></a>", banner.BannerLink.Trim(), banner.AlternateTextTitle.Trim(), banner.BannerResim.Trim(), banner.AlternateTextTitle.Trim());
                sbBanner.AppendLine("</div>");

                //Textboxları doldur
                TextBox txtBnrResim = ((TextBox)((ContentPlaceHolder)this.Master.FindControl("ContentPlaceHolder1")).FindControl("txtBnr" + Say.ToString() + "Resim"));
                TextBox txtBnrLink = ((TextBox)((ContentPlaceHolder)this.Master.FindControl("ContentPlaceHolder1")).FindControl("txtBnr" + Say.ToString() + "Link"));
                TextBox txtBnrAltTitle = ((TextBox)((ContentPlaceHolder)this.Master.FindControl("ContentPlaceHolder1")).FindControl("txtBnr" + Say.ToString() + "AltTitle"));


                txtBnrResim.Text = banner.BannerResim;
                txtBnrLink.Text = banner.BannerLink;
                txtBnrAltTitle.Text = banner.AlternateTextTitle;

                Say++;

            }

            //Banner Thumbs
            sbBanner.AppendLine("<div class=\"slider4-ticker-wrap\">");
            foreach (entAnaSayfaBanner bannerthumb in lstAnaSayfaBanner)
            {
                sbBanner.AppendLine("<div class=\"slider4-ticker\"><img src=\"/UrunResim/MarketingResim/banner/anasayfa/thumbs/" + bannerthumb.BannerResim.Trim() + "\"></div>");
            }
            sbBanner.AppendLine("</div>");

            ltrBanner.Text = sbBanner.ToString();

        }
    }

    protected void btnOnizle_Click(object sender, EventArgs e)
    {
        ContentPlaceHolder MainContent = Page.Master.FindControl("ContentPlaceHolder1") as ContentPlaceHolder;


        StringBuilder sbBanner = new StringBuilder();

        for (int i = 1; i < 11; i++)
        {
            for (int a = 1; a < 11; a++)
            {
                TextBox txtSiraNo = (TextBox)MainContent.FindControl("txtBnr" + a.ToString() + "SiraNo");
                if (txtSiraNo.Text.Trim() == i.ToString())
                {
                    TextBox txtResim = (TextBox)MainContent.FindControl("txtBnr" + a.ToString() + "Resim");
                    TextBox txtLink = (TextBox)MainContent.FindControl("txtBnr" + a.ToString() + "Link");
                    TextBox txtAltTitle = (TextBox)MainContent.FindControl("txtBnr" + a.ToString() + "AltTitle");

                    sbBanner.AppendLine("<div class=\"slider4-slide\">");
                    if (string.IsNullOrEmpty(txtLink.Text.Trim()))
                        sbBanner.AppendFormat("<img class=\"bigCampaign\" src=\"/UrunResim/MarketingResim/banner/anasayfa/{0}\" alt=\"{1}\" />", txtResim.Text.Trim(), txtAltTitle.Text.Trim());
                    else
                        sbBanner.AppendFormat("<a href=\"{0}\" title=\"{1}\" > <img class=\"bigCampaign\" src=\"/UrunResim/MarketingResim/banner/anasayfa/{2}\" alt=\"{3}\" /></a>", txtLink.Text, txtAltTitle.Text.Trim(), txtResim.Text.Trim(), txtAltTitle.Text.Trim());
                    sbBanner.AppendLine("</div>");
                }

            }
        }

        sbBanner.AppendLine("<div class=\"slider4-ticker-wrap\">");

        for (int i = 1; i < 11; i++)
        {
            for (int a = 1; a < 11; a++)
            {
                TextBox txtSiraNo = (TextBox)MainContent.FindControl("txtBnr" + a.ToString() + "SiraNo");
                if (txtSiraNo.Text.Trim() == i.ToString())
                {
                    //Banner Thumbs
                    TextBox txtBnrResim = ((TextBox)((ContentPlaceHolder)this.Master.FindControl("ContentPlaceHolder1")).FindControl("txtBnr" + a.ToString() + "Resim"));
                    sbBanner.AppendLine("<div class=\"slider4-ticker\"><img src=\"/UrunResim/MarketingResim/banner/anasayfa/thumbs/" + txtBnrResim.Text.Trim() + "\"></div>");
                }
            }
        }

        sbBanner.AppendLine("</div>");

        ltrBanner.Text = sbBanner.ToString();






        /*



        StringBuilder sbBanner = new StringBuilder();
        for (int i = 1; i < 11; i++)
        {
            TextBox txtBnrResim = ((TextBox)((ContentPlaceHolder)this.Master.FindControl("ContentPlaceHolder1")).FindControl("txtBnr" + i.ToString() + "Resim"));
            TextBox txtBnrLink = ((TextBox)((ContentPlaceHolder)this.Master.FindControl("ContentPlaceHolder1")).FindControl("txtBnr" + i.ToString() + "Link"));
            TextBox txtBnrAltTitle = ((TextBox)((ContentPlaceHolder)this.Master.FindControl("ContentPlaceHolder1")).FindControl("txtBnr" + i.ToString() + "AltTitle"));

            sbBanner.AppendLine("<div class=\"slider4-slide\">");
            if (string.IsNullOrEmpty(txtBnrLink.Text.Trim()))
                sbBanner.AppendFormat("<img class=\"bigCampaign\" src=\"/UrunResim/MarketingResim/banner/anasayfa/{0}\" alt=\"{1}\" />", txtBnrResim.Text.Trim(), txtBnrAltTitle.Text.Trim());
            else
                sbBanner.AppendFormat("<a href=\"{0}\" title=\"{1}\" > <img class=\"bigCampaign\" src=\"/UrunResim/MarketingResim/banner/anasayfa/{2}\" alt=\"{3}\" /></a>", txtBnrLink.Text, txtBnrAltTitle.Text.Trim(), txtBnrResim.Text.Trim(), txtBnrAltTitle.Text.Trim());
            sbBanner.AppendLine("</div>");
        }

        sbBanner.AppendLine("<div class=\"slider4-ticker-wrap\">");
        for (int i = 1; i < 11; i++)
        {
            //Banner Thumbs
            TextBox txtBnrResim = ((TextBox)((ContentPlaceHolder)this.Master.FindControl("ContentPlaceHolder1")).FindControl("txtBnr" + i.ToString() + "Resim"));
            sbBanner.AppendLine("<div class=\"slider4-ticker\"><img src=\"/UrunResim/MarketingResim/banner/anasayfa/thumbs/" + txtBnrResim.Text.Trim() + "\"></div>");
        }
        sbBanner.AppendLine("</div>");
        */
        ltrBanner.Text = sbBanner.ToString();

    }

    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        ContentPlaceHolder MainContent = Page.Master.FindControl("ContentPlaceHolder1") as ContentPlaceHolder;


        StringBuilder sbBanner = new StringBuilder();

        for (int i = 1; i < 11; i++)
        {
            for (int a = 1; a < 11; a++)
            {
                TextBox txtSiraNo = (TextBox)MainContent.FindControl("txtBnr" + a.ToString() + "SiraNo");
                if (txtSiraNo.Text.Trim() == i.ToString())
                {
                    TextBox txtResim = (TextBox)MainContent.FindControl("txtBnr" + a.ToString() + "Resim");
                    TextBox txtLink = (TextBox)MainContent.FindControl("txtBnr" + a.ToString() + "Link");
                    TextBox txtAltTitle = (TextBox)MainContent.FindControl("txtBnr" + a.ToString() + "AltTitle");

                    if (i != 10)
                        sbBanner.Append(txtResim.Text.Trim() + "##" + txtLink.Text.Trim() + "##" + txtAltTitle.Text.Trim() + "||");
                    else
                        sbBanner.Append(txtResim.Text.Trim() + "##" + txtLink.Text.Trim() + "##" + txtAltTitle.Text.Trim());
                }

            }


        }
        /*
        sbBanner.Append(txtBnr1Resim.Text.Trim() + "##" + txtBnr1Link.Text.Trim() + "##" + txtBnr1AltTitle.Text.Trim() + "||");
        sbBanner.Append(txtBnr2Resim.Text.Trim() + "##" + txtBnr2Link.Text.Trim() + "##" + txtBnr2AltTitle.Text.Trim() + "||");
        sbBanner.Append(txtBnr3Resim.Text.Trim() + "##" + txtBnr3Link.Text.Trim() + "##" + txtBnr3AltTitle.Text.Trim() + "||");
        sbBanner.Append(txtBnr4Resim.Text.Trim() + "##" + txtBnr4Link.Text.Trim() + "##" + txtBnr4AltTitle.Text.Trim() + "||");
        sbBanner.Append(txtBnr5Resim.Text.Trim() + "##" + txtBnr5Link.Text.Trim() + "##" + txtBnr5AltTitle.Text.Trim() + "||");
        sbBanner.Append(txtBnr6Resim.Text.Trim() + "##" + txtBnr6Link.Text.Trim() + "##" + txtBnr6AltTitle.Text.Trim() + "||");
        sbBanner.Append(txtBnr7Resim.Text.Trim() + "##" + txtBnr7Link.Text.Trim() + "##" + txtBnr7AltTitle.Text.Trim() + "||");
        sbBanner.Append(txtBnr8Resim.Text.Trim() + "##" + txtBnr8Link.Text.Trim() + "##" + txtBnr8AltTitle.Text.Trim() + "||");
        sbBanner.Append(txtBnr9Resim.Text.Trim() + "##" + txtBnr9Link.Text.Trim() + "##" + txtBnr9AltTitle.Text.Trim() + "||");
        sbBanner.Append(txtBnr10Resim.Text.Trim() + "##" + txtBnr10Link.Text.Trim() + "##" + txtBnr10AltTitle.Text.Trim());
        */

        if (bllBanner.GuncelleBanner(1, sbBanner.ToString().Trim()) > 0)
        {
            BannerHtmlHazirla();
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Banner güncellendi.');  $(document).ready(function () { window.location.href=window.location.href; });</script>");
        }
        else
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Bir hata oluştu tekrar deneyin.')</script>");
    }

    private string GetirBannerHtml()
    {
        string BannerHtmlText = "";
        dtBanner = bllBanner.GetirBanner(1);
        if (dtBanner.Rows.Count > 0)
            BannerHtmlText = dtBanner.Rows[0]["BannerHtml"].ToString();

        return BannerHtmlText;

    }
    protected void btnGeriAl_Click(object sender, EventArgs e)
    {
        BannerHtmlHazirla();
    }

    protected void btnResimKaydet_Click(object sender, EventArgs e)
    {
        if (fuBuyukResim.HasFile && fuKucukResim.HasFile)
        {
            string pathBuyukResim = HttpContext.Current.ApplicationInstance.Server.MapPath("~/UrunResim/MarketingResim/banner/anasayfa/");
            string pathKucukResim = HttpContext.Current.ApplicationInstance.Server.MapPath("~/UrunResim/MarketingResim/banner/anasayfa/thumbs/");
            string fnBuyukResim = System.IO.Path.GetFileName(fuBuyukResim.PostedFile.FileName);
            string fnKucukResim = System.IO.Path.GetFileName(fuKucukResim.PostedFile.FileName);

            if (fnBuyukResim == fnKucukResim)
            {
                try
                {
                    fuBuyukResim.PostedFile.SaveAs(System.IO.Path.Combine(pathBuyukResim, fnBuyukResim));
                    fuKucukResim.PostedFile.SaveAs(System.IO.Path.Combine(pathKucukResim, fnKucukResim));
                    ClientScript.RegisterStartupScript(typeof(string), "message", "<script type=\"text/javascript\">alert('Resim kaydedildi.');</script>");
                }
                catch (Exception)
                {
                    ClientScript.RegisterStartupScript(typeof(string), "message", "<script type=\"text/javascript\">alert('Hata ile karşılaşıldı tekrar deneyiniz.');</script>");
                }

            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(string), "message", "<script type=\"text/javascript\">alert('Resim isimleri aynı olmalı.');</script>");
            }
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "message", "<script type=\"text/javascript\">alert('İki tane resim seçilmesi gerekiyor.');</script>");
        }
    }
}