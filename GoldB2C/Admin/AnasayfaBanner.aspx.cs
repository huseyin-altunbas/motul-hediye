﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;

public partial class Admin_AnasayfaBanner : System.Web.UI.Page
{
    BannerBLL bllBanner = new BannerBLL();
    DataTable dtBanner;
    protected void Page_Load(object sender, EventArgs e)
    {
        dtBanner = new DataTable();

        if (!IsPostBack)
        {
            txtBanner.Text = GetirBannerHtml();
        }

    }
    protected void btnOnizle_Click(object sender, EventArgs e)
    {
        ltrBanner.Text = txtBanner.Text;
    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        if (bllBanner.GuncelleBanner(1, txtBanner.Text.Trim()) > 0)
        {
            txtBanner.Text = GetirBannerHtml();
            ltrBanner.Text = txtBanner.Text;
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Banner güncellendi.')</script>");

        }
        else
            ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Bir hata oluştu tekrar deneyin.')</script>");
    }

    private string GetirBannerHtml()
    {
        string BannerHtmlText = "";
        dtBanner = bllBanner.GetirBanner(1);
        if (dtBanner.Rows.Count > 0)
            BannerHtmlText = dtBanner.Rows[0]["BannerHtml"].ToString();

        return BannerHtmlText;

    }
    protected void btnGeriAl_Click(object sender, EventArgs e)
    {
        txtBanner.Text = GetirBannerHtml();
    }
}