﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Goldb2cBLL;
using System.Data;
using System.Text;
using Goldb2cEntity;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

public partial class Admin_PopupIslemleri : System.Web.UI.Page
{
    BannerBLL bllBanner = new BannerBLL();
    DataTable dtBanner;
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (int.Parse(Session["YetkiTip"].ToString()) != 0)
        //{
        //    Response.Redirect("Default.aspx");
        //}

        if (!IsPostBack)
        {
            Doldur();
        }

    }
    void Doldur()
    {
        for (int i = 0; i < 30; i++)
        {
            ListItem lst = new ListItem(DateTime.Now.AddDays(i).ToString("dd MMMM yyyy (dddd)"), DateTime.Now.AddDays(i).ToString("dd-MM-yyyy"));
            drpTarih.Items.Add(lst);
        }

        DateTime dtSaat = new DateTime(2010, 12, 12, 0, 0, 0);

        for (int i = 0; i < 48; i++)
        {
            dtSaat = dtSaat.AddMinutes(30);
            ListItem lst = new ListItem(dtSaat.AddDays(i).ToString("HH:mm"), dtSaat.AddDays(i).ToString("HH:mm"));
            drpSaat.Items.Add(lst);
        }

        PopupFactory popupfactory = new PopupFactory();
        popupfactory = popupfactory.TekilGetir(1);
        if (popupfactory != null)
        {
            txtLink.Text = popupfactory.p_Adres;
            drpTarih.SelectedValue = popupfactory.p_SonaErmeTarihi.ToString("dd-MM-yyyy");
            drpSaat.SelectedValue = popupfactory.p_SonaErmeTarihi.ToString("HH:mm");
            Image1.ImageUrl = "../UrunResim/Product/" + popupfactory.p_Resim1;
            //txtGenislik.Text = popupfactory.p_Genislik.ToString();
            //txtYukseklik.Text = popupfactory.p_Yukseklik.ToString();
        }
    }

    protected void btnOnizle_Click(object sender, EventArgs e)
    {
        PopupFactory popupfactory = new PopupFactory();
        popupfactory = popupfactory.TekilGetir(1);

        if (popupfactory == null) popupfactory = new PopupFactory();

        DateTime dtBitTarih = DateTime.Now;
        try
        {
            dtBitTarih = DateTime.Parse(drpTarih.SelectedValue + " " + drpSaat.SelectedValue);
        }
        catch (Exception)
        { }

        string ResimAdi = popupfactory.p_Resim1;
        if (flResim.HasFile)
        {
            string Uzanti = Path.GetExtension(flResim.FileName).ToLower();

            if (flResim.FileName == ResimAdi)
            {
                ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Lütfen resime farklı bir resim adı yazınız ve tekrar resmi seçerek deneyiniz')</script>");
            }
           // return;

            if (Uzanti == ".jpg" || Uzanti == ".png" || Uzanti == ".gif" || Uzanti == ".gİf")
            {
                flResim.SaveAs(Server.MapPath("../UrunResim/Product/" + flResim.FileName));

                ResimAdi = flResim.FileName;
            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('Dosya uzantısı sadece .jpg, .gif veya .png olmalıdır.')</script>");
                return;
            }
        }

        string Adres = txtLink.Text;

        int Yukseklik = 0;
        int Genislik = 0;
        try
        {
            //Yukseklik = int.Parse(txtYukseklik.Text);
        }
        catch (Exception)
        { }

        try
        {
            //Genislik = int.Parse(txtGenislik.Text);
        }
        catch (Exception)
        { }

        popupfactory.Popup_Islemleri(2, 1, 1, 1, "Giriş Popup", ResimAdi, Adres, DateTime.Now, dtBitTarih, 1, Yukseklik, Genislik);

        ClientScript.RegisterStartupScript(typeof(string), "mesaj", "<script>alert('İşlem başarıyla tamamlandı.')</script>");

        Doldur();
    }

    private string GetirBannerHtml()
    {
        string BannerHtmlText = "";
        dtBanner = bllBanner.GetirBanner(1);
        if (dtBanner.Rows.Count > 0)
            BannerHtmlText = dtBanner.Rows[0]["BannerHtml"].ToString();

        return BannerHtmlText;

    }

    protected void btnResimKaydet_Click(object sender, EventArgs e)
    {

    }


}