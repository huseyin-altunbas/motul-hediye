﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="PageNotFound.aspx.cs" Inherits="PageNotFound" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="columns-container">
        <div class="container" id="columns">
            <div class="breadcrumb clearfix">
                <a href="https://motulteam.com/">Anasayfa</a> / Aradığınız sayfa bulunamadı
            </div>
            <div class="row">
                <div class="col-xs-12" style="text-align: center;">
                    <img src="/ImagesGld/404img.gif" alt="Aradığınız sayfa bulunamadı" />
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12" style="font-size: 12px; font-weight: bold;">
                    <p>
                        Sayfa kaldırılmış veya adres eksik girilmiş olabilir</p>
                    <ul>
                        <li>Lütfen adresi doğru yazdığınızdan emin olun.</li>
                        <li>Eğer Sık Kullanılanlar listesinden bu sayfaya yönlendirildiyseniz, motulteam.com
                            <a href="https://motulteam.com/">Anasayfa</a> linkini kullanarak ulaşmaya çalışabilirsiniz.</li>
                        <li style="display:none;">Eğer bunun teknik bir sorun olduğunu düşünüyorsanız, lütfen sayfayı <a href="mailto:online@motulteam.com">
                            online@motulteam.com</a> adresine bildirin.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
