﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MpAna.master" AutoEventWireup="true"
    CodeFile="EditorunSecimi.aspx.cs" Inherits="EditorunSecimi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/GununFirsatlari.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="rootercnav">
        <div class="rcframe">
            <div style="width: 100%;" class="rootername">
                <a href="http://https://motulteam.com//">Anasayfa</a> / Editörün Seçimi
            </div>
        </div>
    </div>
    <asp:Repeater ID="rptEditorunSecimleri" runat="server" OnItemDataBound="rptEditorunSecimleri_ItemDataBound">
        <HeaderTemplate>
            <div class="rootercontainer">
                <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <%# Container.ItemIndex % 4 == 0 ? "<li  class=\"editor no-padding\">" : "<li class=\"editor\">" %>
            <div>
                <a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>" class="thumbPicture"
                    title="<%# DataBinder.Eval(Container.DataItem,"urun_ad") %>">
                    <img src="<%# DataBinder.Eval(Container.DataItem,"resim_ad") %>" alt="<%# DataBinder.Eval(Container.DataItem,"urun_ad") %>" />
                </a><a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>" class="productName"
                    title="<%# DataBinder.Eval(Container.DataItem,"urun_ad") %>">
                    <%# DataBinder.Eval(Container.DataItem,"urun_ad") %>
                </a><a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>" class="motto2">
                    <%# DataBinder.Eval(Container.DataItem,"spot_kelime") %></a>
                <asp:Label ID="lblBrutTutar" runat="server"><a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>" class="burut"
                            title="<%# DataBinder.Eval(Container.DataItem,"urun_ad") %>">
                            <%# Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "brut_tutar")), 2).ToString("###,###,###")%>&nbsp;PUAN</a></asp:Label>
                <asp:Label ID="lblNetTutar" runat="server"><a href="<%# DataBinder.Eval(Container.DataItem,"urun_link") %>"
                    class="net" title="<%# DataBinder.Eval(Container.DataItem,"urun_ad") %>">
                    <%# Math.Round(Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "net_tutar")), 2).ToString("###,###,###")%>&nbsp;PUAN
                </a></asp:Label>
            </div>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul> </div>
        </FooterTemplate>
    </asp:Repeater>
    <%--<li class="no-padding">--%>
</asp:Content>
