﻿function showit(tabBaslik_id, tabIcerik_id) {
    //(el).find('div[id~="tabifty"]').css("display", "none");
    jQuery(".tabcontent ").css("display", "none");
    jQuery("#tabify .active").removeClass("active");
    jQuery(".tabnav #tabify #" + tabBaslik_id).addClass("active");
    jQuery(".tabnav #" + tabIcerik_id).css("display", "block");
}

/** Product Review **/
function showReview(tabBaslik_id, tabIcerik_id) {
    jQuery(".tabreviewcontent").hide();
    jQuery("#tabproduct .active").removeClass("active");
    jQuery(".tabreview #tabproduct #" + tabBaslik_id).addClass("active");
    jQuery(".tabreview #" + tabIcerik_id).show();
    if (tabBaslik_id == 'LiFacebookComment')
        jQuery(".tabreview #tabproduct #" + tabBaslik_id + " a").addClass("facebookActive");
    else
        jQuery(".tabreview #tabproduct #LiFacebookComment a.facebookActive").removeClass("facebookActive");


}
/** Product Review **/


/** Product Photo Scroll **/
$(document).ready(function () {
    $('#bigPic img').click(function () {
        showReview('LiPictures', 'DivPictures')
        var index = $(this).index();
        //        $(document).scrollTop(1000);
        $('html,body').animate({ scrollTop: $("#DivPictures").offset().top - 100 });
    });
});
/** Product Photo Scroll **/


/** Brands Scroll **/
$(document).ready(function () {
    // scroll to top
    $('a.topOfPage').click(function () {
        $(document).scrollTop(940);
        return false;
    });
});

/** Brands Scroll **/

