﻿
$(document).ready(function () {

    /* scroll */
    $(document).scroll(function () {
        var y = $(this).scrollTop();
        if (y > 100) {
            $('.scroll-top').fadeIn();
        } else {
            $('.scroll-top').fadeOut();
        }
    });

    /* animate */
    $('.scroll-top').click(function () {
        $('body,html').animate({ scrollTop: 0 }, 300);
    })

});