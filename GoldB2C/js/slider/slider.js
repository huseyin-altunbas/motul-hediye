var __sliderjs_model, __sliderjs_controller, __sliderjs_view, __sliderjs_sliders = [],
    __sliderjs_slider_id = 0,
    __sliderjs_methods = {
        lim: function (a, b, c) {
            return a < b ? b : a > c ? c : a
        }
    };

function __Spine_Model() {
    this.id = __sliderjs_slider_id;
    this.active_slide = 0;
    this.play = !0;
    this.progress = 0;
    this.stack = [];
    this.breadcrum = this.last_slide = this.first_slide = this.current_slide = this.prev_slide = this.next_slide = 0
}

function __Spine_View() {
    this.id = __sliderjs_slider_id;
    this.grid = !1;
    this.grid_rows = [];
    this.grid_cols = [];
    this.wrap = this.cols = this.rows = 0;
    this.tickers = [];
    this.ticker_wrap = 0;
    this.slides = [];
    this.outer_contents = [];
    this.slides_z = [];
    this.tickers = [];
    this.play_pause = this.arrow_prev = this.arrow_next = 0;
    this.pause_class = "pause";
    this.is_busy = !1;
    this.progress_bar = this.loading_bar_wrap_width = this.loading_bar_wrap = this.transition_length = 0;
    this.inner_content_boxes = [];
    this.outer_content_box = 0;
    this.api = {
        id: 0,
        current_slide: 0,
        next_slide: 0,
        prev_slide: 0,
        breadcrum_slide: 0,
        first_slide: 0,
        last_slide: 0,
        slides: 0,
        current_slide_index: 0,
        next_slide_index: 0,
        prev_slide_index: 0,
        breadcrum_slide_index: 0,
        first_slide_index: 0,
        last_slide_index: 0,
        inner_content_boxes: 0,
        outer_content_box: 0,
        active_ticker: 0,
        prev_ticker: 0,
        next_ticker: 0,
        first_ticker: 0,
        last_ticker: 0,
        loading_bar_wrap: 0,
        loading_bar: 0,
        pagination_current_slide: 0,
        pagination_slides_count: 0,
        play_pause_button: 0,
        arrow_next: 0,
        arrow_prev: 0,
        z_move_to_top: this.z_move_to_top,
        state_play: !0
    }
}

function __Spine_Controller() {
    this.id = __sliderjs_slider_id;
    this.time_interval = this.progress_interval = this.main_interval = 0;
    this.pressed_pause = !1;
    this.api = {
        event: 0,
        state_play: !0
    }
}

function __Spine_Slider() {
    this.id = __sliderjs_slider_id;
    this.model = new __Spine_Model;
    this.view = new __Spine_View;
    this.controller = new __Spine_Controller;
    this.events = {
        view_init: !1,
        show_slide: !1,
        transition_in: !1,
        transition_out: !1,
        transition_complete: !1,
        busy: !1,
        update_tickers: !1,
        update_pagination: !1,
        start_loading: !1,
        loading_complete: !1,
        arrow_next_click: !1,
        arrow_prev_click: !1,
        ticker_click: !1,
        play_pause_click: !1
    };
    this.class_names = {
        slider_wrap: "sliderjs-wrap",
        slide: "sliderjs-slide",
        arrow: "sliderjs-arrow",
        arrow_next: "sliderjs-arrow-next",
        arrow_prev: "sliderjs-arrow-prev",
        pagination: "sliderjs-pagination",
        pagination_current: "sliderjs-current-slide",
        pagination_last: "sliderjs-last-slide",
        play_pause: "sliderjs-play-pause",
        loading_bar: "sliderjs-loading-bar",
        progress_bar: "sliderjs-loading-bar-progress",
        ticker_wrap: "sliderjs-ticker-wrap",
        ticker: "sliderjs-ticker",
        active_ticker: "sliderjs-active-ticker",
        inner_content_box: "sliderjs-inner-content-box",
        outer_content_box: "sliderjs-outer-content-box",
        outer_contents: "sliderjs-outer-contents"
    }
}
__Spine_Model.prototype.init = function () {
    this.next_slide = 1;
    this.prev_slide = this.stack.length - 1;
    this.first_slide = 0;
    this.last_slide = this.stack.length - 1;
    this.current_slide = 0;
    __sliderjs_sliders[this.id].view.prepare()
};
__Spine_Model.prototype.go_forward = function (a) {
    a.no_breadcrum || (this.breadcrum = this.current_slide);
    for (var a = this.stack[0], b = 0; b < this.stack.length - 1; b++) this.stack[b] = this.stack[b + 1];
    this.stack[this.stack.length - 1] = a;
    this.current_slide = this.stack[0];
    this.next_slide = this.current_slide + 1 > this.stack.length - 1 ? 0 : this.current_slide + 1;
    this.prev_slide = 0 > this.current_slide - 1 ? this.stack[this.stack.length - 1] : this.current_slide - 1
};
__Spine_Model.prototype.go_backwards = function (a) {
    a.no_breadcrum || (this.breadcrum = this.current_slide);
    for (var a = this.stack[this.stack.length - 1], b = this.stack.length - 1; 0 < b; b--) this.stack[b] = this.stack[b - 1];
    this.stack[0] = a;
    this.current_slide = this.stack[0];
    this.next_slide = this.current_slide + 1 > this.stack.length - 1 ? 0 : this.current_slide + 1;
    this.prev_slide = 0 > this.current_slide - 1 ? this.stack[this.stack.length - 1] : this.current_slide - 1;
    return "prev"
};
__Spine_Model.prototype.go_at_first = function () {
    this.breadcrum = this.current_slide;
    return 0
};
__Spine_Model.prototype.go_at_last = function () {
    this.breadcrum = this.current_slide;
    return this.stack.length
};
__Spine_Model.prototype.go_at = function (a) {
    this.breadcrum = this.current_slide;
    if (1 < Math.abs(a - this.current_slide)) for (; a != this.current_slide; ) this.go_forward(
    {
        no_breadcrum: !0
    });
    else {
        if (a != this.current_slide)
            0 > a - this.current_slide ? this.go_backwards(
    {
        no_breadcrum: !0
    }) : this.go_forward(
    {
        no_breadcrum: !0
    })
    }
};
__Spine_View.prototype.init = function (a) {
    this.grid = a.grid;
    this.rows = a.rows;
    this.cols = a.cols;
    this.slider_wrap_cn = __sliderjs_sliders[this.id].class_names.slider_wrap;
    this.slide_cn = __sliderjs_sliders[this.id].class_names.slide;
    this.arrow_cn = __sliderjs_sliders[this.id].class_names.arrow;
    this.arrow_next_cn = __sliderjs_sliders[this.id].class_names.arrow_next;
    this.arrow_prev_cn = __sliderjs_sliders[this.id].class_names.arrow_prev;
    this.pagination_cn = __sliderjs_sliders[this.id].class_names.pagination;
    this.pagination_current_cn = __sliderjs_sliders[this.id].class_names.pagination_current;
    this.pagination_last_cn = __sliderjs_sliders[this.id].class_names.pagination_last;
    this.play_pause_cn = __sliderjs_sliders[this.id].class_names.play_pause;
    this.loading_bar_cn = __sliderjs_sliders[this.id].class_names.loading_bar;
    this.progress_bar_cn = __sliderjs_sliders[this.id].class_names.progress_bar;
    this.ticker_wrap_cn = __sliderjs_sliders[this.id].class_names.ticker_wrap;
    this.ticker_cn = __sliderjs_sliders[this.id].class_names.ticker;
    this.active_ticker_cn = __sliderjs_sliders[this.id].class_names.active_ticker;
    this.inner_content_box_cn = __sliderjs_sliders[this.id].class_names.inner_content_box;
    this.outer_content_box_cn = __sliderjs_sliders[this.id].class_names.outer_content_box;
    this.outer_contents_cn = __sliderjs_sliders[this.id].class_names.outer_contents;
    var b = this;
    this.transition_length = a.transition_length;
    this.wrap = $("." + this.slider_wrap_cn);
    this.slides = $("." + this.slide_cn);
    this.arrow_next = $("." + this.arrow_next_cn);
    this.arrow_prev = $("." + this.arrow_prev_cn);
    this.play_pause = $("." + this.play_pause_cn);
    this.loading_bar_wrap = $("." + this.loading_bar_cn);
    this.loading_bar_wrap_width = this.loading_bar_wrap.width();
    this.progress_bar = $("." + this.progress_bar_cn);
    this.ticker_wrap = $("." + this.ticker_wrap_cn);
    this.tickers = $("." + this.ticker_cn);
    this.outer_content_box = $("." + this.outer_content_box_cn);
    this.outer_contents = $("." + this.outer_contents_cn);
    this.update_pagination();
    if (0 == this.ticker_wrap.length) {
        this.wrap.append('<div class="' + this.ticker_wrap_cn + '"></div>');
        this.ticker_wrap = $("." + this.ticker_wrap_cn);
        for (var c = 0; c < this.slides.length; c++) this.ticker_wrap.append('<div class="' + this.ticker_cn + '" id="sliderjs-ticker-' + c + '"></div>');
        this.tickers = $("." + this.ticker_cn)
    }
    else for (c = 0; c < this.tickers.length; c++) $(this.tickers[c]).attr("id", "sliderjs-ticker-" + c);
    this.update_tickers();
    for (c = 0; c < this.slides.length; c++) __sliderjs_sliders[this.id].model.stack[c] = c;
    c = 0;
    this.slides.each(function () {
        $(this).attr("id", "sliderjs-slide-" + c);
        c++
    });
    c = 0;
    this.slides.each(function () {
        b.inner_content_boxes[parseInt($(this).attr("id").replace("sliderjs-slide-", ""))] = $(this).find($("." + b.inner_content_box_cn))
    });
    if (0 != $(this.outer_contents[0]).length) for (c = 0; c < this.slides.length; c++) $(this.outer_contents[c]).attr("id", $(this.slides[c]).attr("id").replace("sliderjs-slide-", "sliderjs-outer-contents-"));
    this.grid && this.split(this.rows, this.cols)
};
__Spine_View.prototype.prepare = function () {
    this.setup_zindex();
    this.update_api();
    __sliderjs_sliders[this.id].events.view_init && __sliderjs_sliders[this.id].view_init.call(this.api)
};
__Spine_View.prototype.show_slide = function () {
    this.transition_in(__sliderjs_sliders[this.id].model.current_slide);
    this.transition_out(__sliderjs_sliders[this.id].model.breadcrum);
    this.update_tickers();
    this.update_pagination();
    this.start_loading();
    this.busy(this.transition_length);
    __sliderjs_sliders[this.id].events.show_slide && __sliderjs_sliders[this.id].show_slide.call(this.api)
};
__Spine_View.prototype.transition_in = function () {
    this.update_api();
    __sliderjs_sliders[this.id].events.transition_in && __sliderjs_sliders[this.id].transition_in.call(this.api)
};
__Spine_View.prototype.transition_out = function () {
    this.update_api();
    __sliderjs_sliders[this.id].events.transition_out && __sliderjs_sliders[this.id].transition_out.call(this.api)
};
__Spine_View.prototype.transition_complete = function () {
    this.update_api();
    __sliderjs_sliders[this.id].events.transition_complete && __sliderjs_sliders[this.id].transition_complete.call(this.api)
};
__Spine_View.prototype.busy = function (a) {
    var b = this;
    b.is_busy = !0;
    setTimeout(function () {
        b.is_busy = !1;
        b.transition_complete()
    }, a);
    this.update_api();
    __sliderjs_sliders[this.id].events.busy && __sliderjs_sliders[this.id].busy.call(this.api)
};
__Spine_View.prototype.update_tickers = function () {
    this.ticker_wrap.find("." + this.active_ticker_cn).removeClass(this.active_ticker_cn);
    this.ticker_wrap.find("#sliderjs-ticker-" + __sliderjs_sliders[this.id].model.current_slide).addClass(this.active_ticker_cn);
    this.update_api();
    __sliderjs_sliders[this.id].events.update_tickers && __sliderjs_sliders[this.id].update_tickers.call(this.api)
};
__Spine_View.prototype.update_pagination = function () {
    $("." + this.pagination_current_cn).html(__sliderjs_sliders[this.id].model.current_slide);
    $("." + this.pagination_last_cn).html(this.slides.length - 1);
    this.update_api();
    __sliderjs_sliders[this.id].events.update_pagination && __sliderjs_sliders[this.id].update_pagination.call(this.api)
};
__Spine_View.prototype.get_index_of = function (a) {
    return parseInt($(a).attr("id").substr($(a).attr("id").length - 1, $(a).attr("id").length))
};
__Spine_View.prototype.start_loading = function () {
    var a = this;
    this.progress_bar.show().css(
    {
        width: 0
    });
    a.progress_bar.stop().animate(
    {
        width: a.loading_bar_wrap_width
    }, a.transition_length, function () {
        a.loading_complete()
    });
    this.update_api();
    __sliderjs_sliders[this.id].events.start_loading && __sliderjs_sliders[this.id].start_loading.call(this.api)
};
__Spine_View.prototype.loading_complete = function () {
    this.progress_bar.hide();
    this.update_api();
    __sliderjs_sliders[this.id].events.loading_complete && __sliderjs_sliders[this.id].loading_complete.call(this.api)
};
__Spine_View.prototype.update_api = function () {
    this.api = {
        id: this.id,
        current_slide: $(this.slides[__sliderjs_sliders[this.id].model.current_slide]),
        next_slide: $(this.slides[__sliderjs_sliders[this.id].model.next_slide]),
        prev_slide: $(this.slides[__sliderjs_sliders[this.id].model.prev_slide]),
        breadcrum_slide: $(this.slides[__sliderjs_sliders[this.id].model.breadcrum]),
        first_slide: $(this.slides[__sliderjs_sliders[this.id].model.first_slide]),
        last_slide: $(this.slides[__sliderjs_sliders[this.id].model.last_slide]),
        slides: $(this.slides),
        current_slide_index: __sliderjs_sliders[this.id].model.current_slide,
        next_slide_index: __sliderjs_sliders[this.id].model.next_slide,
        prev_slide_index: __sliderjs_sliders[this.id].model.prev_slide,
        breadcrum_slide_index: __sliderjs_sliders[this.id].model.breadcrum,
        first_slide_index: __sliderjs_sliders[this.id].model.first_slide,
        last_slide_index: __sliderjs_sliders[this.id].model.last_slide,
        rows: this.grid_rows,
        cols: this.grid_cols,
        num_rows: this.rows,
        num_cols: this.cols,
        inner_content_box: $(this.inner_content_boxes[__sliderjs_sliders[this.id].model.current_slide]),
        inner_content_boxes: $(this.inner_content_boxes),
        outer_content_box: $(this.outer_content_box),
        outer_contents: $(this.outer_contents),
        current_outer_content: $(this.outer_contents[__sliderjs_sliders[this.id].model.current_slide]),
        next_outer_content: $(this.outer_contents[__sliderjs_sliders[this.id].model.next_slide]),
        prev_outer_content: $(this.outer_contents[__sliderjs_sliders[this.id].model.prev_slide]),
        breadcrum_outer_content: $(this.outer_contents[__sliderjs_sliders[this.id].model.breadcrum]),
        first_outer_content: $(this.outer_contents[__sliderjs_sliders[this.id].model.first_slide]),
        last_outer_content: $(this.outer_contents[__sliderjs_sliders[this.id].model.last_slide]),
        active_ticker: $(this.tickers[__sliderjs_sliders[this.id].model.current_slide]),
        prev_ticker: $(this.tickers[__sliderjs_sliders[this.id].model.prev_slide]),
        next_ticker: $(this.tickers[__sliderjs_sliders[this.id].model.next_slide]),
        first_ticker: $(this.tickers[__sliderjs_sliders[this.id].model.first_slide]),
        last_ticker: $(this.tickers[__sliderjs_sliders[this.id].model.last_slide]),
        breadcrum_ticker: $(this.tickers[__sliderjs_sliders[this.id].model.breadcrum]),
        loader_wrap: $(this.loading_bar_wrap),
        pagination_current_slide: __sliderjs_sliders[this.id].model.current_slide,
        pagination_slides_count: this.slides.length - 1,
        play_pause_button: $(this.play_pause),
        arrow_next: $(this.arrow_next),
        arrow_prev: $(this.arrow_prev),
        z_move_to_top: this.z_move_to_top,
        state_play: __sliderjs_sliders[this.id].model.play
    }
};
__Spine_View.prototype.setup_zindex = function () {
    for (var a = 0; a < __sliderjs_sliders[this.id].view.slides.length; a++) __sliderjs_sliders[this.id].view.slides_z[a] = __sliderjs_sliders[this.id].view.slides.length - a;
    __sliderjs_sliders[this.id].view.update_zindex()
};
__Spine_View.prototype.update_zindex = function () {
    for (var a = 0; a < this.slides.length; a++) $(this.slides[a]).css(
    {
        "z-index": this.slides_z[a]
    })
};
__Spine_View.prototype.z_move_to_top = function (a) {
    for (var b = 0; b < __sliderjs_sliders[this.id].view.slides_z.length; b++) __sliderjs_sliders[this.id].view.slides_z[b]--;
    __sliderjs_sliders[this.id].view.slides_z[a] = __sliderjs_sliders[this.id].view.slides.length;
    __sliderjs_sliders[this.id].view.update_zindex()
};
__Spine_View.prototype.split = function (a, b) {
    for (var c = Math.ceil($(this.slides[0]).width() / b), i = Math.ceil($(this.slides[0]).height() / a), f = 0, j, g = f = 0, h = 0; h < this.slides.length; h++) {
        f = $(this.slides[h]);
        j = f.html();
        f.html("").wrapInner('<div class="sliderjs-slide-grid"></div>');
        for (var f = f.find(".sliderjs-slide-grid"), d = 0; d < b; d++) for (var e = 0; e < a; e++) f.append('<div class="sliderjs-slide-piece sliderjs-slide-piece-col-' + d + " sliderjs-slide-piece-row-" + e + '" id="slide-' + h + "-row-" + e + "-col-" + d + '"></div>'), g = $("#slide-" + h + "-row-" + e + "-col-" + d), g.css(
        {
            position: "absolute",
            overflow: "hidden",
            left: d * c,
            top: e * i,
            width: c,
            height: i
        }), g.append('<div class="sliderjs-slide-piece-content"></div>'), g.find(".sliderjs-slide-piece-content").css(
        {
            position: "absolute",
            overflow: "hidden",
            left: -d * c,
            top: -e * i
        }).html(j), e == a && d == b && g.addClass("sliderjs-piece-last")
    }
    for (e = 0; e < a; e++) this.grid_rows[e] = $(".sliderjs-slide-piece-row-" + e);
    for (d = 0; d < b; d++) this.grid_cols[d] = $(".sliderjs-slide-piece-col-" + d)
};
__Spine_Controller.prototype.init = function (a) {
    this.time_interval = a.interval;
    this.play(a.interval);
    this.events()
};
__Spine_Controller.prototype.pause = function () {
    __sliderjs_sliders[this.id].model.play = !1;
    clearInterval(this.main_interval)
};
__Spine_Controller.prototype.play = function () {
    var a = this;
    this.pressed_pause || (this.pause(), this.main_interval = setInterval(function () {
        __sliderjs_sliders[a.id].view.is_busy || (__sliderjs_sliders[a.id].model.go_forward(
        {
            no_breadcrum: !1
        }), __sliderjs_sliders[a.id].view.show_slide(__sliderjs_sliders[a.id].model.current_slide))
    }, this.time_interval), __sliderjs_sliders[this.id].model.play = !0)
};
__Spine_Controller.prototype.next_slide = function () {
    __sliderjs_sliders[this.id].view.is_busy || (__sliderjs_sliders[this.id].model.go_forward(
    {
        no_breadcrum: !1
    }), __sliderjs_sliders[this.id].view.show_slide(__sliderjs_sliders[this.id].model.current_slide))
};
__Spine_Controller.prototype.prev_slide = function () {
    __sliderjs_sliders[this.id].view.is_busy || (__sliderjs_sliders[this.id].model.go_backwards(
    {
        no_breadcrum: !1
    }), __sliderjs_sliders[this.id].view.show_slide(__sliderjs_sliders[this.id].modelcurrent_slide))
};
__Spine_Controller.prototype.go_at_slide = function (a) {
    __sliderjs_sliders[this.id].view.is_busy || (__sliderjs_sliders[this.id].model.go_at(a), __sliderjs_sliders[this.id].view.show_slide(__sliderjs_sliders[this.id].model.current_slide))
};
__Spine_Controller.prototype.events = function () {
    var a = this;
    __sliderjs_sliders[a.id].view.arrow_next.on("click", function (b) {
        a.pause();
        a.next_slide();
        a.play();
        a.update_api(
        {
            e: b
        });
        __sliderjs_sliders[a.id].events.arrow_next_click && __sliderjs_sliders[a.id].arrow_next_click.call(a.api)
    });
    __sliderjs_sliders[a.id].view.arrow_prev.on("click", function (b) {
        a.pause();
        a.prev_slide();
        a.play();
        a.update_api(
        {
            e: b
        });
        __sliderjs_sliders[a.id].events.arrow_prev_click && __sliderjs_sliders[a.id].arrow_prev_click.call(a.api)
    });
    __sliderjs_sliders[a.id].view.tickers.on("click", function (b) {
        a.pause();
        a.go_at_slide(__sliderjs_sliders[a.id].view.get_index_of($(this)));
        a.play();
        a.update_api(
        {
            e: b
        });
        __sliderjs_sliders[a.id].events.ticker_click && __sliderjs_sliders[a.id].ticker_click.call(a.api)
    });
    __sliderjs_sliders[a.id].view.play_pause.on("click", function (b) {
        __sliderjs_sliders[a.id].model.play ? (a.pause(), a.pressed_pause = !0) : (a.pressed_pause = !1, a.play());
        $(this).toggleClass(__sliderjs_sliders[a.id].view.pause_class);
        a.update_api(
        {
            e: b
        });
        __sliderjs_sliders[a.id].events.play_pause_click && __sliderjs_sliders[a.id].play_pause_click.call(a.api)
    })
};
__Spine_Controller.prototype.update_api = function (a) {
    this.api = {
        event: a.e,
        state_play: this.pressed_pause ? !1 : !0
    }
};
__Spine_Slider.prototype.init = function (a) {
    var b = this;
    this.view.init(a);
    this.model.init(function () {
        b.view.prepare()
    });
    this.controller.init(a)
};
__Spine_Slider.prototype.sliderjs_event = function (a, b) {
    __sliderjs_sliders[this.id][a] = b;
    this.events[a] = !0
};
__Spine_Slider.prototype.define_class = function (a, b) {
    this.class_names[a] = b
};
__Spine_Slider.prototype.view_init = function ()
{ };
__Spine_Slider.prototype.show_slide = function ()
{ };
__Spine_Slider.prototype.transition_in = function ()
{ };
__Spine_Slider.prototype.transition_out = function ()
{ };
__Spine_Slider.prototype.transition_complete = function ()
{ };
__Spine_Slider.prototype.busy = function ()
{ };
__Spine_Slider.prototype.update_tickers = function ()
{ };
__Spine_Slider.prototype.update_pagination = function ()
{ };
__Spine_Slider.prototype.start_loading = function ()
{ };
__Spine_Slider.prototype.loading_complete = function ()
{ };
__Spine_Slider.prototype.arrow_next_click = function ()
{ };
__Spine_Slider.prototype.arrow_prev_click = function ()
{ };
__Spine_Slider.prototype.ticker_click = function ()
{ };
__Spine_Slider.prototype.play_pause_click = function ()
{ };

function sliderjs_init_slider() {
    var a = new __Spine_Slider;
    __sliderjs_sliders[__sliderjs_slider_id] = a;
    __sliderjs_slider_id++;
    return __sliderjs_sliders[__sliderjs_slider_id - 1]
};