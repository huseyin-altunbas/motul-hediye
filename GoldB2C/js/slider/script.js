$(document).ready(function () {
    $('.doc-nav li').on('click', function () {
        $('ul.sub').hide();
        $(this).find('ul.sub').toggle();

        $('#content .active').removeClass('active');
        $('#content #' + $(this).attr('class')).addClass('active');
    });

    function slider4_transition_in() {
        this.z_move_to_top(this.current_slide_index);
        this.current_slide.show();
        //        this.z_move_to_top(this.current_slide_index);
        //        this.current_slide.hide().fadeIn();
    }
    function slider4_view_init() {
        this.z_move_to_top(this.current_slide_index);
        this.next_slide.hide();
    }


    var slider4 = sliderjs_init_slider();
    slider4.sliderjs_event('view_init', slider4_view_init);
    slider4.sliderjs_event('transition_in', slider4_transition_in);

    slider4.define_class('slider_wrap', 'slider4-wrap');
    slider4.define_class('slide', 'slider4-slide');
    slider4.define_class('ticker_wrap', 'slider4-ticker-wrap');
    slider4.define_class('ticker', 'slider4-ticker');
    slider4.define_class('active_ticker', 'slider4-active-ticker');
    slider4.define_class('arrow_next', 'slider4-arrow-next');
    slider4.define_class('arrow_prev', 'slider4-arrow-prev');

    slider4.init({ interval: 5000 });


});
