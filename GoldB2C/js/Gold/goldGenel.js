﻿function SayfaScroll(id) {
    $('html,body').animate({ scrollTop: $("#" + id).offset().top - 100 });
}

function RakamKontrol(e) {
    olay = document.all ? window.event : e;
    tus = document.all ? olay.keyCode : olay.which;
    if ((tus < 48 || tus > 57) && tus != 8) {
        if (document.all) { olay.returnValue = false; } else { olay.preventDefault(); alert(olay.keyCode) }
    }
}


function TiklamaKaydet(TiklananLink) {
    $.ajax({
        type: 'POST',
        url: '/AjaxMethods.asmx/TiklamaKaydet',
        data: '{ "TiklananLink" : "' + TiklananLink + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (result) {

        },
        error: function (err) {
        }
    });
}



function PopUpAc(pencere, adres, width, height) {
    window.open(adres, 'yeni', 'width=' + width + ',height=' + height + ',left = 150,top = 150,resizeable=yes,scrollbars=yes');
    pencere.target = 'yeni';
}

function FacebookDivGoster(mesaj) {
    $(document).ready(function () {
        $.blockUI({
            fadeIn: 450,
            fadeOut: 450,
            css: {
                cursor: 'default',
                top: ($(window).height() - 490) / 2 + 'px',
                left: ($(window).width() - 530) / 2 + 'px',
                width: '530px',
                height: '490px'
            },
            message: mesaj + '</br>'
        });
    });
}

function unblockDivKapat() {
    $(document).ready(function () {
        $.unblockUI();
    });
}

function unblockDivKapatYenile(url) {
    $(document).ready(function () {
        window.top.$.unblockUI({
            onUnblock: function (event) {
                event.location = url;
                alert(url);
            }
        });
    });
}
