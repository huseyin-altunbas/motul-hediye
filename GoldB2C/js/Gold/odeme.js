﻿function DivleriTemizle() {
    $('#DivKrediKarti .rightcolumn .creditcardroot .financingoptions').html('');
    $('#DivKrediKarti .rightcolumn .creditcardroot .financingoptions').hide('');
    $('#DivHavale .rightcolumn .transferroot .transferoptions').hide();
    $('#DivHavaleBilgileri').hide();
    $("#txtKartNo1").mask("9999", { completed: function () { $("#txtKartNo2").focus() } });
    $("#txtKartNo2").mask("9999", { completed: function () { $("#txtKartNo3").focus() } });
    $("#txtKartNo3").mask("9999", { completed: function () { $("#txtKartNo4").focus() } });
    $("#txtKartNo4").mask("9999");
    $("#txtKartSkt").mask("99/99");
    $("#txtKartGuvenlikKod").mask("999");
    $('#DivParaKodBilgileri .transferinfo .parokoddescription').hide('');
    $('#ParaKodQR').hide('');
    $('#DivParaKodBilgileri .transferinfo').hide();
    $('#DivParaKodBilgileri .moneytransfer').hide();
    $('#DivParaKodBilgileri').hide();
}

function OdemeDivAc(DivId, hdnDivDurum) {
    var SlideUpTime = 1000;
    var SlideDownTime = 1000;

    if (DivId == 'DivHavale') {
        SlideUpTime = 350;
        SlideDownTime = 200;
    }

    DivleriTemizle();
    $('#form1').validationEngine('hideAll');
    var AcikDivId = $('#' + hdnDivDurum).val();

    if (AcikDivId != '') {
        if (DivId == AcikDivId) {
            $('#' + DivId).slideUp(SlideUpTime);
            $('#' + hdnDivDurum).val('');
            $('html,body').animate({ scrollTop: 0 });
        }
        else {
            $('#' + AcikDivId).slideUp(SlideUpTime);
            $('#' + DivId).slideDown(SlideDownTime);
            $('#' + hdnDivDurum).val(DivId);
        }
        if (AcikDivId == "DivHavale") {
            $('#DivHavaleBilgileri').hide();
            $('#DivHavale .leftcolumn').hide();
        }
    }
    else {
        $('#' + DivId).slideDown(SlideDownTime);
        $('#' + hdnDivDurum).val(DivId);
    }


    if (DivId != AcikDivId) {
        if (DivId == 'DivKrediKarti')
            GetKrediKartiSecenekleri();
        else if (DivId == 'DivHavale')
            GetHavaleSecenekleri();
        else if (DivId == 'DivParaKod')
            GetParaKodSecenekleri();

        else if (DivId == 'DivPostaCeki') {
            var KalanTutar = $('#lblKalanTutar').html().replace(' TL', '').split('.');

            $('#txtPostaCekiOdenecekTL').val(KalanTutar[0]);
            $('#txtPostaCekiOdenecekKurus').val(KalanTutar[1]);

            if ($('#txtPostaCekiOdenecekKurus').val() == '') {
                $('#txtPostaCekiOdenecekKurus').val(0);
            }

        }

        else if (DivId == 'DivPuan')
            ;


    }
}


function DivActive(DivId, isActive) {
    if (isActive) {
        $('.left.active').removeClass('active').addClass('passive');
        $('#' + DivId).removeClass('passive').addClass('active');
    }
    else {
        $('#' + DivId).removeClass('active').addClass('passive');
    }
}

function GetKrediKartiSecenekleri() {
    $('#DivKrediKarti .rightcolumn .creditcardroot .creditcardinfo').hide();
    $('#DivKrediKarti .rightcolumn .creditcardroot .financingoptions').hide();
    var BankaLogoHtml = '';
    $('#DivKrediKarti .leftcolumn').hide();
    $.ajax({
        type: 'POST',
        url: '/Odeme.aspx/GetTahsilatSecenekleri',
        data: '{ "TahsilatTur" : "Kredi Kartı"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (result) {
            $.each(result.d, function (index, value) {
                BankaLogoHtml = BankaLogoHtml + '<div class=\"kredi_Bnk\">' +
	                             '<div class=\"kredi_Bnk_Sol\">' +
	                             '<input name=\"KrediKartiBanka\" type=\"radio\" value=\"' + value.MasterId + '" class="kredi_Bnk_15" onclick="GetKrediKartiDetaylari(this.value);">' +
	                             '</div>' +
	                             '<div class=\"kredi_Bnk_Sag\">' +
	                             '<img src=\"' + value.BankaResimYol + '\" class=\"kredi_Bnk_5\">' +
	                             '<div class=\"kredi_Bnk_Ayrac\"></div>' +
	                             '<span class=\"kredi_Bnk_Text\">' + value.AnlasmaliKurumlar + '</span>' +
	                             '</div>' +
   								 '</div>';

                //                BankaLogoHtml = BankaLogoHtml + '<div class=\"banklogo blPassive\">' +
                //                '<div class=\"blLeft\">' +
                //                '<input name=\"KrediKartiBanka\" type=\"radio\" value=\"' + value.MasterId + '\" onclick=\"GetKrediKartiDetaylari(this.value);\" />' +
                //                '</div>' +
                //                '<div class=\"blRight\"><img src=\"' + value.BankaResimYol + '\"/></div></div>';
            });
            $('#DivKrediKarti .leftcolumn').html(BankaLogoHtml).fadeIn(800);
        },
        error: function () {
            //alert('Hata No : 1 - Talep esnasında sorun oluştu. Yeniden deneyin');
        }
    });
}

function GetKrediKartiDetaylari(BankaMasterId) {
    $('#form1').validationEngine('hideAll');
    $('#DivKrediKarti .rightcolumn .creditcardroot .creditcardinfo').fadeOut(1500);

    var KrediKartiDetayHtml = '<div class=\"title\">' +
                                                '<div class=\"title1 fontSz14\">' +
                                                    'T.Seçeneği</div>' +
                                                '<div class=\"title2 fontSz14\">' +
                                                    'Taksit Sayısı</div>' +
                                                '<div class=\"title3 fontSz14\">' +
                                                    'Taksit Tutarı(TL)</div>' +
                                                '<div class=\"title4 fontSz14\">' +
                                                    'Toplam Tutar(TL)</div>' +
                                                '<div class=\"title5 fontSz14\">' +
                                                    'Ödeme Avantajınız</div>' +
                                            '</div>';


    $.ajax({
        type: 'POST',
        url: '/Odeme.aspx/GetTahsilatSecenekDetaylari',
        data: '{ "TahsilatSecenekId" : "' + BankaMasterId + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $('#DivKrediKarti .rightcolumn .creditcardroot .financingoptions').fadeOut(100);
        },
        success: function (result) {
            $.each(result.d, function (index, value) {
                if (index % 2 == 0) {
                    KrediKartiDetayHtml = KrediKartiDetayHtml + '<div class=\"chooes\">' +
                                                '<div class=\"chooes1 bgcolore8\">' +
                                                    '<input name=\"KrediKartiDetay\" type=\"radio\" value=\"' + value.DetayId + '\" onclick=\"KartOdemeBilgileriAc(' + value.ToplamTutar + ');\" /></Div>' +
                                                '<div class=\"chooes2 bgcolore8\">' +
                                                     value.gTaksitAdet + '</div>' +
                                                '<div class=\"chooes3 bgcolore8\">' +
                                                    value.TaksitTutar + '</div>' +
                                                '<div id=\"OdenecekKrediKarti' + value.DetayId + '\"  class=\"chooes4 bgcolore8\">' +
                                                    value.ToplamTutar + '</div>' +
                                                '<div class=\"chooes5 bgcolore8\">' +
                                                     value.TahsilatAd + '</div>' +
                                                     '</div>';
                }
                else {
                    KrediKartiDetayHtml = KrediKartiDetayHtml + '<div class=\"chooes\">' +
                                                '<div class=\"chooes1 bgcolorf8\">' +
                                                     '<input name=\"KrediKartiDetay\" type=\"radio\" value=\"' + value.DetayId + '\" onclick=\"KartOdemeBilgileriAc(' + value.ToplamTutar + ');\" /></Div>' +
                                                '<div class=\"chooes2 bgcolorf8\">' +
                                                   value.gTaksitAdet + '</div>' +
                                                '<div class=\"chooes3 bgcolorf8\">' +
                                                      value.TaksitTutar + '</div>' +
                                              '<div id=\"OdenecekKrediKarti' + value.DetayId + '\" class=\"chooes4 bgcolorf8\">' +
                                                   value.ToplamTutar + '</div>' +
                                                '<div class=\"chooes5 bgcolorf8\">' +
                                                      value.TahsilatAd + '</div>' +
                                                       '</div>';

                }
            });


        },

        complete: function () {
            $('#DivKrediKarti .rightcolumn .creditcardroot .financingoptions').fadeIn(500).html(KrediKartiDetayHtml);
        },
        error: function () {
            //alert('Hata No : 2 - Talep esnasında sorun oluştu. Yeniden deneyin');
        }
    });


}


function KartOdemeBilgileriAc(ToplamTutar) {
    var TahsilatSecenekId = $("input:radio[name=KrediKartiBanka]:checked").val();
    var TahsilatSecenekDetayId = $("input:radio[name=KrediKartiDetay]:checked").val();
    var _toplamTutar = ToplamTutar.toString().split('.');
    OdenenTL = _toplamTutar[0];
    OdenenKurus = _toplamTutar[1];
    $('#txtKartOdenecekTL').val(OdenenTL);
    $('#txtKartOdenecekKurus').val(OdenenKurus);

    if ($('#txtKartOdenecekKurus').val() == '')
        $('#txtKartOdenecekKurus').val(0);
    $('#DivKrediKarti .rightcolumn .creditcardroot .creditcardinfo').slideDown(500).fadeIn(500);

    if (TahsilatSecenekId == '2521589')
        $('#btnKrediKarti3DCekim').hide();
    else
        $('#btnKrediKarti3DCekim').show();

    if (TahsilatSecenekId == '3384527')
        $('#btnKrediKartiNormalCekim').hide();
    else {
        $('#btnKrediKartiNormalCekim').show();
        if ($('#btnKrediKartiNormalCekim').attr("onclick") == null || $('#btnKrediKartiNormalCekim').attr("onclick") == "")
            $('#btnKrediKartiNormalCekim').attr("onclick", 'KrediKartiOdemeYap(\'Normal\');');
    }

    /* if (TahsilatSecenekDetayId == '3538379') {
    $('.paymentdescription').html('+8 Taksit kampanyası 100TL üzeri siparişlerinizde geçerlidir.');
    $('.paymentdescription').show();
    }
    else */

    if (TahsilatSecenekId == '2520207') {
        $('.paymentdescription').html('+3 Taksit kampanyası 100TL üzeri siparişlerinizde geçerlidir.');
        $('.paymentdescription').show();
    }
    else {
        $('.paymentdescription').html('');
        $('.paymentdescription').hide();
    }
}

function HavaleOdemeBilgileriAc(ToplamTutar) {
    var _toplamTutar = ToplamTutar.toString().split('.');
    OdenenTL = _toplamTutar[0];
    OdenenKurus = _toplamTutar[1];

    $('#DivHavaleBilgileri .transferinfo').show();
    $('#DivHavaleBilgileri .moneytransfer').show();
    $('#DivHavaleBilgileri').slideDown(500).fadeIn(500);

    $('#txtHavaleOdenenTL').val(OdenenTL);
    $('#txtHavaleOdenenKurus').val(OdenenKurus);

    if ($('#txtHavaleOdenenKurus').val() == '')
        $('#txtHavaleOdenenKurus').val(0);
}

function GetHavaleSecenekleri() {
    $('#form1').validationEngine('hideAll');
    var BankaLogoHtml = '';
    $.ajax({
        type: 'POST',
        url: '/Odeme.aspx/GetTahsilatSecenekleri',
        data: '{ "TahsilatTur" : "Havale"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (result) {
            $.each(result.d, function (index, value) {
                BankaLogoHtml = BankaLogoHtml + '<div class=\"banklogo blPassive\">' +
                '<div class=\"blLeft\">' +
                '<input name=\"HavaleBanka\" type=\"radio\" value=\"' + value.MasterId + '\" onclick=\"GetHavaleDetaylari(this.value);\" />' +
                '</div>' +
                '<div class=\"blRight\">' + value.BankaAd + '</div></div>';
            });
            $('#DivHavale .leftcolumn').html(BankaLogoHtml).fadeIn(300);
        },
        complete: function () {
            $('input[name="HavaleBanka"]').click();
        },
        error: function () {
            //            alert('Hata No : 3 - Talep esnasında sorun oluştu. Yeniden deneyin');
        }
    });
}

function GetHavaleDetaylari(BankaMasterId) {
    $('#DivHavaleBilgileri').fadeOut(1500);
    var HavaleDetayHtml = '<div class=\"title\">' +
                                                '<div class=\"title1 fontSz14\">' +
                                                    'Seçenek</div>' +
                                                '<div class=\"title2 fontSz14\">' +
                                                    'Banka/Şube Adı</div>' +
                                                '<div class=\"title3 fontSz14\">' +
                                                    'IBAN</div>' +
                                                '<div class=\"title4 fontSz14\">' +
                                                    'Tutar (TL)</div>' +
                                            '</div>';


    $.ajax({
        type: 'POST',
        url: '/Odeme.aspx/GetTahsilatSecenekDetaylari',
        data: '{ "TahsilatSecenekId" : "' + BankaMasterId + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $('#DivHavale .rightcolumn .transferroot .transferoptions').fadeOut(100);
        },
        success: function (result) {
            $.each(result.d, function (index, value) {
                if (index % 2 == 0) {
                    HavaleDetayHtml = HavaleDetayHtml + '<div class=\"chooes\">' +
                                                '<div class=\"chooes1 bgcolore8\">' +
                                                    '<input name=\"HavaleDetay\" type=\"radio\" value=\"' + value.DetayId + '\" onclick=\"HavaleOdemeBilgileriAc(' + value.ToplamTutar + ');\" /></Div>' +
                                                '<div class=\"chooes2 bgcolore8\">' +
                                                     value.TahsilatAd + '</div>' +
                                                '<div class=\"chooes3 bgcolore8\">' +
                                                   value.Aciklama + '</div>' +
                                                '<div id=\"OdenecekHavale' + value.DetayId + '\" class=\"chooes4 bgcolore8\">' +
                                                    value.ToplamTutar + '</div>' +
                                                '</div>';
                }
                else {
                    HavaleDetayHtml = HavaleDetayHtml + '<div class=\"chooes\">' +
                                                '<div class=\"chooes1 bgcolorf8\">' +
                                                     '<input name=\"HavaleDetay\" type=\"radio\" value=\"' + value.DetayId + '\" onclick=\"HavaleOdemeBilgileriAc(' + value.ToplamTutar + ');\" /></Div>' +
                                                '<div class=\"chooes2 bgcolorf8\">' +
                                                   value.TahsilatAd + '</div>' +
                                                '<div class=\"chooes3 bgcolorf8\">' +
                                                       value.Aciklama + '</div>' +
                                                '<div id=\"OdenecekHavale' + value.DetayId + '\" class=\"chooes4 bgcolorf8\">' +
                                                   value.ToplamTutar + '</div>' +
                                                       '</div>';

                }
            });


        },

        complete: function () {
            $('#DivHavale .rightcolumn .transferroot .transferoptions').html(HavaleDetayHtml).slideDown(500);
        },
        error: function () {
            //            alert('Hata No : 4 - Talep esnasında sorun oluştu. Yeniden deneyin');
        }
    });
}

function HavaleOdemeYap() {
    var TahsilatSecenekDetayId = '';
    var OdeyenAdSoyad = '';
    var KartNo = '';
    var OdenenTutar = '';
    var OdenecekToplam = '';
    var ParcaliOdendi;

    if ($('#form1').validationEngine('validate') == true) {
        TahsilatSecenekDetayId = $("input:radio[name=HavaleDetay]:checked").val();
        OdenecekToplam = $('#OdenecekHavale' + TahsilatSecenekDetayId).html().replace(' TL', '');
        OdeyenAdSoyad = $('#txtHavaleOdeyenAdSoyad').val();

        if (OdenecekToplam.indexOf('.') < 0)
            OdenecekToplam = OdenecekToplam + '.0';

        if ($('#txtHavaleOdenenKurus').val() == '')
            OdenenTutar = $('#txtHavaleOdenenTL').val() + '.0';
        else
            OdenenTutar = $('#txtHavaleOdenenTL').val() + '.' + $('#txtHavaleOdenenKurus').val();

        if (OdenenTutar == OdenecekToplam)
            ParcaliOdendi = false;
        else
            ParcaliOdendi = true;

        HavaleOdemeKaydet(TahsilatSecenekDetayId, OdeyenAdSoyad, KartNo, OdenenTutar, 'Normal', ParcaliOdendi);

    }
}

function KrediKartiOdemeYap(GuvenlikSeviye) {
    var TahsilatSecenekId = '';
    var TahsilatSecenekDetayId = '';
    var OdeyenAdSoyad = '';
    var KartNo = '';
    var Cvc = '';
    var KartTip = '';
    var SonKulAy = '';
    var SonKulYil = '';
    var CekilecekTutar = '';
    var OdenecekToplam = '';
    var ParcaliOdendi;

    if ($('#form1').validationEngine('validate') == true) {

        TahsilatSecenekDetayId = $("input:radio[name=KrediKartiDetay]:checked").val();
        TahsilatSecenekId = $("input:radio[name=KrediKartiBanka]:checked").val();
        OdenecekToplam = $('#OdenecekKrediKarti' + TahsilatSecenekDetayId).html().replace(' TL', '');
        OdeyenAdSoyad = $('#txtKartAdSoyad').val();
        KartNo = $('#txtKartNo1').val() + $('#txtKartNo2').val() + $('#txtKartNo3').val() + $('#txtKartNo4').val();
        Cvc = $('#txtKartGuvenlikKod').val();
        KartTip = $('#dpKartTip').val();
        SonKulAy = $('#txtKartSkt').val().split('/')[0];
        SonKulYil = $('#txtKartSkt').val().split('/')[1];

        $('#btnKrediKartiNormalCekim').removeAttr("onclick")

        $('#txtKartAdSoyad').val('');
        $('#txtKartNo1').val('');
        $('#txtKartNo2').val('');
        $('#txtKartNo3').val('');
        $('#txtKartNo4').val('');
        $('#txtKartGuvenlikKod').val('');
        $('#dpKartTip').val('');
        $('#txtKartSkt').val('')
        $('#chkKrediKartiSozlesmeOnay').removeAttr("checked");



        if (OdenecekToplam.indexOf('.') < 0)
            OdenecekToplam = OdenecekToplam + '.0';

        if ($('#txtKartOdenecekKurus').val() == '')
            CekilecekTutar = $('#txtKartOdenecekTL').val() + '.0';
        else
            CekilecekTutar = $('#txtKartOdenecekTL').val() + '.' + $('#txtKartOdenecekKurus').val();

        if (CekilecekTutar == OdenecekToplam)
            ParcaliOdendi = false;
        else
            ParcaliOdendi = true;


        $.ajax({
            type: 'POST',
            url: '/Odeme.aspx/KrediKartiOdemeGerceklestir',
            data: '{ "TahsilatSecenekId" : "' + TahsilatSecenekId + '", "TahsilatSecenekDetayId" : "' + TahsilatSecenekDetayId + '", "GuvenlikSeviye" : "' + GuvenlikSeviye + '", "PuanSorgula" : "false", "CekilecekTutar" : "' + CekilecekTutar + '", "OdeyenAdSoyad" : "' + OdeyenAdSoyad + '", "KartNo" : "' + KartNo + '", "CVC" : "' + Cvc + '", "SonKulAy" : "' + SonKulAy + '", "SonKulYil" : "' + SonKulYil + '", "KartTipi" : "' + KartTip + '", "ParcaliOdendi" : "' + ParcaliOdendi + '" }',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                LutfenBekleyinDiv();
            },
            success: function (result) {
                UnblockDivKapat();
                if (result.d[0].toString() != "-500") {
                    if (result.d[0].toString() == "3")
                        location.href = "/3dyonlendir.ashx";
                    else {
                        if (result.d[0] != "Onay" && result.d[1] != null && result.d[1] != '')
                            UnblockDivGoster(result.d[1].toString());

                        OdemeDivAc('DivKrediKarti', 'hdnAcikDiv');
                        SayfaScrollTop();
                        GetTahsilatVeriOzeti();
                        GetTahsilatVerileri();
                    }

                }
                else {
                    alert('Uzun süredir işlem yapmadınız. Giriş sayfasına yönlendiriliyorsunuz. Lütfen yeniden giriş yapınız.');
                    location.href = "/MusteriHizmetleri/UyeGirisi.aspx";
                }
            }
        });

    }
}


function HavaleOdemeKaydet(TahsilatSecenekDetayId, OdeyenAdSoyad, KartNo, OdenenTutar, GuvenlikSeviye, ParcaliOdendi) {
    $.ajax({
        type: 'POST',
        url: '/Odeme.aspx/HavaleOdemeKaydet',
        data: '{ "TahsilatSecenekDetayId" : "' + TahsilatSecenekDetayId + '", "OdeyenAdSoyad" : "' + OdeyenAdSoyad + '","KartNo" : "' + KartNo + '","OdenenTutar" : "' + OdenenTutar + '", "GuvenlikSeviye":"' + GuvenlikSeviye + '", "ParcaliOdendi":"' + ParcaliOdendi + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            LutfenBekleyinDiv();
        },
        success: function (result) {
            UnblockDivKapat();
            if (result.d != -500) {
                if (result.d < 1)
                    alert('Bir hata oluştu lütfen tekrar deneyin.');

                OdemeDivAc('DivHavale', 'hdnAcikDiv');
                SayfaScrollTop();
                GetTahsilatVeriOzeti();
            }
            else {
                alert('Uzun süredir işlem yapmadınız. Giriş sayfasına yönlendiriliyorsunuz. Lütfen yeniden giriş yapınız.');
                location.href = "/MusteriHizmetleri/UyeGirisi.aspx";
            }



        },
        error: function () {
            //           alert('Hata No : 5 - Talep esnasında sorun oluştu. Yeniden deneyin');
        }
    });
}

function BkmExpressOdemeYap() {
    var CekilecekTutar = '';
    var OdenecekToplam = '';
    var ParcaliOdendi = false;

    if ($('#form1').validationEngine('validate') == true) {
        CekilecekTutar = $('#lblKalanTutar').text().replace(' TL', '');
        $.ajax({
            type: 'POST',
            url: '/Odeme.aspx/BkmExpressOdemeGerceklestir',
            data: '{ "CekilecekTutar" : "' + CekilecekTutar + '" }',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                LutfenBekleyinDiv();
            },
            success: function (result) {
                UnblockDivKapat();
                if (result.d[0].toString() != "-500") {
                    if (result.d[0].toString() == "4")
                        location.href = "/BkmExpressYonlendir.ashx";
                    else {
                        alert('Bir hata oluştu. Lütfen tekrar deneyiniz.');
                    }
                    OdemeDivAc('DivBkmExpress', 'hdnAcikDiv');
                    SayfaScrollTop();
                    GetTahsilatVeriOzeti();
                    GetTahsilatVerileri();
                }
                else {
                    alert('Uzun süredir işlem yapmadınız. Giriş sayfasına yönlendiriliyorsunuz. Lütfen yeniden giriş yapınız.');
                    location.href = "/MusteriHizmetleri/UyeGirisi.aspx";
                }
            },
            error: function (err) {
                //           alert('Hata No : 5 - Talep esnasında sorun oluştu. Yeniden deneyin');
            }
        });
        $('#chkBkmExpressSozlesmeOnay').removeAttr("checked");

    }




}

function GetTahsilatVerileri() {
    var AlinanTahsilatlar = '';
    var DivAlinanTahsilatlar = $('.paymentinformation').html();
    $.ajax({
        type: 'POST',
        url: '/Odeme.aspx/GetTahsilatVerileri',
        data: '{ }',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (result) {
            $.each(result.d, function (index, value) {
                AlinanTahsilatlar = AlinanTahsilatlar + '<div class=\"info\">' +
                                '<div class=\"info1\">' +
                                    'Ödeme Sayısı: <span class=\"colord8\">' + (index + 1) + '</span></div>' +
                                '<div class=\"info2\">' +
                                    'Ödeme Türü: <span class=\"colord8\">' + value.TahsilatAd + '</span></div>' +
                                '<div class=\"info3\">' +
                                    'Ödeme Tutarı: <span class=\"colord8\">' + value.TahsilatTutar + ' TL</span></div>' +
                            '</div>';
            });

            if (AlinanTahsilatlar != DivAlinanTahsilatlar) {
                $('.paymentinformation').fadeOut(500).html(AlinanTahsilatlar).fadeIn(500);
            }
        },
        error: function () {
            //            alert('Hata No : 6 - Talep esnasında sorun oluştu. Yeniden deneyin...');
        }
    });
}

function GetTahsilatVeriOzeti() {
    $.ajax({
        type: 'POST',
        url: '/Odeme.aspx/GetTahsilatVeriOzeti',
        data: '{ }',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (result) {

            if (result.d[3] == 1001)
                location.href = "/OdemeOnay.aspx";
            else {
                $('#lblSiparisTutar').html(result.d[0].toString() + ' TL');
                $('#lblOdenenTutar').html(result.d[1].toString() + ' TL');
                $('#lblKalanTutar').html(result.d[2].toString() + ' TL');
                GetTahsilatVerileri();
            }
        },
        error: function () {
            //            alert('Hata No : 7 - Talep esnasında sorun oluştu. Yeniden deneyin..');
        }
    });
}

function SayfaScrollTop() {
    $('html,body').animate({ scrollTop: 0 });
}


function UnblockDivGoster(mesaj) {
    $(document).ready(function () {
        $.blockUI({
            fadeIn: 600,
            fadeOut: 600,
            css: {
                cursor: 'default',
                top: ($(window).height() - 400) / 2 + 'px',
                left: ($(window).width() - 400) / 2 + 'px',
                width: '400px'
            },
            message: mesaj + '</br><a onclick="UnblockDivKapat()">Kapat</a>'

        });
    });
}

function LutfenBekleyinDiv() {
    $(document).ready(function () {
        $.blockUI({
            fadeIn: 600,
            fadeOut: 600,
            css: {
                cursor: 'default',
                top: ($(window).height() - 100) / 2 + 'px',
                left: ($(window).width() - 300) / 2 + 'px',
                width: '300px'
            },
            message: '<img src="/imagesgld/islemYapiliyor.gif" />'

        });
    });
}

function UnblockDivKapat() {

    $(document).ready(function () {
        $.unblockUI();
    });
}

function ParaKodOdemeBilgileriAc(ToplamTutar) {
    var _toplamTutar = ToplamTutar.toString().split('.');
    var TahsilatSecenekDetayId = $("input:radio[name=ParaKodDetay]:checked").val();
    $.ajax({
        type: 'POST',
        url: '/Odeme.aspx/GetParaKodQRGetir',
        data: '{ "TahsilatSecenekDetayId" : "' + TahsilatSecenekDetayId + '", "CekilecekTutar" : "' + ToplamTutar + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (result) {
            if (result.d != "-500") {
                $('#ParaKodQR').show();
                $('#ParaKodQR').attr("src", result.d);
            }
            else {
                alert('Uzun süredir işlem yapmadınız. Giriş sayfasına yönlendiriliyorsunuz. Lütfen yeniden giriş yapınız.');
                location.href = "/MusteriHizmetleri/UyeGirisi.aspx";
            }
        },
        complete: function () {
            ;
        },
        error: function (err) {
            //            alert('Hata No : 3 - Talep esnasında sorun oluştu. Yeniden deneyin');
        }
    });



    OdenenTL = _toplamTutar[0];
    OdenenKurus = _toplamTutar[1];




    $('#DivParaKodBilgileri .transferinfo').show();
    $('#DivParaKodBilgileri .moneytransfer').show();
    $('#DivParaKodBilgileri').slideDown(500).fadeIn(500);

    $('#txtParaKodOdenecekTL').val(OdenenTL);
    $('#txtParaKodOdenecekKurus').val(OdenenKurus);

    if ($('#txtParaKodOdenecekKurus').val() == '')
        $('#txtParaKodOdenecekKurus').val(0);
}

function GetParaKodSecenekleri() {
    $('#form1').validationEngine('hideAll');
    var BankaLogoHtml = '';
    $.ajax({
        type: 'POST',
        url: '/Odeme.aspx/GetTahsilatSecenekleri',
        data: '{ "TahsilatTur" : "ParaKod"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (result) {
            $.each(result.d, function (index, value) {
                BankaLogoHtml = BankaLogoHtml + '<div class=\"banklogo blPassive\">' +
                '<div class=\"blLeft\">' +
                '<input name=\"ParaKodBanka\" type=\"radio\" value=\"' + value.MasterId + '\" onclick=\"GetParaKodDetaylari(this.value);\" />' +
                '</div>' +
                '<div class=\"blRight\"><img src="' + value.BankaResimYol + '"/></div></div>';
            });
            $('#DivParaKod .leftcolumn').html(BankaLogoHtml).fadeIn(300);
        },
        complete: function () {
            $('input[name="ParaKodBanka"]').click();
        },
        error: function () {
            //            alert('Hata No : 3 - Talep esnasında sorun oluştu. Yeniden deneyin');
        }
    });
}

function GetParaKodDetaylari(BankaMasterId) {
    $('#DivParaKodBilgileri').fadeOut(1500);
    var ParaKodDetayHtml = '<div class=\"title\">' +
                                                '<div class=\"title1 fontSz14\">' +
                                                    'T.Seçeneği</div>' +
                                                '<div class=\"title2 fontSz14\">' +
                                                    'Taksit Sayısı</div>' +
                                                '<div class=\"title3 fontSz14\">' +
                                                    'Taksit Tutarı(TL)</div>' +
                                                '<div class=\"title4 fontSz14\">' +
                                                    'Toplam Tutar(TL)</div>' +
                                                '<div class=\"title5 fontSz14\">' +
                                                    'Ödeme Avantajınız</div>' +
                                            '</div>';
    $.ajax({
        type: 'POST',
        url: '/Odeme.aspx/GetTahsilatSecenekDetaylari',
        data: '{ "TahsilatSecenekId" : "' + BankaMasterId + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $('#DivParaKod .rightcolumn .creditcardroot .financingoptions').fadeOut(100);
        },
        success: function (result) {
            $.each(result.d, function (index, value) {
                if (index % 2 == 0) {
                    ParaKodDetayHtml = ParaKodDetayHtml + '<div class=\"chooes\">' +
                                                '<div class=\"chooes1 bgcolore8\">' +
                                                    '<input name=\"ParaKodDetay\" type=\"radio\" value=\"' + value.DetayId + '\" onclick=\"ParaKodOdemeBilgileriAc(' + value.ToplamTutar + ');\" /></Div>' +
                                                '<div class=\"chooes2 bgcolore8\">' +
                                                     value.gTaksitAdet + '</div>' +
                                                '<div class=\"chooes3 bgcolore8\">' +
                                                    value.TaksitTutar + '</div>' +
                                                '<div id=\"OdenecekParaKod' + value.DetayId + '\"  class=\"chooes4 bgcolore8\">' +
                                                    value.ToplamTutar + '</div>' +
                                                '<div class=\"chooes5 bgcolore8\">' +
                                                     value.TahsilatAd + '</div>' +
                                                     '</div>';
                }
                else {
                    ParaKodDetayHtml = ParaKodDetayHtml + '<div class=\"chooes\">' +
                                                '<div class=\"chooes1 bgcolorf8\">' +
                                                     '<input name=\"ParaKodDetay\" type=\"radio\" value=\"' + value.DetayId + '\" onclick=\"ParaKodOdemeBilgileriAc(' + value.ToplamTutar + ');\" /></Div>' +
                                                '<div class=\"chooes2 bgcolorf8\">' +
                                                   value.gTaksitAdet + '</div>' +
                                                '<div class=\"chooes3 bgcolorf8\">' +
                                                      value.TaksitTutar + '</div>' +
                                              '<div id=\"OdenecekParaKod' + value.DetayId + '\" class=\"chooes4 bgcolorf8\">' +
                                                   value.ToplamTutar + '</div>' +
                                                '<div class=\"chooes5 bgcolorf8\">' +
                                                      value.TahsilatAd + '</div>' +
                                                       '</div>';

                }
            });


        },

        complete: function () {
            $('#DivParaKod .rightcolumn .creditcardroot .financingoptions').fadeIn(500).html(ParaKodDetayHtml);
        },
        error: function () {
            //alert('Hata No : 2 - Talep esnasında sorun oluştu. Yeniden deneyin');
        }
    });
}

function ParaKodOdemeYap(GuvenlikSeviye) {
    var TahsilatSecenekId = '';
    var TahsilatSecenekDetayId = '';
    var CekilecekTutar = '';
    var OdenecekToplam = '';
    var ParcaliOdendi;

    if ($('#form1').validationEngine('validate') == true) {

        TahsilatSecenekDetayId = $("input:radio[name=ParaKodDetay]:checked").val();
        TahsilatSecenekId = $("input:radio[name=ParaKodBanka]:checked").val();
        OdenecekToplam = $('#OdenecekParaKod' + TahsilatSecenekDetayId).html().replace(' TL', '');

        $('#btnParaKodOdemeYap').removeAttr("onclick")

        $('#chkParaKodSozlesmeOnay').removeAttr("checked");

        if (OdenecekToplam.indexOf('.') < 0)
            OdenecekToplam = OdenecekToplam + '.0';

        if ($('#txtParaKodtOdenecekKurus').val() == '')
            CekilecekTutar = $('#txtParaKodOdenecekTL').val() + '.0';
        else
            CekilecekTutar = $('#txtParaKodOdenecekTL').val() + '.' + $('#txtParaKodOdenecekKurus').val();

        if (CekilecekTutar == OdenecekToplam)
            ParcaliOdendi = false;
        else
            ParcaliOdendi = true;


        $.ajax({
            type: 'POST',
            url: '/Odeme.aspx/ParaKodOdemeGerceklestir',
            data: '{ "TahsilatSecenekId" : "' + TahsilatSecenekId + '", "TahsilatSecenekDetayId" : "' + TahsilatSecenekDetayId + '", "PuanSorgula" : "false", "CekilecekTutar" : "' + CekilecekTutar + '", "ParcaliOdendi" : "' + ParcaliOdendi + '" }',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                LutfenBekleyinDiv();
            },
            success: function (result) {
                UnblockDivKapat();
                if (result.d[0].toString() != "-500") {

                    if (result.d[0] != "Onay" && result.d[1] != null && result.d[1] != '')
                        UnblockDivGoster(result.d[1].toString());

                    OdemeDivAc('DivParaKod', 'hdnAcikDiv');
                    SayfaScrollTop();
                    GetTahsilatVeriOzeti();
                    GetTahsilatVerileri();
                }
                else {
                    alert('Uzun süredir işlem yapmadınız. Giriş sayfasına yönlendiriliyorsunuz. Lütfen yeniden giriş yapınız.');
                    location.href = "/MusteriHizmetleri/UyeGirisi.aspx";
                }
            }
        });

    }
}
