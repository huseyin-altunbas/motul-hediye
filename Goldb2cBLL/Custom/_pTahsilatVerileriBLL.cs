﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class TahsilatVerileriBLL
    {

        public ErpData.GoldB2C baglanti;
        public ErpData.TahsilatVerileri tahsilatVerileri;

        ErpData.StrongTypesNS.TahsilatRotatorTTDataTable dtTahsilatRotatorTT;
        ErpData.StrongTypesNS.TahsilatYontemleriTTDataTable dtTahsilatYontemleriTT;
        ErpData.StrongTypesNS.TahsilatSecenekleriTTDataTable dtTahsilatSecenekleriTT;
        ErpData.StrongTypesNS.TahsilatSecenekDetaylariTTDataTable dtTahsilatSecenekDetaylariTT;
        ErpData.StrongTypesNS.TahsilatDataYapisiTTDataTable dtTahsilatDataYapisiTT;
        ErpData.StrongTypesNS.TahsilatVerileriTTDataTable dtTahsilatVerileriTT;
        ErpData.StrongTypesNS.TaksitSecenekleriDSDataSet dsTaksitSecenekleri;

        public DataTable GetTahsilatRotator(int UrunId)
        {
            dtTahsilatRotatorTT = new ErpData.StrongTypesNS.TahsilatRotatorTTDataTable();
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                tahsilatVerileri = new ErpData.TahsilatVerileri(baglanti);
                tahsilatVerileri.GetTahsilatRotator(UrunId, out dtTahsilatRotatorTT);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (tahsilatVerileri != null) tahsilatVerileri.Dispose();
            }

            return dtTahsilatRotatorTT;

        }

        public DataTable GetTahsilatYontemleri(int SiparisMasterId)
        {
            dtTahsilatYontemleriTT = new ErpData.StrongTypesNS.TahsilatYontemleriTTDataTable();
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                tahsilatVerileri = new ErpData.TahsilatVerileri(baglanti);
                tahsilatVerileri.GetTahsilatYontemleri(SiparisMasterId, out dtTahsilatYontemleriTT);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (tahsilatVerileri != null) tahsilatVerileri.Dispose();
            }

            return dtTahsilatYontemleriTT;

        }


        public DataTable GetTahsilatSecenekleri(int SiparisMasterId, string TahsilatTur)
        {
            dtTahsilatSecenekleriTT = new ErpData.StrongTypesNS.TahsilatSecenekleriTTDataTable();

            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                tahsilatVerileri = new ErpData.TahsilatVerileri(baglanti);
                tahsilatVerileri.GetTahsilatSecenekleri(SiparisMasterId, TahsilatTur, out dtTahsilatSecenekleriTT);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (tahsilatVerileri != null) tahsilatVerileri.Dispose();
            }

            return dtTahsilatSecenekleriTT;
        }


        public DataTable GetTahsilatDataYapisi(int TahsilatSeceneDetayId, string CekimTur)
        {
            dtTahsilatDataYapisiTT = new ErpData.StrongTypesNS.TahsilatDataYapisiTTDataTable();
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                tahsilatVerileri = new ErpData.TahsilatVerileri(baglanti);
                tahsilatVerileri.GetTahsilatDataYapisi(TahsilatSeceneDetayId, CekimTur, out dtTahsilatDataYapisiTT);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (tahsilatVerileri != null) tahsilatVerileri.Dispose();
            }

            return dtTahsilatDataYapisiTT;
        }

        public DataTable GetTahsilatSecenekDetaylari(int SiparisMasterId, int TahsilatSecenekId)
        {
            dtTahsilatSecenekDetaylariTT = new ErpData.StrongTypesNS.TahsilatSecenekDetaylariTTDataTable();

            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                tahsilatVerileri = new ErpData.TahsilatVerileri(baglanti);
                tahsilatVerileri.GetTahsilatSecenekDetaylari(SiparisMasterId, TahsilatSecenekId, out dtTahsilatSecenekDetaylariTT);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (tahsilatVerileri != null) tahsilatVerileri.Dispose();
            }

            return dtTahsilatSecenekDetaylariTT;
        }

        public void GetTahsilatVeriOzeti(int SiparisMasterId, out decimal SiparisTutar, out decimal OdenenTutar, out bool TahsilatTamamlandi)
        {
            dtTahsilatVerileriTT = new ErpData.StrongTypesNS.TahsilatVerileriTTDataTable();
            SiparisTutar = 0;
            OdenenTutar = 0;
            TahsilatTamamlandi = false;
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                tahsilatVerileri = new ErpData.TahsilatVerileri(baglanti);
                tahsilatVerileri.GetTahsilatVeriOzeti(SiparisMasterId, out SiparisTutar, out OdenenTutar, out TahsilatTamamlandi);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (tahsilatVerileri != null) tahsilatVerileri.Dispose();
            }
        }

        public DataTable GetTahsilatVerileri(int SiparisMasterId)
        {
            dtTahsilatVerileriTT = new ErpData.StrongTypesNS.TahsilatVerileriTTDataTable();
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                tahsilatVerileri = new ErpData.TahsilatVerileri(baglanti);
                tahsilatVerileri.GetTahsilatVerileri(SiparisMasterId, out dtTahsilatVerileriTT);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (tahsilatVerileri != null) tahsilatVerileri.Dispose();
            }

            return dtTahsilatVerileriTT;
        }

        public DataSet GetTaksitSecenekleri(int UrunId, out List<int> taksitSayilari)
        {
            dsTaksitSecenekleri = new ErpData.StrongTypesNS.TaksitSecenekleriDSDataSet();
            List<int> TaksitSayi = new List<int>();
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                tahsilatVerileri = new ErpData.TahsilatVerileri(baglanti);
                tahsilatVerileri.GetTaksitSecenekleri(UrunId, out dsTaksitSecenekleri);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (tahsilatVerileri != null) tahsilatVerileri.Dispose();
            }


            if (dsTaksitSecenekleri.TahsilatSecenekDetaylariTT != null && dsTaksitSecenekleri.TahsilatSecenekDetaylariTT.Rows.Count > 0)
            {
                var _taksitSayilari = (from a in dsTaksitSecenekleri.TahsilatSecenekDetaylariTT.AsEnumerable()
                                       orderby a.Field<int>("gtaksit_adet") ascending
                                       select a.Field<int>("gtaksit_adet")).Distinct();

                if (_taksitSayilari.Any())
                {
                    TaksitSayi = _taksitSayilari.ToList();

                }

            }

            taksitSayilari = TaksitSayi;
            return dsTaksitSecenekleri;

        }

    }
}
