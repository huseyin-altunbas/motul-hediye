﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cInfrastructure;
using System.Data;
using System.Web;

namespace Goldb2cBLL
{
    public class SepetIslemleriBLL
    {
        public ErpData.GoldB2C baglanti;
        public ErpData.SepetIslemleri sepetIslemleri;

        public void SepetGuncelle(int ErpCariId)
        {
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                sepetIslemleri = new ErpData.SepetIslemleri(baglanti);
                sepetIslemleri.SepetGuncelle(ErpCariId);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (sepetIslemleri != null) sepetIslemleri.Dispose();
            }

        }

        public DataSet SepetGoruntule(int ErpCariId)
        {
            ErpData.StrongTypesNS.SepetIcerikDSDataSet dsSepetIcerik = new ErpData.StrongTypesNS.SepetIcerikDSDataSet();
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                sepetIslemleri = new ErpData.SepetIslemleri(baglanti);
                sepetIslemleri.SepetGoruntule(ErpCariId, out dsSepetIcerik);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (sepetIslemleri != null) sepetIslemleri.Dispose();
            }

            return dsSepetIcerik;
        }


        public void SepetOlustur(int ErpCariId, int UrunId, string BundleUrunId, string IndirimliUrunId, int VipUrunId, int HediyePaketiId, int UrunAdet, int AksiyonId)
        {

            string IpAdres = UyelikIslemleriBLL.FindIP(HttpContext.Current.Request.Headers);

            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                sepetIslemleri = new ErpData.SepetIslemleri(baglanti);
                sepetIslemleri.SepetOlustur(ErpCariId, UrunId, BundleUrunId, IndirimliUrunId, VipUrunId, HediyePaketiId, UrunAdet, AksiyonId, IpAdres);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (sepetIslemleri != null) sepetIslemleri.Dispose();
            }
        }

        public void UrunSil(int SepetMasterId, string SepetDetayId)
        {
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                sepetIslemleri = new ErpData.SepetIslemleri(baglanti);
                sepetIslemleri.UrunSil(SepetMasterId, SepetDetayId);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (sepetIslemleri != null) sepetIslemleri.Dispose();
            }
        }


    }
}
