﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class KarsilastirmaIslemleriBLL
    {
        ErpData.GoldB2C baglanti;
        ErpData.KarsilastirmaIslemleri karsilastirmaIslemleri;

        ErpData.StrongTypesNS.UrunKarsilastirTTDataTable dtUrunKarsilastir;

        public DataTable KarsilastirmaGoster(int ErpCariId)
        {
            try
            {
                dtUrunKarsilastir = new ErpData.StrongTypesNS.UrunKarsilastirTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                karsilastirmaIslemleri = new ErpData.KarsilastirmaIslemleri(baglanti);
                karsilastirmaIslemleri.KarsilastirmaGoster(ErpCariId, out dtUrunKarsilastir);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (karsilastirmaIslemleri != null)
                    karsilastirmaIslemleri.Dispose();
            }

            return dtUrunKarsilastir;
        }

        public void KarsilastirmaKaydet(int ErpCariId, int UrunId)
        {
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                karsilastirmaIslemleri = new ErpData.KarsilastirmaIslemleri(baglanti);
                karsilastirmaIslemleri.KarsilastirmaKaydet(ErpCariId, UrunId);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (karsilastirmaIslemleri != null)
                    karsilastirmaIslemleri.Dispose();
            }
        }

        public void KarsilastirmaSil(int ErpCariId, int UrunId)
        {
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                karsilastirmaIslemleri = new ErpData.KarsilastirmaIslemleri(baglanti);
                karsilastirmaIslemleri.KarsilastirmaSil(ErpCariId, UrunId);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (karsilastirmaIslemleri != null)
                    karsilastirmaIslemleri.Dispose();
            }
        }
    }
}
