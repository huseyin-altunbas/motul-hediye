﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Goldb2cInfrastructure;


namespace Goldb2cBLL
{
    /// <summary>
    /// Summary description for OdemeFirsatBLL
    /// </summary>
    public class OdemeFirsatBLL
    {
        public OdemeFirsatBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ErpData.GoldB2C baglanti;
        public ErpData.OdemeFirsat OdemeFirsat;

        ErpData.StrongTypesNS.OdemeFirsatlariTTDataTable dtOdemeFirsatlari;

        public DataTable GetirOdemeFirsatlari(int UyelikDurum)
        {
            try
            {
                dtOdemeFirsatlari = new ErpData.StrongTypesNS.OdemeFirsatlariTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                OdemeFirsat = new ErpData.OdemeFirsat(baglanti);
                OdemeFirsat.OdemeFirsatlari(UyelikDurum, out dtOdemeFirsatlari);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (OdemeFirsat != null)
                    OdemeFirsat.Dispose();
            }

            return dtOdemeFirsatlari;
        }
    }
}