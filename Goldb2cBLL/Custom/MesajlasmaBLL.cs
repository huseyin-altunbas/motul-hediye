﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cDAT;

namespace Goldb2cBLL
{
    public class MesajBLL
    {
        public MesajBLL()
        {
        }

        public DataTable SelectMesajForWebByMusteriKullanicId(int kullaniciId)
        {
            MesajDAL mesajDAL = new MesajDAL();
            return mesajDAL.SelectMesajForWebByMusteriKullanicId(kullaniciId);
        }

        public DataTable SelectMesajForWebByParentMesajId(int mesajId)
        {
            MesajDAL mesajDAL = new MesajDAL();
            return mesajDAL.SelectMesajForWebByParentMesajId(mesajId);
        }

        public DataTable SelectMemnuniyetDurumForWeb()
        {
            MesajDAL mesajDAL = new MesajDAL();
            return mesajDAL.SelectMemnuniyetDurumForWeb();
        }

        public DataTable SelectMesajByMesajId(int mesajId)
        {
            MesajDAL mesajDAL = new MesajDAL();
            return mesajDAL.SelectMesajByMesajId(mesajId);
        }

        public bool UpdateMesajByMesajId(DataRow parDataRow)
        {
            MesajDAL mesajDAL = new MesajDAL();
            return mesajDAL.UpdateMesajByMesajId(parDataRow);
        }

        public bool UpdateMesajForWebByParentMesajId(int parentMesajId)
        {
            MesajDAL mesajDAL = new MesajDAL();
            return mesajDAL.UpdateMesajForWebByParentMesajId(parentMesajId);
        }

        public int InsertMesaj(DataRow parDataRow)
        {
            MesajDAL mesajDAL = new MesajDAL();
            return mesajDAL.InsertMesaj(parDataRow);
        }

        public int InsertMesajCevapForWeb(DataRow parDataRow)
        {
            MesajDAL mesajDAL = new MesajDAL();
            return mesajDAL.InsertMesajCevapForWeb(parDataRow);
        }
    }

    public class DepartmanBLL
    {
        public DepartmanBLL()
        {
        }

        public DataTable SelectDepartman()
        {
            DepartmanDAL departmanDAL = new DepartmanDAL();
            return departmanDAL.SelectDepartman();
        }

        public DataTable SelectAltDepartmanByDepartmanId(int departmanId)
        {
            DepartmanDAL departmanDAL = new DepartmanDAL();
            return departmanDAL.SelectAltDepartmanByDepartmanId(departmanId);
        }
    }

    public class MesajKullaniciBLL
    {
        public MesajKullaniciBLL()
        {
        }

        public DataTable SelectMesajKullaniciByAltDepartmanId(int AltDepartmanId)
        {
            MesajKullaniciDAL dalMesajKullanici = new MesajKullaniciDAL();
            return dalMesajKullanici.SelectMesajKullaniciByAltDepartmanId(AltDepartmanId);

        }
    }



}
