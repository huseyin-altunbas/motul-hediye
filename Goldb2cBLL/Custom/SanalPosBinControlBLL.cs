﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Goldb2cBLL
{
    public class SanalPosBinControlBLL
    {
        public SanalPosBinControlBLL()
        {

        }

        static List<string> YkbBinler = new List<string>(new string[] {
    "401622", "404809", "406281", "407046", "409021", "409022", "413382", "414391", "414392", "420342", "420343",
    "420344", "442106", "446212", "450634", "455359", "455360", "477959", "479794", "479795", "490983", "491205",
    "491206", "492128", "492129", "492130", "492131", "494314", "499956", "499957", "499958", "501774", "510054",
    "510117", "527504", "527551", "540061", "540062", "540063", "540122", "540129", "542117", "545103", "545472",
    "545473", "545719", "552645", "552659", "554422", "558295", "588691", "588992", "589117", "601881", "602678",
    "602908", "602981", "603098", "603157", "603797", "639004", "676166" });


        static List<string> TebBinler = new List<string>(new string[] {
   "402458","402459","406015","440293","459026","489494","489495","489496","510221","512753","512803","524346","524839","524840","528920","530853","545124","553090"});


        static List<string> IngBinler = new List<string>(new string[] { 
            "480296", "420322", "420323", "420324", "400684", "408579", "510151", "532443", "542967", "542965", "554570", "547765", "550074", "526973", "526975" });

        static List<string> AnadoluBinler = new List<string>(new string[] { 
            "425846", "425847", "425848", "522240", "522241", "558593", "441341", "554301"});

        static List<string> VakifBinler = new List<string>(new string[] { 
    "493841", "411979", "493846", "542119", "540045", "552101", "542804", "542798", "540046", "520017", "493840", 
    "416757", "411944", "411943", "411942", "411724", "409084", "402940", "428945", "547244", "415792"});

        static List<string> AlbarakaBinler = new List<string>(new string[] {  
    "432284", "432285", "417715", "534264", "547234"});

        static List<string> ParafBinler = new List<string>(new string[] {  
    "415514","420578","492094","492095","498850","498851","498852","510056","510164","521378","540284","540435","543039","543081","552879"});

        public static bool BinMevcutmu(string kartNo, BinBankaIsim bankaIsim)
        {
            List<String> dizi = null;

            switch (bankaIsim)
            {
                case BinBankaIsim.YapiKrediBankasi:
                    dizi = YkbBinler;
                    break;
                case BinBankaIsim.TebBankasi:
                    dizi = TebBinler;
                    break;
                case BinBankaIsim.AnadoluBank:
                    dizi = AnadoluBinler;
                    break;
                case BinBankaIsim.VakifBank:
                    dizi = VakifBinler;
                    break;
                case BinBankaIsim.Albaraka:
                    dizi = AlbarakaBinler;
                    break;
                case BinBankaIsim.ParafKart:
                    dizi = ParafBinler;
                    break;
                case BinBankaIsim.IngBank:
                    dizi = IngBinler;
                    break;
                default:
                    break;
            }
            string ilk6hane = string.Empty;

            if (kartNo.Length < 6) return false;

            ilk6hane = kartNo.Substring(0, 6);

            for (int i = 0; i < dizi.Count; i++)
                if (dizi[i] == ilk6hane)
                    return true;
            return false;
        }
    }

    public enum BinBankaIsim
    {
        YapiKrediBankasi,
        TebBankasi,
        AnadoluBank,
        VakifBank,
        Albaraka,
        ParafKart,
        IngBank
    }

}


