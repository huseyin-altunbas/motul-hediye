﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using Goldb2cEntity;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class SiparisIslemBLL
    {
        SiparisIslemDAT datSiparisIslem = new SiparisIslemDAT();

        public int SiparisIslemKaydet(int siparisId, int urunId, DateTime siparisIslemTarihi)
        {
            try
            {
                return datSiparisIslem.SiparisIslemKaydet(siparisId, urunId, siparisIslemTarihi);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable SiparisIslemGetir()
        {
            DataTable dtSiparisIslem = new DataTable();
            try
            {
                dtSiparisIslem = datSiparisIslem.SiparisIslemGetir();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtSiparisIslem;
        }

    }
}
