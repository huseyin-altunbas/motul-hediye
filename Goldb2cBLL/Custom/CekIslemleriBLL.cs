﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using Goldb2cEntity;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public partial class CekIslemleriBLL
    {
        CekIslemleriDAT datCekIslemleri = new CekIslemleriDAT();

        public DataTable IndirimCekleriKullaniciGetir(int kullaniciId, int firmaKod)
        {
            return datCekIslemleri.IndirimCekleriKullaniciGetir(kullaniciId, firmaKod);
        }

        public int IndirimCekleriKullaniciKaydet(int kullaniciId, string gonderilenEmailAdresi, int firmaKod)
        {
            return datCekIslemleri.IndirimCekleriKullaniciKaydet(kullaniciId, gonderilenEmailAdresi, firmaKod);
        }

        public DataTable IndirimCekleriAlanKullanicilar(int kullaniciId, string gonderilmeTarih1, string gonderilmeTarih2, int firmaKod)
        {
            return datCekIslemleri.IndirimCekleriAlanKullanicilar(kullaniciId, gonderilmeTarih1, gonderilmeTarih2, firmaKod);
        }

        public DataTable IndirimCekleriFirmaGetir(int firmaKod)
        {
            return datCekIslemleri.IndirimCekleriFirmaGetir(firmaKod);
        }

        public DataTable IndirimCekleriFirmalarGetir()
        {
            return datCekIslemleri.IndirimCekleriFirmalarGetir();
        }

    }
}
