﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cInfrastructure;
using System.Web;
using Goldb2cEntity;

namespace Goldb2cBLL
{
    public class PortalHistory
    {
        public ErpData.GoldB2C baglanti;
        public ErpData.PortalHistory portalHistory;

        public void GetTiklamaHistory(string TiklananLink, string KullaniciEmail)
        {
            string TiklamaTarih = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                portalHistory = new ErpData.PortalHistory(baglanti);
                portalHistory.GetTiklamaHistory(TiklananLink, KullaniciEmail, TiklamaTarih);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (portalHistory != null) portalHistory.Dispose();
            }

        }
    }
}
