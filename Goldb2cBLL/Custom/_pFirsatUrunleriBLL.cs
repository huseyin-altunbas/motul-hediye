﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Goldb2cInfrastructure;


namespace Goldb2cBLL
{
    /// <summary>
    /// Summary description for FirsatUrunleriBLL
    /// </summary>
    public class FirsatUrunleriBLL
    {
        public FirsatUrunleriBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        ErpData.GoldB2C baglanti;
        ErpData.FirsatUrunleri firsatUrunleri;
        ErpData.StrongTypesNS.FirsatUrunTTDataTable dtFirsatUrunTT;

        public DataTable GetFirsatUrun(int ErpCariId, int UrunGrupId)
        {
            try
            {
                dtFirsatUrunTT = new ErpData.StrongTypesNS.FirsatUrunTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                firsatUrunleri = new ErpData.FirsatUrunleri(baglanti);
                firsatUrunleri.GetFirsatUrun(ErpCariId, UrunGrupId, out dtFirsatUrunTT);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (firsatUrunleri != null)
                    firsatUrunleri.Dispose();
            }
            return dtFirsatUrunTT;
        }
    }
}