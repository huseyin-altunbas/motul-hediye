﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cInfrastructure;


namespace Goldb2cBLL
{
    public class SiparisGoruntulemeBLL
    {
        public static ErpData.GoldB2C baglanti;
        public static ErpData.SiparisGoruntuleme siparisGoruntuleme;

        ErpData.StrongTypesNS.SiparisVerileriDSDataSet dsSiparisVerileri;
        ErpData.StrongTypesNS.GenelSiparisIzlemeDSDataSet dsGenelSiparisIzleme;
        ErpData.StrongTypesNS.KargoTakipNoTTDataTable dtKargoTakipNo;
        ErpData.StrongTypesNS.SiparisBilgilendirmeTTDataTable dtSiparisBilgilendirme;
        ErpData.StrongTypesNS.CariSiparisListesiDSDataSet dsCariSiparisListesi;
        ErpData.StrongTypesNS.CariStokListesiDSDataSet dsCariStokListesi;

        public DataSet SiparisGoruntule(int SiparisMasterId, int ErpCariId)
        {
            try
            {
                dsSiparisVerileri = new ErpData.StrongTypesNS.SiparisVerileriDSDataSet();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisGoruntuleme = new ErpData.SiparisGoruntuleme(baglanti);
                siparisGoruntuleme.SiparisGoruntule(SiparisMasterId, ErpCariId, out dsSiparisVerileri);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisGoruntuleme != null)
                    siparisGoruntuleme.Dispose();
            }
            return dsSiparisVerileri;
        }

        public DataSet GenelSiparisIzleme(int ErpCariId)
        {
            try
            {
                dsGenelSiparisIzleme = new ErpData.StrongTypesNS.GenelSiparisIzlemeDSDataSet();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisGoruntuleme = new ErpData.SiparisGoruntuleme(baglanti);
                siparisGoruntuleme.GenelSiparisIzleme(ErpCariId, out dsGenelSiparisIzleme);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisGoruntuleme != null)
                    siparisGoruntuleme.Dispose();
            }
            return dsGenelSiparisIzleme;
        }

        public DataTable KargoTakipNoGetir(string SorguTip, string FaturaBelgeNo, DateTime FaturaTarihBas, DateTime FaturaTarihBit)
        {
            try
            {
                dtKargoTakipNo = new ErpData.StrongTypesNS.KargoTakipNoTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisGoruntuleme = new ErpData.SiparisGoruntuleme(baglanti);
                siparisGoruntuleme.KargoTakipNoGetir(SorguTip, FaturaBelgeNo, FaturaTarihBas, FaturaTarihBit, out dtKargoTakipNo);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisGoruntuleme != null)
                    siparisGoruntuleme.Dispose();
            }
            return dtKargoTakipNo;
        }

        public DataTable SiparisBilgilendirmeFormu(int SiparisId)
        {
            try
            {
                dtSiparisBilgilendirme = new ErpData.StrongTypesNS.SiparisBilgilendirmeTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisGoruntuleme = new ErpData.SiparisGoruntuleme(baglanti);
                siparisGoruntuleme.SiparisBilgilendirmeFormu(SiparisId, out dtSiparisBilgilendirme);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisGoruntuleme != null)
                    siparisGoruntuleme.Dispose();
            }
            return dtSiparisBilgilendirme;
        }

        public void SiparisOnay(int InpSipMasterId, string InpOnayDurum1, string InpOnayDurum2, out int OutSiparisMasterId)
        {
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisGoruntuleme = new ErpData.SiparisGoruntuleme(baglanti);
                siparisGoruntuleme.SiparisOnay(InpSipMasterId, InpOnayDurum1, InpOnayDurum2, out OutSiparisMasterId);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisGoruntuleme != null)
                    siparisGoruntuleme.Dispose();
            }
            // return dtSiparisBilgilendirme;
        }


        public void SiparisOnayDurum(int InpSipMasterId, string InpOnayDurum1, string InpOnayDurum2, out string outOnayDurum1, out string outOnayDurum2)
        {
            try
            {

                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisGoruntuleme = new ErpData.SiparisGoruntuleme(baglanti);
                siparisGoruntuleme.SiparisOnayDurum(InpSipMasterId, InpOnayDurum1, InpOnayDurum2, out outOnayDurum1, out outOnayDurum2);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisGoruntuleme != null)
                    siparisGoruntuleme.Dispose();
            }
            // return dtSiparisBilgilendirme;
        }


        public DataSet GenelSiparisListeleme(int ilkCariId, int sonCariId, int ilkSiparisId, int sonSiparisId, DateTime ilkTarih, DateTime sonTarih)
        {
            try
            {
                dsCariSiparisListesi = new ErpData.StrongTypesNS.CariSiparisListesiDSDataSet();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisGoruntuleme = new ErpData.SiparisGoruntuleme(baglanti);
                siparisGoruntuleme.GenelSiparisListesi(ilkCariId, sonCariId, ilkSiparisId, sonSiparisId, ilkTarih, sonTarih, out dsCariSiparisListesi);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisGoruntuleme != null)
                    siparisGoruntuleme.Dispose();
            }
            return dsCariSiparisListesi;
        }

        public DataSet GenelStokListeleme(int ilkStokId, int sonStokId, int ilkCariId, int sonCariId, string ilkSiparisNo, string sonSiparisNo, DateTime ilkSiparisTarih, DateTime sonSiparisTarih, DateTime ilkOnayTarih, DateTime sonOnayTarih, string onayDurum)
        {
            try
            {
                dsCariStokListesi = new ErpData.StrongTypesNS.CariStokListesiDSDataSet();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisGoruntuleme = new ErpData.SiparisGoruntuleme(baglanti);
                siparisGoruntuleme.CariStokListesi(ilkStokId, sonStokId, ilkCariId, sonCariId, ilkSiparisNo, sonSiparisNo, ilkSiparisTarih, sonSiparisTarih, ilkOnayTarih, sonOnayTarih, onayDurum, out dsCariStokListesi);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisGoruntuleme != null)
                    siparisGoruntuleme.Dispose();
            }
            return dsCariStokListesi;
        }
    }

}

