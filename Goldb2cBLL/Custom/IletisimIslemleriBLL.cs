﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using Goldb2cEntity;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public partial class IletisimIslemleriBLL
    {
        IletisimIslemleriDAT datIletisimIslemleri = new IletisimIslemleriDAT();

        public DataTable IletisimFormuGetir()
        {
            return datIletisimIslemleri.IletisimFormuGetir();
        }

        public int IletisimFormuKaydet(string ad, string soyad, string email, string cepTelKod, string cepTelNo, string kartNo, string soru)
        {
            string ipAdres = FindIP(HttpContext.Current.Request.Headers);

            return datIletisimIslemleri.IletisimFormuKaydet(ad, soyad, email, cepTelKod, cepTelNo, kartNo, soru, ipAdres);
        }

        public static string FindIP(NameValueCollection headers)
        {

            for (int i = 0; i < headers.Count; i++)
            {
                if (string.Equals(headers.GetKey(i), "Client-IP",
                    System.StringComparison.OrdinalIgnoreCase))
                {
                    return headers.Get(i);
                }
            }

            return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }


    }
}
