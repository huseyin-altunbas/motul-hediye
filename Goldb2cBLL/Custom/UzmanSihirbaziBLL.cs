﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cDAT;

namespace Goldb2cBLL
{
    public class UzmanSihirbaziBLL
    {
        UzmanSihirbaziDAT datUzmanSihirbazi = new UzmanSihirbaziDAT();

        public DataTable GetirUzmanGorusuSablon()
        {
            return datUzmanSihirbazi.GetirUzmanGorusuSablon();
        }

        public DataTable GetirUzmanGorusuSablonById(int SablonId)
        {
            return datUzmanSihirbazi.GetirUzmanGorusuSablonById(SablonId);
        }
    }
}
