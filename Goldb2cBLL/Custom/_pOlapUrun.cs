﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class OlapUrun
    {
        public OlapUrun()
        {

        }

        ErpData.GoldB2C baglanti;
        ErpData.OlapUrun olapUrun;
        ErpData.StrongTypesNS.UrunKartTTDataTable dtUrunKart;
        ErpData.StrongTypesNS.CokSatanUrunKartTTDataTable dtCokSatanUrunKartTT;

        public DataTable getOlapUrun()
        {
            try
            {
                dtUrunKart = new ErpData.StrongTypesNS.UrunKartTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                olapUrun = new ErpData.OlapUrun(baglanti);
                //olapUrun.GetOlapUrun();
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (olapUrun != null)
                    olapUrun.Dispose();
            }
            return dtUrunKart;
        }


        public DataTable CokSatanUrun(int UrunGrupId, int MarkaId, int UrunId)
        {
            try
            {
                dtCokSatanUrunKartTT = new ErpData.StrongTypesNS.CokSatanUrunKartTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                olapUrun = new ErpData.OlapUrun(baglanti);
                olapUrun.CokSatanUrun(UrunGrupId, MarkaId, UrunId, out dtCokSatanUrunKartTT);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (olapUrun != null)
                    olapUrun.Dispose();
            }
            return dtCokSatanUrunKartTT;
        }


    }
}
