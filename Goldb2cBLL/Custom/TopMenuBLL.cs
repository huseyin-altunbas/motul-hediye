﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;


namespace Goldb2cBLL
{
    /// <summary>
    /// Summary description for TopMenuBLL
    /// </summary>
    public class TopMenuBLL
    {
        public TopMenuBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        CacheHelper cacheHelper = new CacheHelper();

        public DataTable SelectTopMenu()
        {
            DataSet dsTopMenu;
            object CacheObject = null;
            CacheObject = cacheHelper.GetFromCache(CacheNames.defaultCache, "TopMenuBLL_SelectTopMenu");
            if (CacheObject != null)
            {
                dsTopMenu = CacheObject as DataSet;
            }
            else
            {
                TopMenuDAT datTopMenu = new TopMenuDAT();
                dsTopMenu = new DataSet();
                dsTopMenu.Tables.Add(datTopMenu.SelectTopMenu());
                cacheHelper.SaveCache(CacheNames.defaultCache, "TopMenuBLL_SelectTopMenu", dsTopMenu);
            }

            return dsTopMenu.Tables[0];
        }
    }
}