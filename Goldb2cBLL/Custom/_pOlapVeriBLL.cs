﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    /// <summary>
    /// Summary description for OlapVeriBLL
    /// </summary>
    public class OlapVeriBLL
    {
        public OlapVeriBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ErpData.GoldB2C baglanti;
        public ErpData.OlapVeri olapVeri;

        ErpData.StrongTypesNS.UrunGrupMarkalariTTDataTable dtUrunGrupMarkalariTT;

        public DataTable GetUrunGrupMarka(int UrunGrupId)
        {
            dtUrunGrupMarkalariTT = new ErpData.StrongTypesNS.UrunGrupMarkalariTTDataTable();
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                olapVeri = new ErpData.OlapVeri(baglanti);
                olapVeri.GetUrunGrupMarka(UrunGrupId, out dtUrunGrupMarkalariTT);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (olapVeri != null) olapVeri.Dispose();
            }

            return dtUrunGrupMarkalariTT;
        }

    }
}