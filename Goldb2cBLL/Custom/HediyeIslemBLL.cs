﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using Goldb2cEntity;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class HediyeIslemBLL
    {
        HediyeIslemDAT datHediyeIslem = new HediyeIslemDAT();

        public int HediyeIslemKaydet(int siparisId, int erpCariId, DateTime hediyeIslemTarihi)
        {
            try
            {
                return datHediyeIslem.HediyeIslemKaydet(siparisId, erpCariId, hediyeIslemTarihi);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable HediyeIslemGetir()
        {
            DataTable dtHediyeIslem = new DataTable();
            try
            {
                dtHediyeIslem = datHediyeIslem.HediyeIslemGetir();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtHediyeIslem;
        }

    }
}
