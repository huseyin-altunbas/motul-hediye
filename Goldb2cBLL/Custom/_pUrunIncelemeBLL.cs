﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Goldb2cInfrastructure;


namespace Goldb2cBLL
{
    /// <summary>
    /// Summary description for UrunIncelemeBLL
    /// </summary>
    public class UrunIncelemeBLL
    {
        public ErpData.GoldB2C baglanti;
        public ErpData.UrunInceleme urunInceleme;
        public ErpData.StrongTypesNS.UrunIncelemeDSDataSet dsUrunInceleme;

        public UrunIncelemeBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet GetUrunIncele(int UrunId)
        {

            try
            {
                dsUrunInceleme = new ErpData.StrongTypesNS.UrunIncelemeDSDataSet();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                urunInceleme = new ErpData.UrunInceleme(baglanti);
                urunInceleme.GetUrunIncele(UrunId, out dsUrunInceleme);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (urunInceleme != null)
                    urunInceleme.Dispose();
            }

            return dsUrunInceleme;
        }
    }
}