﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using Goldb2cEntity;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public partial class UyelikIslemleriBLL
    {
        public DataTable GetirKullaniciByServiceKey(string ServiceKey)
        {
            return datUyelikIslemleri.GetirKullaniciByServiceKey(ServiceKey);
        }
        // kullanıcı girişi
        public int MusteriKullaniciGiris(string email, string sifre)
        {
            // şifre enkript
            Crypt crypt = new Crypt();
            string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
            sifre = crypt.EncryptData(encryptKey, sifre);

            // dönen değer: KullanıcıID
            // >0 ise kullanıcı giriş yapabilir
            // -1 ise e-mail doğru, şifre hatalı
            // -2 ise kullanıcı kayıtlı değil, e-mail ve şifre hatalı
            // 0 ise hata oluştu
            return datUyelikIslemleri.MusteriKullaniciGiris(email, sifre);
        }

        public Kullanici KullaniciSistemKayit(int kullaniciID)
        {
            try
            {
                DataTable dt = MusteriKullaniciSistemKayit(kullaniciID, HttpContext.Current.Session.SessionID);

                if (dt == null || dt.Rows.Count == 0)
                    return new Kullanici(); // struct olduğu için null döndürmüyor

                bool isEmailActivated = false;
                bool isSmsActivated = false;

                //Kullanici kullanici = KullaniciObjectOlustur(dt);
                bool.TryParse(dt.Rows[0]["EmailAktif"].ToString(), out isEmailActivated);
                bool.TryParse(dt.Rows[0]["SmsAktif"].ToString(), out isSmsActivated);
                //kullanici.EmailAktif = isEmailActivated;
                //kullanici.SmsAKtif = isSmsActivated;

                Kullanici kullanici = this.KullaniciObjectOlustur(dt);

                HttpContext.Current.Session.Add("Kullanici", kullanici);
                return kullanici;
            }
            catch (Exception hata)
            {
                return new Kullanici();
            }
        }

        public Kullanici KullaniciObjectOlustur(DataTable dt)
        {
            Kullanici kullanici = new Kullanici();
            try
            {
                kullanici.ErpCariId = Convert.ToInt32(dt.Rows[0]["ErpCariId"]);
            }
            catch (Exception)
            {

                
            }
            kullanici.KullaniciId = (int)dt.Rows[0]["KullaniciId"];
            kullanici.KullaniciAd = dt.Rows[0]["KullaniciAd"].ToString();
            kullanici.KullaniciSoyad = dt.Rows[0]["KullaniciSoyad"].ToString();
            kullanici.Cinsiyet = dt.Rows[0]["Cinsiyet"].ToString();
            kullanici.Email = dt.Rows[0]["Email"].ToString();
            kullanici.Tel = dt.Rows[0]["TelKod"].ToString() + dt.Rows[0]["TelNo"].ToString();
            kullanici.CepTel = dt.Rows[0]["CepTelKod"].ToString() + dt.Rows[0]["CepTelNo"].ToString();
            kullanici.UlkeKod = dt.Rows[0]["UlkeKod"].ToString();
            kullanici.SehirKod = dt.Rows[0]["SehirKod"].ToString();
            kullanici.IlceKod = dt.Rows[0]["IlceKod"].ToString();
            kullanici.UyelikTarihi = (DateTime)dt.Rows[0]["UyelikTarihi"];
            kullanici.GuncellemeTarihi = (DateTime)dt.Rows[0]["GuncellemeTarihi"];
            kullanici.SonGirisTarihi = (DateTime)dt.Rows[0]["SonGirisTarihi"];
            kullanici.SonGirisIP = dt.Rows[0]["SonGirisIP"].ToString();
            kullanici.Online = (bool)dt.Rows[0]["Online"];
            kullanici.GirisSayisi = (int)dt.Rows[0]["GirisSayisi"];
            kullanici.UyelikTipi = (int)dt.Rows[0]["UyelikTipi"];
            kullanici.EmailAktivasyonKod = dt.Rows[0]["EmailAktivasyonKod"].ToString();
            kullanici.DogumTarihi = (DateTime)dt.Rows[0]["DogumTarihi"];
            kullanici.RefEmail = dt.Rows[0]["RefEmail"].ToString();
            kullanici.PortalKod = dt.Rows[0]["PortalKod"].ToString();
            kullanici.FacebookId = dt.Rows[0]["FacebookId"].ToString();
            kullanici.CRMCustomerGUID = dt.Rows[0]["CRMCustomerGUID"].ToString();
            kullanici.ServiceKey =Guid.Parse(dt.Rows[0]["ServiceKey"].ToString());

            try
            {
                kullanici.IlkSifreDegistimi = bool.Parse(dt.Rows[0]["IlkSifreDegistimi"].ToString());
            }
            catch (Exception ex)
            {

                throw;
            }

            return kullanici;
        }
        // kullanıcıyı sisteme kayıt et ve gerekli bilgilierini al
        public DataTable MusteriKullaniciSistemKayit(int kullaniciID, string sessionID)
        {
            // son giriş ip
            string sonGirisIP = FindIP(HttpContext.Current.Request.Headers);

            // dönen değer: kullanıcının bilgileri
            return datUyelikIslemleri.MusteriKullaniciSistemKayit(kullaniciID, sonGirisIP, sessionID);

        }

        public static string FindIP(NameValueCollection headers)
        {

            for (int i = 0; i < headers.Count; i++)
            {
                if (string.Equals(headers.GetKey(i), "Client-IP",
                    System.StringComparison.OrdinalIgnoreCase))
                {
                    return headers.Get(i);
                }
            }

            return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }

        // kullanıcı çıkışı
        public void MusteriKullaniciCikis(int kullaniciID)
        {
            // Cookie sil
            HttpCookie GoldComTrUser = new HttpCookie("GoldComTrUser");
            GoldComTrUser.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Add(GoldComTrUser);

            // sessiondan kaydı sil
            // kullanıcıyı sistemden çıkart
            UyelikIslemleriBLL bllUyelikIslemleri = new UyelikIslemleriBLL();
            datUyelikIslemleri.MusteriKullaniciCikis(kullaniciID);
        }

        // MÜŞTERİ KULLANICI
        // kullanıcıyı kayıt et
        public int MusteriKullaniciKayit(string musteriAd, string musteriSoyad, string email, string sifre, DateTime dogumTarihi, string cinsiyet, string ulkeKod, string sehirKod, string ilceKod, string telKod, string telNo, string telDahili, string cepTelKod, string cepTelNo, bool cepHaber, bool emailHaber, string UyelikKanali, string neredenDuydu, string ilgiAlanlari, string refemail, string CRMCustomerGUID, bool Aktif = false)
        {
            ////AktivasyonKod
            //string karakterler = "1234567890qwertyuopasdfghjklzxcvbnm";
            //string builder = "";
            ////Rasgele 15 karakter seçtiriyorum
            //Random rnd = new Random();
            //for (int i = 0; i < 15; i++)
            //    builder += karakterler[rnd.Next(0, karakterler.Length)].ToString();

            //// şifre enkript
            //Crypt crypt = new Crypt();
            //string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
            //sifre = crypt.EncryptData(encryptKey, sifre);

            // son giriş ip
            string sonGirisIP = FindIP(HttpContext.Current.Request.Headers);

            //if (telDahili == "") telDahili = null;
            //if (neredenDuydu == "") neredenDuydu = null;
            //if (ilgiAlanlari == "") ilgiAlanlari = null;

            //  dönen değer: KullanıcıID
            //  0 ise hata oluştu
            // -1 ise e-mail adres daha önceden kayıtlı
            // >0 ise kullanıcı kayıt edildi 
            try
            {
                return datUyelikIslemleri.MusteriKullaniciKayit(musteriAd, musteriSoyad, email, sifre, dogumTarihi, cinsiyet, ulkeKod, sehirKod, ilceKod, telKod, telNo, telDahili, cepTelKod, cepTelNo, cepHaber, emailHaber, UyelikKanali, neredenDuydu, ilgiAlanlari, sonGirisIP, refemail, "", CRMCustomerGUID, Aktif);
            }
            catch (Exception hata)
            {
                throw hata;
            }
        }

        public int MusteriKullaniciUpdateSmsEmailActivationStatus(int MusteriKullaniciID, bool SmsStatus, bool EmailStatus)
        {
            return datUyelikIslemleri.MusteriKullaniciUpdateSmsEmailActivationStatus(MusteriKullaniciID, SmsStatus, EmailStatus);
        }

        public int GuncelleKullaniciErpCariId(int? KullaniciID, int ErpCariId)
        {
            int _ErpCariId = datUyelikIslemleri.GuncelleKullaniciErpCariId(KullaniciID, ErpCariId);
            Kullanici kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];
            kullanici.ErpCariId = _ErpCariId;
            HttpContext.Current.Session["Kullanici"] = kullanici;
            return _ErpCariId;
        }

        public string GuncelleKullaniciSifre(int KullaniciId, string Sifre, string YeniSifre)
        {
            string Mesaj;

            string _Sifre, _YeniSifre;
            Crypt crypt = new Crypt();
            string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
            _Sifre = crypt.EncryptData(encryptKey, Sifre);
            _YeniSifre = crypt.EncryptData(encryptKey, YeniSifre);


            if (datUyelikIslemleri.GuncelleKullaniciSifre(KullaniciId, _Sifre, _YeniSifre) > 0)
            {
                Mesaj = "Şifreniz değiştirilmiştir.";

                try
                {
                    Kullanici kullanici = (Kullanici)HttpContext.Current.Session["Kullanici"];
                    kullanici.IlkSifreDegistimi = true;
                    HttpContext.Current.Session["Kullanici"] = kullanici;
                }
                catch (Exception ex)
                {

                }

            }
            else
                Mesaj = "Bir hata oluştu lütfen tekrar deneyiniz.";

            return Mesaj;
        }

        public string GuncelleKullanici(int KullaniciId, DateTime DogumTarihi, string Cinsiyet, string UlkeKod, string SehirKod, string IlceKod, string TelKod, string TelNo,
    string CepTelKod, string CepTelNo, bool CepHaber, bool EmailHaber, string RefEmail)
        {
            if (datUyelikIslemleri.GuncelleKullanici(KullaniciId, DogumTarihi, Cinsiyet, UlkeKod, SehirKod, IlceKod, TelKod, TelNo, CepTelKod, CepTelNo, CepHaber, EmailHaber, RefEmail) > 0)
            {
                KullaniciSistemKayit(KullaniciId);
                return "Kullanıcı bilgileri güncellendi";
            }
            else
                return "Bir hata oluştu lütfen tekrar deneyin.";

        }

        public DataTable GetirKullanici(int KullaniciId)
        {
            return datUyelikIslemleri.GetirKullanici(KullaniciId);
        }

        public DataTable GetirKullanici(string UyeId)
        {
            return datUyelikIslemleri.GetirKullanici(UyeId);
        }

        public DataTable GetirKullaniciBySiparisId(Int32 SiparisId)
        {
            return datUyelikIslemleri.GetirKullaniciBySiparisId(SiparisId);
        }

        public static void MusteriGirisineYonlendir(string Url)
        {
            HttpContext.Current.Session["SonUrl"] = Url;
            //HttpContext.Current.Response.Redirect("~/MusteriHizmetleri/UyeGirisi.aspx", false);
            HttpContext.Current.Response.Redirect("/", false);
        }

        public DataTable GetirKullaniciByEmail(string Email)
        {
            return datUyelikIslemleri.GetirKullaniciByEmail(Email);
        }

        public DataTable GetirKullaniciByFacebookId(string FacebookId)
        {
            return datUyelikIslemleri.GetirKullaniciByFacebookId(FacebookId);
        }

        // şifremi unuttum
        public DataRow SifremiUnuttum(string email)
        {

            DataTable dtKullanici = datUyelikIslemleri.GetirKullaniciByEmail(email);

            if (dtKullanici.Rows.Count > 0 && !string.IsNullOrEmpty(dtKullanici.Rows[0]["Email"].ToString()) && !string.IsNullOrEmpty(dtKullanici.Rows[0]["Sifre"].ToString()))
            {
                // şifrenin decyrpt halini gönder
                string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
                Crypt crypt = new Crypt();
                dtKullanici.Rows[0]["Sifre"] = crypt.DecryptData(encryptKey, dtKullanici.Rows[0]["Sifre"].ToString());
                return dtKullanici.Rows[0];
            }
            else
                return null;

        }

        public string RandomPassword()
        {

            Random rnd = new Random();
            StringBuilder Uretilen = new StringBuilder();
            Uretilen.Append(RandomString(rnd, 2, false));
            Uretilen.Append(RandomNumber(rnd, 10, 99));
            Uretilen.Append(RandomString(rnd, 2, false));
            Uretilen.Append(RandomNumber(rnd, 10, 99));
            return Uretilen.ToString();
        }

        private static int RandomNumber(Random rnd, int min, int max)
        {
            return rnd.Next(min, max);
        }

        //* Rasgele STRİNG Üretebilmek 
        private static string RandomString(Random rnd, int size, bool lowerCase)
        {
            string KarakterSeti = "abcdefghijklmnopqrstuvwxzyABCDEFGHIJKLMNOPQRSTUVWXZY";
            StringBuilder Uretilen = new StringBuilder();

            for (int i = 0; i < size; i++)
                Uretilen.Append(KarakterSeti.Substring(rnd.Next(0, KarakterSeti.Length - 1), 1));
            if (lowerCase)
                return Uretilen.ToString().ToLower();

            return Uretilen.ToString().ToUpper();
        }

        public bool GuncelleKullaniciFacebookId(int KullaniciId, string FacebookId)
        {
            return datUyelikIslemleri.GuncelleKullaniciFacebookId(KullaniciId, FacebookId);
        }


        public int GuncelleKullaniciSifreAktif(int kullaniciId, string sifre, bool aktif)
        {
            int sonuc = 0;
            string _sifre;
            Crypt crypt = new Crypt();
            string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
            _sifre = crypt.EncryptData(encryptKey, sifre);
            sonuc = datUyelikIslemleri.GuncelleKullaniciSifreAktif(kullaniciId, _sifre, aktif);
            return sonuc;
        }

        public DataTable GetirKullaniciAll()
        {
            DataTable dtKullaniciAll = new DataTable();
            dtKullaniciAll = datUyelikIslemleri.GetirKullaniciAll();

            if (dtKullaniciAll.Rows.Count > 0)
            {
                foreach (DataRow dr in dtKullaniciAll.Rows)
                {
                    if (dr["Sifre"].ToString().Length > 1)
                    {
                        // şifrenin decyrpt halini gönder
                        string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
                        Crypt crypt = new Crypt();
                        dr["Sifre"] = crypt.DecryptData(encryptKey, dr["Sifre"].ToString());
                    }
                }
            }

            return dtKullaniciAll;
        }

        public DataTable GetirMinMaxCariId()
        {
            return datUyelikIslemleri.GetirMinMaxCariId();
        }


        public DataTable GetirCariId()
        {
            return datUyelikIslemleri.GetirCariId();
        }

        public int MusteriKullaniciKayitAdmin(string musteriAd, string musteriSoyad, string email, string sifre, DateTime dogumTarihi, string cinsiyet, string ulkeKod, string sehirKod, string ilceKod, string telKod, string telNo, string telDahili, string cepTelKod, string cepTelNo, bool cepHaber, bool emailHaber, string UyelikKanali, string neredenDuydu, string ilgiAlanlari, string refemail, int uyelikTipi)
        {
            //AktivasyonKod
            string karakterler = "1234567890qwertyuopasdfghjklzxcvbnm";
            string builder = "";
            //Rasgele 15 karakter seçtiriyorum
            Random rnd = new Random();
            for (int i = 0; i < 15; i++)
                builder += karakterler[rnd.Next(0, karakterler.Length)].ToString();


            // şifre enkript
            Crypt crypt = new Crypt();
            string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
            sifre = crypt.EncryptData(encryptKey, sifre);

            // son giriş ip
            string sonGirisIP = FindIP(HttpContext.Current.Request.Headers);

            //if (telDahili == "") telDahili = null;
            //if (neredenDuydu == "") neredenDuydu = null;
            //if (ilgiAlanlari == "") ilgiAlanlari = null;

            // dönen değer: KullanıcıID
            // 0 ise hata oluştu
            // -1 ise e-mail adres daha önceden kayıtlı
            // >0 ise kullanıcı kayıt edildi 
            try
            {
                return datUyelikIslemleri.MusteriKullaniciKayitAdmin(musteriAd, musteriSoyad, email, sifre, dogumTarihi, cinsiyet, ulkeKod, sehirKod, ilceKod, telKod, telNo, telDahili, cepTelKod, cepTelNo, cepHaber, emailHaber, UyelikKanali, neredenDuydu, ilgiAlanlari, sonGirisIP, refemail, builder.ToString().ToLower(), uyelikTipi);
            }
            catch (Exception hata)
            {

                throw hata;
            }
        }

        public DataTable UyeListesiRaporu(string ad, string soyad, DateTime uyelikTarih1, DateTime uyelikTarih2, DateTime sonGirisTarih1, DateTime sonGirisTarih2)
        {
            DataTable dtUyeListesi = new DataTable();
            dtUyeListesi = datUyelikIslemleri.UyeListesiRaporu(ad, soyad, uyelikTarih1, uyelikTarih2, sonGirisTarih1, sonGirisTarih2);

            if (dtUyeListesi.Rows.Count > 0)
            {
                foreach (DataRow dr in dtUyeListesi.Rows)
                {
                    // şifrenin decyrpt halini gönder
                    //string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
                    //Crypt crypt = new Crypt();
                    //dr["Sifre"] = crypt.DecryptData(encryptKey, dr["Sifre"].ToString());
                }
            }

            return dtUyeListesi;
        }

    }

}