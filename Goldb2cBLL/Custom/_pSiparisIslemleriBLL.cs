﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cInfrastructure;
using Goldb2cEntity;

namespace Goldb2cBLL
{
    public class SiparisIslemleriBLL
    {
        public static ErpData.GoldB2C baglanti;
        public static ErpData.SiparisIslemleri siparisIslemleri;

        ErpData.StrongTypesNS.SiparisAdresTTDataTable dtSiparisAdres;


        public SiparisIslemleriBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        //public void SiparisStokOnay(string InpSipNo, string InpOnay, string InpStok, out int OutSiparisMasterId)
        //{
        //    try
        //    {

        //        baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
        //        siparisIslemleri = new ErpData.SiparisIslemleri(baglanti);
        //        siparisIslemleri.SiparisStokOnay("20120289", 20121450, "Onaylandı.", "", out OutSiparisMasterId);//Ürün bazında sipariş onaylama bu şekilde yapılıyor.
        //        //siparisIslemleri.SiparisStokOnay(InpSipNo, 0, InpStok,"ll", out OutSiparisMasterId);
        //    }
        //    catch (Exception err)
        //    {
        //        throw new Exception(err.Message);
        //    }
        //    finally
        //    {
        //        if (baglanti != null)
        //            baglanti.Dispose();
        //        if (siparisIslemleri != null)
        //            siparisIslemleri.Dispose();
        //    }
        //    // return dtSiparisBilgilendirme;
        //}


        //public void SiparisUrunOnay(string InpSipNo,int InpSipDetNo, string InpOnay, string InpStok, out int OutSiparisMasterId)
        //{
        //    try
        //    {

        //        baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
        //        siparisIslemleri = new ErpData.SiparisIslemleri(baglanti);
        //        siparisIslemleri.SiparisStokOnay(InpSipNo, InpSipDetNo, InpOnay, "", out OutSiparisMasterId);//Ürün bazında sipariş onaylama bu şekilde yapılıyor.

        //    }
        //    catch (Exception err)
        //    {
        //        throw new Exception(err.Message);
        //    }
        //    finally
        //    {
        //        if (baglanti != null)
        //            baglanti.Dispose();
        //        if (siparisIslemleri != null)
        //            siparisIslemleri.Dispose();
        //    }
        //    // return dtSiparisBilgilendirme;
        //}


        public int SiparisKaydetEx(int ErpCariId, List<SiparisDetayEx> lstSiparisDetay)
        {
            int SiparisMasterId = 0;

            ErpData.StrongTypesNS.SiparisDetayExTTDataTable dtSiparisDetayEx = new ErpData.StrongTypesNS.SiparisDetayExTTDataTable();

            for (int i = 0; i < lstSiparisDetay.Count; i++)
            {
                dtSiparisDetayEx.Rows.Add(dtSiparisDetayEx.NewRow());

                dtSiparisDetayEx.Rows[i]["dmiktar"] = lstSiparisDetay[i].Miktar;
                dtSiparisDetayEx.Rows[i]["dfiyat"] = lstSiparisDetay[i].PuanTutar;
                dtSiparisDetayEx.Rows[i]["stok_kod"] = lstSiparisDetay[i].StokKod;
                dtSiparisDetayEx.Rows[i]["stok_ad"] = lstSiparisDetay[i].StokAd;
            }

            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisIslemleri = new ErpData.SiparisIslemleri(baglanti);
                siparisIslemleri.SiparisKaydetEx(ErpCariId, dtSiparisDetayEx, out SiparisMasterId);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisIslemleri != null)
                    siparisIslemleri.Dispose();
            }

            return SiparisMasterId;
        }


        public string SiparisKaydet(int SepetMasterId, int ErpCariId)
        {
            int SiparisMasterId;

            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisIslemleri = new ErpData.SiparisIslemleri(baglanti);
                siparisIslemleri.SiparisKaydet(SepetMasterId, ErpCariId, out SiparisMasterId);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisIslemleri != null)
                    siparisIslemleri.Dispose();
            }

            return SiparisMasterId.ToString();
        }

        public void SiparisSil(int SipMasterId)
        {
            

            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisIslemleri = new ErpData.SiparisIslemleri(baglanti);
                siparisIslemleri.SiparisIptal(SipMasterId);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisIslemleri != null)
                    siparisIslemleri.Dispose();
            }

            //return SipMasterId;
            
        }

        public int SiparisAdresKaydet(ref int InpOutSiparisMasterId, int FaturaAdresId, int TeslimatAdresId)
        {

            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisIslemleri = new ErpData.SiparisIslemleri(baglanti);
                siparisIslemleri.SiparisAdresKaydet(ref InpOutSiparisMasterId, FaturaAdresId, TeslimatAdresId);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisIslemleri != null)
                    siparisIslemleri.Dispose();
            }

            return InpOutSiparisMasterId;
        }

        public DataTable GetSiparisAdres(int SiparisMasterId)
        {
            try
            {
                dtSiparisAdres = new ErpData.StrongTypesNS.SiparisAdresTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisIslemleri = new ErpData.SiparisIslemleri(baglanti);
                siparisIslemleri.GetSiparisAdres(SiparisMasterId, out dtSiparisAdres);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisIslemleri != null)
                    siparisIslemleri.Dispose();
            }

            return dtSiparisAdres;
        }

        public int SiparisSevkiyatKaydet(ref int SiparisMasterId, int SevkiyatTipId)
        {
            try
            {
                dtSiparisAdres = new ErpData.StrongTypesNS.SiparisAdresTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisIslemleri = new ErpData.SiparisIslemleri(baglanti);
                siparisIslemleri.SiparisSevkiyatKaydet(ref SiparisMasterId, SevkiyatTipId);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisIslemleri != null)
                    siparisIslemleri.Dispose();
            }

            return SiparisMasterId;
        }

        public static int SiparisOdemeKaydet(int SiparisMasterId, int TahsilatSecenekDetayId, string OdeyenAdSoyad, string KartNo, string IpAdres, string GuvenlikSeviye, decimal OdenenTutar, bool ParcaliOdendi, string BkmKampanyaKod)
        {
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisIslemleri = new ErpData.SiparisIslemleri(baglanti);
                siparisIslemleri.SiparisOdemeKaydet(ref SiparisMasterId, TahsilatSecenekDetayId, OdeyenAdSoyad, KartNo, IpAdres, GuvenlikSeviye, OdenenTutar, ParcaliOdendi, BkmKampanyaKod);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisIslemleri != null)
                    siparisIslemleri.Dispose();
            }

            return SiparisMasterId;
        }

        public static int SiparisOdemeLogKaydet(SiparisOdemeLog OdemeLog)
        {
            ErpData.StrongTypesNS.SiparisOdemeLogTTDataTable dtSiparisOdemeLog = new ErpData.StrongTypesNS.SiparisOdemeLogTTDataTable();

            int OdemeLogId = OdemeLog.OdemeLogId;

            dtSiparisOdemeLog.Rows.Add(dtSiparisOdemeLog.NewRow());

            dtSiparisOdemeLog.Rows[0]["cari_id"] = OdemeLog.ErpCariId;

            if (!string.IsNullOrEmpty(OdemeLog.IpAdres))
                dtSiparisOdemeLog.Rows[0]["ip_adres"] = OdemeLog.IpAdres;
            else
                dtSiparisOdemeLog.Rows[0]["ip_adres"] = "";

            if (!string.IsNullOrEmpty(OdemeLog.KartNo))
                dtSiparisOdemeLog.Rows[0]["kart_no"] = OdemeLog.KartNo;
            else
                dtSiparisOdemeLog.Rows[0]["kart_no"] = "";

            if (!string.IsNullOrEmpty(OdemeLog.KartAdSoyad))
                dtSiparisOdemeLog.Rows[0]["kart_sahibi"] = OdemeLog.KartAdSoyad;
            else
                dtSiparisOdemeLog.Rows[0]["kart_sahibi"] = "";

            if (!string.IsNullOrEmpty(OdemeLog.KartTip))
                dtSiparisOdemeLog.Rows[0]["kart_tipi"] = OdemeLog.KartTip;
            else
                dtSiparisOdemeLog.Rows[0]["kart_tipi"] = "";

            if (!string.IsNullOrEmpty(OdemeLog.SonKulAy))
                dtSiparisOdemeLog.Rows[0]["kart_skay"] = OdemeLog.SonKulAy;
            else
                dtSiparisOdemeLog.Rows[0]["kart_skay"] = "";

            if (!string.IsNullOrEmpty(OdemeLog.SonKulYil))
                dtSiparisOdemeLog.Rows[0]["kart_skyil"] = OdemeLog.SonKulYil;
            else
                dtSiparisOdemeLog.Rows[0]["kart_skyil"] = "";

            dtSiparisOdemeLog.Rows[0]["bruttahsilat_tutar"] = OdemeLog.Tutar;
            dtSiparisOdemeLog.Rows[0]["trans_baszaman"] = OdemeLog.IslemBaslangicTarih;
            dtSiparisOdemeLog.Rows[0]["trans_bitzaman"] = OdemeLog.IslemBitisTarih;

            if (!string.IsNullOrEmpty(OdemeLog.OrderId))
                dtSiparisOdemeLog.Rows[0]["order_id"] = OdemeLog.OrderId;
            else
                dtSiparisOdemeLog.Rows[0]["order_id"] = "";

            if (!string.IsNullOrEmpty(OdemeLog.PortalKod))
                dtSiparisOdemeLog.Rows[0]["portal_kod"] = OdemeLog.PortalKod;
            else
                dtSiparisOdemeLog.Rows[0]["portal_kod"] = "";

            if (!string.IsNullOrEmpty(OdemeLog.CekimTur))
                dtSiparisOdemeLog.Rows[0]["cekim_tur"] = OdemeLog.CekimTur;
            else
                dtSiparisOdemeLog.Rows[0]["cekim_tur"] = "";

            if (!string.IsNullOrEmpty(OdemeLog.GidenForm))
                dtSiparisOdemeLog.Rows[0]["giden_form"] = OdemeLog.GidenForm;
            else
                dtSiparisOdemeLog.Rows[0]["giden_form"] = "";

            if (!string.IsNullOrEmpty(OdemeLog.GelenForm))
                dtSiparisOdemeLog.Rows[0]["gelen_form"] = OdemeLog.GelenForm;
            else
                dtSiparisOdemeLog.Rows[0]["gelen_form"] = "";

            if (!string.IsNullOrEmpty(OdemeLog.HataKod))
                dtSiparisOdemeLog.Rows[0]["hata_kod"] = OdemeLog.HataKod;
            else
                dtSiparisOdemeLog.Rows[0]["hata_kod"] = "";

            if (!string.IsNullOrEmpty(OdemeLog.HataAciklama))
                dtSiparisOdemeLog.Rows[0]["hata_aciklama"] = OdemeLog.HataAciklama;
            else
                dtSiparisOdemeLog.Rows[0]["hata_aciklama"] = "";

            if (!string.IsNullOrEmpty(OdemeLog.MdStatus))
                dtSiparisOdemeLog.Rows[0]["md_status"] = OdemeLog.MdStatus;
            else
                dtSiparisOdemeLog.Rows[0]["md_status"] = "";

            if (!string.IsNullOrEmpty(OdemeLog.SessionId))
                dtSiparisOdemeLog.Rows[0]["session_id"] = OdemeLog.SessionId;
            else
                dtSiparisOdemeLog.Rows[0]["session_id"] = "";

            dtSiparisOdemeLog.Rows[0]["siparis_id"] = OdemeLog.SiparisMasterId;

            if (!string.IsNullOrEmpty(OdemeLog.TaksitAdet))
                dtSiparisOdemeLog.Rows[0]["taksit_adet"] = OdemeLog.TaksitAdet;
            else
                dtSiparisOdemeLog.Rows[0]["taksit_adet"] = "";

            dtSiparisOdemeLog.Rows[0]["odeme_masterid"] = OdemeLog.TahsilatSecenekId;
            dtSiparisOdemeLog.Rows[0]["odeme_detayid"] = OdemeLog.TahsilatSecenekDetayId;
            if (!string.IsNullOrEmpty(OdemeLog.OnayDurum))
                dtSiparisOdemeLog.Rows[0]["onay_durum"] = OdemeLog.OnayDurum;
            else
                dtSiparisOdemeLog.Rows[0]["onay_durum"] = "";
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisIslemleri = new ErpData.SiparisIslemleri(baglanti);
                siparisIslemleri.SiparisOdemeLogKaydet(ref OdemeLogId, dtSiparisOdemeLog);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisIslemleri != null)
                    siparisIslemleri.Dispose();
            }

            return OdemeLogId;
        }

        public static void SiparisOdemeBankaVerileri(int OdemeLogId, out string ClienID, out string StorKey, out  string TerminalID, out string UserName, out string Password)
        {
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisIslemleri = new ErpData.SiparisIslemleri(baglanti);
                siparisIslemleri.SiparisOdemeBankaVerileri(OdemeLogId, out ClienID, out StorKey, out TerminalID, out UserName, out Password);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisIslemleri != null)
                    siparisIslemleri.Dispose();
            }

        }

        public static DataTable SiparisOdemeBankaVerileriTablo(int OdemeLogId)
        {
            ErpData.StrongTypesNS.SiparisOdemeLogVeriTTDataTable dtSiparisOdemeLog = new ErpData.StrongTypesNS.SiparisOdemeLogVeriTTDataTable();

            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisIslemleri = new ErpData.SiparisIslemleri(baglanti);
                siparisIslemleri.SiparisOdemeBankaVerileriTablo(OdemeLogId, out dtSiparisOdemeLog);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisIslemleri != null)
                    siparisIslemleri.Dispose();
            }

            return dtSiparisOdemeLog;
        }


        public Tuple<int, string, bool> SiparisTekUrunGuncelleKaydet(int ErpCariId, int SiparisNo, string OrderId, string EkChar07, int SatisAdet)
        {
            int SiparisMasterId = 0;
            string StokKod = string.Empty;
            bool SiparisDetayDurum = false;

            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                siparisIslemleri = new ErpData.SiparisIslemleri(baglanti);
                siparisIslemleri.SiparisTekUrunGuncelleKaydet(ErpCariId, SiparisNo, OrderId, EkChar07, SatisAdet, out SiparisMasterId, out StokKod, out SiparisDetayDurum);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (siparisIslemleri != null)
                    siparisIslemleri.Dispose();
            }

            Tuple<int, string, bool> tupleSiparisStok = new Tuple<int, string, bool>(SiparisMasterId, StokKod, SiparisDetayDurum);
            return tupleSiparisStok;

        }

    }
}
