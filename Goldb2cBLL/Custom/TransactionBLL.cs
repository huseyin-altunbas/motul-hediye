﻿using Goldb2cDAT.Cus;
using Goldb2cEntity.Custom;
using Kendo.DynamicLinq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Goldb2cBLL.Custom
{
    public class TransactionBLL
    {
        TransactionDAT transactionDAT = new TransactionDAT();

        public List<TransactionView> Get(View filters)
        {
            return transactionDAT.Get(filters);
        }

        public TransactionView Get(int id)
        {
            View filters = new View();
            filters = FilterHelper.FilterInit(filters);
            filters.Filter.Filters.Add(new Filter { Field = "id", Operator = "eq", Value = id });
            filters.Take = 1;
            List<TransactionView> list = transactionDAT.Get(filters);

            if (list.Count == 1)
                return list[0];
            else
                return null;

        }

        public int Insert(int userId, string title, string description, decimal amount, Goldb2cEntity.Custom.Enums.Transaction.TransactionType transactionType, Goldb2cEntity.Custom.Enums.Generic.Status status, DateTime period)
        {

            return transactionDAT.Insert(userId, title, description, amount, transactionType, status, period);
        }


        public PuanView KullaniciPuanGetir(int KullaniciId)
        {
            return transactionDAT.KullaniciPuanGetir(KullaniciId);
        }

        public DataTable KulanciPuanDurumlari()
        {
            return transactionDAT.KulanciPuanDurumlari();
        }
    }
}
