﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Goldb2cInfrastructure;


namespace Goldb2cBLL
{
    /// <summary>
    /// Summary description for OneCikanlarBLL
    /// </summary>
    public class OneCikanlarBLL
    {
        public OneCikanlarBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ErpData.GoldB2C baglanti;
        public ErpData.OneCikan oneCikan;

        public ErpData.StrongTypesNS.OneCikanlarTTDataTable dtOneCikanlar;

        public DataTable OneCikanlar()
        {
            dtOneCikanlar = new ErpData.StrongTypesNS.OneCikanlarTTDataTable();
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                oneCikan = new ErpData.OneCikan(baglanti);
                oneCikan.OneCikanlar(out dtOneCikanlar);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (oneCikan != null) oneCikan.Dispose();
            }

            return dtOneCikanlar;
        }
    }
}