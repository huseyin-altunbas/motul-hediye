﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace Goldb2cBLL
{
/// <summary>
/// Summary description for CachingBLL
/// </summary>
    public class CachingBLL
    {
        public CachingBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public object GetCache(Delegate fonksiyonadi, bool yenile, string cacheadi, Cache mycache, params object[] parametreler)
        {
            int tekrar = 0;
            bool kontrol = false;
            object dataset = null;
            if (yenile)
            {
                dataset = fonksiyonadi.DynamicInvoke(parametreler);
                HttpContext.Current.Cache.Insert(cacheadi, dataset, null, DateTime.Now.AddMinutes(5), Cache.NoSlidingExpiration);
            }
            else
            {
                while (!kontrol && tekrar <= 5)
                {
                    try
                    {
                        if (HttpContext.Current.Cache.Get(cacheadi) == null)
                        {
                            dataset = fonksiyonadi.DynamicInvoke(parametreler);
                            HttpContext.Current.Cache.Insert(cacheadi, dataset, null, DateTime.Now.AddMinutes(10), Cache.NoSlidingExpiration);
                            tekrar += 1;
                        }
                        kontrol = true;
                    }
                    catch
                    {
                        kontrol = false;
                    }
                }
            }
            return HttpContext.Current.Cache[cacheadi];
        }
    }
}