﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using Goldb2cEntity;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class KategoriIslemleriBLL
    {
        KategoriIslemleriDAT datKategoriIslemleri = new KategoriIslemleriDAT();

        public DataTable GetirKategoriItems()
        {
            return datKategoriIslemleri.GetirKategoriItems();
        }

        public DataTable GetirKategoriItems(string itemId)
        {
            return datKategoriIslemleri.GetirKategoriItems(itemId);
        }

        public int GuncelleItemNameItemUrl(string itemId, string itemName, string itemUrl, int itemOrder, string itemImage)
        {
            int sonuc = 0;
            sonuc = datKategoriIslemleri.GuncelleItemNameItemUrl(itemId, itemName, itemUrl, itemOrder, itemImage);
            return sonuc;
        }

        public int GuncelleItemHtml(string itemId, string itemHtml)
        {
            int sonuc = 0;
            sonuc = datKategoriIslemleri.GuncelleItemHtml(itemId, itemHtml);
            return sonuc;
        }

        public DataTable GetirKategoriMenuItem(int menuItemId)
        {
            return datKategoriIslemleri.GetirKategoriMenuItem(menuItemId);
        }

    }
}
