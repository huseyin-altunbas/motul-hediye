﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using Goldb2cEntity;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class AnketIslemleriBLL
    {
        AnketIslemleriDAT datAnketIslemleri = new AnketIslemleriDAT();

        public DataTable AnketGetir()
        {
            DataTable dtAnket = new DataTable();
            try
            {
                dtAnket = datAnketIslemleri.AnketGetir();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtAnket;
        }

        public DataTable AnketSecenekleriGetir()
        {
            DataTable dtAnketSecenekleri = new DataTable();
            try
            {
                dtAnketSecenekleri = datAnketIslemleri.AnketSecenekleriGetir();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtAnketSecenekleri;
        }

        public int AnketOylariKaydet(int anketId, int secenekId, int kullaniciId)
        {
            try
            {
                string ipNo = FindIP(HttpContext.Current.Request.Headers);

                return datAnketIslemleri.AnketOylariKaydet(anketId, secenekId, kullaniciId, ipNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string FindIP(NameValueCollection headers)
        {

            for (int i = 0; i < headers.Count; i++)
            {
                if (string.Equals(headers.GetKey(i), "Client-IP",
                    System.StringComparison.OrdinalIgnoreCase))
                {
                    return headers.Get(i);
                }
            }

            return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }

        public DataTable AnketOylariGetir()
        {
            DataTable dtAnketOylari = new DataTable();
            try
            {
                dtAnketOylari = datAnketIslemleri.AnketOylariGetir();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtAnketOylari;
        }

        public int AnketTextKaydet(int anketId, int secenekId, int kullaniciId, string text2)
        {
            try
            {
                return datAnketIslemleri.AnketTextKaydet(anketId, secenekId, kullaniciId, text2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable AnketTextGetir()
        {
            DataTable dtAnketText = new DataTable();
            try
            {
                dtAnketText = datAnketIslemleri.AnketTextGetir();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtAnketText;
        }

        public DataTable AnketDetayGetir(int kullaniciId, int anketId, int aciklama, string oyVermeTarih1, string oyVermeTarih2)
        {
            DataTable dtAnketDetay = new DataTable();
            try
            {
                dtAnketDetay = datAnketIslemleri.AnketDetayGetir(kullaniciId, anketId, aciklama, oyVermeTarih1, oyVermeTarih2);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtAnketDetay;
        }

    }
}
