﻿using Goldb2cEntity.Custom;
using Goldb2cInfrastructure;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DapperExtensions;

namespace Goldb2cBLL.Custom
{
    public class HediyeCekiSiparisIslemleri
    {
        public HediyeCekiSiparis Insert(int ErpCariId, int SiparisId, int StokKod, int Adet, string Ad, String Soyad, string Email, String Gsm)
        {
            HediyeCekiSiparis _HediyeCekiSiparis = new HediyeCekiSiparis();
            _HediyeCekiSiparis.ErpCariId = ErpCariId;
            _HediyeCekiSiparis.SiparisId = SiparisId;
            _HediyeCekiSiparis.StokKod = StokKod;
            _HediyeCekiSiparis.Adet = Adet;
            _HediyeCekiSiparis.Ad = Ad;
            _HediyeCekiSiparis.Soyad = Soyad;
            _HediyeCekiSiparis.Email = Email;
            _HediyeCekiSiparis.Gsm = Gsm;
            _HediyeCekiSiparis.Olusturma = DateTime.Now;

            using (SqlConnection cn = new SqlConnection(CustomConfiguration.GoldB2CSqlConn))
            {
                cn.Open();
                var multiKey = cn.Insert(_HediyeCekiSiparis);
                cn.Close();
            }

            return _HediyeCekiSiparis;
        }

        public HediyeCekiSiparis Get(int SiparisId, int StokKod)
        {
            var predicateGroup = new PredicateGroup { Operator = GroupOperator.And, Predicates = new List<IPredicate>() };
            predicateGroup.Predicates.Add(Predicates.Field<HediyeCekiSiparis>(f => f.SiparisId, Operator.Eq, SiparisId));
            predicateGroup.Predicates.Add(Predicates.Field<HediyeCekiSiparis>(f => f.StokKod, Operator.Eq, StokKod));
            //predicateGroup.Predicates.Add(Predicates.Field<HediyeCekiSiparis>(f => f.HediyeCekiGonderildimi, Operator.Eq, HediyeCekiGonderildimi));
            HediyeCekiSiparis _HediyeCekiSiparis = null;

            using (SqlConnection cn = new SqlConnection(CustomConfiguration.GoldB2CSqlConn))
            {
                cn.Open();
                //var predicate = Predicates.Field<HediyeCekiSiparis>(f => f.SiparisId, Operator.Eq, true);
                _HediyeCekiSiparis = cn.GetList<HediyeCekiSiparis>(predicateGroup).First();
                cn.Close();
            }
            return _HediyeCekiSiparis;
        }

        public bool Update(int Id, bool HediyeCekiGonderildimi)
        {
            var predicateGroup = new PredicateGroup { Operator = GroupOperator.And, Predicates = new List<IPredicate>() };
            predicateGroup.Predicates.Add(Predicates.Field<HediyeCekiSiparis>(f => f.Id, Operator.Eq, Id));
            HediyeCekiSiparis _HediyeCekiSiparis = null;
            using (SqlConnection cn = new SqlConnection(CustomConfiguration.GoldB2CSqlConn))
            {
                cn.Open();
                _HediyeCekiSiparis = cn.GetList<HediyeCekiSiparis>(predicateGroup).First();
                _HediyeCekiSiparis.HediyeCekiGonderildimi = HediyeCekiGonderildimi;
                cn.Update(_HediyeCekiSiparis);
                cn.Close();
            }
            return true;
        }

    }
}
