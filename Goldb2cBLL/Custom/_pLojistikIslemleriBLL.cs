﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class LojistikIslemleriBLL
    {
        ErpData.GoldB2C baglanti;
        ErpData.LojistikIslemleri lojistikIslemleri;

        public LojistikIslemleriBLL()
        {

        }


        public DataTable GetLojistikKart(int SiparisMasterId)
        {
            ErpData.StrongTypesNS.LojistikKartTTDataTable dtLojistikKart;
            try
            {
                dtLojistikKart = new ErpData.StrongTypesNS.LojistikKartTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                lojistikIslemleri = new ErpData.LojistikIslemleri(baglanti);
                lojistikIslemleri.GetLojistikKart(SiparisMasterId, out dtLojistikKart);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (lojistikIslemleri != null)
                    lojistikIslemleri.Dispose();
            }
            return dtLojistikKart;

        }
    }
}
