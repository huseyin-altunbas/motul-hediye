﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cDAT;

namespace Goldb2cBLL
{
    public class PostaciBLL
    {
        public int InsertPostaci(int ProjeId, string MailTo, string MailCc, string MailBcc, bool IsHtml, string MailAttach, string MailSubject, string MailBody, string Data1)
        {
            PostaciDAT datPostaci = new PostaciDAT();
            return datPostaci.InsertPostaci(ProjeId, MailTo, MailCc, MailBcc, IsHtml, MailAttach, MailSubject, MailBody, Data1);
        }
    }
}
