﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cInfrastructure;
using System.Data;

namespace Goldb2cBLL
{
    public class EnCokSatanlarBLL
    {
        public EnCokSatanlarBLL()
        {

        }

        ErpData.GoldB2C baglanti;
        ErpData.EnCokSatanlar enCokSatanlar;

        ErpData.StrongTypesNS.EnCokSatanlarTTDataTable dtEnCokSatanlar;


        public DataTable EnCokSatanlarListesi()
        {
            try
            {
                dtEnCokSatanlar = new ErpData.StrongTypesNS.EnCokSatanlarTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                enCokSatanlar = new ErpData.EnCokSatanlar(baglanti);
                enCokSatanlar.EnCokSatanlarListesi(string.Empty, 0, 0, out dtEnCokSatanlar);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (enCokSatanlar != null)
                    enCokSatanlar.Dispose();
            }
            return dtEnCokSatanlar;
        }


    }
}
