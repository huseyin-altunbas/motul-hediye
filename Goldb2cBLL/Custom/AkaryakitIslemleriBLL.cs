﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using Goldb2cEntity;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class AkaryakitIslemleriBLL
    {
        AkaryakitIslemleriDAT datAkaryakitIslemleri = new AkaryakitIslemleriDAT();

        public DataTable CariAkaryakitKartListele(int erpCariId, int kartId)
        {
            return datAkaryakitIslemleri.CariAkaryakitKartListele(erpCariId, kartId);
        }

        public bool CariAkaryakitKartSil(int erpCariId, int kartId)
        {
            return datAkaryakitIslemleri.CariAkaryakitKartSil(erpCariId, kartId);
        }

        public int CariAkaryakitKartKaydet(int erpCariId, int kartId, string plaka, decimal tutar)
        {
            try
            {
                return datAkaryakitIslemleri.CariAkaryakitKartKaydet(erpCariId, kartId, plaka, tutar);
            }
            catch (Exception hata)
            {
                throw hata;
            }
        }

        public int CariAkaryakitKartSiparisKaydet(int erpCariId, string kartIds, int siparisNo)
        {
            try
            {
                return datAkaryakitIslemleri.CariAkaryakitKartSiparisKaydet(erpCariId, kartIds, siparisNo);
            }
            catch (Exception hata)
            {
                throw hata;
            }
        }

        public DataTable AkaryakitKartUrunListele()
        {
            return datAkaryakitIslemleri.AkaryakitKartUrunListele();
        }

        public DataTable AkaryakitKartSiparisListele(int erpCariId, int siparisNo, string kartNo, int kartYukleme, int gonderimDurumu, string ilkTarih, string sonTarih)
        {
            return datAkaryakitIslemleri.AkaryakitKartSiparisListele(erpCariId, siparisNo, kartNo, kartYukleme, gonderimDurumu, ilkTarih, sonTarih);
        }

        public bool AkaryakitKartSiparisGuncelle(int id, int erpCariId, int siparisNo, int kartId, string kartNo, string belgeNo)
        {
            return datAkaryakitIslemleri.AkaryakitKartSiparisGuncelle(id, erpCariId, siparisNo, kartId, kartNo, belgeNo);
        }

        public bool AkaryakitKartKartKontrol(string plaka, string kartNo)
        {
            return datAkaryakitIslemleri.AkaryakitKartKartKontrol(plaka, kartNo);
        }

        public bool AkaryakitKartSiparisYuklemeDurumu(int akaryakitKartSiparisId, bool kartYuklemeDurumu)
        {
            return datAkaryakitIslemleri.AkaryakitKartSiparisYuklemeDurumu(akaryakitKartSiparisId, kartYuklemeDurumu);
        }

        public DataRow AkaryakitKartSiparisListele(int id)
        {
            DataTable dt = datAkaryakitIslemleri.AkaryakitKartSiparisListele(id);
            if (dt.Rows.Count == 1)
                return datAkaryakitIslemleri.AkaryakitKartSiparisListele(id).Rows[0];
            return null;
        }


        public DataTable AkaryakitKartSiparisGetirbySiparisNo(int SiparisNo)
        {
            DataTable dt = datAkaryakitIslemleri.AkaryakitKartSiparisGetirbySiparisNo(SiparisNo);
            return dt;
        }



    }
}
