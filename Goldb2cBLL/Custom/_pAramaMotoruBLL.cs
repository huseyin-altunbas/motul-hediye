﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class AramaMotoruBLL
    {
        ErpData.GoldB2C baglanti;
        ErpData.AramaMotoru aramaMotoru;
        ErpData.StrongTypesNS.AramaMotoruTTDataTable dtAramaMotoruTT;
        ErpData.StrongTypesNS.KategoriAramaMotoruTTDataTable dtKategoriAramaMotoruTT;

        public DataTable getAramaMotoru(string ArananKelime)
        {
            try
            {
                dtAramaMotoruTT = new ErpData.StrongTypesNS.AramaMotoruTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                aramaMotoru = new ErpData.AramaMotoru(baglanti);
                aramaMotoru.GetAramaMotoru(ArananKelime, out dtAramaMotoruTT);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (aramaMotoru != null)
                    aramaMotoru.Dispose();
            }

            return dtAramaMotoruTT;
        }

        public DataTable getKategoriAramaMotoru(string UrunKod1, string ArananKelime)
        {
            try
            {
                dtKategoriAramaMotoruTT = new ErpData.StrongTypesNS.KategoriAramaMotoruTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                aramaMotoru = new ErpData.AramaMotoru(baglanti);
                aramaMotoru.GetKategoriAramaMotoru(UrunKod1, ArananKelime, out dtKategoriAramaMotoruTT);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (aramaMotoru != null)
                    aramaMotoru.Dispose();
            }

            return dtKategoriAramaMotoruTT;
        }

    }
}
