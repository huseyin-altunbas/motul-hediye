﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class LandingPageBLL
    {
        ErpData.GoldB2C baglanti;
        ErpData.LandingPage landingPage;

        public LandingPageBLL()
        {

        }

        public DataTable GetLandingPage(string KonumKod, string Cinsiyet, int ErpCariId, int UrunGrupId, int MarkaId)
        {
            string OutHtml = "";
            ErpData.StrongTypesNS.LandingPageTTDataTable dtLandingPage;
            try
            {
                dtLandingPage = new ErpData.StrongTypesNS.LandingPageTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                landingPage = new ErpData.LandingPage(baglanti);
                landingPage.GetLandingPage(KonumKod, Cinsiyet + "," + ErpCariId.ToString() + "," + UrunGrupId.ToString() + "," + MarkaId.ToString(), 0, out OutHtml, out dtLandingPage);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (landingPage != null)
                    landingPage.Dispose();
            }
            return dtLandingPage;
        }

        public string GetLandingPage(int DetayId)
        {
            string OutHtml = "";
            ErpData.StrongTypesNS.LandingPageTTDataTable dtLandingPage;
            try
            {
                dtLandingPage = new ErpData.StrongTypesNS.LandingPageTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                landingPage = new ErpData.LandingPage(baglanti);
                landingPage.GetLandingPage("", "", DetayId, out OutHtml, out dtLandingPage);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (landingPage != null)
                    landingPage.Dispose();
            }
            return OutHtml;
        }

    }
}
