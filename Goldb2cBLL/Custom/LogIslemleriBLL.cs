﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using Goldb2cEntity;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public partial class LogIslemleriBLL
    {
        LogIslemleriDAT datLogIslemleri = new LogIslemleriDAT();

        public int KullaniciSiparisLogKayit(int kullaniciId, int kullaniciUyelikTip, int siparisId, string aciklama, string hataMesaj)
        {
            string kullaniciIpAdres = "";
            try
            {
                FindIP(HttpContext.Current.Request.Headers);
            }
            catch (Exception ex)
            {

            }

            return datLogIslemleri.KullaniciSiparisLogKayit(kullaniciId, kullaniciUyelikTip, siparisId, aciklama, hataMesaj, kullaniciIpAdres);

        }

        public static string FindIP(NameValueCollection headers)
        {

            for (int i = 0; i < headers.Count; i++)
            {
                if (string.Equals(headers.GetKey(i), "Client-IP",
                    System.StringComparison.OrdinalIgnoreCase))
                {
                    return headers.Get(i);
                }
            }

            return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }

        public DataTable KullaniciSiparisLogBilgileriGetir(int kullaniciId, int kullaniciUyelikTip, int siparisId, string logTarih1, string logTarih2)
        {
            return datLogIslemleri.KullaniciSiparisLogBilgileriGetir(kullaniciId, kullaniciUyelikTip, siparisId, logTarih1, logTarih2);
        }


        public int KullaniciOlusturLogKayit(int kullaniciId, int kullaniciUyelikTip, string aciklama, string hataMesaj, int olusturulanKullaniciId)
        {
            string kullaniciIpAdres = FindIP(HttpContext.Current.Request.Headers);

            return datLogIslemleri.KullaniciOlusturLogKayit(kullaniciId, kullaniciUyelikTip, aciklama, hataMesaj, kullaniciIpAdres, olusturulanKullaniciId);

        }

        public DataTable KullaniciOlusturLogBilgileriGetir(int kullaniciId, int kullaniciUyelikTip, string logTarih1, string logTarih2, int olusturulanKullaniciId)
        {
            return datLogIslemleri.KullaniciOlusturLogBilgileriGetir(kullaniciId, kullaniciUyelikTip, logTarih1, logTarih2, olusturulanKullaniciId);
        }

        public int KullaniciPuanLogKayit(string CRMCustomerGUID, int? totalGoldPoint, int? totalPointAdvance, int siparisId, string aciklama, bool islemOncesiSonrasi)
        {
            return datLogIslemleri.KullaniciPuanLogKayit(CRMCustomerGUID, totalGoldPoint, totalPointAdvance, siparisId, aciklama, islemOncesiSonrasi);
        }

        public DataTable KullaniciPuanLogBilgileriGetir(string CRMCustomerGUID, int siparisId)
        {
            return datLogIslemleri.KullaniciPuanLogBilgileriGetir(CRMCustomerGUID, siparisId);
        }

        public int KullaniciAvansPuanLogKayit(int kullaniciId, int kullanilacakAvansPuan, int siparisId, string aciklama, DateTime siparisTarih)
        {
            return datLogIslemleri.KullaniciAvansPuanLogKayit(kullaniciId, kullanilacakAvansPuan, siparisId, aciklama, siparisTarih);
        }

        public bool KullaniciAvansPuanLogSiparisOnay(int siparisId)
        {
            if (datLogIslemleri.KullaniciAvansPuanLogSiparisOnay(siparisId) > 0)
            {
                return true;
            }
            else
                return false;

        }

        public bool KullaniciAvansPuanLogSiparisIptal(int siparisId)
        {
            if (datLogIslemleri.KullaniciAvansPuanLogSiparisIptal(siparisId) > 0)
            {
                return true;
            }
            else
                return false;

        }

        public DataTable KullaniciAvansPuanLogBilgileriGetir(int KullaniciId, int siparisId, string siparisTarih1, string siparisTarih2, int siparisDurum)
        {
            return datLogIslemleri.KullaniciAvansPuanLogBilgileriGetir(KullaniciId, siparisId, siparisTarih1, siparisTarih2, siparisDurum);
        }
    }
}
