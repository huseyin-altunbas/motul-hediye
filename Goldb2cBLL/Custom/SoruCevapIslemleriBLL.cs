﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using Goldb2cEntity;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public partial class SoruCevapIslemleriBLL
    {
        SoruCevapIslemleriDAT datSoruCevapIslemleri = new SoruCevapIslemleriDAT();

        public DataTable SoruGetir()
        {
            return datSoruCevapIslemleri.SoruGetir();
        }

        public int SoruKaydet(string soru, int soruAcanKullaniciId, int siparisId, int kullaniciId, int soruBaslikId, string dosyaYolu)
        {
            return datSoruCevapIslemleri.SoruKaydet(soru, soruAcanKullaniciId, siparisId, kullaniciId, soruBaslikId, dosyaYolu);
        }

        public DataTable CevapGetir(int soruId)
        {
            return datSoruCevapIslemleri.CevapGetir(soruId);
        }

        public int CevapKaydet(int soruId, string cevap, int cevapVerenKullaniciId, string dosyaYolu)
        {
            return datSoruCevapIslemleri.CevapKaydet(soruId, cevap, cevapVerenKullaniciId, dosyaYolu);
        }

        public int SoruAktifPasif(int soruId, bool aktifpasif, int kullaniciId)
        {
            return datSoruCevapIslemleri.SoruAktifPasif(soruId, aktifpasif, kullaniciId);
        }

        public DataTable SoruAra(string soruAraText, int soruId, int siparisId, int kullaniciId, int soruBaslikId, int aktifPasif)
        {
            return datSoruCevapIslemleri.SoruAra(soruAraText, soruId, siparisId, kullaniciId, soruBaslikId, aktifPasif);
        }

        public DataTable SoruBaslikGetir()
        {
            return datSoruCevapIslemleri.SoruBaslikGetir();
        }

        public DataTable SoruAktifPasifGetir()
        {
            return datSoruCevapIslemleri.SoruAktifPasifGetir();
        }
    }
}
