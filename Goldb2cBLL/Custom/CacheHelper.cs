﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.ApplicationServer.Caching;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class CacheHelper
    {
        DataCache dCache = null;
        private DataCache GetCache(CacheNames cacheName)
        {
            if (!CustomConfiguration.DisableCache)
            {
                try
                {
                    DataCacheFactory factory = new DataCacheFactory();
                    if (cacheName.Equals(CacheNames.defaultCache))
                        dCache = factory.GetDefaultCache();
                    else
                    {
                        dCache = factory.GetCache(cacheName.ToString());
                    }
                }
                catch (Exception err)
                {
                    dCache = null;
                    throw new Exception(err.Message);
                }
            }
            return dCache;
        }


        public object GetFromCache(CacheNames cacheName, string Key)
        {
            object CacheObject = null;

            dCache = GetCache(cacheName);

            if (dCache != null)
            {
                try
                {
                    CacheObject = dCache.Get(Key);
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return CacheObject;
        }

        public void SaveCache(CacheNames cacheName, string key, object value)
        {
            dCache = GetCache(cacheName);
            if (dCache != null)
            {
                try
                {
                    dCache.Add(key, value, new TimeSpan(0, 20, 0));
                }
                catch (Exception excp)
                {
                    ;
                }
            }
        }

        public void UpdateCache(CacheNames cacheName, string key, object value)
        {
            dCache = GetCache(cacheName);
            if (dCache != null)
            {
                try
                {
                    dCache.Put(key, value);
                }
                catch (Exception excp)
                {
                    ;
                }
            }
        }

        public void RemoveCache(CacheNames cacheName, string key)
        {
            dCache = GetCache(cacheName);
            if (dCache != null)
            {
                try
                {
                    dCache.Remove(key);
                }
                catch (Exception excp)
                {
                    ;
                }
            }
        }

    }

    public enum CacheNames
    {
        defaultCache
    }
}
