﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cInfrastructure;
using System.Data;
using Microsoft.ApplicationServer.Caching;

namespace Goldb2cBLL
{
    public class VitrinIslemleriBLL
    {

        public static ErpData.GoldB2C baglanti;
        public static ErpData.VitrinIslemleri vitrinIslemleri;


        ErpData.StrongTypesNS.VitrinBannerTTDataTable dtVitrinBannerTT;

        CacheHelper cacheHelper = new CacheHelper();

        public VitrinIslemleriBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataTable GetVitrinListele(int UrunGrupId)
        {
            DataTable dtVitrin;

            object CacheObject = null;
            //  CacheObject = cacheHelper.GetFromCache(CacheNames.defaultCache, "VitrinIslemleriBLL_GetVitrinListele_P_" + UrunGrupId.ToString());
            if (CacheObject != null)
            {
                dtVitrin = (CacheObject as DataSet).Tables["VitrinKartTT"];
            }
            else
            {
                ErpData.StrongTypesNS.VitrinKartTTDataTable dtVitrinKartTT;
                try
                {
                    dtVitrinKartTT = new ErpData.StrongTypesNS.VitrinKartTTDataTable();
                    baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                    vitrinIslemleri = new ErpData.VitrinIslemleri(baglanti);
                    vitrinIslemleri.GetVitrinListele(UrunGrupId, out dtVitrinKartTT);
                }
                catch (Exception err)
                {
                    throw new Exception(err.Message);
                }
                finally
                {
                    if (baglanti != null)
                        baglanti.Dispose();
                    if (vitrinIslemleri != null)
                        vitrinIslemleri.Dispose();
                }

                dtVitrin = dtVitrinKartTT;

                dtVitrinKartTT.Dispose();
                //DataSet CacheDataset = new DataSet();
                //CacheDataset.Tables.Add(dtVitrinKartTT);
                //cacheHelper.SaveCache(CacheNames.defaultCache, "VitrinIslemleriBLL_GetVitrinListele_P_" + UrunGrupId.ToString(), CacheDataset);

            }

            return dtVitrin;
        }

        public DataTable GetVitrinBanner(int UrunGrupId)
        {
            try
            {
                dtVitrinBannerTT = new ErpData.StrongTypesNS.VitrinBannerTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                vitrinIslemleri = new ErpData.VitrinIslemleri(baglanti);
                vitrinIslemleri.GetVitrinBanner(UrunGrupId, out dtVitrinBannerTT);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message + " - UrunGrupId:" + UrunGrupId.ToString());
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (vitrinIslemleri != null)
                    vitrinIslemleri.Dispose();
            }
            return dtVitrinBannerTT;
        }


    }
}
