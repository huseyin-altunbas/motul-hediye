﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;


namespace Goldb2cBLL
{
    /// <summary>
    /// Summary description for Cookie
    /// </summary>
    public class Cookie
    {
        private ArrayList CookieArray = new ArrayList();

        public Cookie()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ArrayList SetCookie(string Expression, string ExpressionValue)
        {
            CookieArray.Add(Expression + ":" + ExpressionValue);
            return CookieArray;
        }

        public HttpCookie GetCookie(string CookieName)
        {
            HttpCookie myCookie = new HttpCookie(CookieName);
            for (int i = 0; i < CookieArray.Count; i++)
            {
                myCookie[CookieArray[i].ToString().Split(':')[0]] = CookieArray[i].ToString().Split(':')[1];
            }
            myCookie.Expires = DateTime.Now.AddYears(1);
            return myCookie;
        }
    }
}