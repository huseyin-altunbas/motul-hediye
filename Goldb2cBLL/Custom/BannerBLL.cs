﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cDAT;
using System.Data;

namespace Goldb2cBLL
{
    public class BannerBLL
    {
        BannerDAT datBanner = new BannerDAT();
        public DataTable GetirBanner(int BannerId)
        {
            return datBanner.GetirBanner(BannerId);
        }

        public int GuncelleBanner(int BannerId, string BannerHtml)
        {
            return datBanner.GuncelleBanner(BannerId, BannerHtml);
        }

        public int GuncelleMenuItemBanner(string ItemId, string ItemBannerField)
        {
            return datBanner.GuncelleMenuItemBanner(ItemId, ItemBannerField);
        }
    }
}
