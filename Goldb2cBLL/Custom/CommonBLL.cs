﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Data;
using System.Configuration;
using System.Net.Mail;
using Goldb2cInfrastructure;
using System.Threading.Tasks;

namespace Goldb2cBLL
{
    /// <summary>
    /// Summary description for CommonBLL
    /// </summary>
    public class CommonBLL
    {
        public CommonBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static string SecureString(string szInsecure)
        {

            return szInsecure.Replace(";", "").Replace("--", "").Replace("<script>", "").Replace("</script>", "");
        }

        public static string ChangeTRCharacter(string strText)
        {
            strText = strText.Replace("ç", "c");
            strText = strText.Replace("Ç", "C");
            strText = strText.Replace("ğ", "g");
            strText = strText.Replace("Ğ", "G");
            strText = strText.Replace("ı", "i");
            strText = strText.Replace("İ", "I");
            strText = strText.Replace("ö", "o");
            strText = strText.Replace("Ö", "O");
            strText = strText.Replace("ş", "s");
            strText = strText.Replace("Ş", "S");
            strText = strText.Replace("ü", "u");
            strText = strText.Replace("Ü", "U");

            return strText;

        }
    }

    public class Crypt
    {

        static private Byte[] m_Key = new Byte[8];
        static private Byte[] m_IV = new Byte[8];

        public Crypt()
        {
        }

        public string EncryptData(String strKey, String strData)
        {
            string strResult;

            if (strData.Length > 92160)
            {
                strResult = "Hata. Data Çok Büyük.Max 90Kb olabilir.";
                return strResult;
            }

            if (!InitKey(strKey))
            {
                strResult = "Hata. Şifreleme aşamasında Anahtar üretme hatası";
                return strResult;
            }

            strData = String.Format("{0,5:00000}" + strData, strData.Length);

            byte[] rbData = new byte[strData.Length];
            ASCIIEncoding aEnc = new ASCIIEncoding();
            aEnc.GetBytes(strData, 0, strData.Length, rbData, 0);

            DESCryptoServiceProvider descsp = new DESCryptoServiceProvider();

            ICryptoTransform desEncrypt = descsp.CreateEncryptor(m_Key, m_IV);

            MemoryStream mStream = new MemoryStream(rbData);
            CryptoStream cs = new CryptoStream(mStream, desEncrypt, CryptoStreamMode.Read);
            MemoryStream mOut = new MemoryStream();

            int bytesRead;
            byte[] output = new byte[1024];
            do
            {
                bytesRead = cs.Read(output, 0, 1024);
                if (bytesRead != 0)
                    mOut.Write(output, 0, bytesRead);
            } while (bytesRead > 0);

            if (mOut.Length == 0)
                strResult = "";
            else
                strResult = Convert.ToBase64String(mOut.GetBuffer(), 0, (int)mOut.Length);

            return strResult;
        }

        public string DecryptData(String strKey, String strData)
        {
            string strResult;

            if (!InitKey(strKey))
            {
                strResult = "Hata. Şifre çözme aşamasında Anahtar üretme hatası";
                return strResult;
            }

            int nReturn = 0;
            DESCryptoServiceProvider descsp = new DESCryptoServiceProvider();
            ICryptoTransform desDecrypt = descsp.CreateDecryptor(m_Key, m_IV);

            MemoryStream mOut = new MemoryStream();
            CryptoStream cs = new CryptoStream(mOut, desDecrypt, CryptoStreamMode.Write);

            byte[] bPlain = new byte[strData.Length];
            try
            {
                bPlain = Convert.FromBase64CharArray(strData.ToCharArray(), 0, strData.Length);
            }
            catch (Exception)
            {
                strResult = "Hata. Girilen data base64 şifrelemeye uygun değil.";
                return strResult;
            }

            long lRead = 0;
            long lTotal = strData.Length;

            try
            {
                while (lTotal >= lRead)
                {
                    cs.Write(bPlain, 0, (int)bPlain.Length);

                    lRead = mOut.Length + Convert.ToUInt32(((bPlain.Length / descsp.BlockSize) * descsp.BlockSize));
                };

                ASCIIEncoding aEnc = new ASCIIEncoding();
                strResult = aEnc.GetString(mOut.GetBuffer(), 0, (int)mOut.Length);

                String strLen = strResult.Substring(0, 5);
                int nLen = Convert.ToInt32(strLen);
                strResult = strResult.Substring(5, nLen);
                nReturn = (int)mOut.Length;

                return strResult;
            }
            catch (Exception)
            {
                strResult = "Hata. Şifre Çözme hatası. anahtar Hatası yada bozuk data olabilir.";
                return strResult;
            }
        }

        static private bool InitKey(String strKey)
        {
            try
            {

                byte[] bp = new byte[strKey.Length];
                ASCIIEncoding aEnc = new ASCIIEncoding();
                aEnc.GetBytes(strKey, 0, strKey.Length, bp, 0);

                SHA1CryptoServiceProvider sha = new SHA1CryptoServiceProvider();
                byte[] bpHash = sha.ComputeHash(bp);

                int i;
                for (i = 0; i < 8; i++)
                    m_Key[i] = bpHash[i];

                for (i = 8; i < 16; i++)
                    m_IV[i - 8] = bpHash[i];

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }


    }

    public class SendEmail
    {
        public SendEmail()
        {

        }

        // Mail gönderilecek kişi(ler)
        private string[] _mailTo;
        public string[] MailTo
        {
            get { return _mailTo; }
            set { _mailTo = value; }
        }

        // Mail gönderen kişi
        private string _mailFrom = "destek@motulteam.com" + "<" + CustomConfiguration.NoReply + ">";
        public string MailFrom
        {
            get { return _mailFrom; }
            set { _mailFrom = value; }
        }

        // mail reply to
        private string _mailReplyTo = System.Configuration.ConfigurationManager.AppSettings["NoReply"];
        public string MailReplyTo
        {
            get { return _mailReplyTo; }
            set { _mailReplyTo = value; }
        }

        // Mail gönderilecek cc(ler)
        private string[] _mailCc = new string[0];
        public string[] MailCc
        {
            get { return _mailCc; }
            set { _mailCc = value; }
        }

        // Mail gönderilecek bcc(ler)
        private string[] _mailBcc = new string[0];
        public string[] MailBcc
        {
            get { return _mailBcc; }
            set { _mailBcc = value; }
        }

        // mail formatı
        // true: HTML formatta
        // false: Text formatta
        private bool _mailHtml = false;
        public bool MailHtml
        {
            get { return _mailHtml; }
            set { _mailHtml = value; }
        }

        // mail encoding
        private Encoding _mailEncoding = System.Text.UnicodeEncoding.UTF8;
        public Encoding MailEncoding
        {
            get { return _mailEncoding; }
            set { _mailEncoding = value; }
        }

        private List<Attachment> _mailAttach = new List<Attachment>();
        public List<Attachment> MailAttach
        {
            get { return _mailAttach; }
            set { _mailAttach = value; }
        }

        // mail'in sonucu haberi
        private DeliveryNotificationOptions _mailDeliveryOption = DeliveryNotificationOptions.None;
        public DeliveryNotificationOptions MailDeliveryOption
        {
            get { return _mailDeliveryOption; }
            set { _mailDeliveryOption = value; }
        }

        // mail önemi
        private MailPriority _mailPriority = MailPriority.Normal;
        public MailPriority MailPriority
        {
            get { return _mailPriority; }
            set { _mailPriority = value; }
        }

        // mail konusu
        private string _mailSubject;
        public string MailSubject
        {
            get { return _mailSubject; }
            set { _mailSubject = value; }
        }

        // mail body
        private string _mailBody;
        public string MailBody
        {
            get { return _mailBody; }
            set { _mailBody = value; }
        }

        // maili gönder
        public bool SendMail(bool isAsync)
        {
            MailMessage mail = new MailMessage();
            // mail gönderen
            mail.From = new MailAddress(_mailFrom);

            // mail gönderilen(ler)
            if (_mailTo == null) { return false; }
            for (int i = 0; i < _mailTo.Length; i++)
            {
                if (!String.IsNullOrEmpty(_mailTo[i]))
                    mail.To.Add(_mailTo[i]);
            }

            // mail cc(ler)
            for (int i = 0; i < _mailCc.Length; i++)
            {
                mail.CC.Add(_mailCc[i]);
            }

            // mail bcc(ler)
            for (int i = 0; i < _mailBcc.Length; i++)
            {
                mail.Bcc.Add(_mailBcc[i]);
            }


            // mail reply
            //mail.ReplyTo = new MailAddress(_mailReplyTo);
            mail.ReplyTo = new MailAddress(_mailReplyTo);

            // mail formatı
            mail.IsBodyHtml = _mailHtml;

            // mail delivery notifications
            mail.DeliveryNotificationOptions = _mailDeliveryOption;

            // mail encoding
            mail.BodyEncoding = _mailEncoding;

            // mail attachment
            foreach (Attachment attach in _mailAttach)
            {
                mail.Attachments.Add(attach);
            }

            // mail önemi
            mail.Priority = _mailPriority;

            // mail konusu
            if (_mailSubject == null) { return false; }
            mail.Subject = _mailSubject;

            // mail body
            if (_mailBody == null) { return false; }
            mail.Body = _mailBody;


            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                System.Configuration.Configuration config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                System.Net.Configuration.MailSettingsSectionGroup settings = (System.Net.Configuration.MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");
                System.Net.NetworkCredential credential = new System.Net.NetworkCredential(settings.Smtp.Network.UserName, settings.Smtp.Network.Password);
                SmtpClient client = new SmtpClient();
                client.Host = settings.Smtp.Network.Host;
                client.Port = settings.Smtp.Network.Port;
                client.UseDefaultCredentials = false;
                client.Credentials = credential;
                if (isAsync)
                    client.SendAsync(mail, (object)mail);
                else
                    client.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string SifreCriptolaVer(string prmSifre)
        {
            string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
            Crypt crypt = new Crypt();
            //string sifre = crypt.DecryptData(encryptKey, prmEncryptSifre);
            return crypt.EncryptData(encryptKey, prmSifre);
        }

        public static string SifreyiCozVer(string prmCryptSifre)
        {
            string encryptKey = CustomConfiguration.getEncryptKey;// encrypt key
            Crypt crypt = new Crypt();
            return crypt.DecryptData(encryptKey, prmCryptSifre);
        }

        #region

        public static bool IsNumeric(string value)
        {
            if (value == "")
                return false;
            foreach (char c in value)
                if (!((Int16)c > 47 && (Int16)c < 58)) return false;
            return true;
        }

        public static bool IsEmail(string strEmail)
        {
            string regex_Email = @"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$";
            System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(regex_Email);
            return re.IsMatch(strEmail) ? true : false;
        }

        #endregion

    }


}