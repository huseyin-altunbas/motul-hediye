﻿using DapperExtensions;
using Goldb2cEntity.Custom;
using Goldb2cInfrastructure;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Goldb2cBLL.Custom
{

    public class SmsLogIslemleri
    {
        public SmsLog Insert(int SiparisId, string Gsm)
        {
            SmsLog _SmsLog = new SmsLog();
            _SmsLog.SiparisId = SiparisId;
            _SmsLog.SmsGonderim = 1;
            _SmsLog.Olusturma = DateTime.Now;

            using (SqlConnection cn = new SqlConnection(CustomConfiguration.GoldB2CSqlConn))
            {
                cn.Open();
                var multiKey = cn.Insert(_SmsLog);
                cn.Close();
            }

            return _SmsLog;
        }

        public List<SmsLog> GetBySiparisId(int SiparisId)
        {
            var predicateGroup = new PredicateGroup { Operator = GroupOperator.And, Predicates = new List<IPredicate>() };
            predicateGroup.Predicates.Add(Predicates.Field<SmsLog>(f => f.SiparisId, Operator.Eq, SiparisId));

            List<SmsLog> _SmsLog = new List<SmsLog>();

            using (SqlConnection cn = new SqlConnection(CustomConfiguration.GoldB2CSqlConn))
            {
                cn.Open();
                //var predicate = Predicates.Field<SmsLog>(f => f.SiparisId, Operator.Eq, true);
                _SmsLog = cn.GetList<SmsLog>(predicateGroup).ToList();
                cn.Close();
            }
            return _SmsLog;
        }

        public bool Update(SmsLog _smsLog)
        {
            var predicateGroup = new PredicateGroup { Operator = GroupOperator.And, Predicates = new List<IPredicate>() };
            predicateGroup.Predicates.Add(Predicates.Field<SmsLog>(f => f.Id, Operator.Eq, _smsLog.Id));
            SmsLog smsLog = null;
            using (SqlConnection cn = new SqlConnection(CustomConfiguration.GoldB2CSqlConn))
            {
                cn.Open();
                smsLog = cn.GetList<SmsLog>(predicateGroup).First();
                smsLog = _smsLog;
                cn.Update(smsLog);
                cn.Close();
            }
            return true;
        }

    }
}