﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cDAT;

namespace Goldb2cBLL
{
    public class AdvertisingBLL
    {
        AdvertisingDAT datAdvertising = new AdvertisingDAT();

        public DataTable GetirAdvertisingDetay(int AdvertisingMasterId, bool Aktif)
        {
            return datAdvertising.GetirAdvertisingDetay(AdvertisingMasterId, Aktif);
        }

        public void KaydetAdvertisingLog(int AdvertisingMasterId, string IpAdres)
        {
            datAdvertising.KaydetAdvertisingLog(AdvertisingMasterId, IpAdres);
        }

        public void KaydetAdvertisingDetayLog(int AdvertisingDetayId, string IpAdres)
        {
            datAdvertising.KaydetAdvertisingDetayLog(AdvertisingDetayId, IpAdres);
        }
    }
}
