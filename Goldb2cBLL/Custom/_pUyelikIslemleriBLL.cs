﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using Goldb2cInfrastructure;
using Goldb2cEntity;
using Kendo.DynamicLinq;

namespace Goldb2cBLL
{
    /// <summary>
    /// Summary description for UyelikIslemleriBLL
    /// </summary>
    public partial class UyelikIslemleriBLL
    {
        UyelikIslemleriDAT datUyelikIslemleri = new UyelikIslemleriDAT();
        public static ErpData.GoldB2C baglanti;
        public static ErpData.UyelikIslemleri uyelikIslemleri;

        public UyelikIslemleriBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataTable MusteriKullaniciGirisWithCustomerID(string CustomerID, string Sifre)
        {
            // şifre enkript
            Crypt crypt = new Crypt();
            string encryptKey = CustomConfiguration.getEncryptKey; // encrypt key
            Sifre = crypt.EncryptData(encryptKey, Sifre);

            // dönen değer: KullanıcıID
            // >0 ise kullanıcı giriş yapabilir
            // -1 ise e-mail doğru, şifre hatalı
            // -2 ise kullanıcı kayıtlı değil, e-mail ve şifre hatalı
            // 0 ise hata oluştu
            return datUyelikIslemleri.MusteriKullaniciGirisWithCustomerID(CustomerID, Sifre);
        }

        public List<Kullanici> Get(View filters)
        {
           return datUyelikIslemleri.Get(filters);
        }

        public DataTable GetDataTable(View filters)
        {
            return datUyelikIslemleri.GetDataTable(filters);
        }
            public DataTable SehirListesi(string UlkeKod)
        {
            ErpData.StrongTypesNS.SehirListesiTTDataTable dtSehirListesi;
            try
            {
                dtSehirListesi = new ErpData.StrongTypesNS.SehirListesiTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                uyelikIslemleri = new ErpData.UyelikIslemleri(baglanti);
                uyelikIslemleri.SehirListesi(out dtSehirListesi, UlkeKod);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (uyelikIslemleri != null)
                    uyelikIslemleri.Dispose();
            }
            return dtSehirListesi;
        }

        public static DataTable IlceListesi(string SehirKod)
        {
            ErpData.StrongTypesNS.IlceListesiTTDataTable dtIlceListesi;
            try
            {
                dtIlceListesi = new ErpData.StrongTypesNS.IlceListesiTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                uyelikIslemleri = new ErpData.UyelikIslemleri(baglanti);
                uyelikIslemleri.IlceListesi(out dtIlceListesi, SehirKod);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (uyelikIslemleri != null)
                    uyelikIslemleri.Dispose();
            }
            return dtIlceListesi;
        }



        public Kullanici CariVeriOlustur(Kullanici kullanici)
        {
            if (kullanici.KullaniciId > 0)
            {
                int ErpCariId = 0;
                ErpData.StrongTypesNS.CariVeriTTDataTable dtCariVeri = new ErpData.StrongTypesNS.CariVeriTTDataTable();
                dtCariVeri.Rows.Add(dtCariVeri.NewRow());

                dtCariVeri.Rows[0]["CepTelNo"] = kullanici.CepTel;
                dtCariVeri.Rows[0]["Cinsiyet"] = kullanici.Cinsiyet;
                dtCariVeri.Rows[0]["DogumTarihi"] = kullanici.DogumTarihi;
                dtCariVeri.Rows[0]["Email"] = kullanici.Email;
                dtCariVeri.Rows[0]["CariId"] = kullanici.ErpCariId;
                dtCariVeri.Rows[0]["IlceKod"] = kullanici.ErpCariId;
                dtCariVeri.Rows[0]["MusteriAd"] = kullanici.KullaniciAd;
                dtCariVeri.Rows[0]["MusteriSoyad"] = kullanici.KullaniciSoyad;
                dtCariVeri.Rows[0]["RefEmail"] = kullanici.RefEmail;
                dtCariVeri.Rows[0]["SehirKod"] = kullanici.SehirKod;
                dtCariVeri.Rows[0]["TelNo"] = kullanici.Tel;
                dtCariVeri.Rows[0]["UlkeKod"] = kullanici.UlkeKod;
                dtCariVeri.Rows[0]["UyelikTarihi"] = kullanici.UyelikTarihi;

                try
                {
                    baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                    uyelikIslemleri = new ErpData.UyelikIslemleri(baglanti);
                    uyelikIslemleri.CariVeriOlustur(dtCariVeri, out ErpCariId);
                }
                catch (Exception err)
                {
                    throw new Exception(err.Message);
                }
                finally
                {
                    if (baglanti != null)
                        baglanti.Dispose();
                    if (uyelikIslemleri != null)
                        uyelikIslemleri.Dispose();
                }

                datUyelikIslemleri.GuncelleKullaniciErpCariId(kullanici.KullaniciId, ErpCariId);
                kullanici.ErpCariId = ErpCariId;

                HttpContext.Current.Session["Kullanici"] = kullanici;


            }
            return kullanici;
        }


        public DataTable CariAdresListele(int CariId, int CariAdresId)
        {
            ErpData.StrongTypesNS.CariAdresTTDataTable dtCariAdres;
            try
            {
                dtCariAdres = new ErpData.StrongTypesNS.CariAdresTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                uyelikIslemleri = new ErpData.UyelikIslemleri(baglanti);
                uyelikIslemleri.CariAdresListele(CariId, CariAdresId, out dtCariAdres);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (uyelikIslemleri != null)
                    uyelikIslemleri.Dispose();
            }
            return dtCariAdres;
        }

        public bool CariAdresKaydet(int CariId, int CariAdresId, string AdresBaslik, string AdresTanim, string YazismaUnvan, string UlkeKod, string SehirKod, string IlceKod, string MobilTelefon1, string SabitTelefon, string PostaKod,
             string TckimlikNo, string VergiDaire, string VergiNo)
        {
            bool Sonuc = false;
            long _CariAdresId = -1;
            ErpData.StrongTypesNS.CariAdresTTDataTable dtCariAdres;
            try
            {
                dtCariAdres = new ErpData.StrongTypesNS.CariAdresTTDataTable();
                dtCariAdres.Rows.Add(dtCariAdres.NewRow());
                dtCariAdres.Rows[0]["CariId"] = CariId;
                dtCariAdres.Rows[0]["CariKod"] = "";
                dtCariAdres.Rows[0]["KayitId"] = CariAdresId;
                dtCariAdres.Rows[0]["AdresKod"] = InputSafety.SecureString(AdresBaslik);
                dtCariAdres.Rows[0]["YazismaUnvan"] = InputSafety.SecureString(YazismaUnvan);
                dtCariAdres.Rows[0]["AdresTanim"] = InputSafety.SecureString(AdresTanim).Replace("\\", "/");
                dtCariAdres.Rows[0]["UlkeKod"] = InputSafety.SecureString(UlkeKod);
                dtCariAdres.Rows[0]["SehirKod"] = InputSafety.SecureString(SehirKod);
                dtCariAdres.Rows[0]["IlceKod"] = InputSafety.SecureString(IlceKod);
                dtCariAdres.Rows[0]["MobilTelefon1"] = InputSafety.SecureString(MobilTelefon1);
                dtCariAdres.Rows[0]["SabitTelefon1"] = InputSafety.SecureString(SabitTelefon);
                dtCariAdres.Rows[0]["PostaKod"] = InputSafety.SecureString(PostaKod);
                dtCariAdres.Rows[0]["TckimlikNo"] = InputSafety.SecureString(TckimlikNo);
                dtCariAdres.Rows[0]["VergiDaire"] = InputSafety.SecureString(VergiDaire);
                dtCariAdres.Rows[0]["VergiNo"] = InputSafety.SecureString(VergiNo);

                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                uyelikIslemleri = new ErpData.UyelikIslemleri(baglanti);

                uyelikIslemleri.CariAdresKaydet(dtCariAdres);
                Sonuc = true;
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (uyelikIslemleri != null)
                    uyelikIslemleri.Dispose();
            }
            return Sonuc;
        }

        public void CariAdresSil(int ErpCariId, int CariAdresId)
        {
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                uyelikIslemleri = new ErpData.UyelikIslemleri(baglanti);
                uyelikIslemleri.CariAdresSil(ErpCariId, CariAdresId);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (uyelikIslemleri != null)
                    uyelikIslemleri.Dispose();
            }
        }


        public DataTable CariPuanListesi(int ilkCariId, int sonCariId)
        {

            ErpData.StrongTypesNS.PuanListeTTDataTable dtPuanDurum;

            try
            {
                dtPuanDurum = new ErpData.StrongTypesNS.PuanListeTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                uyelikIslemleri = new ErpData.UyelikIslemleri(baglanti);
                uyelikIslemleri.CariPuanListesi(out dtPuanDurum, ilkCariId, sonCariId);
                //uyelikIslemleri.PuanDurumu(out dtPuanDurum, ErpCariId);

            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (uyelikIslemleri != null)
                    uyelikIslemleri.Dispose();
            }
            return dtPuanDurum;
        }

    }

}