﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cInfrastructure;
using System.Data;

namespace Goldb2cBLL
{
    public class PortalAdminBLL
    {
        public ErpData.GoldB2C baglanti;
        public ErpData.PortalAdmin portalAdmin;

        public ErpData.StrongTypesNS.UzmanTTDataTable dtUzmanTT;

        public DataTable UzmanGorHataliLink()
        {
            try
            {
                dtUzmanTT = new ErpData.StrongTypesNS.UzmanTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                portalAdmin = new ErpData.PortalAdmin(baglanti);
                portalAdmin.UzmanGorHataliLink(out dtUzmanTT);
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (portalAdmin != null) portalAdmin.Dispose();
            }

            return dtUzmanTT;
        }

        public string[] UrunBilgisi(int UrunId)
        {
            string[] UrunBilgileri = new string[2];

            string UrunIsim = "";
            string UrunLink = "";

            try
            {
                dtUzmanTT = new ErpData.StrongTypesNS.UzmanTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                portalAdmin = new ErpData.PortalAdmin(baglanti);
                portalAdmin.UrunBilgisi(UrunId, out UrunIsim, out UrunLink);
                UrunBilgileri[0] = UrunIsim;
                UrunBilgileri[1] = UrunLink;
            }
            catch (Exception err)
            {

            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (portalAdmin != null) portalAdmin.Dispose();
            }

            return UrunBilgileri;
        }


        public string[] UzmanGorusuEkle(int UrunId, string UzmanGorusu)
        {

            string[] Sonuc = new string[] { "false", "" };
            bool _Sonuc = false;
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                portalAdmin = new ErpData.PortalAdmin(baglanti);
                portalAdmin.UzmanGorusuEkle(UrunId, UzmanGorusu, out _Sonuc);
            }
            catch (Exception err)
            {
                Sonuc[1] = err.Message;
            }
            finally
            {
                if (baglanti != null) baglanti.Dispose();
                if (portalAdmin != null) portalAdmin.Dispose();

                Sonuc[0] = _Sonuc.ToString();
            }

            return Sonuc;

        }

    }



}
