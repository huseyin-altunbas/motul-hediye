﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cInfrastructure;
using System.Data;

namespace Goldb2cBLL
{
    public class HitUrunIslemleriBLL
    {
        public HitUrunIslemleriBLL()
        {

        }

        ErpData.GoldB2C baglanti;
        ErpData.HitUrunIslemleri hitUrunIslemleri;

        ErpData.StrongTypesNS.HitUrunKategoriTTDataTable dtHitUrunKategori;
        ErpData.StrongTypesNS.HitUrunKartTTDataTable dtHitUrunKart;

        /*
        Fiyatı Düşenler,1,
        Aynı Gün Sevk,2,
        Beğenilenler,3,
        Sınırlı Stok,4,
        Kargo Bedava,5,
        Ön sipariş,6,
        Avantajlı Paket,7,
        En Çok Satan,8,
        En Yeni,9,
        En Çok İncelenen,10,
        Kargo Bedava,11,
        İndirimli,12,
        Kampanyalı,13,
        Editörün Seçimi,14

         */

        public DataTable HitUrunKategoriListesi(string HitUrunTip)
        {
            try
            {
                dtHitUrunKategori = new ErpData.StrongTypesNS.HitUrunKategoriTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                hitUrunIslemleri = new ErpData.HitUrunIslemleri(baglanti);
                hitUrunIslemleri.HitUrunKategoriListesi(HitUrunTip, out dtHitUrunKategori);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (hitUrunIslemleri != null)
                    hitUrunIslemleri.Dispose();
            }
            return dtHitUrunKategori;
        }


        public DataTable HitUrunKartListesi(string HitUrunTip, int UstGrupId, int AltGrupId)
        {
            try
            {
                dtHitUrunKart = new ErpData.StrongTypesNS.HitUrunKartTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                hitUrunIslemleri = new ErpData.HitUrunIslemleri(baglanti);
                hitUrunIslemleri.HitUrunKartListesi(HitUrunTip, UstGrupId, AltGrupId, out dtHitUrunKart);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (hitUrunIslemleri != null)
                    hitUrunIslemleri.Dispose();
            }
            return dtHitUrunKart;
        }

        //public enum HitUrunTip
        //{
        //    FiyatiDusenler = 1,
        //    AyniGunSevk = 2,
        //    Begenilenler = 3,
        //    SinirliStok = 4,
        //    KargoBedava = 5,
        //    OnSiparis = 6,
        //    AvantajliPaket = 7,
        //    EnCokSatan = 8,
        //    EnYeni = 9,
        //    EnCokIncelenen = 10,
        //    KargoBedava = 11,
        //    OnSiparisli = 12,
        //    Indirimli = 13,
        //    Kampanyali = 14
        //}
    }
}
