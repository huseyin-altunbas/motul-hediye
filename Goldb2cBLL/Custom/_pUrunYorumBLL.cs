﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Goldb2cInfrastructure;


namespace Goldb2cBLL
{
    /// <summary>
    /// Summary description for UrunYorumBLL
    /// </summary>
    public class UrunYorumBLL
    {
        public UrunYorumBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ErpData.GoldB2C baglanti;
        public ErpData.UrunYorum urunYorum;
        public ErpData.StrongTypesNS.UrunYorumTTDataTable dtUrunYorum;

        public DataTable GetUrunYorum(int UrunId)
        {
            try
            {
                dtUrunYorum = new ErpData.StrongTypesNS.UrunYorumTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                urunYorum = new ErpData.UrunYorum(baglanti);
                urunYorum.GetUrunYorum(UrunId, out dtUrunYorum);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (urunYorum != null)
                    urunYorum.Dispose();
            }

            return dtUrunYorum;
        }

        public void SetUrunYorum(ErpData.StrongTypesNS.UrunYorumTTDataTable dtUrunYorumTT)
        {
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                urunYorum = new ErpData.UrunYorum(baglanti);
                urunYorum.SetUrunYorum(dtUrunYorumTT);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (urunYorum != null)
                    urunYorum.Dispose();
            }

        }

        public string SetYorumDegerlendir(int YorumId, int YorumDeger, int CariId)
        {
            string OylamaSonuc = "";
            try
            {
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                urunYorum = new ErpData.UrunYorum(baglanti);
                urunYorum.SetYorumDegerlendir(YorumId, YorumDeger, CariId, out OylamaSonuc);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (urunYorum != null)
                    urunYorum.Dispose();
            }

            return OylamaSonuc;
        }
    }
}