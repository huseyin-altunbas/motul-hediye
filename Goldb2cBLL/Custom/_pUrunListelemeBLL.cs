﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Goldb2cInfrastructure;


namespace Goldb2cBLL
{
    /// <summary>
    /// Summary description for UrunListelemeBLL
    /// </summary>
    public class UrunListelemeBLL
    {
        public UrunListelemeBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ErpData.GoldB2C baglanti;
        public ErpData.UrunListeleme urunListeleme;

        CacheHelper cacheHelper = new CacheHelper();

        public DataSet GetUrunListele(int UrunGrupId, int MarkaId, int SayfaNo, string SiralamaKriter, string InpExtraVeri, out int ToplamSayfa, out int ToplamUrun, out int[] FiyatAralik, int[] InpFiyatAralik)
        {
            DataSet _dsUrunListeleme;
            try
            {
                DataTable dtOutParameters;

                ErpData.StrongTypesNS.ExtraSecimTTDataTable dtExtraSecim = new ErpData.StrongTypesNS.ExtraSecimTTDataTable();
                string[] InpExtraVeriArray = InpExtraVeri.Split(',');
                if (InpExtraVeriArray.Length > 0 && !string.IsNullOrEmpty(InpExtraVeriArray[0]))
                {
                    foreach (string _extraVeri in InpExtraVeriArray)
                    {
                        string[] _extraVeriArr = _extraVeri.Split('-');
                        if (_extraVeriArr.Length == 2)
                        {
                            dtExtraSecim.Rows.Add(dtExtraSecim.NewRow());
                            dtExtraSecim.Rows[dtExtraSecim.Rows.Count - 1]["extra_gsecimid"] = _extraVeriArr[0];
                            dtExtraSecim.Rows[dtExtraSecim.Rows.Count - 1]["extra_secimid"] = _extraVeriArr[1];
                            dtExtraSecim.Rows[dtExtraSecim.Rows.Count - 1]["alt_fiyat"] = InpFiyatAralik[0];
                            dtExtraSecim.Rows[dtExtraSecim.Rows.Count - 1]["ust_fiyat"] = InpFiyatAralik[1];
                        }
                    }
                }
                else
                {
                    if (InpFiyatAralik[0] != 0 || InpFiyatAralik[1] != 0)
                    {
                        dtExtraSecim.Rows.Add(dtExtraSecim.NewRow());
                        dtExtraSecim.Rows[dtExtraSecim.Rows.Count - 1]["extra_gsecimid"] = 0;
                        dtExtraSecim.Rows[dtExtraSecim.Rows.Count - 1]["extra_secimid"] = 0;
                        dtExtraSecim.Rows[dtExtraSecim.Rows.Count - 1]["alt_fiyat"] = InpFiyatAralik[0];
                        dtExtraSecim.Rows[dtExtraSecim.Rows.Count - 1]["ust_fiyat"] = InpFiyatAralik[1];
                    }
                }



                object CacheObject = null;
                // CacheObject = cacheHelper.GetFromCache(CacheNames.defaultCache, "UrunIslemleriBLL_GetUrunListele_P_" + UrunGrupId.ToString() + "_" + MarkaId.ToString() + "_" + SayfaNo.ToString() + "_" + InpExtraVeri);
                if (CacheObject != null)
                {
                    _dsUrunListeleme = CacheObject as DataSet;
                    dtOutParameters = _dsUrunListeleme.Tables["dtOutParameters"];
                    ToplamSayfa = Convert.ToInt32(dtOutParameters.Rows[0]["ToplamSayfa"]);
                    ToplamUrun = Convert.ToInt32(dtOutParameters.Rows[0]["ToplamUrun"]);

                    string[] strFiyatAralik = dtOutParameters.Rows[0]["FiyatAralik"].ToString().Split(',');
                    FiyatAralik = new int[] { Convert.ToInt32(strFiyatAralik[0]), Convert.ToInt32(strFiyatAralik[1]) };
                }
                else
                {
                    ErpData.StrongTypesNS.UrunListelemeDSDataSet dsUrunListeleme;
                    try
                    {
                        dsUrunListeleme = new ErpData.StrongTypesNS.UrunListelemeDSDataSet();
                        baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                        urunListeleme = new ErpData.UrunListeleme(baglanti);
                        urunListeleme.GetUrunListele(UrunGrupId, MarkaId, SayfaNo, SiralamaKriter, out ToplamSayfa, out ToplamUrun, out FiyatAralik, out dsUrunListeleme, dtExtraSecim);
                        //urunListeleme.GetUrunListele(UrunGrupId, MarkaId, SayfaNo, out ToplamSayfa, out ToplamUrun, InpExtraVeri, InpFiyatAralik, out FiyatAralik, out dsUrunListeleme);
                    }
                    catch (Exception err)
                    {
                        throw new Exception(err.Message);
                    }
                    finally
                    {
                        if (baglanti != null)
                            baglanti.Dispose();
                        if (urunListeleme != null)
                            urunListeleme.Dispose();
                    }

                    _dsUrunListeleme = dsUrunListeleme;

                    dtOutParameters = new DataTable();
                    dtOutParameters.TableName = "dtOutParameters";
                    dtOutParameters.Columns.Add("ToplamSayfa", typeof(int));
                    dtOutParameters.Columns.Add("ToplamUrun", typeof(int));
                    dtOutParameters.Columns.Add("FiyatAralik", typeof(string));
                    dtOutParameters.Rows.Add(dtOutParameters.NewRow());
                    dtOutParameters.Rows[0]["ToplamSayfa"] = ToplamSayfa;
                    dtOutParameters.Rows[0]["ToplamUrun"] = ToplamUrun;
                    dtOutParameters.Rows[0]["FiyatAralik"] = FiyatAralik[0].ToString() + "," + FiyatAralik[1].ToString();

                    _dsUrunListeleme.Tables.Add(dtOutParameters);



                    //cacheHelper.SaveCache(CacheNames.defaultCache, "UrunIslemleriBLL_GetUrunListele_P_" + UrunGrupId.ToString() + "_" + SayfaNo.ToString() + "_" + InpExtraVeri, _dsUrunListeleme);
                }
            }

            catch (Exception err)
            {

                throw new Exception("hatalı listeleme  \r\n" + err.Message + "\r\n" + err.StackTrace);

            }
            return _dsUrunListeleme;
        }
    }
}