﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Goldb2cInfrastructure;


namespace Goldb2cBLL
{
    /// <summary>
    /// Summary description for UrunGrupIslemleriBLL
    /// </summary>
    public class UrunGrupIslemleriBLL
    {
        public UrunGrupIslemleriBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        ErpData.GoldB2C baglanti;
        ErpData.UrunGrupIslemleri urunGrupIslemleri;
        ErpData.StrongTypesNS.UrunGrupIziTTDataTable dtUrunGrupIzi;
        ErpData.StrongTypesNS.FooterVeriTTDataTable dtFooterVeri;
        ErpData.StrongTypesNS.UrunGrupKartTTDataTable dtUrunGrupKart;
        ErpData.StrongTypesNS.UstUrunGrupKartTTDataTable dtUstUrunGrupKart;
        ErpData.StrongTypesNS.UrunGrupTipTTDataTable dtUrunGrupTip;
        ErpData.StrongTypesNS.UrunGrupBilgisiTTDataTable dtUrunGrupBilgisi;
        ErpData.StrongTypesNS.UrunGrupKodTTDataTable dtUrunGrupKod;

        public DataTable GetUrunGrupIzi(int UrunGrupId)
        {
            try
            {
                dtUrunGrupIzi = new ErpData.StrongTypesNS.UrunGrupIziTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                urunGrupIslemleri = new ErpData.UrunGrupIslemleri(baglanti);
                urunGrupIslemleri.GetUrunGrupIzi(UrunGrupId, out dtUrunGrupIzi);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (urunGrupIslemleri != null)
                    urunGrupIslemleri.Dispose();
            }
            return dtUrunGrupIzi;
        }

        public DataTable GetFooterVeri(int UrunGrupId)
        {
            try
            {
                dtFooterVeri = new ErpData.StrongTypesNS.FooterVeriTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                urunGrupIslemleri = new ErpData.UrunGrupIslemleri(baglanti);
                urunGrupIslemleri.GetFooterVeri(out dtFooterVeri);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (urunGrupIslemleri != null)
                    urunGrupIslemleri.Dispose();
            }
            return dtFooterVeri;
        }

        public DataTable GetUrunGrupListesi(int UrunGrupId)
        {

            try
            {
                dtUrunGrupKart = new ErpData.StrongTypesNS.UrunGrupKartTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                urunGrupIslemleri = new ErpData.UrunGrupIslemleri(baglanti);
                urunGrupIslemleri.GetUrunGrupListesi(UrunGrupId, out dtUrunGrupKart);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (urunGrupIslemleri != null)
                    urunGrupIslemleri.Dispose();
            }
            return dtUrunGrupKart;
        }

        public DataTable GetUstUrunGrupListesi(int UrunGrupId)
        {
            try
            {
                dtUstUrunGrupKart = new ErpData.StrongTypesNS.UstUrunGrupKartTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                urunGrupIslemleri = new ErpData.UrunGrupIslemleri(baglanti);
                urunGrupIslemleri.GetUstUrunGrupListesi(UrunGrupId, out dtUstUrunGrupKart);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (urunGrupIslemleri != null)
                    urunGrupIslemleri.Dispose();
            }
            return dtUstUrunGrupKart;
        }


        public DataTable GetProductGrupListesi(int UrunGrupId)
        {

            try
            {
                dtUrunGrupKart = new ErpData.StrongTypesNS.UrunGrupKartTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                urunGrupIslemleri = new ErpData.UrunGrupIslemleri(baglanti);
                urunGrupIslemleri.GetProductGrup(UrunGrupId, out dtUrunGrupKart);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (urunGrupIslemleri != null)
                    urunGrupIslemleri.Dispose();
            }
            return dtUrunGrupKart;
        }

        public DataTable GetUrunGrupTip(string urunseviye)
        {
            try
            {
                dtUrunGrupTip = new ErpData.StrongTypesNS.UrunGrupTipTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                urunGrupIslemleri = new ErpData.UrunGrupIslemleri(baglanti);
                urunGrupIslemleri.GetUrunGrupTip(urunseviye, out dtUrunGrupTip);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (urunGrupIslemleri != null)
                    urunGrupIslemleri.Dispose();
            }
            return dtUrunGrupTip;
        }

        public DataTable GetUrunGrupBilgisi(int urunGrupId)
        {
            try
            {
                dtUrunGrupBilgisi = new ErpData.StrongTypesNS.UrunGrupBilgisiTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                urunGrupIslemleri = new ErpData.UrunGrupIslemleri(baglanti);
                urunGrupIslemleri.GetUrunGrupBilgisi(urunGrupId, out dtUrunGrupBilgisi);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (urunGrupIslemleri != null)
                    urunGrupIslemleri.Dispose();
            }
            return dtUrunGrupBilgisi;
        }

        public DataTable GetUrunGrupKod(string urunKod1, string urunKod2, string urunKod3, string urunKod4, string urunKod5, string urunKod6, string urunKod7)
        {
            try
            {
                dtUrunGrupKod = new ErpData.StrongTypesNS.UrunGrupKodTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                urunGrupIslemleri = new ErpData.UrunGrupIslemleri(baglanti);
                urunGrupIslemleri.GetUrunGrupKod(urunKod1, urunKod2, urunKod3, urunKod4, urunKod5, urunKod6, urunKod7, out dtUrunGrupKod);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (urunGrupIslemleri != null)
                    urunGrupIslemleri.Dispose();
            }
            return dtUrunGrupKod;
        }

    }
}