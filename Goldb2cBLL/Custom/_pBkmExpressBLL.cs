﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public partial class BkmExpressBLL
    {
        public ErpData.GoldB2C baglanti;
        public ErpData.BKMTahsilatVerileri bkmTahsilatVerileri;
        public ErpData.StrongTypesNS.BkmTaksitSecenekleriDSDataSet dsBkmTaksitSecenekleriDS;

        public DataSet GetBkmTahsilatSecenekleri(decimal TahsilatTutar, string BkmKampanyaKod)
        {
            try
            {
                dsBkmTaksitSecenekleriDS = new ErpData.StrongTypesNS.BkmTaksitSecenekleriDSDataSet();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                bkmTahsilatVerileri = new ErpData.BKMTahsilatVerileri(baglanti);
                bkmTahsilatVerileri.GetBkmTahsilatSecenekleri(TahsilatTutar, BkmKampanyaKod, out dsBkmTaksitSecenekleriDS);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (bkmTahsilatVerileri != null)
                    bkmTahsilatVerileri.Dispose();
            }

            return dsBkmTaksitSecenekleriDS;
        }



    }
}
