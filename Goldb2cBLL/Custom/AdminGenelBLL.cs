﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cDAT;

namespace Goldb2cBLL
{
    public class AdminGenelBLL
    {
        AdminGenelDAT datAdminGenel = new AdminGenelDAT();

        public DataTable GunlukUyeRapor(DateTime baslangic, DateTime bitis)
        {
            return datAdminGenel.GunlukUyeRapor(baslangic, bitis);
        }

        public DataTable GetirAdvertisingMaster()
        {
            return datAdminGenel.GetirAdvertisingMaster();
        }

        public DataTable GetirAdvertisingDetay(int AdvertisingMasterId)
        {
            return datAdminGenel.GetirAdvertisingDetay(AdvertisingMasterId);
        }

        public DataTable GetirAdvertisingDetayByDetayId(int AdvertisingDetayId)
        {
            return datAdminGenel.GetirAdvertisingDetayByDetayId(AdvertisingDetayId);
        }

        public bool KaydetAdvertisingDetay(int AdvertisingMasterId, string GorselUrl, string HedefUrl, string UrunAd, string UrunResim, string UrunLink, decimal UrunFiyat, decimal UrunIndFiyat)
        {
            return datAdminGenel.KaydetAdvertisingDetay(AdvertisingMasterId, GorselUrl, HedefUrl, UrunAd, UrunResim, UrunLink, UrunFiyat, UrunIndFiyat);
        }

        public bool GuncelleAdvertisingDetay(int AdvertisingDetayId, string GorselUrl, string HedefUrl, string UrunAd, string UrunResim, string UrunLink, decimal UrunFiyat, decimal UrunIndFiyat, bool Aktif)
        {
            return datAdminGenel.GuncelleAdvertisingDetay(AdvertisingDetayId, GorselUrl, HedefUrl, UrunAd, UrunResim, UrunLink, UrunFiyat, UrunIndFiyat, Aktif);
        }

        public DataTable GetirAdvertisingLogRapor(int AdvertisingMasterId)
        {
            return datAdminGenel.GetirAdvertisingLogRapor(AdvertisingMasterId);
        }

        public DataTable GetirAdvertisingDetayLogRapor(int AdvertisingMasterId)
        {
            return datAdminGenel.GetirAdvertisingDetayLogRapor(AdvertisingMasterId);
        }

        public void KaydetUzmanSihirbaziLog(int KullaniciId, int UrunId, bool IslemSonuc, string HataMesaj)
        {
            datAdminGenel.KaydetUzmanSihirbaziLog(KullaniciId, UrunId, IslemSonuc, HataMesaj);
        }

        public DataTable GetirUzmanSihirbaziRaporByTarih(DateTime BaslangicTarih, DateTime BitisTarih)
        {
            return datAdminGenel.GetirUzmanSihirbaziRaporByTarih(BaslangicTarih, BitisTarih);
        }

        public DataTable GetirBankaKampanya()
        {
            return datAdminGenel.GetirBankaKampanya();
        }

        public void GuncelleBankaKampanya(string BankaKod, string KampanyaBaslik, string KampanyaIcerik, int KampanyaTaksitSayi, string KampanyaSolGorsel)
        {
            datAdminGenel.GuncelleBankaKampanya(BankaKod, KampanyaBaslik, KampanyaIcerik, KampanyaTaksitSayi, KampanyaSolGorsel);
        }
    }
}
