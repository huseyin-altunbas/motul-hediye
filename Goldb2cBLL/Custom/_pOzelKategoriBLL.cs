﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class OzelKategoriBLL
    {
        public OzelKategoriBLL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ErpData.GoldB2C baglanti;
        public ErpData.OzelKategori ozelKategori;

        ErpData.StrongTypesNS.OzelKategoriMasterTTDataTable dtOzelKategoriMasterTT;
        ErpData.StrongTypesNS.OzelKategoriDetayTTDataTable dtOzelKategoriDetayTT;

        public DataTable OzelKategoriMaster(int OzelKategoriMasterId)
        {
            try
            {
                dtOzelKategoriMasterTT = new ErpData.StrongTypesNS.OzelKategoriMasterTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                ozelKategori = new ErpData.OzelKategori(baglanti);
                ozelKategori.OzelKategoriMaster(OzelKategoriMasterId, out dtOzelKategoriMasterTT);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (ozelKategori != null)
                    ozelKategori.Dispose();
            }

            return dtOzelKategoriMasterTT;
        }

        public DataTable OzelKategoriDetay(int OzelKategoriMasterId, int OzelKategoriDetayId)
        {
            try
            {
                dtOzelKategoriDetayTT = new ErpData.StrongTypesNS.OzelKategoriDetayTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                ozelKategori = new ErpData.OzelKategori(baglanti);
                ozelKategori.OzelKategoriDetay(OzelKategoriMasterId, OzelKategoriDetayId, out dtOzelKategoriDetayTT);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (ozelKategori != null)
                    ozelKategori.Dispose();
            }

            return dtOzelKategoriDetayTT;
        }


    }
}
