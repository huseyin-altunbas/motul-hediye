﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using Goldb2cEntity;
using Goldb2cInfrastructure;

namespace Goldb2cBLL
{
    public class MenuItemIslemleriBLL
    {
        MenuItemIslemleriDAT datMenuItemIslemleri = new MenuItemIslemleriDAT();

        public DataTable GetirMenuItems()
        {
            return datMenuItemIslemleri.GetirMenuItems();
        }

        public DataTable GetirMenuItems(string itemId)
        {
            return datMenuItemIslemleri.GetirMenuItems(itemId);
        }

        public int GuncelleItemNameItemUrl(string itemId,string itemName,string itemUrl)
        {
            int sonuc = 0;
            sonuc = datMenuItemIslemleri.GuncelleItemNameItemUrl(itemId, itemName, itemUrl);
            return sonuc;
        }

        public int GuncelleItemHtml(string itemId, string itemHtml)
        {
            int sonuc = 0;
            sonuc = datMenuItemIslemleri.GuncelleItemHtml(itemId, itemHtml);
            return sonuc;
        }

    }
}
