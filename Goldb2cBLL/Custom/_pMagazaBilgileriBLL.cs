﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Goldb2cInfrastructure;
using System.Data;

namespace Goldb2cBLL
{
    public class MagazaBilgileriBLL
    {

        public ErpData.GoldB2C baglanti;
        public ErpData.MagazaVerileri MagazaVerileri;

        ErpData.StrongTypesNS.MagazaSehirBilgileriTTDataTable dtMagazaSehirBilgileri;
        ErpData.StrongTypesNS.MagazaBilgileriTTDataTable dtMagazaBilgileri;
        ErpData.StrongTypesNS.MagazaDuyurulariTTDataTable dtMagazaDuyurulari;

        public DataTable MagazaSehirBilgileri()
        {
            try
            {
                dtMagazaSehirBilgileri = new ErpData.StrongTypesNS.MagazaSehirBilgileriTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                MagazaVerileri = new ErpData.MagazaVerileri(baglanti);
                MagazaVerileri.MagazaSehirBilgileri(out dtMagazaSehirBilgileri);
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (MagazaVerileri != null)
                    MagazaVerileri.Dispose();
            }

            return dtMagazaSehirBilgileri;
        }

        public DataTable GetMagazaBilgileri(string SehirAd)
        {
            try
            {
                dtMagazaBilgileri = new ErpData.StrongTypesNS.MagazaBilgileriTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                MagazaVerileri = new ErpData.MagazaVerileri(baglanti);
                MagazaVerileri.MagazaBilgileri(SehirAd, out dtMagazaBilgileri);

            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (MagazaVerileri != null)
                    MagazaVerileri.Dispose();
            }

            return dtMagazaBilgileri;
        }

        public DataTable MagazaDuyurulari()
        {
            try
            {
                dtMagazaDuyurulari = new ErpData.StrongTypesNS.MagazaDuyurulariTTDataTable();
                baglanti = new ErpData.GoldB2C(CustomConfiguration.getUyumBaglanti, "", "", "");
                MagazaVerileri = new ErpData.MagazaVerileri(baglanti);
                MagazaVerileri.MagazaDuyurulari(out dtMagazaDuyurulari);

            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                if (baglanti != null)
                    baglanti.Dispose();
                if (MagazaVerileri != null)
                    MagazaVerileri.Dispose();
            }

            return dtMagazaDuyurulari;
        }
    }
}
